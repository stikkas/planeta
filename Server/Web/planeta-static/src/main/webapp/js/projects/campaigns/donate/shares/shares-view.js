/*globals CampaignDonate, Wizard, DonateUtils, Form*/
CampaignDonate.Views.Share = BaseView.extend({
    tagName: 'li',
    className: 'action-card-list_i',
    template: "#campaign-donate-share-template",
    viewEvents: {
        'click': 'shareClicked',
        'touchstart .action-card_counter-val': 'shareClicked'
    },

    events: {
        'click img': 'showImage'
    },

    construct: function () {
        this.openTimeout = null;
        this.closeTimeout = null;

        this.model.set({isSteelHere: false}, {silent: true});
        this.addChildAtElement('.js-campaign-donate-share-number-picker', new CampaignDonate.Views.ShareNumberPickerV2({
            model: this.model,
            controller: this.model.get('singleShare') ? this.model.controller : undefined
        }));

        this.addChildAtElement('.js-manual-input', new CampaignDonate.Views.ManualDonateInput({
            model: this.model,
            controller: this.model.get('singleShare') ? this.model.controller : undefined
        }));

        if (this.model.get('isDonate')) {
            if (window.workspace && (workspace.currentLanguage === "en")) {
                this.model.set({
                    name: 'No reward',
                    nameHtml: 'No reward',
                    description: 'Thank you I don’t need a reward I just want to back the campaign',
                    descriptionHtml: 'Thank you I don’t need a reward I just want to back the campaign'
                }, {silent: true});
            }

            if (window.workspace && (workspace.currentLanguage === "ru")) {
                this.model.set({
                    name: 'Поддержать на любую сумму',
                    nameHtml: 'Поддержать на любую сумму',
                    description: 'Спасибо, мне не нужно вознаграждение, я просто хочу поддержать проект.',
                    descriptionHtml: 'Спасибо, мне не нужно вознаграждение, я просто хочу поддержать проект.'
                }, {silent: true});
            }
        }
        if (this.model.get('singleShare')) {
            this.stopScroll = true;
        }
    },

    showImage: function () {
        var options = {underlyingModal: this.parent};
        App.Mixins.Media.showPhoto(this.model.get('profileId'), this.model.get('imageId'), null, options);
    },

    //Backbone.VERSION = '0.5.3';
    _addViewEvent: function (key, eventName) {
        var self = this;
        this.events[key] = function (e) {
            //e.preventDefault();
//            e.stopPropagation();
            self.trigger(eventName, self);
        };
    },
    onAuthorQuestionReplyChanged: function (e) {
        this.model.set('reply', $(e.target).val(), {silent: true});
    },

    toggleClass: CampaignDonate.Views.toggleClasses,

    afterRender: function () {
        var isAllSold = this.model.get("isAllSold");
        var isSelected = this.model.get('isSelected');

        this.toggleClass('', 'checked', isSelected, this);
        this.toggleClass('', 'disabled', isAllSold, this);
        this.$('.elastic').elastic();

        this.$('.limited-input').each(function () {
            var $el = $(this);
            $el.limitInput({
                limit: $el.data().limit || 0,
                remTextEl: $el.parent().find('.pcf-textarea-count, .rest-count, .text-counter'),
                toggleCounter: /*{
                 show: "focusin",
                 hide: "focusout"
                 }*/ false,
                enableNegative: true
            });
        });

        if (isSelected && this._isElementInDom() && !this.stopScroll) {
            var offset = this.$el.offset();
            if (offset) {
                _.delay(function () {
                    window.scrollTo(0, offset.top - 37);
                }, 500);
            }
        }
        this.stopScroll = true;
        if (ProfileUtils.isInvestingProject(this.model.get('shareStatus'))) {
            this.$el.addClass('investing');
        }
        var shareData = {
            counterEnabled: false,
            hidden: false,
            parseMetaTags: true,
            url: 'https://' + workspace.serviceUrls.mainHost + '/campaigns/' + this.controller.get('webCampaignAlias') +
            '/donate/' + this.model.get('shareId')
        };

        $('.js-share-share').share(shareData);
        if (isSelected && !this.model.get('isSteelHere')) {
            this.openCard();
        }

        inputCurrencyMask.init(this.$el.find('.js-currency-mask'));
    },

    openCard: function openCard() {
        this.model.set({isSteelHere: true}, {silent: true});
        var el = this.$el.find('.action-card');
        if (window.screen.width > 767) {
            var action = el.find('.action-card_action');
            var actionCont = el.find('.action-card_action-cont');
            var actionHeight;
            var input = el.find('.action-card_counter-val');

            actionHeight = actionCont.height();
            action.height(actionHeight);
            el.addClass('focus active');
            this.openTimeout = setTimeout(function () {
                input.focus();
                action.css('height', 'auto');
            }, 550);
        } else {
            el.addClass('focus active');
        }
    },

    closeCard: function (el) {
        if (window.screen.width > 767) {
            var actionCont = el.find('.action-card_action-cont');
            var action = el.find('.action-card_action');
            var actionHeight = actionCont.height();
            action.height(actionHeight);
            this.closeTimeout = setTimeout(function () {
                action.css('height', '');
                el.removeClass('focus active');
            });
        } else {
            el.removeClass('focus active');
        }
    }
});


CampaignDonate.Views.ShareList = BaseListView.extend({
    tagName: "ul",
    className: "action-card-list",
    itemViewType: CampaignDonate.Views.Share,

    events: {
        'shareClicked .action-card-list_i:not(.disabled)': 'switchShare'
    },

    switchShare: function (e, view) {
        var el = view.$el.find('.action-card');
        if (el.hasClass('active')) return;
        var views = view.parent._childViews;
        for (var o in views) {
            var childViews = views[o];
            if (childViews instanceof CampaignDonate.Views.Share) {
                childViews.model.set({isSteelHere: false}, {silent: true});
                clearTimeout(view.openTimeout);
                clearTimeout(view.closeTimeout);
            }
        }

        view.closeCard($('.action-card.active'));
        view.openCard();

        var self = this;
        if (window.screen.width > 767) {
            setTimeout(function () {
                self.controller.switchShare(view.model);
            }, 550);
        } else {
            self.controller.switchShare(view.model);
        }

    }

});


CampaignDonate.Views.ShareNumberPicker = BaseView.extend({
    template: '#campaign-share-numberpicker-template',

    events: {
        'click .js-increase:not(.disabled)': "tryIncreaseQuantity",
        'click .js-decrease:not(.disabled)': "tryDecreaseQuantity",
        'change input[name="quantity"]': 'tryChangeQuantity',
        'keyup input[name="quantity"]': 'tryChangeQuantity'
    },

    tryIncreaseQuantity: function () {
        var quantity = this.model.get('quantity');
        this.changeQuantity(quantity + 1);
    },
    tryDecreaseQuantity: function () {
        var quantity = this.model.get('quantity');
        this.changeQuantity(quantity - 1);
    },
    tryChangeQuantity: _.debounce(function (e) {
        var quantity = $(e.currentTarget).val(),
            oldQuantity = this.model.get('quantity');

        if (this.model.tryChangeQuantity(quantity)) {
            this.changeQuantity(isNaN(parseInt(quantity, 10)) ? this.model.get('quantity') : parseInt(quantity, 10));
        } else {
            $(e.currentTarget).val(oldQuantity);
        }
    }, 300),

    changeQuantity: function (quantity) {
        if (this.model.tryChangeQuantity(quantity)) {
            this.updateQuantityModifiers(quantity);
        }
        this.model.set({
            quantity: quantity
        });
    },
    updateQuantityModifiers: function (quantity) {
        var decDisabled, incDisabled;
        decDisabled = !this.model.isEnoughQuantity(quantity - 1);
        incDisabled = !this.model.isEnoughQuantity(quantity + 1);

        this.toggleClass('.js-decrease', 'disabled', decDisabled, this);
        this.toggleClass('.js-increase', 'disabled', incDisabled, this);
    },

    afterRender: function () {
        this.updateQuantityModifiers(this.model.get('quantity'));
    },

    toggleClass: CampaignDonate.Views.toggleClasses
});

CampaignDonate.Views.ShareNumberPickerV2 = BaseView.extend({
    template: '#campaign-share-numberpicker-v2-template',

    events: {
        'click .js-increase:not(.disabled)': "tryIncreaseQuantity",
        'click .js-decrease:not(.disabled)': "tryDecreaseQuantity",
        'change input[name="quantity"]': 'tryChangeQuantity',
        'keyup input[name="quantity"]': 'tryChangeQuantity',
        'keyup [name="donateAmount"]': 'onDonateAmountManualChange',
        'mouseup [name="donateAmount"]': 'onDonateAmountManualChange'
    },

    getController: function () {
        return this.controller || this.model;
    },

    tryIncreaseQuantity: function () {
        this.model.set({isSteelHere: true}, {silent: true});
        var quantity = this.model.get('quantity');
        quantity++;
        this.changeQuantity(quantity);
        var donateAmount = this.model.get('price') * quantity;
        this.updateDonateAmount(donateAmount);
    },
    tryDecreaseQuantity: function () {
        this.model.set({isSteelHere: true}, {silent: true});
        var quantity = this.model.get('quantity');
        quantity--;
        this.changeQuantity(quantity);
        var donateAmount = this.model.get('price') * quantity;
        this.updateDonateAmount(donateAmount);
    },
    tryChangeQuantity: _.debounce(function (e) {
        console.log('tryChangeQuantity');
        var quantity = $(e.currentTarget).val(),
            oldQuantity = this.model.get('quantity');

        if (this.model.tryChangeQuantity(quantity)) {
            this.changeQuantity(isNaN(parseInt(quantity, 10)) ? this.model.get('quantity') : parseInt(quantity, 10));
        } else {
            $(e.currentTarget).val(oldQuantity);
        }
    }, 300),

    changeQuantity: function (quantity) {
        if (this.model.tryChangeQuantity(quantity)) {
            this.updateQuantityModifiers(quantity);
        }
        this.model.set({
            quantity: quantity
        });
    },
    updateQuantityModifiers: function (quantity) {
        var decDisabled, incDisabled;
        decDisabled = !this.model.isEnoughQuantity(quantity - 1);
        incDisabled = !this.model.isEnoughQuantity(quantity + 1);

        this.toggleClass('.js-decrease', 'disabled', decDisabled, this);
        this.toggleClass('.js-increase', 'disabled', incDisabled, this);

        if (incDisabled) {
            this.$el.find('.js-increase').attr('disabled', true);
        } else {
            this.$el.find('.js-increase').removeAttr('disabled');
        }

        if (decDisabled) {
            this.$el.find('.js-decrease').attr('disabled', true);
        } else {
            this.$el.find('.js-decrease').removeAttr('disabled');
        }
    },

    setBuyButtonState: function (enabled) {
        if (enabled) {
            this.$el.find('.js-navigate-to-purchase').removeAttr('disabled');
            this.toggleClass('.action-card_counter', 'error', false, this);
        } else {
            this.$el.find('.js-navigate-to-purchase').attr('disabled', true);
            this.toggleClass('.action-card_counter', 'error', true, this);
        }
    },

    onDonateAmountManualChange: _.debounce(function (e) {
        var amount = $(e.currentTarget).val().replace(/\s|\D/g, '');
        this.getController().tryChangeDonateAmount(amount.length === 0 ? amount : this.getParsedDonateAmount(amount), true);
        if (!this.model.get('isDonate')) {
            this.updateDonateAmountFromModel();
            if (amount < this.model.get('price') * this.model.get('quantity')) {
                this.setBuyButtonState(false);
            } else {
                this.setBuyButtonState(true);
            }
        }
    }, 300),

    updateDonateAmountFromModel: function (donateAmount) {
        this.setDonateAmount(donateAmount || this.getController().get('donateAmount'));
    },

    setDonateAmount: function (donateAmount) {
        var $donateAmount = this.$('[name="donateAmount"]');
        if ($donateAmount.val() != donateAmount) { // not "!=="!
            $donateAmount.val(StringUtils.humanNumber(donateAmount));
        }

        var alias = this.getController().get('sponsorAlias');
        var inj = window.injection;
        if (alias && inj && inj.setDonateAmount && inj.setDonateAmount[alias]) {
            inj.setDonateAmount[alias](this.$el, this.model, donateAmount);
        }
    },

    getParsedDonateAmount: function (amount) {
        return DonateUtils.parseDonateAmount(amount);
    },

    updateDonateAmount: function (donateAmount) {
        this.getController().tryChangeDonateAmount(donateAmount, false);
        this.setDonateAmount(donateAmount);
    },

    afterRender: function () {
        this.updateQuantityModifiers(this.model.get('quantity'));

        var input = this.$el.find('[name="donateAmount"]');
        if (ProfileUtils.isInvestingProject(this.model.get('shareStatus'))) {
            input.attr('disabled', true);
        } else {
            var share = this.model.get('selectedShare');
            if (share && (ProfileUtils.isInvestingProject(share.get('shareStatus')))) {
                input.attr('disabled', true);
            } else {
                input.removeAttr('disabled');
            }
        }
    },

    toggleClass: CampaignDonate.Views.toggleClasses
});
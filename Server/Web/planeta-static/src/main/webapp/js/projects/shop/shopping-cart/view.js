/*globals LazyHeader, Modal, Shop*/
Shop.Views.AbctractPurchaseCart = BaseView.extend({
    events: {
        'click .js-purchase-cart': 'purchaseCart'
    },

    purchaseCart: function(e) {
        if (window.gtm) {
            window.gtm.trackPurchaseCart(this.model.get('cartItems').models);
        }

        window.location.href = '/payment/shopping-cart-contacts';
    }
});

/* View for shopping cart page.
Uses Shop.Views.ShoppingCartInternal for cart items
 */
Shop.Views.ShoppingCartWrapper = Shop.Views.AbctractPurchaseCart.extend({
    template: '#shopping-cart-wrapper-template',

    construct: function() {
        this.addChildAtElement('.js-anchor-cart-product-list', new Shop.Views.ShoppingCartInternal({
            model: this.model
        }), true);
    },

    modelJSON: function() {
        var totalSum = this.model.get('totalSum');
        return _.extend(this.model.toJSON(), {
            showDeliveryPromo :  totalSum > 0 && totalSum < 5000 && this.model.doRequireDelivery() && this.model.canFreeDelivery()
        });
    }
});


/* Base view for shopping cart items page Shop.Views.ShoppingCartInternal and
 shopping cart in header Shop.Views.ShoppingCartPopup
 */
Shop.Views.ShoppingCart = Shop.Views.AbctractPurchaseCart.extend({
	template: '#shopping-cart-template',
    anchor: '.plate-cont',
    construct: function() {
        this.addChildAtElement('tbody', new Shop.Views.ShoppingCartList({
            controller: this.model,
            collection: this.model.get('cartItems')
        }), true);

        this.bind('removeItemClicked', this.removeItem, this);
        this.bind('increaseCountClicked', this.increaseCount, this);
        this.bind('decreaseCountClicked', this.decreaseCount, this);
        this.bind('quantityChanged', this.changeQuantity, this);
    },

    removeItem: function(_1, view) {
        if (_1 instanceof BaseView) {
            view = _1;
        }
        this.model.removeProduct(view.model);
    },

    increaseCount: function(_1, view) {
        if (_1 instanceof BaseView) {
            view = _1;
        }
        this.model.changeProductQuantity(view.model, view.model.get('count') + 1);
    },

    decreaseCount: function(_1, view) {
        if (_1 instanceof BaseView) {
            view = _1;
        }
        this.model.changeProductQuantity(view.model, view.model.get('count') - 1);
    },

    changeQuantity: function(_1, view) {
        if (_1 instanceof BaseView) {
            view = _1;
        }
        var quantity = parseInt(view.$('input[name=count]').val(), 10);
        this.model.changeProductQuantity(view.model, quantity);
    }
});

/* View for shopping cart page
 */
Shop.Views.ShoppingCartInternal = Shop.Views.ShoppingCart.extend({
    template: '#shopping-cart-2-template',
    tagName: 'table',
    className: 'cart-list table table-bordered fixed-manage-table',
    modelEvents: {
        'destroy': 'dispose'
    },

    construct: function() {
        this.addChildAtElement('tbody', new Shop.Views.ShoppingCartList2({
            controller: this.model,
            collection: this.model.get('cartItems')
        }), true);

        this.bind('removeItemClicked', this.removeItem, this);
        this.bind('increaseCountClicked', this.increaseCount, this);
        this.bind('decreaseCountClicked', this.decreaseCount, this);
        this.bind('quantityChanged', this.changeQuantity, this);
    },

    removeItem: function(view) {
        var self = this;
        Modal.showConfirm('<p>Вы действительно хотите удалить товар из корзины?</p>', "Подтверждение удаления", {
            success: function() {
                self.model.removeProduct(view.model);
            }
        });
    }
});

/* List and its items for shopping cart page
 Used at Shop.Views.ShoppingCart
 Has nested views Shop.Views.ShoppingCartPopupListItem and Shop.Views.ShoppingCartListItem2
 */
Shop.Views.ShoppingCartListItem = BaseView.extend({
    tagName: 'tr',
    className: 'cart-list_i',
    template: '#shopping-cart-item-template',

    viewEvents: {
        'click .js-basket-del': 'removeItemClicked',
        'click .js-basket-plus': 'increaseCountClicked',
        'click .js-basket-minus': 'decreaseCountClicked',
        'change input[name=count]': 'quantityChanged'
    }

});
Shop.Views.ShoppingCartList = BaseListView.extend({
    tagName: 'tbody',
    itemViewType: Shop.Views.ShoppingCartListItem
});

/* List and its items for shopping cart page
 Used at Shop.Views.ShoppingCartInternal
 */
Shop.Views.ShoppingCartListItem2 = Shop.Views.ShoppingCartListItem.extend({
    template: '#shopping-cart-item-2-template'
});

Shop.Views.ShoppingCartList2 = BaseListView.extend({
    tagName: 'tbody',
    itemViewType: Shop.Views.ShoppingCartListItem2,
    onAdd: function (cartItem) {
        var self = this;

        if (window.gtm) {
            if (!this.cartItemList) {
                this.cartItemList = [];
                this.startIndex = this._itemViews.length;
                setTimeout(function () {
                    window.gtm.trackViewCart(self.cartItemList, self.startIndex);
                    delete self.productList;
                }, 10);
            }
            this.cartItemList.push(cartItem);
        }
        return BaseListView.prototype.onAdd.apply(this, arguments);
    }
});

/* Shopping cart widget in header
has child ShoppingCartPopup
 */
Shop.Views.ShoppingCartWidget = BaseView.extend({
    el: '.shop-header_basket',
    template: '#shopping-cart-widget-template',

    events: {
        'click .shop-basket_link': 'togglePopup'
    },

    construct: function() {
        this.model.set({'popupIsOpen':false});
        this.addChildAtElement('.top-drop-block', new Shop.Views.ShoppingCartPopup({
            model: this.model,
            collection: this.model.get('cartItems')
        }));
    },

    togglePopup: function(e) {
        e.preventDefault();
        var self = this;

        if (this.model.get('totalItems') > 0) {
            var popupIsOpen = this.model.get('popupIsOpen');
            if(!popupIsOpen) {
                this.$el.clickOff(function(e) {
                    self.model.set({'popupIsOpen':false});
                });
            }
            this.model.set({'popupIsOpen':!popupIsOpen});
        }
    }
});


/* Popup for shopping cart widget in header
 */
Shop.Views.ShoppingCartPopup = Shop.Views.ShoppingCart.extend({
	template: '#shopping-cart-popup-template',
    construct: function(options) {
        this.views = {};
        this.views.products = this.addChildAtElement('.scroll-content', new Shop.Views.ShoppingCartPopupList({
            collection: options.collection,
            itemViewType: Shop.Views.ShoppingCartPopupListItem,
            controller: this.model
        }));

        this.bind('removeItemClicked', this.removeItem, this);
        this.bind('increaseCountClicked', this.increaseCount, this);
        this.bind('decreaseCountClicked', this.decreaseCount, this);
        this.bind('quantityChanged', this.changeQuantity, this);
    }
});

/*
List and its items for popup at the yellow button on product page
 and for popup at the cart button in the shop header
 */
Shop.Views.ShoppingCartPopupListItem = Shop.Views.ShoppingCartListItem.extend({
    tagName: 'div',
    className: 'basket_i',
    template: '#shopping-cart-popup-item-template'
});
Shop.Views.ShoppingCartPopupList = DefaultContentScrollListView.extend({
    dontScrollPage: true,
    arrows: false,
    autoAdjustContainerSize: true,
    stickToBottom: false,
    handleSideOffset: 8,

    className: 'basket_list'
});


/* Popup at yellow button on product page
Shows current product added to the cart, has no controls to change product count or del product
 */
Shop.Views.ShoppingCartPopupNotification = BaseView.extend({
	template: '#shopping-cart-popup-notification-template',
    timeoutId: null,

    construct: function(options) {
        var product = this.model.get("product");
        this.addChildAtElement('.basket_list', new Shop.Views.ShoppingCartPopupListItem({
            model: new BaseModel({
                'objectId': product.objectId,
                'objectImageUrl': product.coverImageUrl,
                'objectName': product.nameHtml || product.name,
                'price': this.model.get("popupPrice") || product.price,
                'count': 1,
                'productCategory': product.productCategory,
                'editEnabled': false
            })
        }));
    },

    afterRender: function() {
        var self = this;
        this.showPopup();

        this.$el.mouseleave(function() {
            self.scheduleClose();
        });
        this.$el.mouseenter(function() {
            self.clearTimeout();
        });
        this.$el.clickOff(function(e) {
            self.closePopup();
        });
        this.scheduleClose();
    },

    scheduleClose: function() {
        var self = this;
        this.timeoutId = _.delay(function() {
            self.closePopup();
        }, 3000);
    },


    showPopup: function(e) {
        e && e.preventDefault();
        $('.js-product-add').addClass('open');
    },

    closePopup: function(e) {
        e && e.preventDefault();
        this.clearTimeout();
        this.dispose();
        $('.js-product-add').removeClass('open');
    },

    clearTimeout: function() {
        this.timeoutId && clearTimeout(this.timeoutId);
    }
});




/*global CampaignEdit, CrowdFund, Modal*/

CampaignEdit.Views.SharesContent = BaseView.extend({
    className: 'col-8',
    template: '#campaign-edit-shares-content-template',
    modelEvents: {
        'change:activeShare': 'onActiveShareChanged',
        destroy: 'dispose'
    },
    construct: function (options) {
        if (options && options.controller) {
            options.controller.pageData = this.pageData();
        }
        this.model.set('activeShare', this.model.newShare());
        this.onActiveShareChanged();
    },
    onActiveShareChanged: function () {

        if (this.innerView) {
            this.innerView.dispose();
        }
        this.innerView = this.addChildAtElement('.project-create_cont', new CampaignEdit.Views.SharesContentInner({
            model: this.model.get('activeShare'),
            controller: this.model
        }));
    },
    pageData: function () {
        var L10n = {
            _dictionary: {
                "ru": {
                    "section": "Редактирование кампании",
                    "tabName": "вознаграждения"
                },
                "en": {
                    "section": "Campaign edit",
                    "tabName": "rewards"
                }
            }
        };

        var translate = function (word, lang) {
            if (!lang)
                throw "language is empty.";
            return L10n._dictionary[lang][word] || word;
        };

        var lang = window.workspace && (workspace.currentLanguage === "en") ? "en" : "ru";

        return {
            title: translate('section', lang) + ' - ' + translate('tabName', lang)
        }
    }
});

CampaignEdit.Views.ShareItem = CrowdFund.Views.BaseShareItem.extend({
    tagName: 'li',
    className: 'action-card-list_i',
    template: '#campaign-edit-share-template',
    events: {},

    onEditClick: function () {
        var model = new CampaignEdit.Models.Share(this.model.attributes);
        model.uploaderName = this.model.uploaderName || "CampaignEdit.Views.ShareItem." + model.cid;
        var dialogView = new CampaignEdit.Views.SharesContentModal({
            model: model,
            controller: this.controller
        });
        Modal.showDialog(dialogView).done(function () {
            // only for share-edit dialog
            $('.modal-dialog').scrollTop(0);
        });
    },
    onCloneClick: function () {
        this.controller.switchActiveShare(this.model);
    },

    onDeleteClick: function () {
        var self = this;
        Modal.showConfirm("Вы уверены, что хотите удалить вознаграждение?", "Подтверждение удаления", {
            success: function () {
                self.controller.deleteShare(self.model);
            }
        });
    },

    afterRender: function () {
        if (this.model.get('price') == 0) {
            this.$el.addClass('js-not-sortable-share');
        }
        this.$el.toggleClass('action-card-i__investing', ProfileUtils.isInvestingProject(this.model.get("shareStatus")));
    }
});

CampaignEdit.Views.InteractiveShareItem = CrowdFund.Views.BaseShareItem.extend({
    tagName: 'li',
    className: 'action-card-i',
    template: '#interactive-campaign-edit-share-template',
    events: {},

    onEditClick: function () {
        var model = new CampaignEdit.Models.Share(this.model.attributes);
        model.uploaderName = this.model.uploaderName || "CampaignEdit.Views.ShareItem." + model.cid;
        var dialogView = new CampaignEdit.Views.SharesContentModal({
            model: model,
            controller: this.controller
        });
        Modal.showDialog(dialogView).done(function () {
            // only for share-edit dialog
            $('.modal-dialog').scrollTop(0);
        });
    },
    onCloneClick: function () {
        this.controller.switchActiveShare(this.model);
    },

    onDeleteClick: function () {
        var self = this;
        Modal.showConfirm("Вы уверены, что хотите удалить вознаграждение?", "Подтверждение удаления", {
            success: function () {
                self.controller.deleteShare(self.model);
            }
        });
    },

    afterRender: function () {
        if (this.model.get('price') == 0) {
            this.$el.addClass('js-not-sortable-share');
        }
        this.$el.toggleClass('action-card-i__investing', ProfileUtils.isInvestingProject(this.model.get("shareStatus")));
    }
});

CampaignEdit.Views.MustBeOneShare = BaseView.extend({
    template: "#campaign-edit-share-list-must-be-one-share-template",
    el: ".js-one-anchor"
});

CampaignEdit.Views.SharesEditList = BaseListView.extend({
    tagName: 'ul',
    className: 'action-card-list',
    itemViewType: CampaignEdit.Views.ShareItem,
    construct: function () {
    },

    afterRender: function () {
        var self = this;
        var collection = this.collection;
        this.$el.sortable({
            items: '> li:not(.js-not-sortable-share)',
            axis: 'y',
            update: function (event, ui) {
                var $card = ui.item.find('.action-card');
                var draggedShare = collection.findByAttr('shareId', $card.data('shareId'));
                collection.remove(draggedShare, {silent: true});
                collection.add(draggedShare, {at: ui.item.index(), silent: true});
                //sortable переместил элемент на новое место,
                // а вьюха в  this._itemViews осталось на прежнем месте
                // поэтому вызываем ресет чтобы вьюхи шли в том же порядке что и модельки в коллекции
                self.onReset();
                var index = collection.reduceRight(function (foundIdx, item, idx) {
                    return (!_.isNumber(foundIdx) && (item.get('order') || item === draggedShare)) ? idx : foundIdx;
                }, null);

                collection.forEach(function (share, i) {
                    if (i <= index) {
                        share.set('order', i + 1);
                        share.validateShare();
                    }
                });
            }
        });
    },

    onCollectionLengthChanged: function () {
        if (this.parent) {
            this.parent.render();
        }
    }
});

CampaignEdit.Views.InteractibeSharesEditList = BaseListView.extend({
    tagName: 'ul',
    className: 'action-card-list',
    itemViewType: CampaignEdit.Views.InteractiveShareItem,
    construct: function () {
    },

    afterRender: function () {
        var self = this;
        var collection = this.collection;
        this.$el.sortable({
            items: '> li:not(.js-not-sortable-share)',
            axis: 'y',
            update: function (event, ui) {
                var $card = ui.item.find('.action-card-link');
                var draggedShare = collection.findByAttr('shareId', $card.data('shareId'));
                collection.remove(draggedShare, {silent: true});
                collection.add(draggedShare, {at: ui.item.index(), silent: true});
                //sortable переместил элемент на новое место,
                // а вьюха в  this._itemViews осталось на прежнем месте
                // поэтому вызываем ресет чтобы вьюхи шли в том же порядке что и модельки в коллекции
                self.onReset();
                var index = collection.reduceRight(function (foundIdx, item, idx) {
                    return (!_.isNumber(foundIdx) && (item.get('order') || item === draggedShare)) ? idx : foundIdx;
                }, null);

                collection.forEach(function (share, i) {
                    if (i <= index) {
                        share.set('order', i + 1);
                        share.validateShare();
                    }
                });
            }
        });
    },

    onCollectionLengthChanged: function () {
        if (this.parent) {
            this.parent.render();
        }
    }
});

CampaignEdit.Views.SharesEditListWrapper = BaseView.extend({
    className: 'col-4',
    template: '#campaign-edit-share-list-template',
    construct: function () {
        var self = this;
        this.addChildAtElement('.action-card-block', new CampaignEdit.Views.SharesEditList({
            collection: this.model.getSharesCollection(),
            controller: this.model,
            parent: self
        }));
    }
});

CampaignEdit.Views.InteractiveSharesEditListWrapper = BaseView.extend({
    className: 'col-4',
    template: '#interactive-campaign-edit-share-list-template',
    construct: function () {
        var self = this;
        this.addChildAtElement('.action-card-block', new CampaignEdit.Views.InteractibeSharesEditList({
            collection: this.model.getSharesCollection(),
            controller: this.model,
            parent: self
        }));
    }
});

CampaignEdit.Views.SharesContentModal = Modal.OverlappedView.extend({
    className: 'modal modal-action-card-edit',
    template: '#campaign-edit-share-modal-template',
    modelEvents: {
        destroy: 'dispose',
        closeModal: 'cancel'
    },
    construct: function () {
        this.innerView = this.addChildAtElement('.project-create_list', new CampaignEdit.Views.SharesContentInner({
            model: this.model,
            controller: this.controller
        }));
    },

    cancel: function () {
        var self = this;
        if (this.controller.shareChanged(this.model)) {
            Modal.showConfirm("Текущее вознаграждение не сохранено. Продолжить?", "Подтверждение перехода", {
                success: function () {
                    Modal.OverlappedView.prototype.cancel.call(self);
                }
            });
        } else {
            Modal.OverlappedView.prototype.cancel.call(self);
        }
    }
});



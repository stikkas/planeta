Shop = Shop || {};

Shop.Models = Shop.Models || {};

// TODO remove inheritance from CommonDonate.Models ..... as soon as possible  
Shop.Models.DeliveryPage = CommonDonate.Models.PaymentAndDeliveryPage.extend({
    initialize: function (options) {
        CommonDonate.Models.PaymentAndDeliveryPage.prototype.initialize.apply(this, arguments);
        if(options && options.contacts) {
            var contacts = options.contacts,
                    cur_attrs = this.attributes,
                    new_attrs = {};
            if (!cur_attrs.customerName) {
                new_attrs.customerName = contacts.customerName || '';
            }
            if (!cur_attrs.profileId) {
                new_attrs.profileId = contacts.customerId || -1;
            }
            if (!cur_attrs.email) {
                new_attrs.email = contacts.email || '';
            }
            if (!cur_attrs.phoneNumber) {
                new_attrs.phoneNumber = contacts.phoneNumber || '';
            }
            if (!cur_attrs.comment) {
                new_attrs.comment = contacts.comment || '';
            }
            if (!cur_attrs.deliveryType) {
                new_attrs.deliveryType = contacts.deliveryType || 'NOT_SET';
            }
            _.extend(new_attrs, {
                'serviceId': contacts.serviceId || 0,
                'deliveryPrice': contacts.deliveryPrice || 0,
                'onlyDigitalProducts': contacts.onlyDigitalProducts || false});

            if (contacts.customerContacts) {
                var cur_cc = cur_attrs.customerContacts,
                        new_cc = contacts.customerContacts;
                new_attrs.customerContacts = {
                    'countryId': cur_cc.countryId || new_cc.countryId || 0,
                    'city': cur_cc.city || new_cc.city || '',
                    'street': cur_cc.street || new_cc.street || '',
                    'zipCode': cur_cc.zipCode || new_cc.zipCode || ''
                };
            }
            this.set(new_attrs);
        }
    }
});

Shop.Models.PaymentPage = CommonDonate.Models.PaymentAndDeliveryPage.extend({
    initialize: function (options) {
        CommonDonate.Models.PaymentAndDeliveryPage.prototype.initialize.apply(this, arguments);
    }
});

Shop.Models.PromoCodeSection = BaseModel.extend({
    defaults: {
        promoCode: '',
        promoCodeId: 0,
        discountAmount: 0,
        freeDelivery: false,
        errorMessage: ''
    },

    url: function () {
       return '/api/public/check-promo-code.json?promoCode=' + this.get('promoCode');
    },

    applyPromoCode: function (promoCode) {
        var self = this;

        this.set({promoCode: promoCode}, {silent: true});
        var parentModel = this.get("parentModel");

        this.fetchX()
            .done(function (data) {
                if (data) {
                    var deliveryDiscount = 0;
                    if (data.freeDelivery) {
                        deliveryDiscount = parentModel.get("deliveryPrice");
                    }
                    var totalPriceDiscount = data.totalPriceDiscount || 0;

                    if (totalPriceDiscount > 0 || deliveryDiscount > 0) {
                        var discountAmount = totalPriceDiscount + deliveryDiscount;
                        parentModel.set({
                            discountAmount: totalPriceDiscount + deliveryDiscount
                        });

                        var paymentModel = parentModel.get("payment");
                        paymentModel.setPrice(parentModel.get("donateAmount") + parentModel.get("deliveryPrice") - discountAmount);

                        if (paymentModel.get("price") == 0) {
                            paymentModel.set({'paymentMethodId': paymentModel.PLANETA_PAYMENT_METHOD_ID}, {silent:true});
                        }
                    }
                }
            })
            .fail(function (e) {
                if(e) {
                    self.set({errorMessage : e.errorMessage});
                    e.fieldErrors = {promoCode : 'Неверный промо-код'};
                    Form.isValid(e, $('.js-promo-code-block'), false, 'fieldset');
                    workspace.appView.showErrorMessage(e.errorMessage);
                }
            })
    }
});





/**
 * User: alexa_000
 * Date: 10.01.14
 * Time: 21:22
 */
/*globals Campaign, Orders, PrivacyUtils*/
var CampaignSharesStats = {};
CampaignSharesStats.Models = {};

CampaignSharesStats.Models.BaseFilter = Orders.Models.BaseFilter.extend({

    getUrl: function() {
        return '/admin/campaign-shares-sale-list.json?' + this.getParams();
    },
    getDateData: function() {
        var result = '';
        if (this.has('timeStart')) {
            result += '&dateFrom=' + this.get('timeStart');
        }
        if (this.has('timeFinish')) {
            result += '&dateTo=' + this.get('timeFinish');
        }
        return result;
    }

});

CampaignSharesStats.Models.ReportDownloader = Orders.Models.BaseReportDownloader.extend({

    getDownloadUrl: function() {
        return '/admin/campaign-shares-stats-report.html?' + this.getParams();
    }

});


CampaignSharesStats.Models.Controller = Campaign.Models.AbstractBaseController.extend({

    initialize: function() {
        this.filter = new CampaignSharesStats.Models.BaseFilter({
            campaignId: this.get('campaignId')
        });
        this.ordersLoader = new BaseModel({
            filter: this.getFilter()
        });
        this.downloader = new CampaignSharesStats.Models.ReportDownloader({
            campaignId: this.get('campaignId')
        });
        this.orders = new CampaignSharesStats.Models.OrderCollection({
            currentFilter: this.filter
        });
        this.downloader.controller = this;
        this.orders.controller = this;
        this.getFilter().bind('filterChanged', this.onFilterUrlChange, this);
    },

    getFilter: function() {
        return this.filter;
    },

    prefetch: function (options) {
        var self = this;
        var oldOptions = _.clone(options);
        options.success = function (response) {
            var extraStatsEnabled = self.get('affiliateProgramEnabled') && (PrivacyUtils.isAdmin() || PrivacyUtils.hasAdministrativeRole());
            self.set({
                extraStatsEnabled: extraStatsEnabled
            });
            self.orders.listHeader.set({
                extraStatsEnabled: extraStatsEnabled
            });
            self.filter.set({
                extraStatsEnabled: extraStatsEnabled
            });
            self.orders.extraStatsEnabled = extraStatsEnabled;
            if (oldOptions && oldOptions.success) {
                oldOptions.success(response);
            }
        };
        Campaign.Models.AbstractBaseController.prototype.prefetch.call(this, options);
    }
});

CampaignSharesStats.Models.OrderCollection = BaseCollection.extend({

    initialize: function (options) {
        BaseCollection.prototype.initialize.call(this, options);
        this.listHeader = new BaseModel({
            currentFilter: options.currentFilter
        });
        this.currentFilter = options.currentFilter;
    },

    url: function () {
        return this.controller.getFilter().getUrl();
    },

    parse: function (response) {
        return response;
    }
});

CampaignSharesStats.Models.Page = Campaign.Models.BasePage.extend({
    createContentModel: function() {
        return new CampaignSharesStats.Models.Controller({
            campaignId: this.get('objectId'),
            profileId: this.get('profileModel').get('profileId'),
            subsection: 'shares-stats',
            affiliateProgramEnabled: false,
            profileModel: this.get('profileModel')
        });
    },
    pageData: function() {
        var campaignName = this.getCampaignModel().get('name');
        return {
            title: _.template('Вознаграждения проекта «<%=campaignName%>» | Planeta', {
                campaignName: campaignName
            })
        };
    }
});

Biblio.Views.BinItem = BaseView.extend({
    template: "#basket-item",
    className: "biblio-books-basket_i",
    events: {
        'click .biblio-books-basket_del': 'del'
    },
    del: function() {
        Biblio.data.bin.changeBooks(this.model.get('book'), 0);
    }
});

Biblio.Views.LibraryBinItem = BaseView.extend({
    template: "#library-basket-item",
    className: "biblio-library-basket_i",
    events: {
        'click .biblio-library-basket_del': 'del'
    },
    del: function() {
        Biblio.data.bin.delLibrary(this.model);
        Biblio.data.bin.save();
    }
});

Biblio.Views.LibraryBinList = DefaultListView.extend({
    template: "#library-basket-list",
    itemViewType: Biblio.Views.LibraryBinItem
});

Biblio.Views.LibraryBinListMore = BaseView.extend({
    template: "#library-basket-list-more",
    events: {
        'click .biblio-library-basket_more-link': 'showMore'
    },

    showMore: function() {
        this.model.set({'librariesListOpened':true});
        $('.js-library-list').addClass('collapsed');
    }
});

Biblio.Views.LibraryBin = BaseView.extend({
    template: "#library-bin",
    className: "biblio-library-basket",

    construct: function() {
        var self = this;

        var libraries = _.toArray(self.model.get('libraries'));

        var isListOpened = !self.model.get('librariesListOpened') && self.model.get('librariesCount') > 3;
        if (isListOpened) {
            libraries = libraries.slice(0,3);
        }

        self.addChildAtElement('.js-library-list', new Biblio.Views.LibraryBinList({
            collection: new BaseCollection(libraries)
        }));

        if (isListOpened) {
            self.addChildAtElement('.js-library-list-more', new Biblio.Views.LibraryBinListMore({
                model: self.model
            }));
        }
    }
});

Biblio.Views.LibraryTotalBin = BaseView.extend({
    template: "#biblio-wizard_library-total",
    el: ".biblio-wizard_library-total",
    beforeRender: function () {
        var self = this;
        self.clear();

        if(_.size(self.model.get('libraries')) > 0) {
            self.addChildAtElement('.js-library-bin', new Biblio.Views.LibraryBin({
                model: self.model
            }));
        }
    }
});

Biblio.Views.Bin = BaseView.extend({
    template: "#biblio-wizard_total",
    el: ".biblio-wizard_total",
    beforeRender: function () {
        var self = this;
        self.clear();

        _.each(self.model.get('books'), function (model) {
            self.addChildAtElement('.biblio-books-basket_list', new Biblio.Views.BinItem({
                model: model
            }));
        });
    },
    afterRender: function () {
        var self = this;
        if ( window.location.pathname.indexOf('books') !== -1 && _.size(self.model.get('books')) > 0 ) {
            fixedBtn.init('.biblio-wizard_fix-wrap', {type: 'bottom'});
        } else {
            $('.biblio-wizard_fix-wrap').removeClass('fixed').addClass('unfixed').css('height', '');
            $(window).unbind('scroll.fixedBtn resize.fixedBtn');
        }
    }
});

var Products = {};
Products.Models = {};

Products.Models.ProductCollection = BaseCollection.extend({

    url: function () {
        return this.controller.getFilter().getURL();
    },

    initialize: function (models, options) {
        BaseCollection.prototype.initialize.call(this, this.parse(models), options);
        this.offset = models ? models.length : 0;
        this.allLoaded = (_.size(models) < this.limit) || (this.limit == 0);
    },

    fetch: function (options) {
        options = (options || {});
        var self = this;
        self.trigger('beforeLoading');
        var successCallback = options.complete;
        options.complete = function (collection, response) {
            self.trigger('afterLoading');
            if (successCallback) {
                successCallback(collection, response);
            }
        };
        BaseCollection.prototype.fetch.call(this, options);
    },

    parse: function (response) {
        return _.map(response, function (product) {
            return _.extend(product, {
                productUrl: ShopUtils.getProductUrl(product.productId),
                isMeta: _.isEqual(product.productState, 'META')
            });
        });
    }

});

Products.Models.Referrers = BaseCollection.extend({
    url: function() {
        return '/api/util/referrer-list.json?mnemonicName=' + this.mnemonicName;
    },
    limit : 1000
});

Products.Models.InfoDialog = BaseModel.extend({

    url: function () {
        return  '/product.json?productId=' + this.get('productId');
    },

    parse: function (response) {
        if (response) {
            this.initSelectedProduct(response);
        }
        return response;
    },

    initSelectedProduct: function (parentProduct) {
        var selectedProduct;
        if (this.get('isMeta') && !_.isEmpty(parentProduct.childrenProducts)) {

            _.each(parentProduct.childrenProducts, function (childProduct) {
                childProduct.isEnabled = (childProduct.readyForPurchaseQuantity > 0);
                if (_.isEmpty(selectedProduct) && childProduct.isEnabled) {
                    selectedProduct = childProduct;
                }
            });
        }
        this.set('selectedProduct', _.isEmpty(selectedProduct) ? parentProduct : selectedProduct);
        selectedProduct = this.get("selectedProduct");
        this.set("totalPrice", selectedProduct.price);
        this.set("productAvailable", selectedProduct.readyForPurchaseQuantity > 0, {silent: true});
    },

    addToShoppingCart: function () {
        workspace.shoppingCart.addProduct(this.get('selectedProduct').productId, this.get("productCount"));
    },

    changeActiveProduct: function (productId, options) {
        var activeProduct = _.find(this.get('childrenProducts'), function (product) {
            return product.productId == productId;
        });
        if (activeProduct && activeProduct.isEnabled) {
            this.set({selectedProduct: activeProduct}, options);
            if (activeProduct.readyForPurchaseQuantity > 0) {
                this.setCount(1);
                this.set("productAvailable", true);
            } else {
                this.set("productAvailable", false);
            }
        }
    },

    addCount: function (val) {
        return this.setCount(this.get("productCount") + val);
    },

    setCount: function (val) {
        if (!/\d/g.test(val)) {
            val = 1;
        }
        val = Math.max(1, val);
        var nextVal = {};
        if (val == 1) {
            nextVal.productCount = 1;
            nextVal.minusButton = "disabled";
        } else {
            nextVal.minusButton = "";
        }
        var product = this.get("selectedProduct");
        if (val < product["readyForPurchaseQuantity"]) {
            nextVal.productCount = val;
            nextVal.plusButton = val >= product["readyForPurchaseQuantity"] ? "disabled" : "";
        } else {
            nextVal.productCount = product["readyForPurchaseQuantity"];
            nextVal.plusButton = "disabled";
        }

        this.set(nextVal);
        this.set("totalPrice", nextVal.productCount * this.get("price"));
    }
});
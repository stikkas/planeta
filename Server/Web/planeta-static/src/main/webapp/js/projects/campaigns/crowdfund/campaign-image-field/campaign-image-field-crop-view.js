/*globals CrowdFund, Modal, loadModule*/
CrowdFund.Views.ImageField.CropView = BaseView.extend({
    template: '#campaign-crop-image-field-template',

    onCropClick: function () {
        var view = new CrowdFund.Views.ImageField.ModalCropView({
            model: this.model
        });
        console.log(this.model);
        view.render();
    },

    onRemoveClick: function () {
        this._deletePhoto(this.model.get("loadedImage"));
        this._deletePhoto(this.model.get("croppedImage"));

        this.model.set({
            imageUrl: null,
            imageId: 0,
            loadedImage: null,
            croppedImage: null,
            cropSize: null,
            isImageDeleted: true
        });
    },
    _deletePhoto: CrowdFund.Views.ImageField.prototype._deletePhoto

//    dispose: function () {
//        this._deletePhoto(this.model.get('originalImage'));
//        BaseView.prototype.dispose.apply(this, arguments);
//    }
});

CrowdFund.Views.ImageField.ModalCropView = Modal.OverlappedView.extend({
    template: '#campaign-modal-crop-image-template',
    className: "modal modal-photo-crop",
    initialize: function () {
        Modal.OverlappedView.prototype.initialize.apply(this, arguments);
        if (this.model.get('loadedImage')) {
            this.model.set({
                imageUrl: this.model.get('loadedImage').imageUrl,
                imageId: this.model.get('loadedImage').photoId
            }, {silent: true});
        }
    },

    afterRender: function () {
        $('.modal-dialog').addClass('modal-lg');
        var self = this;
        var $img = this.$('img');

        var originalImage = new Image();
        originalImage.onload = function () {
            var coef = originalImage.width / $img.width();
            var cropSize = self.model.get('cropSize');
            var select;
            var aspectRatio = self.model.get('aspectRatio');
            self.coef = coef;
            if (cropSize) {
                select = [Math.round(cropSize.x / coef), Math.round(cropSize.y / coef), Math.round((cropSize.x + cropSize.w) / coef), Math.round((cropSize.y + cropSize.h) / coef)];
            } else {
                if (aspectRatio) {
                    if (aspectRatio && $img.width() / $img.height() > aspectRatio) {
                        select = [0, 0, Math.round($img.height() * aspectRatio), $img.height()];
                    } else {
                        select = [0, 0, $img.width(), Math.round($img.width() / aspectRatio)];
                    }
                } else {
                    select = [0, 0, $img.width(), $img.height()];
                }
            }
            loadModule('jcrop').done(function () {
                $img.Jcrop({
                    setSelect: select,
                    aspectRatio: aspectRatio,
                    onSelect: function (cropSize) {
                        self.cropSize = cropSize;
                    }
                }, function () {
                    self.jcrop = this;
                });
            });

        };
        originalImage.src = $img.attr('src');
    },

    save: function () {
        if (this.isSaving) {
            return;
        }
        this.isSaving = true;
        this.$('button').prop('disabled', true);
        var self = this;

        var cropSize = this.cropSize;
        if (!cropSize) {
            this.jcrop.destroy();
            this.cancel();
            return;
        }

        cropSize.x = Math.round(cropSize.x * this.coef);
        cropSize.y = Math.round(cropSize.y * this.coef);
        cropSize.w = Math.round(cropSize.w * this.coef);
        cropSize.h = Math.round(cropSize.h * this.coef);
        var options = {
            url: '/api/profile/crop-image.html',
            data: {
                clientId: this.model.get('clientId'),
                ownerId: this.model.get('profileId'),
                albumTypeId: this.model.get('albumTypeId'),
                url: ImageUtils.getThumbnailUrl(this.model.get('imageUrl'), ImageUtils.REAL, ImageType.PHOTO),
                cropX: cropSize.x,
                cropY: cropSize.y,
                cropWidth: cropSize.w,
                cropHeight: cropSize.h
            }
        };
        Backbone.sync('read', this, options).done(function (result) {
            self.isSaving = false;
            $('button').prop('disabled', false);
            if (result && result.success) {
                self._deletePhoto(self.model.get("croppedImage"));

                var newData = {
                    imageId: result.result.photoId,
                    imageUrl: result.result.imageUrl,
                    croppedImage: result.result,
                    cropSize: cropSize
                };

                var coverUrl = self.model.get("coverImageUrl");
                if (coverUrl == options.data.url){
                    newData.coverImageUrl = result.result.imageUrl;
                    callbackUpdateCover = self.model.get('callbackUpdateCover');
                    if(callbackUpdateCover) {
                        callbackUpdateCover(newData.coverImageUrl);
                    }
                }
                self.model.set(newData);

                if (self.onSuccess && _.isFunction(self.onSuccess)) {
                    self.onSuccess(self.model);
                }

                self.jcrop.destroy();
                self.dispose();
            } else {
                workspace.appView.showErrorMessage('Непредвиденная ошибка');
            }
        }).fail(function () {
            self.isSaving = false;
            $('button').prop('disabled', false);
            workspace.appView.showErrorMessage('Непредвиденная ошибка');
        });
    },

    _deletePhoto: CrowdFund.Views.ImageField.prototype._deletePhoto
});


CrowdFund.Views.ImageField.ModalCropBgView = Modal.OverlappedView.extend({
    template: '#campaign-modal-crop-bg-template',
    className: "modal modal-bg-crop",

    events: {
        'click a.close': 'cancel',
        'click .cancel': 'cancel',
        'click button[type=reset]': 'cancel',
        'click button[type=submit]': 'save',
        'click .js-bg-crop-del': 'deleteBg'
        // 'click .bg-crop_link': 'crop'
    },

    afterRender: function () {
        this.$('.image-cropper').cropit({
            imageState: {src: this.model.get('backgroundImageUrl')},
            allowCrossOrigin: true
        });
    },

    deleteBg: function (e) {
        e.preventDefault();
        this.model.unset('backgroundImageUrl');
        this.$('.image-cropper').cropit({
            imageState: {src: this.model.get('backgroundImageUrl')},
            allowCrossOrigin: true
        });
    },

    save: function () {
        if (this.isSaving) {
            return;
        }
        this.isSaving = true;
        this.$('button').prop('disabled', true);
        var self = this;
        try {
            var src = this.$('.image-cropper').cropit('export', {
                type: 'image/jpeg',
                quality: 1,
                originalSize: true,
                allowCrossOrigin: true
            });
            self.model.set({backgroundImageUrl: src});
            this.saveCampaignBgCroped(src, self.model.get('campaignId'));
        } catch (e) {
            this.isSaving = false;
            self.$('button').prop('disabled', false);
            workspace.appView.showErrorMessage('Непредвиденная ошибка');
        }
    },

    saveCampaignBgCroped: function (src, campaignId) {
        var options = {
            url: '/api/change-campaign-bg-image.json',
            data: {
                src: src,
                campaignId: campaignId
            }
        };

        var self = this;

        function succsessCallback(result) {
            self.isSaving = false;
            self.$('button').prop('disabled', false);
            if (result && result.success) {
                self.dispose();
                var newUrl = result.result.backgroundImageUrl;
                self.model.set({backgroundImageUrl: newUrl});
                workspace.appView.changeBackground(self.model);
            } else {
                workspace.appView.showErrorMessage('Превышен размер картинки');
            }
        }

        Backbone.sync('update', this, options).done(function (result) {
            succsessCallback(result);
        }).fail(function () {
            succsessCallback(null);
        });
    }
});




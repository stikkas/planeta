/*globals Form, CrowdFund, moduleLoader*/
CrowdFund.Views.CampaignContactsEdit.Email = BaseView.extend({
    className: 'project-create_row js-edit-email',
    template: '#campaign-contacts-edit-email-template',


    initialize: function () {
        BaseView.prototype.initialize.apply(this, arguments);
        moduleLoader.loadModule('planeta-select2');
    },

    afterRender: function () {
        var self = this;
        moduleLoader.loadModule('planeta-select2').done(function () {

            if (self.model.get('contactList') && self.model.get('possibleContactList')) {
                var parse = function (param) {
                    return _.compact(_.pluck(param, 'email'));
                };

                var contactList = parse(self.model.get('contactList'));
                var possibleContactList = parse(self.model.get('possibleContactList'));

                _.each(contactList, function (contact, idx) {
                    var foundPossibleContact = _.find(possibleContactList, function (possibleContact) {
                        return possibleContact.toLowerCase() === contact.toLowerCase();
                    });

                    if (foundPossibleContact) {
                        contactList[idx] = foundPossibleContact;
                    } else {
                        possibleContactList.push(contact);
                    }
                });
                self.$(".js-select2").val(contactList.join(',')).select2({
                    tags: possibleContactList

                });
                self.$(".js-select2").on("select2-selecting", function (e) {
                    if (!Form.isValidEmail(e.object.text)) {
                        workspace.appView.showErrorMessage('Вы указали неправильный email');
                        e.preventDefault();
                    }
                });
            }
        });
    }
});
if (!CrowdFund.Models.CampaignContactsEdit) {
    CrowdFund.Models.CampaignContactsEdit = {};
}
CrowdFund.Models.CampaignContactsEdit.Email = BaseModel.extend({
    initialize: function () {
        var self = this;
        BaseModel.prototype.initialize.apply(this, arguments);
        var data = {data: { campaignId: this.get("campaignId") ? this.get("campaignId") : this.get("objectId") }};


        Backbone.sync('get', {url: '/api/profile/campaign/contacts-list.json'}, data).done(function (result) {
            self.set('contactList', result);
        });
        Backbone.sync('get', {url: '/api/profile/campaign/possible-contacts-list.json'}, data).done(function (result) {
            self.set('possibleContactList', result);
        });

    }
});
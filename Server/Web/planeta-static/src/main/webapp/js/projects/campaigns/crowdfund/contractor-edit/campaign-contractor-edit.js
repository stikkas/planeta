/*globals CrowdFund,Modal,PrivacyUtils*/
CrowdFund.Views.CampaignContractorEdit = BaseView.extend({
    template: "#campaign-contractor-edit-template",
    isExistsPlanetaUiElement: true,
    
    events: {
        'change .js-country_select, .js-type_select': 'renderFunc',
        'blur .js-save-model': 'renderFuncSilent',
        'click .upload-file': 'showUploadDialog',
        'click .js-agree-personal-data-processing': 'toggleAgreePersonalDataProcessing'
    },

    modelEvents: _.extend({}, BaseView.prototype.modelEvents, {
        'contractorSaveEvent': 'save'
    }),

    toggleAgreePersonalDataProcessing: function () {
        if ($('input[name=agreePersonalDataProcessing]').attr('checked')) {
            $('[name=agreePersonalDataProcessing]').removeAttr('checked');
            $('.js-agree-personal-data-processing').removeClass('active');
        } else {
            $('[name=agreePersonalDataProcessing]').attr('checked', 'true');
            $('.js-agree-personal-data-processing').addClass('active');
        }
    },

    contractorTypeList: {
        "INDIVIDUAL": "Физическое лицо",
        "INDIVIDUAL_ENTREPRENEUR": "ИП",
        "OOO": "ООО",
        "CHARITABLE_FOUNDATION": "Некоммерческая организация",
        "LEGAL_ENTITY": "Другое юридическое лицо"
    },

    contractorPositionList: {
        "NOT_SET": "Не выбрано",
        "OTHER": "Другая",
        "GEN_DIRECTOR": "Генеральный директор",
        "DIRECTOR": "Директор",
        "PREDSEDATEL": "Председатель",
        "PRESIDENT": "Президент"
    },

    showUploadDialog: function () {
        var self = this;
        var profileId = this.model.campaign.get('profileId');
        UploadController.showUploadContractorFiles(profileId, function () {
            self.reloadLastUploadedFiles();
        });
    },

    modelJSON: function () {
        var result = BaseView.prototype.modelJSON.apply(this, arguments);
        result.contractorTypeList = this.contractorTypeList;
        result.contractorPositionList = this.contractorPositionList;
        return result;
    },

    initialize: function (options) {
        if(workspace) {
            if(workspace.currentLanguage) {
                this.model.set('lang', workspace.currentLanguage);
            }
        }

        BaseView.prototype.initialize.apply(this, arguments);
        if (options && options.campaign) {
            this.model.campaign = options.campaign;
        }
        this.model.set('campaign', this.model.campaign, {silent: true});
        this.initLastUploadedFiles();
    },
    initLastUploadedFiles: function () {
        this.lastUploadedFiles = new CrowdFund.Models.ContractorLastUploadedFileListColection([], {
            data: {
                profileId: this.model.attributes.campaign.get('profileId')
            },
            limit: 100
        });
        this.lastLoadedFilewsView = this.addChildAtElement('#scanFilesList', new CrowdFund.Views.ContractorLastUploadedFileList({
            collection: this.lastUploadedFiles,
            itemViewType: CrowdFund.Views.ContractorFile
        }));
        $("#scanFilesList").empty();
        this.lastUploadedFiles.load();
    },
    reloadLastUploadedFiles: function () {
        if (this.lastUploadedFiles) {
            this.lastUploadedFiles.load();
        }
    },
    createContractorChoose: function(contractors) {
        this.contractorCollection = contractors;
        this.contractorListModel = new CrowdFund.Models.ContractorList({
            contractorCollection: contractors,
            contractorId: this.model.get('contractorId')
        });
        this.contractorListModel.bind('change:selectedIndex', this.onContractorChanged, this);
        this.contractorListView = new CrowdFund.Views.ContractorList({model: this.contractorListModel});
        this.addChildAtElement('.js-contractor-list', this.contractorListView);
    },
    renderFunc: function (event) {
        var el = event.target;
        this.model.set(el.name, el.value);
    },
    renderFuncSilent: function (event) {
        var el = event.target;
        var obj = {};
        obj[el.name] = el.value;
        this.model.set(obj, {silent: true});
    },
    onContractorChanged: function () {
        var selectedIndex = this.contractorListModel.get('selectedIndex');
        var contractor = this.contractorCollection[selectedIndex];
        var isNewContractor = !(contractor && contractor.contractorId);
        var isCharity = !!this.model.campaign.get('tags').find(function(obj){return obj.mnemonicName === 'CHARITY';});
        if (isNewContractor) {
            if (this.contractorCollection.length > 0) {
                this.model.set(_.clone(this.contractorCollection[0]), {unset: true, silent: true});
                this.model.set(this.model.defaults, {silent: true});
            }
        } else {
            this.model.set(this.contractorCollection[selectedIndex], {silent: true});
        }
        if (isCharity) {
            this.model.set({type: "CHARITABLE_FOUNDATION"}, {silent: true});
        }
        this.model.set({isCharity: isCharity}, {silent: true});
        this.model.set({
            isNewContractor: isNewContractor
        });
    },

    extendContractor: function (contractor) {
        return _.extend(contractor || {}, {
            campaignId: this.model.get("campaignId"),
            profileId: this.model.campaign.get("creatorProfileId")
        });
    },

    save: function (e, saveOnly, footer, validationUrl, isSchoolWay) {
        var $el = this.$('input[name=agreePersonalDataProcessing]');
        var isAgreePersonalDataProcessing = $el.attr('checked') ? true : false;
        $('.js-agree-error').css('display', 'none');
        $('.js-agree-error').closest('.project-create_row').removeClass('error');
        if (!isAgreePersonalDataProcessing) {
            workspace.appView.showErrorMessage('Требуется согласие на обработку персональных данных');
            $('.js-agree-error').css('display', 'block');
            $('.js-agree-error').closest('.project-create_row').addClass('error');
            if (footer && footer._fail) {
                footer._fail();
            }

            return;
        }

        var self = this,
            data = this.extendContractor({
                contractorId: this.model.get("contractorId")
            }),
            bindOrChangeUrl = validationUrl ? validationUrl : '/admin/contractor-add-and-change-campaign-status.json';

        _.extend(data, this.$('form').serializeObject());

        if (this.model.get('isNewContractor')) {
            var contractor = this.contractorListModel.getContractorBy('inn', data.inn);
            if (contractor) {
                this.showContractorExistsDialog(contractor);
                return;
            }
        }

        var issueDate = this.$('input[data-field-name=issueDate]').attr("data-field-value");
        if (issueDate) {
            data.issueDate = DateUtils.addHours(DateUtils.middleOfDate(issueDate), -DateUtils.getHoursDiff());
        }

        var birthDate = this.$('input[data-field-name=birthDate]').attr("data-field-value");
        if (birthDate) {
            data.birthDate = DateUtils.addHours(DateUtils.middleOfDate(birthDate), -DateUtils.getHoursDiff());
        }

        this.model.set(data);

        return Backbone.sync("update", null, {
            data: data,
            url: bindOrChangeUrl
        }).done(function (result) {
            if (result && result.success) {
                self.model.set({contractorId: result.result.contractorId}, {silent: true});
                self.model.set({inn: result.result.inn});
                if(self.model.parentModel) {
                    if (self.model.parentModel.get('status') === 'DRAFT' && saveOnly) {
                        self.model.parentModel.saveCampaign(saveOnly, footer).done(function(response) {
                            if (response && response.success && isSchoolWay) {
                                if (footer && footer.nextStep) {
                                    $.post('/admin/campaign-save-counterparty-step.json').done(function (response) {
                                        if (response && response.success) {
                                            footer.nextStep();
                                        } else {
                                            workspace.appView.showErrorMessage("Непредвиденная ошибка", 3000);
                                        }
                                    });
                                }
                            }
                            if (!(response && response.success)) {
                                footer._fail();
                                if(!response.errorClass) {
                                    workspace.appView.showErrorMessage(response.errorMessage);
                                    footer.model.navigateToValidation(response.errorMessage, response.fieldErrors, response.errorClass);
                                }
                            }
                        }).fail(function () {
                            workspace.appView.showErrorMessage('Ошибка соединениия');
                        });
                    } else {
                        self.model.parentModel.saveAndValidateCampaign(saveOnly, footer).fail(function () {
                            workspace.appView.showErrorMessage('Ошибка соединениия');
                        });
                    }
                }
            } else {
                if (footer && footer._saved) {
                    footer._saved(false);
                }

                if (result.errorMessage === 'contractor exists') {
                    self.showContractorExistsDialog(result.result, true);
                    return;
                }

                workspace.appView.showErrorMessage(result.errorMessage);
                self.$el.find('.error').removeClass('error');
                self.$el.find('.js-error').empty();
                self.$('.tooltip').show();
                _.each(result.fieldErrors, function (error, fieldName) {
                    var $el = self.$('[name="' + fieldName + '"]').closest('.project-create_row');
                    if ($el.length) {
                        $el.addClass('error');
                        $el.find('.tooltip').hide();
                        $el.find(".js-error").append($('<div class="pc_row_warning tooltip" data-tooltip="' + error + '"><div class="s-pc-action-warning"></div></div>'));
                    }

                });
            }
        }).fail(function () {
            if (footer && footer._saved) {
                footer._saved(false);
            }
            workspace.appView.showErrorMessage('При сохранении произошла ошибка');
        });
    },

    showSuccessAndClose: function (result) {
        workspace.appView.showSuccessMessage('Изменения сохранены');
        this.model.campaign.set("status", "NOT_STARTED");
        this.model.set(result);
    },

    showContractorExistsDialog: function (contractor, isServerError) {
        var self = this;
        if (!contractor) {
            Modal.showAlert('Невозможно сохранить запись. Проверьте верность ввода ИНН или обратитесь в службу поддержки.', 'Внимание!');
        } else {
            Modal.showConfirmEx({
                title: 'Подтверждение действия',
                text: 'Запись с таким ИНН уже существует<br> Название: ' + contractor.name + '<br> Тип: ' + self.contractorTypeList[contractor.type] + '<br> Страна: ' + contractor.countryNameRus + '<br><br> Использовать эти реквизиты?',
                okButtonText: 'Использовать',
                cancelButtonText: 'Продолжить редактирование',
                success: function () {
                    if (isServerError) {
                        Backbone.sync("update", null, {
                            data: self.extendContractor(contractor),
                            url: '/admin/contractor-bind-and-change-campaign-status.json'
                        }).done(function (response) {
                            if (response && response.success) {
                                self.showSuccessAndClose(response.result);
                            } else {
                                workspace.appView.showErrorMessage('При сохранении произошла ошибка');
                            }
                        }).fail(function () {
                            workspace.appView.showErrorMessage('При сохранении произошла ошибка');
                        });

                    } else {
                        self.contractorListView.selectContractor(contractor.contractorId);
                    }
                }
            });
        }

    }
});

CrowdFund.Views.CampaignContractorEdit.open = function (model, campaign) {
    if (PrivacyUtils.hasAdministrativeRole()) {
        Backbone.sync('read', null, {
            url: "/admin/contractor-list-by-profile.json",
            data: {
                profileId: workspace.appModel.get("profileModel").get("profileId"),
                offset: 0,
                limit: 25
            }
        }).done(function (response) {
            if (response && response.success) {
                var view = new CrowdFund.Views.CampaignContractorEdit({
                    model: model,
                    campaign: campaign,
                    contractorCollection: response.result
                });
                view.render();
            }
        });
    }
};

CrowdFund.Views.ContractorList = BaseView.extend({
    template: "#campaign-contractor-list-template",
    isExistsPlanetaUiElement: true,
    construct: function () {
        var contractorCollection = this.model.get('contractorCollection');
        if (contractorCollection.length) {
            var contractorId = this.model.get('contractorId');
            if (contractorId > 0) {
                this.selectContractor(contractorId);
            } else {
                this.model.set('selectedIndex', 0);
            }

        }
    },

    selectContractor: function (contractorId) {
        var self = this;
        var contractorCollection = this.model.get('contractorCollection');
        _.find(contractorCollection, function (item, idx) {
            if (item.contractorId == contractorId) {
                self.model.set('selectedIndex', idx);
            }
        });
    },

    onContractorChanged: function (e) {
        this.model.set('selectedIndex', +$(e.target).val());
    }
});

CrowdFund.Models.ContractorList = BaseModel.extend({
    getContractorBy: function (fiendName, fieldValue) {
        if (fieldValue) {
            return _.find(this.get('contractorCollection'), function (contractor) {
                return (contractor[fiendName] === fieldValue);
            });
        }
    }
});

CrowdFund.Views.ContractorLastUploadedFileList = BaseListView.extend({
    itemViewType: CrowdFund.Views.ContractorFile,
    className: "uploader-filelist"
});

CrowdFund.Views.ContractorFile = BaseView.extend({
    template: '#campaign-contractor-file-template',
    tagName: "li",
    className: "filelist-item",
    events: {
        'click .js-remove-file' : 'removeFile'
    },

    removeFile: function (e) {
        e.preventDefault();
        var self = this;
        Modal.showConfirm('Вы действительно хотите удалить этот документ? Он может уже использоваться для других контрагентов.', 'Удаление документа', {
            success: function() {
                $.post("/admin/deleteByProfileId-uploaded-profile-file.json",{
                    profileId: self.model.get('profileId'),
                    fileId: self.model.get('fileId')
                }, function (response) {
                    if (response.success) {
                        self.parent.collection.load();
                    } else {
                        workspace.appView.showErrorMessage(response.errorMessage);
                    }
                });
            }
        });
    }
});

CrowdFund.Models.ContractorLastUploadedFileListColection = BaseCollection.extend({
    url: '/admin/last-uploaded-files.json',
    limit: 100
});
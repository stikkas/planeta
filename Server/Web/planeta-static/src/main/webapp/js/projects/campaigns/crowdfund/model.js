/*globals Comments, Campaign, Subscription, JobManager, Likes, HoverInfoCard, ProfileInfoHover, Blog, ImageType,
 ImageUtils,_,BaseCollection,$,workspace,BaseModel,PrivacyUtils, ProfileUtils, DonateUtils, News*/
var CrowdFund = {};
CrowdFund.Models = {};
CrowdFund.Models.Campaign = Campaign.Models.BaseCampaign.extend({

    defaults: _.extend({}, Campaign.Models.BaseCampaign.prototype.defaults, {
        previewMode: false,
        contactsCount: 0,
        videoInfo: '',
        showShareHover: false
    }),

    fetch: function (options) {
        var self = this;
        if (workspace.isAuthorized) {
            this.subscriptions.fetch();
        }

        if (this.get('canChangeCampaign')) {
            this.contractor.fetch();
        }
        var success = options.success;
        options.success = function (modelCame, res) {
            workspace.appView.changeBackground(self);
            self.loadVideoInfo();
            if (window.gtm) {
                window.gtm.trackViewCampaignPage(self);
            }
            self.initShares();
            self.subscriptions.set('isActive', self.get('status') === 'ACTIVE');
            self.extendShares(self.shares);
            if (success && _.isFunction(success)) {
                success(modelCame, res);
            }
        };
        options = _.extend(options, {
            data: {
                profileId: this.get('profileId'),
                objectId: this.get('campaignId')
            }
        });
        return Campaign.Models.BaseCampaign.prototype.fetch.call(this, options);
    },

    isValidDonateAmount: function () {
        return this.get('selectedShare').isEnoughAmount(this.get('donateAmount'));
    },

    initialize: function (options) {
        Campaign.Models.BaseCampaign.prototype.initialize.call(this, options);

        this.subscriptions = new Subscription.Models.CampaignSubscription({
            profileId: this.get('profileId')
        });

        this.contractor = new CrowdFund.Models.ContractorAlert({
            campaignId: this.get('campaignId')
        });

        this.initShares();
    },

    initShares: function () {
        if (!(this.shares instanceof CrowdFund.Models.ShareCollection)) {
            this.shares = new CrowdFund.Models.ShareCollection(this.get('shares'), {
                controller: this
            });
            this.shares.on('change:quantity', this.updateDonateAmount, this);
        }
    },

    parse: function (response) {
        response = Campaign.Models.BaseCampaign.prototype.parse.call(this, response);
        if (response.success === false) {
            return {success: false};
        }
        if (this.shares) {
            var self = this;
            _(response.shares).each(function (share) {
//                share.campaign = self;
                var iShare = self.shares.find(function (shareModel) {
                    return (shareModel.id == share.shareId);
                });

                if (iShare) {
                    share.quantity = iShare.get('quantity');
                    share.reply = iShare.get('reply');
                }
            });
            this.shares.reset(response.shares);
            this.set({
                sharesCount: response.shares.length
            }, {silent: true});
        }
        return response;
    },

    loadVideoInfo: function () {
        var self = this;
        if (this.get('videoId')) {
            $.ajax({
                url: '/api/campaign/campaign-video-info.json',
                data: {
                    videoProfileId: this.get('videoProfileId'),
                    videoId: self.get('videoId')
                },
                success: function (response) {
                    self.set({
                        videoInfo: response
                    });
                }
            });
        }
    },

    loadContacts: function () {
        var options = {
            url: '/campaign/contacts-count.json',
            data: {
                campaignId: this.get('campaignId')
            },
            context: this,
            success: function (responce) {
                this.set({contactsCount: responce});
            }
        };
        return Backbone.sync('read', null, options);
    }
});


CrowdFund.Models.Backers = BaseCollection.extend({
    url: '/api/campaign/backers.json',
    model: ProfileInfoHover.Models.Info
});

CrowdFund.Models.FaqQuestion = BaseModel.extend({

    defaults: {
        campaignFaqId: 0,
        question: '',
        answer: ''
    },

    updateFaqQuestion: function (options) {
        var faqCollection = this.collection;
        var syncOptions = {
            url: '/api/profile/campaign-faq-edit.json',
            contentType: 'application/json',
            success: function (response) {
                if (!response.success && options.error && _.isFunction(options.error)) {
                    options.error(response);
                    return;
                }
                faqCollection.reset();
                faqCollection.load();
                if (options && options.success && _.isFunction(options.success)) {
                    options.success(response);
                }
            }
        };
        Backbone.sync('update', this, syncOptions);
    },

    deleteFaqQuestion: function (options) {
        var faqCollection = this.collection;
        var model = this;
        var syncOptions = {
            url: '/api/profile/campaign-faq-deleteByProfileId.json',
            contentType: 'application/json',
            success: function (response) {
                faqCollection.remove(model);
                if (options && options.success && _.isFunction(options.success)) {
                    options.success(response);
                }
            }
        };
        Backbone.sync('delete', this, syncOptions);
    },

    validateModel: function (data) {
        var validation = {success: false, errors: {}};
        if (!data) {
            return validation;
        }
        if (!data.question) {
            validation.errors.questionError = "Пустой вопрос";
        } else if (data.question.length > 400) {
            validation.errors.questionError = "Не более 400 символов";
        }
        if (!data.answer) {
            validation.errors.answerError = "Пустой ответ";
        }
        if (_.isEmpty(validation.errors)) {
            validation.success = true;
        }
        return validation;
    }
});

CrowdFund.Models.Faq = BaseCollection.extend({
    model: CrowdFund.Models.FaqQuestion,
    url: '/api/public/campaign-faq-list.json'
});

CrowdFund.Models.RelatedCampaignsList = BaseCollection.extend({
    url: '/api/profile/related-campaigns.json'
});

CrowdFund.Models.CurrentPlanetaManager = BaseModel.extend({
    url: '/api/campaigns/campaign-get-current-manager.json',

    parse: function (response) {
        response = response || {};

        if (response.success) {
            return response.result || [];
        }
        return [];
    }
});

CrowdFund.Models.Page = Campaign.Models.BasePage.extend({

    initialize: function () {
        this.insertCampaignModel();
        this.insertBreadcrumbsData();
    },
    createCampaignModel: function () {
        var attrs = {
            profileId: this.get('profileModel').get('profileId'),
            campaignId: this.get('objectId'),
            creatorProfile: this.get('profileModel')
        };
        return new CrowdFund.Models.Campaign(attrs);
    },
    prefetch: function (options) {
        var success = options.success;
        var self = this;
        var model = this.getCampaignModel();
        options.success = function (result) {
            workspace.stats.trackStatEvent('CAMPAIGN_VIEW', model.get('objectId'));
            if (success) {
                success(result);
            }
            self.fetchPurchasedShareId(model);
        };
        return this.getCampaignModel().fetch(options);
    },

    fetchPurchasedShareId: function (model) {
        if (!model || !model.get('campaignId')) {
            console.log("No Campaign");
            return;
        }
        Backbone.sync('read', null, {
            url: "/api/campaign/campaign-purchased-share-ids.json",
            data: {
                campaignId: model.get('campaignId')
            }
        }).done(function (response) {
            if (response && response.success && response.result) {
                _.each(response.result, function (id) {
                    var m = model.shares.findByAttr('shareId', id);
                    if (m) {
                        m.set('isPurchased', true);
                    }
                });
            }
        });

    },

    pageData: function () {
        var model = this.getCampaignModel();
        var campaignName = model.get('name');
        var creatorProfile = workspace.appModel.get('profileModel');
        var displayName = creatorProfile.get('displayName');
        var viewImageUrl = model.get('viewImageUrl') || model.get('imageUrl');
        var imageUrl = ImageUtils.getThumbnailUrl(model.get('imageUrl'), ImageUtils.ORIGINAL, ImageType.PRODUCT);
        var title = _.template('<%=campaignName%> | Planeta', {
            campaignName: campaignName
        });

        var description = model.get('shortDescription');

        $('link[rel="canonical"]').remove();
        var result = {
            image: imageUrl,
            link: imageUrl,
            title: title,
            description: description

        };

        if (_.contains(["DRAFT", "PATCH", "NOT_STARTED", "APPROVED"], model.get("status"))) {
            result.robots = "noindex, nofollow";
        }

        if (model.get('campaignId')) {
            CampaignUtils.trackPixel(model.get('campaignId'));
        }

        return result;
    },

    additionalPageData: function () {
        var imageArray = [];
        var model = this.getCampaignModel();

        if (model.get('viewImageUrl')) {
            imageArray.push(ImageUtils.getThumbnailUrl(model.get('viewImageUrl'), ImageUtils.ORIGINAL, ImageType.PRODUCT));
        }
        var video = model.get('videoInfo');
        if (video) {
            imageArray.push(ImageUtils.getThumbnailUrl(video.imageUrl, ImageUtils.VIDEO_MIDDLE, ImageType.VIDEO));
        }
        return {
            imageArray: imageArray
        };
    }
});

/**
 *
 * @author Andrew.Arefyev@gmail.com
 * Date: 15.08.13
 * Time: 22:56
 */

CrowdFund.Models.Share = BaseModel.extend({

    defaults: {
        isSelected: false,
        quantity: 1
    },

    idAttribute: 'shareId',
    initialize: function () {
        var self = this;
        self.controller = self.get('controller') || this.collection.controller;
        var isCharity = self.controller.get('isCharity');
        var hasLimit = !_.isEqual(self.get('amount'), 0);
        self.set({
            hasLimit: hasLimit,
            isAllSold: hasLimit && self.get('purchaseCount') >= self.get('amount'),
            sharesLeft: Math.max(0, self.get('amount') - self.get('purchaseCount')),
            isDonate: _.isEqual(self.get('price'), 0),
            isCharity: _.isBoolean(isCharity) && isCharity
        }, {silent: true});
    },

    isEnoughQuantity: function (quantity) {
        var result = (this.get('isDonate') || !this.get('hasLimit')) && (quantity > 0);
        if (!result) {
            result = !this.get('isAllSold') && (quantity > 0) && (quantity <= this.get('sharesLeft'));
        }
        return result;
    },

    isEnoughAmount: function (amount) {
        var totalPrice = this.get('isDonate') ? 1 : this.getTotalPrice();
        return (amount >= totalPrice);
    },

    tryChangeQuantity: function (quantity) {
        var canChange = this.isEnoughQuantity(quantity);
        var totalPrice = quantity * this.get('price');
        return canChange && (totalPrice == this.parseDonateAmount(totalPrice));
    },
    parseDonateAmount: function (amount) {
        var maxAmount = 1000000000;
        var minAmount = 1;
        var parsed = parseFloat(amount);
        if (_.isNaN(parsed)) {
            return minAmount;
        }
        parsed = Math.abs(Math.round(parsed));
        return parsed > maxAmount ? maxAmount : parsed;
    },
    getTotalPrice: function () {
        return this.get('quantity') * this.get('price');
    },

    setChecked: function () {
        this.set({isSelected: true}, {silent: true});
    },
    setUnchecked: function () {
        this.set({isSelected: false}, {silent: true});
    }
});

CrowdFund.Models.ShareCollection = BaseCollection.extend({
    model: CrowdFund.Models.Share,

    initialize: function (models, options) {
        this.options = options || {};
        BaseCollection.prototype.initialize.apply(this, arguments);
        //that is the usage?
        this.dfd = $.Deferred();
        if (options.controller) {
            this.controller = options.controller;
        } else {
            console.error('"controller" in  CampaignDonate.Models.ShareCollection is required.');
        }
    },

    reset: function () {
        var donateShare;
        BaseCollection.prototype.reset.apply(this, arguments);
        donateShare = this.getDonateShare();
        if (donateShare) {
            donateShare.set('price', this.getShareMinPrice());
        }
    },

    getShareMinPrice: function () {
        return 100; // https://planeta.atlassian.net/browse/PLANETA-18087
    },

    getDonateShare: function () {
        return this.find(function (share) {
            return share.get('isDonate');
        });
    },

    getShareById: function (shareId) {
        return this.find(function (share) {
            return (share.id == shareId);
        });
    },

    getSelectedShare: function () {
        return this.find(function (share) {
            return share.get('isSelected');
        });
    },

    trySelect: function (share) {
        if (!share) {
            return false;
        }
        var canSelect = !share.get('isAllSold');
        if (canSelect) {
            var selectedShare = this.getSelectedShare();
            if (selectedShare) {
                selectedShare.setUnchecked();
            }
            share.setChecked();
            this.controller.set({
                selectedShare: share
            }, {silent: true});
        }

        return canSelect;
    }

});
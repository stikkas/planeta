/*globals CampaignEdit, TinyMcePlaneta, tinymce*/

CampaignEdit.Models.EditorShareDescriptionModel = function (campaignModel) {
    _.extend(this, new TinyMcePlaneta.EditorModel(campaignModel), {
        config: TinyMcePlaneta.getCampaignEditorShareDescriptionConfiguration(),
        width: 510,
        height: 300,
        saveToParentModel: function () {
            this.parentModel.set(_.extend({}, this.parentModel.attributes, {
                descriptionHtml: this.html(),
                description: this.textClean()
            }), {silent: true});
        }
    });
};



CampaignEdit.Views.ShareDescriptionEdit = BaseView.extend({
    className: 'ww-flat edit-blog1',
    template: '#campaign-share-edit-tiny-mce-template',
    editorState: null,
    editorId: '',

    beforeRender: function () {
        this.dispose();
    },

    afterRender: _.debounce(function () { // debounce – hack for double afterRender after "workspace.appView.views.contentView.innerView.render()"
        var tinyModel = this.model.editorShareDescriptionModel;
        var editorEl = this.$el.find('.tinymce-body');
        editorEl.height(300).width(510);
        this.editorId = 'tinymce-body' + new Date().valueOf();
        editorEl[0].id = this.editorId;
        tinyModel.init('#' + this.editorId);
        $('.edit-blog-body').fadeIn();

        var editorInitedTimer = setInterval(function () {
            if (!tinyModel.rendered || !tinymce.activeEditor) return;
            clearInterval(editorInitedTimer);
            try {
                $( tinymce.activeEditor.getDoc() ).on('click', function (e) {
                    if ( !$(e.target).hasClass('mceContentHtml') ) return;
                    tinymce.activeEditor.getBody().focus();
                });

                $(tinymce.activeEditor.getBody()).addClass('mceCampaignEditorBody');
            } catch (error) {
                console.log(error);
            }
        }, 100);

    }, 100),

    dispose: function () {
        this.model.editorShareDescriptionModel.destruct();
    },

    modelEvents: {
        destroy: 'dispose'
    }
});
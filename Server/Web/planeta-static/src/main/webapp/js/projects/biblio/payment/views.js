Biblio.Views.Payment = BaseView.extend({
    el: '.biblio-wizard_payment',
    template: '#biblio-wizard-payment',
    INVOICE_PAYMENT_ID: 12,
    LEGAL_ENTITY: 'LEGAL_ENTITY',
    INDIVIDUAL: 'INDIVIDUAL',
    events: {
        'click .pln-payment-face': 'changeUserType'
    },
    modelEvents: {
        destroy: 'dispose'
    },
    construct: function () {
        var self = this;
        self.fizView = self.addChildAtElement('.js-fiz-payment', new Biblio.Views.Fizic({
            model: self.model
        }), true);
        self.ulView = self.addChildAtElement('.js-ul-payment', new Biblio.Views.Uric({
            model: self.model
        }), true);
    },
    afterRender: function () {
        var self = this;
        var custType;
        if (window.location.pathname === '/payment') {
            self.fizView.$el.removeClass('hide');
            custType = this.INDIVIDUAL;
        } else {
            self.ulView.$el.removeClass('hide');
            self.$('.pln-payment-face').addClass('active');
            custType = this.LEGAL_ENTITY;
            self.model.silentSet('paymentMethodId', this.INVOICE_PAYMENT_ID);
        }
        self.model.silentSet('customerType', custType);
        self.showProperTexts();
    },
    showProperTexts: function () {
        // hello asavan
        if (this.model.get('customerType') == this.LEGAL_ENTITY) {
            $('.biblio-wizard_add-info').html('Нажимая «Получить счет», вы соглашаетесь с <a href="https://planeta.ru/welcome/projects-agreement.html" target="_blank">Условиями предоставления сервиса</a> и <a href="https://files.planeta.ru/biblio/sponsor_contract.pdf" target="_blank">Договором o&nbsp;спонсорских перечислениях</a>.');
            $('.biblio-wizard_btn >.btn-primary').html('Получить&nbsp;счет');
        } else {
            $('.biblio-wizard_add-info').html('Нажимая «Перейти к оплате», вы соглашаетесь с <a href="https://planeta.ru/welcome/projects-agreement.html" target="_blank">Условиями предоставления сервиса</a> и подтверждаете, что ознакомились и принимаете условия <a href="https://files.planeta.ru/biblio/public_offer.pdf"  target="_blank">Публичной&nbsp;оферты</a>.');
            $('.biblio-wizard_btn >.btn-primary').html('Перейти к&nbsp;оплате');
        }
        if (this.model.get('bookIds')) {
            var self = this;
            $.post('/api/public/exists-tag-in-books.json?bookIds[]=' + self.model.get('bookIds')).done(function (response) {
                if (response) {
                    if (self.model.get('customerType') == self.LEGAL_ENTITY) {
                        //fixme: воткнуть настоящий текст оферты для изданий от Почты России
                        $('.biblio-wizard_add-info').html('Нажимая «Получить счет», вы соглашаетесь с <a href="https://planeta.ru/welcome/projects-agreement.html" target="_blank">Условиями предоставления сервиса</a> и <a href="https://files.planeta.ru/biblio/sponsor_contract.pdf" target="_blank">Договором o&nbsp;спонсорских перечислениях</a>.');
                        $('.biblio-wizard_btn >.btn-primary').html('Получить&nbsp;счет');
                    } else {
                        //fixme: воткнуть настоящий текст оферты для изданий от Почты России
                        $('.biblio-wizard_add-info').html('Нажимая «Перейти к оплате», вы соглашаетесь с <a href="https://planeta.ru/welcome/projects-agreement.html" target="_blank">Условиями предоставления сервиса</a> и подтверждаете, что ознакомились и принимаете условия <a href="https://files.planeta.ru/biblio/public_offer.pdf"  target="_blank">Публичной&nbsp;оферты</a>.');
                        $('.biblio-wizard_btn >.btn-primary').html('Перейти к&nbsp;оплате');
                    }
                }
            })
        }
    },
    changeUserType: function cut(e) {
        var custType, methodId;
        if (window.location.pathname === '/payment') {
            $(e.currentTarget).addClass('active');
            workspace.changeUrl('/payment-ul');
            this.ulView.$el.removeClass('hide');
            this.fizView.$el.addClass('hide');
            methodId = this.INVOICE_PAYMENT_ID;
            custType = this.LEGAL_ENTITY;
            cut.methodId = this.model.get('paymentMethodId');
        } else {
            $(e.currentTarget).removeClass('active');
            workspace.changeUrl('/payment');
            this.fizView.$el.removeClass('hide');
            this.ulView.$el.addClass('hide');
            custType = this.INDIVIDUAL;
            methodId = cut.methodId;
        }
        this.model.silentSet({customerType: custType, paymentMethodId: methodId});
        this.showProperTexts();
    },
    setupCheckEmail: function () {
        var self = this;
        if (self.model.get('hasEmail')) {
            return;
        }
        this.fizView.setupCheckEmail();
        this.ulView.setupCheckEmail();
    },
    showErrors: function (response) {
        if (window.location.pathname === '/payment') {
            this.fizView.showErrorsEx(this.fizView.$el, response, '.biblio-form_field');
        } else {
            console.log(response);
            this.ulView.showErrorsEx(this.ulView.$el, response, '.pln-payment-box_field-row');
        }
    }
});
Biblio.Views.Mixin = BaseView.extend({
    modelEvents: {
        destroy: 'dispose'
    },
    setupCheckEmail: function () {
        var self = this;
        var emailBlock = self.$('.js-anchr-email-block');
        var $emailInput = self.$('.js-email');
        var afterCheckCallback = function (response) {
            var status = response && response.result;
            self.model.set({
                isEmailValid: status != 'ALREADY_EXIST' && status != 'NOT_VALID'
            }, {silent: true});
        };
        $emailInput.checkEmail({
            $submit: $('.btn-primary'),
            $email: $emailInput,
            $hint: emailBlock.find('#hint'),
            $hintContainer: emailBlock.find('.dropdown-popup'),
            disableButtons: false,
            disableButtonOnAlreadyExists: true,
            checkCallback: afterCheckCallback
        });
        this.$('.dropdown-popup').on('click', '.signup-link', function () {
            LazyHeader.showAuthForm('signup');
        });
    }
});
Biblio.Views.Fizic = Biblio.Views.Mixin.extend({
    template: "#fiz-payment-template",
    className: 'hide',
    events: {
        'change input.form-control': 'changeValue'
    },
    construct: function () {
        var self = this;
        if (self.model.get('hasEmail')) {
            self.model.set('fio', workspace.appModel.get('myProfile').get('displayName'));
        }
        self.addChildAtElement('.biblio-wizard_payment-type', new UserPayment.Views.OrderPayment({
            model: self.model
        }));
    },
    showErrors: function (response) {
        if (response.fieldErrors)
            this.$('input[type=hidden]').each(function (i, it) {
                var it = $(it),
                        parent = it.parent(),
                        name = it.prop('name');
                if (_.any(response.fieldErrors, function (v, key) {
                    return key === name;
                }))
                    parent.addClass('error');
                else
                    parent.removeClass('error');
            });
    },
    changeValue: function (e) {
        var it = this.$(e.currentTarget);
        this.model.silentSet(it.attr('name'), it.val());
    }
});
Biblio.Views.Uric = Biblio.Views.Mixin.extend({
    template: "#ul-payment-template",
    className: 'hide',
    isExistsPlanetaUiElement: true,
    isSmsSent: false,
    events: {
        'click .js-address-toggle': 'toggleAddress',
        'change [name]': 'changeValue',
        'change [name^="reg"]': 'changeAddress',
        'date-change': 'changeDate',
        'click .js-send-sms:not(.disabled)': 'sendSms'
    },
    // TODO copy-paste from donate-view.js
    beforeSendSms: function (context) {
        context.$('.js-send-sms').addClass('disabled');
        context.$('.js-phone').addClass('disabled');
    },
    afterSendSmsSuccess: function (context) {
        context.$('.js-confirmation-code').removeClass('hidden');
        workspace.appView.showInfoMessage("Вам отправлено sms с кодом подтверждения");
        setTimeout(function () {
            context.$('.js-send-sms').removeClass('disabled');
            context.$('.js-send-sms').html("Выслать код заново");
        }, 10000);
        isSmsSent = true;
    },
    afterSendSmsError: function (context, errorMessage) {
        context.$('.js-send-sms').removeClass('disabled');
        context.$('.js-phone').removeClass('disabled');
        if (errorMessage) {
            workspace.appView.showErrorMessage(errorMessage);
        }
        isSmsSent = false;
    },
    sendSms: function (e) {
        var self = this;
        e.preventDefault();
        if (self.$('.js-send-sms').attr('disabled')) {
            return;
        }
        self.beforeSendSms(self);

        var phone = self.$('.js-phone').val();
        self.model.confirmPhone(phone)
            .done(function (response) {
                self.afterSendSmsSuccess(self);
            })
            .fail(function (response, errorMessage) {
                self.afterSendSmsError(self, errorMessage);
            });
    },

    toggleAddress: function (e) {
        var self = this,
                ab = self.$('.js-address-block');
        self.$(e.currentTarget).find('.checkbox').toggleClass('active');
        ab.toggleClass('hide');
        if (ab.hasClass('hide')) {
            this.$('[name^="reg"]').each(function (i, v) {
                self.changeAddress({currentTarget: v});
            });
        }
    },
    changeAddress: function (e) {
        if (this.$('.js-address-block').hasClass('hide')) {
            var it = this.$(e.currentTarget);
            if (it.is('select')) {
                this.setCountry(it, it.val(), it.attr('name'));
            } else {
                var other = this.$('[name="' + it.attr('name').replace('reg', 'live') + '"]');
                other.val(it.val());
                other.trigger('change');
            }
        }
    },
    changeValue: function (e) {
        var it = this.$(e.currentTarget),
                name = it.attr('name');
        if ('email' === name) {
            this.model.silentSet(name, it.val());
        } else {
            this.model.get('investInfo')[name] = it.val();
        }
    },
    setCountry: function (el, newValue, fieldName, self) {
        var found = el.find('option').filter(function () {
            return $(this).val() == newValue;
        })[0];
        if (!found)
            return;
        var text = found.text;
        this.$el.find('[name="' + (self ? fieldName : fieldName.replace('reg', 'live')) + '"]')
                .next().find('ul > li > a').each(function (i, it) {
            if (it.text == text) {
                $(it).trigger('click');
                return false;
            }
        });
    },
    changeDate: _.debounce(function () {
        this.model.get('investInfo').birthDate = this.$('[name="birthDate"]').val();
    }, 500),
    afterRender: function () {
        var self = this;
        self.$('[name="phoneNumber"]').mask('+7 (999) 999-99-99');
        self.$('[name*="Index"]').mask('999999');
        self.$('[name="birthDate"]').mask('99.99.9999');
        self.$('[name="inn"]').mask('9999999999?99');
    }
});

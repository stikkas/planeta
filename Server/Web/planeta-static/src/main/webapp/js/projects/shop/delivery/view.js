/*globals StorageUtils, ShopAdmin, L10n, Modal, Delivery*/

ShopAdmin.Views = {};
ShopAdmin.Views.DeliveryListItem = BaseView.extend({
    tagName: 'tr',
    attributes: {
        style: 'border-top: 1px solid gray;'
    },
    template: '#delivery-item-template',

    viewEvents: {
        'click .js-remove-delivery': 'removeDeliveryService',
        'click .js-edit-delivery': 'editDeliveryService'
    }

});

ShopAdmin.Views.DeliveryList = BaseListView.extend({
    tagName: 'tbody',
    itemViewType: ShopAdmin.Views.DeliveryListItem
});

ShopAdmin.Views.Delivery = BaseView.extend({
    template: '#delivery-page-template',
    events: {
        'click .js-create-delivery': 'addNewDelivery',
        'removeDeliveryService': 'removeDeliveryService',
        'editDeliveryService': 'editDeliveryService'
    },
    construct: function() {
        this.addChildAtElement('.js-table', new ShopAdmin.Views.DeliveryList({
            collection: this.model.get('linkedDeliveries')
        }));

    },

    addNewDelivery: function(e) {
        new Delivery.Views.ModalAdditor({
            model: new Delivery.Models.LinkedService({
                isNew: true,
                subjectId: 0,
                subjectType: 'SHOP'
            }),
            controllerModel: this.model
        }).render();
    },

    removeDeliveryService: function(e, view) {
        var data = view.model.toJSON();
        var self = this;
        Modal.showConfirm("Вы точно хотите удалить эту запись?", "Подтверждение удаления службы доставки", {
            success: function() {
                var options = {
                    url: '/admin/delivery/remove-linked-delivery.json',
                    data: data,
                    success: function(responce) {
                        if (responce.success) {
                            self.model.get('linkedDeliveries').remove(view.model);

                        }
                    }
                };
                Backbone.sync('update', {}, options);
            }
        });
    },
    editDeliveryService: function(e, view) {
        var data = view.model.toJSON();
        data.isNew = false;
        new Delivery.Views.ModalAdditor({
            model: new Delivery.Models.LinkedService(data),
            controllerModel: this.model
        }).render();
    }
});

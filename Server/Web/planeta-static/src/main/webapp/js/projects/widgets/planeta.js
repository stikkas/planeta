/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 25.07.13
 * Time: 16:25
 */

var page = {};

//backbone stub for TemplateManager correct work
var Backbone = {
    timeout: 1000,
    retryMax: 2
};

var Planeta = {

    init: function () {
        TemplateManager.init({fileTemplates: FileTemplates});
        $('.nav a').each(function () {
            var that = $(this);
            if (document.location.href.indexOf(that.attr('href')) >= 0) {
                that.parent().addClass('active');
            }
        });
        if (page && page.init) {
            page.init();
        }
    }
};

(function ($) {
    $($.proxy(Planeta.init, Planeta));
})(jQuery);
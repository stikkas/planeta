var Account = Account || {
    Views: {},
    Models: {},
    Collections: {}
};

Account.Models.PersonalCabinet = BaseModel.extend({
    defaults: {
        myCards: []
    },
    url: '/api/profile/lk.json'
});

Account.Models.Purchases = BaseModel.extend({

});
Account.Models.Transactions = BaseModel.extend({

});
Account.Models.Cards = BaseModel.extend({

});
Account.Models.Regulars = BaseModel.extend({

});
Account.Models.Billings = BaseModel.extend({

});

/*---------------- Collections ----------------------------------*/
Account.Collections.Purchases = BaseCollection.extend({
    model: Account.Models.Purchases,
    url:'/profile/search-purchases.json'
});
Account.Collections.Transactions = BaseCollection.extend({
    model: Account.Models.Transactions,
    url:'/profile/search-purchases.json' 
});
Account.Collections.Cards = BaseCollection.extend({
    model: Account.Models.Cards, 
    url:'/profile/search-purchases.json' 
});
Account.Collections.Regulars = BaseCollection.extend({
    model: Account.Models.Regulars,
    url:'/profile/search-purchases.json' 
});
Account.Collections.Billings = BaseCollection.extend({
    model: Account.Models.Billings,
    url:'/profile/search-billings.json' 
});

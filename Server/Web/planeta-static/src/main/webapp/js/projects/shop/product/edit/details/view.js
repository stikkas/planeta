/*globals ShopUtils, ProductEdit, ProductStorages, GroupListView, Modal*/

ProductEdit.Views.Tags = BaseView.extend({
    template: '#product-edit-tags-template',
    addProductTag: function (e) {
        var newTagId = e.params.data.id;
        this.model.addProductTagIfNotExists(newTagId)
    },

    removeProductTag: function (e) {
        var newTagId = e.params.data.id;
        this.model.removeProductTag(newTagId);
    },
    afterRender: function() {
        var self = this;
        var $tags = this.$('#type');
        $tags.on("select2:selectCampaignById", function (e) {
            self.addProductTag(e)
        });
        $tags.on("select2:unselect", function (e) {
            self.removeProductTag(e);

            //http://stackoverflow.com/questions/29618382/disable-dropdown-opening-on-select2-clear
            $(this).data('state', 'unselected');
        }).on("select2:open", function(e) {
                //http://stackoverflow.com/questions/29618382/disable-dropdown-opening-on-select2-clear
                if ($(this).data('state') === 'unselected') {
                    $(this).removeData('state');

                    var self = $(this);
                    setTimeout(function() {
                        self.select2('close');
                    }, 1);
                }
            });
        $tags.select2({
            tags: true,
            createTag: function(params) {
                http://stackoverflow.com/questions/30471338/how-to-disable-creating-new-tags-with-select2-v4-0
                return undefined;
            }
        });
    }
});

ProductEdit.Views.TagsInProductList = ProductEdit.Views.Tags.extend({
    template: '#product-edit-tags-in-product-list-template'
});

ProductEdit.Views.Category = BaseView.extend({
    template: '#product-edit-category-template',
    events: {
        'change #productCategory': 'onProductCategoryChanged'
    },
    onProductCategoryChanged: function (e) {
        e.preventDefault();
        var newCategory = this.$(e.currentTarget).find(':selected').val();
        this.model.changeProductCategory(newCategory);
    }
});

ProductEdit.Views.Price = BaseView.extend({
    template: '#product-edit-price-template',
    events: {
        'keyup #price': 'onPriceChanged',
        'keyup #oldPrice': 'onOldPriceChanged'
    },
    onPriceChanged: _.debounce(function (e) {
        var price = parseFloat(this.$(e.currentTarget).val());
        this.model.set({price: price}, {silent: true});
    }, 200),

    onOldPriceChanged: _.debounce(function (e) {
        var oldPrice = parseFloat(this.$(e.currentTarget).val());
        this.model.set({oldPrice: oldPrice}, {silent: true});
    }, 200)
});

ProductEdit.Views.Params = BaseView.extend({
    template: '#product-edit-params-template',
    events: {
        'change #freeDelivery': 'onFreeDeliveryChanged',
        'change #cashAvailable': 'onCashAvailableChanged'
    },
    onFreeDeliveryChanged: function (e) {
        e.preventDefault();
        var isFreeDelivery = this.$(e.currentTarget).val() === 'true';
        this.model.set({freeDelivery: isFreeDelivery}, {silent: true});
    },

    onCashAvailableChanged: function (e) {
        e.preventDefault();
        var isCashAvailable = this.$(e.currentTarget).val() === 'true';
        this.model.set({cashAvailable: isCashAvailable}, {silent: true});
    }
});

ProductEdit.Views.InfoTable = BaseView.extend({
    template: '#product-edit-infotable-template',
    events: {
        'click .add-table-image-url:not(.disabled)': 'onAddAttrTableClicked',
        'click .delete-table-image-url:not(.disabled)': 'onRemoveAttrTableClicked'
    },
    onRemoveAttrTableClicked: function (e) {
        e.preventDefault();
        this.model.unset('tableImageUrl')
    },

    onAddAttrTableClicked: function (e) {
        e.preventDefault();
        this.model.uploadTableImage();
    },
    afterRender: function () {
        var self = this;
        moduleLoader.loadModule('fancybox').done(function () {
            self.$('.photo-zoom-link').fancybox();
        });
    }
});

ProductEdit.Views.Attributes = BaseView.extend({
    template: '#product-edit-attributes-template',
    events: {
        'click .js-add-attr:not(.disabled)': 'onAddAttrClicked',
        'click .remove-child': 'onRemoveChildProduct',
        'click .js-predefined-attribute': 'addPredefinedAttributes',
        'keyup input': 'onQuantityChanged'
    },
    onQuantityChanged: _.debounce(function (e) {
        $el = this.$(e.currentTarget);
        var qty = parseInt($el.val());
        var attrValue = $el.data("attribute-value");

        (_(this.model.get('childrenProducts')).find(function(product) {
            return (product.productAttribute.value == attrValue);
        })|| {}).totalQuantity = qty;
    }, 200),
    onAddAttrClicked: function (e) {
        if (e) e.stopPropagation();
        var inputEl = this.$('input[name=new-attr-name]'),
            self = this;
        var attrValue = inputEl.val();
        if (!attrValue) {
            workspace.appView.showErrorMessage('Пожалуйста укажите название');
            inputEl.focus();
            return;
        } else if (attrValue.length > 25) {
            workspace.appView.showErrorMessage("Длина должна быть в пределах 25 символов");
            inputEl.focus();
            return;
        }

        var newContentUrlsString = this.$('[name="newContentUrlsString"]');
        if (newContentUrlsString.length) {
            if (!newContentUrlsString.val().trim()) {
                workspace.appView.showErrorMessage('Укажите ссылку');
                return;
            }
        }
        var contentUrlsString = newContentUrlsString.val();
        this.model.addAttribute(attrValue, contentUrlsString).fail(function (result) {
            workspace.appView.showErrorMessage('Пожалуйста выберите другое название');
            inputEl.focus();
        });
    },
    addPredefinedAttributes: function (e) {
        if (e) {
            e.stopPropagation();
            var c = $(e.currentTarget);
            var tagName = c.data("tagName");
            var map = ShopUtils.getTagToAttributesTypeMap();
            for (var key in map[tagName]) {
                this.model.addAttribute(map[tagName][key]);
            }
        }
    },

    onRemoveChildProduct: function (event, view) {
        event.stopPropagation();
        var attributeName = this.$(event.currentTarget).data('attributeValue');

        var model = this.model,
            self = this;

        var children = model.get('childrenProducts') || [];
        var attributeForRemove = _.find(children, function (child) {
            return child.productAttribute.value == attributeName;
        });

        if (attributeForRemove.productId) {
            Modal.showConfirm('<p>Вы точно хотите удалить эту модель из ассортимента?</p><p>* модель будет доступна только из истории заказов</p>', "Подтверждение удаления", {
                success: function () {
                    model.removeChildProduct(attributeName);
                    self.render();
                }
            });
        } else {
            model.removeChildProduct(attributeName);
            self.render();
        }
    },

    afterRender: function() {
        this.$('.limited-input').each(function () {
            var $el = $(this);
            $el.limitInput({
                limit: $el.data().limit || 0,
                remText: "",
                remTextEl: $(),
                toggleCounter: /*{
                 show: "focusin",
                 hide: "focusout"
                 }*/ false,
                enableNegative: false
            });
        });
    }
});

ProductEdit.Views.Details = BaseView.extend({
    anchor: '.plate-cont',
    template: '#product-edit-details-template',

    construct: function() {
        this.addChildAtElement('.js-product-tags', new ProductEdit.Views.Tags({
            model: this.model
        }));
        this.addChildAtElement('.js-product-category', new ProductEdit.Views.Category({
            model: this.model
        }));
        this.addChildAtElement('.js-product-price', new ProductEdit.Views.Price({
            model: this.model
        }));
        this.addChildAtElement('.js-product-params', new ProductEdit.Views.Params({
            model: this.model
        }));

        this.addChildAtElement('.js-product-attributes', new ProductEdit.Views.Attributes({
            model: this.model
        }));
        this.addChildAtElement('.js-product-infotable', new ProductEdit.Views.InfoTable({
            model: this.model
        }));

        this.addChildAtElement('.js-product-quantity', new ProductEdit.Views.SingleProductQuantity({
            model: this.model
        }));
    },

    getData: function () {
        var form = this.$('form');
        return form ? form.serializeObject() : {};
    }

});


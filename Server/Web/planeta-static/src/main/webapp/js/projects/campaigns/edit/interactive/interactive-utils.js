
var InteractiveUtils = {
    calculateNeededTargetAmount: function (contractorTypePercent, desiredCollectedAmountPercent, desiredTargetAmount) {
        var calculatedTargetAmount = 0;
        if (contractorTypePercent == 6) {
            calculatedTargetAmount = desiredTargetAmount/((1-desiredCollectedAmountPercent/100)*(1-contractorTypePercent/100));
        } else {
            calculatedTargetAmount = desiredTargetAmount/(1-desiredCollectedAmountPercent/100 - contractorTypePercent/100);
        }
        return calculatedTargetAmount.toFixed(0);
    },

    calculateTargetAmountAfterTax: function (contractorTypePercent, desiredCollectedAmountPercent, targetAmount) {
        var calculatedTargetAmountAfterTax = 0;
        if (contractorTypePercent == 6) {
            var sumAfterPlanetaTax = targetAmount/100*desiredCollectedAmountPercent;
            calculatedTargetAmountAfterTax = targetAmount - sumAfterPlanetaTax - (targetAmount-sumAfterPlanetaTax)/100*contractorTypePercent;
        } else {
            calculatedTargetAmountAfterTax = targetAmount - targetAmount/100*desiredCollectedAmountPercent - targetAmount/100*contractorTypePercent;
        }
        return calculatedTargetAmountAfterTax.toFixed(0);
    }
};
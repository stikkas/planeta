/*globals User, PrivacyUtils,Avatar, Modal, UserPayment, LazyHeader, StorageUtils, loadModule, StringUtils, ProfileUtils*/
if (!window.User) {
    window.User = {
        Models: {},
        Views: {}
    };
}

User.Views.Card = BaseView.extend({
    template: '#user-profile-card-template',
    initialize: function () {
        this.model = this.model.get('profileModel');
        this.model.checkOnlineStatus();
        BaseView.prototype.initialize.apply(this, arguments);
        Banner.init('https://' + workspace.serviceUrls.mainHost, workspace.appModel.get('myProfile').get('isAuthor'));
    },

    onChangeAvatarClicked: function (e) {
        e.preventDefault();
        var self = this;
        if (PrivacyUtils.isAdmin()) {
            loadModule("avatar").done(function () {
                Avatar.changePhoto({
                    success: function (result) {
                        self.model.set({
                            imageId: result.imageId,
                            imageUrl: result.imageUrl,
                            smallImageId: result.smallImageId,
                            smallImageUrl: result.smallImageUrl
                        });
                    }
                });
            });
        }
    },

    onRemoveAvatarClicked: function () {
        var self = this;
        Modal.showConfirm("Вы действительно хотите удалить аватар?", null, {
            success: function () {
                Backbone.sync('delete', null, {
                    url: '/api/profile/avatar-save.json',
                    data: {
                        avatarImageId: 0,
                        profileId: self.model.get("profileId")
                    }
                }).done(function () {
                    self.model.set({
                        imageId: 0,
                        imageUrl: null,
                        smallImageId: 0,
                        smallImageUrl: null
                    });
                });
            }
        });
    },

    onMoreInfoClicked: function () {
        new User.Views.ModalInfo({
            model: this.options.contentModel
        });
    },

    beforeRender: function () {
        this.model.set({
            summaryWithoutHtml: StringUtils.stripHtmlTags(this.model.get('summary'))
        }, {silent: true});
    }
});

User.Views.ModalInfo = Modal.View.extend({
    template: "#user-modal-summary-template",
    urlAnchor: "info",
    clickOff: true,
    initialize: function () {
        Modal.View.prototype.initialize.apply(this, arguments);
        this.model = this.model.get('profileModel');
        this.render();
    }
});

User.Views.Info = BaseView.extend({
    template: '#user-info-template',

    initialize: function (options) {
        this.profileModel = options.model.get('profileModel');
        if (workspace.appModel.isCurrentProfileMine()) {
            var balance = workspace.appModel.get('myProfile').get('myBalance');
            if (!balance) {
                balance = 0;
            }
            this.model = workspace.appModel.get('myProfile');
        } else {
            this.model = this.profileModel;
        }
        this.model.set({allowToggleSubscribe: true}, {silent: true});
        BaseView.prototype.initialize.apply(this, arguments);
    },

    onIncreaseBalanceClicked: function () {
        loadModule('payment').done(function () {
            var model = new UserPayment.Models.BasePayment({
                redirectUrl: '/profile/increase-balance.html',
                oldPayment: true
            });
            Modal.showDialogByViewClass(UserPayment.Views.BalancePaymentDialog, model);
        });
    },

    onUnsubscribeClicked: function () {
        var self = this;
        if (self.model.get('allowToggleSubscribe')) {
            self.model.set({allowToggleSubscribe: false}, {silent: true});
            PrivacyUtils.unsubscribe().always(function (result) {
                self.model.set('allowToggleSubscribe', true);
                if (result && result.success) {
                    self.model.set('subscribersCount', self.model.get('subscribersCount') - 1);
                } else {
                    workspace.appView.showMessageUnexpectedError();
                }
            });
        }
    },

    onSubscribeClicked: function () {
        if (workspace.isAuthorized) {
            var self = this;
            if (self.model.get('allowToggleSubscribe')) {
                self.model.set({allowToggleSubscribe: false}, {silent: true});
                PrivacyUtils.subscribe().always(function (result) {
                    self.model.set('allowToggleSubscribe', true);
                    if (result && result.success) {
                        self.model.set('subscribersCount', self.model.get('subscribersCount') + 1);
                    } else {
                        workspace.appView.showMessageUnexpectedError();
                    }
                });
            }
        } else {
            LazyHeader.showAuthForm('signup');
        }
    },

    onSendMessageClicked: function () {
        if (!workspace.isAuthorized) {
            LazyHeader.showAuthForm('signup');
        } else {
            if (PrivacyUtils.mayISendYouMessage() || PrivacyUtils.isAdmin()) {
                workspace.appModel.get('dialogsController').showSendMessageDialog(this.profileModel.get("profileId"));
            }
        }
    },

    onShowSubscriberListClicked: function () {
        this.model.set('newSubscribersCount', 0);
        this.showSubscriberList();
    },

    showSubscriberList: function (contentModel) {
        var profileModel = contentModel ? contentModel.get('profileModel') : this.profileModel;
        loadModule("user-subscribers").done(function () {
            var view = new User.Views.Subscriber({
                model: new User.Models.Subscriber({
                    profileId: profileModel.get("profileId")
                })
            });
            view.render();
        });
    },

    onShowSubscriptionListClicked: function () {
        this.showSubscriptionList();
    },

    showSubscriptionList: function (contentModel) {
        var profileModel = contentModel ? contentModel.get('profileModel') : this.profileModel;
        loadModule("user-subscribers").done(function () {
            var view = new User.Views.Subscription({
                model: new User.Models.Subscription({
                    profileId: profileModel.get("profileId")
                })
            });
            view.render();
        });
    }
});

User.Views.Tabs = BaseView.extend({
    template: '#user-tabs-template',
    initialize: function (options) {
        var profileModel = options.contentModel.get('profileModel');
        var section = options.contentModel.get('section');
        this.model = new User.Models.Tabs({
            section: section,
            hrefAlias: ProfileUtils.getAbsoluteUserLink(profileModel.get('profileId'), profileModel.get('alias')),
            profileId: profileModel.get('profileId')
        });
        BaseView.prototype.initialize.apply(this, arguments);
        this.visitTab(section);
        if (workspace.appModel.isCurrentProfileMine()) {
            this.model.fetchX();
        }
    },

    profileVisitTypeMap: {
        'news': 'NEWS_TAB',
        'comments': 'COMMENTS_TAB'
    },

    visitTab: function (section) {
        if (workspace.appModel.isCurrentProfileMine() && this.profileVisitTypeMap[section]) {
            Backbone.sync("update", null, {
                url: '/api/profile/profile-last-visit.json',
                data: {
                    profileLastVisitType: this.profileVisitTypeMap[section]
                }
            });
        }
    }
});

User.Models.Tabs = BaseModel.extend({
    url: '/api/profile/get-tabs-count-info.json'
});

User.Models.Social = BaseModel.extend({
    url: '/api/public/profile-sites',
    initialize: function (options) {

        if (!options || !options.profileId) {
            workspace.stats.tackNoProfileId('UserModelsSocial');
            if (!options)
                options = {};
            options.profileId = -1;
        }

        BaseModel.prototype.initialize.apply(this, arguments);
        this.fetch({
            data: {
                profileId: options.profileId
            }
        });
    }
});

User.Views.Social = BaseView.extend({
    template: '#user-social-template',
    initialize: function (options) {
        this.profileModel = options.contentModel.get('profileModel');
        this.model = new User.Models.Social({profileId: this.profileModel.get("profileId")});
        BaseView.prototype.initialize.apply(this, arguments);
    },

    modelJSON: function () {
        return _.extend(this.model.toJSON(), {profile: this.profileModel.attributes});
    }

});

User.Models.BackedProjectsStat = BaseModel.extend({
    url: "/api/campaign/get-user-backed-campaign-tag-count-list.json",

    initialize: function (profileModel) {
        BaseModel.prototype.initialize.call(this);
        this.profileModel = profileModel;
        this.fetch({
            data: {
                profileId: profileModel.get('profileId')
            }
        });
    },

    parse: function (result) {
        var mx = 0;
        _.each(result, function (item) {
            if (item.campaignsCount > mx) {
                mx = item.campaignsCount;
            }
        });
        _.each(result, function (item) {
            item.percent = mx === 0 ? 0 : Math.floor(item.campaignsCount / mx * 100);
        });
        return _.extend({
            campaignTagCountList: result,
            lang: workspace && workspace.currentLanguage ? workspace.currentLanguage : 'ru'
        }, this.attributes);
    }
});

User.Views.BackedProjectsStat = BaseView.extend({
    template: '#user-views-backed-projects-stat',
    initialize: function (options) {
        if (!options.contentModel.get('profileModel').get('showBackedCampaigns')) {
            this.dispose();
            return;
        }
        this.model = new User.Models.BackedProjectsStat(options.contentModel.get('profileModel'));
        BaseView.prototype.initialize.apply(this, arguments);
    }
});

User.Views.AdminActions = BaseView.extend({
    template: '#user-admin-actions-views-template',
    modelEvents: {
        'destroy': 'dispose'
    },
    initialize: function (options) {
        BaseView.prototype.initialize.apply(this, arguments);
        this.profileModel = options.contentModel.get('profileModel');
    },

    onCreateProjectClicked: function () {
        Modal.startLongOperation();
        Backbone.sync("create", null, {
            url: "/admin/create-campaign-by-admin.json",
            data: {
                profileId: this.profileModel.get('profileId')
            }
        }).done(function (result) {
            if (result && result.success) {
                workspace.navigate("/campaigns/" + result.result + "/edit");
            } else {
                workspace.appView.showMessageUnexpectedError();
            }
        }).fail(function () {
            workspace.appView.showMessageUnexpectedError();
        }).always(function () {
            Modal.finishLongOperation();
        });
    }

});

User.Models.ModeratorAlert = BaseModel.extend({
    url: "/api/campaign/last-campaign-moderation-message-list.json",
    parse: function (result) {
        var list = [];
        var campaignAlertHide = StorageUtils.get('campaignAlertHide') || {};
        _.each(result, function (item) {
            if (!(campaignAlertHide[item.campaignId] == item.campaignStatus)) {
                list.push(item);
            }
        });
        return {
            list: list,
            length: list.length
        };
    }
});

User.Views.ModeratorAlert = BaseView.extend({
    template: '#user-views-moderator-alert-template',
    initialize: function (options) {
        this.model = new User.Models.ModeratorAlert();
        if (PrivacyUtils.isAdmin()) {
            this.model.fetchX({
                data: {
                    profileId: options.contentModel.get('profileModel').get("profileId"),
                    campaignStatuses: "APPROVED,DECLINED,PATCH"
                }
            });
        }

        BaseView.prototype.initialize.apply(this, arguments);
    },

    onAlertCloseClicked: function (e) {
        var $el = $(e.currentTarget);
        var campaignId = $el.data("campaignId");
        var status = $el.data("status");
        var list = _.reject(this.model.get('list'), function (item) {
            return item.campaignId == campaignId;
        });
        this.model.set({
            list: list,
            length: list.length
        });

        var campaignAlertHide = StorageUtils.get('campaignAlertHide') || {};
        campaignAlertHide[campaignId] = status;
        StorageUtils.set('campaignAlertHide', campaignAlertHide);
    }

});
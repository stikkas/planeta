var player = {
    player: {},
    controlBar: {},
    track: {},
    progressBar: {},
    captions: {},
    isSeeking: false,
    init: function () {
        var self = this;

        this.player = videojs(document.querySelector('.video-js'), {
            autoplay: !(videojs.browser.IS_IOS || videojs.browser.IS_ANDROID),
            loop: false,
            playsinline: true,
            nativeControlsForTouch: false,
            bigPlayButton: (videojs.browser.IS_IOS || videojs.browser.IS_ANDROID),
            html5: {
                nativeTextTracks: false
            },
            controlBar: {
                playToggle: false,
                volumePanel: false,
                currentTimeDisplay: false,
                timeDivider: false,
                durationDisplay: false,
                progressControl: false,
                liveDisplay: false,
                remainingTimeDisplay: false,
                customControlSpacer: false,
                playbackRateMenuButton: false,
                chaptersButton: false,
                descriptionsButton: false,
                subtitlesButton: false,
                captionsButton: false,
                audioTrackButton: false,
                fullscreenToggle: false,
                subsCapsButton: false
            },
            inactivityTimeout: 0
        }, function() {
            var self = this;

            this.volume(1); // or this.volume(1) for run on ios
            //this.userActive(true);

            this.textTrackSettings.setValues({
                fontPercent: 1,
                backgroundOpacity: "0"
            });

            this.on('touchend', function setAutoplayOnStart() {
                self.userActive(true);
            });

            this.on('play', function setAutoplayOnStart() {
                self.off('play', setAutoplayOnStart);
                self.autoplay(true);
            });


            this.on('loadedmetadata', changeRatio);

            $(window).on('resize', _.debounce(changeRatio, 1000));

            function changeRatio() {
                var videoWidth = player.player.videoWidth();
                var videoHeight = player.player.videoHeight();
                var videoRatio = videoWidth/videoHeight;

                var videoContWidth = $(player.player.el_).width();
                var videoContHeight = $(player.player.el_).height();
                var videoContRatio = videoContWidth/videoContHeight;

                $(player.player.el_).toggleClass('fill-vertical', videoContRatio < videoRatio);
            }
        });


        this.controlBar = this.player.controlBar;


        this.registerPlayerControls();

        this.addRemoteTextTrack = function (options, manualCleanup) {
            if (CookieProvider.get('showSubtitlesPlanetaPlayer')) {
                options.mode = 'showing';
            } else {
                options.mode = 'hidden';
            }
            return this.player.addRemoteTextTrack(options, manualCleanup);
        };
    },
    firstView: function () {
        this.controlBar.addChild('playToggle');
        this.controlBar.addChild('volumePanel', {inline: false});
        this.controlBar.addChild('TinyProgress');
        this.controlBar.addChild('CustomControlSpacer');
        this.controlBar.addChild('Captions');
        // this.controlBar.addChild('Share');
    },
    loopView: function () {
        this.controlBar.addChild('Repeat');
        // this.controlBar.addChild('Share');
    },
    normalView: function () {
        this.controlBar.addChild('playToggle');
        this.controlBar.addChild('volumePanel', {inline: false});
        this.controlBar.addChild('Progress');
        this.controlBar.addChild('TimeDuration');
        this.controlBar.addChild('Captions');
        // this.controlBar.addChild('Share');
    },


    destroyAllControls: function () {
        this.controlBar.removeChild('playToggle');
        this.controlBar.removeChild('volumePanel');

        this.controlBar.removeChild('CustomControlSpacer');
        this.controlBar.removeChild('Repeat');
        this.controlBar.removeChild('Captions');
        // this.controlBar.removeChild('Share');
        this.controlBar.removeChild('TinyProgress');
        this.controlBar.removeChild('Progress');
        this.controlBar.removeChild('TimeDuration');
    },


    registerPlayerControls: function () {
        var self = this;
        var vjsComponent = videojs.getComponent('Component');
        var vjsButton = videojs.getComponent('Button');
        var vjsProgressControl = videojs.getComponent('ProgressControl');


        var mouseDown = 0;
        $(document).on('mousedown touchstart', function (e) {
            ++mouseDown;
        });
        $(document).on('mouseup touchend', function () {
            --mouseDown;
        });


        /* Repeat */
        var Repeat = videojs.extend(vjsButton, {
            constructor: function(player, options) {
                vjsButton.apply(this, arguments);
                this.controlText('Repeat');
            },
            buildCSSClass: function () {
                return 'vjs-repeat-control vjs-play-control vjs-ended ' + vjsButton.prototype.buildCSSClass();
            },
            createEl: function () {
                return vjsButton.prototype.createEl.call(this, 'button', {
                    innerHTML: '' +
                    '<span class="vjs-repeat-control-inner">' +
                    '<span aria-hidden="true" class="vjs-icon-placeholder"></span>' +
                    '<span class="vjs-repeat-label">Повторить видео</span>' +
                    '</span>'
                });
            },
            handleClick: function () {
                this.player_.currentTime(0);
                self.isSeeking = true;
                setTimeout(function () {
                    self.isSeeking = false;
                }, 100);
                $('body').trigger('player.repeat');
            }
        });
        videojs.registerComponent('Repeat', Repeat);
        /* / Repeat */



        /* Captions */
        var Captions = videojs.extend(vjsButton, {
            constructor: function() {
                vjsButton.apply(this, arguments);

                this.controlText('Toggle Captions');
                player.captions = this;

                this.on('touchstart', videojs.bind(this, function(event) {
                    this.toggleCaptions();
                    event.preventDefault();
                }));
                this.on('click', videojs.bind(this, this.toggleCaptions));
            },
            buildCSSClass: function () {
                return 'vjs-caption-toggle-control '
                + (CookieProvider.get('showSubtitlesPlanetaPlayer') ? ' vjs-enabled ' : '')
                    + vjsButton.prototype.buildCSSClass();
            },
            createEl: function () {
                return vjsButton.prototype.createEl.call(this, 'button', {
                    innerHTML: '' +
                    '<span class="vjs-caption-toggle-control-inner">' +
                    '<span class="vjs-repeat-label">Субтитры</span>' +
                    '<span aria-hidden="true" class="vjs-caption-toggle-icon"></span>' +
                    '</span>'
                });
            },
            toggleCaptions: function (val) {
                var tracks = this.player().textTracks(),
                    track,
                    i;

                for (i = 0; i < tracks.length; i++) {
                    track = tracks[i];
                    if (track.kind === 'captions' && typeof val !== 'boolean') {
                        if (track.mode !== 'showing') {
                            track.mode = 'showing';
                            this.addClass('vjs-enabled');
                            var cookieExpiresTime = new Date(new Date().getTime() + (30 * 24 * 60 * 60 * 1000)).toUTCString();
                            CookieProvider.set('showSubtitlesPlanetaPlayer', true,
                                {
                                    path: '/',
                                    expires: cookieExpiresTime
                                });
                        } else {
                            track.mode = 'hidden';
                            this.removeClass('vjs-enabled');
                            CookieProvider.remove('showSubtitlesPlanetaPlayer', '/');
                        }
                    } else if ( typeof val === 'boolean' ) {
                        track.mode = 'hidden';
                    }
                }
            }
        });
        videojs.registerComponent('Captions', Captions);
        /* / Captions */



        /* Progress */
        var Progress = videojs.extend(vjsProgressControl, {
            constructor: function() {
                vjsProgressControl.apply(this, arguments);

                var progress = this;
                self.progressBar = progress;
                var seekBar = this.seekBar;
                seekBar.playProgressBar.removeChild("TimeTooltip");
                seekBar.off(['mousemove', 'mousedown']);

                progress.on(['mousedown', 'touchstart'], function (event) {
                    seekBar.handleMouseMove(event);

                    self.player.one('seeking', function seekingProgress() {
                        if ( self.isSeeking || self.player.paused() || !mouseDown ) return;
                        self.isSeeking = true;

                        var seekingTimeout;
                        clearTimeout(seekingTimeout);
                        seekingTimeout = setTimeout(function() {
                            !!mouseDown && self.player.pause();
                        }, 100);

                        $(document).off('mouseup.seeking touchend.seeking keyup.seeking');

                        $(document).one('mouseup.seeking touchend.seeking keyup.seeking', function () {
                            if ( !self.isSeeking ) return;
                            self.isSeeking = false;
                            clearTimeout(seekingTimeout);
                            self.player.paused() && self.player.play();
                        });
                    });
                });


                var doc = progress.el_.ownerDocument;
                progress.on(doc, 'mousemove', progress.handleMouseMove);

                this.on(['mousedown', 'touchstart'], function () {
                    progress.addClass('vjs-progress-dragging');
                    $(document).one('mouseup', function () {
                        progress.removeClass('vjs-progress-dragging');
                    });
                });
            }
        });
        videojs.registerComponent('Progress', Progress);
        /* / Progress */



        /* TinyProgress */
        var TinyProgress = videojs.extend(vjsProgressControl, {
            constructor: function() {
                vjsProgressControl.apply(this, arguments);
                this.seekBar.removeChild("mouseTimeDisplay");
                this.seekBar.playProgressBar.removeChild("TimeTooltip");

                this.off(['mousemove', 'touchmove', 'mousedown', 'touchstart', 'mouseup', 'touchend']);
                this.seekBar.off(['mousemove', 'touchmove', 'mousedown', 'touchstart', 'mouseup', 'touchend']);

                this.addClass('vjs-tiny-progress');
                this.removeClass('vjs-progress-control');
            }
        });
        videojs.registerComponent('TinyProgress', TinyProgress);
        /* / TinyProgress */



        /* TimeDuration */
        var TimeDuration = videojs.extend(vjsComponent, {
            constructor: function() {
                vjsComponent.apply(this, arguments);
            },
            createEl: function() {
                return videojs.dom.createEl('div', {
                    className: 'vjs-control vjs-time-duration'
                });
            }
        });
        TimeDuration.prototype.options_ = {
            children: [
                'currentTimeDisplay',
                'timeDivider',
                'durationDisplay'
            ]
        };
        videojs.registerComponent('TimeDuration', TimeDuration);
        /* / TimeDuration */



        /* Share */
        var Share = videojs.extend(vjsButton, {
            constructor: function() {
                vjsButton.apply(this, arguments);
            },
            buildCSSClass: function () {
                return 'vjs-share-control ' + vjsButton.prototype.buildCSSClass();
            },
            createEl: function () {
                return vjsButton.prototype.createEl.call(this, 'button', {
                    innerHTML: '' +
                    '<span class="vjs-share-control-inner">' +
                    '<span aria-hidden="true" class="vjs-icon-placeholder"></span>' +
                    '</span>'
                });
            },
            handleClick: function () {
                $('body').trigger('player.share');
            }
        });
        videojs.registerComponent('Share', Share);
        /* / Share */
    }
};


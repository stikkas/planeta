/*globals Welcome, Modal, DefaultContentScrollListView*/
Welcome.Views.SelectDraftCampaignList = Modal.View.extend({
    template: "#selectCampaignById-draft-campaign-modal-template",
    construct: function (options) {
        this.addChildAtElement(".js-draft-campaign-list", new Welcome.Views.DraftCampaignList({
            collection: options.collection
        }));
    },
    afterRender: function () {
        $('#modal-dialog .modal').css('min-width', '0px');
        Modal.View.prototype.afterRender.apply(this, arguments);
    },
    onCreateClicked: function () {
        this.dispose();
    }
});

Welcome.Views.SelectDraftCampaignList.onCreateProjectClicked = function (e) {

    if (workspace.isAuthorized) {
        e.preventDefault();
        Modal.startLongOperation();
        var draftCampaignCollection = new Welcome.Models.DraftCampaignList([]);
        draftCampaignCollection.load().done(function () {
            Modal.finishLongOperation();
            if (draftCampaignCollection.length > 0) {
                var view = new Welcome.Views.SelectDraftCampaignList({collection: draftCampaignCollection});
                view.render();
            } else {
                workspace.navigate('/funding-rules');
            }
        }).fail(function () {
            Modal.finishLongOperation();
        });
    }
};


Welcome.Views.DraftCampaign = BaseView.extend({
    template: '#draft-campaign-item-template',
    className: 'ctba-modal-draft-item',
    events: {
        'click': 'onClick'
    },
    onClick: function () {
        var self = this,
                campaign = workspace.appModel.get('contentModel');
        if (campaign && campaign.getDataToSave) {
            $.get(campaign.url, {campaignId: campaign.get('campaignId'),
                profileId: campaign.get('profileId')}).done(function (res) {
                if (res.result && _.any(res.result, function (value, key) {
                        if (_.contains(['name', 'campaignAlias', 'shortDescription', 
                            'targetAmount', 'timeFinish', 'tags', 'description'], key)) {
                            var campValue = campaign.get(key);
                            return !_.isEqual(value, campValue);
                        }
                        return false;
                    })) {
                        Modal.showConfirm("Имеются несохраненные данные. Продолжить?", "Подтверждение перехода", {
                            success: function () {
                                self.change();
                            }
                        });
                } else {
                    self.change();
                }
            }).fail(function(){
                self.change();
            });
        } else {
            self.change();
        }
    },
    change: function() {
        window.location.href = '//' + workspace.serviceUrls.mainHost + '/campaigns/' + this.model.get('campaignId') + '/edit';
    }
});

Welcome.Views.DraftCampaignList = DefaultContentScrollListView.extend({
    itemViewType: Welcome.Views.DraftCampaign
});


Welcome.Models.DraftCampaignList = BaseCollection.extend({
    url: '/api/profile/get-draft-campaigns.json',
    limit: 999
});
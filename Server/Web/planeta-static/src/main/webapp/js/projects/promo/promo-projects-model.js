var Promo = {
    Models: {},
    Views: {}
};

Promo.Models.Filter = BaseModel.extend({
    defaults: {
        activeFilter: 0,
        category: '',
        limit: 20,
        filters: [],
        isEmpty: true
    }
});


Promo.Models.CampaignList = BaseCollection.extend({
    url: '/search-projects.json',

    defaults: {
        data: {
            categories: '',
            status: 'ACTIVE'
        },
        limit: 20
    },

    parse: function (response) {
        return response.searchResultRecords;
    }
});

Promo.Models.NewsPostsList = BaseCollection.extend({
    url: '/techbattle/promo-news.json',

    defaults: {
        limit: 6
    }
});



Biblio.data.stepThree = true;
(function () {
    var paymentModel;
    var paymentView;
    $('.btn-primary').click(function () {
        $('.btn-primary').prop('disabled', true);

        $('[name="phoneNumber"]').mask('+7 (999) 999-99-99');
        $('[name*="Index"]').mask('999999');
        $('[name="birthDate"]').mask('99.99.9999');
        $('[name="inn"]').mask('9999999999?99');

        Backbone.sync('update', null, {
            url: '/biblio/pay',
            data: paymentModel.toJSON(),
            contentType: 'application/json'
        }).done(function (response) {
            if (response.success) {
                window.location.href = response.result;
            } else {
                workspace.appView.showErrorMessage(response.errorMessage);
                var fieldErrors = {};
                _.each(response.fieldErrors, function (value, key) {
                    if (/^investInfo\./.test(key)) {
                        fieldErrors[key.split('.')[1]] = value;
                    } else {
                        fieldErrors[key] = value;
                    }
                });
                response.fieldErrors = fieldErrors;
                paymentView.showErrors(response);
                $('.btn-primary').prop('disabled', false);
                if (response.fieldErrors && response.fieldErrors.email && !workspace.isAuthorized) {
                    LazyHeader.showAuthForm('signup');
                }
            }
        }).fail(function (_, errorMessage) {
            if (errorMessage) {
                workspace.appView.showErrorMessage(errorMessage);
            }
            $('.btn-primary').prop('disabled', false);
        });
    });
    $('document').ready(function () {
        paymentModel = new Biblio.Models.Payment();
        paymentModel.fetch({url: '/api/profile/payment-methods-biblio.json'}).success(function () {
            paymentView = new Biblio.Views.Payment({model: paymentModel}).render();
            paymentView.setupCheckEmail();
            // Для повторного использования способа оплаты
//            var previosPaymentMethodId = SessionStorageProvider.get("currentPaymentMethodId");
//            if (previosPaymentMethodId) {
//                var methods = paymentModel.get('methods'),
//                        prevModel = methods.find(function (model) {
//                            return model.get('id') === previosPaymentMethodId;
//                        });
//                if (prevModel) {
//                    methods.selectCampaignById(prevModel);
//                }
//            }
        });

        var binModel = new Biblio.Models.Bin();
        binModel.fetch().success(function () {
            new Biblio.Views.LibraryTotalBin({model: binModel}).render();
            new Biblio.Views.Bin({model: binModel}).render();
            paymentModel.setPrice(binModel.get('totalSum'), true);
            if (binModel && binModel.attributes && binModel.attributes.books) {
                paymentModel.set({bookIds: _.keys(binModel.attributes.books)}, {silent: true});
            }
        });
        paymentModel.on('change:planetaPurchase', function (_, value) {
            if (value) {
                binModel.set('totalSum', Math.max(0, binModel.get('totalSum') - paymentModel.get('myBalance')));
            } else {
                binModel.updateTotalSum();
            }
        });

        var appModel = workspace.appModel;
        paymentModel.set({hasEmail: appModel.hasEmail(),
            myBalance: appModel.get('profileModel').get("myBalance"),
            email: appModel.get('profileModel').get('email')
        });

    });
}());

/*globals CampaignDonate, DonateUtils, ImageType, ImageUtils, StringUtils*/
/**
 *
 * @author Andrew.Arefyev@gmail.com
 * Date: 15.08.13
 * Time: 22:56
 */

CampaignDonate.Models.Share = BaseModel.extend({

    defaults: {
        isSelected: false,
        quantity: 1,
        questionToBuyer: "",
        reply: ''
    },

    idAttribute: 'shareId',
    initialize: function () {
        this.controller = this.get('controller') || this.collection.controller;
        var isCharity = this.controller.get('isCharity');

        var hasLimit = !_.isEqual(this.get('amount'), 0);
        this.set({
            hasLimit: hasLimit,
            isAllSold: hasLimit && _.isEqual(this.get('amount'), this.get('purchaseCount')),
            sharesLeft: this.get('amount') - this.get('purchaseCount'),
            isDonate: _.isEqual(this.get('price'), 0),
            isCharity: _.isBoolean(isCharity) && isCharity
        }, {silent: true});
    },

    extendShare: function () {
        var shares = this.controller.shares;
        this.set('donateAmount', this.controller.get('donateAmount'));
        this.set('isFirst', shares.at(0).get('shareId') == this.get('shareId'));
        this.set('isLast', shares.at(shares.size() - 1).get('shareId') == this.get('shareId'));
    },

    isEnoughQuantity: function (quantity) {
        var result = (this.get('isDonate') || !this.get('hasLimit')) && (quantity > 0);
        if (!result) {
            result = !this.get('isAllSold') && (quantity > 0) && (quantity <= this.get('sharesLeft'));
        }

        return result;
    },

    isEnoughAmount: function (amount) {
        var totalPrice = this.get('isDonate') ? 1 : this.getTotalPrice();
        return (amount >= totalPrice);
    },

    tryChangeQuantity: function (quantity) {
        var canChange = this.isEnoughQuantity(quantity);
        var totalPrice = quantity * this.get('price');
        canChange = canChange && (totalPrice == DonateUtils.parseDonateAmount(totalPrice));
        if (canChange) {
            this.set('quantity', quantity);
        }

        return canChange;
    },

    getTotalPrice: function () {
        return this.get('quantity') * this.get('price');
    },

    getDataToShare: function () {
        var imageUrl = this.get('imageUrl') || this.controller.get('imageUrl');
        var description = this.get('description');
        var title = 'Вознаграждение "' + this.get('name') + '" проекта "' + this.controller.get('name') + '"';
        if (description) {
            description = StringUtils.cutString(description, 164);
        } else {
            description = title;
        }
        return {
            title: title,
            image: ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.ORIGINAL, ImageType.PRODUCT),
            description: description,
//            url: location + '#',
            ogDescription: description
        };
    },
    setChecked: function () {
        this.set({
            isSelected: true,
            slide: true
        }, {silent: true});
        var donateThisShareUrl = '/campaigns/' + this.controller.get('webCampaignAlias') +
            (/donatesingle/.test(location.href) ? '/donatesingle' : '/donate') + ((this.get('isDonate') ? '' : '/' + this.get('shareId')));
        Backbone.history.navigate(donateThisShareUrl, {silent: true, replace: true});
        workspace.appView.pageDataObject.update(this.getDataToShare());
        if (workspace && workspace.stats) {
            workspace.stats.trackPageView(donateThisShareUrl);
        }
    },
    setUnchecked: function () {
        this.set({
            isSelected: false
        }, {silent: true});
    }
});

CampaignDonate.Models.ShareCollection = BaseCollection.extend({
    model: CampaignDonate.Models.Share,

    initialize: function (models, options) {
        this.options = options || {};
        BaseCollection.prototype.initialize.call(this, models, options);
        //that is the usage?
        this.dfd = $.Deferred();
        if (options.controller) {
            this.controller = options.controller;
        } else {
            console.error('"controller" in  CampaignDonate.Models.ShareCollection is required.');
        }
    },

    reset: function (models, options) {
        var donateShare, selectedShare;
        BaseCollection.prototype.reset.apply(this, arguments);
        donateShare = this.getDonateShare();
        if (donateShare) {
            donateShare.set('price', this.getShareMinPrice());
        }
        selectedShare = this.getShareById(this.controller.get('selectedShareId') || 0) || donateShare || this.at(0);
        if (!this.trySelect(selectedShare)) {
            console.error('no share selected'); // if (!true) or something become broken
        }
    },

    getShareMinPrice: function () {
        return 100; // https://planeta.atlassian.net/browse/PLANETA-18087
    },

    getDonateShare: function () {
        return this.find(function (share) {
            return share.get('isDonate');
        });
    },

    getSelectedShare: function () {
        return this.find(function (share) {
            return share.get('isSelected');
        });
    },
    trySelect: function (share) {
        if (!share) {
            return false;
        }
        var canSelect = !share.get('isSelected') && !share.get('isAllSold');
        if (canSelect) {
            var selectedShare = this.getSelectedShare();
            if (selectedShare) {
                selectedShare.setUnchecked();
            }
            share.setChecked();
            this.controller.set({
                selectedShare: share
            }, {silent: true});
            this.controller.pageData = share.getDataToShare();
        }

        return canSelect;
    },

    getShareById: function (shareId) {
        return this.find(function (share) {
            return (share.id == shareId);
        });
    }

});
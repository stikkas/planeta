var ProductStorages = {};

ProductStorages.Model = BaseModel.extend({

    idAttribute: 'index',
    defaults: {
        productId: 0
    },

    url: function() {
        return '/product.json?productId=' + this.get('productId');
    },

    initialize: function(options) {
        this.set({
            index: this.get('productAttribute').index
        }, {silent: true});
    },

    parse: function(resp, xhr) {
        return resp && resp.result || resp || {};
    },

    saveTotalQuantity: function() {
        var self = this;
        var totalQuantity = this.get('totalQuantity');

        if (this.get('productId')) {
            return this.updateProductQuantity(totalQuantity).done(function() {
                if (self.controller) {
                    self.controller.trigger('change:quantity', self, totalQuantity);
                }
            });
        } else { // FOR new children(attributes) without fetch
            if (self.controller) {
                self.controller.trigger('change:quantity', self, totalQuantity);
            }
            return $.Deferred().resolveWith(this, [{totalQuantity: totalQuantity}]);
        }
    },

    /**
     *
     * @param productQuantity
     * @returns {*}
     */
    updateProductQuantity: function(productQuantity) {
        var options = {
            url: '/admin/product-update-quantity.json',
            method: 'update',
            data: {
                productId: this.get('productId'),
                quantity: productQuantity
            }
        };
        return this.fetchX(options).done(function() {
            workspace.appView.showSuccessMessage('Количество товаров успешно изменено');
        }).fail(function(_1, errorMessage) {
            workspace.appView.showErrorMessage(errorMessage);
        });
    }
});

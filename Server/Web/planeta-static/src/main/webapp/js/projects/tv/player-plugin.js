(function ($) {
    var $this = {};
    var events = {};
    var adPlayer = null;
    var player = {};

    $.fn.planetaPlayer = function (options) {
        $this = this;
        if (options.event && events[options.event.name]) {
            events[options.event.name](options.event.params);
            return;
        }
        detector = Modernizr;

        player = videoPlayerResolver(options);

        adPlayer = advertisingPlayerResolver(options);

        if (adPlayer != null) {
            adPlayer.startPlay();
        } else {
           player && player.play();
        }
    };
    //only for code complection
    var detector = {
        canvas: false,
        canvastext: false,
        localstorage: false,
        sessionstorage: false,
        touch: false,
        webgl: false,
        websockets: false,
        video: {
            h264: "",
            ogg: "",
            webm: ""
        },
        audio: {
            m4a: "",
            mp3: "",
            ogg: "",
            wav: ""
        }
    };

    function isMobile() {
        return /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent);
    }

    function isAdAvailable() {
        return typeof google != "undefined";
    }

    function advertisingPlayerResolver(options) {
        if (!options.xmlUrl && !options.advertisingConfig){
            return null;
        }

        if (isMobile()) {
            if (window.ga) {
                ga('send', 'event', 'tv-ad', "mobile_device_not_suported", options.broadcastId);
            } else {
                console.error("google analitics not loaded, advertising stats cant send", "player-plugin.js:81");
            }
            return null;
        }

        if (CookieProvider.get(Htmp5PlayerControls.cookiePrefix + options.broadcastId)) {
            return null;
        }

        /*        if (!isAdAvailable()) {
         ga('send', 'event', 'tv-ad', "ima_not_loaded", options.broadcastId);
         return null;
         }*/

        var _player = null;
        if (detector.video.h264 != "") {
            _player = new Html5AdPLayer(options, $this, player);
        } else {
            if (window.ga) {
                ga('send', 'event', 'tv-ad', "html5_h264_not_supported", options.broadcastId);
            } else {
                console.error("google analitics not loaded, advertising stats cant send", "player-plugin.js:81");
            }
            //todo add flash advertising player
        }

        return _player;
    }

    function unEscape(string) {
        return string.replace(/(&lt;)/g, "<")
            .replace(/(&gt;)/g, ">")
            .replace(/(&amp;)/g, "&")
            .replace(/(&#034;)/g, "\"")
            .replace(/(&#039;)/g, "'");
    }

    function extractScriptTags(string) {
        var div = document.createElement('div');
        div.innerHTML = string;
        div.style.height = "100%";
        var scripts = div.getElementsByTagName('script');
        var i = scripts.length;

        var result = {
            libScripts: [],
            initScripts: []
        };
        while (i--) {
            //unshift to correct order
            if (scripts[i].src) {
                result.libScripts.unshift(scripts[i]);
            } else {
                result.initScripts.unshift(scripts[i]);
            }
            scripts[i].parentNode.removeChild(scripts[i]);
        }
        result.html = div;
        return result;
    }

    function videoPlayerResolver(options) {
        var player = $.extend({}, ParentPlayer);
        if (options.embedPlayer) {
            options.embedPlayer = unEscape(options.embedPlayer);
            var extracted = extractScriptTags(options.embedPlayer);
            player.$html = $(extracted.html);
            player.$html.hide();

            player.initScript = extracted.initScripts;

            player.scriptLoader = new ScriptLoader(extracted.libScripts);

            var innerPlayer = player.$html.find("planeta-player");
            if (innerPlayer.length > 0) {
                var attr = innerPlayer.find("planeta-attr");
                var attrObj = {};
                for (var i = 0; i < attr.length; i++) {
                    var $attr = $(attr[i]);
                    var name = $attr.attr("name");
                    var value = $attr.attr("value");
                    if ("true" === value || "false" === value) {
                        attrObj[name] = "true" === value;
                    } else {
                        attrObj[name] = value;
                    }
                }
                if (!attrObj["url"]) {
                    throw "attribute src not exists or invalid value";
                }

                if (isMobile() && attrObj["url"].indexOf("rtmp") != -1) {
                    attrObj["url"] = attrObj["url"].replace("rtmp", "http") + "/playlist.m3u8";
                }

                if (attrObj["mobileUrl"] && isMobile()) {
                    attrObj["url"] = attrObj["mobileUrl"];
                }

                if (attrObj["onlyAudio"]) {
                    if (attrObj["useFlash"]) {
                        useFlash = !isMobile();
                    } else {
                        var useFlash = (!isMobile() && detector.audio.mp3 == "") || attrObj["url"].indexOf("rtmp") != -1;
                    }

                    //todo after implement new sound api for html5 remove use flash conditions
                    var useEqualizer = !!attrObj["equalizer"] && useFlash;
                    attrObj = $.extend(attrObj, {
                        modelChanged: function (e) {
                            player[e.name] && player[e.name](e.data);
                        },
                        useFlash: useFlash,
                        useEqualizer: useEqualizer
                    });

                    player = $.extend(player, PlayerAdapter.planetaRadioPlayer);

                    var template = $("#html5player");
                    var imageUrl = attrObj["imageUrl"] || "";
                    player.$html = $.tmpl(template, $.extend(
                        {
                            isLive: true,
                            is360: false,
                            isRadio: true,
                            useEqualizer: false,//todo make dynamic equalizer show
                            displayText: !!attrObj["textUrl"] ? "display:none" : "",
                            imageUrl: imageUrl,
                            allowFullScreen: false//disable fullscreen button.
                        },
                        Htmp5PlayerControls
                    ));

                    if (options.xmlUrl || options.advertisingConfig) {
                        attrObj['autoplay'] = false;
                    }

                    player["videoEl"] = new RadioPlayer(attrObj);

                    player.$html.find("." + Htmp5PlayerControls.controls.playPauseButton).click(function () {
                        player.play();
                    });

                    player.$html.find("#" + Htmp5PlayerControls.volumeIcons.id).click(function () {
                        player.videoEl.toggleMuted();
                    });

                    player.$html.find("#" + Htmp5PlayerControls.volumeBar.volumeBarId).parent().click(function (e) {
                        var maxW = +$(this).css("width").replace("px", "");
                        var val = (e.offsetX / (maxW / 100)).toFixed();
                        player.setVolume(val);
                    });

                    player.$html.hide();
                    player.deferred.resolveWith(player);
                }


            } else {
                if (/krpano/.test(options.embedPlayer)) {
                    player = $.extend(player, PlayerAdapter.krpano);
                } else if (/jwplayer/.test(options.embedPlayer)) {
                    player.videoEl = player.$html.find("#player")[0];
                    player = $.extend(player, PlayerAdapter.jwplayer);
                }
            }

            $this.append(player.$html);
            if (!player.initScript.length) {
                player.pause();
            }
            return player;
        } else {
            return null;
            //todo add default player
        }
    }

    events.resize = function (param) {
        player && player.resize(param.w, param.h);
        adPlayer && adPlayer.resize(param.w, param.h);
    };
}(jQuery) );

var ScriptLoader = function (scripts) {
    var loadingTimeOut = 5000;
    var deferred = $.Deferred();
    var count = 0;
    var head;
    var failCounter = 0;
    var init = function (scripts) {
        var res = document.getElementsByTagName("head");
        if (res.length) {
            head = res[0];
            if (!scripts.length) {
                deferred.resolve();
            } else {
                var indx = 0;
                count = scripts.length;
                while (indx < scripts.length) {
                    var script = scripts[indx++];
                    var temp = document.createElement("script");
                    temp.setAttribute("type", "text/javascript");
                    temp.setAttribute("src", script.src);
                    temp.async = 'true';
                    temp.onload = onSuccessCallback;
                    temp.onerror = onErrorCallback;
                    head.appendChild(temp);
                }
                setTimeout(onTimeOut, loadingTimeOut);
            }
        } else {
            if (++failCounter <= 10) {
                setTimeout(function () {
                    init(scripts);
                }, 100);
            } else {
                deferred.reject();
            }
        }
    };

    var onTimeOut = function () {
        if (count) {
            onErrorCallback();
        }
    };

    var onSuccessCallback = function () {
        if (--count <= 0) {
            deferred.resolve()
        }
    };

    var onErrorCallback = function () {
        deferred.reject();
    };

    init(scripts);
    return deferred.promise();
};

ParentPlayer = {
    deferred: $.Deferred(),
    currentTime: 0,//google ima require this field
    init: function () {
        this._beforeInit && this._beforeInit();
        var scripts = this.initScript;
        this.initScript = null;
        var indx = 0;
        try {
            while (indx < scripts.length) {
                eval(scripts[indx++].innerHTML);
            }
        } catch (e) {
            console.error(e);
            throw e;
        }
        this._afterInit && this._afterInit();
        this.deferred.resolveWith(this);
    },
    play: function () {
        if (this.initScript && this.initScript.length) {
            var self = this;
            this.scriptLoader.done(function () {
                self.init();
            });
        }

        if (this.$html[0].children.length) {
            this.$html.show();
        }

        if (this.deferred.isResolved()) {
            this._play && this._play();
        } else {
            this.deferred.done(function () {
                this._play && this._play();
            });
        }
    },
    pause: function () {
        if (this.deferred.isResolved()) {
            this._pause && this._pause();
        }
    },
    resize: function (w, h) {
        this._resize && this._resize(w, h);
    },
    modelChanged: function (event) {
        this[event.name] && this[event.name](event.data);
    }
};

//define _beforeInit and _afterInit methods if required
PlayerAdapter = {
    krpano: {
        _play: function () {
            this.videoEl && this.videoEl.call("plugin[get(videointerface_video)].play();");
        },
        _pause: function () {
            this.videoEl && this.videoEl.call("plugin[get(videointerface_video)].pause();");
        },
        _afterInit: function () {
            this.videoEl = this.$html.find("#krpanoSWFObject")[0];
        },
        _resize: function (w, h) {

        }
    },
    slon: {
        _play: function () {
        },
        _pause: function () {
        },
        _afterInit: function () {
        },
        _resize: function (w, h) {
        }
    },
    jwplayer: {
        _beforeInit: function () {
            this.initScript.sort(function (a, b) {
                return /jwplayer.key/.test(a.innerHTML) ? -1 : 1;
            });
        },
        _play: function () {
            jwplayer().play();
            /* Duplication because autoplay don't working otherwise.
             But you also must to set "autostart" parameter to "true" in code of player.*/
            jwplayer().play();
        },

        _pause: function () {
            jwplayer().pause();
        },
        _resize: function (w, h) {

        }
    },
    html5: {
        _resize: function (w, h) {
            $(this.videoEl).attr("width", w);
            $(this.videoEl).attr("height", h);
        },
        _play: function () {
            //player.videoEl.load();
            this.$html.show();
            this.videoEl.play();
        },
        _pause: function () {
            this.videoEl.pause();
        }
    },
    planetaRadioPlayer: {
        _resize: function (w, h) {
            this.videoEl.resize && this.videoEl.resize(w, h);
        },
        _play: function () {
            //player.videoEl.load();
            this.$html.show();
            this.videoEl.playPause();
        },
        _pause: function () {
            this.videoEl.playPause();
        },
        setVolume: function (val) {
            this.videoEl.changeVolume(+val);
        },
        onVolumeChanged: function (val) {
            val = +val;
            if (val >= 75) {
                this.$html.find("#" + Htmp5PlayerControls.volumeIcons.id).attr("class", Htmp5PlayerControls.volumeIcons.FULL);
            } else if (val > 25 && val < 75) {
                this.$html.find("#" + Htmp5PlayerControls.volumeIcons.id).attr("class", Htmp5PlayerControls.volumeIcons.MEDIUM);
            } else if (val <= 25) {
                this.$html.find("#" + Htmp5PlayerControls.volumeIcons.id).attr("class", Htmp5PlayerControls.volumeIcons.MIN);
            } else if (val == 0) {
                this.$html.find("#" + Htmp5PlayerControls.volumeIcons.id).attr("class", Htmp5PlayerControls.volumeIcons.NONE);
            }
            this.$html.find("#" + Htmp5PlayerControls.volumeBar.volumeBarId).css("width", val + "%");
        },
        onPause: function () {
            this.$html.find("." + Htmp5PlayerControls.controls.playPauseButton + " span").removeClass(Htmp5PlayerControls.controls.pauseButtonClass);
        },
        onPlay: function () {
            this.$html.find("." + Htmp5PlayerControls.controls.playPauseButton + " span").addClass(Htmp5PlayerControls.controls.pauseButtonClass);
        },
        onTextReceived: function (text) {
            if (!!text) {
                this.$html.find("." + Htmp5PlayerControls.displayTextClass).show();
                this.$html.find("." + Htmp5PlayerControls.displayTextClass).text(text);
            }
        }
    }
};

var Html5AdPLayer = function (options, container, contentPlayer) {
    var self = this;
    this.manager = null;
    this.resize = function (w, h) {
        this.manager.resize(w, h);
    };

    this.show = function () {
        this.$html.show();
    };

    this.complete = function () {
        this.$html.hide();
    };
    this.enableSkip = function () {
        var skipAd = function () {
            self.manager.skip();
        };

        this.$html.find("." + Htmp5PlayerControls.ad.skipAdButton).click(skipAd);
        this.$html.find("." + Htmp5PlayerControls.ad.skipAdButton).parent().show();
        this.$html.find("#" + Htmp5PlayerControls.ad.timeToSkip).parent().hide();
    };
    this.changeAdSkipTime = function (time) {
        this.$html.find("#" + Htmp5PlayerControls.ad.timeToSkip).text(time);
    };

    this.setDurationBarSize = function (size) {
        this.$html.find("." + Htmp5PlayerControls.ad.durationBar).css("width", size + "%");
    };

    this.onLoaded = function (canSkip) {
        if (canSkip) {
            this.$html.find("#" + Htmp5PlayerControls.ad.timeToSkip).parent().show();
        }

        this.$html.find("." + Htmp5PlayerControls.ad.clickOnMe).click(function () {
            self.$html.find("#" + Htmp5PlayerControls.ad.clickOnMe).click();
        });
    };

    this.init = function () {
        var template = $("#" + options.html5AdvertisingPlayerTmpl);

        this.$html = $.tmpl(template, $.extend(
            {
                timeToSkipValue: Math.floor(Htmp5PlayerControls.adSkipTime * 0.001),
                buttonText: options.advertisingConfig.buttonText || "Подробнее"
            },
            Htmp5PlayerControls.ad
        ));
        this.iframeContainer = this.$html.find("#" + Htmp5PlayerControls.ad.adFrame)[0];
        this.videoEl = this.$html.find("#" + Htmp5PlayerControls.ad.advertisingVideoPlayer)[0];
        this.onClickElement = this.$html.find("#" + Htmp5PlayerControls.ad.clickOnMe)[0];
        this.$html.hide();
        container.append(this.$html);
        //this.manager = new AdManager(options, this, contentPlayer);
        this.manager = new PlanetaAdManager(options, this, contentPlayer);
    };

    this.startPlay = function () {
        this.manager.play();
    };

    this.init();
};

Htmp5PlayerControls = {
    volumeIcons: {
        NONE: "icon-vol-loud",
        MIN: "icon-vol-min",
        MEDIUM: "icon-vol-quiet",
        FULL: "icon-vol-max",
        id: "js-volume-icon"
    },
    controls: {
        playPauseButton: "js-play-button",
        playButtonClass: "icon-play",
        pauseButtonClass: "icon-pause"
    },
    volumeBar: {
        volumeBarId: "js-volume-bar"
    },
    displayTextClass: "js-display-text",
    ad: {
        advertisingVideoPlayer: "js-advertising-video-player",
        clickOnMe: "js-ad-click-area",
        adFrame: "js-ad-iframe",
        skipAdButton: "js-skip-ad-button",
        timeToSkip: "js-time-to-skip",
        durationBar: "js-duration-bar"
    },
    adSkipTime: 5000,
    cookiePrefix: "tv-ad-"
};

PlanetaAdManager = function (params, adPlayer, contentPlayer) {

    var broadcastId = params.broadcastId;

    this.play = function(){

    };
    this.resize = function(){

    };

    function sendGA(eventType) {
        if (!!window.ga) {
            console.log(eventType);
            ga('send', 'event', 'tv-ad', eventType, broadcastId);
        } else {
            console.error("google analitics not loaded, advertising stats cant send", "player-plugin.js:558");
        }
    }

    var init = function () {
        var xmlDoc;
        if (window.DOMParser) {
            var parser = new DOMParser();
            xmlDoc = parser.parseFromString(params.advertisingConfig.vastXml, "text/xml");
        }
        else // Internet Explorer
        {
            xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.async = false;
            xmlDoc.loadXML(params.advertisingConfig.vastXml);
        }
        var mediaFile = xmlDoc.getElementsByTagName("MediaFile")[0];
        console.log(mediaFile);

        var clickEl = xmlDoc.getElementsByTagName("ClickThrough")[0];
        console.log(clickEl);

        var fileUrl = mediaFile.innerHTML || mediaFile.textContent;
        var redirectUrl = clickEl.innerHTML || clickEl.textContent;

        contentPlayer.pause();

        var redirectFunc = function () {
            //here 'js-ugly-hack-to-open-new-tab-from-js
            sendGA("clicked");
            var a = $('a[target="_blank"]')[0];
            var attr = a.getAttribute("href");
            a.setAttribute('href', redirectUrl);
            a.click();
            a.setAttribute('href', attr);
            endedFunction(false);
        };

        $("#" + Htmp5PlayerControls.ad.clickOnMe).click(redirectFunc);

        $("." + Htmp5PlayerControls.ad.skipAdButton).click(function () {
            endedFunction(true);
            sendGA("skiped");
        });

        var endedFunction = function (startPlay) {
            adPlayer.videoEl.pause();

            adPlayer.complete();
            contentPlayer.$html.show();
            if (startPlay) {
                contentPlayer.play();
            } else {
                contentPlayer.init();
            }
        };

        var $videoEl = $(adPlayer.videoEl);

        $videoEl.bind("ended", function () {
            endedFunction(true);
            sendGA("end_play");
        });

        $videoEl.bind("error", function () {
            endedFunction(true);
        });

        $videoEl.bind("canplay", function () {
            adPlayer.onLoaded(true);
            adPlayer.show();
            adPlayer.videoEl.play();

            adDuration = adPlayer.videoEl.duration;
            if (!/localhost/i.test(window.location.hostname)) {
                var cookieExpriesTime = new Date(new Date().getTime() + (3 * 60 * 60 * 1000)).toUTCString();
                CookieProvider.set(Htmp5PlayerControls.cookiePrefix + broadcastId, true, {expires: cookieExpriesTime});
            }

            sendGA("start_play");
        });

        var canSkip;
        var adDuration;

        $videoEl.bind("timeupdate", function () {
            var duration = Math.floor(adPlayer.videoEl.currentTime * 1000);
            if (!canSkip && duration >= Htmp5PlayerControls.adSkipTime) {
                canSkip = true;
                adPlayer.enableSkip();
            } else {
                if (!canSkip) {
                    var timeToSkip = Math.floor((Htmp5PlayerControls.adSkipTime + 1 - duration) * 0.001);
                    adPlayer.changeAdSkipTime(timeToSkip);
                }
            }
            adPlayer.setDurationBarSize((duration * 0.001) / (adDuration * 0.01));
        });

        $videoEl.attr("autoplay", "autoplay");
        $videoEl.attr("src", fileUrl);

        //adPlayer.videoEl.load();
    };

    init();
};

AdManager = function (params, adPlayer, contentPlayer) {
    var adsManager;
    var canSkip = false;
    var broadcastId;
    var deferred = $.Deferred();
    var intervalTimer;
    this.skip = function () {
        adsManager.skip();
    };

    this.resize = function (w, h) {
        if (deferred.state() == "resolved") {
            adsManager.resize(w, h, google.ima.ViewMode.NORMAL)
        } else {
            deferred.done(function () {
                adsManager.resize(w, h, google.ima.ViewMode.NORMAL)
            });
        }
    };

    this.play = function () {
        if (deferred.state() == "resolved") {
            adsManager.start();
        } else {
            deferred.done(function () {
                adsManager.start();
            });
        }
    };

    function onAdsManagerLoaded(adsManagerLoadedEvent) {
        var adsRenderingSettings = new google.ima.AdsRenderingSettings();
        adsRenderingSettings.restoreCustomPlaybackStateOnAdBreakComplete = true;
        // Get the ads manager.
        adsManager = adsManagerLoadedEvent.getAdsManager({}, adsRenderingSettings);  // See API reference for contentPlayback

        // Add listeners to the required events.
        adsManager.addEventListener(google.ima.AdErrorEvent.Type.AD_ERROR, onAdError);
        adsManager.addEventListener(google.ima.AdEvent.Type.CONTENT_PAUSE_REQUESTED, onContentPauseRequested);
        adsManager.addEventListener(google.ima.AdEvent.Type.CONTENT_RESUME_REQUESTED, onContentResumeRequested);
        adsManager.addEventListener(google.ima.AdEvent.Type.ALL_ADS_COMPLETED, onAdEvent);

        // Listen to any additional events, if necessary.
        adsManager.addEventListener(google.ima.AdEvent.Type.LOADED, onAdEvent);
        adsManager.addEventListener(google.ima.AdEvent.Type.STARTED, onAdEvent);
        adsManager.addEventListener(google.ima.AdEvent.Type.COMPLETE, onAdEvent);
        adsManager.addEventListener(google.ima.AdEvent.Type.SKIPPED, onAdEvent);
        adsManager.addEventListener(google.ima.AdEvent.Type.CLICK, onAdEvent);
        adsManager.addEventListener(google.ima.AdEvent.Type.FIRST_QUARTILE, onAdEvent);
        adsManager.addEventListener(google.ima.AdEvent.Type.MIDPOINT, onAdEvent);
        adsManager.addEventListener(google.ima.AdEvent.Type.THIRD_QUARTILE, onAdEvent);
        /* adsManager.addEventListener(google.ima.AdEvent.Type.SKIPPABLE_STATE_CHANGED, onAdEvent);*/


        try {
            // Initialize the ads manager. Ad rules playlist will start at this time.
            adsManager.init(100, 500, google.ima.ViewMode.NORMAL);
            // Call play to start showing the ad. Single video and overlay ads will
            // start at this time; the call will be ignored for ad rules.
            deferred.resolve();
        } catch (adError) {
            // An error may be thrown if there was a problem with the VAST response.
            deferred.reject();
        }
    }


    this.init = function (params) {
        var adsLoader;
        broadcastId = params.broadcastId;
        // Create the ad display container.
        var adDisplayContainer = new google.ima.AdDisplayContainer(adPlayer.iframeContainer, adPlayer.videoEl, adPlayer.onClickElement);
        // Initialize the container, if requestAds is invoked in a user action.
        // This is only needed on iOS/Android devices.
        adDisplayContainer.initialize();
        // Create ads loader.
        adsLoader = new google.ima.AdsLoader(adDisplayContainer);
        // Listen and respond to ads loaded and error events.
        adsLoader.addEventListener(google.ima.AdsManagerLoadedEvent.Type.ADS_MANAGER_LOADED, onAdsManagerLoaded, false);
        adsLoader.addEventListener(google.ima.AdErrorEvent.Type.AD_ERROR, onAdError, false);


        // Request video ads.
        var adsRequest = new google.ima.AdsRequest();
        if (params.advertisingConfig) {
            adsRequest.adsResponse = params.advertisingConfig.vastXml;
        } else {
            adsRequest.adTagUrl = params.xmlUrl;
        }

        // Specify the linear and nonlinear slot sizes. This helps the SDK to
        // selectCampaignById the correct creative if multiple are returned.
        adsRequest.linearAdSlotWidth = 640;
        adsRequest.linearAdSlotHeight = 400;

        adsRequest.nonLinearAdSlotWidth = 640;
        adsRequest.nonLinearAdSlotHeight = 150;

        adsLoader.requestAds(adsRequest);
    };

    function onAdEvent(adEvent) {
        // Retrieve the ad from the event. Some events (e.g. ALL_ADS_COMPLETED)
        // don't have ad object associated.
        if (!!window.ga) {
            ga('send', 'event', 'tv-ad', adEvent.type, broadcastId);
        } else {
            console.error("google analitics not loaded, advertising stats cant send", "player-plugin.js:493");
        }
        var ad = adEvent.getAd();
        switch (adEvent.type) {
            case google.ima.AdEvent.Type.LOADED:
                // This is the first event sent for an ad - it is possible to
                // determine whether the ad is a video ad or an overlay.
                if (ad.isLinear()) {
                    // Position AdDisplayContainer correctly for overlay.
                    // Use ad.width and ad.height.
                    var adDuration = ad.getDuration() || 0;
                    adPlayer.onLoaded(ad.isSkippable());
                }
                break;
            case google.ima.AdEvent.Type.STARTED:
                adPlayer.show();
                // This event indicates the ad has started - the video player
                // can adjust the UI, for example display a pause button and
                // remaining time.
                if (ad.isLinear()) {
                    if (!adDuration) {
                        adDuration = Math.floor(+$("#" + Htmp5PlayerControls.ad.advertisingVideoPlayer)[0].duration);
                    }
                    var startTime = new Date().getTime();
                    if (!/localhost/i.test(window.location.hostname)) {
                        var cookieExpriesTime = new Date(startTime + (3 * 60 * 60 * 1000)).toUTCString();
                        CookieProvider.set(Htmp5PlayerControls.cookiePrefix + broadcastId, true, {expires: cookieExpriesTime});
                    }
                    // For a linear ad, a timer can be started to poll for
                    // the remaining time.
                    intervalTimer = setInterval(function () {
                        var now = new Date().getTime();
                        var duration = now - startTime;
                        if (ad.isSkippable() && !canSkip && duration >= Htmp5PlayerControls.adSkipTime) {
                            canSkip = true;
                            adPlayer.enableSkip();
                        } else {
                            if (!canSkip) {
                                var timeToSkip = Math.floor((Htmp5PlayerControls.adSkipTime + 1 - duration) * 0.001);
                                adPlayer.changeAdSkipTime(timeToSkip);
                            }
                        }
                        adPlayer.setDurationBarSize((duration * 0.001) / (adDuration * 0.01));
                    }, 300); // every 300ms
                }
                break;
            case google.ima.AdEvent.Type.SKIPPED:

            case google.ima.AdEvent.Type.COMPLETE:
                // This event indicates the ad has finished - the video player
                // can perform appropriate UI actions, such as removing the timer for
                // remaining time detection.
                if (ad.isLinear()) {
                    clearInterval(intervalTimer);
                }
                adPlayer.complete();
                contentPlayer.play();
                break;
        }
    }

    function onAdError(adErrorEvent) {
        // Handle the error logging.
        console.log(adErrorEvent.getError());
        var skipDestroy = false;
        if (!skipDestroy) {
            adsManager.destroy();
            adPlayer.complete();
            contentPlayer.play();
        }
    }

    function onContentPauseRequested() {
        contentPlayer.pause();
        // This function is where you should setup UI for showing ads (e.g.
        // display ad timer countdown, disable seeking etc.)
        // setupUIForAds();
    }

    function onContentResumeRequested() {

        contentPlayer.play();
        // This function is where you should ensure that your UI is ready
        // to play content. It is the responsibility of the Publisher to
        // implement this function when necessary.
        // setupUIForContent();
    }

    this.init(params);
};
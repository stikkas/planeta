/*globals FileUploader, CrowdFund, moduleLoader, FileUploadStatuses, FileUploadModel, Attach, Modal*/
CrowdFund.Views.ImageField.LoadView = BaseView.extend({
    template: '#campaign-load-image-field-template',
    modelEvents: {
        'destroy': 'dispose'
    },
    events: {
        'click .js-link-to-image': 'onLinkToImageClick',
        'click .js-link-to-video': 'onLinkToVideoClick'
    },

    uploadVideoParams: {
        uploadPath: '/uploadVideo',
        fileType: 'video',
        uploadSettings: {
            file_upload_limit: 1,
            file_post_name: 'userfile1',
            file_size_limit: "1024 MB",
            file_types: "*.3g2;*.3gp;*.asf;*.avi;*.flv;*.mj2;*.mp4;*.mpeg;*.mpg;*.mpeg1video;*.mpeg2video;*.mpegvideo;*.psp;*.rm;*.swf;*.vob;*.wmv;*.mov",
            file_types_description: "Video Files"
        }
    },

    uploadImageParams: {
        uploadPath: '/uploadImage',
        fileType: 'image',
        uploadSettings: {
            file_upload_limit: 1,
            file_post_name: 'userfile1',
            file_size_limit: '10 MB',
            file_types: '*.png;*.jpg;*.jpeg;*.gif',
            file_types_description: 'Image files'
        }
    },

    initialize: function () {
        BaseView.prototype.initialize.apply(this, arguments);
        moduleLoader.loadModule('upload');
    },

    dispose: function () {
        CrowdFund.Views.ImageField.Drag.finish();
        this.removeFileUploader();
        BaseView.prototype.dispose.apply(this, arguments);
    },

    afterRender: function () {
        var self = this;
        CrowdFund.Views.ImageField.Drag.start(this.$(".js-drag-zone"));
        moduleLoader.loadModule('upload').done(function () {
            self.fileUploader = self.getFileUploader();
        });
    },

    getFileUploader: function () {
        return new FileUploader(this.getFileUploaderOptions());
    },

    removeFileUploader: function () {
        this.fileUploader.cancel(this.file);
    },

    getFileUploaderOptions: function () {
        return {
            dropZone: this.$(".js-drag-zone"),
            container: this.$(".js-input-file"),
            listener: this,
            isOneFileMode: true
        };
    },

    onDrop: function (e) {
        CrowdFund.Views.ImageField.Drag.reset();
        try {
            var dt = e.originalEvent.dataTransfer;
            var link = dt.getData('text/uri-list');
            if (!link) {
                link = dt.getData('text');
                if (link) {
                    if (!StringUtils.isAbsoluteUrl(link)) {
                        link = 'http://' + link;
                    }
                }
            }
            if (link) {
                this.onLinkAdded(link);
            }
        } catch (ignore) {
        }
    },

    onAdd: function (file) {
        var self = this;

        file = file || {};
        this.file = file;
        var isImage = file.isImage = file.type.indexOf('image') === 0;
        var isVideo = file.isVideo = file.type.indexOf('video') === 0;
        var canUploadVideo = this.model.get('video');
        if (!(isImage || canUploadVideo && isVideo)) {
            workspace.appView.showErrorMessage(canUploadVideo ? 'Можно загружать только картинки и видео' : 'Можно загружать только картинки');
            return;
        }
        var uploadSettings = isImage ? this.uploadImageParams.uploadSettings : this.uploadVideoParams.uploadSettings;
        var msg;
//        if (this.isOneFileMode() && this.fileUploadListCollection.length > 1) {
//            msg = "Вы пытаетесь закачать слишком много файлов.\nВы можете выбрать только один файл.";
//        } else
        if (!FileUploader.isGoodFileExtention(file.name, uploadSettings.file_types)) {
            msg = "Вы пытаетесь закачать файл неподходящего типа: " + file.name;
        } else if (file.size > FileUploader.parseFileLimitSize(uploadSettings.file_size_limit)) {
            msg = "Вы пытаетесь закачать слишком большой файл: " + file.name + ". Файл должен быть не более " + uploadSettings.file_size_limit;
        }

        if (msg) {
            self.model.set({'errors': {'imageUrl': msg}});
            workspace.appView.showErrorMessage(msg);
            return;
        }

        var fileUploadModel = new FileUploadModel({
            fileName: file.name,
            fileSize: file.size,
            uploadedSize: 0,
            file: file
        });

        file.fileUploadModel = fileUploadModel;

        fileUploadModel.set("status", FileUploadStatuses.PENDING);

        if (this.progressView) {
            this.fileUploader.cancel(this.progressView.model.get("file"));
            this.progressView.dispose();
        }

        this.progressView = this.createProgressView(fileUploadModel, this.fileUploader);
        this.addChildAtElement('.js-progress', this.progressView);
        this.$('.js-drag-zone').hide();
        this.$('.js-error').hide();

        if (isImage) {
            this.fileUploader.setOptions({
                url: 'https://' + workspace.staticNodesService.getStaticNode() + this.uploadImageParams.uploadPath,
                paramName: this.uploadImageParams.uploadSettings.file_post_name,
                headers: {
                    clientId: this.model.get('clientId'),
                    ownerId: this.model.get('profileId'),
                    albumId: this.model.get('albumId'),
                    albumTypeId: this.model.get('albumTypeId')
                }
            });
        } else {
            this.fileUploader.setOptions({
                url: 'https://' + workspace.staticNodesService.getStaticNode() + this.uploadVideoParams.uploadPath,
                //paramName: this.uploadVideoParams.uploadSettings.file_post_name,
                headers: {
                    clientId: this.model.get('clientId'),
                    ownerId: this.model.get('profileId')
                }
            });
        }
        this.fileUploader.send(file);
    },

    createProgressView: function (fileUploadModel, fileUploader) {
        return new CrowdFund.Views.ImageField.LoadView.Progress({
            model: fileUploadModel,
            fileUploader: fileUploader
        });
    },

    onProgress: function (file, loadedSize) {
        file.fileUploadModel.set("uploadedSize", loadedSize);
        if (loadedSize >= file.size) {
            file.fileUploadModel.set("status", FileUploadStatuses.FINISHED);
        }
    },

    onDone: function (file, result) {
        delete this.file;

        if (result.success) {
            file.fileUploadModel.set({
                uploadedSize: file.size,
                status: FileUploadStatuses.SAVED
            });
            if (file.isImage) {
                this.onImageUploadDone(result.result);
            } else {
                this.onVideoLoaded(result.result);
            }
        } else {
            this.onError(file);
            console.log(result.errorMessage);
        }
    },

    onError: function (file) {
        delete this.file;
        if (file.fileUploadModel) {
            file.fileUploadModel.set("status", FileUploadStatuses.ERROR);

        }
        if (this.progressView) {
            this.progressView.dispose();
        }
        this.$('.js-drag-zone').show();
        this.$('.js-error').hide();
    },

    onLinkAdded: function (link) {
        var self = this;
        var img = new Image();
        img.onload = function () {
            var options = {
                url: '/api/parse.json',
                data: {
                    message: link,
                    profileId: workspace.appModel.get('profileModel').get('profileId')
                }
            };
            Backbone.sync('update', this, options).done(function (result) {
                if (result && result.imageAttachments && result.imageAttachments[0]) {
                    var image = result.imageAttachments[0];
                    self.model.set({
                        imageUrl: image.url,
                        imageId: image.objectId,
                        loadedImage: {
                            imageUrl: image.url,
                            photoId: image.objectId
                        }
                    });
                } else {
                    self.showErrorMessageUnexpectedError();
                }
            }).fail(this.showErrorMessageUnexpectedError);
        };
        img.onerror = function () {
            if (self.model.get('video')) {
                self.tryLoadVideoByLink(link);
            } else {
                self.showErrorMessageBadFormat();
            }
        };
        img.src = link;
    },

    tryLoadVideoByLink: function (link) {
        var self = this;
        var options = {
            url: '/api/public/video-external.json',
            data: {
                url: link,
                profileId: workspace.appModel.get('profileModel').get('profileId')
            }
        };

        Backbone.sync('read', this, options).done(function (result) {
            if (result && result.videoUrl) {
                Backbone.sync('create', this, options).done(function (result) {
                    if (result && result.success) {
                        self.model.set({
                            loadedVideo: result.result,
                            imageId: result.result.imageId,
                            imageUrl: result.result.imageUrl,
                            videoUrl: result.result.videoUrl,
                            videoId: result.result.videoId,
                            videoType: result.result.videoType
                        });
                    } else {
                        self.showErrorMessageUnexpectedError();
                    }
                }).fail(self.showErrorMessageUnexpectedError);
            } else {
                self.showErrorMessageBadFormat();
            }
        }).fail(this.showErrorMessageBadFormat);
    },

    showErrorMessageUnexpectedError: function () {
        workspace.appView.showErrorMessage('Непредвиденная ошибка');
    },

    showErrorMessageBadFormat: function () {
        workspace.appView.showErrorMessage('Не удалось распознать формат');
    },

    onImageUploadDone: function (loadedImage) {
        var self = this;
        this.model.set({
            loadedImage: loadedImage
        });
        var img = new Image();
        img.onload = function () {
            self.onImageLoaded(img);
        };
        img.src = loadedImage.imageUrl;
    },

    onImageLoaded: function (img) {
        this.model.set({
            'errors': {}
        });

        var self = this;
        var aspectRatio = this.model.get('aspectRatio');
        var cropSize = this.model.get('cropSize');
        if (!cropSize) {
            cropSize = {
                x: 0,
                y: 0,
                w: img.width,
                h: img.height
            };
            if (aspectRatio && img.width / img.height > aspectRatio) {
                cropSize.w = Math.round(img.height * aspectRatio);
            } else {
                cropSize.h = Math.round(img.width / aspectRatio);
            }
            this.model.set({
                cropSize: cropSize
            }, {silent: true});
        }

        if (!aspectRatio || Math.abs(img.width / img.height - aspectRatio) < 0.01) {
            this.model.set({
                imageUrl: this.model.get("loadedImage").imageUrl,
                imageId: this.model.get("loadedImage").photoId
            });
            return;
        }

        var options = {
            url: '/api/profile/crop-image.html',
            data: {
                clientId: this.model.get('clientId'),
                ownerId: this.model.get('profileId'),
                albumTypeId: this.model.get('albumTypeId'),
                url: this.model.get('loadedImage').imageUrl,
                cropX: Math.round(cropSize.x),
                cropY: Math.round(cropSize.y),
                cropWidth: Math.round(cropSize.w),
                cropHeight: Math.round(cropSize.h)
            }
        };

        Backbone.sync('read', this, options).done(function (result) {
            if (result && result.success) {
                self.model.set({
                    croppedImage: result.result,
                    imageUrl: result.result.imageUrl,
                    imageId: result.result.photoId
                });
            } else {
                self.showErrorMessageUnexpectedError();
            }
        }).fail(this.showErrorMessageUnexpectedError);
    },

    onVideoLoaded: function (video) {
        this.model.set({
            loadedVideo: video,
            imageUrl: video.imageUrl,
            imageId: video.imageId,
            videoProfileId: video.profileId,
            videoId: video.videoId,
            videoType: video.videoType,
            videoUrl: video.videoUrl
        });
    },


    onLinkToImageClick: function () {
        var self = this;
        var model = new Attach.Models.PhotoSelect({
            selected: function (photo) {
                self.onImageUploadDone({
                    photoId: photo.id,
                    imageUrl: photo.value
                });
            },
            step: 'showPhotoLink'
        });
        var view = new Attach.Views.PhotoLink({
            model: model
        });
        Modal.showDialog(view);
    },

    onLinkToVideoClick: function () {
        var self = this;
        Attach.showExternalVideo({
            selected: function (result) {
                self.model.set({
                    imageUrl: result.value,
                    imageId: 0,
                    videoId: result.id,
                    videoProfileId: result.owner,
                    videoUrl: result.url,
                    videoType: "YOUTUBE",
                    loadedVideo: {
                        imageUrl: result.value,
                        imageId: 0,
                        videoId: result.id,
                        videoProfileId: result.owner,
                        videoUrl: result.url,
                        videoType: "YOUTUBE"
                    }
                });
            },
            addVideoToProfileId: self.model.get('profileId')
        });
    },


    onCancelLoadClick: function (file) {
        this.fileUploader.cancel(file);
        this.removeFileUploader();
        this.$('.js-drag-zone').show();
    }
});


CrowdFund.Views.ImageField.LoadView.Progress = BaseView.extend({
    template: '#campaign-load-image-field-progress-template',
    events: {
        'click .js-cancel': 'onCancelLoadClick'
    },
    onCancelLoadClick: function () {
        this.parent.onCancelLoadClick(this.model.get('file'));
        this.dispose();
    }
});

var RadioPlayerView = function (options) {
    var self = this;
    var init = function () {
        if (options.detector && options.detector.canHtml5) {

        } else {
            self.equalizerView = new EqualizerView({
                container: options.container
            })
        }
    };

    this.showPlay = function () {

    };

    this.showPause = function () {

    };

    this.draw = function (data) {
        this.equalizerView.draw(data);
    };


    init();
};

var EqualizerView = function (params) {
    this.x = 0;
    this.y = 0;
    this.colCount = 16;
    this.cellWidth = 10;
    this.cellHeight = 10;
    this.cellColor = '#00B0E0';
    this.backColor = '#000000';
    this.intervalSize = 1;

    var oldLen = [];

    var self = this;

    var init = function () {
        var i = -1;
        while (++i < self.colCount) {
            oldLen[i] = 0;
        }

        for (var key in params) {
            self[key] = params[key];
        }

        self.canvas = document.createElement("canvas");
        self.container.prepend(self.canvas);
        self.canvas.width = self.container.container.width();
        self.canvas.height = self.container.container.height();

        self.canvas.width = (self.cellWidth * self.colCount) + self.colCount * self.intervalSize;
        // 11 = cellInCol + 1;
        self.canvas.height = self.cellHeight * 11 * self.intervalSize;
        $(self.canvas).css("position", "absolute")
            .css("z-index", "1000")
            .css("left", 0)
            .css("top", 0);
        self.ctx = self.canvas.getContext('2d');
    };


    var clear = function () {
        self.ctx.fillStyle = self.backColor;
        self.ctx.fillRect(self.x, self.y, self.canvas.width, self.canvas.height);
    };

    this.draw = function (data) {
        clear();
        this.ctx.fillStyle = this.cellColor;
        var x = this.x;
        for (var i = 0; i < data.length; i++) {
            var len = Math.round(data[i] * 10);
            if (isNaN(len)) len = 0;

            if (oldLen[i] < len) {
                oldLen[i] = len;
            } else {
                oldLen[i] = Math.floor((oldLen[i] + len) / 2);
            }

            // 9 = cellInCol - 1; 10 = cellInCol
            var y = this.y + (this.cellHeight * 9) + 10 * this.intervalSize;

            for (var j = 0; j < oldLen[i]; j++) {
                this.ctx.fillRect(x, y, this.cellWidth, this.cellHeight);
                y -= this.cellWidth + this.intervalSize;
            }
            x += this.cellWidth + this.intervalSize;
        }

    };


    init();
};

var RadioPlayer = function (options) {
    //this.template = "#planetaRadioPlayer";
    this.container = null;
    this.url = null;
    this.swfFile = "/flash/internet-radio.swf";
    var bridgeName = "jsToFlashRadioBridge";
    this.soundVolume = 100;
    this.soundMuted = false;
    this.playerImpl = null;
    this.useFlash = true;
    var self = this;
    var init = function () {
        for (var key in options) {
            self[key] = options[key];
        }
        if (!self.url || self.url.length == 0) {
            throw "radio player options must contains url param";
        }
        var playerContainer = $("<div id='js-radio-core'></div>");
        $("body").append(playerContainer);
        playerContainer.hide();
        //self.$html = $.tmpl($(self.template));
        //self.container.append(self.$html);
        if (self.useFlash) {
            // For version detection, set to min. required Flash Player version, or 0 (or 0.0.0), for no version detection.
            var swfVersionStr = "11.1.0";
            // To use express install, set to playerProductInstall.swf, otherwise the empty string.
            var xiSwfUrlStr = "";
            var flashvars = {
                //url: "rtmp://stream-7.antenne.de/liveedge/rockantenne",
                url: self.url,
                startPlay: !!self.autoplay,
                bridgeName: bridgeName
            };
            var params = {};
            var attributes = {};

            window[flashvars.bridgeName] = new JavaScriptToFlashBridge({
                container: playerContainer,
                dispatchEvent: function (event) {
                    self.modelChanged(event);
                },
                useEqualizer: self.useEqualizer
            });
            self.playerImpl = window[flashvars.bridgeName];
            swfobject.embedSWF(self.swfFile, "js-radio-core",
                "0", "0",
                swfVersionStr, xiSwfUrlStr,
                flashvars, params, attributes, function (e) {
                    if (e.success) {
                        window[flashvars.bridgeName].init($(e.ref)[0]);
                    } else {
                        console.error("fail to initialize flash player");
                    }
                });
        } else {
            self.playerImpl = new Html5PAudioPlayer({
                container: playerContainer,
                dispatchEvent: function (event) {
                    self.modelChanged(event);
                },
                url: self.url,
                autoplay: !!self.autoplay
            })
        }

        self.soundVolume = +(StorageUtils.get("radio-player-sound-volume") || 100);
        self.soundMuted = !!StorageUtils.get("radio-player-is-muted");

        if (self.textUrl) {
            // call this func with data context and textPath param
            // return value of data object field at 'textPath' path
            // example:
            // object {
            //    a {
            //       b {
            //         c = "some text";
            //       }
            //    }
            // }
            //  textFromPath.call(object,"a.b.c") will return "some text"
            function textFromPath(path) {
                return eval("this." + path);
            }

            var updateText = function () {
                $.get(self.textUrl).done(function (data) {
                    //fall-through is normal for this code.
                    switch (self.textType) {
                        case "JSON":
                            data = JSON.parse(data);
                        case "PATH":
                            if (self.textPath.indexOf("+") != -1) {
                                //path maybe compless separeted by '+'
                                var path = self.textPath.split("+");
                                var separator = self.textSeparator || ": ";
                                var temp = textFromPath.call(data, path[0]);
                                for (var i = 1; i < path.length; i++) {
                                    temp += separator + textFromPath.call(data, path[i]);
                                }
                                data = temp;
                            }
                        default :
                        case "RAW":
                            self.modelChanged({name: "onTextReceived", data: data.toString()});
                            break;
                    }
                }).fail(function (e) {

                }).always(function () {
                    setTimeout(updateText, 2000);
                });
            };
            setTimeout(updateText, 2000);
        }
        if (self.soundMuted) {
            self.playerImpl.setVolume(0);
            StorageUtils.set("radio-player-is-muted", true);
        } else {
            StorageUtils.remove("radio-player-is-muted");
            self.playerImpl.setVolume(self.soundVolume);
        }
    };
    this.playPause = function () {
        this.playerImpl.playPause();
    };

    this.changeVolume = function (vol) {
        if (vol < 3) {
            vol = 0;
        } else {
            vol += 3;
            vol = Math.min(100, vol);
        }
        self.soundVolume = vol;
        StorageUtils.set("radio-player-sound-volume", vol);
        self.playerImpl.setVolume(vol);
    };

    this.toggleMuted = function () {
        if (this.soundMuted) {
            StorageUtils.remove("radio-player-is-muted");
            self.playerImpl.setVolume(self.soundVolume);
        } else {
            self.playerImpl.setVolume(0);
            StorageUtils.set("radio-player-is-muted", true);
        }
        this.soundMuted = !this.soundMuted;
    };

    init();
};

var Html5PAudioPlayer = function (options) {
    var self = this;

    var init = function () {
        for (var key in options) {
            self[key] = options[key];
        }

        self.video = document.createElement("video");
        var source = document.createElement("source");
        source.src = self.url;
        self.video.appendChild(source);
        options.container.append(self.video);

        self.video.onplay = function (e) {
            self.dispatchEvent({name: "onPlay"});
        };

        self.video.onpause = function (e) {
            self.dispatchEvent({name: "onPause"});
        };

        //todo move to upper abstract layer after implement getPlayState in flash player
        if (self.autoplay) {
            self.autoplayTryes = 0;
            var startPlay = function () {
                if (self.video.paused) {
                    if (++self.autoplayTryes <= 20) {
                        self.video.play();
                        setTimeout(startPlay, 100);
                    }
                }
            };
            startPlay();
        }
    };

    this.playPause = function () {
        //prevent autoplay on user input
        self.autoplayTryes = 21;

        if (this.video.paused)
            this.video.play();
        else
            this.video.pause();
    };

    this.setVolume = function (vol) {
        this.video.volume = vol * 0.01;
        this.dispatchEvent({name: "onVolumeChanged", data: vol});
    };

    this.useEqualizer = function (val) {
        //not implemented
    };

    init();
};

var FakeFlashObject = function (obj) {
    this.pool = [];
    var self = this;
    for (var key in obj) {
        if (isFunction(obj[key])) {
            this[key] = (function (k) {
                return function () {
                    var args = arguments;
                    self.pool.push(function () {
                        obj[k].call(obj, args);
                    })
                };
            })(key);
        }
    }
    this.run = function () {
        for (var i = 0; i < this.pool.length; i++) {
            this.pool[i]();
        }
    }
};

function isFunction(functionToCheck) {
    var getType = {};
    return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
}

var JavaScriptToFlashBridge = function (options) {
    this.ready = false;

    for (var key in options) {
        this[key] = options[key];
    }

    this.isReady = function () {
        console.log("isReady call " + this.ready);
        return this.ready;
    };

    this.onPlay = function () {
        console.log("onPlay call");
        this.dispatchEvent({name: "onPlay"});
        //this.interfaceObject.find(".js-play-pause-button").addClass("icon-pause");
        this.flashObject.setVolume(this.volume);
    };

    this.onPause = function () {
        console.log("onPause call");
        this.dispatchEvent({name: "onPause"});
    };

    this.onEqualizerData = function (data) {
        this.dispatchEvent({name: "onEqualizerData", data: data[0]});
    };

    this.onVolumeChanged = function (vol) {
        this.dispatchEvent({name: "onVolumeChanged", data: vol});
    };

    this.playerVersion = function (version) {
        console.log(version)
    };

    this.playPause = function () {
        this.flashObject.playPause();
    };

    this.setVolume = function (vol) {
        this.flashObject.setVolume(vol);
    };

    this.useEqualizer = function (val) {
        this.flashObject.disableSpectrum(!val);
    };

    this.onPlayerReady = function () {
        //call then player init complete
        var self = this;
        var finishInit = function () {
            if (!self.ready) {
                setTimeout(finishInit, 100);
            } else {
                var fake = self.flashObject;
                self.flashObject = self.readyFlash;
                $(self.readyFlash).css("visibility", "hidden");
                fake.run();
            }
        };
        finishInit();
    };

    this.init = function (flashObject) {
        this.readyFlash = flashObject;
        this.ready = true;
    };

    //this must be at end of constructor to valid clone fields
    this.flashObject = new FakeFlashObject(this);
};

JavaScriptToFlashBridge.SOUND_VOLUME_ICONS = {
    NONE: "icon-vol-loud",
    MIN: "icon-vol-min",
    MEDIUM: "icon-vol-quiet",
    FULL: "icon-vol-max"
};
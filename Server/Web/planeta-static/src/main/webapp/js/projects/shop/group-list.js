/**
 *  Adds group of views as one element. All binded to one inem om model collectoion,
 *  .nextGroupView & .prevGroupView fields of views in group pointers to each other in dual order.
 * User: Andrew Arefyev
 * Date: 09.08.12
 * Time: 22:59
 *
 */

var GroupListView = BaseListView.extend({

    clearItems: function() {
        _(this._itemViews).each(function(views) {
            _.each(views, function(view) {
                if (view && view.dispose) {
                    view.dispose();
                }
            });
        });
        this._itemViews = [];
    },

	onAdd: function(itemModel, collection, options) {
        try {
	        var views = [],
	            types = this.itemViewTypes;
	        for(var item in types){
		        views[item] = new types[item]({
		                            model: itemModel,
		                            controller: this.controller
		                    });
		        views[item].prevGroupView = views[item-1];
		        views[item-1] && (views[item-1].nextGroupView = views[item]);
	        }
	        var size = views.length;

			if (views[0] && options && options.at >= 0 && this._itemViews.length > 0) {

				for(var i in views)
					this._itemViews.splice(options.at * size + i, 0, views[i]);

                if (options.at == 0) {
                    var nextView = this._itemViews[options.at + size];
                    if (this.sortDirection == 'asc') {
	                    for(var i in views)
	                        $(nextView.el).before(views[i].el);
                    } else {
	                    for(var i =size-1;i>=0;i--)
                            $(nextView.el).after(views[i].el);
                    }
                } else {
                    var prevView = this._itemViews[options.at - 1];
                    if (this.sortDirection == 'asc') {
	                    for(var i =size-1;i>=0;i--)
	                        $(prevView.el).after(views[i].el);
                    } else {
	                    for(var i in views)
	                        $(prevView.el).before(views[i].el);
                    }
                }
            } else {
					this._itemViews.push(views);
		    }
	        var dfo = this.addChildAsync(views[0]);
	        for(var i = 1;i<size;i++)
		        this.addChildAsync(views[i]);

	        return dfo;
        } catch (ex) {
            if (Backbone.onRenderError) {
                Backbone.onRenderError(ex);
            }
            return ex;
        }
    }
});


/*global $,Theme,workspace*/
var WidgetsPage = {
    iframeBaseUrl: '',
    imgBaseUrl: '',
    shareId: 0,
    campaignId: 0, // default value
    affiliateId: 0, // default value
    activeWidget: null,

    init: function () {
        this.iframeBaseUrl = 'https://' + workspace.hosts.widgetsAppUrl + '/widgets/affiliate-campaign-widget.html?';
        this.imgBaseUrl = 'https://' + workspace.hosts.widgetsAppUrl + '/widgets/affiliate-campaign-widget-image?';
        var self = this;

        $(".js-color").click(function () {
            var $el = $(this);
            if ($el.is('.active')) {
                return false;
            }
            $('.js-color').toggleClass('active');
            var theme = Theme[$el.attr("data-color")];
            self.updateAllWidgets(theme);
            var type = $("li.active .js-render-type").attr("data-render-type");
            self.renderWidgetHtmlCode(type);
            return true;
        });

        $(".js-radio-input").change(function () {
            var $el = $(this);
            var parentDiv = $el.closest(".row");
            var iframe = parentDiv.find(".js-widget-iframe");
            self.shareId = $el.attr("data-share-id");
            self.updateWidget(iframe);
            self.renderWidgetHtmlCode();
        });

        this.shareId = $('[data-share-id]:first').attr('data-share-id');

        this.updateAllWidgets(Theme.WHITE);

        $('.modal-widget-nav-item').click(function () {
            $('.modal-widget-nav-item').removeClass('active');
            var $this = $(this);
            $this.addClass('active');
            if ($this.hasClass("js-built-in-sales")) {
                $(".affiliate-widgets-choice-options").hide();
            } else {
                $(".affiliate-widgets-choice-options").show();
            }
            var index = $this.index();
            widgetReset();
            $('.awc-list-pane').hide();
            $('.awc-list-pane:eq(' + index + ')').fadeIn(200);
        });

        $('.awc-item').click(function () {
            $('.awc-item.active').removeClass('active');
            $('.awc-item').hide();
            $(this).addClass('active').show();
            $('.awc-list-opt').removeClass('hide');
            self.activeWidget = $(this).find("iframe");
            var type = self.activeWidget.attr("data-widget-type");
            if (type && type === "CAMPAIGN_240X400_WITH_SHARE") {
                $(".js-shares-container").show();
            }

            var type = $("li.active .js-render-type").attr("data-render-type");
            self.renderWidgetHtmlCode(type);
            return false;
        });

        $('.awc-head-link').click(function () {
            widgetReset();
            return false;
        });


        var widgetReset = function () {
            $('.awc-item.active').removeClass('active');
            $('.awc-list-opt').addClass('hide');
            $('.awc-item').show();
            $(".js-shares-container").hide();
        };

        $('.js-stream-create-a').click(function () {
            $('#modal-stream-create').modal();
        });

        $('.widget-share-textarea, .aw-action-list-textarea').click(function () {
            $(this).select();
        });

        $('.js-stream-create').click($.proxy(this.createSource, this));

        $("#js-stream-list").change(this.updateWidgetCodeOnSourceChange);

        $(".js-render-type").click(function () {
            var type = $(this).attr("data-render-type");
            self.renderWidgetHtmlCode(type);
        });

        self.initShowPreview(null);
    },

    updateWidgetCodeOnSourceChange: function () {
        var type = $("li.active a.js-render-type").attr("data-render-type");
        page.renderWidgetHtmlCode(type);
    },

    renderWidgetHtmlCode: function (type) {
        if (!this.activeWidget) return;
        if(!type){
            type = this.widgetType || 'iframe';
        }
        this.widgetType = type;
        var html = '';
        var data = this.getDataForWidget(this.activeWidget);
        if (type === 'iframe') {
            html = $('<iframe frameborder="0"/>');
            html.attr('width', this.activeWidget.attr('width'));
            html.attr('height', this.activeWidget.attr('height'));
            html.attr('src', this.iframeBaseUrl + $.param(data));
        } else {
            html = $('<a target="_blank"></a>');
            var query = {affiliate: this.affiliateId};
            if (data.source && data.source !== 0) {
                query.source = data.source;
            }
            html.attr('href', 'https://' + workspace.hosts.mainHost + '/campaigns/' + this.campaignId + '?' + $.param(query));
            html.append($('<img/>').attr('src', this.imgBaseUrl + $.param(data)));
        }
        var parent = $("<div></div>");
        parent.append(html);
        $("#js-iframe-html").text(parent.html().replace(/&amp;/gi, "&").trim());
    },

    updateAllWidgets: function (theme) {
        var self = this;
        $(".js-widget-iframe").each(function () {
            var $el = $(this);
            self.updateWidget($el, theme);
        });
    },

    updateWidget: function (iframe, theme) {
        iframe.attr("src", this.iframeBaseUrl + $.param(this.getDataForWidget(iframe, theme)));
    },

    getDataForWidget: function (iframe, theme) {
        var self = this;
        theme = theme || Theme[$(".js-color.active").attr("data-color")];
        var source = $('#js-stream-list').val();
        source = source === undefined ? 0 : +source;
        var data = {
            'name': iframe.attr("data-widget-type"),
            'campaign-id': self.campaignId,
            'affiliate-id': self.affiliateId,
            'background': theme.background,
            'font-color': theme.font
        };
        if (source) {
            data.source = source;
        }
        if (self.shareId) {
            data['share-id'] = self.shareId;
        }
        return data;
    },

    createSource: function (name) {
        var self = this;
        var input = $('#js-input-source-name');
        $.ajax('/create-source.json', {type: 'post', dataType: 'json', data: {sourceName: input.val()}}).done(function (resp) {
            var option = $('<option></option>').text(resp.result.name).attr('value', resp.result.sourceId);
            $('#modal-stream-create').modal('hide');
            $('#js-stream-list').prepend(option);
            $('#js-stream-list').val(resp.result.sourceId);
            input.val('');
            self.updateWidgetCodeOnSourceChange();
        });
    },
    initShowPreview: function (random) {
        random = random || 1234567890 - Math.floor(Math.random() * 10000);
        var id = "js-planeta-buy-share-button" + random;
        TemplateManager.tmpl("#affiliate-builtin-sales-button-template", {
            campaignId: this.campaignId,
            id: id
        }).done(function (text) {
                try {
                    var cont = $(".js-preview-button-container");
                    cont.empty();
                    cont.append(text);
                    //get(0) - div with image
                    //get(1) - script with js-code
                    var html = text.get(0).innerHTML + "<script>" + text.get(1).innerHTML + "</script>";
                    $(".js-builtin-sales-textarea").text(html);
                    var prevLink = $(".js-show-preview");
                    prevLink.off("click");
                    prevLink.on("click", function(){
                        planeta.init(id);
                    });
                } catch (e) {
                    console.error(e);
                }
            }).fail(function (a, b) {
                console.error(a, b);
            });
    }
};

var Theme = {
    BLACK: {
        font: "WHITE",
        background: "BLACK"
    },
    WHITE: {
        font: "BLACK",
        background: "WHITE"
    }
};
/*global Form, Backbone, BaseModel*/
var Settings = {Models: {}};
Settings.Models.Password = BaseModel.extend({

    savePrivateInfo: function (data, successCallback) {
        data.profileId = this.get('profileModel').get('profileId');
        var options = {
            url: '/profile/user-save-password.json',
            data: data,
            context: this,
            success: function (response) {
                if (Form.isValid(response)) {
                    successCallback();
                }
            }
        };

        Backbone.sync('update', this, options);
    },
    pageData: function () {
        return {
            title: 'Смена пароля ' + this.get('profileModel').get('displayName')
        };
    }
});

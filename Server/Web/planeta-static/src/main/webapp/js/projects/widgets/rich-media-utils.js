/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 24.10.13
 * Time: 15:56
 */
jQuery.extend(RichMediaUtils, {
    bindHandlers: function (el) {
        for (var i in RichMediaUtils.TAGS) {
            var self = this;
            var tagName = RichMediaUtils.TAGS[i];
            var collection = el.find('[data-role=' + tagName + ']');
            var mediaCollection = [];
            collection.each(function () {
                var $this = $(this);
                var item = self.parseObject($this.attr('data-json'));
                mediaCollection.push(item);

            });
            switch (tagName) {
                case 'photo':

                    break;
                case 'audio':
                    var playlist = [];
                    collection.each(function (index) {
                        var self = $(this);
                        var obj = mediaCollection[index];
                        var track = {
                            profileId: obj.owner,
                            trackId: obj.id,
                            artistName: obj.artist,
                            playlist: playlist,
                            trackDuration: obj.duration,
                            el: self
                        };
                        playlist.push(track);
                        self.unbind('click.playAudio').bind('click.playAudio', function (e) {
                            e.stopPropagation();
                            Player.onclick(track);
                        });
                    });
                    break;
                case 'video':
                    collection.each(function (index) {
                        var object = mediaCollection[index];
                        var self = $(this);
                        self.unbind('click.playVideo').bind('click.playVideo', function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            VideoPlayer.show(object.owner, object.id, self);
                        });
                    });
                    break;
            }
        }
    }
});
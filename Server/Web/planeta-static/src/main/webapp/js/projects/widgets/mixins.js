/**
 * Created with IntelliJ IDEA.
 * User: a.volkov
 * Date: 30.10.13
 * Time: 16:29
 */
var VideoPlayer = {
    elements: {},
    show: function (profileId, videoId, el) {
        this.elements[videoId] = el;
        this.play(videoId, profileId);
    },

    play: function (videoId, profileId) {
        if (!videoId || !profileId) {
            return;
        }
        var iframe = $('<iframe width="590" height="332" frameborder="0" allowfullscreen=""></iframe>');
        iframe.attr('src', '//' + workspace.hosts.tv + '/video-frame?profileId=' + profileId + '&videoId=' + videoId + '&autostart=true');
        this.elements[videoId].replaceWith(iframe);
    }
};


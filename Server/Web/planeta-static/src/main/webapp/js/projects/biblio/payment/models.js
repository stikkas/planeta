Biblio.Models.Method = UserPayment.Models.PaymentMethods.extend({
    parse: function (result) {
        var self = this;
        self.trigger('investInfo', result.investInfo);
        return UserPayment.Models.PaymentMethods.prototype.parse.call(this, result.methods);
    }

});

Biblio.Models.Payment = UserPayment.Models.OrderPayment.extend({
    methodsModel: Biblio.Models.Method,
    defaults: _.extend({}, UserPayment.Models.OrderPayment.prototype.defaults, {
            email: '',
            hasEmail: false,
            customerType: 'INDIVIDUAL',
            paymentMedthodId: 0,
            payeerPhone: ''
    }),
    initialize: function () {
        UserPayment.Models.OrderPayment.prototype.initialize.call(this, arguments);
        var self = this,
            methods = self.get('methods');
        methods.on('investInfo', function(investInfo) {
            methods.off('investInfo');
            self.set('investInfo', investInfo);
        });
    },
    get: function(fieldName) {
        var investInfo = this.attributes.investInfo;
        if (investInfo && _.contains(_.keys(investInfo), fieldName))
            return investInfo[fieldName];
        else 
            return UserPayment.Models.OrderPayment.prototype.get.call(this, fieldName);
    }
});

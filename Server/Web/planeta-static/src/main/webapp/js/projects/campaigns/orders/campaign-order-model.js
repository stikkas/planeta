/*globals Orders, Campaign, Order*/

var CampaignOrders = {};
CampaignOrders.Models = {};

CampaignOrders.Models.ReportDownloader = Orders.Models.BaseReportDownloader.extend({

    getDownloadUrl: function () {
        return '/admin/campaign-orders-report.html?' + this.getParams();
    }

});

CampaignOrders.Models.EmailReportDownloader = Orders.Models.BaseReportDownloader.extend({

    getDownloadUrl: function () {
        return '/admin/campaign-orders-report-to-email.json?' + this.getParams();
    }
});


CampaignOrders.Models.Actions = {
    IN_TRANSIT: {
        canChangeStatus: function (order) {
            return Order.Utils.isOrderInPaymentStatus(order, ['COMPLETED']) &&
                    Order.Utils.isOrderInDeliveryStatus(order, ['PENDING', 'DELIVERED']);
        },
        getOptions: function () {
            return {
                url: '/admin/batch-orders-complete.json',
                statuses: Order.Utils.getStatuses('COMPLETED', 'IN_TRANSIT'),
                messages: {
                    success: 'Заказ отправлен',
                    error: 'Ошибка смены статуса'
                }
            };
        }
    },

    DELIVERED: {
        canChangeStatus: function (order) {
            return Order.Utils.isOrderInPaymentStatus(order, ['COMPLETED']) &&Order.Utils.isOrderInDeliveryStatus(order, ['PENDING', 'IN_TRANSIT']);
        },
        getOptions: function () {
            return {
                url: '/admin/batch-orders-complete.json',
                statuses: Order.Utils.getStatuses('COMPLETED', 'DELIVERED'),
                messages: {
                    success: 'Заказ доставлен',
                    error: 'Ошибка смены статуса'
                }
            };
        }
    },

    CANCEL: {
        canChangeStatus: function (order) {
            return Order.Utils.isOrderInPaymentStatus(order, ['COMPLETED', 'CANCELLED']);
        },
        getOptions: function () {
            return {
                url: '/admin/batch-orders-cancel.json',
                statuses: Order.Utils.getStatuses('CANCELLED', 'CANCELLED'),
                messages: {
                    success: 'Продажа аннулирована',
                    error: 'Ошибка аннулирования продажи'
                }
            };
        }
    },

    GIVEN: {
        canChangeStatus: function (order) {
            return Order.Utils.isOrderInPaymentStatus(order, ['COMPLETED']) &&
                   Order.Utils.isOrderInDeliveryStatus(order, ['DELIVERED', 'PENDING']);
        },
        getOptions: function () {
            return {
                url: '/admin/batch-orders-complete.json',
                statuses: Order.Utils.getStatuses('COMPLETED', 'GIVEN'),
                messages: {
                    success: 'Продажа выполнена',
                    error: 'Ошибка смены статуса'
                }
            };
        }
    }
};

CampaignOrders.Models.Filter = Orders.Models.BaseFilter.extend({
    defaults: _.extend({}, Orders.Models.BaseFilter.prototype.defaults, {
        selectedShareId: 0
    }),
    getUrl: function () {
        return '/admin/campaign-orders-list.json?' + this.getParams();
    },

    setStatus: function (statusName, status) {
        var statuses = Order.Utils.getStatuses('ALL', 'ALL');
        this.set({status: status}, {silent: true});
        switch (status) {
            case 'NEW':
                statuses = Order.Utils.getStatuses('COMPLETED', 'PENDING');
                break;
            case 'COMPLETED':
                statuses = Order.Utils.getStatuses('COMPLETED', 'DELIVERED');
                break;
            case 'CANCELLED':
                statuses = Order.Utils.getStatuses('CANCELLED', 'CANCELLED');
                break;
            case 'GIVEN':
                statuses = Order.Utils.getStatuses('COMPLETED', 'GIVEN');
                break;
        }
        this.silentSetAttrs(statuses);
    },
    getParams: function () {
        var params = Orders.Models.BaseFilter.prototype.getParams.call(this);

        if (this.get("shareId")) {
            params += "&shareId=" + this.get("shareId");
        }
        if (this.get("cityAndCountrySearchString")) {
            params += "&cityAndCountrySearchString=" + encodeURIComponent(this.get("cityAndCountrySearchString"));
        }
        return params;
    }
});

CampaignOrders.Models.Controller = Campaign.Models.AbstractBaseController.extend({

    initialize: function () {
        this.filter = new CampaignOrders.Models.Filter({
            campaignId: this.get('campaignId')
        });
        this.downloader = new CampaignOrders.Models.ReportDownloader();
        this.downloader.controller = this;

        this.emaildownloader = new CampaignOrders.Models.EmailReportDownloader();
        this.emaildownloader.controller = this;

        Campaign.Models.AbstractBaseController.prototype.initialize.call(this);
    },

    getFilter: function () {
        return this.filter;
    },

    executeAction: function (actionName, order, options) {
        this.changeStatuses(actionName, [order], options);
    },

    batchExecuteAction: function (actionName, options) {
        this.changeStatuses(actionName, this.orders.getSelected(), options);
    },
    updateOrderState: function (info, model) {
        var action = this.getAction(info.orderState);
        if (!this.canChangeStatuses([model], action) && !model.get("isDeliveryInfoChanged")) {
            workspace.appView.showErrorMessage("Невозможно изменить статус", 3000);
        } else {
            $.post('/admin/update-order-state.json', info).done(function (response) {
                if (response && response.success) {
                    model.set("isDeliveryInfoChanged", false, {silent: true});
                    model.set(action.getOptions().statuses);
                    workspace.appView.showSuccessMessage("Статус заказа изменён", 1000);
                } else {
                    workspace.appView.showErrorMessage("Ошибка смены статуса", 3000);
                }
            });
        }
    },
    changeStatuses: function (actionName, orders, options) {
        var action = this.getAction(actionName);

        if (this.canChangeStatuses(orders, action)) {
            Campaign.Models.AbstractBaseController.prototype.changeStatuses.call(this, orders, _.extend(action.getOptions(), options));
        } else {
            workspace.appView.showErrorMessage("Невозможно изменить статус", 3000);
        }
    },

    sendEmail: function () {
        var orders = this.orders.getSelected();
        var mailAddress = 'admin@planeta.ru';
        var subject = 'Спонсорам проекта "' + this.get('campaignName') + '"';
        var bcc = '';
        _.each(orders, function (order) {
            if (bcc.indexOf(order.get('buyerEmail')) != -1) {
                return;
            }
            if (bcc) {
                bcc += ',';
            }
            bcc += order.get('buyerEmail');
        });
        var mailToLink = 'mailto:' + mailAddress + '?subject=' + subject + '&bcc=' + bcc;
        window.location.href = mailToLink;
    },

    getAction: function (actionName) {
        return Orders.hasProperty(CampaignOrders.Models.Actions, actionName) ? CampaignOrders.Models.Actions[actionName] : null;
    },

    canChangeStatuses: function (orders, action) {
        var hasAction = !_.isNull(action);
        var unchangeableOrder;

        if (hasAction) {
            unchangeableOrder = _.find(orders, function (order) {
                return !action.canChangeStatus(order);
            });
        }
        return hasAction && _.isUndefined(unchangeableOrder);
    },

    getShareDeliverySettings: function (shareId) {
        return this.has('sharesDeliverySettings') ? this.get('sharesDeliverySettings')[shareId] : null;
    },

    loadShareDeliverySettings: function (shareId) {
        var self = this;
        var dfd = $.Deferred();

        Backbone.sync('read', this, {
            url: '/admin/get-share-delivery-settings.json',
            data: {shareId: shareId},
            success: function (response) {
                if (response.success) {
                    dfd.resolve(shareId, response.result || {});
                } else {
                    dfd.reject(shareId, {});
                    if (response.errorMessage) {
                        workspace.appView.showErrorMessage(response.errorMessage);
                    }
                }
            }
        });

        return dfd.then(function (shareId, deliverySettings) {
            var sharesDeliverySettings = self.get('sharesDeliverySettings') || {};
            sharesDeliverySettings[shareId] = deliverySettings;
            self.set({sharesDeliverySettings: sharesDeliverySettings}, {silent: true});
        }).promise();
    },

    fetch: function (options) {
        options = options || {};
        var success = options.success;
        var self = this;
        var _options = _.extend({}, options, {
            success: function () {
                self.filter.set({'shares': self.shares.toJSON()});
                if (success) {
                    success();
                }
            }
        });

        return Campaign.Models.AbstractBaseController.prototype.fetch.call(this, _options);
    }
});

CampaignOrders.Models.Page = Campaign.Models.BasePage.extend({
    createContentModel: function () {
        return new CampaignOrders.Models.Controller({
            campaignId: this.get('objectId'),
            profileId: this.get('profileModel').get('profileId'),
//            place here initial values
            subsection: 'orders',
            affiliateProgramEnabled: false,
            profileModel: this.get('profileModel')
        });
    },
    pageData: function () {
        var campaignName = this.getCampaignModel().get('name');
        return {
            title: _.template('Продажи проекта «<%=campaignName%>» | Planeta', {
                campaignName: campaignName
            })
        };
    }
});
/*globals Campaign, Orders, PrivacyUtils*/
var CampaignAdvertisingTools = {};
CampaignAdvertisingTools.Models = {};

CampaignAdvertisingTools.Models.Targeting = Form.Model.extend({
    url: '/admin/campaign/campaign-targeting.json',
    defaults: {
        vkTargetingId: '',
        fbTargetingId: '',
        url: '/admin/campaign/campaign-targeting.json' // this is the url for form saving
    },
    initialize: function (options) {
        BaseModel.prototype.initialize.call(this, options);
    }
});

CampaignAdvertisingTools.Models.Page = BaseModel.extend({
    url: '/api/campaign/get-campaign.json',
    initialize: function () {
        BaseModel.prototype.initialize.apply(this, arguments);
        this.targeting = new CampaignAdvertisingTools.Models.Targeting({campaignId: this.get('objectId')});
    },
    prefetch: function (options) {
        options.data = {campaignAlias : this.get('objectId'),
            campaignId: this.get('objectId')
        };
        this.parallel([this, this.targeting], options);
    },
    pageData: function () {
        var campaignName = this.get('name');
        return {
            title: _.template('Рекламные инструменты «<%=campaignName%>» | Planeta', {
                campaignName: campaignName
            })
        };
    },
    parse: BaseModel.prototype.parseActionStatus
});
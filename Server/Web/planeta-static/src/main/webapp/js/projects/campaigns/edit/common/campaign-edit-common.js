/*global CampaignEdit, Form, CrowdFund, AlbumTypes, ImageUtils, ImageType, CampaignUtils, Campaign, DateUtils, Modal, StringUtils*/

CampaignEdit.Models.Common = CampaignEdit.Models.Tab.extend({
    defaults: _.extend({}, CampaignEdit.Models.Tab.prototype.defaults, {
        targetAmount: 10000
    })
});

CampaignEdit.Views.Common = BaseView.extend({
    className: 'col-9',
    template: '#campaign-edit-common-content-template',
    modelEvents: {
        destroy: 'dispose'
    },
    construct: function (options) {
        if (options && options.controller) {
            options.controller.pageData = this.pageData();
        }
        var self = this;
        this.cardImageModel = new CampaignEdit.Models.ShareImage({
            errors: this.model.get('errors'),
            originalImage: {
                imageUrl: this.model.get('imageUrl'),
                photoId: this.model.get('imageId')
            },
            profileId: this.model.get('profileId'),
            albumTypeId: AlbumTypes.ALBUM_COMMENT,
            thumbnail: {
                imageConfig: ImageUtils.MEDIUM,
                imageType: ImageType.PHOTO
            },
            aspectRatio: 220 / 134,
            width: 220,
            title: 'Картинка на витрину'
        });
        CampaignUtils.getCampaignTags().done(function (tags) {
            self.campaignTags = tags;
            self.init();
        });
    },
    init: function () {
        var attrs = {model: this.model};
        this.addChildAtElement('.project-create_list', new CampaignEdit.Views.Name(attrs));
        this.addChildAtElement('.project-create_list', new CampaignEdit.Views.CampaignAlias(attrs));
        this.addChildAtElement('.project-create_list', new CampaignEdit.Views.Image({
            model: this.cardImageModel,
            controller: this.model
        }));

        this.addChildAtElement('.project-create_list', new CampaignEdit.Views.ShortDescription(attrs));
        this.addChildAtElement('.project-create_list', new CampaignEdit.Views.Location(attrs));
        this.addChildAtElement('.project-create_list', new CampaignEdit.Views.Categories({
            model: this.model,
            campaignTags: this.campaignTags,
            dropdownCssClass: 'project-create-select2-drop',
            initTags: this.model.get('tags').length ? [] : // Используем тэги из запроса только при отсутствии в модели
                    workspace && workspace.urlQuery ? workspace.urlQuery.split('&') : []
        }));
        this.addChildAtElement('.project-create_list', new CampaignEdit.Views.TargetAmount(attrs));
        this.addChildAtElement('.project-create_list', new CampaignEdit.Views.TimeFinish(attrs));
    },
    pageData: function () {
        var L10n = {
            _dictionary: {
                "ru": {
                    "section": "Редактирование кампании",
                    "tabName": "основные данные"
                },
                "en": {
                    "section": "Campaign edit",
                    "tabName": "basic data"
                }
            }
        };

        var translate = function (word, lang) {
            if (!lang)
                throw "language is empty.";
            return L10n._dictionary[lang][word] || word;
        };

        var lang = window.workspace && (workspace.currentLanguage === "en") ? "en" : "ru";

        return {
            title: translate('section', lang) + ' - ' + translate('tabName', lang)
        }
    }
});

CampaignEdit.Views.TopCampaign = BaseView.extend({
    className: 'col-3',
    template: '#campaign-edit-top-campaign-template',
    afterRender: function () {
        StringUtils.hyphenate(this);
    }
});

CampaignEdit.Views.Name = CampaignEdit.Views.BaseInput.extend({
    template: '#campaign-edit-name-template',
    modelAttr: 'name',
    onInputChange: function () {
        CampaignEdit.Views.BaseInput.prototype.onInputChange.apply(this, arguments);
        var escapedName = $("<div></div>").text(this.model.get('name')).html();
        this.model.set('nameHtml', escapedName);

    }
});

CampaignEdit.Views.CampaignAlias = CampaignEdit.Views.BaseInput.extend({
    template: '#campaign-edit-campaign-alias-template',
    modelAttr: 'campaignAlias'
});

CampaignEdit.Views.Image = CampaignEdit.Views.ImageField.extend({
    modelAttr: 'imageUrl',
    afterRender: function () {
        if (this.controller.get('errors') && this.controller.get('errors')[this.modelAttr]) {
            this.$el.addClass('error');
        }
    }
});

CampaignEdit.Views.ShortDescription = CampaignEdit.Views.TextArea.extend({
    template: '#campaign-edit-short-description-template',
    modelAttr: 'shortDescription'
});

CampaignEdit.Views.Location = CampaignEdit.Views.BaseSelectAccordion.extend({
    template: '#campaign-edit-location-template',
    modelAttr: 'countryId',
    className: 'project-create_row',
    isExistsPlanetaUiElement: true,
    selectorToToggle: '[data-anchor=countrySelect]',
    afterRender: function () {
        if (this.model.get('regionId') !== 0) {
            this.$('[name=cityNameRus]').attr('disabled', false);
            this.$('[name=cityNameEng]').attr('disabled', false);
        }
    }
});

CampaignEdit.Views.Categories = Campaign.Views.Tags.extend({
    className: 'project-create_row',
    template: '#campaign-edit-categories-template',
    modelAttr: 'tags',
    construct: function (options) {
        this.model.set({
            useEng: !!(workspace && workspace.currentLanguage && workspace.currentLanguage === "en"),
            isCharity: this.isCharity(this.getCollection()),
            warnings: _.filter(_.map(this.getCollection(), function (tag) {
                return tag.warnText;
            }), function (text) {
                return text != null;
            })
        });
        this.campaignTags = options.campaignTags;
        if (options.initTags && options.initTags.length) {
            this.initTags = options.initTags;
        }
        if (options.dropdownCssClass) {
            this.dropdownCssClass = options.dropdownCssClass;
        }
    },
    isCharity: function (tags) {
        return _.contains(this.parse(tags, 'mnemonicName'), 'CHARITY');
    },
    getCollection: function () {
        return _.filter(this.model.get('tags'), function (obj) {
            return obj.editorVisibility || PrivacyUtils.hasAdministrativeRole();
        });
    },
    // Устанавливаем категории, которые прилетели с запросом
    // Если проект не относится ни к одной категории (это проверяется выше)
    initCollection: function () {
        var self = this;
        if (self.initTags) {
            var tagObjects = _.filter(_.map(self.initTags, function (tag) {
                return _.findWhere(self.campaignTags, {mnemonicName: tag});
            }), function(it) {return it;});
            if (tagObjects.length) {
                this.model.set({
                    isCharity: self.isCharity(tagObjects),
                    tags: this.model.get('useEng') ? _.sortBy(tagObjects, 'engName') : _.sortBy(tagObjects, 'name'),
                    warnings: _.filter(_.map(tagObjects, function (tag) {
                        return tag.warnText;
                    }), function (text) {
                        return text != null;
                    })
                });
            }
        }
    },
    setCollection: function (tags) {
        var tagObjects = this.tagsToObjects(tags);
        this.model.set({
            isCharity: this.isCharity(tagObjects),
            tags: tagObjects,
            warnings: _.filter(_.map(tagObjects, function (tag) {
                return tag.warnText;
            }), function (text) {
                return text != null;
            })
        });
    },
    tagsToObjects: function (tags) {
        var self = this;
        tags = _.sortBy(tags, function (value) {
            var checkValue = self.model.get('useEng') ? 'Charity' : 'Благотворительность';
            return (value == checkValue) ? 0 : 1;
        });
        var variants = this.getVariants();
        var self = this;
        return _.map(tags, function (tag) {
            if (self.model.get('useEng')) {
                return _.where(variants, {engName : tag}, true);
            } else {
                return _.where(variants, {name : tag}, true);
            }

        });
    },
    getVariants: function () {
        var collection = _.filter(this.campaignTags, function (obj) {
            return obj.editorVisibility || PrivacyUtils.hasAdministrativeRole();
        });
        var paramName = this.model.get('useEng') ? 'engName' : 'name';
        return _.sortBy(collection, paramName);
    },
    _setError: function (errorMessage) {
        var errors = this.model.get('errors') || {};
        errors.tags = errorMessage;
        this.model.set('errors', errors, {silent: true});
    },
    showError: function (errorMessage) {
        this._setError(errorMessage);
        this.render();
    },
    showCategoriesError: function (text) {
        var self = this;
        this.showError(text);
        setTimeout(function () {
            var tooltip = self.$('.pc_row_warning.tooltip');
            if (tooltip.length) {
                tooltip.twipsy();
                tooltip.data('twipsy').show();
                self._setError(null);
            }
        }, 200);
    },
    validateTags: function (tags) {
        var self = this;
        if (tags.length > 3 && !PrivacyUtils.hasAdministrativeRole()) {
            this.showCategoriesError('Проект не может иметь более 3 категорий');
            return false;
        }
        var tagObjects = this.tagsToObjects(tags);
        var isCharity = this.isCharity(tagObjects);
        var modelIsCharity = this.isCharity(this.model.get('tags'));
        if ((this.model.get('targetAmount') == 0 || this.model.get('timeFinish') == null) && !isCharity && modelIsCharity) {
            Modal.showConfirm("Неблаготворительный проект должен иметь финансовую цель и дату окончания",
                    'Удаление тега "Благотворительность"', {
                        cancel: function () {
                            tags.unshift('Благотворительность');
                            self.setCollection(tags);
                        },
                        success: function () {
                            // do nothing
                        }
                    });
        }
        if (_.filter(tagObjects, function (tag) {
            return tag.sponsorAlias != null;
        }).length > 1) {
            this.showCategoriesError('Вы не можете выбрать более одной спонсорской категории');
            return false;
        }
        var paramName = this.model.get('useEng') ? 'engName' : 'name';
        var intersection = _.intersection(tags, this.parse(this.getVariants(), paramName));
        return intersection.length == tags.length;
    },
    afterRender: function () {
        Campaign.Views.Tags.prototype.afterRender.apply(this, arguments);
        CampaignEdit.Views.CampaignValidatedRow.prototype.afterRender.apply(this, arguments);
        this.initCollection();
    }
});

CampaignEdit.Views.TargetAmount = CampaignEdit.Views.BaseInputOrCheckbox.extend({
    template: '#campaign-edit-target-amount-template',
    modelAttr: 'targetAmount',
    modelEvents: {
        destroy: 'dispose',
        'change:timeFinish,tags': 'render'
    },
    zeroValue: 0,
    defaultValue: 10000,
    onInputChange: function () {
        if (!this.model.get('notFirstChangeTargetAmount')) {
            this.model.set('notFirstChangeTargetAmount', true);
        }
        CampaignEdit.Views.BaseInputOrCheckbox.prototype.onInputChange.apply(this, arguments);
    }
});

CampaignEdit.Views.TimeFinish = CampaignEdit.Views.BaseInputOrCheckbox.extend({
    template: '#campaign-edit-time-finish-template',
    modelAttr: 'timeFinish',
    zeroValue: null,
    defaultValue: DateUtils.addDays(new Date().getTime(), 60),
    modelEvents: {
        destroy: 'dispose',
        'change:targetAmount,tags': 'render'
    },
    onInputChange: function () {
        if (!this.model.get('notFirstChangeTimeFinish')) {
            this.model.set('notFirstChangeTimeFinish', true);
            this.model.set('notFirstChangeTargetAmount', true);
        }
        var date = this.picker.datepicker('getDate');
        var time = null;
        if (date) {
            time = DateUtils.addHours(DateUtils.endOfDate(date.getTime()), -DateUtils.getHoursDiff());
        }
        this.model.set(this.modelAttr, time);
    },
    afterRender: function () {
        CampaignEdit.Views.BaseInputOrCheckbox.prototype.afterRender.apply(this, arguments);
        var picker;
        if (!PrivacyUtils.hasAdministrativeRole())
            picker = this.picker = this.getInput().datepicker({maxDate: "+99d", dateFormat: "dd.mm.yy",
                constrainInput: true});
        else
            picker = this.picker = this.getInput().datepicker();

        picker.on("change", function () {
            picker.datepicker("setDate", picker.datepicker("getDate"));
        });

        if (this.model.get('timeFinish')) {
            var date = DateUtils.addHours(this.model.get('timeFinish'), DateUtils.getHoursDiff());
            picker.datepicker('setDate', new Date(date));
        }
    }
});
/*globals workspace,BaseView,BaseModel,Search,DefaultScrollableListView,RegionAutocompleter, Blog, BlogUtils,Campaign,
 Products, Audios, moduleLoader, ShopUtils, DefaultListView, LazyHeader, StringUtils */
Search.Views.Query = BaseView.extend({
    className: 'project-list-search',

    events : {
        "input #query" : "save",
        "paste #query" : "save",
        "keypress #query" : "save"
    },

    save: _.debounce(function() {
        this.model.query.set({query: $('#query').val()}, {silent: true});
        this.model.searchResults.load();
        this.model.query.navigate(false);
    }, Search.INPUT_DELAY)
});

Search.Views.BaseResultList = DefaultListView.extend({
    construct: function(options) {
        this.itemTemplatePath = options.templatePath;
    },
    createItemView: function(itemModel) {
        var itemView = new this.itemViewType({
            model: itemModel,
            controller: this.controller,
            template: this.itemTemplatePath
        });
        return itemView;
    }
});


Search.Views.ResultItem = BaseView.extend({
    className: 'clearfix',
    events: {
        'click': 'onItemClick',
        'hover': 'onHover'
    },

    onItemClick: function(e) {
        if (!$(e.target).is('a')) {
            workspace.onClickNavigate(e, this.$el.find('a').attr('href'));
        }
    },

    onHover: function(e) {
    },

    construct: function (options) {
        if(options && options.template) {
            this.template = options.template;
        }
    },
    modelJSON: function() {
        var model = this.model;
        var json = this.model.toJSON();
        return _.extend({
            humanDuration: function (trackDuration) {
                return StringUtils.humanDuration(trackDuration);
            },
            getPostUrl: function() {
                return model.get('profileId') + '/blog/' + model.get('blogPostId');
            },
            getOwnerUrl: function() {
                return model.get('profileId');
            }
        }, json, json.profile);
    }
});


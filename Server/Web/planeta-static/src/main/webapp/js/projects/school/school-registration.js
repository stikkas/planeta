var SeminarRegistration = {
    Models: {
        sync: function(url, data) {
            var $dfd = $.Deferred();
            $.ajax(url, _.extend({
                type: 'POST',
                data: JSON.stringify(data),
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function(response) {
                    if (response.success) {
                        $dfd.resolve(response.result);
                    } else {
                        $dfd.reject(response.errorMessage, response.fieldErrors);
                    }
                }
            }));

            return $dfd.promise();
        }
    },
    Views: {}
};

SeminarRegistration.Models.Main = BaseModel.extend({
    defaults: {
        formatSelection: false,
        seminarRegistrationId: "",
        seminarId: 0,
        profileId: "",
        fio: "",
        phone: "",
        profileId2: "",
        fio2: "",
        phone2: "",
        city: "",
        campaignShortDescription: "",
        campaignTargetSum: "",
        organizationName: "",
        organizationLineOfAtivity: "",
        position: "",
        workPhone: "",
        organizationRegistrationDate: "",
        organizationSite: "",
        name:"",
        seminarFormat: "true"
    }
});

SeminarRegistration.Views.Main = Modal.OverlappedView.extend({
    viewType: "",
    viewEvents: {
        'click': 'onRegistrationClicked'
    },

    construct: function (options) {
        Modal.OverlappedView.prototype.construct.apply(this, arguments);
        this.callbacks = (options && options.callback) || {};
    },

    callback: function (callbackName) {
        var callback = this.callbacks[callbackName];
        if (_.isFunction(callback)) {
            callback(this.model, this);
        }
    },

    send: function () {
        var serializedForm = this.$('form').serializeObject();
        serializedForm.organizationRegistrationDate = new Date(serializedForm.year, serializedForm.month - 1, serializedForm.day).getTime();
        this.model.set(serializedForm, {silent: true});

        this.$('.error').removeClass('error');
        this.$('.error-message').remove();
        this.callback('send');
        if(window.gtm) {
            window.gtm.trackUniversalEvent('successful_crowdschool_request');
        }
    }
});

SeminarRegistration.Views.Solo = SeminarRegistration.Views.Main.extend({
    viewType: "SOLO",
    template: '#seminar-registration-view-solo-template'
});

SeminarRegistration.Views.SoloSimple = SeminarRegistration.Views.Main.extend({
    viewType: "SOLOSIMPLE",
    template: '#seminar-registration-view-solosimple-template'
});

SeminarRegistration.Views.Pair = SeminarRegistration.Views.Main.extend({
    viewType: "PAIR",
    template: '#seminar-registration-view-pair-template'
});

SeminarRegistration.Views.Webinar = SeminarRegistration.Views.Main.extend({
    viewType: "WEBINAR",
    template: '#seminar-registration-view-webinar-template'
});

SeminarRegistration.Views.Company = SeminarRegistration.Views.Main.extend({
    viewType: "COMPANY",
    template: '#seminar-registration-view-company-template'
});

SeminarRegistration.Views.Success = Modal.OverlappedView.extend({
    template: '#seminar-registration-view-success-template'
});
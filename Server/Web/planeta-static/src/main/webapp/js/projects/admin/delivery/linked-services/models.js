/*globals Delivery*/
/**
 *
 * @author Andrew
 * Date: 31.10.13
 * Time: 21:27
 */

Delivery.Models.BaseServicesSelictionList = BaseCollection.extend({
    initialize: function(models, options) {
        BaseCollection.prototype.initialize.apply(this, arguments);
        this.controller = options.controller;
    }
});
Delivery.Models.LinkedService = BaseModel.extend({
    url: '/admin/delivery/get-linked-service.json',
    defaults: {
        serviceId: 0,
        description: '',
        price: 0,
        isNew: false
    },
    idAttribute: 'serviceId',
    initialize: function(options) {
        if (this.get('isNew')) {
            this.baseServicesForSelection = new Delivery.Models.BaseServicesSelictionList([], {
                url: '/moderator/base-delivery-services.json',
                data: {
                    shareId: this.get('shareId')
                },
                controller: this
            });
//            this.baseServicesForSelection.load();
        } else {
            this.fetchX({
                data: {
                    subjectId: this.get('subjectId'),
                    serviceId: this.get('serviceId'),
                    subjectType: this.get('subjectType')
                }
            });
        }
    },
    update: function(_options) {
        var $dfd = $.Deferred();
        var data = this.toJSON();
        if (!data.serviceId) return $dfd.rejectWith(this, [null, {deliveryService: 'Обязательное поле'}]);
        delete data.location.parent;
        var options = {
            url: '/admin/delivery/update-linked-service.json',
            data: data,
            method: 'update',
            context: this,
            contentType: 'application/json'
        };
        _.extend(options, _options);
        return this.fetchX(options);
    },
    enable: function() {
        var data = this.toJSON();
        data.enabled = true;
        return this.update({
            data: data
        });
    },
    disable: function() {
        var data = this.toJSON();
        data.enabled = false;
        return this.update({
            data: JSON.stringify(data)
        });
    }

});


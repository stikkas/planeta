
Biblio.Collections.Partner = BaseCollection.extend({
    url: '/api/public/partners.json',
    model: Biblio.Models.Partner
});

Biblio.Collections.Advertise = BaseCollection.extend({
    url: '/api/public/advertises.json',
    model: Biblio.Models.Advertise
});
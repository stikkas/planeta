Biblio.Models.PaymentLegacy = BaseModel.extend({
	getData: function(data) {
		var result = null;
		if(this.get('show')) {
			result = JSON.stringify({
				userType: 1,
				phoneNumber: this.get('phoneNumber'),
				lastName: this.get('lastName'),
				firstName: this.get('firstName'),
				middleName: this.get('middleName'),
				bossPosition: this.get('bossPosition'),
				orgName: this.get('orgName'),
				orgBrand: this.get('orgBrand'),
				birthDate: data.birthDate,
				birthPlace: null,
				regIndex: this.get('regIndex'),
				regCountry: this.get('regCountry'),
				regCountryId: this.get('regCountryId'),
				regRegion: this.get('regRegion'),
				regRegionId: null,
			    regLocation: this.get('regLocation'),
			    regAddress: this.get('regAddress'),
			    liveIndex: this.get('placeFact')?this.get('regIndex'):this.get('liveIndex'),
			    liveCountry: this.get('placeFact')?this.get('regCountry'):this.get('liveCountry'),
			    liveCountryId: this.get('placeFact')?this.get('regCountryId'):this.get('liveCountryId'),
			    liveRegion: this.get('placeFact')?this.get('regRegion'):this.get('liveRegion'),
			    liveRegionId: null,
			    liveLocation: this.get('placeFact')?this.get('regLocation'):this.get('liveLocation'),
			    liveAddress: this.get('placeFact')?this.get('regAddress'):this.get('liveAddress'),
			    passportNumber: null,
			    passportIssuer: null,
			    passportIssueDate: null,
			    passportIssuerCode: null,
			    inn: this.get('inn'),
			    webSite: this.get('webSite')				
			});
		}
		return result;
	}
});

Biblio.Views.PaymentLegacy = BaseView.extend({
    isExistsPlanetaUiElement: true,
	template: '#payment-legacy-information',
	events: {
		'click .js-address-toggle': 'changeFact',
		'change input, textarea, select': 'updateModel'
	},
	changeFact: function() {		
        this.model.set('placeFact', !this.model.get('placeFact'));        
	},
	updateModel: function(e) {
		var target = this.$(e.target);
        this.model.set(target.prop('name'), target.val(), {silent: true});
	},
	checkData: function(callback) {
		var data = {};
		
		var self = this;
		self.$el.find('[name]').each(function () {
            var $this = $(this);
            data[$this.attr('name')] = $this.val();
        });
		
		data = this.model.getData(data);

		if(data!=null) {
			Backbone.sync('update', null, {
	            url: '/campaign/validate-invest-purchase.json',
	            data: data,
	            contentType: 'application/json'
	        }).done(function(response) {
	        	self.showErrorsEx(self.$el, response, '.pln-payment-box_field-row');
	            if (response.success) {
	            	callback.apply(self, [data]);
	            }
	        });
		} else {
            callback.apply(this, [data]);
		}
	}
});
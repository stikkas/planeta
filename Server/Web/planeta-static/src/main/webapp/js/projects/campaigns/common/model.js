/*globals ProfileUtils, PrivacyUtils, Orders*/
/**
 *
 * @author Andrew.Arefyev@gmail.com
 * Date: 04.04.13
 * Time: 21:18
 */
var Campaign = window.Campaign || {};
Campaign.Models = {
    createCampaign: function (ownerProfileId) {
        moduleLoader.loadModule('planeta-campaigns-edit').done(function () {
            CampaignEdit.Models.createCampaign(ownerProfileId);
        });
    }
};
Campaign.Views = Campaign.Views || {};

Campaign.Models.BaseCampaign = BaseModel.extend({
    url: '/api/campaign/campaign.json',
    initialize: function () {
        BaseModel.prototype.initialize.apply(this, arguments);
        this.shares = new BaseCollection(this.get('shares'));
        var response = this.setPermissionsSafe(this.attributes);
        this.set(response);
    },

    defaults: {
        tags: [],
        faqCount: 0,
        pendingStart: false
    },

    extendShare: function (shareAttrs, campaignAttrs) {
        campaignAttrs = campaignAttrs || this.attributes;
        return _.extend({}, shareAttrs, {
            notCharity: !(campaignAttrs.tags[0] && campaignAttrs.tags[0].mnemonicName === 'CHARITY'),
            showShareHover: campaignAttrs.showShareHover || false,
            campaignStatus: campaignAttrs.status,
            sponsorAlias: campaignAttrs.sponsorAlias
        });
    },

    extendShares: function (shares, campaignAttrs) {
        var self = this;
        campaignAttrs = campaignAttrs || this.attributes;
        if (shares.each) {
            shares.each(function (share) {
                share.set(self.extendShare(share.attributes, campaignAttrs));
            });
        } else {
            _(shares).each(function (share) {
                _.extend(share, self.extendShare(share, campaignAttrs));
            });
        }
    },

    parse: function (response) {
        if (response.success === false) {
            return {success: false};
        }
        if (this.shares) {
            this.extendShares(response.shares, response);
            this.shares.reset(response.shares);
            this.set({
                sharesCount: response.shares.length,
                shares: response.shares
            }, {silent: true});
        }

        response = this.setPermissionsSafe(response);

        return response;
    },

    setPermissionsSafe: function (response) {
        try {
            var hasAdministrativeRole = PrivacyUtils.hasAdministrativeRole();
            response.isModerationButtonsVisible = hasAdministrativeRole;
            response.canChangeCampaign = response.canChangeCampaign || hasAdministrativeRole;
            response.notDeleted = hasAdministrativeRole || PrivacyUtils.isAdmin();

            response.canSendNews = hasAdministrativeRole || (PrivacyUtils.isAdmin() && _.contains(['ACTIVE', 'PAUSED', 'FINISHED'], response.status));
            response.showOrdersButton = !hasAdministrativeRole && response.canSendNews;
            if (response.moderationMap) {
                var moderationMap = response.moderationMap[response.status];
                response.canStartCampaign = hasAdministrativeRole && !!(moderationMap && _.contains(moderationMap, 'ACTIVE'));
                response.canPauseCampaign = hasAdministrativeRole && !!(moderationMap && _.contains(moderationMap, 'PAUSED'));
            }
        } catch (ignore) {
            console.log(ignore);
        }
        return response;
    },

    /**
     * Start specified campaign
     * Returns deferred object
     */
    startCampaign: function (successCallback) {
        var $dfd = $.Deferred();
        if (this.get('pendingStart')) {
            $dfd.promise();
        }
        this.set('pendingStart', true);

        var data = {
            campaignId: this.get('campaignId')
        };
        var self = this;
        var options = {
            url: '/admin/campaign-start.json',
            data: data,
            context: this,
            success: function (response) {
                if (response.success === true) {
                    self.set({
                        status: 'ACTIVE',
                        pendingStart: false
                    });
                    if (workspace.appModel.get('myProfile').get('isAdmin') || PrivacyUtils.hasAdministrativeRole()) {
                        $dfd.resolveWith(this, ['Проект запущен']);
                    } else {
                        $dfd.resolveWith(this, ['Заявка на запуск проекта отправлена']);
                    }
                    if (successCallback) {
                        successCallback();
                    }
                } else {
                    var errorMessage = response.errorMessage || 'Ошибка запуска проекта';
                    $dfd.rejectWith(this, [errorMessage]);
                }
            },
            error: function (response) {
                $dfd.rejectWith(this, ['Ошибка запуска проекта']);
            }
        };

        Backbone.sync('update', this, options);

        return $dfd.promise();
    },

    fetch: function (options) {
        
        options = _.extend({}, options);
        options.data = options.data || {
            profileId: this.get('profileId'),
            objectId: this.get('campaignId')
        };
        return BaseModel.prototype.fetch.call(this, options);
    },

    /**
     * Pause specified campaign
     * Returns deferred object
     */
    pauseCampaign: function (reason, successCallback) {
        var $dfd = $.Deferred();
        var data = {
            campaignId: this.get('campaignId'),
            reason: reason
        };
        var options = {
            url: '/admin/campaign-pause.json',
            data: data,
            context: this,
            success: function (response) {
                if (response.success) {
                    this.set({
                        status: 'PAUSED'
                    });
                    if (successCallback) {
                        successCallback();
                    }
                    $dfd.resolveWith(this, ['Проект остановлен']);
                }
            },
            complete: function () {
                $dfd.rejectWith(this, ['Ошибка остановки проекта']);
            }
        };
        Backbone.sync('update', this, options);
        return $dfd.promise();
    },

    /**
     *
     * @param donateAmount amount
     * @param isManual is it manual input?
     * @returns {boolean} can change or not
     */
    tryChangeDonateAmount: function (donateAmount, isManual) {
        var canChange = !_.isEqual(donateAmount, this.get('donateAmount'));
        if (!canChange) {
            return false;
        }

        if (isManual === true) {
            this.silentSet('donateAmount', donateAmount, this);
            this.set('isUserAmount', true, {silent: true});
            return true;
        }

        var isOldUserAmount = this.get('isUserAmount') && (this.get('donateAmount') > donateAmount);
        this.set('isUserAmount', isOldUserAmount, {silent: true});

        canChange = !isOldUserAmount;
        if (canChange) {
            this.silentSet('donateAmount', donateAmount, this);
        }

        return canChange;
    },

    updateDonateAmount: function () {
        this.tryChangeDonateAmount(this.get('selectedShare').getTotalPrice());
    },

    switchShare: function (share, withReturn) {
        if (share.get('isSelected') && withReturn) {
            return;
        }
        if (!(this.shares.trySelect && this.shares.trySelect(share))) {
            return;
        }

        this.silentSet('selectedShare', share, this);
        this.updateDonateAmount();
        this.trackCampaignSocialTargeting();
    },
    silentSet: function (attrName, attrValue, context) {
        var attr = {};
        attr[attrName] = attrValue;
        context.set(attr, {silent: true});
        context.trigger('change:' + attrName, attrValue, context);
    },

    addNewPost: function () {
        //TODO: add post to post list if need (model stores at this.model.newPost field)
    },

    trackCampaignSocialTargeting: function () {
        try {
            if (this.get('campaignId')) {
                CampaignUtils.trackPixel(this.get('campaignId'));
            }
        } catch(ex){
        }
    }
});

/**
 * prepares models for breadcrumbs
 * @type {*}
 * @abstract
 */
Campaign.Models.BasePage = BaseModel.extend({
    hasCampaignModel: function () {
        return this.has('campaignModel');
    },
    /**
     * recommended to overwrite implementation in child
     * @returns {Campaign.Models.BaseCampaign}
     */
    createCampaignModel: function () {
        var attrs = {
            profileId: this.get('profileModel').get('profileId'),
            campaignId: this.get('objectId'),
            creatorProfile: this.get('profileModel')
        };
        return new Campaign.Models.BaseCampaign(attrs);
    },
    insertCampaignModel: function () {
        this.set({
            campaignModel: this.createCampaignModel()
        }, {silent: true});
    },
    getCampaignModel: function () {
        if (this.hasCampaignModel()) {
            return this.get('campaignModel');
        }

        this.insertCampaignModel();
        return this.get('campaignModel');
    },
    /**
     * check return value before usage
     * @returns {boolean| model}
     */
    //todo rename to getCreatorProfileModel
    getGroupModel: function () {
        return this.get('profileModel');
    },
    /**
     * strongly recommended to implement in childs
     * @returns {BaseModel}
     * @abstract
     * createContentModel: function() {},
     */
    _insertContentModel: function () {
        this.set({
            contentModel: this.createContentModel()
        }, {silent: true});
    },
    getContentModel: function () {
        if (this.has('contentModel')) {
            return this.get('contentModel');
        }

        this._insertContentModel();
        return this.get('campaignModel');
    },

    insertBreadcrumbsData: function () {
        if (this.get('objectId') !== 'create' && (!_.isEmpty(this.get('objectId')) || _.isNumber(this.get('objectId')))) {
            this.set({
                campaign: this.getCampaignModel().attributes
            }, {silent: true});
        }
    },
    initialize: function () {
        this.insertBreadcrumbsData();
        this._insertContentModel();
    },
    prefetch: function (options) {
        if (this.hasCampaignModel()) {
            return this.parallel([this.getCampaignModel(), this.getContentModel()], options);
        }
        return this.parallel([this.getContentModel()], options);
    }
});

Campaign.Models.AbstractBaseController = Campaign.Models.BaseCampaign.extend({
    defaults: {
        extraStatsEnabled: false
    },
    initialize: function () {
        Campaign.Models.BaseCampaign.prototype.initialize.call(this);
        this.getFilter().bind('filterChanged', this.onFilterUrlChange, this);
        this.ordersLoader = new BaseModel({
            filter: this.getFilter()
        });
        this.orders = new Orders.Models.OrderCollection();
        this.orders.controller = this;
    },

    onFilterUrlChange: function () {
        if (this.filter.get('aggregationType') === "DATE") {
            this.ordersLoader.set({loading: true});
            this.filter.set({loading: true});
        }
        this.orders.reset();
        this.loadOrders();
    },

    loadOrders: function() {
        var self = this;
        var errorFunc = function () {
            document.location.href = '/campaigns/' + self.get('campaignId');
        };
        var options = {
            success: function (response) {
                self.ordersLoader.set({loading: false});
                self.filter.set({loading: false});
            },
            error: function (response) {
                errorFunc();
            },
            complete: function () {
                self.ordersLoader.set({loading: false});
                self.filter.set({loading: false});
            }
        };
        self.ordersLoader.set({loading: true});

        this.orders.loading = false;
        return this.orders.load(options);
    },

    fetch: function (options) {
        var self = this;
        var oldOptions = _.clone(options);
        options.success = function (response) {
            self.orders.load({
                data: {
                    campaignId: self.get('campaignId')
                }
            });
            if (oldOptions && oldOptions.success) {
                oldOptions.success(response);
            }
        };
        Campaign.Models.BaseCampaign.prototype.fetch.call(this, options);
    },

    // must return filter reference
    getFilter: function () {
        return null;
    },

    changeStatuses: function (orders, options) {
        var data = {
            orderIds: _.map(orders, function (order) {
                return order.get('orderId');
            })
        };
        data = _.extend(data, options.statuses, {reason: _.isUndefined(options.reason) ? '' : options.reason}, {
            orderState: options.orderState,
            isSendNotification: _.isUndefined(options.isSendNotification) ? false : options.isSendNotification
        });

        var requestOptions = {
            url: options.url,
            data: data,
            type: 'post',
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    _.each(orders, function (order) {
                        order.set(options.statuses);
                    });
                    workspace.appView.showSuccessMessage(options.messages.success, 1000);
                } else {
                    workspace.appView.showErrorMessage(options.messages.error, 1000);
                }
            },
            error: function () {
                workspace.appView.showErrorMessage(options.messages.error, 1000);
            }
        };

        return $.ajax(requestOptions).promise();
    }
});

/**
 * @abstract
 */
Campaign.Views.BasePage = BaseView.extend({
    /*must be implemented
     contentViewClass: BaseView,*/
    construct: function () {
        this.addChild(new this.contentViewClass({
            model: this.model.getContentModel()
        }));
    }
});
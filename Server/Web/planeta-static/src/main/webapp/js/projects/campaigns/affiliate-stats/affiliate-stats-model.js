/*globals Orders,$,_,BaseCollection,Campaign,BaseModel,PrivacyUtils*/

var CampaignAffiliateStats = {};
CampaignAffiliateStats.Models = {};

CampaignAffiliateStats.Models.BaseFilter = Orders.Models.BaseFilter.extend({

    defaults: {
        timeFilter: 'yesterday',
        timeStart: DateUtils.addDays(DateUtils.startOfDate(DateUtils.getCurrentTime()), -1),
        timeFinish: DateUtils.addDays(DateUtils.endOfDate(DateUtils.getCurrentTime()), -1),
        paymentStatus: 'ALL',
        deliveryStatus: 'ALL',
        searchString: null,
        aggregationType: 'DATE',
        loading: false
    },

    getUrl: function() {
        return '/admin/campaign-stats-list.json?' + this.getParams();
    },

    getDateData: function() {
        var result = '';
        if (this.has('timeStart')) {
            result += '&dateFrom=' + DateUtils.formatDateSimple(this.get('timeStart'), 'YYYY-MM-DD');
        }
        if (this.has('timeFinish')) {
            result += '&dateTo=' + DateUtils.formatDateSimple(this.get('timeFinish'), 'YYYY-MM-DD');
        }
        return result;
    }
});

CampaignAffiliateStats.Models.ReportDownloader = Orders.Models.BaseReportDownloader.extend({

    getDownloadUrl: function() {
        return '/admin/campaign-stats-report.html?' + this.getParams();
    }

});

CampaignAffiliateStats.Models.TotalStats = BaseModel.extend({
    defaults: {
        aggregationType: 'DATE',
        visitors: 0,
        views: 0,
        purchases: 0,
        buyers: 0,
        comments: 0,
        amount: 0
    },

    url: function() {
        return '/admin/campaign-total-stats.json?' + this.get('filter').getParams();
    }
});

CampaignAffiliateStats.Models.Graph = BaseModel.extend({
    defaults: {
        graphInited: false,
        statsLength: 0,
        storageFirstGraphName : "affiliate-stat-first-graph",
        storageSecondGraphName : "affiliate-stat-second-graph",
        initParams:  {
            date:[],
            visitors: [],
            views:[],
            purchases:[],
            buyers:[],
            comments:[],
            amount:[]
        },
        firstGraph: {
            name: "Посетители",
            target: 'visitors',
            values: []
        },
        secondGraph: {
            name: "Куплено вознаграждений",
            target: 'purchases',
            values: []
        },
        graphOptions: {
            aggregationTarget: 'category',
            focusTarget: 'category',

            axisTitlesPosition: 'none',
            backgroundColor: '#fff',
            chartArea: {
                width: '100%',
                height: '75%'
            },
            legend: {
                position: 'top',
                textStyle: {
                    color: 'black',
                    fontSize: 14
                }
            },
            /*legend: 'none',*/
            lineWidth: 3,
            hAxis: {
                textStyle: {
                    fontSize: 11
                }
            },
            vAxis: {
                textPosition: 'in',
                minValue: 0,
                textStyle: {
                    fontSize: 11
                }
            }
        }
    },
    initialize: function (options) {
        this.firstGraph = this.defaults.firstGraph || options.firstGraph;
        this.secondGraph = this.defaults.secondGraph || options.secondGraph;
        this.currentFilter = options.currentFilter;
        this.controller = options.controller;
        this.statisticsList = new CampaignAffiliateStats.Models.StatisticsList();
        this.statisticsList.controller = this;
    },

    getFilter: function() {
        return this.controller.getFilter();
    },
    reload: function() {
        if (this.getFilter().get('aggregationType') == "DATE") {
            var self = this;
            this.statisticsList.load().done(function(){
                self.setGraphData();
            });
        } else {
            this.set({
                aggrTypeDate: false
            });
        }
    },

    setGraphData: function () {
        var list = this.statisticsList.models.reverse();
        var data = this.get('initParams');
        data.date = [];
        data.visitors = [];
        data.views = [];
        data.purchases = [];
        data.buyers = [];
        data.comments = [];
        data.amount = [];
        _.each(list, function(m) {
            data.date.unshift(DateUtils.parseDate(m.get('date')));
            data.visitors.unshift(m.get('visitors'));
            data.views.unshift(m.get('views'));
            data.purchases.unshift(m.get('purchases'));
            data.buyers.unshift(m.get('buyers'));
            data.comments.unshift(m.get('comments'));
            data.amount.unshift(m.get('amount'));
        });
        var firstGraph = this.get('firstGraph');
        var secondGraph = this.get('secondGraph');
        firstGraph.values = data[firstGraph.target];
        secondGraph.values = data[secondGraph.target];
        this.set({
            statsLength: list.length || 0,
            firstGraph: firstGraph,
            secondGraph: secondGraph,
            aggrTypeDate: true
        });
    },

    fetch: function (options) {
        options = options || {};
        if (this.getFilter().get('aggregationType') == "DATE") {
            var self = this;
            var success = options.success;
            options.success = function (result) {
                self.setGraphData();
                if (_.isFunction(success)) {
                    success(result);
                }
            };
            this.statisticsList.load(options);
        } else {
            this.set({
                aggrTypeDate: false
            });
            try {
                BaseModel.prototype.fetch.call(this, options);
            } catch (ex) {
                if (options && options.success) {
                    options.success(this);
                }
            }
        }
    }
});

CampaignAffiliateStats.Models.StatisticsList = BaseCollection.extend({
    loading: false,
    url: function() {
        return "/campaigns/general-stats.json?campaignId=" + this.controller.get('campaignId') + this.controller.getFilter().getDateData();
    }
});

CampaignAffiliateStats.Models.Controller = Campaign.Models.AbstractBaseController.extend({

    initialize: function() {
        this.filter = new CampaignAffiliateStats.Models.BaseFilter({
            campaignId: this.get('campaignId')
        });
        this.totalStats = new CampaignAffiliateStats.Models.TotalStats({
            campaignId: this.get('campaignId'),
            filter: this.filter
        });
        this.set({
            filter: this.filter,
            totalStats: this.totalStats
        });
        this.downloader = new CampaignAffiliateStats.Models.ReportDownloader({
            campaignId: this.get('campaignId')
        });
        this.orders = new CampaignAffiliateStats.Models.OrderCollection({
            currentFilter: this.filter
        });
        this.ordersLoader = new BaseModel({
            filter: this.filter
        });
        this.graph = new CampaignAffiliateStats.Models.Graph({
            currentFilter: this.filter,
            campaignId: this.get('campaignId'),
            controller: this
        });
        this.downloader.controller = this;
        this.orders.controller = this;
        this.getFilter().bind('filterChanged', this.onFilterUrlChange, this);
        this.getFilter().bind('filterChanged', this.graph.reload, this.graph);
    },

    getFilter: function() {
        return this.filter;
    },

    fetch: function (options) {
        var self = this;
        var oldOptions = _.clone(options);
        options.success = function (response) {
            var extraStatsEnabled = self.get('affiliateProgramEnabled') && (PrivacyUtils.isAdmin() || PrivacyUtils.hasAdministrativeRole());
            self.set({
                extraStatsEnabled: extraStatsEnabled
            });
            self.ordersLoader.set({
                extraStatsEnabled: extraStatsEnabled
            });
            self.orders.listHeader.set({
                extraStatsEnabled: extraStatsEnabled
            });
            self.filter.set({
                extraStatsEnabled: extraStatsEnabled
            });
            self.orders.extraStatsEnabled = extraStatsEnabled;
            self.graph.set({
                extraStatsEnabled: extraStatsEnabled
            });
            self.graph.fetch();
            if (oldOptions && oldOptions.success) {
                oldOptions.success(response);
            }
            self.ordersLoader.set({loading: false});
        };
        self.ordersLoader.set({loading: true});
        Campaign.Models.AbstractBaseController.prototype.fetch.call(this, options);
    }
});

CampaignAffiliateStats.Models.OrderCollection = BaseCollection.extend({

    initialize: function (options) {
        BaseCollection.prototype.initialize.call(this, options);
        this.listHeader = new BaseModel({
            currentFilter: options.currentFilter
        });
        this.currentFilter = options.currentFilter;
    },

    url: function () {
        return this.controller.getFilter().getUrl();
    },

    parse: function (response) {
        var self = this;
        _.each(response, function (order, index) {
            order.aggregationType = self.currentFilter.get('aggregationType');
            order.extraStatsEnabled = self.extraStatsEnabled;
        });
        return response;
    }
});

CampaignAffiliateStats.Models.Page = Campaign.Models.BasePage.extend({
    createContentModel: function() {
        return new CampaignAffiliateStats.Models.Controller({
            campaignId: this.get('objectId'),
            profileId: this.get('profileModel').get('profileId'),
//            place here initial values
            subsection: 'affiliate-stats',
            affiliateProgramEnabled: false,
            profileModel: this.get('profileModel')
        });
    },
    pageData: function() {
        var campaignName = this.getCampaignModel().get('name');
        return {
            title: _.template('Статистика проекта «<%=campaignName%>» | Planeta', {
                campaignName: campaignName
            })
        };
    }
});
Biblio.data.bin = new Biblio.Models.Bin();
Biblio.data.initParams = Biblio.Utils.parseSearch();

// отправлять запрос на участие в проекте
$('#send-request').click(function request(e) {
    e.preventDefault();
    var dialog = new Biblio.Views.ModalWindow({
        view: Biblio.Views.OrganizationType,
        clazz: 'modal-biblio-request',
        el: '#modal-biblio-request'
    });
    dialog.open();
});

(function () {
    var specModel = new BaseModel();
// инициализация статистических данных
    new Biblio.Models.Stats().fetch({
        success: function (model) {
            new Biblio.Views.Stats({model: model}).render();
            specModel.set('count', model.get('books'));
        }
    });

// инициализация изданий
    new Biblio.Models.IndexBooksList().fetch().success(function (models) {
        new Biblio.Views.IndexBooksList({
            first: models.slice(0, 5),
            second: models.slice(5, 7),
            spec: specModel
        }).render();
        Biblio.data.bin.fetch();
    });
})();

// инициализация блока объявлений
new Biblio.Collections.Advertise().fetch({
    success: function (collection) {
        new Biblio.Views.Advertises({el: '#advertises-first',
            collection: collection});
    }
});

// инициализация блока партнеров
new Biblio.Collections.Partner().fetch({
    success: function (collection) {
        new Biblio.Views.Partners({collection: collection});
    }
});

$(document).ready(function () {
    // блок "поделиться в соцсетях"
    $('.sps-button').share({
        hidden: false,
        counterEnabled: false
    });
    var curBookId = parseInt(Biblio.data.initParams.bookId);
    if (!isNaN(curBookId)) {
        Biblio.data.bin.fetch().success(function () {
            $.get('/api/public/book-get.json?bookId=' + curBookId, function (model) {
                if (model)
                    new Biblio.Views.SimpleBook({
                        model: new Biblio.Models.Book(model)
                    }).showDetails();
                else
                    workspace.changeUrl('/');
            });
        });
    }

    if(Biblio.openBook) {
        var dialog = new Biblio.Views.ModalWindow({
            view: Biblio.Views.BookRequest,
            clazz: 'modal-biblio-request',
            el: '#modal-biblio-request',
            bodyModel: new Biblio.Models.BookRequest()
        });
        dialog.open();
    }

    if(Biblio.openLibrary) {
        var dialog = new Biblio.Views.ModalWindow({
            view: Biblio.Views.LibraryRequest,
            clazz: 'modal-biblio-request',
            el: '#modal-biblio-request',
            bodyModel: new Biblio.Models.LibraryRequest()
        });
        dialog.open();
    }

});
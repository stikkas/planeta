/*globals Form, UserPayment, Wizard, Campaign, SessionStorageProvider, Campaign, CampaignDonate, SessionStorageProvider*/

var DonateUtils = {
    parseDonateAmount: function(amount) {
        var maxAmount = 1000000000;
        var minAmount = 1;
        var parsed = parseFloat(amount);
        if (_.isNaN(parsed)) {
            return minAmount;
        }
        parsed = Math.abs(Math.round(parsed));
        return parsed > maxAmount ? maxAmount : parsed;
    },
    isCharity: function (campaignModel) {
        return !!(_.find(campaignModel.get('tags'), function (tag) {
            return tag.mnemonicName === 'CHARITY';
        }));
    }
};

var CampaignDonate = CampaignDonate || {};
CampaignDonate.Models = _.extend(CampaignDonate.Models || {}, {
    silentSet: function(attrName, attrValue, context) {
        var attr = {};
        attr[attrName] = attrValue;
        context.set(attr, {silent: true});
        context.trigger('change:' + attrName, attrValue, context);
    }
});

CampaignDonate.Models.Controller = Campaign.Models.BaseCampaign.extend({

    defaults: {
        needComment: true,
        comment: '',
        isAgree: true,
        isUserAmount: false,
        email: ''
    },

    initialize: function(options) {
        BaseModel.prototype.initialize.call(this, options);
        this.initSharesAndCharity();
    },

    initSharesAndCharity: function() {
        this.set({
            isCharity: DonateUtils.isCharity(this)
        }, {silent: true});

        if (!(this.shares instanceof CampaignDonate.Models.ShareCollection)) {
            this.shares = new CampaignDonate.Models.ShareCollection(this.get('shares'), {
                controller: this
            });
            this.shares.on('change:quantity', this.updateDonateAmount, this);
        }
        var selectedShare = this.shares.getSelectedShare();
        if (!selectedShare) {
            selectedShare = this.shares.models[0];
            this.shares.trySelect(selectedShare);
        }
        this.set({
            campaign: this.attributes,
            selectedShare: selectedShare,
            donateAmount: this.get('donateAmount') || selectedShare.getTotalPrice()
        }, {silent: true});

    },

    parse: function(responce) {
        if (responce.success === false) {
            return { success: false };
        }
        if (this.shares) {
            var self = this;
            _(responce.shares).each(function(share) {
//                share.campaign = self;
                var iShare = self.shares.find(function(shareModel) {
                    return (shareModel.id == share.shareId);
                });

                if (iShare) {
                    share.quantity = iShare.get('quantity');
                    share.reply = iShare.get('reply');
                }
            });
            this.shares.reset(responce.shares);
            this.set({
                sharesCount: responce.shares.length
            }, {silent: true});
        }
        return responce;
    },

    fetch: function(options) {
        var newOptions = _.extend({
            data: {
                profileId: this.get('profileId'),
                objectId: this.get('campaignId')
            }
        }, options);
        var self = this;
        newOptions.success = function() {
            self.initSharesAndCharity();
            if (options && options.success && _.isFunction(options.success)) {
                options.success();
            }
        };
        try {
            return Campaign.Models.BaseCampaign.prototype.fetch.call(this, newOptions);
        } catch (ex) {
            console.warn('error on fetch in CampaignDonate.Models.Controller', ex);
            if (newOptions && newOptions.success) {
                newOptions.success(this);
            }
            return null;
        }
    },

    isAuthorized: function() {
        return this.get('isAuthorized');
    },

    isCustomerReady: function() {
        return this.get('isAgree') && !this.get('donateInProcess');
    },

    isEmailExistsOrAuthorized: function() {
        return (workspace.isAuthorized && this.get('hasEmail')) || Form.isValidEmail(this.get('email'));
    },

    isValidDonateAmount: function() {
        return this.get('selectedShare').isEnoughAmount(this.get('donateAmount'));
    },

    onSubmit: function() {
        var self = this;
        self.disablePayment();
        this.get('selectedShare').purchase({
            //todo additional parameters
            donateAmount: this.get('donateAmount'),
            serviceId: this.get('serviceId'),
            planetaPurchase: this.get('planetaPurchase'),
            paymentType: this.get('paymentType'),
            amount: this.get('amount'),
            email: this.get('email'),
            campaignId: this.get('campaign').campaignId,
            phone: this.get('phone'), //payment phone
            hasEmail: this.get('hasEmail')
        }).always(function() {
            self.triggerValidation();
        });
    },

    /**
     * @param {Object} data
     */
    fullFillCustomerContacts: function(data) {
        this.shares.getSelectedShare().set({
            customerContacts: this.shares.getSelectedShare().get('customerContacts') || {}
        });
        var ss = this.shares.getSelectedShare().get('customerContacts');
        ss.customerName = data.customerName || ss.customerName;
        ss.zipCode = data.zipCode || ss.zipCode;
        ss.city = data.city || ss.city;
        ss.street = data.street || ss.street;
        ss.phone = data.phone || ss.phone;
    },

    presetFromData: function(data) {
        if (!data || !data.step) {
            return;
        }
        switch (data.step) {
            case 'SHARE_SELECTION':
                this.shares.each(function(share) {
                    share.set({
                        quantity: data.shares[share.id].quantity,
                        reply: data.shares[share.id].reply
                    });
                });
                this.tryChangeDonateAmount(data.donateAmount);
                break;
            case 'DELIVERY':
                this.fullFillCustomerContacts(data);
                this.set({
                    serviceId: data.serviceId,
                    deliveryPrice: data.deliveryPrice,
                    filterCountryId: data.filterCountryId
                }, {silent: true});
                break;
            case 'PAYMENT':
                break;
        }
    },
    getDataToStore: function(currentStepKey) {

        var data;
        switch (currentStepKey) {
            case 'SHARE_SELECTION':
                var shares = {};
                this.shares.each(function(share) {
                    shares[share.id] = {
                        quantity: share.get('quantity'),
                        reply: share.get('reply')
                    };
                });

                data = {
                    shares: shares,
                    donateAmount: this.get('donateAmount')
                };

                break;
            case 'DELIVERY':
                data = {
                    serviceId: this.get('serviceId'),
                    deliveryPrice: this.get('deliveryPrice'),
                    filterCountryId: this.get('filterCountryId'),
                    customerName: this.get('customerName'),
                    zipCode: this.get('zipCode'),
                    city: this.get('city'),
                    street: this.get('street'),
                    phone: this.get('phone')
                };
                break;
            case 'PAYMENT':
                break;
        }
        return data;
    },
    // TODO: check this
    triggerValidation: function(settings) {
        settings = settings || {};
        if (!_.isUndefined(settings.paymentSystemIsReady)) {
            this.set({
                paymentSystemIsReady: settings.paymentSystemIsReady
            }, {silent: true});
        }
        if (this.isEmailExistsOrAuthorized()
            && this.isCustomerReady()
            && this.get('paymentSystemIsReady')) {
            this.enablePayment();
        } else {
            this.disablePayment();
        }
    },
    disablePayment: function() {
        return 'submit locked';
    },
    enablePayment: function() {
        return 'submit released';
    },
    enableDelivery: function() {

    },
    disableDelivery: function() {

    },
    silentSet: function(attrName, attrValue) {
        CampaignDonate.Models.silentSet(attrName, attrValue, this);
    }
});

CampaignDonate.Models.DonateSharePage = CampaignDonate.Models.Controller.extend({
    initialize: function(options) {
        var shareId = this.get('navigationState').get('arg4');
        if (shareId) {
            this.set(SessionStorageProvider.get('sharePurchase_' + shareId));
        }
        this.set({
            profileId: this.get('profileModel').get('profileId'),
            campaignId: this.get('objectId') || 0,
            group: this.get('profileModel').attributes,
            selectedShareId: shareId || 0
        });
    },

    prefetch: function(options) {
        var self = this;
        var newOptions = _.extend({}, options, {
            success: function() {
                var selectedShare = self.shares.findByAttr('shareId', self.get('selectedShareId'));
                if (selectedShare && self.get('quantity')) {
                    selectedShare.set('quantity', self.get('quantity'));
                }
                if (selectedShare && self.get('donateAmount')) {
                    selectedShare.set('donateAmount', self.get('donateAmount'));
                }
                options.success();
            }
        });
        try {
            this.fetch(newOptions);
        } catch (e) {
            options.error();
        }
    }
});
/*global BaseAppModel,BaseModel */
/**
 * Global application model
 */
var AdminAppModel = BaseAppModel.extend({

    defaults: {
        myProfile: null,
        staticNode: null,
        notifications: null,
        dialogsController: null,
        countriesList: null,
        // Current viewing profile
        profileModel: null,
        // Current viewing page model
        contentModel: null
    },

    initialize: function (options) {
        BaseModel.prototype.initialize.call(this, options);
    }
});

Shop.Models.ProductListItem = BaseModel.extend({

    initialize: function (options) {
        BaseModel.prototype.initialize.call(this, options);
        var name, value;
        this.childNode = {};
        switch (this.get('productState')) {
            case 'GROUP':
            case 'META':
                name = 'childrenCollection';
                value = new Shop.Models.ProductList([], {
                    data: {
                        productId: this.get('productId')
                    },
                    url: '/admin/products-list.json'
                });
                this.childNode.loadHandler = 'load';
                break;
            case 'SOLID_PRODUCT':
            case 'ATTRIBUTE':
                name = 'distribution';
                value = new ProductStorages.Model(this.toJSON());
                this.childNode.loadHandler = 'fetch';
                break;
            default:
                console.error('productState is not defined.');
        }
        this.childNode.name = name;

        this.set(name, value, {silent: true});
        this.get(name).controller = this;
    },

    startSale: function () {
        return this._changeSaleStatus('ACTIVE', '/admin/product-start.json', 'Не удалось начать продажу товара');
    },

    pauseSale: function () {
        return this._changeSaleStatus('PAUSE', '/admin/product-pause.json', 'Не удалось остановить продажу товара');
    },

    _changeSaleStatus: function (newStatus, url, errorMessage) {
        var $dfd = $.Deferred();
        var options = {
            url: url,
            data: {productId: this.get('productId')},
            context: this,
            success: function (response) {
                if (response.success) {
                    this.set({productStatus: newStatus});
                    $dfd.resolveWith(this, [response.result]);

                } else {
                    $dfd.rejectWith(this, [response.errorMessage]);
                }
            },
            error: function () {
                $dfd.rejectWith(this, [errorMessage]);
            }
        };

        Backbone.sync('update', this, options);
        return $dfd;
    },

    fetch: function () {
        return this.get(this.childNode.name)[this.childNode.loadHandler]();
    },

    addProductTagIfNotExists: function (tagId) {
        if (tagId) {
            var newTags = this.get('tags');
            var result = $.grep(newTags, function (e) {
                return e.categoryId == tagId;
            });
            if (result.length == 0) {
                newTags.push(ShopUtils.getProductTag(tagId));
                this.saveTags(newTags);
            }
        }
    },

    removeProductTag: function (tagId) {
        if (tagId) {
            var newTags = this.get('tags');
            newTags = $.grep(newTags, function (e) {
                return e.categoryId != tagId;
            });
            this.saveTags(newTags);
        }
    },

    saveTags: function (newTags) {
        var self = this;
        $.ajax('/admin/product-tags-save.json', _.extend({
            data: JSON.stringify({
                productId: self.get('productId'),
                tags: newTags
            }),
            dataType: 'json',
            type: 'post',
            contentType : 'application/json',
            success:function (response) {
                if (response && response.success) {
                    self.set({tags: newTags}, {silent: true});
                    workspace.appView.showSuccessMessage("Добавление/удаление тега прошло успешно", 3000);
                } else {
                    setTimeout(function () {
                        self.trigger('change');
                    }, 100);
                    if (response && response.errorMessage) {
                        workspace.appView.showErrorMessage(response.errorMessage, 4000);
                    } else {
                        workspace.appView.showErrorMessage("При добавление/удаление тега произошла ошибка", 4000);
                    }
                }
            }
        }));
    }

});

Shop.Models.ProductList = BaseCollection.extend({
    url: '/admin/load-store-products.json',
    model: Shop.Models.ProductListItem
});

Shop.Models.Products = BaseModel.extend({
    initialize: function (attributes, options) {
        this.set({
            products: new Shop.Models.ProductList([], {})
        });
    },
    deleteProduct: function (model) {
        var dfd = $.Deferred();
        var options = {
            url: '/admin/product-deleteByProfileId.json',
            data: {
                productId: model.get('productId')
            },
            success: function (responce) {
                responce.success ?
                    dfd.resolve() :
                    dfd.reject(responce.errorMessage);
            }
        };
        (this.sync || Backbone.sync)('delete', null, options);
        return dfd.promise();
    },

    cloneProduct: function (model) {
        var dfd = $.Deferred();
        var options = {
            url: '/admin/clone-product.json',
            data: {
                productId: model.get('productId')
            },
            success: function (response) {
                response.success ?
                    dfd.resolve(response.result) :
                    dfd.reject(response.errorMessage);
            }
        };
        (this.sync || Backbone.sync)('update', null, options);
        return dfd.promise();
    },

    fetch: function (options) {
        return this.get('products').load(options);
    }
});
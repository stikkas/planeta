/*global Modal, CampaignDonate, SessionStorageProvider, DonateUtils, moduleLoader*/
CampaignDonate.Views.ShareModalInner = BaseView.extend({
    template: '#share-modal-inner-template',
    injectionHtml: 'projectCardHtml',
    injectionFunc: 'rewardModal',

    events: {
        'click input[type="submit"]:not(.disabled)': 'navigateToPurchaseDetails',
        'click .modal-ac-descr-expand-link': 'expand',
        'click .modal-ac-arr-prev': 'onPrevClicked',
        'click .modal-ac-arr-next': 'onNextClicked',
        'click .modal-action-card_close': 'close',
        'click img': 'showImage'
    },
    close: function() {
        this.parent.dispose();
    },
    showImage: function () {
        var options = {underlyingModal : this.parent};
        App.Mixins.Media.showPhoto(this.model.get('profileId'), this.model.get('imageId'), null, options);

    },

    construct: function (options) {
        this.parent = options.parent;
        this.input = this.addChildAtElement('.js-manual-input', new CampaignDonate.Views.ModalManualDonateInput({
            model: this.model,
            controller: this.controller
        }));
        if (!this.model.get('isDonate')) {
            this.numberpicker = this.addChildAtElement('.js-quantity-numberpicker', new CampaignDonate.Views.ModalShareNumberPicker({
                model: this.model
            }));
        }

    },

    expand: function () {
        var $modal = this.$('.modal-ac-descr');
        $modal.toggleClass('expand');
        var $expandSpan = this.$('.modal-ac-descr-expand-link');

        var L10n = {
            _dictionary: {
                "ru": {
                    "roll.up": "Развернуть",
                    "roll.down": "Свернуть"
                },
                "en": {
                    "roll.up": "Open",
                    "roll.down": "Roll down"
                }
            }
        };

        var translate = function (word, lang) {
            if (!lang)
                throw "language is empty.";
            return L10n._dictionary[lang][word] || word;
        };

        var lang = window.workspace && (workspace.currentLanguage === "en") ? "en" : "ru";

        if ($modal.hasClass('expand')) {
            $expandSpan.text(translate('roll.down', lang));
        } else {
            $expandSpan.text(translate('roll.up', lang));
        }
    },

    navigateToPurchaseDetails: function () {
        if (!this.isValid()) {
            return false;
        }
        var currentShare = this.model;
        var shareId = currentShare.get('shareId');
        SessionStorageProvider.extendByKey('commonInfo', {orderType: 'SHARE'});
        SessionStorageProvider.extendByKey('sharePurchase_' + shareId, {
            donateAmount: this.controller.get('donateAmount'),
            quantity: this.model.get('quantity')
        });

        if (window.gtm) {
            window.gtm.trackSelectShare(this.model);
        }
        var donateThisShareUrl = '/campaigns/' + this.controller.get('campaignId') + '/donate/' + shareId + '/payment';
        if (ProfileUtils.isInvestingProject(currentShare.get('shareStatus'))) {
            donateThisShareUrl += 'investfiz';
        }
        $('#share-modal-template').hide();
        workspace.navigate(donateThisShareUrl);
    },

    isValid: function () {
        var errMessage;
        this.saveModel();
        if (!this.controller.isValidDonateAmount() ||
            this.controller.get('status') === "FINISHED") {
            if (this.model.get('isDonate')) {
                errMessage = '<div class="modal-ac-error">Сумма не может быть меньше одного рубля.</div>';
            } else {
                errMessage = '<div class="modal-ac-error">Сумма не может быть меньше цены выбранного вознаграждения.</div>';
            }
            this.$('.js-donate-amount-error').html(errMessage);
            this.$('[name="donateAmount"]')[0].focus();
            return false;
        }
        return true;
    },

    afterRender: function () {
        var $desc = this.$('.js-descriptionHtml'),
                self = this;
        self.$(".modal-ac-descr-expand-link").toggle($desc.innerHeight() > $desc.parent().innerHeight());
        $('.modal-backdrop').addClass('white');

        var shareData = {
            counterEnabled: false,
            hidden: false,
            parseMetaTags: true,
            url: 'https://' + workspace.serviceUrls.mainHost + '/campaigns/' + self.controller.get('webCampaignAlias') +
                '/donate/' + self.model.get('shareId')
        };
        shareData.className = 'donate-sharing';
        $('.js-share-share').share(shareData);
    },

    dispose: function () {
        this.saveModel();
        BaseView.prototype.dispose.apply(this, arguments);
    },
    /**
     *
     * @param {Number} direction +1 or -1
     */
    onArrowClick: function (direction) {
        var self = this;
        var nextShare, nextShareIdx;
        var shares = this.controller.shares;
        _.each(shares.models, function (share, idx) {
            if (share.get('shareId') == self.model.get('shareId')) {
                nextShareIdx = idx + direction;
            }
        });
        if (nextShareIdx === null) {
            return;
        }

        nextShare = shares.at(nextShareIdx);
        if (nextShare !== undefined && !(nextShare.get('amount') && nextShare.get('amount') == nextShare.get('purchaseCount'))) {
            // switch uses this.controller.shares collection data, so synchronize with it before switching
            this.saveModel();
            this.controller.switchShare(nextShare, false);
            this.model.set(nextShare.attributes, {silent: true});

            this.model.extendShare();
            var el = $('.js-modal-overlapped-view > .modal-action-card');
            if (ProfileUtils.isInvestingProject(nextShare.get('shareStatus'))) {
                el.addClass('modal-action-card__investing');
            } else {
                el.removeClass('modal-action-card__investing');
            }
        }
    },
    /**
     * To save changed quantity, amount etc. for closed, switched or verified share
     */
    saveModel: function () {
        var self = this;
        var currentShare = this.controller.shares.find(function (share) {
            return share.get('shareId') == self.model.get('shareId');
        });
        currentShare.set({
            amount: this.model.get('amount'),
            purchaseCount: this.model.get('purchaseCount'),
            donateAmount: this.model.get('purchaseCount'),
            quantity: this.model.get('quantity')
        });
        this.controller.set('donateAmount', this.model.get('donateAmount'));
    },

    onPrevClicked: function () {
        this.onArrowClick(-1);
    },
    onNextClicked: function () {
        this.onArrowClick(1);
    }
});


CampaignDonate.Views.ShareModal = Modal.OverlappedView.extend({
    template: '#share-modal-template',
    className: 'modal modal-action-card',
    clickOff: true,
    /**
     * Prevent multiple clickOff in repeating afterRender method
     * This class is just modal wrapper, model needed in inner model
     */
    modelEvents: {
        'destroy': 'dispose'
    },
    construct: function () {
        /**
         * create empty share-model container to insert in it different shares
         * we can't change original model, it connected with fixed share in right sidebar
         */

        var share = new CampaignDonate.Models.Share({controller: this.model.controller});
        share.set(this.model.attributes);
        // synthetic variables for easy templating
        share.extendShare();

        this.addChildAtElement('.js-modal-inner', new CampaignDonate.Views.ShareModalInner({
            model: share,
            controller: this.model.controller,
            parent : this
        }));
        if (ProfileUtils.isInvestingProject(this.model.get('shareStatus'))) {
            this.$el.addClass('modal-action-card__investing');
        }
    }
});

CampaignDonate.Views.ModalShareNumberPicker = CampaignDonate.Views.ShareNumberPicker.extend({
    template: '#campaign-share-modal-numberpicker-template'
});

/**
 * Try to catch superclass model events with synthetic field share.donateAmount
 * To do it override setDonateAmount (for right template rendering) and updateDonateAmountFromModel for right backbone model event handling
 */
CampaignDonate.Views.ModalManualDonateInput = CampaignDonate.Views.ManualDonateInput.extend({
    template: '#manual-modal-input-template',

    getController: function () {
        return this.controller;
    },

    /**
     * @override
     * @param model
     * @param donateAmount
     */
    updateDonateAmountFromModel: function (model, donateAmount) {
        if (donateAmount === null) {
            donateAmount = model;
        }
        this.setDonateAmount(donateAmount || this.getController().get('donateAmount'));
    },

    /**
     * @override
     */
    setDonateAmount: function (donateAmount) {
        CampaignDonate.Views.ManualDonateInput.prototype.setDonateAmount.apply(this, arguments);
        this.model.set('donateAmount', donateAmount, {silent: true});
    }
});

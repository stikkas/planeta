CampaignEdit.Views.CreatedSuccessful = BaseView.extend({
    template: '#campaign-edit-created-successful-template',
    events: {
      'click .js-send-feedback-form' : 'sendFeedback'
    },

    afterRender: function () {
        var self = this;
        $.post('/api/profile/check-campaign-user-feedback-exists.json', {campaignId: this.model.get('campaign').campaignId}).done(function (response) {
            if (response && !response.result) {
                self.$('.js-feedback-form').css('display', 'block');
            }
        });
    },

    sendFeedback: function (e) {
        e.preventDefault();
        var form = this.$("#user-feedback-form").serialize();
        form = form + '&userId=' + workspace.appModel.myProfileId();
        if (form.indexOf('score=') == -1) {
            return;
        }
        var self = this;
        $.post('/api/profile/add-user-feedback.json', form).done(function (response) {
            if (response && response.success) {
                if (response.result) {
                    self.$('.js-feedback-message-container').html('<div class="quality-polling_head">Спасибо! Ваш отзыв отправлен!</div>');
                } else {
                    self.$('.js-feedback-message-container').html('<div class="quality-polling_head">Вы уже оставили отзыв ранее!</div>');
                }
            } else {
                self.$('.js-feedback-message-container').html('<div class="quality-polling_head">При отправке отзыва поизошла ошибка!</div>');
            }
        });
    }
});
/*globals ProductEdit, ProductStorages*/

ProductEdit.Views.SingleProductQuantity = BaseView.extend({
    className: '',
    template: '#product-edit-quantity-template',

    events: {
        'click .js-update-quantity': 'saveNewTotalQuantity',
        'click .js-cancel-changes': 'cancelChanges',
        'keyup input[name=totalQuantity]': 'updateTotalQuantity'
    },

    construct: function() {
        this.backupTotalQuantity();
    },

    backupTotalQuantity: function() {
        this._tQ = this.model.get('totalQuantity');
    },
    restoreTotalQuantity: function() {
        this.$('[name=totalQuantity]').val(this._tQ);
        this.model.set({
            totalQuantity: this._tQ,
            isQuantityChanged: false
        });
    },
    updateTotalQuantity: function(e) {
        this.onNewQuantityChanged(e);
    },

    _numberFromValue: function(e) {
        var number;
        var el = this.$(e.target);
        var value = el.val();
        var parsed = parseInt(value, 10);
        if (value != parsed) {
            number = _.isNaN(parsed) ? 1 : parsed;
        } else {
            number = parsed;
        }
        return number < this.model.get('totalOnHoldQuantity') ? this.model.get('totalOnHoldQuantity') : number;
    },
    onNewQuantityChanged: _.debounce(function(e) {
        var number = this._numberFromValue(e);
        this.$(e.target).val(number);
        if (!this.model.get('isQuantityChanged') && this.model.get('productId')) {
            this.showActionBlock();
        }
        this.model.set({
            totalQuantity: number,
            isQuantityChanged: true
        }, {silent: true});
        if (!this.model.get('productId')) {
            this.saveNewTotalQuantity();
        }
    }, 350),

    saveNewTotalQuantity: function() {
        var self = this;
        this.model.saveTotalQuantity().done(function() {
            self.backupTotalQuantity();
            if (self.controller) self.controller.trigger('change:quantity', self.model);
            self.model.set({
                isQuantityChanged: false
            });
        });
    },

    cancelChanges: function() {
        this.hideActionBlock();
        this.restoreTotalQuantity();
    },

    showActionBlock: function() {
        this.$('.action-block').removeClass('hidden');
    },
    hideActionBlock: function() {
        this.$('.action-block').addClass('hidden');
    }

});
/*
//===========META
ProductEdit.Views.ProductChildItem = ProductEdit.Views.SingleProductQuantity.extend({
    tagName: 'tr',
    template: '#product-edit-attr-item-template'
});

ProductEdit.Views.ProductChildDistributionList = BaseListView.extend({
    tagName: 'tbody',
    itemViewType: ProductEdit.Views.ProductChildItem,
    pagerLoadingTemplate: "#pager-loading-template"
});


ProductEdit.Views.ProductChildQuantityBlock = BaseView.extend({
    template: '#product-edit-attr-distribution-block-template',
    modelEvents: _.extend({}, BaseView.prototype.modelEvents,{
        'change:quantity':'onChangeTotalQuantity'
    }),
    construct: function(options) {
        this.addChildAtElement('.product-manage-table', new ProductEdit.Views.ProductChildDistributionList({
            controller: this.model,
            collection: new ProductEdit.Models.ChildProducts(this.model.get('childrenProducts'))
        }));
    },
    onChangeTotalQuantity: function(attrModel) {
        (_(this.model.get('childrenProducts')).find(function(product) {
            return (product.productAttribute.value == attrModel.get('productAttribute').value);
        })|| {}).totalQuantity = attrModel.get('totalQuantity');
    }
});

//============ quantity tab
ProductEdit.Views.QuantityTab = BaseView.extend({
    anchor: '.plate-cont',
    stateSettings: {
        META: {
            model: null,
            view: ProductEdit.Views.ProductChildQuantityBlock
        },
        ATTRIBUTE: {
            model: ProductStorages.Model,
            view: ProductEdit.Views.SingleProductQuantity
        },
        SOLID_PRODUCT: {
            model: ProductStorages.Model,
            view: ProductEdit.Views.SingleProductQuantity
        }
    },
    construct: function() {
        var stateSetting = this.stateSettings[this.model.get("productState")];
        var view = stateSetting.view;
        var distributionModel = stateSetting.model && _.extend(this.model, stateSetting.model.prototype) || this.model;
        this.addChild(new view({
            model: distributionModel
        }));
    },

    validate: function() {
        return this.model.validateDistribution();
    },

    onRevert: function() {
        return this.model.validateDistribution();
    }
});
*/

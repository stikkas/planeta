CrowdFund.Views.ImageField.VideoView = BaseView.extend({
    template: '#campaign-image-field-video-template',
    events: {
        'click .js-change-cover': 'onChangeCoverClick',
        'click .js-remove': 'onRemoveClick'

    },

    onRemoveClick: function () {
        this._deleteVideo(this.model.get("loadedVideo"));

        this.model.set({
            imageUrl: null,
            imageId: 0,
            videoId: 0,
            videoProfileId: 0,
            videoType: null,
            videoUrl: null,
            isVideoDeleted: true
        }, {silent: true});
        //https://planeta.atlassian.net/browse/PLANETA-14395
        this.model.trigger("change:imageUrl");
    },

    onChangeCoverClick: function () {
        var view = new CrowdFund.Views.ImageField.VideoView.ChangeCoverView({
            model: this.model
        });
        view.render();
    },

    _deleteVideo: CrowdFund.Views.ImageField.prototype._deleteVideo


});

CrowdFund.Views.ImageField.VideoView.ChangeCoverView = Modal.OverlappedView.extend({
    template: '#campaign-image-field-change-cover-template',
    initialize: function () {
        Modal.OverlappedView.prototype.initialize.apply(this, arguments);
        this.changeCoverModel = new CrowdFund.Models.ImageField({
            originalImage: {
                imageUrl: this.model.get('imageUrl'),
                photoId: this.model.get('imageId')
            },
            profileId: this.model.get('profileId'),
            albumTypeId: AlbumTypes.ALBUM_COMMENT,
            thumbnail: {
                imageConfig: ImageUtils.ORIGINAL,
                imageType: ImageType.PHOTO
            },
            aspectRatio: 640 / 360,
            title: 'Обложка',
            description: ''
        });
        this.changeCoverView = new CrowdFund.Views.ImageField({
            model: this.changeCoverModel
        });

        this.addChildAtElement('.js-edit-cover', this.changeCoverView);
    },

    cancel: function () {
        this.changeCoverView.cancel();
        Modal.OverlappedView.prototype.cancel.apply(this, arguments);
    },

    save: function () {
        this.changeCoverView.save();
        this.model.set({
            imageUrl: this.changeCoverModel.get('imageUrl'),
            imageId: this.changeCoverModel.get('imageId')
        });
        this.dispose();
    }
});
/*global CampaignEdit, CrowdFund, CampaignDonate*/

CampaignEdit.Models.Check = CampaignEdit.Models.Tab.extend({
    initialize: function (options) {
        CampaignEdit.Models.Tab.prototype.initialize.apply(this, arguments);
        this.contractor = new CrowdFund.Models.ContractorEdit({campaignId: options.objectId, parentModel: this});
    }
});

CrowdFund.Models.ContractorEdit = BaseModel.extend({
    url: function () {
        return '/admin/contractor-get-campaign.json?campaignId=' + this.get('campaignId');
    },
    defaults: {
        countryId: 1,
        cityId: 0,
        name: '',
        type: 'INDIVIDUAL',
        ogrn: '',
        inn: '',
        passportNumber: '',
        otherDetails: '',
        phone: '',
        legalAddress: '',
        actualAddress: '',
        authority: '',
        issueDate: null,
        checkingAccount: '',
        beneficiaryBank: '',
        correspondingAccount: '',
        bic: '',
        unit: '',
        birthDate: null,
        kpp: '',
        responsiblePerson: '',
        scanPassportUrl: null,
        initialsWithLastName: '',
        position: 'NOT_SET',
        customPosition: ''
    },
    initialize: function (options) {
        BaseModel.prototype.initialize.call(this, options);
        this.parentModel = options.parentModel;
    }
});

CampaignEdit.Views.Check = BaseView.extend({
    className: 'col-12',
    template: '#campaign-edit-check-content-template',
    construct: function (options) {
        if (options && options.controller) {
            options.controller.pageData = this.pageData();
        }
        var self = this,
            contractorsSelector,
            contractors;
        Backbone.sync('read', null, {
            url: "/admin/contractor-list-by-profile.json",
            data: {
                profileId: workspace.appModel.get("profileModel").get("profileId"),
                offset: 0,
                limit: 25
            }
        }).done(function (response) {
            if (response && response.success) {
                if (contractorsSelector)
                    contractorsSelector.createContractorChoose(response.result);
                else
                    contractors = response.result;
            }
        });
        self.model.contractor.fetch({success: function () {
                var isNewContractor = !(self.model.contractor && self.model.contractor.contractorId);
                var isCharity = !!self.model.get('tags').find(function(obj){return obj.mnemonicName === 'CHARITY';});
                if (isCharity && isNewContractor) {
                    self.model.contractor.set({isCharity: isCharity, type: "CHARITABLE_FOUNDATION"});
                }
                contractorsSelector = self.addChildAtElement('.project-create_cont', new CrowdFund.Views.CampaignContractorEdit({
                    model: self.model.contractor,
                    campaign: self.model
                }));
                if (contractors)
                    contractorsSelector.createContractorChoose(contractors);
            }});

    },
    pageData: function () {
        var L10n = {
            _dictionary: {
                "ru": {
                    "section": "Редактирование кампании",
                    "tabName": "контрагент"
                },
                "en": {
                    "section": "Campaign edit",
                    "tabName": "contractor"
                }
            }
        };

        var translate = function (word, lang) {
            if (!lang)
                throw "language is empty.";
            return L10n._dictionary[lang][word] || word;
        };

        var lang = window.workspace && (workspace.currentLanguage === "en") ? "en" : "ru";

        return {
            title: translate('section', lang) + ' - ' + translate('tabName', lang)
        }
    }
});


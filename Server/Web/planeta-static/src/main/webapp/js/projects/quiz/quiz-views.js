Quiz.Views.Base = BaseView.extend({
    template: '#quiz-template',
    className: 'polling',
    events: {
        'click .js-send-quiz-form' : 'sendForm'
    },

    construct: function() {
        if (!workspace.isAuthorized) {
            LazyHeader.showAuthForm('signup');
        }
    },

    afterRender: function() {
        var self = this;
        if (self.model.get('enabled')) {
            $.post('/api/quiz/check-user-answered-quiz.json', {quizId: self.model.get('quizId')}).done(function (response) {
                if (response && !response.result) {
                    self.loadQuestions();
                } else {
                    self.$('.js-already-answered').show();
                }
            });
        } else {
            self.$('.js-quiz-closed').show();
        }
    },

    loadQuestions: function () {
        var self = this;
        this.model.quizQuestions.load().done(function () {
            if (self.model.quizQuestions.length > 0) {
                self.questionsView = new Quiz.Views.Questions({model: new Quiz.Models.QuizQuestion(), collection: self.model.quizQuestions});
                self.addChildAtElement('#quiz-questions-list', self.questionsView);
                self.$('.polling-action').show();
            } else {
                console.log('quizQuestion collection is empty');
            }
        }).fail(function () {
            console.log('quizQuestion collection load error');
        });
    },

    sendForm: function(e) {
        e.preventDefault();
        var self = this;
        var isValidationFail = false;

        this.$('.polling-list_i').removeClass('error');
        _.each(this.model.quizQuestions.models, function (quizQuestion) {
            quizQuestion.set({
                quizId: self.model.get('quizId')
            }, {silent: true});

            if (!(quizQuestion.get('answerInteger') || quizQuestion.get('answerBoolean') || quizQuestion.get('answerCustomText'))) {
                isValidationFail = true;
                quizQuestion.set({
                    error: true
                }, {silent: true});
                var inputType = 'input';
                if (quizQuestion.get('type') == 'TEXT_BIG') {
                    inputType = 'textarea';
                }
                self.$el.find(inputType + '[name=answer' + quizQuestion.get('quizQuestionId') + ']').closest('.polling-list_i').addClass('error');
            } else {
                quizQuestion.set({
                    error: false
                }, {silent: true});
            }
        });

        if (isValidationFail) {
            return;
        }

        var self = this;
        $.ajax('/api/quiz/add-quiz-answers.json', _.extend({
            type: 'POST',
            data: JSON.stringify(self.model.quizQuestions.models),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function(response) {
                if (response && response.success) {
                    if (response.result) {
                        self.$el.html('<div class="polling_success"><div class="polling_success-head">Спасибо за уделённое время!</div><div class="polling_success-text">Ваше мнение очень важно для нас, оно помогает нам улучшить сервис.</div></div>');
                        $('html, body').animate({scrollTop: $('body').offset().top}, 'slow');
                    } else {
                        workspace.appView.showErrorMessage('При отправке данных произошла ошибка');
                    }
                } else {
                    workspace.appView.showErrorMessage('При отправке данных произошла ошибка');
                }
            }
        }));
    }
});

Quiz.Views.Question = BaseView.extend({
    template: '#quiz-question-item-template',
    className: 'reward-rich_project-name',
    events: {
        'change input' : 'changeData',
        'change textarea' : 'changeData'
    },

    changeData: function (e){
        if (this.model.get('hasCustomRadio')) {
            var customRadio = $(e.currentTarget).attr('quiz-custom-radio');
            if (customRadio) {
                this.$el.find('textarea[name=answer' + this.model.get('quizQuestionId') + ']').removeAttr('disabled');
                return;
            } else {
                this.$el.find('textarea[name=answer' + this.model.get('quizQuestionId') + ']').attr('disabled', true);
            }
        }
        var $el = $(e.currentTarget);
        var fieldName = $el.attr('quiz-answer-field');
        var value = $el.attr('value');

        switch (fieldName) {
            case 'answerInteger':
                this.model.set({
                    answerInteger: value
                }, {silent: true});
                break;
            case 'answerBoolean':
                this.model.set({
                    answerBoolean: value
                }, {silent: true});
                break;
            case 'answerCustomText':
                this.model.set({
                    answerCustomText: value
                }, {silent: true});
                break;
            default:
                console.log('unknown data type.');
        }
    }
});

Quiz.Views.Questions = BaseListView.extend({
    itemViewType: Quiz.Views.Question
});
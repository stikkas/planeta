if (!CampaignEdit.Models.Interactive) {
    CampaignEdit.Models.Interactive = {};
}
CampaignEdit.Models.Interactive.Base = BaseModel.extend({
    url: '/api/campaign/get-interactive-editor-data.json',
    parse: BaseModel.prototype.parseActionStatus,
    defaults: {
        editorData: {
            reachedStepNum: 0,
            agreeReceiveLessonsMaterialsOnEmail: false,
            agreeStorePersonalData: false
        },
        activeIndex: 0,
        stepsCount: 11,
        showForm: false,
        showFormAlways: false,
        baseUrl: '/interactive',
        lessonsMaterials: ['https://files.planeta.ru/interactive/lessons/Доп.материалы.zip',
            'https://files.planeta.ru/interactive/lessons/Доп.материалы.zip',
            'https://files.planeta.ru/interactive/lessons/Доп.материалы.zip',
            'https://files.planeta.ru/interactive/lessons/Доп.материалы.zip',
            'https://files.planeta.ru/interactive/lessons/Доп.материалы.zip',
            'https://files.planeta.ru/interactive/lessons/Доп.материалы.zip',
            'https://files.planeta.ru/interactive/lessons/Доп.материалы.zip',
            'https://files.planeta.ru/interactive/lessons/Доп.материалы.zip',
            'https://files.planeta.ru/interactive/lessons/Доп.материалы.zip',
            'https://files.planeta.ru/interactive/lessons/Доп.материалы.zip',
            'https://files.planeta.ru/interactive/lessons/Доп.материалы.zip',
            'https://files.planeta.ru/interactive/lessons/Доп.материалы.zip'],

        steps: {
            info: {
                name: 'Основные данные',
                fields: ['name', 'shortDescription', 'imageUrl', 'tags']
            },
            duration: {
                name: 'Сроки',
                fields: ['timeFinish']
            },
            price: {
                name: 'Цель',
                fields: ['targetAmount']
            },
            description: {
                name: 'Описание',
                fields: ['descriptionHtml']
            },
            video: {
                name: 'Описание',
                fields: ['videoContent']
            },
            reward: {
                name: 'Вознаграждения',
                fields: ['shares']
            },
            counterpart: {
                name: 'Контрагент',
                fields: []
            }
        }
    },
    initialize: function () {
        if (this.get('activeIndex') < 3) {
            StorageUtils.increaseIfItIsMoreThanMaxInteractiveReachedStepNum(this.get('activeIndex'));
        }
    },

    loadPageLogic: function () {
        var self = this;
        var campaignId = self.get('editorData').campaignId || 0;
        if (campaignId == 0 && self.get('activeIndex') > 4) {
            document.location.href = self.get('baseUrl');
            return;
        }

        if (!workspace.isAuthorized && this.get('activeIndex') < 3) {
            var editorData = self.get('editorData');
            editorData.agreeReceiveLessonsMaterialsOnEmail = StorageUtils.isInteractiveDraftCampaignAgreeReceiveLessonsMaterialsOnEmail();
            editorData.reachedStepNum = StorageUtils.getInteractiveReachedStepNum();
            self.set({editorData: editorData});
        }

        var reachedStepId = (self.get('editorData') && self.get('editorData').reachedStepNum) ? self.get('editorData').reachedStepNum : StorageUtils.getInteractiveReachedStepNum() || 0;
        if ((self.get('activeIndex') > reachedStepId) || (!workspace.isAuthorized && this.get('activeIndex') > 2)) {
            document.location.href = self.get('baseUrl');
            return;
        }

        if (!workspace.isAuthorized && this.get('activeIndex') < 3) {
            var editorData = self.get('editorData');
            editorData.reachedStepNum = StorageUtils.getInteractiveReachedStepNum();
            self.set({editorData: editorData});
        }

        if (self.get('editorData') && self.get('editorData').formsSettings) {
            self.set(_.extend({}, self.attributes, self.get('editorData').formsSettings[self.get('activeIndex')]));
        }

        self.set({arcValue : StorageUtils.getInteractiveReachedStepNum() / self.get('stepsCount') * 100});
        self.set({lessonMaterialUrl : self.get('lessonsMaterials')[self.get('activeIndex')]});
    },

    prefetch: function (options) {
        var self = this;
        $.when($.get("/api/campaign/get-interactive-editor-data.json", {
            activeIndex: StorageUtils.getInteractiveReachedStepNum(),
            agreeReceiveLessonsMaterialsOnEmail: StorageUtils.isInteractiveDraftCampaignAgreeReceiveLessonsMaterialsOnEmail() || self.get('editorData').agreeReceiveLessonsMaterialsOnEmail
        })).done(function(response){
            if (response && response.result) {
                if (response.result.formsSettings) {
                    response.result.formsSettings = JSON.parse(response.result.formsSettings);
                }
                self.set({editorData: response.result});
                StorageUtils.setInteractiveReachedStepNum(response.result.reachedStepNum);
                StorageUtils.setInteractiveDraftCampaignAgreeReceiveLessonsMaterialsOnEmail(response.result.agreeReceiveLessonsMaterialsOnEmail);
                if (self.campaign) {
                    self.campaign.set({campaignId: self.get('editorData').campaignId});
                    self.campaign.set({objectId: self.get('editorData').campaignId});
                }
            }
            self.loadPageLogic();
            self.fetch(options);
        });
    },

    setShowForm: function (value) {
        this.set({showForm: value});
        var formsSettings = this.get('editorData') ? this.get('editorData').formsSettings : {};
        if (formsSettings) {
            formsSettings[this.get('activeIndex')] ? formsSettings[this.get('activeIndex')].showForm = value : formsSettings[this.get('activeIndex')] = {showForm : value};
        } else {
            formsSettings = {};
            formsSettings[this.get('activeIndex')] = {showForm : value};
        }
        for (var i = 0; i < this.get('activeIndex'); i++) {
            formsSettings[i] = {showForm : value};
        }
        //StorageUtils.setInteractiveFormsSettings(formsSettings);
        var self = this;
        $.ajax({
            type: 'post',
            url: '/api/campaign/save-interactive-editor-data-form-settings.json',
            data: {
                formsSettings: JSON.stringify(formsSettings),
                activeIndex: self.get('activeIndex')
            }
        });
    },

    resetStorage: function () {
        StorageUtils.clearInteractiveData();
    },

    navigateToValidation: function (globalError, errors, errorClass) {
        if (errors == null) {
            if (globalError && !errorClass) {
                workspace.appView.showErrorMessage(globalError);
            }
            return;
        }
        var self = this;
        // get first tab with errors
        $.each(this.get('steps'), function (name, tab) {
            var hasError = _.any(tab.fields, function (field) {
                return errors[field];
            });
            if (hasError) {
                // save validation errors and redirect to tab with errors
                self.set('errors', errors);
                workspace.onClickNavigate(null, '/interactive/' + name);
                // break $.each
                return false;
            }
        });
    }

});

CampaignEdit.Models.Interactive.Start = CampaignEdit.Models.Interactive.Base.extend({
    defaults: _.extend({}, CampaignEdit.Models.Interactive.Base.prototype.defaults, {
        activeIndex: 0
    }),

    initialize: function () {
        BaseModel.prototype.initialize.apply(self, arguments);
    }
});

CampaignEdit.Models.Interactive.Name = CampaignEdit.Models.Interactive.Base.extend({
    defaults: _.extend({}, CampaignEdit.Models.Interactive.Base.prototype.defaults, {
        activeIndex: 1
    }),

    initialize: function () {
        CampaignEdit.Models.Interactive.Base.prototype.initialize.apply(this, arguments);
        var userName = StorageUtils.getInteractiveDraftCampaignUserName();
        console.log('userNameFromDraft: ' + userName);
        if (userName) {
            this.set(_.extend({}, this.attributes, {name: userName}));
        }

        try {
            var currentName = workspace.appModel.get('myProfile').get('displayName');
            console.log('userNameFromWorkspace: ' + currentName);
            if (currentName) {
                this.set(_.extend({}, this.attributes, {name: currentName}));
            }
        } catch(e){}

        var agree = StorageUtils.getInteractiveDraftCampaignAgreeStorePersonalData();
        if (agree) {
            var editorData = this.get('editorData');
            editorData.agreeStorePersonalData = agree;
            this.set({editorData: editorData});
        }
    }
});

CampaignEdit.Models.Interactive.Email = CampaignEdit.Models.Interactive.Base.extend({
    defaults: _.extend({}, CampaignEdit.Models.Interactive.Base.prototype.defaults, {
        activeIndex: 2
    }),

    initialize: function () {
        CampaignEdit.Models.Interactive.Base.prototype.initialize.apply(this, arguments);

        var userEmail = StorageUtils.getInteractiveDraftCampaignUserEmail();
        if (userEmail) {
            this.set(_.extend({}, this.attributes, {email: userEmail}));
        }

        try {
            var currentEmail = workspace.appModel.get('myProfile').get('email');
            if (currentEmail) {
                this.set(_.extend({}, this.attributes, {email: currentEmail}));
            }
        } catch(e){}

        var userName = StorageUtils.getInteractiveDraftCampaignUserName();
        if (userName) {
            this.set(_.extend({}, this.attributes, {name: userName}));
        }

        try {
            var currentName = workspace.appModel.get('myProfile').get('displayName');
            if (currentName) {
                this.set(_.extend({}, this.attributes, {name: currentName}));
            }
        } catch(e){}
    }
});

CampaignEdit.Models.Interactive.Proceed = CampaignEdit.Models.Interactive.Base.extend({
    defaults: _.extend({}, CampaignEdit.Models.Interactive.Base.prototype.defaults, {
        activeIndex: 3
    }),

    initialize: function () {
        CampaignEdit.Models.Interactive.Base.prototype.initialize.apply(this, arguments);

        var campaignId = this.get('editorData').campaignId || 0;
        this.campaign = (new CampaignEdit.Models.DraftCampaign({
            profileId: workspace.appModel.myProfileId(),
            campaignId: campaignId
        }));
    }
});

CampaignEdit.Models.Interactive.Info = CampaignEdit.Models.Interactive.Base.extend({
    defaults: _.extend({}, CampaignEdit.Models.Interactive.Base.prototype.defaults, {
        activeIndex: 4
    }),

    initialize: function () {
        CampaignEdit.Models.Interactive.Base.prototype.initialize.apply(this, arguments);
        var campaignId = this.get('editorData').campaignId || 0;
        this.campaign = (new CampaignEdit.Models.DraftCampaign({
            profileId: workspace.appModel.myProfileId(),
            campaignId: campaignId
        }));
    },

    fetch: function (options) {
        if (!workspace.isAuthorized) {
            options.success();
            return;
        }

        var self = this;
        if (this.campaign.get('campaignId') > 0) {
            this.campaign.fetch().done(function () {
                self.cardImageModel = new CampaignEdit.Models.ShareImage({
                    errors: {},
                    originalImage: {
                        imageUrl: self.campaign.get('imageUrl'),
                        photoId: self.campaign.get('imageId')
                    },
                    profileId: self.campaign.get('profileId'),
                    albumTypeId: AlbumTypes.ALBUM_COMMENT,
                    thumbnail: {
                        imageConfig: ImageUtils.MEDIUM,
                        imageType: ImageType.PHOTO
                    },
                    aspectRatio: 220 / 134,
                    width: 220,
                    title: 'Картинка на витрину'
                });

                CampaignUtils.getCampaignTags().done(function (tags) {
                    self.campaignTags = tags;
                    if (options) {
                        options.success();
                    }
                });
            });

        } else {
            this.campaign.saveCampaign(null, null, '/admin/campaign-create-info-step.json').done(function (response) {
                if(response.result) {
                    self.campaign.set(_.extend({}, self.campaign.attributes, response.result));
                    self.cardImageModel = new CampaignEdit.Models.ShareImage({
                        errors: {},
                        originalImage: {
                            imageUrl: self.campaign.get('imageUrl'),
                            photoId: self.campaign.get('imageId')
                        },
                        profileId: self.campaign.get('profileId'),
                        albumTypeId: AlbumTypes.ALBUM_COMMENT,
                        thumbnail: {
                            imageConfig: ImageUtils.MEDIUM,
                            imageType: ImageType.PHOTO
                        },
                        aspectRatio: 220 / 134,
                        width: 220,
                        title: 'Картинка на витрину'
                    });

                    CampaignUtils.getCampaignTags().done(function (tags) {
                        self.campaignTags = tags;
                        if (options) {
                            options.success();
                        }
                    });
                } else {
                    console.log('Ошибка при создании/получении кампании');
                }
            });
        }
    }
});

CampaignEdit.Models.Interactive.Duration = CampaignEdit.Models.Interactive.Base.extend({
    defaults: _.extend({}, CampaignEdit.Models.Interactive.Base.prototype.defaults, {
        activeIndex: 5
    }),

    initialize: function () {
        CampaignEdit.Models.Interactive.Base.prototype.initialize.apply(this, arguments);
        var campaignId = this.get('editorData').campaignId || 0;
        this.campaign = (new CampaignEdit.Models.DraftCampaign({
            profileId: workspace.appModel.myProfileId(),
            campaignId: campaignId
        }));
    },

    fetch: function (options) {
        if (!workspace.isAuthorized) {
            options.success();
            return;
        }
        this.campaign.fetch(options);
    }
});

CampaignEdit.Models.Interactive.Price = CampaignEdit.Models.Interactive.Base.extend({
    defaults: _.extend({}, CampaignEdit.Models.Interactive.Base.prototype.defaults, {
        activeIndex: 6,
        desiredCollectedAmountPercent: 10,
        contractorTypePercent: 13,
        desiredTargetAmount: 0,
        targetAmount: 0,
        errors: {}
    }),

    initialize: function () {
        CampaignEdit.Models.Interactive.Base.prototype.initialize.apply(this, arguments);
        var campaignId = this.get('editorData').campaignId || 0;
        this.campaign = (new CampaignEdit.Models.DraftCampaign({
            profileId: workspace.appModel.myProfileId(),
            campaignId: campaignId
        }));
    },

    fetch: function (options) {
        if (!workspace.isAuthorized) {
            options.success();
            return;
        }
        var self = this;
        if (this.campaign.get('campaignId') > 0) {
            this.campaign.fetch().done(function () {
                var targetAmountFromStorage = StorageUtils.getInteractiveDraftCampaignTargetAmount();
                if (!self.campaign.get('targetAmount') && targetAmountFromStorage) {
                    self.set({targetAmount: targetAmountFromStorage});
                    self.campaign.set({targetAmount: targetAmountFromStorage});
                } else {
                    self.set({targetAmount: self.campaign.get('targetAmount')});
                    StorageUtils.setInteractiveDraftCampaignTargetAmount(self.campaign.get('targetAmount'));
                }

                self.cardImageModel = new CampaignEdit.Models.ShareImage({
                    errors: {},
                    originalImage: {
                        imageUrl: self.campaign.get('imageUrl'),
                        photoId: self.campaign.get('imageId')
                    },
                    profileId: self.campaign.get('profileId'),
                    albumTypeId: AlbumTypes.ALBUM_COMMENT,
                    thumbnail: {
                        imageConfig: ImageUtils.MEDIUM,
                        imageType: ImageType.PHOTO
                    },
                    aspectRatio: 220 / 134,
                    width: 220,
                    title: 'Картинка на витрину'
                });

                CampaignUtils.getCampaignTags().done(function (tags) {
                    self.campaignTags = tags;
                    if (options) {
                        options.success();
                    }
                });
            });
        }
    }
});

CampaignEdit.Models.Interactive.Video = CampaignEdit.Models.Interactive.Base.extend({
    defaults: _.extend({}, CampaignEdit.Models.Interactive.Base.prototype.defaults, {
        activeIndex: 7,
        showFormAlways: true
    }),

    initialize: function () {
        CampaignEdit.Models.Interactive.Base.prototype.initialize.apply(this, arguments);

        var campaignId = this.get('editorData').campaignId || 0;
        this.campaign = (new CampaignEdit.Models.Shares({
            profileId: workspace.appModel.myProfileId(),
            ownerProfileId: workspace.appModel.myProfileId(),
            profileModel: workspace.appModel.get('profileModel'),
            objectId: campaignId,
            campaignId: campaignId
        }));
    },

    fetch: function (options) {
        if (!workspace.isAuthorized) {
            options.success();
            return;
        }
        var self = this;
        this.campaign.fetch().done(function () {
            self.imageOrVideoModel = new CampaignEdit.Models.ViewImage({
                errors: this.get('errors'),
                originalImage: {
                    imageUrl: self.campaign.get('viewImageUrl'),
                    photoId: self.campaign.get('viewImageId')
                },
                profileId: self.campaign.get('profileId'),
                albumTypeId: AlbumTypes.ALBUM_COMMENT,
                thumbnail: {
                    imageConfig: ImageUtils.ORIGINAL,
                    imageType: ImageType.PHOTO
                },
                aspectRatio: 640 / 360,
                width: 640,
                video: {
                    originalVideo: {
                        videoProfileId: self.campaign.get('videoProfileId'),
                        videoId: self.campaign.get('videoId'),
                        videoUrl: self.campaign.get('videoUrl')
                    }
                }
            });
            if (options) {
                options.success();
            }
        });
    }

});

CampaignEdit.Models.Interactive.Description = CampaignEdit.Models.Interactive.Base.extend({
    defaults: _.extend({}, CampaignEdit.Models.Interactive.Base.prototype.defaults, {
        activeIndex: 8,
        showFormAlways: true
    }),

    initialize: function () {
        CampaignEdit.Models.Interactive.Base.prototype.initialize.apply(this, arguments);

        var campaignId = this.get('editorData').campaignId || 0;
        this.campaign = (new CampaignEdit.Models.Shares({
            profileId: workspace.appModel.myProfileId(),
            ownerProfileId: workspace.appModel.myProfileId(),
            profileModel: workspace.appModel.get('profileModel'),
            objectId: campaignId,
            campaignId: campaignId
        }));
    },

    fetch: function (options) {
        if (!workspace.isAuthorized) {
            options.success();
            return;
        }
        this.campaign.fetch(options);
    }
});

CampaignEdit.Models.Interactive.Reward = CampaignEdit.Models.Interactive.Base.extend({
    defaults: _.extend({}, CampaignEdit.Models.Interactive.Base.prototype.defaults, {
        activeIndex: 9,
        showFormAlways: true
    }),

    initialize: function () {
        CampaignEdit.Models.Interactive.Base.prototype.initialize.apply(this, arguments);

        var campaignId = this.get('editorData').campaignId || 0;
        this.campaign = (new CampaignEdit.Models.Shares({
            profileId: workspace.appModel.myProfileId(),
            ownerProfileId: workspace.appModel.myProfileId(),
            profileModel: workspace.appModel.get('profileModel'),
            objectId: campaignId,
            campaignId: campaignId
        }));
    },

    fetch: function (options) {
        if (!workspace.isAuthorized) {
            options.success();
            return;
        }
        this.campaign.fetch(options);
    }
});

CampaignEdit.Models.Interactive.Counterparty = CampaignEdit.Models.Interactive.Base.extend({
    defaults: _.extend({}, CampaignEdit.Models.Interactive.Base.prototype.defaults, {
        activeIndex: 10,
        showFormAlways: true
    }),

    initialize: function () {
        CampaignEdit.Models.Interactive.Base.prototype.initialize.apply(this, arguments);

        var campaignId = this.get('editorData').campaignId || 0;
        this.campaign = (new CampaignEdit.Models.DraftCampaign({
            profileId: workspace.appModel.myProfileId(),
            campaignId: campaignId
        }));
    },

    fetch: function (options) {
        if (!workspace.isAuthorized) {
            options.success();
            return;
        }
        this.contractor = new CrowdFund.Models.ContractorEdit({campaignId: this.campaign.get('campaignId'), parentModel: this.campaign});
        this.campaign.fetch(options);
    }
});

CampaignEdit.Models.Interactive.Check = CampaignEdit.Models.Interactive.Base.extend({
    defaults: _.extend({}, CampaignEdit.Models.Interactive.Base.prototype.defaults, {
        activeIndex: 11
    }),

    initialize: function () {
        CampaignEdit.Models.Interactive.Base.prototype.initialize.apply(this, arguments);

        var campaignId = this.get('editorData').campaignId || 0;
        this.campaign = (new CampaignEdit.Models.Shares({
            profileId: workspace.appModel.myProfileId(),
            ownerProfileId: workspace.appModel.myProfileId(),
            profileModel: workspace.appModel.get('profileModel'),
            objectId: campaignId,
            campaignId: campaignId
        }));
    },

    fetch: function (options) {
        if (!workspace.isAuthorized) {
            options.success();
            return;
        }
        this.campaign.fetch(options);
    }
});

CampaignEdit.Models.Interactive.FinalModeration = CampaignEdit.Models.Interactive.Base.extend({
    defaults: _.extend({}, CampaignEdit.Models.Interactive.Base.prototype.defaults, {
        activeIndex: 12
    }),

    initialize: function () {
        var campaignId = this.get('editorData').campaignId || 0;
        this.campaign = (new CampaignEdit.Models.Shares({
            profileId: workspace.appModel.myProfileId(),
            ownerProfileId: workspace.appModel.myProfileId(),
            profileModel: workspace.appModel.get('profileModel'),
            objectId: campaignId,
            campaignId: campaignId
        }));
    },

    fetch: function (options) {
        if (!workspace.isAuthorized) {
            options.success();
            return;
        }
        this.campaign.fetch(options);
    }
});

CampaignEdit.Models.Interactive.FinalDraft = CampaignEdit.Models.Interactive.Base.extend({
    defaults: _.extend({}, CampaignEdit.Models.Interactive.Base.prototype.defaults, {
        activeIndex: 12
    }),

    initialize: function () {
        //BaseModel.prototype.initialize.apply(this, arguments);
        var campaignId = this.get('editorData').campaignId || 0;
        this.campaign = (new CampaignEdit.Models.Shares({
            profileId: workspace.appModel.myProfileId(),
            ownerProfileId: workspace.appModel.myProfileId(),
            profileModel: workspace.appModel.get('profileModel'),
            objectId: campaignId,
            campaignId: campaignId
        }));
    },

    fetch: function (options) {
        if (!workspace.isAuthorized) {
            options.success();
            return;
        }
        this.campaign.fetch(options);
    }
});
/*global CampaignEdit, CrowdFund, JobManager, DateUtils, Modal*/

CampaignEdit.Views.Header = BaseView.extend({
    template: '#campaign-edit-header-template',
    onContactsClick: function () {
        if (!this.profileSitesView) {
            var pId = this.model.get('creatorProfileId');
            if (!pId) {
                workspace.stats.tackNoProfileId('CampaignEditViewsHeader: ' + JSON.stringify(this.model.toJSON()));
            }
            this.profileSitesView = this.addChildAtElement('.js-profile-sites', new CrowdFund.Views.ProfileSites({
                profileId: pId
            }));
        }
        CrowdFund.Views.CampaignAuthor.prototype.onAddContactsClick.apply(this, arguments);
    }
});

CampaignEdit.Views.Tabs = BaseView.extend({
    template: '#campaign-edit-tabs-template',
    modelEvents: {
        destroy: 'dispose'
    },
    afterRender: function () {
        var $project = $('.project-create_edit-date');
        $project.removeClass('anim');
        var self = this;
        if (!self.activeTrack) {
            function track() {
                $.post("/api/profile/campaign-edit-track.json",
                        {campaignId: self.model.get('campaignId')}
                ).done(function (response) {
                    if (response && (response.success !== true)) {
                        workspace.appView.showInfoMessage(response.errorMessage, 10000);
                    }
                }).fail(function (response) {
                    console.log("May be not authorized");
                    console.log(response);
                });
            }
            track();
            self.activeTrack = window.setInterval(track, 60000);
        }
        this.$('a.project-create_tab_link').on('click', function (e) {
            self.model.onLocationChange(e, {
                success: function () {
                    workspace.onClickNavigate(null, e.target.href);
                }
            });
        });
    },
    dispose: function () {
        window.clearInterval(this.activeTrack);
        this.activeTrack = undefined;
        BaseView.prototype.dispose.apply(this, arguments);
    }
});

CampaignEdit.Views.Footer = BaseView.extend({
    template: '#campaign-edit-footer-template',
    modelEvents: {
        'destroy': 'dispose'
    },
    events: {
        'click .js-project-preview': 'togglePreview'
    },
    firstRender: true,
    lang: workspace && workspace.currentLanguage ? workspace.currentLanguage : 'ru',
    _preview: false,
    construct: function () {
        try {
            this.campaignPreview = this.addChildAtElement('.hidden', new CrowdFund.Views.CampaignPreview({
                model: this.model
            }));
        } catch (e) {
        }
        this.model.set("isStarted",
                ['ACTIVE', 'PAUSED', 'FINISHED'].indexOf(this.model.get('status')) > -1,
                {silent: true});
        this.model.set("isEditMode", true, {silent: true});
    },
    validate1: function (e) {
        this.validate(e, true);
    },
    validate: function (e, saveOnly) {
        var self = this;
        self._saving();
        CampaignEdit.uploaderController.alertIfExistsUploadingFiles({
            success: function () {
                workspace.appModel.unset('campaignModel');
                if (self.model.contractor) {
                    self.model.contractor.trigger('contractorSaveEvent', e, saveOnly, self, '/admin/contractor-add-extra-validate-and-change-campaign-status.json');
                } else {
                    self.model.saveAndValidateCampaign(self).done(function (resp) {
                        if (!(resp && resp.success)) {
                            self._fail();
                            if (!resp.errorClass) {
                                workspace.appView.showErrorMessage(resp.errorMessage);
                                self.parent.render();
                            }
                        } else {
                            self._saved(resp.result.edited);
                            self.parent.render();
                        }
                    }).fail(function () {
                        self._fail();
                    });
                }
            }
        });
    },
    _fail: function () {
        this.model.set({isSaving: false, saveError: true}, {silent: true});
        this.render();
    },
    _saving: function () {
        this.model.set({isSaving: true}, {silent: true});
        this.render();
    },
    _saved: function (edited) {
        this.model.set({isSaving: false, saveError: false, edited: edited},
                {silent: true});
        this.render();
    },
    saveCampaign: function (e) {
        var self = this;
        CampaignEdit.uploaderController.alertIfExistsUploadingFiles({
            success: function () {
                self._saving();
                if (self.model.contractor) {
                    self.model.contractor.trigger('contractorSaveEvent', e, true, self);
                } else {
                    self.model.saveCampaign().done(function (response) {
                        if (response && response.success) {
                            self._saved(response.result.edited);
                            self.parent.render();
                        } else {
                            self._fail();
                            if (!response.errorClass) {
                                workspace.appView.showErrorMessage(response.errorMessage);
                                self.model.navigateToValidation(response.errorMessage, response.fieldErrors, response.errorClass);
                                self.parent.render();
                            }
                        }
                    }).fail(function () {
                        self._fail();
                    });
                }
            }
        });
    },
    onUndoChangesClick: function () {
        var self = this;
        Modal.showConfirm("Вы действительно хотите отменить изменения и вернуться к опубликованной версии проекта?", null, {
            success: function () {
                Modal.startLongOperation();
                Backbone.sync('update', null, {
                    url: '/admin/draft-campaign-undo-changes.json',
                    data: {
                        campaignId: self.model.get('campaignId')
                    }
                }).done(function () {
                    workspace.reloadPage();
                }).always(function () {
                    Modal.finishLongOperation();
                });
            }
        });
    },
    fixedBtn: function (selector) {
        var blockWrap = $(selector);
        if (!blockWrap.length) {
            return;
        }
        var blockOuterHeight = blockWrap.outerHeight();
        blockWrap.height(blockOuterHeight);

        function fixedScroll() {
            var position = blockWrap.offset().top + blockOuterHeight;
            var screenHeight = $(window).height();
            var winBottom = $(window).scrollTop() + screenHeight;
            if (winBottom > position) {
                blockWrap.removeClass('fixed');
            } else {
                blockWrap.addClass('fixed');
            }
        }

        $(window).bind('scroll.fixedBtn resize.fixedBtn', function () {
            fixedScroll();
        });
        fixedScroll();
    },
    togglePreview: function () {
        this._preview = !this._preview;
        var render = true;
        if (!this.campaignPreview) {
            this.campaignPreview = this.addChildAtElement('.hidden', new CrowdFund.Views.CampaignPreview({
                model: this.model
            }));
            render = false;
        }
        if (render && this._preview) {
            this.model.getDataToSave();
            this.campaignPreview.render();
        }
        var buttons = $('[data-event-click="validate"], [data-event-click="validate1"], #next_tab, #prev_tab, [data-event-click="saveCampaign"]');

        if (this._preview) {
            buttons.hide();
        } else {
            buttons.show();
        }

        this.$('.js-project-preview').toggleClass('active', this._preview).text(!this._preview ? this.words(1) : this.words(2));
        $('.project-view-card_share').hide();
        $('#center-container > .wrap').toggle(!this._preview);
        $('.model-preview').toggle(this._preview).html(this._preview ? this.$('.hidden').html() : '');
    },
    scrollTo: function (height) {
        $(window).scrollTop(height + 100);
        $('body, html').animate({scrollTop: height}, 300);
    },
    afterRender: function () {
        this.fixedBtn('.project-create_action-wrap');
        var $firstError = $('.error:first');
        if ($firstError.length && this.firstRender) {
            this.scrollTo($firstError.offset().top);
            this.firstRender = false;
        }
        if (!this.preview) {
            $('[data-event-click="validate"], [data-event-click="validate1"], [data-event-click="saveCampaign"]').css({visibility: ''});
            this.$('.js-project-preview').removeClass('active').text(!this._preview ? this.words(1) : this.words(2));
            $('#center-container > .wrap').toggle(!this._preview);
            $('.model-preview').toggle(this._preview).html('');
        }
        var self = this;
        self.$('a.btn').on('click', function (e) {
            self.model.onLocationChange(e, {
                success: function () {
                    workspace.onClickNavigate(null, e.target.href);
                }
            });
        });
    },
    words: function (flag) {
        var v1, v2;
        if (flag == 1) {
            if (this.lang == 'ru') {
                v1 = 'Предпросмотр';
            } else {
                v1 = 'Preview';
            }
            return v1;
        } else if (flag == 2) {
            if (this.lang == 'ru') {
                v2 = 'Вернуться в редактор';
            } else {
                v2 = 'Back to the editor';
            }
            return v2;
        }
    }
});

CampaignEdit.Views.CampaignValidatedRow = BaseView.extend({
    className: 'project-create_row',
    modelAttr: 'input',
    afterRender: function () {
        var hasError = this.model.get('errors')[this.modelAttr];
        this.$el.toggleClass('error', !!hasError);
    }
});

CampaignEdit.Views.BaseInput = CampaignEdit.Views.CampaignValidatedRow.extend({
    className: 'project-create_row',
    template: '',
    maxValue: 99999999,
    modelEvents: {
        destroy: 'dispose'
    },
    currentCaretPosition: 0,
    needRestoreCaretAfterRender: false,
    events: {
        'change input': 'onInputChange',
        'keydown input': 'onKeyDown'
    },
    onKeyDown: _.debounce(function () {
        if (this.needRestoreCaretAfterRender && this.getInput().attr('id')) {
            sessionStorage.setItem('lastFocusedInputField', this.getInput().attr('id'));
            this.storeCaretPosition(this.getInput());
        }
        this.onInputChange();
    }, 1000),
    onInputChange: function () {
        var $input = this.getInput();
        var value = $input.val();
        if ($input.data().parse == "number") {
            var number = parseInt(value.replace(/\D/g, ''), 10) || 0;
            if (number > this.maxValue) {
                number = this.maxValue;
            }
            if (value != number.toString()) {
                // don't create zero from nowhere
                $input.val(number || '');
            }
            value = number;
        }
        this.model.set(this.modelAttr, value);
    },
    afterRender: function () {
        CampaignEdit.Views.CampaignValidatedRow.prototype.afterRender.apply(this, arguments);
        var $input = this.getInput();
        var $count = this.$('.rest-count');
        var status = this.model.get('status');
        var enableNegative = false;
        if (status == 'ACTIVE' || status == 'PAUSED' || status == 'FINISHED') {
            enableNegative = true;
        }
        if ($count.length) {
            $input.limitInput({
                limit: $input.data().limit || 0,
                remTextEl: $count,
                toggleCounter: false,
                warnIfZeroComes: false,
                enableNegative: enableNegative
            });
        }
        if (this.needRestoreCaretAfterRender && (this.getInput().attr('id') == sessionStorage.getItem('lastFocusedInputField'))) {
            this.restoreCaretPosition();
        }
    },
    getInput: function () {
        return this.$('input');
    },
    storeCaretPosition: function (el) {
        this.currentCaretPosition = el.selection().start || 0;
    },

    restoreCaretPosition: function () {
        var $elem = this.getInput();

        if($elem != null && $elem[0]) {
            var elem = $elem[0];
            var caretPos = this.currentCaretPosition;
            if(elem.createTextRange) {
                var range = elem.createTextRange();
                range.move('character', caretPos);
                range.select();
            }
            else {
                if(elem.selectionStart) {
                    elem.focus();
                    elem.setSelectionRange(caretPos, caretPos);
                }
                else
                    elem.focus();
            }
        }
    }
});

CampaignEdit.Views.TextArea = CampaignEdit.Views.BaseInput.extend({
    events: {
        'change textarea': 'onInputChange',
        'keydown textarea': 'onKeyDown'
    },
    afterRender: function () {
        CampaignEdit.Views.BaseInput.prototype.afterRender.call(this);
        this.getInput().elastic();
    },
    getInput: function () {
        return this.$('textarea');
    }
});

CampaignEdit.Views.Date = CampaignEdit.Views.BaseInput.extend({
    events: {
        'change input': 'onInputChange',
        'keydown input': 'onKeyDown',
        'date-change input': 'onInputChange'
    },
    onInputChange: function () {
        if (this.$('input').attr("value")) {
            var value = this.$('input').attr("data-field-value");
            this.model.set(this.modelAttr, value);
        } else {
            this.model.unset(this.modelAttr);
        }

    }
});

CampaignEdit.Views.BaseInputOrCheckbox = CampaignEdit.Views.BaseInput.extend({
    events: _.extend({}, CampaignEdit.Views.BaseInput.prototype.events, {
        'change input[type=checkbox]': 'onCheckboxChange'
    }),
    oldValue: null,
    zeroValue: 0,
    defaultValue: 10,
    onCheckboxChange: function () {
        var $checkbox = this.getCheckbox();
        var bValue = !!$checkbox.attr('checked');
        if (bValue) { // critical case
            this.oldValue = this.model.get(this.modelAttr);
            this.model.set(this.modelAttr, this.zeroValue);
        } else { // restore old value or set default
            this.model.set(this.modelAttr, this.oldValue || this.defaultValue);
        }
        this.render();
    },
    afterRender: function () {
        CampaignEdit.Views.BaseInput.prototype.afterRender.call(this);
        this.getCheckbox().checkbox({wrapperClassName: 'checkbox-row flat-ui-control'});
    },
    getCheckbox: function () {
        return this.$('input[type=checkbox]');
    }
});

CampaignEdit.Views.BaseCheckboxAccordion = BaseView.extend({
    className: 'project-create_row',
    /**
     * selecting element to slide up/down
     */
    selectorToToggle: '',
    modelAttr: 'checkboxAccordion',
    modelEvents: {
        destroy: 'dispose'
    },
    events: {
        'change input[type=checkbox]': 'toggle'
    },
    // override initialize instead of construct to prevent need for call super in overridden construct
    initialize: function () {
        BaseView.prototype.initialize.apply(this, arguments);
        this._isBooleanAttr = _.isBoolean(this.model.get(this.modelAttr));
        // this values can be overridden in class definition
        this.trueValue = this.trueValue || this._isBooleanAttr ? true : '';
        this.falseValue = this.falseValue || this._isBooleanAttr ? false : '';
    },
    toggle: function () {
        this._checked = !this._checked;
        if (!this._checked) { // erase value or set it to false
            this._oldValue = this.model.get(this.modelAttr);
            this.model.set(this.modelAttr, this.falseValue);
        } else { // restore old value or set default
            this.model.set(this.modelAttr, this._oldValue || this.trueValue);
        }
        this.slideToggle(this._checked);
    },
    slideToggle: function (bValue) {
        if (bValue) {
            this.$(this.selectorToToggle).slideDown(function () {
                if ($('[data-anchor=questionToBuyer]')) {
                    $('[data-anchor=questionToBuyer]').css('overflow', 'inherit');
                    $('.dropdown-menu > .active').removeClass('active');
                }
            });
        } else {
            this.$(this.selectorToToggle).slideUp();
        }
    },
    afterRender: function () {
        this._checked = !!this.model.get(this.modelAttr);
        $('input[type=checkbox]').checkbox({wrapperClassName: 'checkbox-row flat-ui-control'});
        this.slideToggle(this.model.get(this.modelAttr));
    }
});

CampaignEdit.Views.BaseSelectAccordion = BaseView.extend({
    selectorToToggle: '',
    modelAttr: 'selectAccordion',
    modelEvents: {
        destroy: 'dispose'
    },
    events: {
        'change select': 'toggle',
        'change #countryId, #regionId, #cityId': 'selectCountry'
    },
    initialize: function () {
        BaseView.prototype.initialize.apply(this, arguments);
    },
    toggle: function () {
        if (this.$('[name=countryId]').val() != 0) {
            this.$(this.selectorToToggle).slideDown();
            $('[data-anchor=countrySelect]').attr('style', 'display: block;');
        } else {
            this.$(this.selectorToToggle).slideUp();
        }
    },
    selectCountry: _.debounce(function (event) {
        var el = event.target;
        this.model.set(el.name, el.value);
        if (el.name == 'countryId') {
            this.model.set({
                regionNameRus: '',
                regionNameEng: '',
                cityNameRus: '',
                cityNameEng: ''
            });
            this.$('[name=cityNameRus]').attr('disabled', true);
            this.$('[name=cityNameEng]').attr('disabled', true);
        } else if (el.name == 'regionId') {
            this.model.set('regionNameRus', this.$('[name=regionNameRus]').val());
            this.model.set('regionNameEng', this.$('[name=regionNameEng]').val());

            if (el.value != 0) {
                this.$('[name=cityNameRus]').attr('disabled', false);
                this.$('[name=cityNameEng]').attr('disabled', false);
            } else {
                this.$('[name=cityNameRus]').attr('disabled', true);
                this.$('[name=cityNameEng]').attr('disabled', true);
            }
        } else if (el.name == 'cityId') {
            this.model.set('cityNameRus', this.$('[name=cityNameRus]').val());
            this.model.set('cityNameEng', this.$('[name=cityNameEng]').val());
        }
    }, 500)
});

CampaignEdit.Views.ImageField = CrowdFund.Views.ImageField.extend({
    createLoadView: function () {
        return new CampaignEdit.Views.ImageField.LoadView({model: this.model, controller: this.controller});
    }
});

CampaignEdit.Views.ImageField.LoadView = CrowdFund.Views.ImageField.LoadView.extend({
    getFileUploader: function () {
        return CampaignEdit.uploaderController.getFileUploader(this.model, this.getFileUploaderOptions());
    },
    removeFileUploader: function () {
        CampaignEdit.uploaderController.removeFileUploaderIfNotUploading(this.model.uploaderName);
    }
});

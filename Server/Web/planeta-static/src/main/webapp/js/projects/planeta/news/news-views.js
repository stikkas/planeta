/*globals News, BaseRichView, Comments, DefaultMutableListView, moduleLoader, StringUtils, Modal, BlogUtils, ImageUtils, ImageType*/

News.Views.Post = BaseRichView.extend({
    className: 'feed_i',
    template: '#user-news-item-template',
    bindOptions: {
        dontBindPhoto: true
    },

    construct: function () {
        if (this.model.get('type') === 'POST') {
            this.addChildAtElement('.comments-list', new Comments.Views.NewsModalComments({
                model: this.model.comments
            }));
        }
    },
    onClick: function (e) {
        switch (this.model.get('type')) {
            case 'POST':
                this.onPostClick(e);
                break;
        }
    },

    beforeRender: function () {
        BaseRichView.prototype.beforeRender.apply(this, arguments);
        var postUrl;
        if (this.model.get('campaignId')) {
            postUrl = ProfileUtils.getAbsoluteCampaignLink(this.model.get('webCampaignAlias')) + '/updates!post' + this.model.get('postId');
        } else {
            postUrl = ProfileUtils.getAbsoluteUserLink(this.model.get('profileId'), this.model.get("authorAlias")) + '/news!post' + this.model.get('objectId');
        }
        this.model.set({
            postUrl: postUrl
        }, {silent: true});
    },

    onPostClick: function (e) {
        e.preventDefault();
        News.Views.Posts.prototype.showPostModal.call(this, this.model.get('objectId'));
    },

    renderView: function () {
        return BaseRichView.prototype.renderView.apply(this, arguments);
    },

    showSharing: function () {
        var sharingIsVisible = this.model.get('sharingIsVisible');
        this.model.set({sharingIsVisible: !sharingIsVisible});
    },

    afterRender: function () {
        BaseRichView.prototype.afterRender.apply(this, arguments);
        var self = this;
        this.$('img').on('click', function (e) {
            e.stopPropagation();
            self.onPostClick(e);
        });
        this.addSharing();
    },

    addSharing: function () {
        if (!this.sharingData) {
            var post = this.model;
            var data = {
                title: post.get('postName'),
                counterEnabled: false,
                hidden: false,
                className: 'donate-sharing'
            };

            var isCampaignPost = post.get('campaignId') > 0;
            data.url = isCampaignPost
                ? ProfileUtils.getAbsoluteUserLink(this.model.get('profileId'), this.model.get("authorAlias")) + '/news/' + post.get('postId')
                : ProfileUtils.getAbsoluteUserLink(this.model.get('profileId'), this.model.get("authorAlias")) + '/news!post' + post.get('postId');
            // get description
            var description = BlogUtils.trimContentHtml(post.get('headingText'));
            if (description) {
                description = StringUtils.cutString(description, BlogUtils.maxContentLength);
            } else {
                description = data.title;
            }
            data.description = description;
            // get image
            var photo = this.$el.find('[data-role="photo"]');
            data.image = photo.length ? photo.data('json').image : post.get(isCampaignPost ? 'campaignImageUrl' : 'authorImageUrl');

            this.sharingData = data;
        }

        this.$('.feed_sharing').share(this.sharingData);
    },

    addCharitySharing: function () {
            var post = this.model;
            var data = {
                title: post.get('title'),
                counterEnabled: false,
                hidden: false,
                className: 'donate-sharing'
            };

            // get description
            var description = BlogUtils.trimContentHtml(post.get('headingText'));
            if (description) {
                description = StringUtils.cutString(description, BlogUtils.maxContentLength);
            } else {
                description = data.title;
            }
            data.description = description;
            // get image
            var photo = this.$el.find('[data-role="photo"]');
            data.image = photo.length ? photo.data('json').image : post.get('authorImageUrl');

        this.$('.feed_sharing').share(data);
    }

});

News.Views.Posts = DefaultScrollableListView.extend({
    className: 'feed_list',
    itemViewType: News.Views.Post,
    emptyListTemplate: '#empty-user-news-template',
    construct: function () {
        var anchor = workspace.navigationState.get('anchor');
        if (anchor && StringUtils.startsWith(anchor, 'post')) {
            var postId = parseInt(anchor.replace('post', ''), 10);
            this.showPostModal(postId);
        }
    },

    showPostModal: function (postId) {
        var model = this.model;
        var post = new News.Models.SinglePost({
            postId: postId,
            type: 'POST',
            authorProfileId: model.get('authorProfileId'),
            campaignId: model.get('campaignId'),
            profileId: model.get('profileId')
        }, {
            collection: model.collection
        });

        Modal.showDialogByViewClass(News.Views.Modal.CampaignPost, post);
    }
});

Comments.Views.PostItem = Comments.Views.CampaignItem.extend({
    template: '#post-comment-item-template'
});

Comments.Views.PostComments = Comments.Views.CampaignComments.extend({
    commentItemViewType: function () {
        return Comments.Views.PostItem;
    }
});

Comments.Views.NewsModalComments = Comments.Views.ModalComments.extend({
    construct: function () {
        var commentModel = this.model;
        if (commentModel.get('showCounters')) {
            this.addChildAtElement('.attach-post-option', new Comments.Views.CommentsCounter({
                model: commentModel.commentableObject
            }));
        }
        this.addChildAtElement('.attach-comment-placeholder', new Comments.Views.PostComments({
            model: commentModel
        }));
        this.model = commentModel.commentableObject;
    }
});


News.Views.TinyEditor = BaseView.extend({
    className: 'edit-blog2',
    template: '#user-create-post-tiny-mce-template',
    generatedId: null,

    generateId: function () {
        window.globalTinyId = window.globalTinyId || 0;
        window.globalTinyId += 1;
        return 'tinymce-body-' + window.globalTinyId;
    },

    createEditorModel: function (model, $element) {
        return new News.Models.TinyEditor(model, $element);
    },

    initTiny: function () {
        var model = this.model;
        var self = this;
        var $dfd = $.Deferred();
        if (News.Views._tinyViewInited) {
            $dfd.resolve();
            return $dfd;
        }

        this.generatedId = this.generatedId || this.generateId();
        moduleLoader.loadModule('tiny-mce-campaigns').done(function () {
            var $element = self.$('.tinymce-body').attr({id: self.generatedId});
            var tinyModel = self.editorModel || self.createEditorModel(model, $element);
            var onDisable = function () {
                self.render();
            };
            setTimeout(function () {
                tinyModel.init($element);
                self.editorModel = tinyModel;
                $dfd.resolve();
                News.Views.initTinyView(onDisable);
                $('.edit-blog-body').fadeIn();
                $('.edit-blog-body-stub').hide();
            }, 100);
            self.$('.tb-bottom').hide();
        });
        return $dfd;
    },

    dispose: function () {
        if (this.editorModel) {
            News.Views.disableTinyView();
            this.editorModel.destruct();
        }
    },

    modelEvents: {
        'destroy': 'dispose'
    }
});

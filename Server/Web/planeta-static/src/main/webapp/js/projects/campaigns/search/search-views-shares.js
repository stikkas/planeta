/*globals Search, Campaign, DefaultScrollableListView, moduleLoader, Banners, StringUtils, CampaignUtils, Header, PrivacyUtils, DefaultListView */
var ShareSearch = {
    Models: {},
    Views: {}
};

ShareSearch.Views.ResultItem = Search.Views.ResultItem.extend({
    className: 'pd-condition-i donate-condition-item',
    tagName: 'div',
    template: '#share-search-item-template',
    injectionHtml: 'projectCardHtml',
    injectionFunc: 'projectCard'
});

ShareSearch.Views.ContentView = BaseView.extend({
    template: '#bb-reward-search-page',

    construct: function () {
        $(window).on("stateChanged", function() {
            //Костыль
            $('.project-list-container').remove();
        });
    },

    afterRender: function () {
        Vue.use(VueNumeric.default);

        new Vue({
            el: '#center-container',
            i18n: new VueI18n({
                locale: workspace.currentLanguage,
                messages: workspace.i18messages
            })
        });

        window.toTopStart = 0;
        window.toTopOffset = 0;

        var toTop = $('.to-top');
        var toTopBlock = $('.to-top_block');
        var winHeight = window.innerHeight;

        toTopBlock.css({paddingTop: window.toTopOffset});

        $(window).on('resize', function () {
            winHeight = window.innerHeight;
        });

        $(window).on('scroll', function () {
            var winTop = $(window).scrollTop();

            if ( winTop > winHeight + window.toTopStart ) {
                toTop.addClass('to-top__show');
            } else {
                toTop.removeClass('to-top__show');
            }
        });

        toTop.on('click', function () {
            $(window).scrollTop(50);
            $('html, body').animate({scrollTop: 0}, 200, null);
        });
    }
});

ShareSearch.Views.Projects = DefaultScrollableListView.extend({
    emptyListTemplate: '#campaign-search-results-empty-container-template',
    pagerLoadingTemplate: '#search-scrollable-list-loader-template',

    modelEvents: _.extend({
        "change:fetching": "onFetchingChanged"
    }, DefaultScrollableListView.prototype.modelEvents),

    onAdd: function (campaign) {
        return DefaultScrollableListView.prototype.onAdd.apply(this, arguments);
    },

    onFetchingChanged: function () {
        this.onAdd();
        if (this.model.get('fetching')) {
            this.collection.reset();
            this.renderPagerLoadingElement();
        } else {
            this.addPagerElement(null);
        }
    }
});

ShareSearch.Views.Results = BaseView.extend({
    tagName: 'div',
    id: 'resultlist',
    className: 'wrap',
    template: '#share-search-results-container-template',
    injectionHtml: "searchPageHtml",
    injectionAnchor: ".js-campaign-search-injection",

    construct: function () {
        this.addChildAtElement('.project-list-block', new ShareSearch.Views.Projects({
            id: 'search-results',
            className: 'project-card-list',
            collection: this.model.searchResults,
            itemViewType: ShareSearch.Views.ResultItem,
            model: this.model
        }));
    }
});

ShareSearch.Views.Query = Search.Views.Query.extend({
    className: 'n-search',
    template: '#share-search-block-template',

    afterRender: function () {
        this.$el.find('input[id="query"]').focus();
    }
});


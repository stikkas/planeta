/*globals ShopUtils, Form*/

var ShopAdmin = {};
ShopAdmin.Models = {};

ShopAdmin.Models.DeliveryListModel = BaseModel.extend({
    initialize: function(options) {
        this.set({linkedDeliveries: new BaseCollection(options || [], {limit: 0})});
    }
});

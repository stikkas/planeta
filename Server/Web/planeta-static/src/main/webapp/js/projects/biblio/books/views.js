/**
 * Отображает найденные книги
 */
Biblio.Views.Books = DefaultListView.extend({
    template: "#biblio-wizard_cont",
    el: '.biblio-wizard_cont',
    itemViewType: Biblio.Views.Book,
    afterRender: function () {
        if (Biblio.data.bin)
            Biblio.data.bin.trigger('change');
    },
    /**
     * Добавляет в корзину по одному невыбранному изданию
     * из найденных, т.е. тех которые отображаются на странице
     */
    checkAll: function () {
        Biblio.data.bin.addBooks(this.collection.models);
    }
});
/**
 * Абстрактрый класс для отображения категорий 
 */
Biblio.Views.Tag = BaseView.extend({
    events: {
        'click span': 'choose'
    },
    choose: function () {
        _.each(['#wizard-main_tags', '.biblio-wizard-tabs_drop-list'], function (it) {
            $(it).children().each(function () {
                var it = $(this);
                if (it.hasClass('active')) {
                    it.removeClass('active');
                    return false;
                }
            });
        });
        this.$el.addClass('active');
        Biblio.data.filter.set('tagId', this.model.get('tagId'));
    }
});

/**
 * Основная категория 
 */
Biblio.Views.MainTag = Biblio.Views.Tag.extend({
    template: '#biblio-wizard-main_tag',
    className: 'biblio-wizard-tabs_i'
});

/**
 * Дополнительная категория, отображается только по нажатию кнопки "Еще" 
 */
Biblio.Views.SecondaryTag = Biblio.Views.Tag.extend({
    template: '#biblio-wizard-secondary_tag',
    className: 'biblio-wizard-tabs_drop-i'
});

/**
 * Меню с категориями и полем ввода для поиска
 */
Biblio.Views.SearchFilter = BaseView.extend({
    template: '#books-filter-template',
    el: '.biblio-wizard-filter',
    events: {
        'click .biblio-wizard-tabs_drop-switch': 'openMore',
        'keyup input': 'changeQuery'
    },
    construct: function () {
        var self = this,
                mainCount = 0,
                maxMainTags = 4,
                tagId = self.model.get('tagId'),
                found = false,
                first = self.addChildAtElement('#wizard-main_tags', new Biblio.Views.MainTag({
                    model: new BaseModel({tagId: '', name: 'Все'})
                }));
        _.each(self.model.get('tags'), function (data) {
            var view;
            if (data.primary && mainCount < maxMainTags) {
                view = self.addChildAtElement('#wizard-main_tags', new Biblio.Views.MainTag({
                    model: new BaseModel(data)
                }));
                ++mainCount;
            } else {
                view = self.addChildAtElement('.biblio-wizard-tabs_drop-list', new Biblio.Views.SecondaryTag({
                    model: new BaseModel(data)
                }));
            }
            if (!found && data.tagId === tagId) {
                found = true;
                view.$el.addClass('active');
            }
        });
        if (!(tagId && found)) {
            first.$el.addClass('active');
            if (tagId)
                setTimeout(function () {
                    self.model.unset('tagId');
                });
        }
    },
    changeQuery: _.debounce(function (e) {
        this.model.set('query', $(e.currentTarget).val());
    }, 1000),
    openMore: function (e) {
        e.stopPropagation();
        this.$('.biblio-wizard-tabs_drop').addClass('open');
    }
});

/**
 * Надпись под корзиной "столько-то выбранных изданий пойдут в библиотеки" 
 */
Biblio.Views.AddInfo = BaseView.extend({
    template: '#wizard_add_info-template',
    el: '.biblio-wizard_add-info'
});

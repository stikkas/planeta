/*globals ProductEdit, UploadController,moduleLoader*/

ProductEdit.Views.ProductPhotoItem = BaseView.extend({
    tagName: 'li',
    className: 'donate-photo-item',
    template: '#product-edit-photo-item-template',

    viewEvents: {
        'click .select-photo img': 'onPhotoClicked',
        'click .crop-photo': 'onCropPhotoClicked',
        'click .delete-photo': 'onDeletePhotoClicked',
        'click .upload-photo': 'onUploadPhotoClicked'
    },

    afterRender: function () {
        if (this.model.get('selected')) {
            this.$el.addClass('active');
        } else {
            this.$el.removeClass('active');
        }
    }
});

ProductEdit.Views.ProductPhotoList = BaseListView.extend({
    tagName: 'ul',
    className: 'donate-photo-block',
    itemViewType: ProductEdit.Views.ProductPhotoItem,

    afterRender: function () {
        var self = this;
        var collection = this.collection;
        var startIndex;

        this.$el.sortable({
            items: ".donate-photo-item",
            start: function(event, ui) {
                ui.placeholder.html(ui.helper.html());
                startIndex = ui.item.index();
            },

            update: function(event, ui) {
                var stopIndex = ui.item.index();
                if(stopIndex !== startIndex) {
                    self.trigger("onSwapPhotos", startIndex, stopIndex);
                }
            }
        });
    }
});

ProductEdit.Views.ProductPhotoContainer = BaseView.extend({
    template: '#product-edit-photo-template',

    construct: function() {
        this.addChildAtElement('.donate-photo', new ProductEdit.Views.ProductPhotoList({
            collection: this.model.photos,
            controller: this.model
        }));
        
        this.bind('onPhotoClicked', this.onPhotoClicked, this);
        this.bind('onCropPhotoClicked', this.onCropPhotoClicked, this);
        this.bind('onDeletePhotoClicked', this.onDeletePhotoClicked, this);
        this.bind('onUploadPhotoClicked', this.onUploadPhotoClicked, this);
        this.bind('onSwapPhotos', this.onSwapPhotos, this);
    },

    onSwapPhotos: function (startIndex, stopIndex) {
        this.model.swapPhotos(startIndex, stopIndex);
    },

    onPhotoClicked: function (view) {
        this.model.selectPhoto(view.model);
    },

    onCropPhotoClicked: function (view) {
        var self = this;
        var photoModel = view.model;
        photoModel.set({
            clientId: workspace.appModel.myProfileId(),
            profileId: workspace.appModel.myProfileId(),
            albumTypeId: 7,
            aspectRatio: 1,
            coverImageUrl: this.model.get("coverImageUrl"),
            callbackUpdateCover: function(data) {
                self.model.set({coverImageUrl : data}, {silent: true});
            }
        }, {silent: true});

        var view = new CrowdFund.Views.ImageField.ModalCropView({
            model: photoModel
        });
        view.render();
    },

    onDeletePhotoClicked: function (view) {
        // remove tooltips
        this.$(".twipsy").removeClass("in");
        this.model.deletePhoto(view.model);
    },

    onUploadPhotoClicked: function () {
        var model = this.model;
        moduleLoader.loadModule('upload').done(function () {
            UploadController.showUploadProductImages(
                model.get('merchantProfileId'), model.get('albumId') || 0,
                function (result) {

                    if (result.status && result.status == 'ERROR') {
                        workspace.appView.showErrorMessage('Произошла ошибка при загрузке фото.');
                        return;
                    }
                    model.savePhotos(result);
                }
            );
        });
    }
});


ProductEdit.Views.ProductDescription = BaseView.extend({
    template: '#product-edit-description-template',
    events: {
        'keyup #name': 'nameChanged',
        'keyup #textForMail': 'textForMailChanged'
    },

    modelEvents: {
        'destroy': 'dispose'
    },

    nameChanged: _.debounce(function() {
        this.model.set({'name': $('#name').val()});
    }, 200),

    textForMailChanged: _.debounce(function() {
        this.model.set({'textForMail': $('#textForMail').val()});
    }, 200),

    afterRender: function() {
        var tinyModel;
        moduleLoader.loadModule('attachments');
        moduleLoader.loadModule('tiny-mce-campaigns').done(function () {

            var modelClass = function (campaignModel) {
                _.extend(this, new TinyMcePlaneta.EditorModel(campaignModel), {
                    config: TinyMcePlaneta.getCampaignEditorConfiguration(),
                    width: 550,
                    height: 400,
                    saveToParentModel: function () {

                    },
                    destructPlugins: function () {
                        if (tinymce.activeEditor.plugins.planetafullscreencampaign.fullScreen) {
                            tinymce.activeEditor.execCommand("planetafullscreencampaign");
                        }
                    }
                });
            };

            tinyModel = new modelClass();
            tinyModel.init($('.tinymce-body'));
        });
    }
});



ProductEdit.Views.Preorder = BaseView.extend({
    template: '#product-edit-preorder-template',

    afterRender: function() {
        var self = this;
        var input = this.$("#js-human-date");

        input.datetimepicker({
            format: 'DD.MM.YYYY'
        });

        if (this.model.get('startSaleDate')) {
            input.data("DateTimePicker").date(new Date(this.model.get('startSaleDate')));
        }
    }
});



ProductEdit.Views.BindProfile = BaseView.extend({
    template: '#product-bind-profile-template',
    construct: function () {
        if (!this.model.get('referrerWebAlias')) {
            this.model.set('referrerWebAlias', this.model.get('referrer') ? this.model.get('referrer').webAlias : '', {silent: true});
        }
    },

    onBindProductToProfileClicked: function () {
        var self = this;
        var showOnCampaign = (self.$('#showOnCampaign').val() === 'true');
        var campaignIdsString = self.$('#campaignIdsString').val();
        Backbone.sync('update', null, {
            url: '/admin/product-bind-to-profile.json',
            data: {
                productId: this.model.get('productId'),
                referrerWebAlias: this.$('#referrerWebAlias').val(),
                showOnCampaign: showOnCampaign,
                campaignIdsString: campaignIdsString
            }
        }).done(function (response) {
            if (response && response.success) {
                self.model.set({
                    referrer: response.result,
                    referrerWebAlias: response.result.webAlias,
                    showOnCampaign: showOnCampaign,
                    campaignIdsString: campaignIdsString
                });
                workspace.appView.showSuccessMessage('Автор успешно привязан к товару');
            } else {
                workspace.appView.showErrorMessage('Произошла ошибка');
            }
        }).fail(function () {
            workspace.appView.showErrorMessage('Произошла ошибка');
        });
    },

    onReferrerWebAliasChanged: function () {
        this.model.set('referrerWebAlias', this.$('#referrerWebAlias').val(), {silent: true});
    },

    onShowOnCampaignChanged: function () {
        this.model.set('showOnCampaign', !!this.$('#showOnCampaign').attr('checked'));
    },

    onCampaignIdsStringChanged: function () {
        this.model.set('campaignIdsString', this.$('#campaignIdsString').val(), {silent: true});
    }
});

ProductEdit.Views.BindDonate = BaseView.extend({
    template: '#product-bind-donate-template',

    onDonateIdChanged: function () {
        this.model.set('donateId', this.$('#donateId').val(), {silent: true});
    },

    onFindDonateByReferrerClicked: function() {
        var self = this;
        var referrer;
        if (this.model.get('referrer')) {
            referrer = this.model.get('referrer').webAlias;
        } else {
            referrer = $('#referrerWebAlias').val();
        }

        if (referrer) {
            Backbone.sync('read', null, {
                url: '/admin/product-find-donate-by-profile.json',
                data: {
                    referrerWebAlias: referrer
                }
            }).done(function (response) {
                    if (response && response.success) {
                        self.model.set({
                            donateId: response.result
                        });
                        workspace.appView.showSuccessMessage('Найден товар ' + response.result);
                    } else {
                        workspace.appView.showErrorMessage(response.errorMessage);
                    }
                }).fail(function () {
                    workspace.appView.showErrorMessage('Произошла ошибка');
                });
        }
    }
});

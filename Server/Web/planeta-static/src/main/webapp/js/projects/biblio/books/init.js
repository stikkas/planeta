$(document).ready(function () {
Biblio.data = Biblio.data || {};
Biblio.data.stepOne = true;
Biblio.data.bin = new Biblio.Models.Bin();
Biblio.data.filter = new Biblio.Models.SearchFilter();
Biblio.data.books = new Biblio.Collections.Book({url: '/api/public/books-search.json', limit: 12});
Biblio.data.initParams = Biblio.Utils.parseSearch();
Biblio.data.booksView = new Biblio.Views.Books({
    el: '#biblio-found-books',
    collection: Biblio.data.books
});

Biblio.data.filter.on('change', function () {
    var query = Biblio.data.filter.get('query'),
            tagId = Biblio.data.filter.get('tagId');
    Biblio.data.books.data = {
        query: query,
        tagId: tagId
    };
    Biblio.data.books.load({
        success: function () {
            if (Biblio.data.books.length) {
                Biblio.data.booksView.$el.show();
                Biblio.data.booksView.render();
            } else {
                Biblio.data.booksView.$el.hide();
                Biblio.data.booksView.clear();
            }
            var searchInput = $('input[name="query"]').focus();
            searchInput.val(searchInput.val());
        }
    });
    workspace.changeUrl('/books' + (query ? '?query=' + query : '') + (tagId ? (query ? '&' : '?') + 'tagId=' + tagId : ''));
});

Biblio.data.filter.fetch().success(function (data) {
    var obj = {tags: data};
    if (Biblio.data.initParams.query)
        obj.query = decodeURI(Biblio.data.initParams.query);
    var tagId = parseInt(Biblio.data.initParams.tagId);
    if (!isNaN(tagId))
        obj.tagId = tagId;

    Biblio.data.filter.set(obj);
    new Biblio.Views.SearchFilter({
        model: Biblio.data.filter
    }).render();
});

// Инициализация корзины
Biblio.data.bin.fetch().success(function () {
    new Biblio.Views.LibraryTotalBin({
        model: Biblio.data.bin
    }).render();
    new Biblio.Views.Bin({
        model: Biblio.data.bin
    }).render();
    var addInfo = new BaseModel();
    $('.biblio-wizard-head_i.js-third').addClass('disabled');
    addInfo.on('change', function () {
        var needDisable = addInfo.get('count') <= 0;
        $('.biblio-wizard_btn > .btn-primary').prop('disabled', needDisable);

        if (needDisable){
            $('.biblio-wizard-head_i.js-second').addClass('disabled');
        } else {
            $('.biblio-wizard-head_i.js-second').removeClass('disabled');
        }
    });
    Biblio.data.bin.on('books', function (count) {
        addInfo.set('count', count);
    });
    new Biblio.Views.AddInfo({model: addInfo});
    Biblio.data.bin.booksChanged();
});
$(document).click(function () {
    $('.biblio-wizard-tabs_drop').removeClass('open');
});

    $('.biblio-wizard_btn > .btn-primary').click(function (e) {
        Biblio.Utils.changeUrl('/library');
    });
    var curBookId = parseInt(Biblio.data.initParams.bookId);
    if (!isNaN(curBookId)) {
        Biblio.data.bin.fetch().success(function () {
            $.get('/api/public/book-get.json?bookId=' + curBookId, function (model) {
                if (model)
                    new Biblio.Views.SimpleBook({
                        model: new Biblio.Models.Book(model)
                    }).showDetails();
            });
        });
    }
});

/*global BroadcastHelper, LazyHeader*/
/*
 * View for broadcast subscription controls
 */
BroadcastHelper.Views.BroadcastSubscription = BaseView.extend({
    template: "#broadcast-subscription-template",

    events: {
        'click .js-cancel-subscribe':'cancelSubscription',
        'click .js-accept-subscribe':'subscribe'
    },

    afterRender: function () {
        var self = this;

        this.$('.pln-dropdown').dropPopup({
            trigger: '.pln-d-switch',
            popup: '> .pln-d-popup',
            closeTrigger: '.pln-d-close',
            activeClass: 'open',
            bottomUp: true,
            open: function(el) {
                el.elem.find('.modal-scroll-content').scrollbar('repaint');
            }
        });

        this.$el.clickOff(function (e) {
            self.$('.pln-dropdown').removeClass('open');
        });
    },

    cancelSubscription:function(e){
        e.preventDefault();
        this.model.cancelSubscription();
    },

    subscribe: function (e) {
        e.preventDefault();

        var email = this.$(".js-email").val();
        this.model.updateSubscription(email);
    }
});
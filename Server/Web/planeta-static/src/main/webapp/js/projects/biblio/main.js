var Biblio = Biblio || {};
_.each(['Models', 'Views', 'Collections', 'Utils', 'data'], function (module) {
    Biblio[module] = Biblio[module] || {};
});

Biblio.Utils.changeUrl = function (url) {
    if (Biblio.data.bin) {
        Biblio.data.bin.save().done(function () {
            window.location = url;
        });
    } else {
        window.location = url;
    }
};

Biblio.Utils.parseSearch = function () {
    return _.object(
            _.compact(_.map(window.location.search.slice(1).split('&'), function (item) {
                if (item) {
                    return item.split('=');
                }
            })));
};

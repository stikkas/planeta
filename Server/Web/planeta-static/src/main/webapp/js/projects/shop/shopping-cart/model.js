Shop.Models.ShoppingCart = BaseModel.extend({

    defaults: {
        cartItems: new BaseCollection([]),
        notificationCartItems: new BaseCollection([]),
        totalSum: 0,
        totalItems: 0,
        showPopup: false,
        editEnabled: true,
        onCheckoutPage: false,
        inShoppingCart: false,
        deliveryPrice: 0
    },

    addProduct: function (productId, quantity) {
        if (!this.get('editEnabled')) {
            return;
        }

        quantity = quantity || 1;
        var cartItem = this.get('cartItems').findByAttr('objectId', productId);
        if (cartItem != null) {
            quantity += cartItem.get('count');
        }
        var self = this;
        var options = {
            url: '/payment/change-quantity-in-shopping-cart.json',
            data: {
                productId: productId,
                quantity: quantity
            },
            context: this,
            success: function (response) {
                if (response.success) {
                    var cartItem = self.get('cartItems').findByAttr('objectId', response.result.objectId);
                    if (cartItem) {
                        cartItem.set(response.result);
                    } else {
                        cartItem = new BaseModel(response.result);
                        self.get('cartItems').add(cartItem);
                    }
                    self.get('notificationCartItems').reset(cartItem);
                    self.recalculateTotals();
                    if (window.gtm) {
                        window.gtm.trackAddProductToCart(response.result);
                    }
                } else {
                    workspace.appView.showErrorMessage(response.errorMessage);
                }
            },
            error: function () {
                workspace.appView.showErrorMessage("Не удалось добавить товар в корзину");
            }
        };
        return Backbone.sync('update', this, options);
    },

    isContains: function (productId) {
        return this.get('cartItems').findByAttr('objectId', productId)
    },

    removeProduct: function (model) {
        if (!this.get('editEnabled')) {
            return;
        }
        var self = this;
        var options = {
            url: '/payment/remove-from-shopping-cart.json',
            data: {productId: model.get('objectId')},
            context: this,
            success: function (response) {
                if (response.success) {
                    workspace.appView.showSuccessMessage("Товар удалён из корзины");
                    self.get('cartItems').remove(model);
                    self.get('notificationCartItems').remove(model);
                    self.recalculateTotals();
                    if (window.gtm) {
                        window.gtm.trackRemoveProductFromCart(model);
                    }
                } else {
                    workspace.appView.showErrorMessage("Не удалось удалить товар из корзины");
                }
            },
            error: function () {
                workspace.appView.showErrorMessage("Не удалось удалить товар из корзины");
            }
        };
        Backbone.sync('update', this, options);
    },

    changeProductQuantity: function (model, quantity) {
        if (!this.get('editEnabled')) {
            return;
        }
        if (quantity <= 0) {
            model.change();
            return;
        }
        var options = {
            url: '/payment/change-quantity-in-shopping-cart.json',
            method: 'update',
            data: {
                productId: model.get('objectId'),
                quantity: quantity
            },
            context: this
        };
        return this.fetchX(options).done(function (result) {
            model.set('count', result.count);
            this.recalculateTotals();
        }).fail(function (undefined, errorMessage) {
            workspace.appView.showErrorMessage(errorMessage);
        });
    },

    setDeliveryPrice: function (deliveryPrice) {
        var parsed = parseInt(deliveryPrice, 10);
        var price = _.isNaN(parsed) ? 0 : Math.abs(parsed);

        this.set('deliveryPrice', price);
    },

    recalculateTotals: function () {
        var isEditEnabled = this.get('editEnabled');
        var total = {totalSum: 0, totalItems: 0};

        this.get('cartItems').each(function (cartItem) {
            cartItem.set('editEnabled', isEditEnabled, {silent: true});
            total.totalSum += cartItem.get('price') * cartItem.get('count');
            total.totalItems += cartItem.get('count');
        });

        this.set(total);
    },

    load: function (_options) {
        var options = {
            url: '/payment/get-shopping-cart.json',
            data: {},
            context: this
        };

        return this.fetchX(options).done(function (result) {
            this.get('cartItems').reset(result);
            this.recalculateTotals();
            if (_options && _options.success) {
                _options.success();
            }
            if (_.any(result, function(it){return it.countChanged;})) {
                workspace.appView.showInfoMessage("Количество товара в наличии изменилось");
            }
        }).fail(function (_$1, errorMessage) {
            workspace.appView.showErrorMessage(errorMessage || "Не удалось загрузить корзину");
        });
    },
    fetch: function (options) {
        this.load(options);
    },

    hasDigitalTrades: function () {
        return !!this.get('cartItems').findByAttr('productCategory', "DIGITAL");
    },
    hasProductWithDisabledCash: function () {
        return (!!this.get('cartItems').findByAttr("cashAvailable", false));
    },
    doRequireDelivery: function () {
        return !!this.get('cartItems').findByAttr('productCategory', "PHYSICAL");
    },
    canFreeDelivery: function() {
        return !this.get('cartItems').findByAttr('noFreeDelivery', true);
    }

});
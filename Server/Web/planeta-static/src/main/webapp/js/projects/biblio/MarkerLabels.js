$().ready(function(){
	Biblio.Marker = function(options){
	
		google.maps.Marker.apply(this, arguments);
	
		if (options.label) {
			this.MarkerLabel = new Biblio.MarkerLabel({
				map: this.map,
				marker: this,
				text: options.label
			});
			this.MarkerLabel.bindTo('position', this, 'position');
		}
	};
	
	Biblio.Marker.prototype = $.extend(new google.maps.Marker(), {
		// If we're adding/removing the marker from the map, we need to do the same for the marker label overlay
		setMap: function(){
			google.maps.Marker.prototype.setMap.apply(this, arguments);
			(this.MarkerLabel) && this.MarkerLabel.setMap.apply(this.MarkerLabel, arguments);
		},
		setLabel: function(label){
			//google.maps.Marker.prototype.setLabel.apply(this, arguments);
			this.label = label;
			this.MarkerLabel = new Biblio.MarkerLabel({
				map: this.map,
				marker: this,
				text: label.text,
				style: label.style
			});
			this.MarkerLabel.bindTo('position', this, 'position');
		}
	});
	
	Biblio.MarkerLabel = function(options) {
	
		var self = this;
	
		this.setValues(options);
	
		// Create the label container
		this.div = document.createElement('div');
		this.div.style = options.style+';text-align:center;font-family:Arial,sans-serif; font-weight:bold';
		this.div.style.position = 'absolute';		
		this.style = this.div.style; 
		//this.div.className = 'map-marker-label';
	
		// Trigger the marker click handler if clicking on the label
		google.maps.event.addDomListener(this.div, 'click', function(e){
			(e.stopPropagation) && e.stopPropagation();
			google.maps.event.trigger(self.marker, 'click');
		});
	};
	
	Biblio.MarkerLabel.prototype = $.extend(new google.maps.OverlayView(), {
		onAdd: function() {
			this.getPanes().overlayImage.appendChild(this.div);
	
			// Ensures the label is redrawn if the text or position is changed.
			var self = this;
			this.listeners = [
				google.maps.event.addListener(this, 'position_changed', function() { self.draw(); }),
				google.maps.event.addListener(this, 'text_changed', function() { self.draw(); }),
				google.maps.event.addListener(this, 'zindex_changed', function() { self.draw(); })
			];
		},
		onRemove: function() {
			this.div.parentNode.removeChild(this.div);
			// Label is removed from the map, stop updating its position/text
			for (var i = 0, l = this.listeners.length; i < l; ++i) {
				google.maps.event.removeListener(this.listeners[i]);
			}
		},
		draw: function() {
			var
				text = String(this.get('text')),
				fontSize = this.style.fontSize.split('px')[0]/2+1,
				position = this.getProjection().fromLatLngToDivPixel(this.get('position'));
			
			this.div.innerHTML = text;
			this.div.style.left = (position.x - fontSize*text.length/2) + 'px';
			this.div.style.top = (position.y - fontSize-2) + 'px';
			this.div.style.zIndex = this.marker.getZIndex()+1;
		}
	});
});
var Charity = Charity || {};
Charity.Views = Charity.Views || {};

Charity.Views.News = BaseView.extend({
    template: "#charity-news-template",

    construct: function (options) {
        var newsTags = new BaseModel({
            headingText: 'Новостные рубрики',
            tagCollection: options.newsTags
        });

        this.model = new BaseModel({
            title: options.title,
            banner: options.banner
        });

        this.addChildAtElement('.js-news-list', new Charity.Views.NewsList({
            collection: this.collection
        }));

        this.addChildAtElement('.js-news-tags', new Charity.Views.NewsTagBlock({
            model: newsTags,
            collection: this.collection
        }));
    }
});

Charity.Views.ModalPopup = BaseRichView.extend({
    template: '#charity-news-modal-popup',
    events: {
        'click .close': 'close'
    },

    htmlBlock: $('html'),

    construct: function() {
        this.$bg = $('.modal-backdrop-charity');
        this.$bg.addClass('modal-backdrop in js-charity-popup-opened');
    },

    afterRender: function() {
        BaseRichView.prototype.afterRender.call(this, arguments);
        News.Views.Post.prototype.addCharitySharing.apply(this, arguments);
        if (workspace) {
            workspace.changeUrl('/news/' + this.model.get('id'));
        }
    },

    close: function () {
        this.$el.hide();
        this.$bg.hide();
        this.$bg.removeClass('modal-backdrop in js-charity-popup-opened');
        this.hidden = true;
        this.htmlBlock.css('overflow', 'auto');
        this.htmlBlock.css('padding-right', '0');
        if (workspace) {
            workspace.changeUrl('/news');
        }
    },
    open: function () {
        var self = this;
        this.$el.show();
        this.$bg.show();
        this.hidden = false;
        this.render();
        this.htmlBlock.css('overflow', 'hidden');
        this.htmlBlock.css('padding-right', '17px');

        $('.js-modal-overlapped-view').clickOff(function (e) {
            self.close();
        }, {
            once: true,
            selector: '.close'
        });
    }
});

Charity.Views.NewsItem = BaseView.extend({
    template: "#item-charity-news-template",
    className: 'charity-news_i',

    onPostClick: function (e) {
        e.preventDefault();
        var view = new Charity.Views.ModalPopup({
            el: '#modal-charity-news-popup',
            model: this.model
        });
        view.open();
    }
});

Charity.Views.NewsList = DefaultListView.extend({
    className: 'charity-news_list',
    pagerTemplate: '#charity-news-pager-template',
    pagerLoadingTemplate: '#charity-news-loader-template',
    emptyListTemplate: '#nothing-found-template',
    itemViewType: Charity.Views.NewsItem,

    construct: function() {
        this.collection.loading = false;
    }
});

Charity.Views.NewsTagBlock = BaseView.extend({
    template: "#charity-news-tags-block-template",
    className: 'charity-nav_block',

    events: {
        'click .charity-nav_link': 'switchNewsCategory'
    },

    construct: function() {
        this.addChildAtElement('.js-charity-news-tag-list', new Charity.Views.NewsTagList({
            collection: new BaseCollection(this.model.get('tagCollection'), {block: 'NEWS'})
        }));
    },

    afterRender: function() {
        var $elem = this.$('.charity-nav_link').filter('[data-tag-id=0]');
        if($elem.length == 0) {
            this.$('.charity-nav_list').prepend("<div class='charity-nav_i active'><a href='#' data-tag-id='0' class='charity-nav_link'>Все</a></div>");
        }
    },

    switchNewsCategory: function(e) {
        e.preventDefault();
        var limit = 3,
            offset = 0,
            self = this,
            tagId = e.target.getAttribute('data-tag-id');

        this.$('.charity-nav_i').filter('.active').removeClass('active');
        this.$('[data-tag-id=' + tagId + ']').parent().addClass('active');

        this.collection.fetch({
            data: {
                limit: limit,
                newsTagId: tagId,
                offset: offset
            }
        }).done(function(response) {
            self.collection.data = {
                newsTagId: tagId
            };
            self.collection.offset = response.length;
            self.collection.allLoaded = response.length < limit;
        });
    }
});

Charity.Views.NewsTagItem = BaseView.extend({
    template: "#charity-news-tag-item-template",
    className: 'charity-nav_i'
});

Charity.Views.NewsTagList = DefaultListView.extend({
    className: "charity-nav_list",
    itemViewType: Charity.Views.NewsTagItem
});

Charity.Views.Events = BaseView.extend({
    template: "#charity-events-template",

    construct: function (options) {
        this.model = new BaseModel({
            title: options.title
        });

        this.addChildAtElement('.js-school-events-list', new Charity.Views.EventList({
            collection: this.collection
        }));
    }
});

Charity.Views.SchoolEventItem = BaseView.extend({
    template: "#charity-events-item-template",
    className: 'charity-events_i',

    events: {
        'click .charity-events_link': 'onPostClick'
    },

    onPostClick: function (e) {
        e.preventDefault();
        var view = new Charity.Views.ModalPopup({
            el: '#modal-charity-news-popup',
            model: this.model
        });
        view.open();
    }
});

Charity.Views.EventList = DefaultListView.extend({
    className: 'charity-events_list',
    pagerTemplate: '#charity-news-pager-template',
    pagerLoadingTemplate: '#charity-news-loader-template',
    emptyListTemplate: '#nothing-found-template',
    itemViewType: Charity.Views.SchoolEventItem,

    afterRender: function() {
        DefaultListView.prototype.afterRender.call(this);
        if(this.collection.length > 0) {
            this.collection.models[0].set('first', true);
        }
    },

    construct: function() {
        this.collection.loading = false;
    }
});
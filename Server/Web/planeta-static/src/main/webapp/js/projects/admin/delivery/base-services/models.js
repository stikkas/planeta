/**
 *
 * @author Andrew
 * Date: 31.10.13
 * Time: 21:27
 */

var Delivery = {
    Models: {},
    Views: {}
};

Delivery.Models.NestedLocations = BaseCollection.extend({
    url: '/admin/get-nested-locations.json'
});


Delivery.Models.BaseService = BaseModel.extend({
    defaults: {
        serviceId: 0,
        name: '',
        description: '',
        serviceType: "DELIVERY",
        address: {
            street: ''
        },
        location: {
            locationType: '',
            locationId: 0,
            parent: false
        }
    },
    idAttribute: 'serviceId',
    initialize: function(options) {
        if (!this.has('address')) {
            this.set({
                address: {
                    street: ''
                }
            });
        }

    },
    update: function(_options) {
        var $dfd = $.Deferred();
        var data = this.toJSON();
        if (data.location.locationId == 0 && data.location.locationType != "WORLD") return $dfd.rejectWith(this, [null, {location: 'Обязательное поле'}]);
        delete data.location.parent;
        var options = {
            url: '/admin/delivery/update-base-service.json',
            data: data,
            context: this,
            contentType: 'application/json',
            success: function(responce) {
                if (responce.success) {
                    this.set(this.parse(responce.result));
                    $dfd.resolveWith(this, [responce.result]);
                } else {
                    $dfd.rejectWith(this, [responce.errorMessage, responce.fieldErrors]);
                }
            },
            error: function() {
                $dfd.rejectWith(this, ['Ошибка соединения']);
            }
        };
        _.extend(options, _options);
        Backbone.sync('update', this, options);
        return $dfd.promise();
    },
    enable: function() {
        var data = this.toJSON();
        data.enabled = true;
        return this.update({
            data: data
        });
    },
    disable: function() {
        var data = this.toJSON();
        data.enabled = false;
        return this.update({
            data: JSON.stringify(data)
        });
    },
    remove: function() {
        return this.update({
            url: '/admin/delivery/remove-base-service.json',
            contentType: 'application/x-www-form-urlencoded',
            data: {
                serviceId: this.get('serviceId')
            }
        }).done(function() {
            var collection = this.collection;
            collection.remove(this);
            collection.trigger('reset');
        });
    }

});

Delivery.Models.BaseServicesCollection = BaseCollection.extend({
    model: Delivery.Models.BaseService
});

Delivery.Models.ModerateBaseServicesContent = BaseModel.extend({
    initialize: function(options) {
        this.services = new Delivery.Models.BaseServicesCollection(options.services);
    }
});
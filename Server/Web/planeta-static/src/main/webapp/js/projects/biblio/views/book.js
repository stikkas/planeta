/**
 * Сам по себе не рисуется, нужен только для предаставления метода showDetails
 */
Biblio.Views.SimpleBook = BaseView.extend({
    showDetails: function sd() {
        var self = this;
        if (!sd.bookDetails) {
            var data = {
                view: Biblio.Views.BookDetails,
                title: 'Оформите подписку для библиотеки',
                clazz: 'modal-book-card',
                el: '#modal-book-card',
                bodyModel: self.model
            };
            sd.bookDetails = new Biblio.Views.ModalWindow(data);
        } else {
            sd.bookDetails.setBodyModel(self.model);
        }
        sd.bookDetails.open();
        Biblio.data.savedUrl = window.location.href;
        workspace.changeUrl(window.location.pathname + '?bookId=' + self.model.get('bookId'));
    }
});

/**
 * Рисует квадратик с обложкой, ценой и кнопками для изменения кол-ва издания
 */
Biblio.Views.Book = Biblio.Views.SimpleBook.extend({
    template: '#book-card_i',
    className: 'book-card_i',
    events: {
        'click .book-card_counter': 'addCount',
        'click .biblio-counter_minus': 'minusCount',
        'click': 'showDetails'
    },
    construct: function () {
        var self = this;
        Biblio.data.bin.on('change', function () {
            if (!self.$el.hasClass('active'))
                self.$el.addClass('active');
            self._chV(Biblio.data.bin.countBook(self.model));
        });
    },
    afterRender: function () {
        this.counter = this.$('.biblio-counter_count');
    },
    _chV: function (value) {
        if (value == 0)
            this.$el.removeClass('active');
        this.counter.text(value);
        Biblio.data.bin.changeBooks(this.model, value);
    },
    addCount: function (e) {
        var self = this;
        if (self.$el.hasClass('active')) {
            e.stopPropagation();
            self._chV(parseInt(self.counter.text()) + 1);
        }
    },
    minusCount: function (e) {
        e.stopPropagation();
        this._chV(parseInt(this.counter.text()) - 1);
    }
});

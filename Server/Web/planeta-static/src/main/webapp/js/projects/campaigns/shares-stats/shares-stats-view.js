/**
 * Created with IntelliJ IDEA.
 * User: alexa_000
 * Date: 10.01.14
 * Time: 21:22
 */

/*globals CampaignSharesStats, Orders, ModalConfirmView, Form, Campaign, DefaultListView, TemplateManager*/

CampaignSharesStats.Views = {};


CampaignSharesStats.Views.Filter = Orders.Views.BaseFilter.extend({
    template: '#campaign-shares-stats-filter-template',
    events: _.extend({}, Orders.Views.BaseFilter.prototype.events, {
        'click .js-filter-link-item': 'onAggregationTypeClicked'
    }),

    onAggregationTypeClicked: function (e) {
        var $e = $(e.currentTarget);
        if ($e.hasClass('active')) {
            return;
        }
        this.model.set({aggregationType: $e.attr('data-type')});
        this.model.trigger('filterChanged');
    },

    afterRender: function() {
        Orders.Views.BaseFilter.prototype.afterRender.call(this);
        var timeFilter = this.model.get('timeFilter');
        var $activeEl = $('#periods .active');
        if (timeFilter != $activeEl.children().attr('id')) {
            $activeEl.removeClass('active');
            this.$('#' + timeFilter).parent().addClass('active');
        }
    }
});

CampaignSharesStats.Views.Order = BaseView.extend({
    tagName: 'tr',
    template: '#shares-stats-item-template'
});

CampaignSharesStats.Views.ListView = DefaultListView.extend({
    itemViewType: CampaignSharesStats.Views.Order,
    tagName: 'tbody',
    addPagerElement: function (pager) {
        if (this.pagerElement) {
            this.pagerElement.remove();
            $('.list-loader').remove();
        }
        this.pagerElement = pager;

        if (!this.pagerElement) {
            return;
        }
        if (this.sortDirection != 'asc') {
            if (this.pagerPosition == 'outside') {
                $('table').before(this.pagerElement);
            } else {
                $('table').prepend(this.pagerElement);
            }
        } else {
            if (this.pagerPosition == 'outside') {
                $('table').after(this.pagerElement);
            } else {
                $('table').append(this.pagerElement);
            }
        }
    },

    renderPagerElement: function () {
        if (this.pagerElement) {
            $('.list-loader').remove();
            this.pagerElement.remove();
        }
        if (this.pagerTemplate && !this.collection.allLoaded) {
            var self = this;
            $.when(TemplateManager.tmpl(this.pagerTemplate, this.modelJSON(), this)).done(function (pager) {
                pager.click(function (e) {
                    self.onPagerClicked(e);
                });
                self.addPagerElement(pager);
            });
        }
    }
});

CampaignSharesStats.Views.Controller = BaseView.extend({
    template: '#campaign-shares-stats-template',

    construct: function() {
        this.addChildAtElement('.search-block', new CampaignSharesStats.Views.Filter({
            model: this.model.filter
        }));

        var header = this.addChildAtElement('.project-stats-table', new CampaignSharesStats.Views.ListViewHeaderShares({
            model: this.model.orders.listHeader
        }));

        this.addChildAtElement('.project-stats-table', new CampaignSharesStats.Views.ListView({
            collection: this.model.orders
        }));

        //TODO: check if we really need 'true' flag
        this.addChildAtElement('.top-downloader-placeholder', new CampaignOrders.Views.BaseReportDownloader({
            model: this.model.downloader
        }), true);

        //TODO: check if we really need 'true' flag
        this.addChildAtElement('.bottom-downloader-placeholder', new CampaignOrders.Views.BaseReportDownloader({
            model: this.model.downloader,
            additionalClass: 'pull-right'
        }), true);
        this.model.filter.bind('change', header.render, this);
    }
});

CampaignSharesStats.Views.ListViewHeaderShares = BaseView.extend({
    tagName: 'thead',
    template: '#shares-stats-list-header-template'
});


CampaignSharesStats.Views.Page = Campaign.Views.BasePage.extend({
    contentViewClass: CampaignSharesStats.Views.Controller
});

var playlist = [
    {
        name: 'start',
        video: [
            {
                type: "video/mp4",
                src: "https://s4.planeta.ru/v/11a7f/720p.mp4"
            }
        ],
        track: {
            kind: "captions",
            mode: "hidden",
            label: "Егор Видео 1 сабы",
            language: "Russian",
            srclang: "ru",
            src: "https://files.planeta.ru/interactive/tracks/step01.vtt"
        },
        loop: [
            {
                type: "video/mp4",
                src: "https://s4.planeta.ru/v/11a80/720p.mp4"
            }
        ]
    }, {
        name: 'name',
        video: [
            {
                type: "video/mp4",
                src: "https://s4.planeta.ru/v/11a82/720p.mp4"
            }
        ],
        video2: [
            {
                type: "video/mp4",
                src: "https://s4.planeta.ru/v/11a81/720p.mp4"
            }
        ],
        track: {
            kind: "captions",
            mode: "hidden",
            label: "Егор Видео 2 сабы",
            language: "Russian",
            srclang: "ru",
            src: "https://files.planeta.ru/interactive/tracks/step02b.vtt"
        },
        track2: {
            kind: "captions",
            mode: "hidden",
            label: "Егор Видео 2 сабы",
            language: "Russian",
            srclang: "ru",
            src: "https://files.planeta.ru/interactive/tracks/step02a.vtt"
        },
        loop: [
            {
                type: "video/mp4",
                src: "https://s4.planeta.ru/v/11c3f/720p.mp4"
            }
        ]
    }, {
        name: 'email',
        video: [
            {
                type: "video/mp4",
                src: "https://s4.planeta.ru/v/11a84/720p.mp4"
            }
        ],
        track: {
            kind: "captions",
            mode: "hidden",
            label: "Егор Видео 2 сабы",
            language: "Russian",
            srclang: "ru",
            src: "https://files.planeta.ru/interactive/tracks/step03.vtt"
        },
        loop: [
            {
                type: "video/mp4",
                src: "https://s4.planeta.ru/v/11a85/720p.mp4"
            }
        ]
    }, {
        name: 'proceed',
        video: [
            {
                type: "video/mp4",
                src: "https://s4.planeta.ru/v/11a86/720p.mp4"
            }
        ],
        track: {
            kind: "captions",
            mode: "hidden",
            label: "Егор Видео 2 сабы",
            language: "Russian",
            srclang: "ru",
            src: "https://files.planeta.ru/interactive/tracks/step04.vtt"
        },
        loop: [
            {
                type: "video/mp4",
                src: "https://s4.planeta.ru/v/11a87/720p.mp4"
            }
        ]
    }, {
        name: 'info',
        video: [
            {
                type: "video/mp4",
                src: "https://s4.planeta.ru/v/11a88/720p.mp4"
            }
        ],
        track: {
            kind: "captions",
            mode: "hidden",
            label: "Егор Видео 1 сабы",
            language: "Russian",
            srclang: "ru",
            src: "https://files.planeta.ru/interactive/tracks/step05.vtt"
        },
        loop: [
            {
                type: "video/mp4",
                src: "https://s4.planeta.ru/v/11c40/720p.mp4"
            }
        ]
    }, {
        name: 'duration',
        video: [
            {
                type: "video/mp4",
                src: "https://s4.planeta.ru/v/11a8a/720p.mp4"
            }
        ],
        track: {
            kind: "captions",
            mode: "hidden",
            label: "Егор Видео 2 сабы",
            language: "Russian",
            srclang: "ru",
            src: "https://s4.planeta.ru/f/2b20/1502785755787_renamed.vtt"
        },
        loop: [
            {
                type: "video/mp4",
                src: "https://s4.planeta.ru/v/11c41/720p.mp4"
            }
        ]
    }, {
        name: 'price',
        video: [
            {
                type: "video/mp4",
                src: "https://s4.planeta.ru/v/11a8c/720p.mp4"
            }
        ],
        track: {
            kind: "captions",
            mode: "hidden",
            label: "Егор Видео 1 сабы",
            language: "Russian",
            srclang: "ru",
            src: "https://files.planeta.ru/interactive/tracks/step07.vtt"
        },
        loop: [
            {
                type: "video/mp4",
                src: "https://s4.planeta.ru/v/11a8d/720p.mp4"
            }
        ]
    }, {
        name: 'video',
        video: [
            {
                type: "video/mp4",
                src: "https://s4.planeta.ru/v/11a8e/720p.mp4"
            }
        ],
        track: {
            kind: "captions",
            mode: "hidden",
            label: "Егор Видео 1 сабы",
            language: "Russian",
            srclang: "ru",
            src: "https://files.planeta.ru/interactive/tracks/step08.vtt"
        },
        loop: [
            {
                type: "video/mp4",
                src: "https://s4.planeta.ru/v/11a8f/720p.mp4"
            }
        ]
    }, {
        name: 'text',
        video: [
            {
                type: "video/mp4",
                src: "https://s4.planeta.ru/v/11a90/720p.mp4"
            }
        ],
        track: {
            kind: "captions",
            mode: "hidden",
            label: "Егор Видео 2 сабы",
            language: "Russian",
            srclang: "ru",
            src: "https://files.planeta.ru/interactive/tracks/step09.vtt"
        },
        loop: [
            {
                type: "video/mp4",
                src: "https://s4.planeta.ru/v/11a91/720p.mp4"
            }
        ]
    }, {
        name: 'reward',
        video: [
            {
                type: "video/mp4",
                src: "https://s4.planeta.ru/v/11a92/720p.mp4"
            }
        ],
        track: {
            kind: "captions",
            mode: "hidden",
            label: "Егор Видео 2 сабы",
            language: "Russian",
            srclang: "ru",
            src: "https://files.planeta.ru/interactive/tracks/step10.vtt"
        },
        loop: [
            {
                type: "video/mp4",
                src: "https://s4.planeta.ru/v/11a93/720p.mp4"
            }
        ]
    }, {
        name: 'counterparty',
        video: [
            {
                type: "video/mp4",
                src: "https://s4.planeta.ru/v/11a94/720p.mp4"
            }
        ],
        track: {
            kind: "captions",
            mode: "hidden",
            label: "Егор Видео 1 сабы",
            language: "Russian",
            srclang: "ru",
            src: "https://files.planeta.ru/interactive/tracks/step11.vtt"
        },
        loop: [
            {
                type: "video/mp4",
                src: "https://s4.planeta.ru/v/11a95/720p.mp4"
            }
        ]
    }, {
        name: 'check',
        video: [
            {
                type: "video/mp4",
                src: "https://s4.planeta.ru/v/11a96/720p.mp4"
            }
        ],
        track: {
            kind: "captions",
            mode: "hidden",
            label: "Егор Видео 2 сабы",
            language: "Russian",
            srclang: "ru",
            src: "https://files.planeta.ru/interactive/tracks/step12.vtt"
        },
        loop: [
            {
                type: "video/mp4",
                src: "https://s4.planeta.ru/v/11a97/720p.mp4"
            }
        ]
    },
    {
        name: 'final',
        video: [
            {
                type: "video/mp4",
                src: "https://s4.planeta.ru/v/11a98/720p.mp4"
            }
        ],
        track: {
            kind: "captions",
            mode: "hidden",
            label: "Егор Видео 2 сабы",
            language: "Russian",
            srclang: "ru",
            src: "https://files.planeta.ru/interactive/tracks/step13.vtt"
        },
        loop: [
            {
                type: "video/mp4",
                src: "https://s4.planeta.ru/v/11a98/720p.mp4"
            }
        ]
    }
];


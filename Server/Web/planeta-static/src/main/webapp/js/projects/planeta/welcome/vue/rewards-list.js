Vue.component('rewards-list', {
    template: '#rewards-list',

    data: function() {
        this.load();

        return {
            rewards: []
        }
    },

    methods: {
        load: function () {
            this.$http.get('/api/welcome/shares-list').then(
                function (response) {
                    this.rewards = response.body.result;
                    if (window.gtm) {
                        window.gtm.trackViewRewardsList(response.body.result, 'Карусель вознаграждений на главной');
                    }
                },

                function (data, status, request) {
                    //TODO handle error
                }
            );
        },

        getImageUrl: function (url) {
            return ImageUtils.getThumbnailUrl(url, ImageUtils.SHARE_COVER);
        },

        getHumanPrice: function (price) {
            return StringUtils.humanNumber(price);
        }
    }
});
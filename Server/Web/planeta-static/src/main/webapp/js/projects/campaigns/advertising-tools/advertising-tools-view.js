/*globals Campaign, Orders, PrivacyUtils*/
CampaignAdvertisingTools.Views = {};

CampaignAdvertisingTools.Views.Targeting = Form.BaseView.extend({
    className: '',
    template: '#campaign-targeting-tools-inner-template',
    events: _.extend({}, Modal.View.prototype.events, {
        'click .js-open-targeting-help-vk' : 'openTargetingHelperVkModal',
        'click .js-open-targeting-help-fb' : 'openTargetingHelperFbModal'
    }),
    beforeRender: function () {
    },
    onConfirm: function (data) {
        return this.model.saveForm(data, function () {
            workspace.appView.showSuccessMessage('Данные сохранены.');
        });
    },
    openTargetingHelperVkModal: function (e) {
        Modal.showDialogByViewClass(CampaignAdvertisingTools.Views.VkTargetingHelperModal, this.model, null, true, null, null, true);
    },
    openTargetingHelperFbModal: function (e) {
        Modal.showDialogByViewClass(CampaignAdvertisingTools.Views.FbTargetingHelperModal, this.model, null, true, null, null, true);
    }
});

CampaignAdvertisingTools.Views.VkTargetingHelperModal = Modal.OverlappedView.extend({
    template: '#campaign-targeting-tools-vk-targeting-help-template',
    clickOff: true
});

CampaignAdvertisingTools.Views.FbTargetingHelperModal = Modal.OverlappedView.extend({
    template: '#campaign-targeting-tools-fb-targeting-help-template',
    clickOff: true
});

CampaignAdvertisingTools.Views.Page = BaseView.extend({
    template: '#campaign-targeting-tools-template',
    construct: function () {
        this.addChildAtElement('.js-targeting-anchor', new CampaignAdvertisingTools.Views.Targeting({
            model: this.model.targeting
        }));
    }
});

/*globals CampaignEdit, TinyMcePlaneta, tinymce*/

CampaignEdit.Models.EditorModel = function (campaignModel) {

    _.extend(this, new TinyMcePlaneta.EditorModel(campaignModel), {
        config: TinyMcePlaneta.getCampaignEditorConfiguration(),
        width: 700,
        height: 400,
        saveToParentModel: function () {
            this.parentModel.set('descriptionHtml', this.html(), {silent: true});
            this.parentModel.set('description', this.text(), {silent: true});
        },
        destructPlugins: function () {
            try{
                if (tinymce.activeEditor && tinymce.activeEditor.plugins && tinymce.activeEditor.plugins.planetafullscreencampaign.fullScreen) {
                    tinymce.activeEditor.execCommand("planetafullscreencampaign");
                }
            } catch (err) {
                console.log(err);
            }
        }
    });
};

CampaignEdit.Views.DescriptionEdit = BaseView.extend({
    className: 'ww-flat edit-blog1',
    template: '#campaign-edit-tiny-mce-template',
    editorState: null,

    afterRender: function () {
        var tinyModel = this.model.editorModel;
        $('#tinymce-body').height(400).width(700);
        tinyModel.init('#tinymce-body');
        $('.edit-blog-body').fadeIn();
        $('.edit-blog-body-stub').hide();
        this.$el.find('.tb-bottom').hide();

        var editorInitedTimer = setInterval(function () {
            if (!tinyModel.rendered || !tinymce.activeEditor) return;

            clearInterval(editorInitedTimer);

            try {
                $( tinymce.activeEditor.getDoc() ).on('click', function (e) {
                    if ( !$(e.target).hasClass('mceContentHtml') ) return;
                    tinymce.activeEditor.getBody().focus();
                });

                $(tinymce.activeEditor.getBody()).addClass('mceCampaignEditorBody');
            } catch (error) {
                console.log(error);
            }
        }, 100);

    },

    dispose: function () {
        this.model.editorModel.destruct();
    },

    modelEvents: {
        'destroy': 'dispose'
    }
});
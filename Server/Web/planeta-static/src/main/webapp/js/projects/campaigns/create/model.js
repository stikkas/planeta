/*globals CampaignEdit, Campaign*/
/**
 *
 * @author Andrew
 * Date: 26.09.13
 * Time: 14:29
 */
CampaignEdit.Models.CreateCampaignPage = BaseModel.extend({

    _createCampaignAndGoOnEditIt: function(options) {
        var self = this;
        this.createNewCampaign().done(function(campaign) {
            options.success();
            //noinspection JSUnresolvedVariable
            workspace.navigate('/' + self.get('id') + '/' + self.get('section') + '/' + campaign.campaignId + '/edit', {replace: true});
        }).fail(function() {
                options.success({success: false});
            });

    },
    prefetch: function(options) {
        if ((this.get('profileModel').get('profileType') == 'GROUP' || this.get('profileModel').get('profileType') === 'HIDDEN_GROUP') && (this.get('objectId') == 'create')) {
            this._createCampaignAndGoOnEditIt(options);
        } else {
            workspace.appView.showErrorMessage('Unexpected link.');
            options.success();
        }
    },
    createNewCampaign: function() {
        return (new CampaignEdit.Models.DraftCampaign({
            profileId: this.get('profileModel').get('profileId'),
            campaignId: 0
        })).saveCampaign();
    },
    pageData: function() {
        //TODO: make dynamic title here (better override in campaign create model)
        return {
            title: "Создание нового проекта | Planeta"
        };
    }
});


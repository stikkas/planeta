/*global CampaignEdit, CrowdFund, AlbumTypes, ImageUtils, ImageType*/

CampaignEdit.Models.Details = CampaignEdit.Models.Tab.extend({
    defaults: _.extend({}, CampaignEdit.Models.Tab.prototype.defaults, {
        planetaDelivery: true
    })
});

CampaignEdit.Views.Details = BaseView.extend({
    className: 'col-12',
    template: '#campaign-edit-details-content-template',
    modelEvents: {
        destroy: 'dispose'
    },
    construct: function (options) {
        if (options && options.controller) {
            options.controller.pageData = this.pageData();
        }
        this.imageOrVideoModel = new CampaignEdit.Models.ViewImage({
            errors: this.model.get('errors'),
            originalImage: {
                imageUrl: this.model.get('viewImageUrl'),
                photoId: this.model.get('viewImageId')
            },
            profileId: this.model.get('profileId'),
            albumTypeId: AlbumTypes.ALBUM_COMMENT,
            thumbnail: {
                imageConfig: ImageUtils.ORIGINAL,
                imageType: ImageType.PHOTO
            },
            aspectRatio: 640 / 360,
            width: 640,
            video: {
                originalVideo: {
                    videoProfileId: this.model.get('videoProfileId'),
                    videoId: this.model.get('videoId'),
                    videoUrl: this.model.get('videoUrl')
                }
            }
        });
        this.imageOrVideoView = new CampaignEdit.Views.ViewImage({
            model: this.imageOrVideoModel,
            controller: this.model
        });

        this.addChildAtElement('.project-create_list', this.imageOrVideoView);

        this.addChildAtElement('.project-create_list', new CampaignEdit.Views.Description({
            model: this.model
        }));
    },
    pageData: function () {
        var L10n = {
            _dictionary: {
                "ru": {
                    "section": "Редактирование кампании",
                    "tabName": "детали"
                },
                "en": {
                    "section": "Campaign edit",
                    "tabName": "details"
                }
            }
        };

        var translate = function (word, lang) {
            if (!lang)
                throw "language is empty.";
            return L10n._dictionary[lang][word] || word;
        };

        var lang = window.workspace && (workspace.currentLanguage === "en") ? "en" : "ru";

        return {
            title: translate('section', lang) + ' - ' + translate('tabName', lang)
        }
    }
});
CampaignEdit.Views.ViewImage = CampaignEdit.Views.ImageField.extend({
    template: '#campaign-edit-view-image-template',
    modelAttr: 'viewImageUrl',
    afterRender: function () {
        if (this.controller.get('errors').videoContent) {
            this.$el.addClass('error');
        }
    }
});
CampaignEdit.Models.ViewImage = CrowdFund.Models.ImageField.extend({
    uploaderName: 'CampaignEdit.ViewImage',
    onImageUrlChange: function (campaignModel) {
        campaignModel.set({
            viewImageUrl: this.get('imageUrl'),
            viewImageId: this.get('imageId'),
            videoProfileId: this.get('videoProfileId'),
            videoId: this.get('videoId'),
            videoUrl: this.get('videoUrl')
        });
    }
});

CampaignEdit.Views.Description = CampaignEdit.Views.CampaignValidatedRow.extend({
    template: '#campaign-edit-description-editor-template',
    modelAttr: 'descriptionHtml',
    modelEvents: {
        destroy: 'dispose'
    },
    construct: function () {
        this.addChildAtElement('[data-anchor=tiny-mce]', new CampaignEdit.Views.DescriptionEdit({
            model: this.model
        }));
    },
    // Сохраниение детального описания проекта
    afterRender: function () {
        CampaignEdit.Views.CampaignValidatedRow.prototype.afterRender.call(this);
        var self = this;
        if (!self.autoSaver) {
            self.autoSaver = setInterval(function () {
                if (/\/edit-details$/.test(window.location.pathname)) {
                    $.post("/api/public/save-campaign-draft.json", {campaignId: self.model.get('campaignId'), 
                        desc: tinymce.activeEditor.getContent()}).done(function (result) {
                        if (!result.success) {
                            clearInterval(self.autoSaver);
                        }
                    }).fail(function () {
                        clearInterval(self.autoSaver);
                    });
                } else {
                    clearInterval(self.autoSaver);
                }
            }, 300000);
        }
    }
});



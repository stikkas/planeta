/*globals News */

News.Views.Search.Posts = News.Views.Posts.extend({
    emptyListTemplate: '#nothing-found-template'
});

News.Views.Search.Page = BaseView.extend({
    template: '#blog-search-block-template',
    events: {
        'click .radiobox-row': 'onTypeClicked',
        'change .n-search_input': 'onFilterChanged',
        'keyup .n-search_input': 'onFilterChanged',
        'dropdown .n-search_input': 'onFilterChanged'
    },

    construct: function() {
        this.addChildAtElement('.feed', new News.Views.Search.Posts({
            collection: this.model.get('posts')
        }));
    },

    onFilterChanged: _.debounce(function() {
        var query = this.$el.find('.n-search_input').val();
        var type = this.$el.find('.n-search_blog-filter_cont .active').data('type');
        this.model.setFilterData(query, type);
        window.history.pushState(null, null, StringUtils.hashEscape($.param({query: query})));
    }, 700),

    onTypeClicked: function(e) {
        var $el = $(e.currentTarget).find('.radiobox');
        if ($el.is('.active')) {
            return;
        }

        this.$el.find('.active').removeClass('active');
        $el.addClass('active');

        this.onFilterChanged();
    }
});

/*globals Campaign, moduleLoader, CampaignUtils*/

Campaign.Views.Tags = BaseView.extend({
    initialize: function () {
        BaseView.prototype.initialize.apply(this, arguments);
        moduleLoader.loadModule('planeta-select2');
    },
    /**
     * Initial entered tags
     * @abstract
     * @returns {{name: string}[]}
     */
    getCollection: function () {
        return [
            {name: 'tag1'}
        ];
    },

    setCollection: function () {
        throw 'NotSupportedException';
    },
    /**
     * Dropdown variants
     * @abstract
     * @returns {{name: string}[]}
     */
    getVariants: function () {
        return [
            {name: 'tag2'}
        ];
    },
    /**
     * Maps objects to their names array
     * @param collection
     * @param {string} tagName
     * @returns {Array}
     */
    parse: function (collection, tagName) {
        return _.compact(
            _.map(collection, function (value) {
                return value[tagName];
            })
        );
    },
    /**
     * Decides to change tags or not after attempt to add new tag
     * @abstract
     * @param {Array} tags
     * @param {string} [event]
     * @returns {boolean}
     */
    validateTags: function () {
        return true;
    },
    afterRender: function () {
        var self = this;
        var $select2 = self.$(".js-select2");
        var tagName = $select2.data('fieldName');
        moduleLoader.loadModule('planeta-select2').done(function () {
            var collection = self.getCollection();
            var variants = self.getVariants();
            if (collection && variants) {
                var parsedCollection = self.parse(collection, tagName);
                var parsedVariants = self.parse(variants, tagName);

                $select2.val(parsedCollection.join(',')).select2({
                    tags: parsedVariants,
                    dropdownCssClass: self.dropdownCssClass
                });
                $select2.on("change", function (e) {
                    var tags = $select2.select2('val');
                    if (!self.validateTags(tags)) {
                        e.preventDefault();
                        self.render();
                    } else {
                        self.setCollection(tags);
                    }
                });
            }
        });
    }
});

/*global CrowdFund*/
CrowdFund.Views.ImageField.Drag = (function () {
    var countField = 0;
    var enterCount = 0;
    var $document = $(document);
    return {
        reset: function () {
            enterCount =0;
            $('body').removeClass('can-drop');
        },

        start: function ($dropZone) {
            countField++;
            if (countField === 1) {
                $document.on('dragenter.campaign-image-filed-drag', function (e) {
                    e.stopPropagation();
                    enterCount++;
                    if (enterCount === 1) {
                        $('body').addClass('can-drop');
                    }
                });
                $document.on("dragleave.campaign-image-filed-drag", function (e) {
                    e.stopPropagation();
                    enterCount--;
                    if (enterCount === 0) {
                        $('body').removeClass('can-drop');
                    }
                });
                $dropZone.on('drop', this.reset());
            }
        },

        finish: function () {
            countField--;
            if (countField === 0) {
                $document.on('dragenter.campaign-image-filed-drag');
                $document.on("dragleave.campaign-image-filed-drag");
                this.reset();
            }
        }
    };
}());
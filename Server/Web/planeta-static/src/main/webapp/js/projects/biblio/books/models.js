Biblio.Models.SearchFilter = BaseModel.extend({
    defaults: {
        query: '',
        tagId: '',
        tags: []
    },
    fetch: function () {
        var self = this;
        return Backbone.sync('read', null, {
            url: '/api/public/tags.json'
        });
    }
});

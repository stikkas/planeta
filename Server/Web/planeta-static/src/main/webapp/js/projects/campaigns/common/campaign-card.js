/**
 *
 * @author Andrew Arefyev
 * Date: 14.12.12
 * Time: 17:34
 */
var Campaign = Campaign || {};
Campaign.Models = Campaign.Models || {};
Campaign.Views = Campaign.Views || {};
//============== Views ================
/**
 *  campaign card widget it self.
 *
 */
Campaign.Views.Widget = BaseView.extend({
    className: "shop-catalogue-item",
    template: "#campaign-card-widget-template",
    beforeRender: function() {
        var timeFinish = this.model.get('timeFinish');
        if (timeFinish) {
            var daysDiff = DateUtils.getDaysTo(timeFinish);
            (daysDiff >= 0) || (daysDiff = 0);
            var hoursDiff = DateUtils.getHoursTo(timeFinish);
            (hoursDiff >= 0) || (hoursDiff = 0);

            this.model.set({
                daysDiff: daysDiff,
                hoursDiff: hoursDiff % 24
            }, {silent: true});
        }

        if (this.model.get('targetAmount')) {
            var progress = Math.floor(this.model.get('collectedAmount') / this.model.get('targetAmount') * 100);
            if (Math.round(this.model.get('collectedAmount')) == 0 && Math.round(this.model.get('targetAmount')) == 0) {
                progress = 0;
            }
            var progressText = progress;
            var overFullProgressBars = [];
            if (progress > 100) {
                var maxCount = 10;
                for (var over = progress - 100; over > 0 && maxCount > 0; over = over - 100) {
                    overFullProgressBars.push(over >= 100 ? 100 : over);
                    maxCount--;
                }
                progress = 100;
            }
            this.model.set({
                progressText: progressText,
                progress: progress,
                over100ProgressBars: overFullProgressBars
            }, {silent: true});
        }
    }
});
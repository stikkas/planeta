var Charity = Charity || {};
Charity.Views = Charity.Views || {};

Charity.Views.CategoryListItem = BaseView.extend({
    template: "#charity-tag-item-template",
    className: "charity-nav_i",

    afterRender: function () {
        if (this.parent.query && this.model.get("mnemonicName") == this.parent.query.get("categories")) {
            this.$el.addClass("active");
        }
    }
});

Charity.Views.CategoriesList = DefaultListView.extend({
    className: "charity-nav_list",
    itemViewType: Charity.Views.CategoryListItem,

    construct: function (options) {
        this.query = options.query
    }
});


Charity.Views.CategoriesBlock = BaseView.extend({
    template: "#charity-tags-template",

    events: {
        "click [data-categories]": "changeCategories"
    },

    changeCategories: function (e) {
        $(".js-special").find(".charity-nav_i").removeClass('active');
        $(".js-tags").find(".charity-nav_i").removeClass('active');
        var $sel = $(e.currentTarget);
        $sel.parent().addClass('active');
        this.resultsModel.changeCategory($sel.data('categories'));
    },

    construct: function (options) {
        this.resultsModel = options.resultsModel;
        this.addChildAtElement('.js-charity-tag-list', new Charity.Views.CategoriesList({
            collection: new BaseCollection(this.model.get('tagCollection')),
            query: this.resultsModel.query
        }));
    }
});

Charity.Views.CityListItem = BaseView.extend({
    template: "#charity-city-item-template",
    className: 'charity-nav_i',

    afterRender: function () {
        if (this.parent.query && this.model.get("cityId") == this.parent.query.get("cityId")) {
            this.$el.addClass("active");
        }
    }
});

Charity.Views.CitiesList = DefaultListView.extend({
    className: "charity-nav_list",
    itemViewType: Charity.Views.CityListItem,

    construct: function (options) {
        this.query = options.query
    }
});

Charity.Views.CityBlock = BaseView.extend({
    template: "#charity-city-template",

    events: {
        "click [data-cities]": "changeCities",
        'change #cityId': "citySelect",
        'click #cityNameRus': "makeAutocompleterActive"
    },


    construct: function (options) {
        this.resultsModel = options.resultsModel;
        this.addChildAtElement('.js-city-list', new Charity.Views.CitiesList({
            collection: new BaseCollection(this.model.get('tagCollection')),
            query: this.resultsModel.query
        }));
    },

    makeAutocompleterActive: function () {
        $(".js-city").find(".charity-nav_i").removeClass('active');
        this.$('.js-other-city').parent().addClass('active');
    },

    citySelect: _.debounce(function (event) {
        var el = event.target;
        if (el.value && this.resultsModel.query.get('cityId')) {
            this.resultsModel.changeLocation(0, 0, el.value);
        }
    }, Search.INPUT_DELAY),

    getCityName: function (cityId) {
        var cityNameRus = '';
        $.ajax({
            url: '/api/search/search-location-filter-names',
            async: false,
            data: {
                cityId: cityId
            },
            success: function (response) {
                if (response.cityNameRus) {
                    cityNameRus = response.cityNameRus
                }
            }
        });
        return cityNameRus;
    },

    changeCities: function (e) {
        $(".js-city").find(".charity-nav_i").removeClass('active');
        var $sel = $(e.currentTarget);
        $sel.parent().addClass('active');
        if ($sel.hasClass("js-other-city")) {
            this.toggleAutocompliter();
        } else {
            this.$('#cityId').val($sel.data('cities'));
            this.$('#cityNameRus').val('');
            this.resultsModel.changeLocation(0, 0, $sel.data('cities'));
        }
    },

    initAutocompliter: function () {
        var params = {
            countrySelect: this.$("#countryId"),
            selectedCountryId: this.$("#countryId"),
            cityNameInput: this.$("#cityNameRus"),
            cityIdInput: this.$("#cityId")
        };
        RegionAutocompleter.autocomplete(params);
    },

    toggleAutocompliter: function () {
        this.$('.js-other-city').hasClass('.js-other-city.opened');

        this.$('.charity-nav_city').slideToggle(300, function () {
            $(this).css('overflow', '');
        });
    },

    afterRender: function () {
        this.initAutocompliter();

        if (!this.resultsModel.query.get("cityId")) {
            this.$(".charity-nav_i").first().addClass("active");
        } else {
            var queryCityId = this.resultsModel.query.get("cityId");
            this.$('#cityId').val(queryCityId);
            if (queryCityId != 0) {
                var cityCollection = this.model.get('tagCollection');
                if (!_.any(cityCollection, function (object) {
                        return object.cityId == queryCityId;
                    })) {
                    var cityNameRus = this.getCityName(queryCityId);
                    if (cityNameRus) {
                        this.$('#cityNameRus').val(cityNameRus);
                    }
                    this.$('.js-other-city').parent().addClass('active');
                    this.toggleAutocompliter();
                }
            }
        }
    }
});


Charity.Views.Query = BaseView.extend({
    template: "#charity-query-template",

    events: {
        "input #query": "changeQueryString",
        "paste #query": "changeQueryString",
        "keypress #query": "changeQueryString"
    },

    changeQueryString: _.debounce(function () {
        this.resultsModel.changeQueryString($('#query').val());
    }, Search.INPUT_DELAY),

    construct: function (options) {
        this.resultsModel = options.resultsModel;
    },

    afterRender: function () {
        if (this.resultsModel && this.resultsModel.query) {
            this.$("#query").val(this.resultsModel.query.get("query"));
        }
    }
});


Charity.Views.ResultItem = CampaignSearch.Views.ResultItem.extend({
    beforeRender: function () {
        this.model.set({'projectCardThumb': ImageUtils.CHARITY_PROJECT_ITEM}, {silent: true});
    },
    afterRender: function () {
        var img = this.$el.find('img');
        if ($._data(img[0], 'events') === undefined) {
            img.attr('src', img.attr('data-original'));
            img.css({
                opacity: 1,
                '-webkit-filter': 'none',
                'filter': 'none'
            });
        } else {
            img.trigger('appear'); // 'appear' - lazyload binding
        }
    }
});

Charity.Views.EmptyResult = BaseView.extend({
    template: "#campaign-search-results-empty-container-template"
});


Charity.Views.SearchPage = BaseView.extend({
    template: "#charity-maincontainer-template",
    modelEvents: {
        'destroy': 'dispose'
    },
    construct: function (options) {
        this.resultsModel = options.resultsModel;
        this.addChildAtElement('.js-query-string', new Charity.Views.Query({
            resultsModel: this.resultsModel
        }));

        /* special projects */
        var specialTags = _.filter(options.campaignTags, function (tag) {
            return tag.sponsorAlias;
        });
        var specialTagsModel = new BaseModel({
            headingText: 'Спецпроекты',
            tagCollection: specialTags
        });

        this.addChildAtElement('.js-special', new Charity.Views.CategoriesBlock({
            model: specialTagsModel,
            resultsModel: this.resultsModel
        }));

        /* charity tags */
        var charityTags = _.filter(options.campaignTags, function (tag) {
            return !tag.sponsorAlias;
        });
        charityTags.unshift({name: 'Все проекты', id: 0});
        var charityTagsModel = new BaseModel({
            headingText: 'Категории проектов',
            tagCollection: charityTags
        });
        this.addChildAtElement('.js-tags', new Charity.Views.CategoriesBlock({
            model: charityTagsModel,
            resultsModel: this.resultsModel
        }));

        /* cities */
        var cityCollection = [{name: 'Все', cityId: 0},
            {name: 'Москва', cityId: 1},
            {name: 'Санкт-Петербург', cityId: 173},
            {name: 'Нижний Новгород', cityId: 1342},
            {name: 'Казань', cityId: 2090},
            {name: 'Ярославль', cityId: 2532},
            {name: 'другие города', cityId: -1, cityNameRus: '', extended: true}];
        var cityModel = new BaseModel({
            headingText: "Регионы проектов",
            tagCollection: cityCollection
        });

        this.addChildAtElement('.js-city', new Charity.Views.CityBlock({
            model: cityModel,
            resultsModel: this.resultsModel
        }));

        /* search results */
        this.addChildAtElement('.js-results', new CampaignSearch.Views.Projects({
            className: 'project-card-list project-card-list__item-3',
            collection: this.resultsModel.searchResults,
            itemViewType: Charity.Views.ResultItem,
            model: this.resultsModel
        }));
    },

    afterRender: function () {
        function charityProjectsNavMobile() {
            if (!window.isMobileDev) return;


            var projectsCont = $('.charity-projects-maincontainer');
            var projectsSidebar = $('.charity-main-sidebar_in');

            function containerSize() {
                projectsCont.css({
                    minHeight: projectsSidebar.height()
                });
            }

            $(window).on('resize.maincontainer', _.debounce(containerSize, 1000));
            containerSize();


            $(document).on('click', '.charity-main-sidebar_toggle, .charity-main-sidebar_overlay', function () {
                var sidebar = $('.charity-main-sidebar');
                var sidebarIn = $('.charity-main-sidebar_in');

                sidebar.toggleClass('open');

                if (sidebar.hasClass('open')) {
                    setTimeout(function () {
                        sidebarIn
                            .stick_in_parent({
                                parent: ".charity-main-sidebar_cont"
                            });
                    }, 400);
                } else {
                    setTimeout(function () {
                        sidebarIn
                            .trigger("sticky_kit:detach");
                    }, 400);
                }

            });

            $(".charity-main-sidebar_toggle")
                .stick_in_parent({
                    parent: ".charity-main-sidebar_wrap"
                })
                .on("sticky_kit:stick sticky_kit:unbottom", function (e) {
                    toggleFixed(e);
                })
                .on("sticky_kit:bottom sticky_kit:unstick", function (e) {
                    toggleUnFixed(e);
                });

            function toggleFixed(e) {
                $(e.target)
                    .addClass('notrns')
                    .addClass('fixed');

                setTimeout(function () {
                    $(e.target)
                        .removeClass('notrns');
                });
            }

            function toggleUnFixed(e) {
                $(e.target)
                    .addClass('notrns')
                    .removeClass('fixed');

                setTimeout(function () {
                    $(e.target)
                        .removeClass('notrns');
                });
            }

        }


        charityProjectsNavMobile();
    }
});

Charity.Views.TopProjects = DefaultListView.extend({
    className: 'project-card-list',
    itemViewType: Charity.Views.ResultItem
});


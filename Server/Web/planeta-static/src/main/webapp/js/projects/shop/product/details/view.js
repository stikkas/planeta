/*globals Comments, ProductPreview*/

/* View for similar products at the product page
 */
ProductPreview.Views.OtherProducts = BaseView.extend({
    template: '#product-other-preview-template',
    construct: function () {
        this.addChildAtElement('.js-anchor-other-products-list', new Products.Views.HorizontalProductListView({
            collection: new BaseCollection(this.model.get('otherProducts'))
        }));
    }
});


/* View for yellow (or blue for preorderable product) button on product page
 * action - add current product to the cart
 */
ProductPreview.Views.AddToCartControl = BaseView.extend({
    template: '#add-to-cart-control-template',

    events: {
        'click .js-add-to-cart': 'addToShoppingCart'
    },

    addToShoppingCart: function (e) {
        e.preventDefault();
        var self = this;
        if (this.model.get('canPurchase')) {
            workspace.shoppingCart.addProduct(this.model.get('product').productId, 1).done(function (response) {
                if (response.success === false) {
                    workspace.appView.showErrorMessage(response.errorMessage);
                    return;
                }

                if (self.model.get('digital')) {
                    self.model.set({'canPurchase': false,
                        'alreadyInCart': true
                    });
                }

                self.showNotification();  // show product
            });
        } else {
            workspace.appView.showErrorMessage('К сожалению, товар отсутствует в продаже.');
        }
    },

    showNotification: function () {
        this.addChildAtElement('.js-anchor-added-to-cart', new Shop.Views.ShoppingCartPopupNotification({
            model: this.model
        }));
    },

    construct: function () {
        var product = this.model.get('product');
        this.model.set({'startSaleDate': product.startSaleDate,
            'productCategory': product.productCategory,
            'canPurchase': this.model.get('canPurchase')
        });
    }
});


ProductPreview.Views.AddToCartExtendedControl = BaseView.extend({
    template: '#add-to-cart-extended-control-template',
    events: {
        'click .js-plus': 'onPlus',
        'click .js-minus': 'onMinus'
    },

    construct: function () {
        var donatePrice = this.model.get("product").donate.price;
        this.model.set({
            minDonatePrice: donatePrice,
            donatePrice: donatePrice
        });
    },

    onPlus: function () {
        var donatePrice = this.model.get("donatePrice");
        var minDonatePrice = this.model.get("minDonatePrice");
        this.model.set({donatePrice: donatePrice + minDonatePrice});
    },

    onMinus: function () {
        var donatePrice = this.model.get("donatePrice");
        var minDonatePrice = this.model.get("minDonatePrice");
        var currentPrice = donatePrice - minDonatePrice;
        if (currentPrice >= minDonatePrice) {
            this.model.set({donatePrice: currentPrice});
        }
    },

    showNotification: function (isDonate, popupPrice) {
        var product;
        if (isDonate) {
            product = this.model.get("product").donate;
        } else {
            product = this.model.get("product");
        }
        this.addChildAtElement('.js-anchor-added-to-cart', new Shop.Views.ShoppingCartPopupNotification({
            model: new BaseModel({
                product: product,
                popupPrice: popupPrice
            })
        }));
    }
});

ProductPreview.Views.AddToCartDonateControl = ProductPreview.Views.AddToCartExtendedControl.extend({
    events: _.extend({}, ProductPreview.Views.AddToCartExtendedControl.prototype.events, {
        'click .js-add-to-cart': 'addToShoppingCartDonate'
    }),

    addToShoppingCartDonate: function (e) {
        e.preventDefault();

        if (this.model.get('canPurchase') && this.model.get('product').donate) {
            var price = this.model.get('minDonatePrice');
            var newprice = this.model.get('donatePrice');
            if (price < 0 || price > newprice || !newprice) {
                console.log("wrong newprice");
                return;
            }
            var qty = newprice / price;

            var self = this;
            workspace.shoppingCart.addProduct(this.model.get('product').donate.objectId, qty).done(function (response) {
                if (response.success === false) {
                    workspace.appView.showErrorMessage(response.errorMessage);
                    return;
                }

                self.showNotification(true, newprice); // show donate
            });
        } else {
            workspace.appView.showErrorMessage('К сожалению, товар отсутствует в продаже.');
        }
    }
});

ProductPreview.Views.AddToCartExpensiveControl = ProductPreview.Views.AddToCartExtendedControl.extend({
    events: _.extend({}, ProductPreview.Views.AddToCartExtendedControl.prototype.events, {
        'click .js-add-to-cart': 'addToShoppingCartExpensive'
    }),

    addToShoppingCartExpensive: function (e) {
        e.preventDefault();

        if (this.model.get('canPurchase') && this.model.get('product').donate) {
            var price = this.model.get('minDonatePrice');
            var newprice = this.model.get('donatePrice');
            if (price < 0 || price > newprice || !newprice) {
                console.log("wrong newprice");
                return;
            }
            var qty = newprice / price;

            var self = this;
            workspace.shoppingCart.addProduct(this.model.get('product').productId, 1).done(function (response) {
                if (response.success === false) {
                    workspace.appView.showErrorMessage(response.errorMessage);
                    return;
                }
                workspace.shoppingCart.addProduct(self.model.get('product').donate.objectId, qty).done(function (response) {
                    if (response.success === false) {
                        workspace.appView.showErrorMessage(response.errorMessage);
                    }
                });

                self.showNotification(false, self.model.get('product').price + newprice);  // show product with additional price
            });
        } else {
            workspace.appView.showErrorMessage('К сожалению, товар отсутствует в продаже.');
        }
    }
});



ProductPreview.Views.ImageListItem = BaseView.extend({
    className: 'product-preview_i',
    template: '#image-list-item-template',
    viewEvents: {
        'click': 'onImageItemClick'
    }
});


ProductPreview.Views.ImageList = DefaultContentScrollListView.extend({
    dontScrollPage: true,
    arrows: false,
    autoAdjustContainerSize: true,
    stickToBottom: false,
    itemViewType: ProductPreview.Views.ImageListItem,
    className: 'product-preview_list'
});

ProductPreview.Views.ExtendedControlsContainer = BaseView.extend({
    template: '#add-to-cart-extended-container-template',

    afterRender: function () {
        var donateView = new ProductPreview.Views.AddToCartDonateControl({
            el: '.js-anchor-donate',
            model: new BaseModel({
                canPurchase: this.model.get('canPurchase'),
                product: this.model.get("product"),
                type: 'DONATE'
            })
        });
        donateView.render();

        var expensiveView = new ProductPreview.Views.AddToCartExpensiveControl({
            el: '.js-anchor-expensive',
            model: new BaseModel({
                canPurchase: this.model.get('canPurchase'),
                product: this.model.get("product"),
                type: 'EXPENSIVE'
            })
        });
        expensiveView.render();

    }
});

ProductPreview.Views.ExtendedControlsContainer = BaseView.extend({
    template: '#add-to-cart-extended-container-template',

    afterRender: function () {
        console.log('ExtendedControlsContainer render');

        var donateView = new ProductPreview.Views.AddToCartDonateControl({
            el: '.js-anchor-donate',
            model: new BaseModel({
                canPurchase: this.model.get('canPurchase'),
                product: this.model.get("product"),
                type: 'DONATE'
            })
        });
        donateView.render();

        var expensiveView = new ProductPreview.Views.AddToCartExpensiveControl({
            el: '.js-anchor-expensive',
            model: new BaseModel({
                canPurchase: this.model.get('canPurchase'),
                product: this.model.get("product"),
                type: 'EXPENSIVE'
            })
        });
        expensiveView.render();

    }
});

/* View for product main page
 Use for switching images and child products
 */
ProductPreview.Views.Main = BaseRichView.extend({
    events: {
        'onImageItemClick': 'switchImage',
        'click .js-child-product:enabled': 'changeChildProduct'
    },

    construct: function () {
        var comments = this.model.get('comments');
        var images = this.model.get('product').imageUrls;
        var collection = new BaseCollection(_.map(images, function (img) {
            return {imageUrl: img}
        }));
        this.addChildAtElement('.scroll-content', new ProductPreview.Views.ImageList({
            collection: collection
        }));

        this.addChildAtElement('.js-anchor-comments', new Comments.Views.ModalComments({
            model: comments
        }));

        this.addChildAtElement('.js-anchor-other-products', new ProductPreview.Views.OtherProducts({
            model: this.model
        }));


        this.addChildAtElement('.js-anchor-cart-controls', new ProductPreview.Views.AddToCartControl({
            model: this.model.get('selectedProduct')
        }));

        var donateId = this.model.get('product').donateId;
        if (donateId) {
            this.addChildAtElement('.js-anchor-extended-controls', new ProductPreview.Views.ExtendedControlsContainer({
                model: this.model.get('selectedProduct')
            }));
        }

        var cover = this.$('.product-cover_img');
        cover.zoom({
            url: cover.data('src')
        });


        this.setChildChecked();

        if (window.gtm) {
            window.gtm.trackViewProductPage(this.model.get('product'));
        }
    },

    afterRender: function () {
        BaseRichView.prototype.afterRender.call(this);
        var self = this;
        moduleLoader.loadModule('fancybox').done(function () {
            self.$('.photo-zoom-link').fancybox({
                padding: 0,
                minWidth: 800,
                minHeight: 600,
                autoHeight: true,
                autoWidth: true,
                autoResize: true
            });
        });
        $(window).on('resize', _.debounce(function () {
            $('.product-preview_list-wrap .scroll-content').scrollbar('repaint');
        }, 1000));
    },

    setChildChecked: function () {
        var product = this.model.get('product');
        if (product.productState === 'META') {
            var selectedChildId = this.model.get('selectedProduct').get('product').objectId;
            if (selectedChildId) {
                this.$('#radio-' + selectedChildId).prop('checked', true);
            }
        }
    },

    changeChildProduct: function (e) {
        var $el = $(e.currentTarget);
        return this.model.switchProduct($el.data('childId'), this.model.get("product"));
    },

    switchImage: function (e, view) {
        e.preventDefault();
        var imageUrl = view.model.get('imageUrl');
        this.$('.product-preview_i.active').removeClass('active');
        view.$el.addClass('active');
        var cover = this.$('.product-preview_cover');
        cover.find('img').attr('src', ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.ORIGINAL, ImageType.PHOTO));
    }
});
/*globals News */
News.Models.Search = {};
News.Views.Search = {};

News.Models.Search.Posts = News.Models.Posts.extend({
    url: '/search-posts.json',

    parse: function (response) {
        return response.searchResultRecords || [];
    }
});

News.Models.Search.Main = BaseModel.extend({

    initialize: function () {
        var query = this.getQuery();
        this.set({
            query: query,
            posts: new News.Models.Search.Posts([], {
                data: {
                    query: query,
                    filter: 'ALL'
                },
                limit: 10
            })
        });
    },

    fetch: function () {
        this.get('posts').load();
        BaseModel.prototype.fetch.apply(this, arguments);
    },

    getQuery: function () {
        var query = this.get('objectId');
        return _.isEmpty(query) ? '' : decodeURIComponent(query.substr('query='.length, query.length).replace(/\./g, '%'));
    },

    setFilterData: function (query, filterType) {
        var posts = this.get('posts');
        var newFilter = {
            query: query,
            filter: filterType
        };
        var curFilter = {
            query: posts.data.query,
            filter: posts.data.filter
        };

        if (!_.isEqual(curFilter, newFilter)) {
            _.extend(posts.data, newFilter);
            posts.load();
        }
    }
});
var vjsPlayer = {
    player: {},
    controlBar: {},
    track: {},
    progressBar: {},
    captions: {},
    isSeeking: false,
    init: function (elem, options) {
        var self = this;

        var resolution = parseInt(vjsPlayer.utils.storage.get('vjsResolution')) || 'high';
        if ( !videojs.browser.IS_IPAD && (videojs.browser.IS_IOS || videojs.browser.IS_ANDROID) ) {
            resolution = 480;
        }

        if ( !options ) {
            options = {};
        } else {
            if ( options.autoplay != undefined ) {
                options.bigPlayButton = !options.autoplay;
            }

            if ( options.poster && (videojs.browser.IS_IOS || videojs.browser.IS_ANDROID) ) {
                options.posterUrl = options.poster;
                delete options.poster;
            }

            if ( !(options.poster && options.bigPlayButton) ) {
                delete options.poster;
            }

        }

        var defaults = {
            autoplay: true,
            bigPlayButton: false,
            loop: false,
            playsinline: true,
            nativeControlsForTouch: (videojs.browser.IS_IOS || videojs.browser.IS_ANDROID),
            html5: {
                nativeTextTracks: false
            },
            controlBar: {
                playToggle: false,
                volumePanel: false,
                currentTimeDisplay: false,
                timeDivider: false,
                durationDisplay: false,
                progressControl: false,
                liveDisplay: false,
                remainingTimeDisplay: false,
                customControlSpacer: false,
                playbackRateMenuButton: false,
                chaptersButton: false,
                descriptionsButton: false,
                subtitlesButton: false,
                captionsButton: false,
                audioTrackButton: false,
                fullscreenToggle: false,
                subsCapsButton: false
            },
            inactivityTimeout: 1000,
            plugins: {
                videoJsResolutionSwitcher: {
                    'default': resolution,
                    dynamicLabel: true
                },
                thumbnails: {
                    enabled: !(videojs.browser.IS_IOS || videojs.browser.IS_ANDROID)
                }
            }
        };


        options = _.extend(defaults, options);


        this.player = videojs(elem, options, function() {

            if ( options.posterUrl ) {
                vjsPlayer.setNativePoster(options.posterUrl);
            }

            setTimeout(function () {
                vjsPlayer.player.volume(vjsPlayer.utils.storage.get('vjsVolume') || 1);
                vjsPlayer.player.muted(JSON.parse(vjsPlayer.utils.storage.get('vjsMuted')) || false);
            });
            this.on('volumechange', function () {
                vjsPlayer.utils.storage.set('vjsVolume', vjsPlayer.player.volume());
                vjsPlayer.utils.storage.set('vjsMuted', vjsPlayer.player.muted());
            });

            this.on('resolutionchange', function (e, i) {
                var res = vjsPlayer.player.currentResolution().sources[0].res;
                vjsPlayer.utils.storage.set('vjsResolution', res);
            });

            this.textTrackSettings.setValues({
                fontPercent: 1,
                backgroundOpacity: "0"
            });

            this.hotkeys({
                enableVolumeScroll: false,
                volumeStep: 0.1,
                seekStep: 5,
                enableModifiersForNumbers: false,
                fullscreenKey: function(event) {
                    return (event.which === 70 && !(event.ctrlKey || event.altKey || event.metaKey || event.shiftKey));
                }
            });


            this.on('ended', function seekingProgress() {
                this.one('seeking', function seekingProgress() {
                    self.player.paused() && self.player.play();
                });
            });


        });


        this.controlBar = this.player.controlBar;


        this.registerPlayerControls();
    },

    utils: {
        localStorageTest: (function () {
            var test = 'lstest';
            try {
                localStorage.setItem(test, test);
                localStorage.removeItem(test);
                return true;
            } catch(e) {
                return false;
            }
        })(),
        storage: {
            'set': function (key, value) {
                if ( vjsPlayer.utils.localStorageTest !== true ) return;
                localStorage.setItem(key, value);
            },
            'get': function (key) {
                if ( vjsPlayer.utils.localStorageTest !== true ) return;
                return localStorage.getItem(key);
            }
        },
        videoSupport: function () {
            var testEl = document.createElement( "video" ),
                h264;
            if ( testEl.canPlayType ) {
                return h264 = "" !== ( testEl.canPlayType( 'video/mp4; codecs="avc1.42E01E"' )
                    || testEl.canPlayType( 'video/mp4; codecs="avc1.42E01E, mp4a.40.2"' ) );
            } else {
                return false;
            }
        }
    },

    setNativePoster: function (poster) {
        this.player.tech_.el_.setAttribute('poster', poster);
        // videojs.dom
        /*$('.vjs-poster').css({
         backgroundImage: 'url(' + poster + ')'
         }).removeClass('vjs-hidden');*/
    },

    playerView: function () {
        this.destroyAllControls();

        this.controlBar.addChild('playToggle');
        this.controlBar.addChild('volumePanel', {inline: false});
        this.controlBar.addChild('Progress');
        this.controlBar.addChild('TimeDuration');
        this.controlBar.addChild('fullscreenToggle');
    },

    destroyAllControls: function () {
        this.controlBar.removeChild('playToggle');
        this.controlBar.removeChild('volumePanel');

        //this.controlBar.removeChild('CustomControlSpacer');
        this.controlBar.removeChild('Progress');
        this.controlBar.removeChild('TimeDuration');
        this.controlBar.removeChild('FullscreenToggle');
    },


    registerPlayerControls: function () {
        var self = this;
        var vjsComponent = videojs.getComponent('Component');
        var vjsProgressControl = videojs.getComponent('ProgressControl');

        var mouseDown = 0;
        $(document).on('mousedown touchstart', function (e) {
            ++mouseDown;
        });
        $(document).on('mouseup touchend', function () {
            --mouseDown;
        });


        /* Progress */
        var Progress = videojs.extend(vjsProgressControl, {
            constructor: function() {
                vjsProgressControl.apply(this, arguments);

                var progress = this;
                self.progressBar = progress;
                var seekBar = this.seekBar;
                seekBar.playProgressBar.removeChild("TimeTooltip");
                seekBar.off(['mousemove', 'mousedown']);


                progress.on(['mousedown', 'touchstart'], function (event) {
                    seekBar.handleMouseMove(event);

                    self.player.one('seeking', function seekingProgress() {
                        if ( self.isSeeking || self.player.paused() || !mouseDown ) return;
                        self.isSeeking = true;

                        var seekingTimeout = setTimeout(function() {
                            !!mouseDown && self.player.pause();
                        }, 100);

                        $(document).off('mouseup.seeking touchend.seeking keyup.seeking');

                        $(document).one('mouseup.seeking touchend.seeking keyup.seeking', function () {
                            if ( !self.isSeeking ) return;
                            self.isSeeking = false;
                            clearTimeout(seekingTimeout);
                            self.player.paused() && self.player.play();
                        });
                    });
                });


                var doc = progress.el_.ownerDocument;
                progress.on(doc, 'mousemove', progress.handleMouseMove);

                this.on(['mousedown', 'touchstart'], function () {
                    progress.addClass('vjs-progress-dragging');
                    $(document).one('mouseup touchend', function () {
                        progress.removeClass('vjs-progress-dragging');
                    });
                });

            }
        });
        videojs.registerComponent('Progress', Progress);
        /* / Progress */



        /* TimeDuration */
        var TimeDuration = videojs.extend(vjsComponent, {
            constructor: function() {
                vjsComponent.apply(this, arguments);
            },
            createEl: function() {
                return videojs.dom.createEl('div', {
                    className: 'vjs-control vjs-time-duration'
                });
            }
        });
        TimeDuration.prototype.options_ = {
            children: [
                'currentTimeDisplay',
                'timeDivider',
                'durationDisplay'
            ]
        };
        videojs.registerComponent('TimeDuration', TimeDuration);
        /* / TimeDuration */
    }
};


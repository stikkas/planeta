tinyMCEPopup.requireLangPack();

var PhotoDialog = {
	init : function() {
		var f = document.forms[0];

		// Get the selected contents as text and place it in the input
        f.url.value = tinyMCEPopup.editor.selection.getContent({format : 'text'});
        if(f.url.value == '')
            f.url.value = tinyMCEPopup.getWindowArg('example_url');
	},

	insert : function() {
		// Insert the contents from the input into the document
        var html = tinyMCEPopup.getWindowArg('html');
        html = html.replace('PLACEHOLDER', document.forms[0].url.value);
		tinyMCEPopup.editor.execCommand('mceInsertContent', false, html);
		tinyMCEPopup.close();
	}
};

tinyMCEPopup.onInit.add(PhotoDialog.init, PhotoDialog);

(function () {
    /*
     * designations used:
     * html - DOM node with nested structure or real HTML
     * object - dom-like structure node in editor (repeats object nesting)
     * img - dom-like structure node in editor
     * json - pure data of the object
     *
     * DomObject(HTML) transforms into Editor Node using serializer/parser
     * Defining a filter for serializer/parser transforms html into img Node
     * this filter works with imgToObject/objectToImg functions
     * each of these functions works throught JSON
     * user must define JSON <-> object mapping
     * JSON <-> img mapping use data-mce-json image attribute
     * native <IMG> attributes are reserved for editing img-object in editor,
     * don't use it for mapping
     *
     * VERY IMPORTANT to not check one node twice, it leads to
     * node.replace() crash
     * */
// todo-s.kalmykov: editor internals replaces <i> to <em>,
// todo-s.kalmykov: empty <a> becomes <br/>
// todo-s.kalmykov: empty <span> becomes NULL
    tinymce.create('tinymce.plugins.DivToImgPlugin', {

        init: function (ed, url) {
        },

        Mapping: function (editor, objectClassName, objectTag) {
            var self = this;
            self.clsName = objectClassName;
            self.objectTag = objectTag;
            if (self.objectTag == undefined)
                self.objectTag = 'div';
            self.editor = editor;
            self.rootAttributes = tinymce.explode('id,name,class,src');
            self.styleAttributes = tinymce.explode('width,height,align,float,hspace,vspace,background,display');
            self.Node = tinymce.html.Node;

            self.jsonToObject = function (json) {
                var err = 'override self.jsonToNode method';
                throw(err);
            };

            self.objectToJSON = function (node) {
                var err = 'override self.nodeToJSON method';
                throw(err);
            };

            self.attrs = null;

            self.imgToJSON = function (img) {
                var data = tinymce.util.JSON.parse(img.attr('data-mce-json')) || {};
                var style = img.attr('style');
                if (style) {
                    $(self.styleAttributes).each(function (i, styleAttr) {
                        var stringS = styleAttr + '\\s*:\\s*(\\S*?)(;|$)';
                        var re = new RegExp(stringS);
                        var match;
                        if (match = style.match(re)) {
                            data[styleAttr] = match[1];
                            style = style.replace(re, '');
                        }
                    });
                }
                $(self.rootAttributes).each(function (i, el) {
                    if (img.attr(el)) {
                        data[el] = img.attr(el);
                    }
                });
                return data;
            };

            self.jsonToImg = function (json, trueDom) {
                var data = {};
                $.each(json, function (key, value) {
                    data[key] = value;
                });
                var opts = {};
                $(self.rootAttributes).each(function (i, el) {
                    if (data[el]) {
                        opts[el] = data[el];
                        delete(data[el]);
                    }
                });
                var style = '';
                $(self.styleAttributes).each(function (i, sa) {
                    if (data[sa]) {
                        style += sa + ':' + data[sa] + ';';
                        delete(data[sa]);
                    }
                });
                opts['style'] = style;
                opts['class'] = self.clsName + ' mceItemNoResize ' + (json['class']?json['class']:'');
                opts['unselectable'] = 'on';
                opts['data-mce-json'] = tinymce.util.JSON.serialize(data, "'");

                var img = self.Node.create('img', opts);
                if (trueDom) {
                    img = self.editor.dom.create('img', opts);
                }

                return img;
            };

            self.imgToObject = function (img) {
                var data = self.imgToJSON(img);
                $.each(self.attrs, function (key, value) { // check required attrs
                    // img node cannot have no required attrs (objectToImg error)
                    if (value.required && data[key] == undefined)
                        throw('cant find required attr ' + key);
                    if (data[key] == undefined)
                        data[key] = value.empty;
                });
                var node = self.jsonToObject(data);
                img.replace(node);
            };


            self.objectToImg = function (node) {
//                if(!node.parent)
//                    node.parent = self.editor.getBody();
                if (!self.attrs) {
                    var err = 'override self.attrs property';
                    alert(err);
                    throw(err);
                }
                var data = self.objectToJSON(node);
                if (data == null) {
                    return; // not object = no replacement
                }
                var img = self.editor.parser.parse(' ').firstChild; // empty element
                parsed = true;

                $.each(self.attrs, function (key, value) {
                    // img node cannot have no required attrs (objectToImg error)
                    if (value.required && data[key] == undefined)
                        parsed = false;
                    if (data[key] == undefined)
                        data[key] = value.empty;
                });
                if (parsed) {
                    img = self.jsonToImg(data);
                }
//                node.parent.appendNode(img)
                node.replace(img);
            };

            self.imgToHtml = function (img, force_absolute) {
                return self.editor.serializer.serialize(img,
                {forced_root_block: '', force_absolute: force_absolute});
            };

            self.domToHtml = function (domNode) {
                return self.editor.serializer.serialize(domNode,
                {forced_root_block: '', force_absolute: true});
            };

            self.htmlToImg = function (html) {
                var fragment = self.editor.parser.parse(html);
                return fragment.getAll('img')[0];
            };

            self.jsonToHtml = function (data, force_absolute) {
                var img = self.jsonToImg(data, true);
                return self.imgToHtml(img, force_absolute);
            };

            self.htmlToJSON = function (html) {
                var img = self.htmlToImg(html);
                return data = self.imgToJSON(img);
            };

            self.editor.onPreInit.add(function () {
                // Convert object elements to image placeholder
                self.editor.parser.addNodeFilter(self.objectTag, function (nodes) {
                    var i = nodes.length;

                    while (i--)
                        self.objectToImg(nodes[i]);
                });

                // Convert image placeholders to video elements
                self.editor.serializer.addNodeFilter('img', function (nodes, name, args) {
                    var i = nodes.length, node;

                    while (i--) {
                        node = nodes[i];
                        if ((node.attr('class') || '').indexOf(self.clsName) !== -1)
                            self.imgToObject(node);
                    }
                });
            });


//            var getRootNode = function(n) {
//                self.body = self.editor.getBody();
//                if(n == self.body)
//                    return n;
////                n = n.parentNode;
//                while (n.parentElement != self.body) {
//                    n = n.parentElement;
//                }
//                return n;
//            };

//            editor.onNodeChange.add(function(ed, cm, n) {
//                var node = $(n);
//                if(n.nodeName === 'IMG' && node.hasClass(self.clsName)) {
//                    var container = getRootNode(n.parentNode);
//                    if(container != self.body){
//                        node.insertAfter(container);
//                        console.log(self.body);
//                    }
//
//                }
//            });


            return self;
        }


    });

    tinymce.PluginManager.add('divtoimgmapping', tinymce.plugins.DivToImgPlugin);

    tinymce.plugins.DivToImgPlugin.Mapping = tinymce.plugins.DivToImgPlugin.prototype.Mapping;
})();


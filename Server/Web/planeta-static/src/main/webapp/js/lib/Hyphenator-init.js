/*globals Hyphenator*/
try {
    Hyphenator.config({
        remoteloading: false,
        defaultlanguage: 'ru',
        onerrorhandler: function (e) {
            //do nothing
        }
    });
    Hyphenator.run();
    Hyphenator.hyphenateView = function (view) {
        try {
            var hypElements = view.$('.hyphenate');
            hypElements.each(function () {
                $(this).html(Hyphenator.hyphenate($(this).html(), 'ru'));
            });
        } catch (ex) {
        }
    };
} catch (ex) {
}

function TransitionAvalanche(elems) {
    var arr = [];
    var arrActive = [];
    for ( var i in elems ) {
        var p = new addTransition(elems[i]);
        arr.push(p);
    }

    function addTransition(obj) {
        this.initAnim = function() {
            if (arrActive.length > 1){
                if ($.support['transition']) {
                    obj.elem.bind('transitionend.avalanche', function(e) {
                        e.stopPropagation();
                        if (e.target == $(this)[0]) {
                            obj.elem.unbind('transitionend.avalanche');
                            animApply();
                        }
                    });
                } else {
                    animApply();
                }
            }

            obj.anim.call();
        };
    }

    function animApply() {
        arrActive[0].initAnim();
        arrActive.shift();
    }

    this.init = function() {
        arrActive = arr.slice(0);
        setTimeout(function() {
            animApply();
        });
    };
}

function transitionSteps(obj) {
    obj.wrap.css({
        height: obj.wrap.height(),
        width: obj.wrap.width()
    });

    var anim = new TransitionAvalanche([
        {
            elem: obj.step1,
            anim: function() {
                obj.step1.removeClass('in');
            }
        },
        {
            elem: obj.wrap,
            anim: function() {
                obj.step1.addClass('hide');
                obj.wrap.addClass('size-transition');

                var newHeight = obj.step2.outerHeight();
                var newWidth = obj.step2.outerWidth();

                if ( obj.wrap.width() == newWidth && obj.wrap.height() == newHeight ) {
                    newHeight++;
                }

                obj.wrap.css({
                    height: newHeight,
                    width: newWidth
                });
            }
        },
        {
            anim: function() {
                obj.step2.removeClass('hide');
                setTimeout(function() {
                    obj.step2.addClass('in');
                    obj.wrap.css({
                        height: '',
                        width: ''
                    });
                });
            }
        }
    ]);

    anim.init();
}
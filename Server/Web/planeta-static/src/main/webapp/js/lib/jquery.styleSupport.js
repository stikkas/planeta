$.fn.styleSupport = function( prop ) {
    if ( !!$.support[prop] ) return $.support[prop];

    var vendorProp, supportedProp,
        capProp = prop.charAt(0).toUpperCase() + prop.slice(1),
        prefixes = [ "Moz", "Webkit", "O", "ms" ],
        div = document.createElement( "div" );

    if ( prop in div.style ) {
        supportedProp = prop;
    } else {
        for ( var i = 0; i < prefixes.length; i++ ) {
            vendorProp = prefixes[i] + capProp;
            if ( vendorProp in div.style ) {
                supportedProp = vendorProp;
                break;
            }
        }
    }

    div = null;

    $.support[ prop ] = supportedProp;

    return supportedProp;
}
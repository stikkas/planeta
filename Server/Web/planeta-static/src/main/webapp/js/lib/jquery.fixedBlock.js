/**
 * @author a.pavlov <joseperez@bk.ru>, Andrew.Arefyev@gmail.com
 * Date: 15.08.13
 * Time: 17:01
 */
(function ($) {
    var defaults = {
        fixedClass: 'fixed'
    };
    var $window = $(window);
    $.fn.fixedBlock = function (options) {
        return $(this).each(function () {
            var blockOuterHeight;
            var blockWrap = $(this);
            if (!blockWrap.length) {
                return this;
            }
            if (options === "remove") {
                $window.off('scroll.fixedBtn' + blockWrap.data("uid"));
                $window.off('resize.fixedBtn' + blockWrap.data("uid"));
                return this;
            }

            var _options = $.extend({}, defaults, options);
            var fixedClass = _options.fixedClass;
            blockOuterHeight = blockWrap.outerHeight();
            // console.log(blockOuterHeight);
            // blockWrap.height(blockOuterHeight);
            function fixedScroll() {
                var screenHeight = $window.height();
                var winTop = $window.scrollTop();
                var winBottom = winTop + screenHeight;
                var positionTop = blockWrap.offset().top;
                var position = positionTop + blockOuterHeight;

                if (_options.type === 'top') {
                    if (winTop > positionTop) {
                        blockWrap.addClass(fixedClass);
                    } else {
                        blockWrap.removeClass(fixedClass);
                    }
                } else {
                    if (winBottom > position) {
                        blockWrap.removeClass(fixedClass);
                    } else {
                        blockWrap.addClass(fixedClass);
                    }
                }
            }


            var uid = parseInt(Math.random() * 0xFFFF, 10);
            $window.on('scroll.fixedBtn' + uid, fixedScroll);
            $window.on('resize.fixedBtn' + uid, fixedScroll);
            blockWrap.data("uid", uid);
            fixedScroll();
            return this;
        });
    };
}(jQuery));

(function($) {

    var _transitionAvalanche = function(elems) {

        if ($.support['transition']) {
            for (var i in elems) {
                if (typeof elems[i] != 'function') {

                    if (elems[i].elem) {
                        elems[i].elem.one('transitionend', parseInt(i) + 1, function(e) {
                            e.stopPropagation();
                            elems[e.data].anim.call(elems[e.data].elem);
                        });
                    }
                }
            }
            elems[0].anim();
        } else {
            for (var i in elems) {
                elems[i].anim.call(elems[i].elem);
            }
        }

    };

    /**
     *
     * @param selectorFrom
     * @param selectorTo
     */
    var _flow = function(selectorFrom, selectorTo) {
        var $this = $(this);
        var $from = $this.find(selectorFrom);
        var $to = $this.find(selectorTo);

        $this.css({
            height: $this.height(),
            width: $this.width()
        });

        _transitionAvalanche([
            {
                elem: $from,
                anim: function() {
                    $from.removeClass('in');
                }
            },
            {
                elem: $this,
                anim: function() {
                    $from.addClass('hide');
                    this.addClass('size-transition').css({
                        height: $to.outerHeight(),
                        width: $to.outerWidth()
                    });
                }
            },
            {
                anim: function() {
                    $to.removeClass('hide').queue(function(next) {
                        setTimeout(function() {
                            $to.addClass('in');
                            next();
                        }, 150);
                    });
                }
            }
        ]);

    };
    $.fn.flowForm = function(selectorFrom, selectorTo) {
        return this.each(function() {
            _flow.call(this, selectorFrom, selectorTo);
        });
    }
})(jQuery);

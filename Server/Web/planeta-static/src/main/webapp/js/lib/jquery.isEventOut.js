function isEventOut(blocks, e) {
    var r = true;
    $(blocks).each(function() {
        if (!r) return;
        if ($(e.target).closest('HTML', $(this).get(0)).length==0) {r = false; return;}
        if ($(e.target).get(0)==$(this).get(0)) {r = false; return;}
    });
    return r;
}
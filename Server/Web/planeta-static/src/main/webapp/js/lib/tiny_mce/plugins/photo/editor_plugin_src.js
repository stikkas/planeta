/**
 * editor_plugin_src.js
 *
 * Copyright 2009, Moxiecode Systems AB
 * Released under LGPL License.
 *
 * License: http://tinymce.moxiecode.com/license
 * Contributing: http://tinymce.moxiecode.com/contributing
 */

(function() {
    // Load plugin specific language pack
    tinymce.PluginManager.requireLangPack('photo');

    tinymce.create('tinymce.plugins.PhotoPlugin', {
                /**
                 * Initializes the plugin, this will be executed after the plugin has been created.
                 * This call is done before the editor instance has finished it's initialization so use the onInit event
                 * of the editor instance to intercept that event.
                 *
                 * @param {tinymce.Editor} ed Editor instance that the plugin is initialized in.
                 * @param {string} url Absolute URL to where the plugin is located.
                 */

                init : function(ed, url) {
                    // singleton class with dettachable dom node
                    function PhotoContextMenu(activeEditor, imgClsName) {
                        var self = this;
                        var handleSize = 7;

                        self.clsName = 'mce-photo-context-menu';
                        self.closeClsName = 'mce-photo-close';
                        self.alignClsName = 'mce-photo-align';
                        self.clsNames = {menu: 'mce-photo-context-menu', close: 'mce-photo-close',
                            align: 'mce-photo-align', img: imgClsName};
                        self.photoUrlSelectHtml = tinymce.DOM.createHTML('button', {'class': 'clear'}, 'X')
                                + tinymce.DOM.createHTML('input')
                                + tinymce.DOM.createHTML('button', {'class': 'submit'}, 'OK');
                        self.urlSelectNode = tinymce.DOM.create('div', {'class': self.clsNames.menu + ' mceItemNoResize'},
                                self.photoUrlSelectHtml);
                        self.closeNode = tinymce.DOM.create('div', {'class': self.clsNames.close + ' mceItemNoResize'});
                        self.alignSelectHtml = tinymce.DOM.createHTML('button', {'class': 'left'}, '<')
                                + tinymce.DOM.createHTML('button', {'class': 'block'}, 'b')
                                + tinymce.DOM.createHTML('button', {'class': 'inline'}, 'i')
                                + tinymce.DOM.createHTML('button', {'class': 'right'}, '>');
                        self.alignNode = tinymce.DOM.create('div', {'class': self.clsNames.align + ' mceItemNoResize'},
                                self.alignSelectHtml);
                        // image resizing wrapper
                        self.imgWrapper = tinymce.DOM.create('div', {'class' : 'mce-photo-resize-wrapper'});
//                        self.imgWrapperHandleS = tinymce.DOM.create('div', {'class' : 'mce-photo-resize-handle-s'});
                        self.imgWrapperHandleE = tinymce.DOM.create('div', {'class' : 'mce-photo-resize-handle-e'});
//                        self.imgWrapperHandleSE = tinymce.DOM.create('div', {'class' : 'mce-photo-resize-handle-se'});
//                        self.imgWrapper.appendChild(self.imgWrapperHandleS);
                        self.imgWrapper.appendChild(self.imgWrapperHandleE);
//                        self.imgWrapper.appendChild(self.imgWrapperHandleSE);
                        function setWrapperSize() {
                            // подгоняет размер обертки под картинку
                            self.imgWrapper.style.width = self.imageRef.offsetWidth + handleSize + 'px';
                            self.imgWrapper.style.height = self.imageRef.offsetHeight + handleSize + 'px';
                        }

                        function startResize(type) {

                            self.body.onmousemove = function(e) {
//                              e = fixEvent(e);

                                self.detachNode();
                                var offset = {
                                    top : self.imgWrapper.offsetTop,
                                    left : self.imgWrapper.offsetLeft
                                };

                                var newHeight, newWidth;

                                if (type.match(/s/)) {
                                    // в ручке есть буква "s" - значит меняем высоту картинки
                                    newHeight = Math.max(e.pageY - offset.top, handleSize);
                                    self.imageRef.style.height = newHeight + 'px';
                                }
                                if (type.match(/e/)) {
                                    // в ручке есть буква "e" - значит меняем ширину картинки
                                    newWidth = Math.max(e.pageX - offset.left, handleSize);
                                    self.imageRef.style.width = newWidth + 'px';
                                }
//                                if (type.match(/se/)) {
//                                  newHeight = Math.max(e.pageY - offset.top, handleSize);
//                                 self.imgWrapper.style.height = newHeight + 'px';
//                                  newWidth = Math.max(e.pageX - offset.left, handleSize);
//                                   self.imgWrapper.style.width = newWidth + 'px';
//                                }


                                // подогнать обертку
                                self.setWrapperSize();

                            }

                            self.body.onmouseup = function() {
                                // конец ресайза
                                self.body.onmousemove = self.body.onmouseup = null;
                                self.unsetResizable();
//                              Eventer.trigger(self, "resize", [newWidth, newHeight]);
                            }

                        }

                        self.setWrapperSize = setWrapperSize;
                        self.imgWrapper.onmousedown = function(e) {
//                            e = fixEvent(e);

                            var className = e.target.className;
                            if (className.indexOf("mce-photo-resize-handle-") == 0) {
                                // поймали клик на "ручке" - вызываем начало ресайза
                                var type = className.slice("mce-photo-resize-handle-".length);
                                startResize(type);
                            }
                            return false;

                        };
                        self.imgWrapper.onselectstart = self.imgWrapper.ondragstart = function() {
                            return false;
                        };

                        self.urlSelectNode.contentEditable = false;
                        self.closeNode.contentEditable = false;
                        self.alignNode.contentEditable = false;
                        self.imgWrapper.contentEditable = false;
                        self.input = function() {
                            return $(self.urlSelectNode).find('input')[0];
                        };
                        self.editor = activeEditor;
                        self.body = self.editor.dom.doc.body;
                        self.imageRef = null; // current photo-img element
                        self.isInDom = false; // self.urlSelectNode is attached to dom

                        self.debugEvent = function(e) {
                            console.log(e);
                            return false;
                        }

                        self.setResizable = function() {
                            if (!tinymce.isIE) {
//                            {
                                self.imageRef.parentNode.insertBefore(self.imgWrapper, self.imageRef);
                                var matches = self.imageRef.className.match(/(mce-photo-float-\w+)/);
                                var floatCls = '';
                                if(matches){
                                    floatCls = matches[0];
                                }
                                self.imgWrapper.className = 'mce-photo-resize-wrapper ' + floatCls;
                                self.imgWrapper.style.width = self.body.offsetWidth + 'px';
                                self.imgWrapper.style.height = self.body.offsetHeight + 'px';

                                self.imgWrapper.insertBefore(self.imageRef, self.imgWrapperHandleE);
                                self.setWrapperSize();
                            }
                        };

                        self.unsetResizable = function() {
                            if (!tinymce.isIE) {
//                                {
                                try {
                                    self.imageRef = self.imgWrapper.removeChild(self.imageRef);
                                    self.imgWrapper.parentNode.insertBefore(self.imageRef, self.imgWrapper);
                                    self.imgWrapper.parentNode.removeChild(self.imgWrapper);
                                } catch(e) {
                                    console.log(e);
                                }
                            }
//                            self.imageRef = null;
                        };

                        self.attachNode = function() {
                            self.body.appendChild(self.urlSelectNode);
                            self.body.appendChild(self.alignNode);
                            self.body.appendChild(self.closeNode);

                            self.isInDom = true;
                            self.body.contentEditable = false;
                            self.editor.contentEditable = false;
                            self.input().contentEditable = true;
                        };
                        self.detachNode = function() {
                            var parent = self.urlSelectNode.parentNode;
                            try {
                                self.urlSelectNode = parent.removeChild(self.urlSelectNode);
                                self.closeNode = parent.removeChild(self.closeNode);
                                self.alignNode = parent.removeChild(self.alignNode);

                            } catch(e) {
                                console.log(e);
                            }
                            self.isInDom = false;
                            self.body.contentEditable = true;
                        };
                        self.submitImgUrl = function() {
                            var src = self.input().value;
                            self.imageRef.src = src;
                            try {  // for drag'n'drop todo check in IE 9+ maybe time to change dataset.x -> $().data('x')?
                                self.imageRef.dataset.mceSrc = src;
                            } catch(e) { // IE quirk
                                self.imageRef.setAttribute('data-mce-src', src);
                            }
//                            self.imageRef.style.width = 250 + 'px';
                            self.detachNode();
                            self.unsetResizable();
                        }
                        self.isMenuNode = function(node, elementClassName) {
                            if (elementClassName === undefined) {
                                var ret = false;
                                for (var key in self.clsNames) {
                                    ret = ret || self.isMenuNode(node, self.clsNames[key]);
                                }
                                return ret;
                            }
                            var lcClsName = elementClassName.toLowerCase();  // lc for webkit
                            while (node !== self.body && node !== null) {
                                if (self.editor.dom.hasClass(node, lcClsName)) {
                                    return true;
                                }
                                node = node.parentNode;
                            }
                            return false;
                        };
                        self.onClick = function(ed, e) {
                            var target = e.target;
                            if (self.isInDom) {
                                if (self.isMenuNode(target, self.clsName)) {
                                    if (target.tagName == 'INPUT') {
                                        self.input().focus();
                                        target = target.parentNode;
                                    } else if (target.tagName == 'BUTTON' && self.editor.dom.hasClass(target, 'submit')) {
                                        self.submitImgUrl();
                                    } else if (target.tagName == 'BUTTON' && self.editor.dom.hasClass(target, 'clear')) {
                                        self.input().value = '';
                                    }
                                    $(target).find('input').focus();
                                    return false;
                                }
                                if (self.isMenuNode(target, self.closeClsName)) {
                                    self.detachNode();
                                    self.unsetResizable();
                                    var imgParent = self.imageRef.parentNode;
                                    imgParent.removeChild(self.imageRef);

                                    return false;
                                }
                                if (self.isMenuNode(target, self.alignClsName)) {
                                    var floatClass = self.imageRef.className;
                                    floatClass = floatClass.replace(/\smce-photo-float-\w+/, '');
                                    if (!self.imageRef) {
                                        console.log(self);
                                        return false;
                                    }
                                    if (self.editor.dom.hasClass(target, 'right')) {
                                        if (tinymce.isIE)
                                            self.imageRef.style.float = 'right';
                                        floatClass += ' mce-photo-float-right';
                                    }
                                    if (self.editor.dom.hasClass(target, 'left')) {
                                        if (tinymce.isIE)
                                            self.imageRef.style.float = 'left';
                                        floatClass += ' mce-photo-float-left';
                                    }
                                    if (self.editor.dom.hasClass(target, 'block')) {
                                        if (tinymce.isIE) {
                                            self.imageRef.style.float = 'none';
                                            self.imageRef.style.display = 'block';
                                        }
                                        floatClass += ' mce-photo-float-block';
                                    }
                                    if (self.editor.dom.hasClass(target, 'inline')) {
                                        if (tinymce.isIE) {
                                            self.imageRef.style.float = 'none';
                                            self.imageRef.style.display = 'inline';
                                        }
                                        floatClass += ' mce-photo-float-inline';
                                    }
                                    self.imageRef.className = floatClass;
                                }
                                self.detachNode();
                                self.unsetResizable();
                            }
                            if (target.nodeName === 'IMG' && self.editor.dom.hasClass(target, self.clsNames.img)) {
                                var oftop = target.offsetTop;
                                var ofheight = target.offsetHeight;
                                var ofleft = target.offsetLeft;
                                var ofwidth = target.offsetWidth;
                                self.imageRef = target;

                                self.attachNode();
//                                if (!self.imageRef.className.match(/mce-photo-float/))
                                    self.setResizable();

                                self.input().value = target.src;

//                            musicContextMenu.style.top = (e.pageY || e.y) + 'px';
//                            musicContextMenu.style.left = (e.pageX || e.x) + 'px';
                                self.urlSelectNode.style.top = oftop + ofheight + 'px';
                                self.urlSelectNode.style.left = ofleft + 'px';
                                self.urlSelectNode.style.width = ofwidth + 'px';

                                self.closeNode.style.top = oftop + 'px';
                                self.closeNode.style.left = ofleft + ofwidth - 24 + 'px';

                                self.alignNode.style.top = oftop + 'px';
                                self.alignNode.style.left = ofleft + 'px';


                                ed.selection.select(target);
                                self.input().focus();
                                e.stopPropagation();
                                return false;
                            }
                        };

                        self.disableControls = function(ed, cm, n) {
//
                            var isTrue = self.isMenuNode(n);
                            for (var key in cm.controls)
                                cm.controls[key].setDisabled(isTrue);
                        };

                        // Enter on input submits url
                        self.onInputEnter = function(ed, e) {
                            if (e.keyCode == 13 && e.target === self.input()) {
                                self.submitImgUrl();
                                e.stopPropagation();
                                e.preventDefault();
                                return false;
                            }
                            if (e.keyCode == 86 && e.ctrlKey) {
                                if (e.target == self.input()) {
                                    return false;
                                }
                            }


                        }
                        activeEditor.onClick.add(self.onClick);
                        activeEditor.onKeyDown.add(self.onInputEnter);
                        activeEditor.onNodeChange.add(self.disableControls);
                        activeEditor.onExecCommand.add(function() {
                            if (self.isInDom && !self.imageRef) {
                                self.detachNode();
                                self.unsetResizable();
                            }
                        });
                    }

                    // Register the command so that it can be invoked by using tinyMCE.activeEditor.execCommand('mceExample');
                    var photoClsName = 'mce-photo';
                    var musicImgHtml = tinymce.DOM.createHTML('img', {src: 'PLACEHOLDER', 'class': photoClsName + ' mce-photo-float-inline'/*+ ' mceItemNoResize', 'style': 'width: 250px'*/});
                    var sep = '<photo/>';
                    var pbRE = new RegExp(sep.replace(/[\?\.\*\[\]\(\)\{\}\+\^\$\:]/g, function(a) {
                        return '\\' + a;
                    }), 'g');

//                    var photoContextMenuInstance;


                    ed.addCommand('mcePhoto', function() {
                        ed.windowManager.open({
                                    file : url + '/dialog.htm',
                                    width : 320 + parseInt(ed.getLang('music.delta_width', 0)),
                                    height : 120 + parseInt(ed.getLang('music.delta_height', 0)),
                                    inline : 1
                                }, {
                                    plugin_url : url, // Plugin absolute URL
                                    example_url : 'images/temp/mosaic-2.jpg', // Custom argument
                                    html: musicImgHtml
                                });
                        if (ed.dom.loadCSS) {
                            ed.dom.loadCSS('css/photo.css');
                        }



                        var photoContextMenuInstance = new PhotoContextMenu(ed, photoClsName);

                    });

                    // Register example button
                    ed.addButton('photo', {
                                title : 'photo.desc',
                                cmd : 'mcePhoto'
//                                image : url + '/img/icon-photo.png'
                            });

                    // Add a node change handler, selects the button in the UI when a image is selected
                    ed.onNodeChange.add(function(ed, cm, n) {
                        // disable all controls
                        var isTrue = n.nodeName === 'IMG' && ed.dom.hasClass(n, photoClsName);
                        for (var key in cm.controls)
                            cm.controls[key].setDisabled(isTrue);
                    });

//                    ed.onNodeChange.add(function(ed, cm, n) {
//                        cm.setActive('photo', n.nodeName == 'IMG');
//                    });
//
                    ed.onBeforeSetContent.add(function(ed, o) {
                        o.content = o.content.replace(pbRE, musicImgHtml);
                    });

                    ed.onPostProcess.add(function(ed, o) {
                        if (o.get)
                            o.content = o.content.replace(/<img[^>]+>/g, function(im) {
                                if (im.indexOf('class="' + photoClsName) !== -1)
                                    im = sep;

                                return im;
                            });
                    });

                },


                /**
                 * Creates control instances based in the incomming name. This method is normally not
                 * needed since the addButton method of the tinymce.Editor class is a more easy way of adding buttons
                 * but you sometimes need to create more complex controls like listboxes, split buttons etc then this
                 * method can be used to create those.
                 *
                 * @param {String} n Name of the control to create.
                 * @param {tinymce.ControlManager} cm Control manager to use inorder to create new control.
                 * @return {tinymce.ui.Control} New control instance or null if no control was created.
                 */
                //		createControl : function(n, cm) {
                //			return null;
                //		},

                /**
                 * Returns information about the plugin as a name/value array.
                 * The current keys are longname, author, authorurl, infourl and version.
                 *
                 * @return {Object} Name/value array containing information about the plugin.
                 */
                getInfo : function() {
                    return {
                        longname : 'Photo plugin',
                        author : 'Some author',
                        authorurl : 'http://tinymce.moxiecode.com',
                        infourl : 'http://wiki.moxiecode.com/index.php/TinyMCE:Plugins/music',
                        version : "1.0"
                    };
                }
            });

    // Register plugin
    tinymce.PluginManager.add('photo', tinymce.plugins.PhotoPlugin);
})();
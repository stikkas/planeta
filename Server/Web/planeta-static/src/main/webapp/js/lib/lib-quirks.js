
if (typeof String.prototype.trim !== 'function') {
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, '');
    };
}

/** Tinyscrollbar and mousewheel plugins can conflict.
 *  When tinyscrollbar loads after mousewheel (in javascript.properties)
 *  mousewheel plugin loses deltaX and deltaY args in
 *  thumbBlock.bind('mousewheel', function(event, delta, deltaX, deltaY) {
 *
 *  Otherwise tinyscrollbar scroll speed becomes very small
 */

/**
 * Small replacement of json2.js for supported browsers
 */
var JSON;
if (!JSON) {
    JSON = {};
}

if (!JSON.parse) {
    JSON.parse = $.parseJSON;
}


(function () {

    tinymce.create('tinymce.plugins.ImgAlignPlugin', {

        init: function (ed, url) {
            ed.onPreInit.add(function (ed) {
                ed.dom.loadCSS(url + '/align.css');
            });


        },

        Menu: function (activeEditor, imgClsName, parentNode) {
            var self = this;
            self.imgClsName = imgClsName.toLowerCase();

            self.clsNames = {
                img: self.imgClsName,
                close: self.imgClsName + '-context-close',
                align: self.imgClsName + '-context-align'
            };
            self.padding = 5;
            self.closeNode = tinymce.DOM.create('div', {'class': self.clsNames.close + ' mceItemNoResize'},
            '<i class="icon-close icon-gray"></i>'
            );
            self.alignSelectHtml = '<span class="left"><i class="icon-ww-ileft icon-gray"></i></span>' +
            '<span class="none"><i class="icon-ww-inone icon-gray"></i></span>' +
            '<span class="right"><i class="icon-ww-iright icon-gray"></i></span>';
//                        if(!tinymce.isIE)
//                            self.alignSelectHtml += '<button class="resize">resize</button>';
            self.alignNode = tinymce.DOM.create('div', {'class': self.clsNames.align + ' mceItemNoResize'},
            self.alignSelectHtml);

            var onCloseNodeClick = function(e) {
                self.detachNode();
                self.onImgDelete(e);
                self.imageRef.detach();
                self.editor.undoManager.add();
                tinymce.dom.Event.cancel(e);
            };

            tinymce.dom.Event.add(self.closeNode, 'click', onCloseNodeClick);

            tinymce.dom.Event.add(self.alignNode, 'click', function (e) {
                var clickName = e.target.className;
                var aligns = ['left', 'right', 'none'];
//                self.imageRef.removeClass('mce-img-left', 'mce-img-right', 'mce-img-center');
                $(aligns).each(function (i, el) {
                    var alignClassName = 'mce-img-' + el;
                    self.imageRef.removeClass(alignClassName);
                    if (clickName.match(el)) {
                        self.imageRef.css('float', el);
                        self.imageRef.addClass(alignClassName);
                        self.imageRef.attr('align', el);
                        self.detachNode();
                        self.editor.undoManager.add();
                    }
                });
            });

            self.closeNode.contentEditable = false;
            self.alignNode.contentEditable = false;
            self.editor = activeEditor;
            self.body = self.editor.dom.doc.body;
            self.parentNode = parentNode || self.body;
            self.imageRef = null; // current photo-img element
            self.isInDom = function () {
                return $(self.body).find('.' + self.clsNames.close).length;
            };

            self.attachNode = attachMenu;
            self.detachNode = detachMenu;
            self.isMenuNode = function (node, elementClassName) {
                if (elementClassName === undefined) {
                    var ret = false;
                    _.each(self.clsNames, function (value) {
                        ret = ret || self.isMenuNode(node, value);
                    });
                    return ret;
                }
                while (node !== self.body && node !== null) {
                    if (self.editor.dom.hasClass(node, elementClassName)) {
                        return true;
                    }
                    node = node.parentNode;
                }
                return false;
            };
            self.onClick = function (ed, e) {
                var target = e.target;

                if (self.isInDom() && (
                target.className == null
                ||
                !target.className.match('mce-resize')
                ) &&
                (!target.className.match(self.clsNames.img) || (self.imageRef != $(target)))) {
                    self.detachNode();
                }
                if (!self.isInDom() && target.nodeName === 'IMG' && self.editor.dom.hasClass(target, self.clsNames.img)) {
                    ed.selection.collapse();
                    self.imageRef = $(target);
                    self.attachNode();
                    setMenuSize();
                    tinymce.dom.Event.cancel(e);
                    return false; // webkit quirk (to cancel selection)
                }

            };

            self.editor.onBeforeExecCommand.add(function () {
                if (self.isInDom()) {
                    self.detachNode();
                }
            });

            function setMenuSize() {
                self.closeNode.style.top = self.imageRef.offset().top + self.padding + 'px';
                self.closeNode.style.left = self.imageRef.offset().left + self.imageRef.width() - self.padding - 16 + 'px';
                self.alignNode.style.top = self.imageRef.offset().top + self.padding + 'px';
                self.alignNode.style.left = self.imageRef.offset().left + self.padding + 'px';
            }

            function attachMenu() {
                self.onAttach();
                $(self.imageRef).attr('unselectable', 'on');
                self.parentNode.appendChild(self.alignNode);
                self.parentNode.appendChild(self.closeNode);
                self.body.contentEditable = false;
                self.editor.contentEditable = false;
                $(self.imageRef).bind('mousedown', detachMenu);
                if (tinymce.isIE) {
                    $(self.imageRef).bind('mousedown', function () {
                        $(self.imageRef).attr('unselectable', 'off');
                        setTimeout(function () {
                            $('.' + self.clsNames.img).attr('unselectable', 'on');
                        }, 1000);
                    });
                }
//                tinymce.dom.Event.add(self.imageRef, 'mousedown', detachMenu);
            }

            function detachMenu() {
                self.onDetach();
                var parent = self.closeNode.parentNode;
                try {
                    self.closeNode = parent.removeChild(self.closeNode);
                    self.alignNode = parent.removeChild(self.alignNode);

                } catch (e) {
//                    console.log(e);
                }
//                self.isInDom = false;
                self.body.contentEditable = true;
                if (tinymce.isIE) {
                    $(self.imageRef).unbind('mousedown');
                }
                $(self.imageRef).unbind('mousedown');     // return mousedown
//                $(self.imageRef).attr('unselectable', 'on');
            }

            function destroy() {
                if (self.isInDom())
                    self.detachNode();
            }

            self.destroy = destroy;
            self.setMenuSize = setMenuSize;

            // Enter on input submits url
            self.onKeyPressed = function (ed, e) {
                if((e.keyCode == tinymce.VK.DELETE || e.keyCode == tinymce.VK.BACKSPACE ) && self.isInDom()){
                    onCloseNodeClick(e);
                }
            };

            activeEditor.onClick.add(self.onClick);
            activeEditor.onKeyDown.add(self.onKeyPressed);
            $(document.body).bind('click', self.destroy);
            activeEditor.onRemove.add(self.destroy);
            activeEditor.onBeforeGetContent.add(self.destroy);

        }
    });

    tinymce.PluginManager.add('imgalign', tinymce.plugins.ImgAlignPlugin);

    tinymce.plugins.ImgAlignPlugin.Menu = tinymce.plugins.ImgAlignPlugin.prototype.Menu;
})();


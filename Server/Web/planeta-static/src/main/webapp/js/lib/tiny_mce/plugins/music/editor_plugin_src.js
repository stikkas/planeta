/**
 * editor_plugin_src.js
 *
 * Copyright 2009, Moxiecode Systems AB
 * Released under LGPL License.
 *
 * License: http://tinymce.moxiecode.com/license
 * Contributing: http://tinymce.moxiecode.com/contributing
 */

(function() {
    // Load plugin specific language pack
    tinymce.PluginManager.requireLangPack('music');
    var rootAttributes = tinymce.explode('id,name,width,height,style,align,class,hspace,vspace,bgcolor,type');
    var clsName = 'mceMusic';
    tinymce.create('tinymce.plugins.MusicPlugin', {

                /**
                 * Initializes the plugin, this will be executed after the plugin has been created.
                 * This call is done before the editor instance has finished it's initialization so use the onInit event
                 * of the editor instance to intercept that event.
                 *
                 * @param {tinymce.Editor} ed Editor instance that the plugin is initialized in.
                 * @param {string} url Absolute URL to where the plugin is located.
                 */
                init : function(ed, url) {
                    var self = this;
                    self.JSON = tinymce.util.JSON;
                    self.editor = ed;
			        self.url = url;
                    self.avatarUrl = 'images/planeta/track-play.png';
                    self.html = '';


                    // Register the command so that it can be invoked by using tinyMCE.activeEditor.execCommand('mceExample');

                    self.clsName = clsName;
                    var contextMenuClass = 'mceMusicContextMenu';
                    var musicImgHtml = tinymce.DOM.createHTML('img', {src: url + 'PLACEHOLDER', 'class': clsName + ' mceItemNoResize'});
                    var sep = '<music/>';
                    var pbRE = new RegExp(sep.replace(/[\?\.\*\[\]\(\)\{\}\+\^\$\:]/g, function(a) {
                        return '\\' + a;
                    }), 'g');


                    var musicContextMenu = tinymce.DOM.create('div', {'class': contextMenuClass + ' mceItemNoResize'},
                            '<div><span>Вывести html</span></div>');


                    ed.addCommand('mceMusic', function() {
                        ed.windowManager.open({
                            file : url + '/dialog.htm',
                            width : 320 + parseInt(ed.getLang('music.delta_width', 0)),
                            height : 120 + parseInt(ed.getLang('music.delta_height', 0)),
                            inline : 1
                        }, {
                            plugin_url : url, // Plugin absolute URL
                            html: self.html
                        });
                        if (ed.dom.loadCSS) {
                            ed.dom.loadCSS('css/music.css');
                        }
                        ed.dom.doc.body.appendChild(musicContextMenu);
                    });

                    // Register example button
                    ed.addButton('music', {
                                title : 'music.desc',
                                cmd : clsName
//                                image : url + '/img/icon-audio.png'
                            });

                     function musicClick(ed, e) {
                        function isContextMenu(node) {
                           while (node !== ed.dom.doc.body && node !== null) {
                                if (ed.dom.hasClass(node, 'mceMusicContextMenu')) {
                                    return true;
                                }
                                node = node.parentNode;
                            }
                            return false;
                        }

                        var target = e.target;
                        if (isContextMenu(target)) {
                            if (target.tagName == 'SPAN') {
                                var url = musicContextMenu.imageRef.src;
//                                alert(self.dataToUrl(self.urlToData(url)));
                                alert(self.dataToHtml(self.urlToData(url)));
                            }

                            e.stopPropagation();
                        }
                        if (musicContextMenu.style.display != 'none') {
                            musicContextMenu.style.display = 'none';
                        }
                        if (target.nodeName === 'IMG' && ed.dom.hasClass(target, clsName)) {
                            musicContextMenu.style.display = 'block';
                            musicContextMenu.imageRef = target;
                            // webkit quirks
                            if(tinymce.isWebKit){
                                musicContextMenu.style.position = 'absolute';
                                musicContextMenu.style.background = '-webkit-gradient(linear,left top,left bottom,from(#E2EBFC),to(#C5D7F8))';
                                musicContextMenu.style.padding = '10px';
                            }
                            // do not forget px for webkit
//                            musicContextMenu.style.top = (e.pageY || e.y) + 'px';
//                            musicContextMenu.style.left = (e.pageX || e.x) + 'px';
                            musicContextMenu.style.top = target.offsetTop + target.offsetHeight + 'px';
                            musicContextMenu.style.left = target.offsetLeft + 'px';
                            ed.selection.select(target);
                            e.stopPropagation();
                            return false;


                        }
                    }


                    self.musicClick = musicClick;
                    //ed.onClick.add(self.musicClick);

                    ed.onNodeChange.add(function(ed, cm, n) {
                        cm.setActive('music', n.nodeName === 'IMG' && ed.dom.hasClass(n, clsName));
                        var elem =  $(tinymce.DOM.doc.body).find('#staticPage')[0];
                        elem.innerHTML = self.mapping.domToHtml(this.dom.doc.body);
                        if( n.nodeName === 'IMG' && ed.dom.hasClass(n, clsName))
                            self.html = self.mapping.imgToHtml(n, true);
                    });

                    ed.onBeforeSetContent.add(function(ed, o) {
//                        o.content = o.content.replace(pbRE, musicImgHtml);
                    });

                    ed.onPostProcess.add(function(ed, o) {
                        if (o.get)
                            o.content = o.content.replace(/<img[^>]+>/g, function(im) {
                                if (im.indexOf('class="' + clsName) !== -1)
                                    im = sep;

                                return im;
                            });
                    });

                    self.mapping = new tinymce.plugins.DivToImgPlugin.Mapping(ed, clsName);
                    self.mapping.attrs = {
                        title : {required : true, empty : ' '},
                        time : {required: true, empty: ' '},
                        author: {required: false, empty: ' '},
                        authorUrl: {required: false, empty: 'javascript: void();'},
                        playUrl: {required: false, empty: 'javascript: void();'}
                    };
                    // self.mapping.Node docs are here: http://www.tinymce.com/wiki.php/API3:class.tinymce.html.Node
                    self.mapping.objectToJSON = function(node) {
                        if(!node.attr('class').match(/(\s|^)track-list(\s|$)/))
                            return null; // null means node under consideration is not object
                        var data = {};
                        var texts = node.getAll('#text');
                        tinymce.each(texts, function(txt) {
                            if(txt.parent.attr('class') == 'track-list-item-name-title')
                                data.title = txt.value;
                            if(txt.parent.attr('class') == 'track-list-item-option-time')
                                data.time = txt.value;
                        });
                        var links = node.getAll('a');
                        tinymce.each(links, function(a) {
                            if(a.parent.attr('class') == 'track-list-item-name-author'){
                                data.authorUrl = a.attr('href');
                                if(a.getAll('#text')[0]){
                                    data.author = a.getAll('#text')[0].value;
                                }
                            }
                            if(a.parent.attr('class') == 'track-list-item-count')
                                data.playUrl = a.attr('href');
                        });

                        return data;
                    };
                    // it looks like this file doesn't use in projects at all, remove?
                    self.mapping.jsonToObject = function(data) {
                        var Node = self.mapping.Node;
                        var container = Node.create('div');
                        TemplateManager.tmpl("#tiny-mce-audio-stub", data).done(function ($tmpl) {
                            container.append((new tinymce.html.DomParser).parse($tmpl));
                        });
                        return container;
                    };
                    self.mapping.generateImgSrc = function(json) {
                        var url = self.avatarUrl;
                        var query = [];
                        for (var k in json){
                            query.push(k + '=' + json[k]);
                        }
                        url += '?'+query.join('&');
                        return url;
                    };
                },

                getInfo : function() {
                    return {
                        longname : 'Music plugin',
                        author : 'Some author',
                        authorurl : 'http://tinymce.moxiecode.com',
                        infourl : 'http://wiki.moxiecode.com/index.php/TinyMCE:Plugins/music',
                        version : "1.0"
                    };
                }
            });

    // Register plugin
    tinymce.PluginManager.add('music', tinymce.plugins.MusicPlugin, ['divtoimgmapping']);
})();
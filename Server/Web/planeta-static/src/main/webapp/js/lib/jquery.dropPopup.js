(function( $ ){

    var defaults = {
        activeClass: 'active',
        bottomUp: false
    };

    var methods = {
        init: function( options ) {
            options = $.extend(true, {}, defaults, options);

            return this.each(function() {
                var data = $(this).data('dropPopup');
                if (data) return this;

                $(this).data('dropPopup', true);
                methods.init_el($(this), options);
            });
        },
        init_el: function($elem, options) {
            $elem.data('options', options);
            var $trigger = $(options.trigger, $elem);
            var $popup = $(options.popup, $elem);
            var $closeTrigger = $(options.closeTrigger, $elem);

            if ( $popup.length ) {

                $trigger.click(function(e) {
                    e.preventDefault();

                    if ( $elem.hasClass(options.activeClass) ) {
                        closePopup($elem, options);
                    } else {
                        openPopup($elem, options);
                    }

                });

                $closeTrigger.click(function () {
                    closePopup($elem, options);
                });

                $elem.find('.dropdown-menu .item').click(function (e) {
                    var $elem = $(e.currentTarget);
                    $elem.closest('.dropdown-menu').find('.active').removeClass('active');
                    $elem.addClass('active');

                    closePopup($elem, options);
                })
            }
        },
        open: function () {
            openPopup($(this), $(this).data('options'));
        },
        close: function () {
            closePopup($(this), $(this).data('options'));
        }
    };

    var openPopup = function ($elem, options) {
        var $trigger = $(options.trigger, $elem);
        var $popup = $(options.popup, $elem);

        if ( $trigger.hasClass('btn') )
            $trigger.addClass('active');
        $elem.addClass(options.activeClass);
        if ( options.open ) options.open({elem: $elem});

        if ( options.bottomUp ) {
            $elem.removeClass('bottom-up');
            var popupBottom = $popup.offset().top + $popup.outerHeight(true);
            var winBottom = $(window).scrollTop() + $(window).height();
            if ( popupBottom > winBottom ) {
                $elem.addClass('bottom-up');
            }
        }

        $(document).bind('click.dropPopup', $elem, function(e) {
            if (isEventOut($('>*', $elem), e)) {
                closePopup($elem, options);
            }
        });
    };

    var closePopup = function ($elem, options) {
        var $trigger = $(options.trigger, $elem);

        if ( $trigger.hasClass('btn') )
            $trigger.removeClass('active');
        $elem.removeClass(options.activeClass);
        if ( options.close ) options.close({elem: $elem});

        $(document).unbind('click.dropPopup', $elem);
    };

    $.fn.dropPopup = function( method ) {

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on jQuery.dropPopup' );
        }

    };

    function isEventOut(blocks, e) {
        var r = true;
        $(blocks).each(function () {
            if (!r) return;
            if ($(e.target).closest('HTML', $(this).get(0)).length == 0) {
                r = false;
                return;
            }
            if ($(e.target).get(0) == $(this).get(0)) {
                r = false;
                return;
            }
        });
        return r;
    }

})( jQuery );

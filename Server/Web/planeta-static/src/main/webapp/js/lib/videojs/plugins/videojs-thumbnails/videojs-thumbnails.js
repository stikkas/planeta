(function (f) {
    if (typeof exports === "object" && typeof module !== "undefined") {
        module.exports = f()
    } else if (typeof define === "function" && define.amd) {
        define([], f)
    } else {
        var g;
        if (typeof window !== "undefined") {
            g = window
        } else if (typeof global !== "undefined") {
            g = global
        } else if (typeof self !== "undefined") {
            g = self
        } else {
            g = this
        }
        g.videojsThumbnails = f()
    }
})(function () {
    var define, module, exports;
    return (function e(t, n, r) {
        function s(o, u) {
            if (!n[o]) {
                if (!t[o]) {
                    var a = typeof require == "function" && require;
                    if (!u && a)return a(o, !0);
                    if (i)return i(o, !0);
                    var f = new Error("Cannot find module '" + o + "'");
                    throw f.code = "MODULE_NOT_FOUND", f
                }
                var l = n[o] = {exports: {}};
                t[o][0].call(l.exports, function (e) {
                    var n = t[o][1][e];
                    return s(n ? n : e)
                }, l, l.exports, e, t, n, r)
            }
            return n[o].exports
        }

        var i = typeof require == "function" && require;
        for (var o = 0; o < r.length; o++)s(r[o]);
        return s
    })({
        1: [function (require, module, exports) {
            (function (global) {
                if (typeof window !== "undefined") {
                    module.exports = window;
                } else if (typeof global !== "undefined") {
                    module.exports = global;
                } else if (typeof self !== "undefined") {
                    module.exports = self;
                } else {
                    module.exports = {};
                }

            }).call(this, typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

        }, {}], 2: [function (require, module, exports) {
            'use strict';

            var _createClass = function () {
                function defineProperties(target, props) {
                    for (var i = 0; i < props.length; i++) {
                        var descriptor = props[i];
                        descriptor.enumerable = descriptor.enumerable || false;
                        descriptor.configurable = true;
                        if ("value" in descriptor) descriptor.writable = true;
                        Object.defineProperty(target, descriptor.key, descriptor);
                    }
                }

                return function (Constructor, protoProps, staticProps) {
                    if (protoProps) defineProperties(Constructor.prototype, protoProps);
                    if (staticProps) defineProperties(Constructor, staticProps);
                    return Constructor;
                };
            }();

            var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
                return typeof obj;
            } : function (obj) {
                return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
            };

            function _classCallCheck(instance, Constructor) {
                if (!(instance instanceof Constructor)) {
                    throw new TypeError("Cannot call a class as a function");
                }
            }

            function _interopDefault(ex) {
                return ex && (typeof ex === 'undefined' ? 'undefined' : _typeof(ex)) === 'object' && 'default' in ex ? ex['default'] : ex;
            }

            var videojs = _interopDefault((typeof window !== "undefined" ? window['videojs'] : typeof global !== "undefined" ? global['videojs'] : null));
            var global = require(1);

            var ThumbnailHelpers = function () {
                function ThumbnailHelpers() {
                    _classCallCheck(this, ThumbnailHelpers);
                }

                _createClass(ThumbnailHelpers, null, [{
                    key: 'hidePlayerOnHoverTime',
                    value: function hidePlayerOnHoverTime(progressControl) {
                        var mouseTime = progressControl.el_.getElementsByClassName('vjs-mouse-display')[0];

                        mouseTime.style.display = 'none';
                    }
                }, {
                    key: 'createThumbnails',
                    value: function createThumbnails() {
                        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
                            args[_key] = arguments[_key];
                        }

                        var thumbnailClip = args.shift() || {};

                        Object.keys(args).map(function (i) {
                            var singleThumbnail = args[i];

                            Object.keys(singleThumbnail).map(function (property) {
                                if (singleThumbnail.hasOwnProperty(property)) {
                                    if (_typeof(singleThumbnail[property]) === 'object') {
                                        thumbnailClip[property] = ThumbnailHelpers.createThumbnails(thumbnailClip[property], singleThumbnail[property]);
                                    } else {
                                        thumbnailClip[property] = singleThumbnail[property];
                                    }
                                }
                                return thumbnailClip;
                            });
                            return thumbnailClip;
                        });
                        return thumbnailClip;
                    }
                }, {
                    key: 'getComputedStyle',
                    value: function getComputedStyle(thumbnailContent, pseudo) {
                        return function (prop) {
                            if (global.window.getComputedStyle) {
                                return global.window.getComputedStyle(thumbnailContent, pseudo)[prop];
                            }
                            return thumbnailContent.currentStyle[prop];
                        };
                    }
                }, {
                    key: 'getVisibleWidth',
                    value: function getVisibleWidth(thumbnailContent, width) {
                        if (width) {
                            return parseFloat(width);
                        }

                        var clip = ThumbnailHelpers.getComputedStyle(thumbnailContent)('clip');

                        if (clip !== 'auto' && clip !== 'inherit') {
                            clip = clip.split(/(?:\(|\))/)[1].split(/(?:,| )/);
                            if (clip.length === 4) {
                                return parseFloat(clip[1]) - parseFloat(clip[3]);
                            }
                        }
                        return 0;
                    }
                }, {
                    key: 'getScrollOffset',
                    value: function getScrollOffset() {
                        if (global.window.pageXOffset) {
                            return {
                                x: global.window.pageXOffset,
                                y: global.window.pageYOffset
                            };
                        }
                        return {
                            x: global.document.documentElement.scrollLeft,
                            y: global.document.documentElement.scrollTop
                        };
                    }
                }, {
                    key: 'suportAndroidEvents',
                    value: function suportAndroidEvents(player) {
                        // Android doesn't support :active and :hover on non-anchor and non-button elements
                        // so, we need to fake the :active selector for thumbnails to show up.
                        var progressControl = player.controlBar.progressControl;

                        var addFakeActive = function addFakeActive() {
                            progressControl.addClass('fake-active');
                        };

                        var removeFakeActive = function removeFakeActive() {
                            progressControl.removeClass('fake-active');
                        };

                        progressControl.on('touchstart', addFakeActive);
                        progressControl.on('touchend', removeFakeActive);
                        progressControl.on('touchcancel', removeFakeActive);
                    }
                }, {
                    key: 'createThumbnaislHolder',
                    value: function createThumbnaislHolder() {
                        var wrap = global.document.createElement('div');

                        wrap.className = 'vjs-thumbnail-holder';
                        return wrap;
                    }
                }, {
                    key: 'createThumbnailImgWrap',
                    value: function createThumbnailImgWrap() {
                        var thumbnailImgWrap = global.document.createElement('div');
                        thumbnailImgWrap.className = 'vjs-thumbnail-img-wrap';
                        return thumbnailImgWrap;
                    }
                }, {
                    key: 'createThumbnailImg',
                    value: function createThumbnailImg(thumbnailClips) {
                        var thumbnailImg = global.document.createElement('div');

                        thumbnailImg.src = thumbnailClips['0'].src;
                        thumbnailImg.style.backgroundImage = 'url(' + thumbnailClips['0'].src + ')';

                        thumbnailImg.className = 'vjs-thumbnail-img';
                        return thumbnailImg;
                    }
                }, {
                    key: 'createThumbnailTime',
                    value: function createThumbnailTime() {
                        var time = global.document.createElement('div');

                        time.className = 'vjs-thumbnail-time';
                        time.id = 'vjs-time';
                        return time;
                    }
                }, {
                    key: 'createThumbnailArrowDown',
                    value: function createThumbnailArrowDown() {
                        var arrow = global.document.createElement('div');

                        arrow.className = 'vjs-thumbnail-arrow';
                        arrow.id = 'vjs-arrow';
                        return arrow;
                    }
                }, {
                    key: 'mergeThumbnailElements',
                    value: function mergeThumbnailElements(thumbnailsHolder, thumbnailImg, timelineTime, thumbnailArrowDown, thumbnailImgWrap) {

                        thumbnailsHolder.appendChild(thumbnailImgWrap);
                        thumbnailImgWrap.appendChild(thumbnailImg);
                        thumbnailsHolder.appendChild(timelineTime);
                        thumbnailsHolder.appendChild(thumbnailArrowDown);
                        return thumbnailsHolder;
                    }
                }, {
                    key: 'centerThumbnailOverCursor',
                    value: function centerThumbnailOverCursor(thumbnailImg) {
                        // center the thumbnail over the cursor if an offset wasn't provided
                        if (!thumbnailImg.style.left && !thumbnailImg.style.right) {
                            thumbnailImg.onload = function () {
                                var thumbnailWidth = {width: -(thumbnailImg.naturalWidth / 2)};

                                thumbnailImg.style.left = thumbnailWidth + 'px';
                            };
                        }
                    }
                }, {
                    key: 'getVideoDuration',
                    value: function getVideoDuration(player) {
                        var duration = player.duration();

                        player.on('durationchange', function () {
                            duration = player.duration();
                        });
                        return duration;
                    }
                }, {
                    key: 'addThumbnailToPlayer',
                    value: function addThumbnailToPlayer(progressControl, thumbnailsHolder) {
                        progressControl.seekBar.el().appendChild(thumbnailsHolder);
                    }
                }, {
                    key: 'findMouseLeftOffset',
                    value: function findMouseLeftOffset(pageMousePositionX, progressControl, pageXOffset, event) {
                        // find the page offset of the mouse
                        var leftOffset = pageMousePositionX || event.clientX + global.document.body.scrollLeft + global.document.documentElement.scrollLeft;

                        // subtract the page offset of the positioned offset parent
                        leftOffset -= progressControl.seekBar.el_.getBoundingClientRect().left + pageXOffset;
                        return leftOffset;
                    }
                }, {
                    key: 'getMouseVideoTime',
                    value: function getMouseVideoTime(mouseLeftOffset, progressControl, duration) {
                        // return Math.floor((mouseLeftOffset - progressControl.el().offsetLeft) / progressControl.width() * duration);
                        return Math.floor((mouseLeftOffset) / progressControl.seekBar.width() * duration);
                    }
                }, {
                    key: 'updateThumbnailTime',
                    value: function updateThumbnailTime(timelineTime, progressControl) {
                        timelineTime.innerHTML = progressControl.seekBar.mouseTimeDisplay.timeTooltip.el_.innerText;
                    }
                }, {
                    key: 'getPageMousePositionX',
                    value: function getPageMousePositionX(event) {
                        var pageMouseOffsetX = event.pageX;

                        if (event.changedTouches) {
                            pageMouseOffsetX = event.changedTouches[0].pageX;
                        }
                        return pageMouseOffsetX;
                    }
                }, {
                    key: 'keepThumbnailInsidePlayer',
                    value: function keepThumbnailInsidePlayer(thumbnailImg, activeThumbnail, thumbnailClips, mouseLeftOffset, progresBarRightOffset) {

                        // var width = ThumbnailHelpers.getVisibleWidth(thumbnailImg, activeThumbnail.width || thumbnailClips[0].width);

                        // var halfWidth = width / 2;

                        // make sure that the thumbnail doesn't fall off the right side of
                        // the left side of the player
                        /*
                         if (mouseLeftOffset + halfWidth > progresBarRightOffset) {
                         mouseLeftOffset -= mouseLeftOffset + halfWidth - progresBarRightOffset;
                         } else if (mouseLeftOffset < halfWidth) {
                         mouseLeftOffset = halfWidth;
                         }
                         */
                        if (mouseLeftOffset > progresBarRightOffset) {
                            mouseLeftOffset = progresBarRightOffset;
                        } else if (mouseLeftOffset < 0) {
                            mouseLeftOffset = 0;
                        }
                        return mouseLeftOffset;
                    }
                }, {
                    key: 'updateThumbnailLeftStyle',
                    value: function updateThumbnailLeftStyle(mouseLeftOffset, thumbnailsHolder) {
                        var leftValue = {mouseLeftOffset: mouseLeftOffset};

                        thumbnailsHolder.style.left = leftValue.mouseLeftOffset + 'px';
                    }
                }, {
                    key: 'getActiveThumbnail',
                    value: function getActiveThumbnail(thumbnailClips, mouseTime) {
                        var activeClip = 0;

                        for (var clipNumber in thumbnailClips) {
                            if (mouseTime > clipNumber) {
                                activeClip = Math.max(activeClip, clipNumber);
                            }
                        }
                        return thumbnailClips[activeClip];
                    }
                }, {
                    key: 'updateThumbnailSrc',
                    value: function updateThumbnailSrc(activeThumbnail, thumbnailImg) {
                        if (activeThumbnail.src && thumbnailImg.src !== activeThumbnail.src) {
                            thumbnailImg.src = activeThumbnail.src;
                        }
                    }
                }, {
                    key: 'updateThumbnailStyle',
                    value: function updateThumbnailStyle(activeThumbnail, thumbnailImg) {
                        if (activeThumbnail.style && thumbnailImg.style !== activeThumbnail.style) {
                            ThumbnailHelpers.createThumbnails(thumbnailImg.style, activeThumbnail.style);
                        }
                    }
                }, {
                    key: 'moveListener',
                    value: function moveListener(event, progressControl, thumbnailsHolder, thumbnailClips, timelineTime, thumbnailImg, player) {

                        var duration = ThumbnailHelpers.getVideoDuration(player);
                        var pageXOffset = ThumbnailHelpers.getScrollOffset().x;
                        var progresBarPosition = progressControl.seekBar.el().getBoundingClientRect();

                        var progresBarRightOffset = (progresBarPosition.width || progresBarPosition.right) + pageXOffset;

                        var pageMousePositionX = ThumbnailHelpers.getPageMousePositionX(event);

                        var mouseLeftOffset = ThumbnailHelpers.findMouseLeftOffset(pageMousePositionX, progressControl, pageXOffset, event);
                        mouseLeftOffset = ThumbnailHelpers.keepThumbnailInsidePlayer(thumbnailImg, activeThumbnail, thumbnailClips, mouseLeftOffset, progresBarRightOffset);


                        var mouseTime = ThumbnailHelpers.getMouseVideoTime(mouseLeftOffset, progressControl, duration);

                        var activeThumbnail = ThumbnailHelpers.getActiveThumbnail(thumbnailClips, mouseTime);

                        ThumbnailHelpers.updateThumbnailTime(timelineTime, progressControl);

                        ThumbnailHelpers.updateThumbnailSrc(activeThumbnail, thumbnailImg);

                        ThumbnailHelpers.updateThumbnailStyle(activeThumbnail, thumbnailImg);

                        // mouseLeftOffset = ThumbnailHelpers.keepThumbnailInsidePlayer(thumbnailImg, activeThumbnail, thumbnailClips, mouseLeftOffset, progresBarRightOffset);

                        ThumbnailHelpers.updateThumbnailLeftStyle(mouseLeftOffset, thumbnailsHolder);
                    }
                }, {
                    key: 'upadateOnHover',
                    value: function upadateOnHover(progressControl, thumbnailsHolder, thumbnailClips, timelineTime, thumbnailImg, player) {

                        // update the thumbnail while hovering
                        /*
                         progressControl.on('mousemove', function (event) {
                         ThumbnailHelpers.moveListener(event, progressControl, thumbnailsHolder, thumbnailClips, timelineTime, thumbnailImg, player);
                         });
                         */

                        var doc = progressControl.el_.ownerDocument;
                        progressControl.on(doc, 'mousemove', function (event) {
                            ThumbnailHelpers.moveListener(event, progressControl, thumbnailsHolder, thumbnailClips, timelineTime, thumbnailImg, player);
                        });


                        progressControl.on('touchmove', function (event) {
                            ThumbnailHelpers.moveListener(event, progressControl, thumbnailsHolder, thumbnailClips, timelineTime, thumbnailImg, player);
                        });
                    }
                }, {
                    key: 'hideThumbnail',
                    value: function hideThumbnail(thumbnailsHolder) {
                        // thumbnailsHolder.style.left = '-1000px';
                    }
                }, {
                    key: 'upadateOnHoverOut',
                    value: function upadateOnHoverOut(progressControl, thumbnailsHolder, player) {

                        // move the placeholder out of the way when not hovering
                        progressControl.on('mouseout', function (event) {
                            ThumbnailHelpers.hideThumbnail(thumbnailsHolder);
                        });
                        progressControl.on('touchcancel', function (event) {
                            ThumbnailHelpers.hideThumbnail(thumbnailsHolder);
                        });
                        progressControl.on('touchend', function (event) {
                            ThumbnailHelpers.hideThumbnail(thumbnailsHolder);
                        });
                        player.on('userinactive', function (event) {
                            ThumbnailHelpers.hideThumbnail(thumbnailsHolder);
                        });
                    }
                }]);

                return ThumbnailHelpers;
            }();

            // Default options for the plugin.


            var defaults = {};

            // Cross-compatibility for Video.js 5 and 6.
            var registerPlugin = videojs.registerPlugin || videojs.plugin;
            // const dom = videojs.dom || videojs;

            /**
             * Function to invoke when the player is ready.
             *
             * This is a great place for your plugin to initialize itself. When this
             * function is called, the player will have its DOM and child components
             * in place.
             *
             * @function onPlayerReady
             * @param    {Player} player
             *           A Video.js player.
             * @param    {Object} [options={}]
             *           An object of options left to the plugin author to define.
             */

            var prepareThumbnailsClips = function prepareThumbnailsClips(videoTime, options) {

                //var currentIteration = 0;
                //var thumbnailOffset = 0;
                var spriteURL = options.spriteUrl;
                /*
                 var thumbnailClips = {
                 0: {
                 src: spriteURL,
                 style: {
                 left: thumbnailWidth / 2 * -1 + 'px',
                 width: (Math.floor(videoTime / stepTime) + 1) * thumbnailWidth + 'px',
                 clip: 'rect(0,' + options.width + 'px,' + options.width + 'px, 0)'
                 }
                 }
                 };
                 */

                var thumbnailWidth = options.thumbWidth;
                var thumbnailHeight = options.thumbHeight || Math.floor(options.thumbWidth * options.height / options.width);
                var thumbRatio = options.thumbWidth / options.width;
                var thumbHRatio = options.thumbHeight / options.height;

                var thumbnailClips = {
                    0: {
                        src: spriteURL,
                        style: {
                            width: thumbnailWidth + 'px',
                            height: thumbnailHeight + 'px',
                            marginLeft: (thumbnailWidth / -2) + 'px',
                            backgroundPosition: '0 0',
                            backgroundSize: (1920 * thumbRatio) + 'px auto'
                        }
                    }
                };

                var loopIndex = 2;
                var frames;
                if (videoTime <= 120) {
                    frames = 60;
                    loopIndex = 3;
                } else if (videoTime <= 200) {
                    frames = 120;
                } else if (videoTime <= 600) {
                    frames = 210;
                } else if (videoTime < 1200) {
                    frames = 300;
                } else {
                    frames = 390;
                }
                var stepTime = videoTime / frames;


                var currentTime = 0;
                var col = 0;
                var row = 0;
                while (loopIndex < frames) {
                    //thumbnailOffset = ++currentIteration * options.width;

                    col = loopIndex % 30;
                    row = Math.floor(loopIndex / 30);
                    var posX = col * options.width;
                    var posY = row * options.height;

                    if ( !!thumbnailClips[currentTime] ) {
                        thumbnailClips[currentTime].style.backgroundPosition = (-posX * thumbRatio) + 'px ' + (-posY * thumbHRatio) + 'px';
                    } else {
                        thumbnailClips[currentTime] = {
                            style: {
                                backgroundPosition: (-posX * thumbRatio) + 'px ' + (-posY * thumbHRatio) + 'px'
                            }
                        };
                    }

                    currentTime += stepTime;
                    loopIndex++;
                }

                thumbnailClips[0].style.backgroundSize = (1920 * thumbRatio) + 'px ' + ((row + 1) * options.height * thumbHRatio) + 'px';

                return thumbnailClips;
            };

            var initializeThumbnails = function initializeThumbnails(thumbnailsClips, player) {

                var thumbnailClips = ThumbnailHelpers.createThumbnails({}, defaults, thumbnailsClips);
                var progressControl = player.controlBar.progressControl || player.controlBar.getChild('Progress');
                var thumbnailImgWrap = ThumbnailHelpers.createThumbnailImgWrap();
                var thumbnailImg = ThumbnailHelpers.createThumbnailImg(thumbnailClips);
                var timelineTime = ThumbnailHelpers.createThumbnailTime();
                var thumbnailArrowDown = ThumbnailHelpers.createThumbnailArrowDown();
                var thumbnaislHolder = ThumbnailHelpers.createThumbnaislHolder();

                thumbnaislHolder = ThumbnailHelpers.mergeThumbnailElements(thumbnaislHolder, thumbnailImg, timelineTime, thumbnailArrowDown, thumbnailImgWrap);
                ThumbnailHelpers.hidePlayerOnHoverTime(progressControl);

                if (global.window.navigator.userAgent.toLowerCase().indexOf('android') !== -1) {
                    ThumbnailHelpers.suportAndroidEvents();
                }

                var stylesImgWrap = {
                    width: thumbnailClips['0'].style.width,
                    height: thumbnailClips['0'].style.height,
                    marginLeft: thumbnailClips['0'].style.marginLeft
                };
                delete thumbnailClips['0'].style.width;
                delete thumbnailClips['0'].style.height;
                delete thumbnailClips['0'].style.marginLeft;
                var stylesImg = thumbnailClips['0'].style;
                ThumbnailHelpers.createThumbnails(thumbnailImgWrap.style, stylesImgWrap);
                ThumbnailHelpers.createThumbnails(thumbnailImg.style, stylesImg);

                ThumbnailHelpers.centerThumbnailOverCursor(thumbnailImg);

                ThumbnailHelpers.addThumbnailToPlayer(progressControl, thumbnaislHolder);

                ThumbnailHelpers.upadateOnHover(progressControl, thumbnaislHolder, thumbnailClips, timelineTime, thumbnailImg, player);

                ThumbnailHelpers.upadateOnHoverOut(progressControl, thumbnaislHolder, player);
            };

            var onPlayerReady = function onPlayerReady(player, options) {
                if ( options.enabled === false ) return;

                player.one('loadedmetadata', function () {
                    var thumbnailsClips = prepareThumbnailsClips(player.duration(), options);
                    initializeThumbnails(thumbnailsClips, player);
                });

                if ( player.duration() > 0 ) {
                    var thumbnailsClips = prepareThumbnailsClips(player.duration(), options);
                    initializeThumbnails(thumbnailsClips, player);
                }
            };
            /**
             * A video.js plugin.
             *
             * In the plugin function, the value of `this` is a video.js `Player`
             * instance. You cannot rely on the player being in a "ready" state here,
             * depending on how the plugin is invoked. This may or may not be important
             * to you; if not, remove the wait for "ready"!
             *
             * @function thumbnails
             * @param    {Object} [options={}]
             *           An object of options left to the plugin author to define.
             */
            var thumbnails = function thumbnails(options) {
                var _this = this;

                this.setThumbnails = function (settings) {
                    this.ready(function () {
                        onPlayerReady(_this, videojs.mergeOptions(options, settings));
                    });
                };

            };

            // Register the plugin with video.js.
            registerPlugin('thumbnails', thumbnails);

            // Include the version number.
            thumbnails.VERSION = '1.0.3';

            module.exports = thumbnails;

        }, {"1": 1}]
    }, {}, [2])(2)
});


(function ($) {
//    $.fn.extend({
//        live: function (event, fn) {
//            $('body').on(event, this.selector, function() {
//                fn.apply(this, arguments);
//            });
//            return this;
//        }
//    });

    if (!$.fn.addBack) {
        jQuery.fn.extend({
            addBack: function (selector) {
                return this.add(selector == null ?
                    this.prevObject : this.prevObject.filter(selector)
                );
            }
        });
    }

    $.support.transition = (function () {
      var thisBody = document.body || document.documentElement
        , thisStyle = thisBody.style
        , support = thisStyle.transition !== undefined || thisStyle.WebkitTransition !== undefined || thisStyle.MozTransition !== undefined || thisStyle.MsTransition !== undefined || thisStyle.OTransition !== undefined;

      return support && {
        end: (function () {
          var transitionEnd = "TransitionEnd";
          if (thisStyle.WebkitTransition !== undefined) {
          	transitionEnd = "webkitTransitionEnd";
          } else if (thisStyle.MozTransition !== undefined) {
          	transitionEnd = "transitionend";
          } else if (thisStyle.OTransition !== undefined) {
          	transitionEnd = "oTransitionEnd";
          }
          return transitionEnd;
        }())
      };
    })();
})(jQuery);

(function () {
// todo-s.kalmykov: webkit right-float resizing hint
//    todo-s.kalmykov: move presentation to templates
    tinymce.create('tinymce.plugins.ImgResizePlugin', {

        init: function (ed, url) {
            ed.onPreInit.add(function (ed) {
                ed.dom.loadCSS(url + '/resize.css');
            });
        },

        Menu: function (editor, imgClassName, parentElement) {
            var self = this;
            self.imgClsName = imgClassName.toLowerCase();
            self.editor = editor;
            self.body = self.editor.dom.doc.body;
            self.imageRef = null; // current $(img) element
            self.parentElement = parentElement || self.body;
            self.isInDom = false; // menu is attached to dom
//            self.clsNames = {
//                img: self.imgClsName,
//                wrapper: self.imgClsName + '-resize-wrapper',
//                resizeInner: self.imgClsName + '-resize-inner',
//                resizeLeft: self.imgClsName + '-resize-left',
//                resizeTop: self.imgClsName + '-resize-top',
//                resizeRight: self.imgClsName + '-resize-right',
//                resizeBottom: self.imgClsName + '-resize-bottom',
//                menu: self.imgClsName + '-resize-context-menu',
//                sizeHint: self.imgClsName + '-resize-hint'
//            };
            self.clsNames = {
                img: self.imgClsName,
                wrapper: 'mce' + '-resize-wrapper',
                resizeInner: 'mce' + '-resize-inner',
                resizeLeft: 'mce' + '-resize-left',
                resizeTop: 'mce' + '-resize-top',
                resizeRight: 'mce' + '-resize-right',
                resizeBottom: 'mce' + '-resize-bottom',
                menu: 'mce' + '-resize-context-menu',
                sizeHint: 'mce' + '-resize-hint'
            };

            self.imgWrapper = tinymce.DOM.create('div',
            {'class': self.clsNames.wrapper + ' mceItemNoResize se'},
            '<div class="' + self.clsNames.resizeLeft + ' ' + self.clsNames.resizeTop + '"></div>' +
            '<div class="' + self.clsNames.resizeTop + '"></div>' +
            '<div class="' + self.clsNames.resizeRight + ' ' + self.clsNames.resizeTop + '"></div>' +
            '<div class="' + self.clsNames.resizeLeft + '"></div>' +
            '<div class="' + self.clsNames.resizeInner + ' mceItemNoResize"></div>' +
            '<div class="' + self.clsNames.resizeRight + ' mceItemNoResize"></div>' +
            '<div class="' + self.clsNames.resizeLeft + ' ' + self.clsNames.resizeBottom + '"></div>' +
            '<div class="' + self.clsNames.resizeBottom + '"></div>' +
            '<div class="' + self.clsNames.resizeRight + ' ' + self.clsNames.resizeBottom + ' mceItemNoResize"></div>'

            );
            self.sizeHint = tinymce.DOM.create('div', {id: self.clsNames.sizeHint});
            self.sizeHint.style.display = 'none';
            self.menuNode = tinymce.DOM.create('div',
            {'class': self.clsNames.menu + ' mceItemNoResize'},
//                    'Размеры изображения, ш&times;в: ' +
//                    '<input class="width"/>' +
            '<span class="save-rate"><i class="icon-ww-size icon-gray save-rate"></i></span>' +
//                    '<input class="height"/> px ' +
//                    '<button class="submit">Готово</button>' +
            '<span class="clear"><i class="icon-ww-undo icon-gray clear"></i></span>'
            );
            self.menuNode.contentEditable = false;
            self.imgWrapper.contentEditable = false;
            self.inputs = function () {
                return $(self.menuNode).find('input');
            };
            self.checkbox = function () {
                var menus = $(self.menuNode).find('i.save-rate');
                return $(menus[0]);
            };

//            self.inputs()[0].contentEditable = self.inputs()[2].contentEditable = true;
            self.imgWrapper.appendChild(self.menuNode);
            $(self.imgWrapper).find('.' + self.clsNames.resizeRight + '.' + self.clsNames.resizeBottom)[0].appendChild(self.sizeHint);

            tinymce.dom.Event.add(self.menuNode, 'mousedown', function (e) {
                if ($(e.target).hasClass('clear')) {
//                    self.imageRef.width(self.oldSize.width);
//                    self.imageRef.height(self.oldSize.height);
                    self.imageRef.width('');
                    self.imageRef.removeAttr('width');
                    self.imageRef.height('');
                    self.imageRef.removeAttr('height');
                    self.imageRef.css('width', 'max-width');
                    setWrapperSize();
//                    self.disappear();
                }
                if ($(e.target).hasClass('submit')) {
                    var rightWidth = +(self.inputs()[0].value) || self.oldSize.width;
                    var rightHeight = +(self.inputs()[2].value) || self.oldSize.height;
                    if (self.inputs()[1].checked) {
                        rightHeight = Math.floor(self.inputs()[0].value * (self.oldSize.height / self.oldSize.width));
                    }
                    if (self.inputs()[2].value == rightHeight && self.inputs()[0].value == rightWidth) {
                        self.imageRef.width(rightWidth);
                        self.imageRef.attr('width', rightWidth);
                        self.imageRef.height(rightHeight);
                        self.imageRef.attr('height', rightHeight);
                        self.disappear();
                    } else {
                        self.inputs()[0].value = rightWidth;
                        self.inputs()[2].value = rightHeight;
                        self.imageRef.width(rightWidth);
                        self.imageRef.attr('width', rightWidth);
                        self.imageRef.height(rightHeight);
                        self.imageRef.attr('height', rightHeight);
                        setWrapperSize();
                    }
                }
                if ($(e.target).hasClass('save-rate')) {
                    self.checkbox().toggleClass('is-checked');
                    $(self.checkbox().parentNode).toggleClass('is-checked');
                    tinymce.dom.Event.cancel(e);
                }
                if (e.target.className == 'width') {
//                    self.inputs()[0].contentEditable = true;
                    self.inputs()[0].focus();
                }
                if (e.target.className == 'height') {
//                    self.inputs()[2].contentEditable = true;
                    self.inputs()[2].focus();
                }
            });
            tinymce.dom.Event.add(self.menuNode, 'click', function (e) {
                if ($(e.target).hasClass('save-rate')) {
                    tinymce.dom.Event.cancel(e);
                    return false;
                }
            });

//            tinymce.dom.Event.add(self.inputs()[0], 'change', function(e) {
//                self.imageRef.style.width = self.inputs()[0].value;
//                setWrapperSize();
//            });
//            tinymce.dom.Event.add(self.inputs()[2], 'change', function(e) {
//                self.imageRef.style.height = self.inputs()[2].value;
//                setWrapperSize();
//            });

            tinymce.dom.Event.add(self.imgWrapper, 'mousedown', function (e) {

                self.resizeType = null;
                if (self.editor.dom.hasClass(e.target, self.clsNames.resizeRight +
                ' ' + self.clsNames.resizeBottom) && $(self.imageRef).css('float') != 'right') {
                    self.resizeType = 'se';
                }
                if (self.editor.dom.hasClass(e.target, self.clsNames.resizeLeft +
                ' ' + self.clsNames.resizeBottom) && $(self.imageRef).css('float') == 'right')
                    self.resizeType = 'sw';
                if (self.editor.dom.hasClass(e.target, self.clsNames.resizeRight +
                ' ' + self.clsNames.resizeTop) && $(self.imageRef).css('float') == 'left')
                    self.resizeType = null;
                if (self.editor.dom.hasClass(e.target, self.clsNames.resizeLeft +
                ' ' + self.clsNames.resizeTop) && $(self.imageRef).css('float') == 'right')
                    self.resizeType = null;

                if (self.resizeType) {
                    $(self.sizeHint).css('display', 'block');

                    tinymce.dom.Event.add(self.body, 'mousemove', resize);
                    tinymce.dom.Event.add(self.body, 'mouseup', function (e) {
                        tinymce.dom.Event.clear(self.body, 'mouseup');
                        tinymce.dom.Event.clear(self.body, 'mousemove');
                        $(self.sizeHint).css('display', 'none');
                    });
                }
                tinymce.dom.Event.cancel(e);
            });

            function disableEvent(ed, a, b, c, d) {
                d.terminate = true;
            }

            self.handleSize = 7;
            self.appear = appear;
            self.disappear = disappear;

            if (tinymce.isOpera)
                tinymce.dom.Event.add(self.editor, 'mousedown', function (e) {
                    if (e.target.nodeName === 'IMG' && self.editor.dom.hasClass(e.target, self.clsNames.img)) {
                        // opera drag'n'drop
                    }
                });

            editor.onExecCommand.add(function () {
                if (self.isInDom) {
                    self.disappear();
                }
            });

            function appear() {
                if (tinymce.isIE6)
                    return;
                self.body.appendChild(self.imgWrapper);
//                self.parentElement.appendChild(self.menuNode);
                //self.checkbox().addClass('is-checked');
                //$(self.checkbox().parentNode).addClass('is-checked');
                self.isInDom = true;
                self.editor.contentEditable = false;
                self.body.contentEditable = false;
                $(self.imageRef).addClass('is-under-resize');
                self.oldSize = {
                    width: self.imageRef.width(),
                    height: self.imageRef.height()
                };
                setWrapperSize();
                self.imageRef.contentEditable = false;
                self.editor.onBeforeExecCommand.add(disableEvent);
            }

            function disappear() {
                if (tinymce.isIE6)
                    return;
                var parent = self.imgWrapper.parentNode;
                self.imageRef.className = $(self.imageRef).removeClass('is-under-resize');
                self.imageRef.contentEditable = true;
                self.imgWrapper = parent.removeChild(self.imgWrapper);
//                self.menuNode = $(self.menuNode).detach();
                self.isInDom = false;
                self.body.contentEditable = true;
                self.editor.onBeforeExecCommand.remove(disableEvent);
                self.editor.getContent();
                self.editor.undoManager.add();
            }

            function setWrapperSize() {
                self.onResize(null);
                if ($(self.imageRef).css('float') == 'right') {
                    $(self.imgWrapper).removeClass('se').addClass('sw');
                } else {
                    $(self.imgWrapper).removeClass('sw').addClass('se');
                }
                $(self.imgWrapper).css('top', self.imageRef.offset().top);
                $(self.imgWrapper).css('left', self.imageRef.offset().left);
                $(self.imgWrapper).width(self.imageRef.width());
//                $(self.imgWrapper).attr('width', (self.imageRef.width()));
                $(self.imgWrapper).height(self.imageRef.height());
//                $(self.imgWrapper).attr('height', (self.imageRef.height()));
            }

            function resize(e) {
                var offset = {
                    top: self.imgWrapper.offsetTop,
                    left: self.imgWrapper.offsetLeft,
                    height: self.imgWrapper.offsetHeight,
                    width: self.imgWrapper.offsetWidth
                };
                if (e.pageX == null && e.clientX != null) {
                    var html = self.editor.dom.doc.documentElement;
                    var body = self.body;

                    e.pageX = e.clientX + (html.scrollLeft || body && body.scrollLeft || 0);
                    e.pageX -= html.clientLeft || 0;

                    e.pageY = e.clientY + (html.scrollTop || body && body.scrollTop || 0);
                    e.pageY -= html.clientTop || 0;
                }
                if (self.resizeType == 'se') {
                    var newHeight = Math.max(e.pageY - offset.top, self.handleSize);
                    var newWidth = Math.max(e.pageX - offset.left, self.handleSize);
                    self.sizeHint.style.left = 20;
                } else if (self.resizeType == 'ne') {
                    newHeight = Math.max(offset.top - e.pageY + offset.height, self.handleSize);
                    newWidth = Math.max(e.pageX - offset.left, self.handleSize);
                } else if (self.resizeType == 'sw') {
                    newHeight = Math.max(e.pageY - offset.top, self.handleSize);
                    newWidth = Math.max(offset.left - e.pageX + offset.width, self.handleSize);
                    self.sizeHint.style.left = -100;//- newWidth - 100;
                } else if (self.resizeType == 'nw') {
                    newHeight = Math.max(offset.top - e.pageY + offset.height, self.handleSize);
                    newWidth = Math.max(offset.left - e.pageX + offset.width, self.handleSize);
                }
                if (!self.checkbox().hasClass('is-checked')) {
                    newHeight = Math.floor(newWidth * (self.oldSize.height / self.oldSize.width));
                }
                self.imageRef.height(newHeight);
                self.imageRef.attr('height', newHeight);
                self.imageRef.width(newWidth);
                self.imageRef.attr('width', newWidth);
                self.sizeHint.innerHTML = newWidth + ' &times; ' + newHeight + ' px';

//                self.inputs()[0].value = newWidth;
//                self.inputs()[2].value = newHeight;
                setWrapperSize();
                tinymce.dom.Event.cancel(e);
                return false;
            }
        }
    });

    tinymce.PluginManager.add('imgresize', tinymce.plugins.ImgResizePlugin);

    tinymce.plugins.ImgResizePlugin.Menu = tinymce.plugins.ImgResizePlugin.prototype.Menu;
})();


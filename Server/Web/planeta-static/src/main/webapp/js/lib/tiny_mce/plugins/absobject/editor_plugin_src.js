/**
 * editor_plugin_src.js
 *
 * Copyright 2009, Moxiecode Systems AB
 * Released under LGPL License.
 *
 * License: http://tinymce.moxiecode.com/license
 * Contributing: http://tinymce.moxiecode.com/contributing
 */

(function() {
	tinymce.create('tinymce.plugins.AbsObjectPlugin', {
		init : function(ed, url) {
			// Register commands


			ed.addCommand('mceAbsObject', function() {
				// Internal image object like a flash placeholder
				if (ed.dom.getAttrib(ed.selection.getNode(), 'class', '').indexOf('mceItem') != -1)
					return;

                var absClass = 'abstract-object-wrapper';
                var absObjectId = 'abs-object';

                if(ed.dom.loadCSS){
                    ed.dom.loadCSS('css/absobject.css');
                }

//                tinymce.dom.ScriptLoader.loadScripts('absobject.js');

                var innerImg = ed.dom.createHTML('img', {src: 'img/Penguins.jpg', alt: 'Пингвины', width: 200});
                var titleDiv = ed.dom.createHTML('div', {align: 'center'}, 'Пингвины');
                var ifr = ed.dom.create('iframe', {src: 'ifr.html', id: 'ifr1'});

                var outerDivElement = ed.dom.create('div', {id: absObjectId, 'class': absClass, 'style' : 'width: 200px'}, titleDiv + innerImg);

                var outerDiv = ed.dom.createHTML('div', {id: absObjectId, 'class': 'abs-object'}, titleDiv + innerImg);
//                ed.dom.cre
                var objectHMTL = '';

                ed.dom.insertAfter(outerDivElement, ed.selection.getNode());
//                ed.dom.insertAfter(ifr, ed.selection.getNode());
//                var ifrElem = $(ed.dom.getRoot()).find("#ifr1");
//                var iframeContents = ifrElem.contents();
//                var iframeBody = iframeContents.find("body");
//                var newElement = $("<img src='img/Penguins.jpg'>");
//                iframeBody.append(newElement);
//
//                var titleElement = document.createElement('div');
//                var textElement = document.createTextNode('TEST');
//                titleElement.appendChild(textElement);
////                $(iframeBody).append(titleElement);
//
//                var imgElement = document.createElement('img');
//                $(imgElement).attr({'src': 'img/Penguins.jpg', alt: 'Пингвины', width: 200});
//                $(iframeBody).append(imgElement);
//                var ifw = iframeContents.width();
//                var ifh = iframeContents.height();
//                ifrElem.width(ifw); ifrElem.height(ifh);
//                $(ed.dom.getRoot()).find("#ifr1")y.attr('width', ifw); ifrbody.attr('height', ifh);
//                $(iframe).contents.find("#ifrbody").innerHTML = "";
                ed.dom.bind(ed.dom.getRoot(),'mousedown', function(e) {
                    var event = e || window.event;
                    var target = event.target || event.srcElement;
                    while(target != window && target.getAttribute && target.getAttribute('class') != absClass ) {
                        target = target.parentNode;
                    }
                    tinymce.activeEditor.selection.select(target);
                    var str = objectHMTL = tinymce.activeEditor.selection.getContent();
                    console.log('--------------------------------------');
                    console.log(str);
                });
//                ed.dom.bind(ed.dom.getRoot(),'mousemove', function(e) {
//                    var event = e || window.event;
//                    var target = event.target || event.srcElement;
//                    while(target != window && target.getAttribute('class') != absClass ) {
//                        target = target.parentNode;
//                    }
//                    tinymce.activeEditor.selection.selectCampaignById(target);
//                    var aaa = tinymce.activeEditor.selection.getContent();
////                    alert(aaa);
//                });

//                tinymce.dom.Event.add(ed.dom.getRoot(), 'mouseup', function(e) {
//                    var sel = tinymce.activeEditor.selection.getContent();
//                    if(sel == ''){
//                        return;
//                    }
//                    var event = e || window.event;
//                    var target = event.target || event.srcElement;
//                    var body = tinymce.activeEditor.dom.getRoot();
//                    // check if we are in object wrapper
//                    while(target != body && target.getAttribute('class') != absClass ) {
//                        target = target.parentNode;
//                    }
//                    if(target != body){ // we try to drop into object
//                        var parent = target.parentNode;
//                        this.stopPropagation();
//                        return null;
//                    }
//
////                    tinymce.activeEditor.selection.selectCampaignById(target);
//                });

                ed.dom.bind(ed.dom.getRoot(),'drop', function(e) {
                    var bindedElement = this;
                    var sel = tinymce.activeEditor.selection.getContent();
                    if(sel == ''){
                        return;
                    }
                    var event = e || window.event;
                    var target = event.target || event.srcElement;
                    var body = tinymce.activeEditor.dom.getRoot();
//                    var bodyHtml = body.innerHTML;
//                    var expr = /<p\s+_moz_dirty[^>]+>/gi;
//                    var matches = bodyHtml.match(expr);
//                    console.log(matches);
                    //$(body).find("p").find("[_moz_dirty]")
                    // check if we are in object wrapper
                    while(target != body && target.getAttribute && target.getAttribute('class') != absClass ) {
                        target = target.parentNode;
                    }
                    if(target != body){ // we try to drop into object
                        alert('dont!');
                        event.stopPropagation();
                        tinymce.activeEditor.selection = document.createTextNode('');
//                        return null;
                    }

//                    tinymce.activeEditor.selection.selectCampaignById(target);
                });

//                ed.execCommand('mceInsertContent', false, outerDiv, {skip_undo : 1});


//                ed.dom.bind()contents().find(".abs-object");
//                console.log(classes);
//                classes.bind('click', selectAbsObject);
//                ed.dom.get('style').value = dom.serializeStyle(dom.parseStyle(img.style.cssText), 'img');
//				ed.windowManager.open({
//					file : url + '/absobject.htm',
//					width : 480 + parseInt(ed.getLang('absobject.delta_width', 0)),
//					height : 385 + parseInt(ed.getLang('absobject.delta_height', 0)),
//					inline : 1
//				}, {
//					plugin_url : url
//				});
			});

			// Register buttons
			ed.addButton('absobject', {
				title : 'absobject.image_desc',
				cmd : 'mceAbsObject',
                image : url + '/img/logo.png'
			});
		},

		getInfo : function() {
			return {
				longname : 'Editor Object',
				author : 'Moxiecode Systems AB',
				authorurl : 'http://tinymce.moxiecode.com',
				infourl : 'http://wiki.moxiecode.com/index.php/TinyMCE:Plugins/object',
				version : tinymce.majorVersion + "." + tinymce.minorVersion
			};
		}
	});

	// Register plugin
	tinymce.PluginManager.add('absobject', tinymce.plugins.AbsObjectPlugin);
    tinymce.execCommand('mceAbsObject');
    //tinymce.activeEditor.execCommand("enableObjectResizing", false, "false");

})();
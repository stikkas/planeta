tinyMCEPopup.requireLangPack();

var MusicDialog = {
	init : function() {
        this.musicData = {
            title : '',
            time : '',
            playUrl : ''
        };
		var f = document.forms[0];

        this.editor = tinyMCEPopup.editor;

		// Get the selected contents as text and place it in the input
        if(tinyMCEPopup.getWindowArg('html')){
            f.html.value = tinyMCEPopup.getWindowArg('html');
            this.musicData = tinyMCEPopup.editor.plugins.music.mapping.htmlToJSON(f.html.value);
            f.html.value = '';
        }
		f.title.value = this.musicData.title;
        f.time.value = this.musicData.time;
        f.url.value = this.musicData.playUrl;
	},

	insert : function() {
        var editor = tinyMCEPopup.editor;

        this.formToData();
//        editor.execCommand('mceRepaint');  // for preview
        tinyMCEPopup.restoreSelection();
        var html = this.editor.plugins.music.mapping.jsonToHtml(this.musicData);
//        var html = this.editor.plugins.music.mapping.imgToHtml(node)
//        tinyMCEPopup.editor.execCommand('mceInsertContent', false, html);
        tinyMCEPopup.editor.selection.setNode(this.editor.plugins.music.mapping.jsonToImg(this.musicData, true));
        tinyMCEPopup.close();
    },

    preview : function() {
        document.getElementById('prev').innerHTML = this.editor.plugins.music.mapping.jsonToHtml(this.musicData, true);
	},

    formToData : function() {
        var f = document.forms[0];
		// Insert the contents from the input into the document
        this.musicData = {
            title : f.title.value || 'Бонни и Клайд',
            time : f.time.value || '2:40',
            playUrl : f.url.value || 'javascript:workspace.playTrack(23709, 13222);'

        };
        if(f.html.value != ''){
            this.musicData = tinyMCEPopup.editor.plugins.music.mapping.htmlToJSON(f.html.value);
        }
	}
};

tinyMCEPopup.onInit.add(MusicDialog.init, MusicDialog);

(function ($) {

    var defaults = {
        range: true
    };

    var methods = {
        init : function( options ) {
            var data = $(this).data('filterSlider');
            if (data) return this;

            $(this).data('filterSlider', true);

            options = $.extend(true, {}, defaults, options);

            return this.each(function() {
                methods.init_el($(this), options);
            });
        },
        init_el: function($el, options) {
            var sliderContainer;
            sliderContainer = $el;
            var sliderWrap = $('.project-filter-slider', sliderContainer);
            var slider = $('.prjf-slider-range', sliderWrap);

            var heterogeneity = slider.data('heterogeneity');
            var isHeterogeneity = !!heterogeneity;

            var fromInput = $('.prjf-slider-from', sliderWrap);
            var fromVal = isHeterogeneity ? valueToPrc(fromInput.val(), {isMin:true}) : fromInput.val();
            var toInput = $('.prjf-slider-to', sliderWrap);
            var isToInput = !(_.isEmpty(toInput));
            var toVal = isHeterogeneity ? valueToPrc(toInput.val()) : toInput.val();

            var sliderLabel = $('.project-filter-slider-label', sliderContainer);
            var sliderFromVal = $('.prjf-slider-from-val', sliderLabel);
            var sliderToPrefix = $('.prjf-slider-to-only-prefix', sliderLabel);
            var sliderToVal = $('.prjf-slider-to-val', sliderLabel);
            var sliderTopostfix = $('.prjf-slider-to-postfix', sliderLabel);
            var sliderHiddenVal = $('.prjf-slider-hidden-val', sliderLabel);
            var sliderAllInterval = $('.prjf-slider-all-interval', sliderLabel);
            var sliderInterval = $('.prjf-slider-interval', sliderLabel);

            var superMaxIsPositiveInfinity = slider.data('superMaxIsPositiveInfinity');
            var superMinIsNegativeInfinity = slider.data('superMinIsNegativeInfinity');

            if ( isToInput ) {
                setValues(fromVal, toVal);
            } else {
                setValues(fromVal);
            }

            slider.slider({
                range: isToInput ? true : 'min',
                min: options.range ? 0 : slider.data('min'),
                max: options.range ? 100 : slider.data('max'),
                value: isToInput ? null : fromVal,
                values: isToInput ? [fromVal, toVal] : null,
                slide: function(event, ui) {
                    if ( isToInput ) {
                        setValues(ui.values[0], ui.values[1]);
                    } else {
                        setValues(ui.value);
                    }
                },
                start: function() {
                    $('body').addClass('cur-p');
                },
                stop: function() {
                    $('body').removeClass('cur-p');
                    triggerChange();
                }
            });

            function setValues(prcFrom, prcTo) {
                if (prcTo == null) prcTo = prcFrom;
                if (isHeterogeneity) {
                    from = prcToValue(prcFrom, { isMin:true });
                    to = prcToValue(prcTo);
                } else {
                    from = prcFrom;
                    to = prcTo;
                }

                var maxBorderValue = slider.data('maxBorderValue');
                var minBorderValue = slider.data('minBorderValue');

                sliderLabel.children(':not(.prjf-slider-label-text)').hide();
                if (prcTo == 100 && superMaxIsPositiveInfinity) {
                    if (prcFrom == 0 || !isToInput) {
                        //написать что весь интервал
                        sliderAllInterval.show();
                    } else {
                        //написать что интервал от minVal
                        sliderHiddenVal.show();
                        if (minBorderValue && prcFrom == 0) {
                            sliderFromVal.html(minBorderValue);
                        } else {
                            sliderFromVal.html(StringUtils.humanNumber(from));
                        }
                    }
                } else if (prcFrom == prcTo) {
                    //написать что строго val

                    //sliderToVal.html(StringUtils.humanNumber(to));
                    if (minBorderValue && prcFrom == 0) {
                        sliderToVal.show();
                        sliderToVal.html(minBorderValue);
                    } else {
                        sliderToVal.show();
                        sliderTopostfix.show();
                        sliderToVal.html(StringUtils.humanNumber(from));

                    }
                } else if (prcFrom == 0) {
                    //написать что интервал до maxVal
                    sliderToPrefix.show();
                    sliderToVal.show();
                    sliderTopostfix.show();
                    sliderToVal.html(StringUtils.humanNumber(to));
                } else {
                    //todo написать интервал от minVal и до maxVal
                    sliderHiddenVal.show();
                    sliderFromVal.html(StringUtils.humanNumber(from));
                    sliderInterval.show();
                    sliderToPrefix.show();
                    sliderToVal.show();
                    sliderTopostfix.show();
                    sliderToVal.html(StringUtils.humanNumber(to));
                }

                //update values
                if (fromInput.val() != from) {
                    fromInput.val(from);
                }
                if (toInput.val() != to) {
                    toInput.val(to);
                }
            }

            function triggerChange() {
                isToInput && toInput.change();
                fromInput.change();
            }
            function prcToValue(prc, options) {
                options = options || {};
                var max = slider.data('max');
                var h = heterogeneity.split(';');
                var _begin = 0, _from = 0, value = 'undefined';

                var _prc = parseInt(prc);
                for (var i = 0; i <= h.length; i++) {
                    var v;
                    if (h[i]) {
                        v = h[i].split("/");
                    } else {
                        v = [100, max];
                    }

                    var _end = parseInt(v[0]);
                    if(_begin == _end) continue;

                    var _to = parseInt(v[1]);

                    if (_prc >= _begin && _prc <= _end) {
                        value = _from + ( (_prc - _begin) * (_to - _from) ) / (_end - _begin);
                    }
                    _begin = _end;
                    _from = _to;
                }
                if (_prc == 100 && options.isMin) {
                    return Math.floor(value);
                } else if (_prc == 100 && superMaxIsPositiveInfinity) {
                    return '';
                } else if (_prc == 0 && superMinIsNegativeInfinity && options.isMin) {
                    return '';
                }

                return Math.floor(value);
            }

            function valueToPrc(value, options) {
                options = options || {};
                if (value == '') {
                    if (options.isMin) {
                        return 0;
                    } else {
                        return 100;
                    }
                }
                var max = slider.data('max');
                var h = heterogeneity.split(';');
                var _start = 0;
                var _from = 0;
                var v, prc;

                for (var i = 0; i <= h.length; i++) {
                    if (h[i]) {
                        v = h[i].split("/");
                    } else {
                        v = [100, max];
                    }
                    v[0] = new Number(v[0]);
                    v[1] = new Number(v[1]);

                    if (value >= _from && value <= v[1]) {
                        prc = _start + (value - _from) * (v[0] - _start) / (v[1] - _from);
                    }

                    _start = v[0];
                    _from = v[1];
                }

                return prc;
            }
        },
        disabled: function() {
            var sliderContainer = $(this);
            var sliderWrap = $('.project-filter-slider', sliderContainer);
            var slider = $('.prjf-slider-range', sliderWrap);
            slider.slider({ disabled: true });
            sliderContainer.addClass('disabled');
        },
        enabled: function() {
            var sliderContainer = $(this);
            var sliderWrap = $('.project-filter-slider', sliderContainer);
            var slider = $('.prjf-slider-range', sliderWrap);
            slider.slider({ disabled: false });
            sliderContainer.removeClass('disabled');
        }
    };

    $.fn.filterSlider = function( method ) {

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on jQuery.filterSlider' );
        }

    };

})(jQuery)
/**
 * Fancybox jquery plugin fix :
 * Planeta proxy images
 * (http://s1.dev.planeta.ru/p?url=http%3A%2F%2Fwww.naykris.com%2Fimg%2Ffutbolka-man.png&width=0&height=0)
 * doesn't match native isImage function
 */
(function () {
    var isImage = $.fancybox.isImage;
    $.fancybox.isImage = function (str) {
        if (workspace && workspace.staticNodesService && workspace.staticNodesService.isProxyServerPath(str)) {
            // add restrictions if you want
            // native isImage function checks if str is String
            return $.type(str) === "string";
        }
        return isImage(str);
    };
})();

Tooltip.autoinit = false;

Tooltip.init = function () {
    $(document).on('hover', '[data-tooltip]', function () {
        tooltipCreate(this);
    });

    function tooltipCreate(elem) {
        var el = $(elem);
        if ( el.data('tooltipInit') ) return;

        el.data('tooltipInit', true);

        var classes = $(el).data('tooltip-theme') || "";
        var tooltip = new Tooltip({
            target: el[0],
            classes: "tooltip-theme-arrows " + classes
        });
        tooltip.open();

        el.data('tooltipOption', {
            tooltip: tooltip,
            drop: tooltip.drop.drop
        });

        var randomNamespace = Math.random().toString(36).substring(2);

        el.on('mouseup.' + randomNamespace, function (e) {
            setTimeout(function () {
                if ( !el.is(':visible') && tooltip.drop.isOpened() ) {
                    tooltip.destroy();
                    el.off('mouseup.' + randomNamespace);
                }
            }, 50);
        });


        $(window).on('checkTooltipTarget.' + randomNamespace, function () {
            if ( !el.is(':visible') && tooltip.drop.isOpened() ) {
                tooltip.destroy();
                $(window).off('checkTooltipTarget.' + randomNamespace);
            }
        });
    }
};

$(function () {
    Tooltip.init();
});

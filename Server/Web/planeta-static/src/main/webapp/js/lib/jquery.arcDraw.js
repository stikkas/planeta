(function( $ ){

    var defaults = {
        procent: 0,
        radius: 52,
        fill: 'none',
        strokeWidth: 3,
        strokeBgColor: '#29292a',
        strokeColor: '#3498db'
    };

    var filterId = 0;

    var methods = {
        init: function( options ) {
            var data = $(this).data('arcDraw');
            if (data) return this;

            $(this).data('arcDraw', true);

            var settings = $.extend(true, {}, defaults, options);

            return this.each(function() {
                $(this).data('settings', settings);
                methods.init_el($(this));
            });
        },
        init_el: function($el) {
            var settings = $el.data('settings');
            filterId++;

            $el.data('queueVal', ($el.data('procent') || settings.procent) * 360 / 100);

            var canvas = createSvgElem('svg');
            settings.canvasSize = (settings.radius + Math.ceil(settings.strokeWidth / 2)) * 2;
            $(canvas).attr({
                width: settings.canvasSize,
                height: settings.canvasSize
            });
            canvas.setAttribute('viewBox', '0 0 ' + settings.canvasSize + ' ' + settings.canvasSize);
            $el.append(canvas);

            var circ = createSvgElem('path');
            $(circ).attr({
                'fill': settings.fill,
                'stroke': settings.strokeBgColor,
                'stroke-width': settings.strokeWidth,
                'd': arcPath( 360, $el )
            });
            $(canvas).append(circ);

            var arc = createSvgElem('path');
            $(arc).attr({
                'class': 'svg-arc',
                'fill': 'none',
                'stroke': settings.strokeColor,
                'stroke-width': settings.strokeWidth,
                'd': arcPath( ($el.data('procent') || settings.procent) * 360 / 100, $el )
            });
            $(canvas).append(arc);

            $el.data('settings', settings);
        },
        set: function(value){
            this.each(function() {
                var $el = $(this);
                var settings = $el.data('settings');
                var arc = $el.find('.svg-arc');

                $el.data('queueVal', value);

                arc.attr({
                    d: arcPath( value * 360 / 100, $el )
                });

                $el.data('settings', settings);
            })
        },
        animate: function(options){
            this.each(function() {
                var $el = $(this);
                var settings = $el.data('settings');
                var arc = $el.find('.svg-arc');

                var animValues = {
                    start: 0,
                    end: ($el.data('procent') || settings.procent),
                    duration: 1000
                };
                animValues = $.extend(true, {}, animValues, options);

                var animStartVal = animValues.start * 360 / 100;
                var animEndVal = animValues.end * 360 / 100;

                // animate arc
                $({
                    arcValue: $el.data('queueVal') != animStartVal ? $el.data('queueVal') : animStartVal
                }).stop(1).animate({
                    arcValue: animEndVal
                }, {
                    duration: animValues.duration,
                    easing: 'swing',
                    step: function (value) {
                        arc.attr({
                            d: arcPath( value, $el )
                        });
                        $el.data('queueVal', value);
                    }
                });

                $el.data('settings', settings);
            })
        }
    };


    function createSvgElem(name) {
        return document.createElementNS('http://www.w3.org/2000/svg',name);
    }

    function polarToCartesian(centerX, centerY, radius, angleInDegrees) {
        var angleInRadians = angleInDegrees * Math.PI / 180;
        var x = centerX + radius * Math.cos(angleInRadians);
        var y = centerY + radius * Math.sin(angleInRadians);
        return [x,y];
    }

    function arcPath(deg, $el) {
        var settings = $el.data('settings');

        var arcDeg = deg;
        arcDeg = arcDeg >= 360 ? 359.99 : arcDeg;
        arcDeg = arcDeg < 0 ? 0 : arcDeg;
        var flag = arcDeg <= 180 ? 0 : 1;

        var arcCord = polarToCartesian(settings.canvasSize / 2, settings.canvasSize / 2, settings.radius, arcDeg - 90);

        return 'M ' + settings.canvasSize / 2 + ' ' + Math.ceil(settings.strokeWidth / 2) +
            ' A ' + settings.radius + ' ' + settings.radius +
            ' 0 ' + flag + ' 1 ' +
            arcCord[0] + ' ' + arcCord[1];
    }


    $.fn.arcDraw = function( method ) {

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on jQuery.tooltip' );
        }

    };

})( jQuery );

(function () {
    Flickity.createMethods.push('_swipeFix');

    Flickity.prototype._swipeFix = function() {
        if ( !this.options.draggable ) return;
        var carousel = this;

        var windowScroll = true;
        var disableDrag = false;
        function carouselMove(event, pointer, vector) {
            if ( disableDrag ) return;
            if (Math.abs(vector.x) > carousel.options.dragThreshold ||
                Math.abs(vector.y) > carousel.options.dragThreshold) {
                disableDrag = true;
                if ( Math.abs(vector.x) > Math.abs(vector.y) ) {
                    windowScroll = false;
                } else {
                    carousel.options.draggable = false;
                    carousel.updateDraggable();
                }
            } else {
                windowScroll = true;
            }
        }
        function carouselEndMove() {
            document.removeEventListener('touchend', carouselEndMove);
            document.removeEventListener('touchcancel', carouselEndMove);
            carousel.options.draggable = true;
            carousel.updateDraggable();
            windowScroll = true;
            disableDrag = false;
        }
        carousel.on('dragStart', function () {
            document.addEventListener('touchend', carouselEndMove);
            document.addEventListener('touchcancel', carouselEndMove);
        });
        carousel.on('dragMove', carouselMove);
        carousel.element.addEventListener('touchmove', function (e) {
            if ( !windowScroll ) {
                e.preventDefault();
            }
        }, { passive: false });
    };
})();

/*
 * Lazy Load - jQuery plugin for lazy loading images
 *
 * Copyright (c) 2007-2013 Mika Tuupola
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Project home:
 *   http://www.appelsiini.net/projects/lazyload
 *
 * Version:  1.9.3
 *
 */

(function ($, window, document, undefined) {
    var $window = $(window);

    function elementTop($el) {
        var el = $el[0];
        if (el.getBoundingClientRect) {
            return el.getBoundingClientRect().top - el.ownerDocument.body.getBoundingClientRect().top;
        }
        return this.$el.offset().top;

    }

    function elementLeft($el) {
        var el = $el[0];
        if (el.getBoundingClientRect) {
            return el.getBoundingClientRect().left - el.ownerDocument.body.getBoundingClientRect().left;
        }
        return this.$el.offset().left;

    }

    $.fn.lazyload = function (options) {
        if ($(this).length === 0) {
            return;
        }
        var elements = this;
        var $container;
        var settings = {
            threshold: 0,
            failure_limit: 0,
            event: "scroll",
            effect: "show",
            container: window,
            data_attribute: "original",
            skip_invisible: true,
            appear: null,
            load: null,
            placeholder: "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
        };

        function update() {
            var counter = 0;

            elements.each(function () {
                var $this = $(this);
                if (settings.skip_invisible && !$this.is(":visible")) {
                    return;
                }
                if ($.abovethetop(this, settings) ||
                    $.leftofbegin(this, settings)) {
                    /* Nothing. */
                } else if (!$.belowthefold(this, settings) && !$.rightoffold(this, settings)) {
                    $this.trigger("appear");
                    /* if we found an image we'll load, reset the counter */
                    counter = 0;
                } else {
                    if (++counter > settings.failure_limit) {
                        return false;
                    }
                }
            });

        }

        if (options) {
            /* Maintain BC for a couple of versions. */
            if (undefined !== options.failurelimit) {
                options.failure_limit = options.failurelimit;
                delete options.failurelimit;
            }
            if (undefined !== options.effectspeed) {
                options.effect_speed = options.effectspeed;
                delete options.effectspeed;
            }
            options.namespace = "." + (options.namespace || "");
            $.extend(settings, options);

        }

        /* Cache container as jQuery as object. */
        $container = (settings.container === undefined ||
        settings.container === window) ? $window : $(settings.container);

        function _update() {
            return update();
        }

        /* Fire one scroll event per scroll. Not one scroll event per image. */
        if (0 === settings.event.indexOf("scroll")) {
            $container.bind(settings.event + settings.namespace, _update);
        }

        function storeOriginalDimensions(element) {
            var width = element.attr('width');
            var height = element.attr('height');
            if (width) {
                element.attr('originalWidth', width);
            }
            if (height) {
                element.attr('originalHeight', height);
            }
            element.attr('width', '100%');
            element.attr('height', '100%');
        }

        function resetOriginalDimensions(element) {
            var width = element.attr('originalWidth');
            var height = element.attr('originalHeight');
            element.attr('width', width ? width : '');
            element.attr('height', height ? height : '');
        }

        this.each(function () {
            var self = this;
            var $self = $(self);

            self.loaded = false;

            /* If no src attribute given use data:uri. */
            if ($self.attr("src") === undefined || $self.attr("src") === false) {
                if ($self.is("img")) {
//                    $self.attr("src", settings.placeholder);
                    if ($self.data("noZoom") === undefined) {
                        storeOriginalDimensions($self);
                    }
                }
            }

            /* When appear is triggered load original image. */
            $self.one("appear", function () {
                $self.unbind(settings.namespace);
                $container.unbind(settings.namespace);
                $window.unbind(settings.namespace);

                resetOriginalDimensions($self);

                if (!this.loaded) {
                    if (settings.appear) {
                        var elements_left = elements.length;
                        settings.appear.call(self, elements_left, settings);
                    }
                    $("<img />")
                    .bind("load", function () {
                        var original = $self.attr("data-" + settings.data_attribute);
                        $self.hide();
                        if ($self.is("img")) {
                            $self.attr("src", original);
                        } else {
                            $self.css("background-image", "url('" + original + "')");
                        }
                        $self[settings.effect](settings.effect_speed);
                        if (settings.effect == "show") {
                            $self.css({
                                opacity: 1,
                                '-webkit-filter': 'none',
                                'filter': 'none'
                            });
                        }

                        self.loaded = true;

                        /* Remove image from array so it is not looped next time. */
                        var temp = $.grep(elements, function (element) {
                            return !element.loaded;
                        });
                        elements = $(temp);

                        if (settings.load) {
                            var elements_left = elements.length;
                            settings.load.call(self, elements_left, settings);
                        }
                    })
                    .attr("src", $self.attr("data-" + settings.data_attribute));
                }
            });

            var _customEventFunc = function () {
                if (!self.loaded) {
                    $self.trigger("appear");
                }
            };
            /* When wanted event is triggered load original image */
            /* by triggering appear.                              */
            if (0 !== settings.event.indexOf("scroll")) {
                $self.bind(settings.event + settings.namespace, _customEventFunc);
            }
        });

        var _resize = function () {
            update();
        };
        /* Check if something appears when window is resized. */
        $window.bind("resize" + settings.namespace, _resize);

        /* With IOS5 force loading images when navigating with back button. */
        /* Non optimal workaround. */
        if ((/(?:iphone|ipod|ipad).*os 5/gi).test(navigator.appVersion)) {
            $window.bind("pageshow", function (event) {
                if (event.originalEvent && event.originalEvent.persisted) {
                    elements.each(function () {
                        $(this).trigger("appear");
                    });
                }
            });
        }

        /* Force initial check if images should appear. */
        $(document).ready(function () {
            update();
        });

        return this;
    };

    /* Convenience methods in jQuery namespace.           */
    /* Use as  $.belowthefold(element, {threshold : 100, container : window}) */

    $.belowthefold = function (element, settings) {
        var fold;

        if (settings.container === undefined || settings.container === window) {
            fold = (window.innerHeight ? window.innerHeight : $window.height()) + $window.scrollTop();
        } else {
            fold = elementTop($(settings.container)) + $(settings.container).height();
        }

        return fold <= elementTop($(element)) - settings.threshold;
    };

    $.rightoffold = function (element, settings) {
        var fold;

        if (settings.container === undefined || settings.container === window) {
            fold = (window.innerWidth || $window.width()) + $window.scrollLeft();
        } else {
            fold = elementLeft($(settings.container)) + $(settings.container).width();
        }

        return fold <= elementLeft($(element)) - settings.threshold;
    };

    $.abovethetop = function (element, settings) {
        var fold;

        if (settings.container === undefined || settings.container === window) {
            fold = $window.scrollTop();
        } else {
            fold = elementTop($(settings.container));
        }

        return fold >= elementTop($(element)) + settings.threshold + $(element).height();
    };

    $.leftofbegin = function (element, settings) {
        var fold;

        if (settings.container === undefined || settings.container === window) {
            fold = $window.scrollLeft();
        } else {
            fold = elementLeft($(settings.container));
        }

        return fold >= elementLeft($(element)) + settings.threshold + $(element).width();
    };

    $.inviewport = function (element, settings) {
        return !$.rightoffold(element, settings) && !$.leftofbegin(element, settings) && !$.belowthefold(element, settings) && !$.abovethetop(element, settings);
    };

    /* Custom selectors for your convenience.   */
    /* Use as $("img:below-the-fold").something() or */
    /* $("img").filter(":below-the-fold").something() which is faster */

    $.extend($.expr[":"], {
        "below-the-fold": function (a) {
            return $.belowthefold(a, {threshold: 0});
        },
        "above-the-top": function (a) {
            return !$.belowthefold(a, {threshold: 0});
        },
        "right-of-screen": function (a) {
            return $.rightoffold(a, {threshold: 0});
        },
        "left-of-screen": function (a) {
            return !$.rightoffold(a, {threshold: 0});
        },
        "in-viewport": function (a) {
            return $.inviewport(a, {threshold: 0});
        },
        /* Maintain BC for couple of versions. */
        "above-the-fold": function (a) {
            return !$.belowthefold(a, {threshold: 0});
        },
        "right-of-fold": function (a) {
            return $.rightoffold(a, {threshold: 0});
        },
        "left-of-fold": function (a) {
            return !$.rightoffold(a, {threshold: 0});
        }
    });

})(jQuery, window, document);
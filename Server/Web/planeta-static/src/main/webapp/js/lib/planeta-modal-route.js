/*global Backbone, jQuery, _ */

(function ($) {
    'use strict';

    var ModalState = new Backbone.Model({
        last: ''
    });


    var ModalsRoute = Backbone.Router.extend({

        routes: {
            "": "modalClose",
            "modal-:page": "modalOpen",
            "modal-:page?:getParam": "modalOpen"
        },

        initialize: function(){
            Backbone.history.start();
        },

        modalOpen: function(page, getParam) {
            ModalState.set('getParam', getParam);
            var self = this;
            this.modalClose();

            ModalState.set('last', page);
            page = 'modal-' + page;
            var $page = $('#' + page);


            if ( !ModalState.get(page) ) {
                ModalState.set(page, true);

                if ( !$page.length ) {
                    $.get('/modals/' + page + '.php', getParam, function (data) {
                        $('body').append(data);
                        modalShow(page);

                        uiSelect();
                    });
                    return;
                }
            }

            modalShow(page);


            function modalShow(page) {
                $page = $('#' + page);
                $page.modal('show');
                $page.one('hidden.pln.modal', function (e, param) {
                    ModalState.set('getParam', '');
                    if ( !!param ) return;
                    self.navigate('', {trigger: true});
                });

                $(document).on('keydown.dismiss.pln.modal', function (e) {
                    if ( e.which == 27 ) {
                        $(document).off('keydown.dismiss.pln.modal');
                        self.navigate('', {trigger: true});
                    }
                })

            }
        },

        modalClose: function() {
            var last = ModalState.get('last');
            if (last !== '') {
                $('#modal-' + last).modal('hide', null, true);
                ModalState.set('last', '');
            }
        }

    });

    $(function () {
        new ModalsRoute();
    });

})(jQuery);

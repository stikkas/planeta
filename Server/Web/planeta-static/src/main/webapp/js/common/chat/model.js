/*global Chat,workspace,confirm,BaseModel,JobManager,Backbone,BaseCollection,_*/
/**
 * Chat models
 */

var Chat = {
    Models: {},
    Views: {},
    MAX_LENGTH: 200
};

/**
 * Chat model
 */
Chat.Models.Chat = BaseModel.extend({

    url: '/api/profile/join-chat.json',
    CHAT_INFO_REFRESH_DELAY: 15000, // 15 seconds
    CHAT_INFO_REFRESH_PERIOD: 120000, // 120 seconds
    CHAT_MESSAGES_LOAD_DELAY: 10000, // 10 seconds
    CHAT_MESSAGES_LOAD_PERIOD: 5000, // 5 seconds

    defaults: {
        profileId: 0,
        ownerObjectId: 0,
        ownerObjectType: 'BROADCAST'
    },

    fetchOptions: {
        ignoreErrors: true
    },

    initialize: function (options) {
        BaseModel.prototype.initialize.call(this, options);
        this.set(this.parse(options));

        // chat info loader (starts after 15 seconds and runs every 2 minutes)
        JobManager.schedule('chat-info-loader', this.refreshChatInfo, this.CHAT_INFO_REFRESH_DELAY, this.CHAT_INFO_REFRESH_PERIOD, {
            context: this
        });

        // chat messages loader (starts after 10 seconds and runs every 5 seconds)
        JobManager.schedule('chat-messages-loader', this.loadLastMessages, this.CHAT_MESSAGES_LOAD_DELAY, this.CHAT_MESSAGES_LOAD_PERIOD, {
            context: this
        });
    },

    removeChatJobs: function(){
        JobManager.removeJob('chat-info-loader');
        JobManager.removeJob('chat-messages-loader');
    },

    fetch: function(options) {
        options = _.extend({
            data: {
                profileId: this.get('profileId'),
                ownerObjectId: this.get('ownerObjectId'),
                ownerObjectType: this.get('ownerObjectType')
            }
        }, options);
        BaseModel.prototype.fetch.call(this, options);
    },

    parse: function(response) {
        if (!response || !response.chat) {
            return {};
        }

        // setting chat main properties
        var chat = response.chat;
        chat.id = chat.chatId;
        chat.numOfUsers = response.numOfUsers;

        // Setting chat users
        if (response.chatUsers) {
            chat.users = response.chatUsers;
        }

        // Setting chat messages collection
        chat.messages = new Chat.Models.Messages([], {
            data: {
                profileId: response.chat.profileId,
                chatId: chat.chatId
            },
            limit: 50
        });

        return chat;
    },

    refreshChatInfo: function() {
        var self = this;
        var options = {
            context: this,
            data: {
                profileId: this.get('profileId'),
                ownerObjectId: this.get('ownerObjectId'),
                ownerObjectType: this.get('ownerObjectType')
            },
            ignoreErrors: true,
            success: function(response) {
                if (!response){
                    self.removeChatJobs();
                    $('#chat-container').hide();
                    return;
                }
                if (response.numOfUsers && response.chatUsers) {
                    self.set({numOfUsers : response.numOfUsers});
                    self.set({users: response.chatUsers});
                }
            }
        };

        Backbone.sync('read', this, options);
    },

    postMessage: function(messageText, success) {
        var self = this;
        if (this.isSending) {
            return;
        }
        this.isSending = true;
        var messages = this.get('messages');
        var messageTextInner = messageText.bbcode || messageText;
        var options = {
            url: '/api/profile/post-chat-message.json',
            context: this,
            data: {
                profileId: this.get('profileId'),
                chatId: this.get('chatId'),
                messageText: messageTextInner,
                startMessageId: messages.getLastMessageId()
            },
            ignoreErrors: true,
            success: function(response) {
                self.isSending = false;
                if (response.success) {
                    messages.add(response.result);

                    if (success) {
                        success(response.result);
                    }
                } else {
                    workspace.appView.showErrorMessage(response.errorMessage);
                }
            },
            error: function(response) {
                self.isSending = false;
            }
        };

        Backbone.sync('update', this, options);
    },

    deleteMessage: function(model) {
        var options = {
            url: '/api/profile/deleteByProfileId-chat-message.json',
            data: {
                profileId: model.get('profileId'),
                chatId: model.get('chatId'),
                messageId: model.get('messageId')
            },
            ignoreErrors: true,
            context: this,
            success: function (response) {
                if (response.success) {
                    var messages = this.get('messages');
                    var index = messages.indexOf(model);
                    var next = messages.at(index + 1);
                    messages.remove(model);
                    if (next) {
                        next.trigger('change');
                    }
                } else {
                    workspace.appView.showErrorMessage(response.errorMessage);
                }
            }
        };

        Backbone.sync('delete', model, options);
    },

    banUser: function(model) {
        var options = {
            url: '/api/profile/ban-chat-user.json',
            data: {
                profileId: model.get('profileId'),
                userId: model.get('authorProfileId'),
                chatId: model.get('chatId')
            },
            context: this,
            success: function (response) {
                if (response.success === true) {
                    workspace.appView.showSuccessMessage("Юзер забанен на 30 минут!");
                } else {
                    workspace.appView.showErrorMessage(response.errorMessage);
                }
            }
        };

        Backbone.sync('update', this, options);
    },

    loadNextMessages: function(scrollTo) {
        try {
            var messages = this.get('messages');
            messages.loadNext();
        } catch (err) {

        }
    },

    loadLastMessages: function() {
        try {
            var messages = this.get('messages');
            messages.loadLast();
        } catch (err) {

        }

    },

    leaveChat: function() {
        var options = {
            url: '/api/profile/leave-chat.json',
            data: {
                profileId: this.get('profileId'),
                chatId: this.get('chatId')
            },
            success: function(response) {
                if (!response || !response.success) {
                    workspace.appView.showErrorMessage(response.errorMessage);
                }
            }
        };

        if (!this.get('chatId') || !this.get('profileId')) {
            workspace.appView.showErrorMessage("Чат недоступен. Попробуйте перезагрузить страницу");
            return;
        }

        Backbone.sync('update', this, options);
    }

});

/**
 * Chat message model
 */
Chat.Models.Message = BaseModel.extend({

    initialize: function(options) {
        BaseModel.prototype.initialize.call(this, options);
        this.set({
            id: options.messageId
        });
    }
});

/**
 * Collection for holding chat messages
 */
Chat.Models.Messages = BaseCollection.extend({

    url: '/api/profile/chat-messages.json',
    loading: false,
    model: Chat.Models.Message,

    fetchOptions: {
        ignoreErrors: true
    },

    comparator: function(messageOne, messageTwo) {
        return messageOne.get('messageId') < messageTwo.get('messageId') ? -1 : 1;
    },

    getLastMessageId: function() {
        return this.last() == null ? 0 : this.last().get('messageId');
    },

    loadLast: function(options) {
        if (this.loading) {
            return;
        }
        this.loading = true;

        if (this.length === 0) {
            this.loadNext();
            return;
        }

        this.url = '/api/profile/chat-last-messages.json';
        var self = this;
        var data = _.extend({startMessageId: this.getLastMessageId()}, this.data);
        this.fetch({
            data: data,
            add: true,
            success: function(collection) {
                if (self.length > Chat.MAX_LENGTH) {
                    for (var i = self.length; i >= Chat.MAX_LENGTH; i--) {
                        // Removing first element because list view is ordered desc
                        var toRemove = self.at(0);
                        self.remove(toRemove);
                    }
                }
                self.loading = false;
                if (options && options.success) {
                    options.success(collection);
                }
            },
            error: function() {
                self.loading = false;
            }
        });
    }

});

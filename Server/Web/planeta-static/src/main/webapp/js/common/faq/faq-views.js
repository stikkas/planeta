/*global FAQ, BaseRichView, FeedbackHelper, Modal, loadModule, News, TinyMcePlaneta, tinymce*/
FAQ.Views.Main = BaseView.extend({
    template: '#main-faq-template',
    construct: function () {
        this.addChildAtElement('.js-container', new FAQ.Views.CategoryList({
            collection: this.model.faqCategories
        }));
    }
});

FAQ.Views.Category = BaseView.extend({
    className: "faq-main_col-i",
    template: '#main-faq-category-template',
    construct: function () {
        var collection = new FAQ.Models.ArticleCollection();
        collection.load({
            data: {
                faqCategoryId: this.model.get('faqCategoryId')
            }
        });
        this.addChildAtElement('.js-faq-article-list', new FAQ.Views.ArticleList({
                collection: collection
            })
        );
        this.articleCollection = collection;
    },

    onAddCategoryClicked: function (e) {
        e.preventDefault();

        var view = new FAQ.Views.EditArticle({
            model: new BaseModel({
                faqArticleId: 0,
                faqCategoryId: this.model.get('faqCategoryId')
            }),
            articleCollection: this.articleCollection
        });
        view.render();
    }
});

FAQ.Views.CategoryList = BaseListView.extend({
    className: "faq-main_block",
    itemViewType: FAQ.Views.Category
});

FAQ.Views.ArticleListItem = BaseView.extend({
    template: '#faq-list-item-template',
    className: 'js-article-list-item',

    afterRender: function () {
        this.$el.data('model', this.model);
    },

    onArticleCollapsedClicked: function () {
        this.model.set('folded', !this.model.get('folded'));

        if (!this.paragraphListView) {
            this.paragraphListView = new FAQ.Views.ParagraphList({
                collection: new FAQ.Models.ParagraphCollection([], {
                    data: {
                        faqArticleId: this.model.get('faqArticleId')
                    }
                })
            });
            this.addChildAtElement('.js-faq-paragraph-list', this.paragraphListView);
            this.paragraphListView.loadNext();
        }
    },

    onDeleteArticleClicked: function (e) {
        e.stopPropagation();
        FAQ.Views.DeleteArticle(this.model);
    },

    onEditArticleClicked: function (e) {
        e.stopPropagation();
        var view = new FAQ.Views.EditArticle({model: this.model});
        view.render();
    },

    onAddParagraphClicked: function (e) {
        e.preventDefault();

        var model = new BaseModel({
            title: '',
            textHtml: '',
            faqArticleId: this.model.get('faqArticleId'),
            faqParagraphId: 0
        });
        var edit = new FAQ.Views.ParagraphEdit({model: model, collection: this.paragraphListView.collection});
        edit.render();
    }
});

FAQ.Views.ArticleList = BaseListView.extend({
    itemViewType: FAQ.Views.ArticleListItem,
    afterRender: function () {
        var self = this;
        var collection = this.collection;
        this.$el.sortable({
            items: ".js-article-list-item",
            axis: 'y',
            update: function (event, ui) {
                var draggedModel = ui.item.data('model');
                self.resort({
                    idTo: collection.at(ui.item.index()).get('faqArticleId'),
                    idFrom: draggedModel.get('faqArticleId'),
                    faqCategoryId: draggedModel.get('faqCategoryId'),
                    draggedModel: draggedModel,
                    indexTo: ui.item.index()
                });
            }
        });
    },
    resort: function (options) {
        var self = this;

        function fail() {
            workspace.appView.showMessageUnexpectedError();
            self.onReset();
        }

        Backbone.sync('update', null, {
            url: '/admin/faq-edit/resort-article.json',
            data: {
                idFrom: options.idFrom,
                idTo: options.idTo,
                faqCategoryId: options.faqCategoryId
            }
        }).done(function (response) {
            if (response && response.success) {
                self.collection.remove(options.draggedModel, {silent: true});
                self.collection.add(options.draggedModel, {at: options.indexTo, silent: true});
                self.onReset();
            }else{
                fail();
            }

        }).fail(fail);

    }

});

FAQ.Views.ParagraphListItem = BaseView.extend({
    template: '#faq-paragraph-list-item-template',
    className: 'js-paragraph-list-item',

    afterRender: function () {
        this.$el.data('model', this.model);
    },
    onEditParagraphClicked: function (e) {
        e.stopPropagation();
        var edit = new FAQ.Views.ParagraphEdit({model: this.model});
        edit.render();
    },
    onDeleteParagraphClicked: function (e) {
        e.stopPropagation();
        FAQ.Views.DeleteParagraph(this.model);
    }

});

FAQ.Views.ParagraphList = BaseListView.extend({
    pagerLoadingTemplate: '#pager-loading-template',
    itemViewType: FAQ.Views.ParagraphListItem,
    afterRender: function () {
        var self = this;
        var collection = this.collection;
        this.$el.sortable({
            items: ".js-paragraph-list-item",
            axis: 'y',
            update: function (event, ui) {
                var draggedModel = ui.item.data('model');
                self.resort({
                    idTo: collection.at(ui.item.index()).get('faqParagraphId'),
                    idFrom: draggedModel.get('faqParagraphId'),
                    faqArticleId: draggedModel.get('faqArticleId'),
                    draggedModel: draggedModel,
                    indexTo: ui.item.index()
                });
            }
        });
    },
    resort: function (options) {
        var self = this;

        function fail() {
            workspace.appView.showMessageUnexpectedError();
            self.onReset();
        }

        Backbone.sync('update', null, {
            url: '/admin/faq-edit/resort-paragraph.json',
            data: {
                idFrom: options.idFrom,
                idTo: options.idTo,
                faqArticleId: options.faqArticleId
            }
        }).done(function (response) {
            if (response && response.success) {
                self.collection.remove(options.draggedModel, {silent: true});
                self.collection.add(options.draggedModel, {at: options.indexTo, silent: true});
                self.onReset();
            }else{
                fail();
            }

        }).fail(fail);

    }
});


FAQ.Views.SearchHeaderWithBreadCrumbs = BaseView.extend({
    template: '#faq-full-header',
    modelEvents: {
        'destroy': 'dispose'
    },
    construct: function () {
        this.addChildAtElement('.js-search-bar', new FAQ.Views.SearchHeader({
            model: this.model
        }));
        this.addChildAtElement('.js-bread-crumbs', new FAQ.Views.BreadCrumbs({
            model: this.model
        }));
    }
});

FAQ.Views.SearchHeader = BaseView.extend({
    template: '#faq-search-bar-template',
    modelEvents: {
        'destroy': 'dispose',
        'change:query': 'afterRender'
    },
    events: {
        'keydown .faq-search_input': 'onKeyDown'
    },

    onKeyDown: _.debounce(function () {
        var query = this.$('#query').val();
        if (this.model && this.model.search && _.isFunction(this.model.search)) {

            this.model.search(query);
        }
    }, 500),
    afterRender: function() {
        this.$el.find('input[id="query"]').focus();
    }
});
FAQ.Views.BreadCrumbs = BaseView.extend({
    template: '#faq-breadcrumbs-template'
});

FAQ.Views.SnippetList = BaseListView.extend({
    emptyListTemplate: '#campaign-search-results-empty-container-template'
});

FAQ.Views.Snippet = BaseView.extend({
    template: '#faq-search-snippet-template',

    beforeRender: function () {
        this.$el.hide();
    },

    afterRender: function () {
        this.$el.fadeIn(500);
    }
});

FAQ.Views.SearchResults = BaseView.extend({
    template: '#faq-search-template',
    construct: function () {
        this.addChildAtElement('.js-snippets', new FAQ.Views.SnippetList({
            itemViewType: FAQ.Views.Snippet,
            collection: this.model.snippets
        }));
    }
});
FAQ.Views.Article = BaseRichView.extend({
    template: '#faq-article-template',
    events: {
        'click [href*=mailto:support@planeta.ru]': 'onMailToClick'
    },

    initialize: function (options) {
        BaseRichView.prototype.initialize.call(this, options);
        if (this.controller) {
            this.controller.pageData = this.pageData();
        }
    },

    pageData: function () {
        var title = StringUtils.escapeHtml(this.model.get('faqArticle').title || '');
        return {
            title: title
        };
    },

    onMailToClick: function (e) {
        FeedbackHelper.showFeedBackDialog();
        e.preventDefault();
    }

});


FAQ.EditSync = function (url, data, view, isAdd) {
    Modal.startLongOperation(500);
    function fail() {
        Modal.finishLongOperation();
        workspace.appView.showMessageUnexpectedError();
        view.render();
    }

    Backbone.sync('update', null, {
        url: url,
        data: data
    }).done(function (response) {
        if (response && response.success) {
            view.dispose();
            view.model.set(response.result);
            if (isAdd && view.collection) {
                view.collection.add(view.model);
            }
            Modal.finishLongOperation();
        } else {
            fail();
        }
    }).fail(fail);
};

FAQ.Views.EditArticle = Modal.View.extend({
    template: "#faq-edit-article-template",
    construct: function (options) {
        this.collection = options.articleCollection;
    },
    save: function (e) {
        e.preventDefault();
        var form = $("form", this.el);
        var data = _.extend({
            faqArticleId: this.model.get('faqArticleId'),
            faqCategoryId: this.model.get('faqCategoryId')
        }, form.serializeObject());


        FAQ.EditSync('/admin/faq-edit/edit-article.json', data, this, !this.model.get('faqArticleId'));
    }
});

FAQ.Views.DeleteArticle = function (model) {
    function fail() {
        workspace.appView.showMessageUnexpectedError();
    }

    Modal.showConfirmEx({
        text: 'Вы действительно хотите удалить?',
        success: function () {
            Backbone.sync("update", null, {
                data: {faqArticleId: model.get('faqArticleId')},
                url: '/admin/faq-edit/deleteByProfileId-article.json'
            }).done(function (response) {
                if (response && response.success) {
                    model.collection.remove(model);
                } else {
                    fail();
                }
            }).fail(fail);
        }
    });
};

FAQ.Views.DeleteParagraph = function (model) {
    function fail() {
        workspace.appView.showMessageUnexpectedError();
    }

    Modal.showConfirmEx({
        text: 'Вы действительно хотите удалить?',
        success: function () {
            Backbone.sync("update", null, {
                data: {faqParagraphId: model.get('faqParagraphId')},
                url: '/admin/faq-edit/deleteByProfileId-paragraph.json'
            }).done(function (response) {
                if (response && response.success) {
                    model.collection.remove(model);
                } else {
                    fail();
                }
            }).fail(fail);
        }
    });
};


FAQ.Views.ParagraphEdit = Modal.OverlappedView.extend({
    template: '#faq-edit-paragraph-modal-template',
    construct: function (options) {
        this.collection = options.collection;
    },

    afterRender: function () {
        this.$el.closest(".modal").css('max-width', 'none');
        var self = this;
        loadModule('tiny-mce-campaigns').done(function () {
            self.initTiny();
        });
    },

    initTiny: function () {
        var TinyModel = function () {
            _.extend(this, new TinyMcePlaneta.EditorModel(), {
                config: TinyMcePlaneta.getFaqEditorConfiguration(),
                width: 940,
                height: 400,
                destructPlugins: function () {
                    if (tinymce.activeEditor.plugins.planetafullscreenfaqedit.fullScreen) {
                        tinymce.activeEditor.execCommand("planetafullscreenfaqedit");
                    }
                }
            });
        };

        this.tinyModel = new TinyModel();
        var $element = this.$('.js-tiny-mce-faq-edit');
        $element.attr('id', 'ParagraphEdit' + this.cid);
        this.tinyModel.init($element);
    },

    save: function () {
        var data = this.$('form').serializeObject();
        data.textHtml = this.tinyModel.html();
        if (data.textHtml) {
            FAQ.EditSync('/admin/faq-edit/edit-paragraph.json', data, this, !this.model.get('faqParagraphId'));
        } else {
            console.log("No data");
        }
    }
});
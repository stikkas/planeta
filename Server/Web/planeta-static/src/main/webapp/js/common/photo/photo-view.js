/*globals Photo, Comments, UploadController, swfobject, Html5Fullscreen, ProfileUtils, PrivacyUtils, Form, Dialogs, Attach, Modal */

Photo.Views = {
    showPhoto: function (profileAlias, photoId, self, options) {
        options = options || {};
        photoId = parseFloat(photoId) || photoId;
        var activePhoto = {
            profileId: profileAlias,
            photoId: photoId
        };
        var collection = [activePhoto];
        var index = 0;
        if (!isNaN(parseFloat(profileAlias)) && isFinite(profileAlias) && self) {
            collection = $(self).closest('.image-attachments').find('a[onclick^="javascript:workspace.showPhoto"]').map(function (i) {
                //noinspection JSLint
                var ids = (/.*showPhoto\((['"])([^'"]*)\1\s*,\s*(['"])(\d+)\3/im).exec($(this).attr('onclick'));
                if (!ids || ids.length < 5) {
                    return;
                }
                var imageId = parseFloat(ids[4]);
                if (imageId == photoId) {
                    index = i;
                }
                var imageProfileId = parseFloat(ids[2]);
                if (imageProfileId != profileAlias) {
                    console.log('bad params for showPhoto: ' + $(self).closest('.image-attachments').html());
                }
                return {
                    profileId: profileAlias,
                    photoId: imageId
                };
            }).toArray();
        }
        var model = new Photo.Models.PhotoModal({
            profileId: profileAlias,
            photos: collection,
            photo: activePhoto,
            activeIndex: index
        });

        Modal.showDialogByViewClass(Photo.Views.PhotoModal, model, options.error, null, null, function () {
            if (options.changePageData) {
                workspace.appView.changePageData(model);
            }
        });
    },

    showPhotoCollection: function (id, owner, collection, activeIndex, fullscreen) {
        var activePhoto = {
            profileId: owner,
            photoId: id
        };
        var myCollection = [];
        _.each(collection, function (value, index) {
            myCollection.push({
                photoId: value.id,
                profileId: value.owner
            });
        });
        var model = new Photo.Models.PhotoModal({
            profileId: owner,
            photos: myCollection,
            photo: activePhoto,
            activeIndex: activeIndex,
            fullscreen: fullscreen
        });

        Modal.showDialogByViewClass(Photo.Views.PhotoModal, model);
    }
};

Photo.Views.PreCachedPhoto = BaseView.extend({

    modelEvents: {
        'destroy': 'dispose'
    },

    resize: function () {
        var $photoBlock = $('.photo-layer .photo-block');
        this.$el.height($photoBlock.height());
        var self = this;
        this.$('img').each(function () {
            var $img = $(this);
            var marginTop = self.getTopMargin($img);
            $img.css({marginTop: marginTop, textAlign: 'center'});
        });
    },

    construct: function () {
        var self = this;
        $(window).resize(function () {
            self.resize();
        });
        this.newImage();
        // to calculate position before inserting in dom
        // define image width
        var $photoBlock = $('.photo-layer .photo-block');
        this.$el.width($photoBlock.width()).css({textAlign: 'center'});
        this.resize();
        this.$el.width('inherit');
    },

    newImage: function () {
        var $newImg = this.model.currentPhoto.get('loadedImg') || $('<img>', {
                src: this.model.currentPhoto.get('loadedImgSrc')
            });
        $newImg.addClass('photo-cover');
        var photos = this.model.get('photos');
        if (!photos || photos.length < 2) {
            $newImg.addClass('photo-no-pointer');
        }
        var marginTop = this.getTopMargin($newImg);
        $newImg.css({marginTop: marginTop}).hide();
        $newImg.appendTo(this.$el);
        return $newImg;
    },

    renderAsync: function () {
        var model = this.model;
        var $images = this.$('img');
        var $oldImg = this.$('img:visible');
        var $newImg = $images.filter(function () {
            return this.src === model.currentPhoto.get('loadedImgSrc');
        });
        if ($newImg.length === 0) {
            $newImg = this.newImage();
        }
        // redrawing large images can be slow - no matter show-than-hide or hide-than-show
        $oldImg.hide();
        $newImg.show();
    },

    getTopMargin: function ($el) {
        if (!$el || !$el.length || !$el[0].height || !$el[0].width) {
            return 0;
        }
        var height = $el[0].height;
        var ratio = $el[0].width / this.$el.width();
        if (ratio > 1) {
            height = height / ratio;
        }
        var diff = (this.$el.height() - height) / 2;
        $el.removeAttr('height').removeAttr('width');
        return (diff < 0) ? 0 : diff;
    }
});

Photo.Views.PhotoModal = Modal.OverlappedView.extend({
    template: '#photo-view-template',
    clickOff: true,
    className: '',

    events: {
        'click .photo-cover': 'showNextPhoto',
        'click div.photo-prev': 'showPrevPhoto',
        'click div.photo-next': 'showNextPhoto',
        'click .photo-fullscreen': 'toggleFullScreen',
        'click a.album-link': 'lightClose',
        'focus textarea': 'checkNotAnonymous',
        'click textarea': 'checkNotAnonymous'
    },

    construct: function () {
        this.enableHtml5FullScreen = true;
    },

    beforeRender: function () {
        Modal.OverlappedView.prototype.beforeRender.call(this);
        this.unscrollPaned = true;
        this.$('.scrollbar-pane').unscrollpan();
    },

    afterRender: function () {
        var self = this;
        Modal.OverlappedView.prototype.afterRender.call(this);
        workspace.stats.trackPhotoView(this.model.currentPhoto);

        if (!this.imageView) {
            // don't append this view in construct to prevent its renderAsync before image margin is calculated
            this.imageView = this.addChildAtElement('.js-photos-no-change', new Photo.Views.PreCachedPhoto({
                model: this.model
            }), true);
            this.imageView.resize();
        } else {
            this.imageView.resize();
        }

        this.$('.photo-cover').clickOff(function (e) {
            self.cancel(e);
        }, {
            selector: '.photo-fullscreen',
            once: true
        });
    },

    updateScroll: function () {
        if (this.unscrollPaned) {  //this to avoid async collision
            this.unscrollPaned = false;

            this.$('.scrollbar-pane').scrollpan({
                scroll: 100, // in %
                precision: 5,   //setup presicion of handler positioning.
                elastic: true
            });

            $.updateScrollItems();
        }
    },

    lightClose: function (e) {
        this.dispose();
    },
    showPrevPhoto: function (e) {
        this.showPhoto(e, -1);
    },
    showNextPhoto: function (e) {
        this.showPhoto(e, 1);
    },

    showPhoto: function (e, step) {
        e.preventDefault();
        e.stopPropagation();
        if (this.model.photos.length > 1) {
            var activeItemIndex = this.model.get('activeItemIndex') || 0;
            activeItemIndex += step;
            activeItemIndex = activeItemIndex > this.model.photos.length - 1 ? 0 : activeItemIndex;
            activeItemIndex = activeItemIndex < 0 ? this.model.photos.length - 1 : activeItemIndex;
            this.model.setPhotoIndex(activeItemIndex);
        }
    },

    checkCommandKeys: function (event) {
        switch (event.keyCode) {
            case $.ui.keyCode.ESCAPE:
                this.cancel(event);
                break;
            case $.ui.keyCode.LEFT:
                this.showPrevPhoto(event);
                break;
            case $.ui.keyCode.RIGHT:
                this.showNextPhoto(event);
                break;
        }
    },

    toggleFullScreen: function () {
        if (this.enableHtml5FullScreen) {
            if (this.model.get('fullscreen')) {
                Html5Fullscreen.cancel();
            } else {
                Html5Fullscreen.request();
            }
        }
        //this.$('.photo-layer').toggleClass('fullscreen');
        this.model.set({
            fullscreen: !this.model.get('fullscreen')
        }, {silent: true});
        var self = this;
        _.delay(function () {
            var icon = self.$('.photo-fullscreen > i');
            icon.toggleClass('icon-resize-small icon-resize-full');
            var $img = self.imageView.$('img:visible');
            var marginTop = self.imageView.getTopMargin($img);
            $img.addClass('animated').css({"marginTop": marginTop});
            _.delay(function () {
                $img.removeClass('animated');
            }, Photo.FULL_SCREEN_CSS_EASING_DELAY);
        }, Photo.FULL_SCREEN_CSS_EASING_DELAY);
    },

    dispose: function () {
        this.$('.scrollbar-pane').unscrollpan();
        Html5Fullscreen.cancel();
        this.$('.photo-cover').clickOff('remove');
        Modal.OverlappedView.prototype.dispose.call(this);
        $(window).unbind('.' + this.cid);
        $('body .photo-view-sharing').remove();
    },
    checkNotAnonymous: function () {
        if (LazyHeader.checkNotAnonymous()) {
            return true;
        }
        this.dispose();
        return false;
    }
});


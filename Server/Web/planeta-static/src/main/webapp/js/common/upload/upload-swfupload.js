/**
 * Model for several files upload
 */
var UploadModel = BaseModel.extend({

    defaults:{
        title:'Загрузка файла',
        text:'Это текст с описанием этой загрузки. Его нужно поменять в модели.',
        successCallback:null,
        autoSaveResult:false,
        saveEnabled:false,
        uploadPath:'/uploadImage',
        previewEnabled:false,
        uploadResult:null,
        uploadSettings:{
            flash_url:"/flash/swfupload.swf",
            file_post_name:'userfile1',
            post_params:{
                clientId:0,
                ownerId:0
            },
            file_size_limit:'10 MB',
            file_types:'*.png;*.jpg;*.jpeg;*.gif',
            file_types_description:'Image files',
            file_upload_limit:1,
            file_queue_limit:0,
            debug:false,
            customSettings:{},

            // Button settings
            button_image_url:'',
            button_width:"101",
            button_height:"26",
            button_placeholder_id:"spanButtonPlaceHolder",
            button_text:'<span class="theFont"></span>',
            button_text_style:".theFont { color: #ffffff; font-size: 0px; font-family: Arial; }",
            button_text_left_padding:12,
            button_text_top_padding:3
        }
    },

    initialize:function (options) {
        BaseModel.prototype.initialize.call(this, options);

        // Setting handlers functions
        var settings = _.clone(this.defaults.uploadSettings);

        // Binding all methods to this object context
        _.bindAll(this);
        settings.file_queued_handler = this.fileQueuedHandler;
        settings.file_queue_error_handler = this.fileQueueErrorHandler;
        settings.file_dialog_complete_handler = this.fileDialogCompleteHandler;
        settings.upload_start_handler = this.uploadStartHandler;
        settings.upload_progress_handler = this.uploadProgressHandler;
        settings.upload_error_handler = this.uploadErrorHandler;
        settings.upload_success_handler = this.uploadSuccessHandler;
        settings.upload_complete_handler = this.uploadCompleteHandler;
        settings.queue_complete_handler = this.queueCompleteHandler;
        settings.button_image_url = workspace.staticNodesService.getResourceUrl("/images/TestImageNoText_101x26.png");

        // Overriding settings fields with properties passed in constructor
        if (options && options.uploadSettings) {
            for (var property in options.uploadSettings) {
                settings[property] = options.uploadSettings[property];
            }
        }

        // Setting new upload settings
        this.set({
            uploadSettings:settings,
            filesInProgress:new BaseCollection()
        });
    },

    addUpload:function (file) {
        // Creating new upload file model
        // and adding it to "filesInProgress" collection
        var fileUploadModel = new FileUploadModel({
            fileName:file.name,
            fileSize:file.size,
            uploadedSize:0,
            status:FileUploadStatuses.PENDING
        });
        fileUploadModel.id = file.id;
        this.get('filesInProgress').add(fileUploadModel);

    },

    startUpload:function (file) {
        var fileUploadModel = this.get('filesInProgress').get(file.id);
        fileUploadModel.set({
            status:FileUploadStatuses.STARTED
        });
    },

    setUploadStatus:function (file, uploadedSize) {
        var fileUploadModel = this.get('filesInProgress').get(file.id);
        fileUploadModel.set({
            uploadedSize:uploadedSize
        });
    },

    setUploadError:function (file, errorMessage) {
        var fileUploadModel = this.get('filesInProgress').get(file.id);
        fileUploadModel.set({
            status:FileUploadStatuses.ERROR,
            errorMessage:errorMessage
        });
    },

    setUploadCanceled:function (file) {
        var fileUploadModel = this.get('filesInProgress').get(file.id);
        fileUploadModel.set({
            status:FileUploadStatuses.CANCELED
        });
    },

    setUploadCompleted:function (file, uploadResult) {
        var fileUploadModel = this.get('filesInProgress').get(file.id);
        fileUploadModel.set({
            status:FileUploadStatuses.FINISHED,
            statusDescription:'Файл загружен',
            uploadResult:uploadResult
        });
    },

    setUploadPreCompleted:function (file) {
        var fileUploadModel = this.get('filesInProgress').get(file.id);
        fileUploadModel.set({
            statusDescription:'Файл обрабатывается, пожалуйста, подождите'
        });
    },

    /**
     * Called when file is added to swfupload.
     * We create new fileuploadmodel on this step.
     */
    fileQueuedHandler:function (file) {
        try {
            this.addUpload(file);
        } catch (ex) {
            workspace.appView.showErrorMessage("Произошла ошибка:" + ex);
        }
        return true;
    },

    /**
     * Called when there is an error adding file to swfupload queue.
     */
    fileQueueErrorHandler:function (file, errorCode, message) {
        try {
            if (errorCode === SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED) {
                workspace.appView.showErrorMessage("Вы пытаетесь закачать слишком много файлов.\n" + (message === 0 ? "Достигнут лимит закачиваемых файлов." : "Вы можете выбрать " + (message > 1 ? "до " + message + " " + StringUtils.declOfNum(message, ['файл', 'файла', 'файлов']) + "." : "только один файл.")));
                return;
            }

            switch (errorCode) {
                case SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT:
                    workspace.appView.showErrorMessage("Вы пытаетесь закачать слишком большой фаил: " + file.name);
                    //TODO: console.log("Error Code: File too big, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
                    break;
                case SWFUpload.QUEUE_ERROR.ZERO_BYTE_FILE:
                    workspace.appView.showErrorMessage("Вы пытаетесь закачать пустой фаил: " + file.name);
                    //TODO: console.log("Error Code: Zero byte file, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
                    break;
                case SWFUpload.QUEUE_ERROR.INVALID_FILETYPE:
                    workspace.appView.showErrorMessage("Вы пытаетесь закачать фаил неподходящего типа: " + file.name);
                    //TODO: console.log("Error Code: Invalid File Type, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
                    break;
                default:
                    workspace.appView.showErrorMessage("Произошла ошибка при загрузке фаила: " + file.name);
                    //TODO: console.log("Error Code: " + errorCode + ", File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
                    break;
            }
        } catch (ex) {
            workspace.appView.showErrorMessage("Произошла ошибка: " + ex);
            //TODO: console.log(ex);
        }
    },

    fileDialogCompleteHandler:function (numFilesSelected, numFilesQueued) {
        try {
            /* I want auto start the upload and I can do that here */
            this.trigger('startUpload');
        } catch (ex) {
            workspace.appView.showErrorMessage("Произошла ошибка:" + ex);
            //TODO: console.log(ex);
        }
    },

    /**
     * Called when swfupload has begun to load specified file.
     */
    uploadStartHandler:function (file) {
        try {
            this.startUpload(file);
            /**
             * I don't want to do any file validation or anything,  I'll just update the UI and
             * return true to indicate that the upload should start.
             * It's important to update the UI here because in Linux no uploadProgress events are called. The best
             * we can do is say we are uploading.
             */
        }
        catch (ex) {
            workspace.appView.showErrorMessage("Произошла ошибка:" + ex);
            //TODO: console.log(ex);
        }

        return true;
    },

    /**
     * File upload progress has changed
     */
    uploadProgressHandler:function (file, uploadedSize) {
        try {
            this.setUploadStatus(file, uploadedSize);
            if (uploadedSize == file.size) {
                this.setUploadPreCompleted(file);
            }
        } catch (ex) {
            workspace.appView.showErrorMessage("Произошла ошибка:" + ex);
            //TODO: console.log(ex);
        }
    },

    /**
     * Called on upload exception
     */
    uploadErrorHandler:function (file, errorCode, message) {
        try {
            this.setUploadError(file, message);

            switch (errorCode) {
                case SWFUpload.UPLOAD_ERROR.HTTP_ERROR:
                    workspace.appView.showErrorMessage("Произошла ошибка при загрузке фаила:" + file.name);
                    //TODO: console.log("Error Code: HTTP Error, File name: " + file.name + ", Message: " + message);
                    break;
                case SWFUpload.UPLOAD_ERROR.UPLOAD_FAILED:
                    workspace.appView.showErrorMessage("Произошла ошибка при загрузке фаила:" + file.name);
                    //TODO: console.log("Error Code: Upload Failed, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
                    break;
                case SWFUpload.UPLOAD_ERROR.IO_ERROR:
                    workspace.appView.showErrorMessage("Произошла ошибка при загрузке фаила:" + file.name);
                    //TODO: console.log("Error Code: IO Error, File name: " + file.name + ", Message: " + message);
                    break;
                case SWFUpload.UPLOAD_ERROR.SECURITY_ERROR:
                    workspace.appView.showErrorMessage("Произошла ошибка при загрузке фаила:" + file.name);
                    //TODO: console.log("Error Code: Security Error, File name: " + file.name + ", Message: " + message);
                    break;
                case SWFUpload.UPLOAD_ERROR.UPLOAD_LIMIT_EXCEEDED:
                    workspace.appView.showErrorMessage("Произошла ошибка при загрузке фаила:" + file.name);
                    //TODO: console.log("Error Code: Upload Limit Exceeded, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
                    break;
                case SWFUpload.UPLOAD_ERROR.FILE_VALIDATION_FAILED:
                    workspace.appView.showErrorMessage("Произошла ошибка при загрузке фаила:" + file.name);
                    //TODO: console.log("Error Code: File Validation Failed, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
                    break;
                case SWFUpload.UPLOAD_ERROR.FILE_CANCELLED:
                    workspace.appView.showErrorMessage("Произошла ошибка при загрузке фаила:" + file.name);
                    //TODO: console.log("Error Code: File cancelled, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
                    break;
                case SWFUpload.UPLOAD_ERROR.UPLOAD_STOPPED:
                    workspace.appView.showErrorMessage("Произошла ошибка при загрузке фаила:" + file.name);
                    //TODO: console.log("Error Code: Upload stopped, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
                    break;
                default:
                    workspace.appView.showErrorMessage("Произошла ошибка при загрузке фаила:" + file.name);
                    //TODO: console.log("Error Code: " + errorCode + ", File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
                    break;
            }
        } catch (ex) {
            workspace.appView.showErrorMessage("Произошла ошибка при загрузке фаила:" + ex);
        }
    },

    /**
     * Called on upload success
     */
    uploadSuccessHandler:function (file, data) {
        try {
            var response = jQuery.parseJSON(data);

            if (!response) {
                this.setUploadError(file, 'Response is empty');
                return;
            } else if (!response.success || !response.result) {
                this.setUploadError(file, response.errorMessage);
                return;
            }

            this.setUploadCompleted(file, response.result);
            this.set({
                saveEnabled:true,
                lastUploadResult:response.result
            });
        } catch (ex) {
            workspace.appView.showErrorMessage("Произошла ошибка:" + ex);
            //TODO: console.log(ex);
        }
    },

    uploadCompleteHandler:function (file) {
    },

    queueCompleteHandler:function (numFilesUploaded) {
        try {
            if (this.get('autoSaveResult')) {
                this.saveUploadResult();
            }
        } catch (ex) {
            workspace.appView.showErrorMessage("Произошла ошибка:" + ex);
            //TODO: console.log(ex);
        }
    },

    /**
     * Calls save callback and destroys model.
     */
    saveUploadResult:function () {
        var successCallback = this.get('successCallback');
        var filesUploaded = this.get('filesInProgress');
        this.destroy();
        if (successCallback) {
            successCallback(filesUploaded);
        }
    }
});

/**
 * View for one file upload progress bar
 */
var UploadProgressView = BaseView.extend({

    tagName:'div',
    className:'progressWrapper',
    template:'#upload-progress-template',

    events:{
        'click a.progressCancel':'cancelUpload'
    },

    cancelUpload:function (e) {
        e.preventDefault();
        alert('Пока не сделано');
    }
});

var UploadProgressListView = BaseListView.extend({
    anchor:'#divStatus',
    itemViewType:UploadProgressView
});

/**
 * View for the upload dialog window
 */
var UploadView = Modal.OverlappedView.extend({

    template:'#upload-form-template',
    previewTemplate:$('#upload-image-preview').template(),
    alternativeContentTemplate:$('#flash-alternative-content').template(),

    modelEvents:{
        'startUpload':'onStartUpload',
        'change:saveEnabled':'onSaveEnabledChanged',
        'change:lastUploadResult':'onUploadResultChanged',
        'destroy':'dispose'
    },

    initialize:function (options) {
        Modal.OverlappedView.prototype.initialize.call(this, options);
        this.addChild(new UploadProgressListView({
            collection:this.model.get('filesInProgress')
        }));
    },

    afterRender:function () {
        Modal.OverlappedView.prototype.afterRender.call(this);

        var uploadSettings = this.model.get('uploadSettings');

        var self = this;
        uploadSettings.upload_url = 'https://' + workspace.staticNodesService.getStaticNode() + self.model.get('uploadPath');
        self.swfUpload = new SWFUpload(uploadSettings);
        // show alternative content if flash player is too old or is not supported
        var swfUploadEl = self.swfUpload.getMovieElement();
        $(swfUploadEl).append($.tmpl(self.alternativeContentTemplate));
        return this;
    },

    onStartUpload:function () {
        this.swfUpload.startUpload();
    },

    onSaveEnabledChanged:function () {
        var saveEnabled = this.model.get('saveEnabled');
        var submitButton = $(this.el).find("button[type=submit]");
        if (saveEnabled) {
            submitButton.removeAttr("disabled");
        } else {
            submitButton.attr("disabled", true);
        }
    },

    onUploadResultChanged:function () {
        if (this.model.get('previewEnabled') == true) {
            $(this.el).find(".uploaded-preview").children().remove();
            $(this.el).find(".uploaded-preview").append($.tmpl(this.previewTemplate, this.model.get('lastUploadResult')));
        }
    },

    dispose:function () {
        if (this.swfUpload) {
            try {
                this.swfUpload.cancelQueue();
                this.swfUpload.cancelUpload();
            } catch (e) {
                // Ignoring exception
            }
        }
        if (this.swfUpload) {
            try {
                this.swfUpload.cleanUp();
            } catch (e) {
                // Ignoring exception
            }
        }
        if (SWFUpload) {
            _(SWFUpload.instances).each(function (instance) {
                try {
                    instance.destroy();
                } catch (e) {
                    // Ignoring exception
                }
            });
        }
        Modal.OverlappedView.prototype.dispose.call(this);
    },

    save:function (e) {
        e.preventDefault();
        this.model.saveUploadResult();
    }
});
/* File containing upload views and ma
 * */
var FileUploadStatuses = {
    IDLE: 'IDLE',
    PENDING: 'PENDING',
    STARTED: 'STARTED',
    FINISHED: 'FINISHED',
    SAVED: "SAVED",
    CANCELED: 'CANCELED',
    ERROR: 'ERROR',
    CROP: 'CROP'
};

var FileUploadImage = {
    IDLE: "",
    STARTED: "",
    PENDING: "",
    FINISHED: "/images/upload/processing.png",
    CANCELED: "",
    ERROR: "/images/upload/upload-error.png",
    SAVED: ""
};

var FileUploadMsg = {
    _dictionary:{
        "ru":{
            IDLE: "",
            STARTED: "Загрузка началась",
            PENDING: "Файл поставлен в очередь на загрузку",
            FINISHED: "Загрузка завершена.<br/> Файл обрабатывается на сервере.",
            CANCELED: "Загрузка отменена",
            ERROR: "При загрузке файла произошла ошибка",
            SAVED: "Файл сохранен на сервере."
        },
        "en":{
            IDLE: "",
            STARTED: "Uploading started",
            PENDING: "The file is queued for uploading",
            FINISHED: "Uploading is done.<br/> The file is processing on the server.",
            CANCELED: "Uploading cancelled",
            ERROR: "Error",
            SAVED: "Uploading is done."
        }
    },
    _translate: function (word, lang) {
        if (!lang)
            throw "language is empty.";
        return FileUploadMsg._dictionary[lang][word] || word;
    }
};

/**
 * Model for file uploading progress
 */
var FileUploadModel = BaseModel.extend({

    defaults: {
        fileName: '',
        fileSize: 0,
        uploadedSize: 0,
        status: FileUploadStatuses.IDLE,
        statusDescription: 'Загружается файл:',
        uploadResult: null,
        errorMessage: null,
        allowRemoveFileAfterUpload: false
    }
});




/*****************************************************************************
 * Base Model for the profile.
 * Profile may be user, group or event.
 ****************************************************************************/

var BaseProfileModel = Backbone.Model.extend({

    url: '/api/public/profile.json',

    initialize: function (options) {
        var attributes = this.parse(options);
        this.set(attributes);
    },

    parse: function (response) {
        if (!response) {
            return {};
        }
        var json = _.extend({}, response.profile, response);
        json.profile = undefined;
        json.userRelationStatus = response.profileRelationStatus;
        json.group = response.group ? new BaseModel(response.group) : null;
        json.user = response.user ? new BaseModel(response.user) : null;
        return json;
    },

    /**
     * Method for loading profile relations (friends, groups, etc).
     * Overriden in ProfileModel.
     */
    loadProfileRelations: function() {}
});
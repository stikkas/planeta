/*global AclRoute,ProfileUtils,Search*/

/**************************************************************************
 * Contains classes and helpers for working with profile block settings
 **************************************************************************/

var PrivacyUtils = {
    getDefaultSection: function (section, profile) {
        return ProfileUtils.getUserLink(profile.get('profileId'), profile.get('alias'));
    },

    getNavigateString: function (section, profile) {
        if (section !== null) {
            return ProfileUtils.getUserLink(profile.get('profileId'), profile.get('alias')) + '/' + section;
        }
        return ProfileUtils.getUserLink(profile.get('profileId'), profile.get('alias'));
    },

    checkSubSectionAccess: function (section, profile, subsection, id) {
        if (section === 'campaigns') {
            switch (id) {
                case null:
                    return true;
                case 'create':
                    return this.isAdmin();

                default: // numeric id
                    var campaign = workspace.appModel.get('campaignModel');
                    var status = campaign && campaign.get('status');
                    switch (subsection) {
                        case null: return true;
                        case 'donate':
                        case 'donatesingle':
                            return (status === "ACTIVE");
                        case 'orders':
                        case 'widget-stats':
                        case 'shares-stats':
                        case 'advertising-tools':
                        case 'edit':
                            return _.intersection(profile.get('userRelationStatus'), ['ADMIN', 'SELF', 'MANAGER']).length > 0
                                || PrivacyUtils.hasAdministrativeRole();
                    }
            }
        }

        return true;
    },

    isObjectOwnerOrAuthor: function (ownerProfileId, authorProfileId) {
        if (authorProfileId == workspace.appModel.myProfileId()) {
            return true;
        }
        var profileModel = workspace.appModel.get('profileModel');
        if (profileModel && ownerProfileId == profileModel.get('profileId')) {
            var relationStatuses = profileModel.get('userRelationStatus');
            return _(relationStatuses).indexOf('SELF') >= 0 || _(relationStatuses).indexOf('ADMIN') >= 0;
        }
        return false;
    },
    isAdmin: function () {
        var profileModel = workspace.appModel.get('profileModel');
        if (profileModel) {
            var relationStatuses = profileModel.get('userRelationStatus');
            return _(relationStatuses).indexOf('SELF') >= 0 || _(relationStatuses).indexOf('ADMIN') >= 0;
        }

        return false;
    },
    noComment: function(){
        var profileModel = workspace.appModel.get('profileModel');
        if (profileModel) {
            var relationStatuses = profileModel.get('userRelationStatus');
            return _(relationStatuses).indexOf('NO_COMMENT') >= 0;
        }
        return false;
    },
    isGlobalAdmin: function () {
        var myProfile = workspace.appModel.get('myProfile');
        return !_.isUndefined(myProfile) && myProfile.get('isAdmin');
    },
    hasAdministrativeRole: function () {
        return this.isGlobalAdmin();
    },

    getRelationStatuses: function (profileModel) {
        profileModel = profileModel || workspace.appModel.get('profileModel');
        if (profileModel instanceof Backbone.Model) {
            return profileModel.get('userRelationStatus') || profileModel.get('profileRelationStatus') || [];
        }
        return profileModel && (profileModel.userRelationStatus || profileModel.profileRelationStatus) || [];
    },

    setRelationStatuses: function (relationStatus, profileModel) {
        profileModel = profileModel || workspace.appModel.get('profileModel');
        if (profileModel) {
            if (profileModel instanceof Backbone.Model) {
                profileModel.set(profileModel.get('userRelationStatus') ? 'userRelationStatus' : 'profileRelationStatus', relationStatus);
            } else {
                if (profileModel.userRelationStatus) {
                    profileModel.userRelationStatus = relationStatus;
                } else {
                    profileModel.profileRelationStatus = relationStatus;
                }
            }
        }
    },

    doISubscribeYou: function (profileModel) {
        return _(this.getRelationStatuses(profileModel)).indexOf('SUBSCRIPTION') >= 0;
    },

    doYouSubscribeMe: function (profileModel) {
        return _(this.getRelationStatuses(profileModel)).indexOf('SUBSCRIBER') >= 0;
    },

    addRelationStatus: function (status, profileModel) {
        var relationStatus;
        profileModel = profileModel || workspace.appModel.get('profileModel');
        if (profileModel && status) {
            relationStatus = _.union(_.difference(this.getRelationStatuses(profileModel), ['NOT_SET']), _.isArray(status) ? status : [status]);
            if (relationStatus.length === 0) {
                relationStatus = 'NOT_SET';
            }
            this.setRelationStatuses(relationStatus, profileModel);
        }

        return relationStatus;
    },

    removeRelationStatus: function (status, profileModel) {
        var relationStatus;
        profileModel = profileModel || workspace.appModel.get('profileModel');
        if (profileModel && status) {
            relationStatus = _.difference(this.getRelationStatuses(profileModel), _.isArray(status) ? status : [status]);
            if (relationStatus.length === 0) {
                relationStatus = 'NOT_SET';
            }
            this.setRelationStatuses(relationStatus, profileModel);
        }

        return relationStatus;
    },

    _subscribeUnsubscribeSync: function (url, subscribe, profileModel) {
        var self = this;
        var profileId;
        if (_.isNumber(profileModel)) {
            profileId = profileModel;
            profileModel = workspace.appModel.get('profileModel');
        } else {
            profileModel = profileModel || workspace.appModel.get('profileModel');
            profileId = profileModel.get('profileId');
        }
        return Backbone.sync('update', null, {
            url: url,
            data: {
                profileId: profileId
            }
        }).done(function (result) {
            if (result && result.success) {
                if (subscribe) {
                    self.addRelationStatus("SUBSCRIPTION", profileModel);
                } else {
                    self.removeRelationStatus("SUBSCRIPTION", profileModel);
                    if (!self.hasAdministrativeRole()) {
                        self.removeRelationStatus("ADMIN", profileModel);
                    }
                }
            }
        });
    },

    /*profileModel - profileModel or profileId*/
    unsubscribe: function (profileModel) {
        return this._subscribeUnsubscribeSync("/api/profile/unsubscribe.json", false, profileModel);
    },

    /*profileModel - profileModel or profileId*/
    subscribe: function (profileModel) {
        return this._subscribeUnsubscribeSync("/api/profile/subscribe.json", true, profileModel);
    },

    setAdmin: function (isAdmin, profileId, subjectProfileId) {
        var self = this;
        return Backbone.sync('update', null, {
            url: "/api/profile/set-admin-subscriber.json",
            data: {
                profileId: profileId || workspace.appModel.get("myProfile").get("profileId"),
                subjectProfileId: subjectProfileId || workspace.appModel.get("profileModel").get("profileId"),
                isAdmin: isAdmin
            },
            successMessage: true,
            errorMessage: true
        }).done(function (result) {
            if (result && result.success && workspace.appModel.isCurrentProfileMine() && !self.isGlobalAdmin()) {
                if (isAdmin) {
                    self.addRelationStatus("ADMIN");
                } else {
                    self.removeRelationStatus("ADMIN");
                }
            }
        });
    },

    mayISendYouMessage: function (profileModel) {
        profileModel = profileModel || workspace.appModel.get('profileModel');
        var limitMessages = profileModel instanceof Backbone.Model ? profileModel.get('limitMessages') : profileModel && profileModel.limitMessages;

        return profileModel && (!limitMessages || this.doYouSubscribeMe(profileModel));
    }
};

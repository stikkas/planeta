/*globals Modal*/

var SelectableCollection = BaseCollection.extend({
    getSelected: function () {
        if (!this.selected) {
            this.selected = this.find(function (model) {
                return !!model.get('selected');
            });
        }
        return this.selected;
    },
    hasSelected: function () {
        return !_.isEmpty(this.getSelected());
    },
    select: function (model) {
        if (!model || model.get('selected')) {
            return;
        }

        if (this.selected) {
            this.selected.set('selected', false);
        }
        this.selected = model;
        this.selected.set('selected', true);
        this.trigger('selectionChanged', this.selected, this);
    },
    clearSelection: function () {
        var selected = this.getSelected();
        if (selected) {
            selected.set('selected', false);
            this.selected = null;
        }
        this.trigger('selectionChanged', null, this);
    },
    assign: function (another, options) {
        this.reset(another.models, options);
        _.extend(this._callbacks, another._callbacks);

        this.trigger('reassigned', this);
    }
});

var UserPayment = {};
UserPayment.Models = {};

UserPayment.Models.PaymentMethods = SelectableCollection.extend({
    url: function () {
        var url = (location.href.endsWith('/delivery-payment') || this.oldPayment)
                ? '/api/profile/payment-methods.json' : '/api/public/payment-methods';
        if (this.shareId)
            url += '?shareId=' + this.shareId;
        return url;
    },
    initialize: function () {
        SelectableCollection.prototype.initialize.apply(this, arguments);
        this.on('selectionChanged', this.onSelected, this);
    },
    parse: function (methods) {
        var methods = methods.result ? methods.result : methods;
        var self = this;
        return _.map(methods, function (method) {
            var children = new SelectableCollection(method.children);
            children.on('selectionChanged', self.onChildSelected, self);
            return _.extend(method, {
                children: children
            });
        });
    },
    selectionChanged: function (id, needPhone, method) {
        this.trigger('methodSelected', {
            id: id,
            needPhone: needPhone
        }, method, this);
    },
    onChildSelected: function (child) {
        this.selectionChanged(child.get('id'), child.get('needPhone'), child);
    },
    onSelected: function (method) {
        if (method) {
            var children = method.get('children');
            if (children.hasSelected()) {
                var child = children.getSelected();
                this.selectionChanged(child.get('id'), child.get('needPhone'), method);
            } else {
                this.selectionChanged(children.isEmpty() ? method.get('id') : 0, method.get('needPhone'), method);
            }
            // Для повторного использования способа оплаты
//            SessionStorageProvider.set("currentPaymentMethodId", method.get('id'));
        }
    }
});


UserPayment.Models.BasePayment = BaseModel.extend({
    defaults: {
        amount: 100,
        needPhone: false,
        phone: null,
        ready: false,
        redirectUrl: '/payment-create.html',
        redirectHost: 'mainHost'
    },
    initialize: function () {
        BaseModel.prototype.initialize.call(this, arguments);

        var methods;
        if (this.methodsModel) {
            methods = new this.methodsModel();
        }
        else {
            methods = new UserPayment.Models.PaymentMethods();
        }
        methods.shareId = this.get('shareId');
        methods.oldPayment = this.get('oldPayment');
        methods.on('methodSelected', this.onPaymentMethodSelected, this);

        this.set({
            methods: methods,
            childrenMethods: new SelectableCollection([])
        });

        this.on('change paymentChanged', this.checkReadiness, this);
    },
    fetch: function (options) {
        return this.get('methods').load(options);
    },
    onPaymentMethodSelected: function (data, method) {
        this.silentSet({
            paymentMethodId: data.id,
            needPhone: data.needPhone
        });
        if (method.has('children')) {
            this.get('childrenMethods').assign(method.get('children'));
        }
    },
    checkReadiness: function () {
        var isReady = this.get('ready');
        this.set('ready', this.isReady(), {silent: true});
        if (isReady != this.get('ready')) {
            this.trigger('readinessChanged', this.get('ready'), this);
        }
    },
    isReady: function () {
        return this.isAmountReady() && this.isPaymentMethodReady();
    },
    isAmountReady: function () {
        return this.get('amount') > 0;
    },
    isPaymentMethodReady: function () {
        return this.get('paymentMethodId') > 0 && this.isPhoneReady();
    },
    isPhoneReady: function () {
        return this.get('needPhone') ? this.has('phone') : true;
    },
    silentUnset: function () {
        var self = this;
        _.each(arguments, function (attrName) {
            self.unset(attrName, {silent: true});
        });
        this.trigger('paymentChanged', this);
    },
    silentSet: function (key, val) {
        if (_.isObject(key)) {
            this.set(key, {silent: true});
        } else {
            this.set(key, val, {silent: true});
        }
        this.trigger('paymentChanged', this);
    },
    setAmount: function (amount) {
        this.silentSet('amount', amount);
    },
    setPhone: function (phone) {
        var parsed = parseInt(phone, 10);
        var isValid = !_.isNaN(parsed) && _.isEqual(parsed.toString().length, 11);

        this.silentSet('phone', isValid ? phone : null);
    },
    getAttrs: function (names) {
        var result = {};
        var self = this;
        _.each(_.intersection(names || [], _.keys(this.attributes)), function (name) {
            result[name] = self.get(name);
        });
        return result;
    },
    createPaymentForm: function (customParams) {
        var paramNames = _.union(customParams || [], ['amount', 'paymentMethodId', 'phone']);
        var params = this.getAttrs(paramNames);
        var url = this.get('redirectUrl');

        return $.form(url, params);
    }
});

UserPayment.Models.OrderPayment = UserPayment.Models.BasePayment.extend({
    defaults: _.extend({}, UserPayment.Models.BasePayment.prototype.defaults, {
        smsVerificationEnabled: true,
        isPlanetaPurchaseEnabled: true,
        planetaPurchase: false,
        price: 0,
        myBalance: 0
    }),
    PLANETA_PAYMENT_METHOD_ID: 10,
    initialize: function () {
        UserPayment.Models.BasePayment.prototype.initialize.call(this, arguments);
        this.calculateAmountAndTotalSum();
        this.set('amountPreset', true);
    },
    setAmount: function (amount) {
        this.setPrice(amount, true);
    },
    calculateAmountAndTotalSum: function () {
        var balance = this.get('myBalance');
        var price = this.get('price');
        var planetaPurchase = this.get('planetaPurchase');
        var amount = planetaPurchase ? Math.max(0, (price - balance)) : price;
        var fromBalance = price > 0 && planetaPurchase && price <= balance;

        this.set({
            amount: amount,
            totalSum: fromBalance ? price : amount,
            fromBalance: fromBalance
        });

        if (planetaPurchase && fromBalance) {
            this.silentSet('paymentMethodId', this.PLANETA_PAYMENT_METHOD_ID);
        };
    },
    togglePlanetaPurchase: function tpp() {
        var isPlanetaPurchase = !this.get('planetaPurchase');
        this.set('planetaPurchase', isPlanetaPurchase);
        this.calculateAmountAndTotalSum();
        if (isPlanetaPurchase && this.get('fromBalance')) {
            tpp.lastMethod = this.get('paymentMethodId');
            this.silentSet('paymentMethodId', this.PLANETA_PAYMENT_METHOD_ID);
        } else if (!isPlanetaPurchase) {
            if (tpp.lastMethod !== undefined && !this.get('paymentMethodId'))
                this.silentSet('paymentMethodId', tpp.lastMethod);
        }
        return isPlanetaPurchase;
    },
    isReady: function () {
        return (this.get('paymentGroup') === 'CASH')
                || this.get('fromBalance')
                || UserPayment.Models.BasePayment.prototype.isReady.apply(this, arguments);
    },
    setPrice: function (price, silent) {
        if (silent) {
            this.set('price', price, {silent: true});
        } else {
            this.set('price', price);
        }
        this.calculateAmountAndTotalSum();
    },
    confirmPhone: function (phone) {
        console.log('confirmPhone');
        return this.fetchX({
            contentType: 'application/json',
            method: 'update',
            url: '/api/confirm-phone.json',
            data: {'phoneNumber': phone}
        });
    }
});


/*global Video,Audios,Photo,loadModule,StorageUtils,workspace,$,BaseCollection,Modal,console,_,BaseModel*/

/**
 * Common functions for show/play rich media content
 */
var App = {
    Mixins: {}
};

App.Mixins.Media = {

    showPhoto: function (profileAlias, photoId, self, options) {
        loadModule("photo").done(function () {
            Photo.Views.showPhoto(profileAlias, photoId, self, options);
        });
    },

    showPhotoCollection: function (id, owner, collection, activeIndex, fullscreen) {
        loadModule("photo").done(function () {
            Photo.Views.showPhotoCollection(id, owner, collection, activeIndex, fullscreen);
        });
    },

    showVideo: function (profileAlias, videoId, self, options) {
        loadModule("video").done(function () {
            Video.Views.showVideo(profileAlias, videoId, self, options);
        });
    },

    showVideoPlayer: function (profileId, videoId, element, options) {
        loadModule("video").done(function () {
            Video.Views.showVideoPlayer(profileId, videoId, element, options);
        });
    },

    audioPlayerPause: function () {
        if (workspace.appModel.audioPlayer && workspace.appModel.audioPlayer.isPlaying()) {
            workspace.appModel.audioPlayer.playPause();
        }
    },

    /**
     * @param element - use this
     * @param url - url of external video
     * @see VideoService for server part
     *
     * for XSS security reasons it is better to
     * use it only like showExternal('http://...', this)
     * */
    showExternal: function (url, element) {
        loadModule("video").done(function () {
            Video.Views.showExternal(url, element);
        });
    },

    showModalExternalVideo: function (url, profileId, displayName, imageUrl) {
        loadModule("video").done(function () {
            Video.Views.showModalExternalVideo(url, profileId, displayName, imageUrl);
        });
    }
};
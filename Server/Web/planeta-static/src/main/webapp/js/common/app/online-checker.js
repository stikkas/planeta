var ProfileOnlineChecker = BaseModel.extend({
    /**
     * Users array is divided into chunks (30 userIds each).
     * Each chunk is processed by a separate request.
     */
    CHUNK_SIZE: 20,
    fillOnlineStatus: function (collection) {
        var self = this;
        collection.each(function (item) {
            item.set({
                onlineStatus: _.include(self.get('onlineUsers'), item.get('subjectProfileId'))
            });
        });
    },
    checkOnlineStatus: function (collection, tagId) {
        var self = this;
        var undefinedOnly = false;

        var onlineUsersTagId = this.get('onlineUsersTagId');
        if (this.lastOnlineStatusUpdate && this.lastOnlineStatusUpdate > new Date().getTime() - 30000 && (onlineUsersTagId == 0 || onlineUsersTagId == tagId)) {
            undefinedOnly = true;
        }

        var first = collection.at(0);
        if (!first || first.get('profileType') != 'USER') {
            return;
        }
        var users = [];
        if (undefinedOnly) {
            collection.each(function (itemModel) {
                var profileId = itemModel.get('subjectProfileId');
                if (itemModel.get('onlineStatus') == null && !_(self.get('checkedUsers')).include(profileId)) {
                    users.push(profileId);
                }
            });
        } else {
            users = collection.pluck('subjectProfileId');
        }

        this.fillOnlineStatus(collection);
        if (!users || !users.length) {
            return;
        }

        this.lastOnlineStatusUpdate = new Date().getTime();
        this.sendCheckOnlineStatusAjax({users: users}).done(function (data) {
            self.set({
                onlineUsers: _(_(self.get('onlineUsers')).difference(users)).union(data.users),
                checkedUsers: _(self.get('checkedUsers')).union(users),
                onlineUsersTagId: tagId
            });
            self.fillOnlineStatus(collection);
        });
    },
    /**
     * Splits users array into smaller chunks.
     *
     * @param users
     */
    getUsersChunks: function (users) {
        var chunksCount = Math.ceil(users.length / this.CHUNK_SIZE);
        var chunks = [];

        var i;
        for (i = 0; i < chunksCount; i += 1) {
            // Creating chunk
            chunks[i] = [];

            var j;
            for (j = 0; j < this.CHUNK_SIZE; j += 1) {

                // Filling chunk with user identifiers
                var userId = users[this.CHUNK_SIZE * i + j];
                if (userId) {
                    chunks[i].push(userId);
                } else {
                    // Exit loop, this is the end of array
                    break;
                }
            }
        }

        return chunks;
    },
    sendCheckOnlineStatusAjax: function (options) {
        if (!options || !options.users) {
            return;
        }

        var users = options.users;

        if (_.isNumber(users)) {
            users = [users];
        } else if (!_.isArray(users) || !users.length) {
            return;
        }

        var chunks = this.getUsersChunks(users);
        var onlineUsers = [];
        var deferreds = [];

        // Executes a number of requests (against each chunk of users).
        // We split users array into chunks because of browser limitations -- we
        // cannot execute jsonp request with too much data.
        _(chunks).each(function (chunk) {
            var dfd = $.get(workspace.appModel.get('statusServiceUrl') + "/count/get-online",
                    {users: chunk}).done(function (response) {
                if (response && response.users) {
                    onlineUsers = _.union(onlineUsers, response.users);
                }
            });
            deferreds.push(dfd);
        });

        return $.when.apply(this, deferreds);
    },
    sendCheckOnlineFullStatusAjax: function (options) {

        if (!options || !options.profileId) {
            return;
        }

        return $.get(workspace.appModel.get('statusServiceUrl') + "/count/get-user-last-online-time.json",
            {u: options.profileId},
            function (response) {
                if (options.success) {
                    options.success(response);
                }
            });
    }
});
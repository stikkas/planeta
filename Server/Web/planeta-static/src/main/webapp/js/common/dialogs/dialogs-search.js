/*globals Dialogs,dialogsPageController,ResizableScrollListView,BaseCollection,_,$,workspace,BaseView,BaseModel,Backbone*/
/**
 * Dialog search views and models. Used for rendering search dialogs sidebar.
 */
Dialogs.Models.DialogsSearchCollection = BaseCollection.extend({
    url: function () {
        return '//' + workspace.serviceUrls.imServiceUrl + '/dialogs-friends.json';
    },
    model: Dialogs.Models.Dialog,

    parse: function (response) {
        var activeDialog = workspace.appModel.get('dialogsController').get('activeDialog');
        _.forEach(response, function (dialogInfo) {
            // java DialogInfo has long dialogId field, so it's hard to send it as null
            // but Backbone.Collection.add decide models are equal if id != null (2 models with id = 0)
            if (dialogInfo.dialog.dialogId == 0) {
                dialogInfo.dialog.dialogId = null;
            }
            dialogInfo.onlineStatus = false;
        });
        return response;
    },

    load: function (options) {
        var self = this;
        options = _.extend({}, options);
                
        var oldSuccess = options.success;
        options.success = function (response, collection) {
            this.trigger("onDialogsFriendsLoaded", this);
            if (oldSuccess) {
                oldSuccess.call(self, collection);
            }
        };
        return BaseCollection.prototype.load.call(this, options);
    }
});

Dialogs.Views.DialogsSearchItem = BaseView.extend({
    tagName: 'li',
    template: '#dialog-search-item-template',
    className: 'rd-item',

    viewEvents: {
        'click': 'startDialogClicked'
    },

    afterRender: function () {
        if (this.model.get('onlineStatus')) {
            this.$el.addClass('status-online');
        }
    }
});

Dialogs.Views.DialogsSearchList = ResizableScrollListView.extend({
    tagName: 'ul',
    className: 'right-dialog-list',
    maxHeight: 360
});

Dialogs.Views.DialogsSearchInput = BaseView.extend({
    className: 'input-icon input-field shift-icon-top',
    template: '#dialog-search-input-template',

    viewEvents: {
        'keyup #dialogs-search': 'onChangeDialogSearch'
    },

    afterRender: function () {
        this.searchInput = $(this.el).find('#dialogs-search');
    }
});

Dialogs.Views.DialogsSearch = BaseView.extend({
    template: '#dialog-search-template',

    construct: function () {
        if (!(this.model instanceof Dialogs.Models.DialogsSearch)) {
            this.model = this.model.dialogsSearch;
        }

        this.isFloat = !!this.model.get("isFloat");

        this.searchFilter = this.addChildAtElement('#dialog-search-filter', new Dialogs.Views.DialogsSearchInput());
        this.searchList = this.addChildAtElement('.right-dialog', new Dialogs.Views.DialogsSearchList({
            collection: this.model.get('list'),
            itemViewType: Dialogs.Views.DialogsSearchItem
        }));

        this.addViewSortSelection();

        this.searchFilter.bind('onChangeDialogSearch', this.searchDialog, this);
        this.searchList.bind('startDialogClicked', this.startDialog, this);

        if (!this.isFloat) {
            dialogsPageController.addListener(this, this.searchList);
        }


    },
    addViewSortSelection: function () {
        var collection = new Dialogs.Models.DialogsSearchSortSelectionCollection(Dialogs.DialogsSearchSortSelectionModels);


        var model = new BaseModel();
        collection.on("selectedItemChanged", function (selectedItem) {
            model.set("text", selectedItem.get("text"));
            var list = this.model.get('list');
            list.comparator = selectedItem.get("comparator");
            list.sort();

        }, this);


        var defItem = collection.findByAttr("showByDefault", true);

        collection.selectItem(defItem ? defItem.id : 0);

        var view = this.addChildAtElement(".dm-footer", new Dialogs.Views.DialogsSearchSortSelection({model: model}));

        collection.on("selectedItemChanged", function () {
            view.hideDropDown();
        });

        view.addChildAtElement(".dmc-list", new Dialogs.Views.DialogsSearchSortSelectionList({
            collection: collection,
            itemViewType: Dialogs.Views.DialogsSearchSortSelectionListItem
        }), true);

        if (!this.isFloat) {
            dialogsPageController.addListener(this, view);
        }

    },

    startDialog: function (view) {
        this.model.startDialog(view.model);
    },

    searchDialog: _.debounce(function (view) {
        var searchString = view.searchInput.val();
        this.model.searchDialog(searchString);
    }, 500)
});

Dialogs.Models.DialogsSearch = BaseModel.extend({

    REFRESH_ONLINE_STATUS: 5 * 10 * 1000,

    initialize: function (options) {
        BaseModel.prototype.initialize.call(this, options);
        this.dialogsController = workspace.appModel.get('dialogsController');
        this.dialogsController.set("dialogsSearchModel", this);
        var list = new Dialogs.Models.DialogsSearchCollection([], {
            limit: 20,
            data: {}
        });
        this.set({
            list: list
        });
        var collectionChanged = function (event) {
            this.checkOnlineStatus(this.get('list'));
        };
        list.on('reset', collectionChanged, this);
        list.on('onDialogsFriendsLoaded', this.dialogsController.onDialogsFriendsLoaded, this.dialogsController);
    },

    onUnreadMessagesLoaded: function (loadedCollection) {
        var list = this.get('list');
        var isSearchStringEmpty = !$.trim(list.data.searchString);
        loadedCollection.each(function (loadedItem) {
            var dialogId = loadedItem.get("dialog").dialogId;
            var isDialogExists = false;
            list.each(function (listItem) {
                if (listItem.get("dialogId") == dialogId) {
                    isDialogExists = true;
                    var lastMessage = loadedItem.get("lastMessage");
                    listItem.set("unreadMessagesCount", lastMessage ? loadedItem.get("unreadMessagesCount") : 0);
                    if (lastMessage) {
                        listItem.set("timeUpdated", lastMessage.timeAdded);
                    }
                }
            });

            if (isSearchStringEmpty && !isDialogExists) {
                list.add(new Dialogs.Models.Dialog(loadedItem.attributes));
            }
        });
        if (list.comparator) {
            list.sort();
        }
    },

    onUnreadMessagesChanged: function (dialogId, unreadMessagesCount, lastMessage) {
        var self = this;
        var list = this.get('list');
        list.each(function (listItem) {
            if (lastMessage && listItem.get("dialogId") == dialogId) {
                listItem.set("unreadMessagesCount", unreadMessagesCount);
                var dialogInfo = listItem.get('dialogInfo');

                dialogInfo.lastMessage = _.clone(lastMessage.attributes);
                dialogInfo.dialog.lastMessageId = lastMessage.get('messageId');
                dialogInfo.unreadMessagesCount = unreadMessagesCount;
                self.dialogsController.onDialogsFriendsLoaded(new BaseCollection(listItem));

            }
        });
    },

    fetch: function (options) {
        this.get('list').load(options);
    },

    startDialog: function (model) {
        var dialogId = model.id;
        var self = this;
        if (dialogId > 0) {
            self.dialogsController.showDialog(dialogId);
        } else {
            var options = {
                url: '//' + workspace.serviceUrls.imServiceUrl + '/dialog-with-user.json',
                data: {
                    companionProfileId: model.get('companionId')
                },
                method: 'GET',
                context: this,
                success: function (response) {
                    self.dialogsController.showDialog(response.dialog.dialogId);
                }
            };

            Backbone.sync('read', model, options);
        }
    },

    searchDialog: function (searchString) {
        var list = this.get('list');
        list.data.searchString = searchString;
        list.load();
    },

    getLastDialog: function () {
        return this.get('list').at(0);
    },

    fillOnlineStatus: function (users, collection) {
        var activeDialog = workspace.appModel.get('dialogsController').get('activeDialog');
        if (activeDialog) {
            activeDialog.set("onlineStatus", _.include(users, activeDialog.get('companionId')));
        }
        collection.each(function (item) {
            item.set({
                onlineStatus: _.include(users, item.get('companionId'))
            });
        });
    },
    checkOnlineStatus: function (collection) {
        if (collection.length == 0) {
            return;
        }
        var idList = collection.pluck('companionId');

        var controller = workspace.appModel.get('dialogsController');
        if (controller.get('lastOnlineUpdate') != null && controller.get('lastOnlineUpdate') > new Date().getTime() - this.REFRESH_ONLINE_STATUS) {
            this.fillOnlineStatus(controller.get('onlineUsers'), collection);
            return;
        }

        controller.set({
            lastOnlineUpdate: new Date().getTime()
        });
        var self = this;
        workspace.appModel.get('onlineChecker').sendCheckOnlineStatusAjax({users: idList}).done(function (data) {
            controller.set({
                onlineUsers: data.users
            });
            self.fillOnlineStatus(data.users, collection);
            if (collection.comparator) {
                collection.sort();
            }
        });
    }
});

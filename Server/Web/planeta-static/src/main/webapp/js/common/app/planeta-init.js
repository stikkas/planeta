/**
 * Created by asavan on 30.04.2017.
 */

(function() {

    workspaceInitParameters.myProfile.constructor = ProfileModel;
    var c = workspaceInitParameters.configuration;
    var staticNodesService = new StaticNodesService(c.staticNode, c.staticNodes, c.resourcesHost,
        c.jsBaseUrl, c.flushCache, c.compress, workspaceInitParameters.currentLanguage);

    TemplateManager.init(workspaceInitParameters.currentLanguage, staticNodesService);


    $(document).ready(function () {

    window.workspace = Planeta.init({
        myProfile: workspaceInitParameters.myProfile,
        appModel: {
            constructor: AppModel,
            profileConstructor: ProfileModel,
            profile: workspaceInitParameters.profile
        },
        configuration: workspaceInitParameters.configuration,
        staticNodesService: staticNodesService,
        routing: {
            navigationMap: NavigationMap,
            router: PlanetaRouter
        },
        currentLanguage: workspaceInitParameters.currentLanguage

    });


        LazyHeader.init({
        auth: headerInitParameters.auth
    });

    if (workspaceInitParameters.needStartRouter) {
        setTimeout(function(){
            workspace.start(true);
        }, 1);
    }

    });
})();


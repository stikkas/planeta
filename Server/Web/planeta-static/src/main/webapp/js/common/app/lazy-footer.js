/**
 * Created by asavan on 26.04.2017.
 */
(function() {

    if (!window.$) {
        return;
    }
    $(function () {

        function initDigestSubscribe() {
            new DigestSubscribe.Views.DigestSubscribeForm({
                el: '.js-footer-digest'
            }).render();
        }

        function initSocialHrefs() {
            var isCharity = /^charity/.test(window.location.hostname);
            if (isCharity) {
                var normalSocialIcons = [
                    {href: "https://vk.com/planetaru", className: "f-social-vk"},
                    {href: "https://www.facebook.com/planetaru", className: "f-social-fb"},
                    {href: "https://twitter.com/PlanetaPortal", className: "f-social-tw"},
                    {href: "https://plus.google.com/+PlanetaRuportal/posts", className: "f-social-gp"},
                    {href: "https://www.youtube.com/user/planetarutv", className: "f-social-yt"},
                    {href: "https://ok.ru/planetaportal", className: "f-social-ok"},
                    {href: "https://instagram.com/planeta_ru", className: "f-social-ig"}
                ];

                var baldSocialIcons = [
                    {href: "https://vk.com/charityplaneta", className: "f-social-vk"},
                    {href: "https://www.facebook.com/charityplaneta", className: "f-social-fb"},
                    {href: "https://ok.ru/charityplanetaru", className: "f-social-ok"}
                ];
            } else {
                 var normalSocialIcons = [
                    {href: "https://vk.com/planetaru", className: "f-social-vk"},
                    {href: "https://www.facebook.com/planetaru", className: "f-social-fb"},
                    {href: "https://twitter.com/PlanetaPortal", className: "f-social-tw"},
                    {href: "https://www.youtube.com/user/planetarutv", className: "f-social-yt"},
                    {href: "https://instagram.com/planeta_ru", className: "f-social-ig"}
                ];

                var baldSocialIcons = [
                    {href: "https://vk.com/charityplaneta", className: "f-social-vk"},
                    {href: "https://www.facebook.com/charityplaneta", className: "f-social-fb"},
                ];
            }


            var iconsArray;
            if (window.workspaceInitParameters && workspaceInitParameters.configuration.projectType === 'CHARITY') {
                iconsArray = baldSocialIcons;
            } else {
                iconsArray = normalSocialIcons;
            }

            var iconsHtml = "";
            _.each(iconsArray, function (icon) {
                iconsHtml += _.template('<a class="f-social_i" href="<%= href %>"><i class="<%= className %>"></i></a>', icon)
            });
            $('.js-social-anchor').append(iconsHtml);

        }

        if (window.isMobileDev) {
            $('.js-mobile-footer').removeClass('hidden');
        } else {
            $('.js-footer').removeClass('hidden');

            initSocialHrefs();
            initDigestSubscribe();
        }

        var isEngLangOnPage = workspaceInitParameters.currentLanguage === 'en';
        $('#switch-language').click(function (e) {
            e.preventDefault();
            toggleLanguage(workspace.currentLanguage);
        });
        
        $('.f-lang_ico > span').addClass(isEngLangOnPage ? 'flag-icon-ru' : 'flag-icon-gb');
        $(".fic-mistake-a").click(function (e) {
            e.preventDefault();
            FeedbackHelper.showFeedBackDialog();
        });
    });
})();


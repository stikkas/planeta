/*global Audios,StringUtils*/

Audios.Views.NewPlayer = {
    TrackList: {}
};

Audios.Views.NewPlayer.Slider = BaseView.extend({
    alwaysShowTooltip: true,
    progressBarBlockContainer: ".song-pln-progress-block",
    sliderContainer: ".song-pln-progress",

    modelEvents: {
        'destroy': 'dispose'
    },

    initialize: function () {
        this._initEvents();
        BaseView.prototype.initialize.apply(this, arguments);
    },

    dispose: function () {
        if (this.tooltip) {
            this.tooltip.dispose();
        }
        BaseView.prototype.dispose.apply(this, arguments);
    },

    _initEvents: function () {
        this.events = this.events || {};
        this.events["click " + this.progressBarBlockContainer] = "progressClick";

        this.modelEvents = _.extend({
            'destroy': 'dispose'
        }, this.modelEvents);
        this.modelEvents["change:" + this.progressField] = "progressChange";
    },

    afterRender: function () {
        this._initSlider();
        if (this.tooltip) {
            this.tooltip.dispose();
        }

        this.tooltip = new Audios.Views.NewPlayer.Slider.Tooltip({
            $element: this.$el,
            controller: this,
            progressBarBlockContainer: this.progressBarBlockContainer
        });
    },

    _initSlider: function () {
        var self = this;
        this.$slider = this.$(this.sliderContainer);
        this.$slider.slider({
            orientation: "horizontal",
            range: "min",
            slide: function (event, ui) {
                this._isDrag = true;
                self.setProgressValue(ui.value);
                if (self.onSlide) {
                    self.onSlide();
                }
                this._isDrag = false;
                Backbone.Events.trigger.call(self, 'onSlide', event.originalEvent);
            },

            start: function () {
                self._isSliding = true;
                self.trigger('onSlideStart');
                Audios.Views.NewPlayer.Slider.Tooltip.currentTooltip = self.tooltip;
            },

            stop: function () {
                self._isSliding = false;
                self.trigger('onSlideStop');
                Audios.Views.NewPlayer.Slider.Tooltip.currentTooltip = null;
            }
        });
        this.setDisabled(!this.isAvailable());
        this.progressChange();
    },


    getTooltipText: function (progress) {
        return parseInt(progress, 10);
    },


    _getProgressWidth: function () {
        if (!this._progressWidth) {
            this._progressWidth = this.$(this.sliderContainer).width();
        }
        return this._progressWidth;
    },

    progressChange: function () {
        if (this._isProgressChange) {
            return;
        }
        try {
            this._isProgressChange = true;
            if (this.isAvailable()) {
                this._progressChange();
            }
        } finally {
            this._isProgressChange = false;
        }
    },

    _progressChange: function () {
        var progress = this.getProgressValue();
        if (this.onProgressChange) {
            this.onProgressChange(progress);
        }
        this._setSliderValue();
    },

    _setSliderValue: function () {
        if (this._isDrag) {
            return;
        }
        this._setSliderOptions("value", this.getProgressValue());
    },

    getProgressValue: function () {
        return this.model.get(this.progressField);
    },

    setProgressValue: function (newValue) {
        this.model.set(this.progressField, newValue);
    },

    progressClick: function (event) {
        if (this.isAvailable()) {
            this.setProgressValue((event.offsetX / this._getProgressWidth()) * 100);
        }
    },

    isAvailable: function () {
        return true;
    },

    setDisabled: function (disabled) {
        this.$('.ui-slider-handle').toggleClass('hide', disabled);
        this._setSliderOptions("disabled", disabled);
    },

    _setSliderOptions: function (variable, value) {
        this.$slider.slider("option", variable, value);
    },

    _getSliderOptions: function (variable) {
        return this.$slider.slider("option", variable);
    },

    isSliding: function () {
        return this._isSliding;
    },

    getAlwaysShowTooltip: function () {
        return this.alwaysShowTooltip;
    }
});

Audios.Views.NewPlayer.Slider.Tooltip = BaseView.extend({
    className: "popup-container",
    template: "#np-slider-tooltip",
    modelEvents: {
        "controller onSlideStop": "onSlideStop"
    },

    construct: function (options) {
        this.$element = options.$element;
        this.$progressBlock = options.progressBarBlockContainer ? this.$element.find(options.progressBarBlockContainer) : this.$element;
        this.$progressBlock
            .mousemove(_.bind(this._mouseMove, this))
            .hover(_.bind(this._mouseIn, this), _.bind(this._mouseOut, this))
            .mousewheel(_.bind(this._mouseWheel, this))
            .mousedown(_.bind(this._mouseDown, this))
        ;
        this.controller.on("onSlide", this.onSlide, this);

        this.$el.hide();
        this.$el.appendTo($("body"));
        this.isIn = false;
        this.render();
    },

    dispose: function () {
        this.controller.off("onSlide", this.onSlide);
        BaseView.prototype.dispose.apply(this, arguments);
    },


    onSlide: function (e) {
        this.setPosition(e.pageX);
    },

    onSlideStop: function () {
        if (this.isVisible) {
            if (!this.isIn || !this.controller.getAlwaysShowTooltip()) {
                this._hide();
            }
        }
    },

    _mouseDown: function (e) {
        if (!this.isVisible && this.controller.isSliding()) {
            this._show(e.pageX);
        }
    },

    _mouseMove: function (e) {
        this._move(e.pageX);
    },

    _move: function (x) {
        if (this.isIn) {
            if (this.isVisible) {
                this.setPosition(x);
            } else {
                this._show(x);
            }
        }
    },

    setPosition: function (pageX) {
        var progressWidth = this.$progressBlock.width();
        var progressLeft = this.$progressBlock.offset().left;
        var x = pageX - progressLeft;
        x = Math.min(Math.max(x, 0), progressWidth);
        var progress = x / progressWidth * 100;

        this.$(".tooltip-inner").text(this.controller.getTooltipText(progress));
        this.$(".black-tooltip").css({
            left: (progressLeft + x) + "px",
            top: (this.$progressBlock.offset().top - this.$(".tooltip-inner").outerHeight(true) - 18) + "px"
        });
    },

    _show: function (x) {
        this.isVisible = false;
        var currentTooltip = Audios.Views.NewPlayer.Slider.Tooltip.currentTooltip;
        if (currentTooltip && currentTooltip !== this) {
            return;
        }

        if (this.controller.isAvailable() && (this.controller.getAlwaysShowTooltip() || this.controller.isSliding())) {
            this.$el.fadeIn(100);
            this.setPosition(x);
            this.isVisible = true;
        }
    },

    _mouseIn: function (e) {
        this.isIn = true;
        this._show(e.pageX);
    },

    _mouseOut: function () {
        this.isIn = false;
        if (this.isVisible && !this.controller.isSliding()) {
            this._hide();
        }
    },

    _hide: function () {
        this.$el.fadeOut(100);
        this.isVisible = false;
    },

    _mouseWheel: function (e) {
        this.$el.hide();
        this.isVisible = false;
    }
});

Audios.Views.TrackSlider = Audios.Views.NewPlayer.Slider.extend({
    template: "#audio-global-song-progress-bar",
    progressField: "trackPosition",
    progressBarBlockContainer: ".track-list-item-progress-bar-wrap",
    sliderContainer: ".track-list-item-progress-bar",
    modelEvents: {
        "change:trackLoaded": "onTrackLoadedChanged"
    },

    setProgressValue: function (newValue) {
        this.model.get("trackPosition", newValue);
        this.controller.rewind(this.controller.getActiveTrack(), newValue);
    },

    getTooltipText: function (progress) {
        var trackDuration = this.model.get('track').get('trackDuration');
        return StringUtils.humanDuration((trackDuration / 100 * progress) || 0);
    },

    onTrackLoadedChanged: function (model, value) {
        this.$('.track-list-item-progress-bar-dwnd').width((value | 0) + '%');
    }

});

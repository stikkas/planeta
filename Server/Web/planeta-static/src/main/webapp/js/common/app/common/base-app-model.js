/*globals BaseModel,$,Audios,StorageUtils,Backbone,_,moduleLoader*/
/**
 * Created by IntelliJ IDEA.
 * User: ameshkov
 * Date: 23.03.12
 * Time: 16:58
 * To change this template use File | Settings | File Templates.
 */
var BaseAppModel = BaseModel.extend({

    initialize: function (options) {

        BaseModel.prototype.initialize.call(this, options);

        var videoManager = {
            currentVideoView: null,
            currentVideoCoverHtml: null,
            currentElement: null,
            showVideo: function (view, element) {
                this.pauseOldVideo();
                this.currentElement = element;
                this.currentVideoCoverHtml = $(element).html();
                this.oldOnClick = $(element).attr('onclick');
                $(element).attr('onclick', null);
                this.currentVideoView = view;
            },

            pauseOldVideo: function () {
                if (this.currentVideoView) {
                    this.currentVideoView.dispose();
                    this.currentVideoView = null;
                }
                if (this.currentElement) {
                    $(this.currentElement).html(this.currentVideoCoverHtml);
                    $(this.currentElement).attr('onclick', this.oldOnClick);
                }
            },

            interruptAudio: function () {
                StorageUtils.setAudioPlayerState({
                    tabName: window.name,
                    audioStatus: 'interrupt'
                });
            },

            //calls every time after video player been disposed
            resumeAudio: function () {
                var videosCount = StorageUtils.getVideosCount();
                //checking active video players count,  if more than one then return
                if (videosCount && videosCount > 0) {
                    return;
                }
                StorageUtils.setAudioPlayerState({
                    tabName: window.name,
                    audioStatus: 'resume'
                });
            },

            increaseVideosCount: function () {
                var videosCount = StorageUtils.getVideosCount();
                if (videosCount && videosCount < 2) {
                    videosCount++;
                    StorageUtils.setVideosCount(videosCount);
                } else {
                    StorageUtils.setVideosCount(1);
                }
            },

            decreaseVideosCount: function () {
                var videosCount = StorageUtils.getVideosCount();
                if (videosCount) {
                    videosCount--;
                    StorageUtils.setVideosCount(videosCount);
                }
            }
        };

        this.set({
            videoManager: videoManager
        });
    },

    /**
     * Checks if the specified profile id is equal to id parameter
     */
    isSameProfile: function (id) {
        var currentProfileModel = this.get('profileModel');
        if (!currentProfileModel) {
            return false;
        }
        return currentProfileModel.get('profileId') == id || currentProfileModel.get('alias') == id;
    },

    myProfileId: function () {
        return this.get('myProfile').get('profileId');
    },

    isAuthorized: function () {
        return this.get('myProfile').get('isAuthorized');
    },

    hasEmail: function () {
        return !!this.get('myProfile').get('email');
    },


    isRegistrationConfirmed: function () {
        return this.get('myProfile').get('status') !== "NOT_SET";
    },

    myProfileAlias: function () {
        return this.get('myProfile').get('alias');
    },

    isMyProfile: function (id) {
        if (!id) {
            return false;
        }
        if (id == -1) {
            return false;
        }
        return !id || this.myProfileId() == id || this.myProfileAlias() == id;
    },
    resendConfirmationEmail: function (options) {
        (this.sync || Backbone.sync)('read', this, _.extend(options || {}, {url: '/api/profile/resend-email.json'}));
    },
    isCurrentProfileMine: function () {
        return this.isSameProfile(this.myProfileId());
    },

    getAudioPlayer: function () {
        var self = this;
        var dfd = $.Deferred();
        if (this.audioPlayer) {
            dfd.resolve(this.audioPlayer);
        } else {
            moduleLoader.loadModule('audio').done(function () {
                dfd.resolve(self.audioPlayer());
            });
        }
        return dfd.promise();
    }
});
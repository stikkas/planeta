/*globals CampaignDonate, Wizard, UserPayment, LazyHeader, AccountMerge, StorageUtils, Modal*/
/**
 *
 * @author Andrew.Arefyev@gmail.com
 * Date: 13.08.13
 * Time: 14:40
 */
if (!window.CampaignDonate) {
    var CampaignDonate = {};
}
CampaignDonate.Views = CampaignDonate.Views || {};

CampaignDonate.Views.Payment = UserPayment.Views.OrderPayment.extend({
    template: '#order-payment-block-template',
    className: 'pln-payment-box',

    construct: function() {
        UserPayment.Views.OrderPayment.prototype.construct.apply(this, arguments);

        if (this.model.get('orderType') === 'DELIVERY') {
            this.addChildAtElement('.js-delivery-payment-total', new CampaignDonate.Views.DeliveryPaymentTotal({
                model: this.model
            }));
        }
    },
    
    isCashEnabled: function() {
        // now cash is disabled for every type of order objects
        /*
        var parent = this.model.controller;
        var isPostService =
            (parent.get('serviceId') != -1) &&
            (parent.get('serviceId') != 11) &&
            (parent.get('serviceId') != 12) &&
            (parent.get('serviceId') != 13);

        return (parent.get('orderType') === 'PRODUCT') && isPostService && !parent.hasDigitalTrades();
        */
        return false;
    }
});

CampaignDonate.Views.HELLSOMETHING = Modal.View.extend({
    template: "#account-auto-merge-content",
    className: 'modal modal-login',

    events: {
        'click a.close': 'cancel',
        "submit .modal-login-form": "sendEmail"
    },

    sendEmail: function (e) {
        e.preventDefault();
        var data = this.$(".modal-login-form").serializeObject();
        this.model.set(data);
//        StorageUtils.set("campaigns.donate.selectedShare", this.model.get('selectedShare'));
        this.model.sendEmail(data, {
            success: function () {
                var jForm = $('<form action="' + workspace.serviceUrls.casHost + '/login" method="post">' +
                '<input name="username" value="' + data.email + '"/>' +
                '<input name="password" value="' + data.emailPassword + '"/>' +
                '<input name="rememberMe" type="checkbox" value="true" checked="1"/>' +
                '<input name="successUrl" type="text" value="' + location.href + '"/>' +
                '<input name="service" value="https://' + location.host + '/login/cas" />' +
                '</form>');
                jForm.appendTo('body').submit();
            },
            fatal: function () {
                workspace.appView.showErrorMessage("Ошибка обращения к серверу");
            }
        });
    },

    afterRender: function () {
        this.$('[name=emailPassword]').focus();
    }
});

CampaignDonate.Views.DeliveryPaymentTotal = UserPayment.Views.AmountEditor.extend({
    template: '#delivery-payment-total-template'
});
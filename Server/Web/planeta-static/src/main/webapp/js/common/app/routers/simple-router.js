/**
 * Simple router implementation.
 * Does not use backbone navigation (through hashtags).
 *
 */
var SimpleRouter = BaseRouter.extend(
    _.extend({}, App.Mixins.Media, {

    initialize: function(options) {
        this.appModel = options.appModel;
        this.appView = new BaseAppView({
            model: this.appModel
        });
    },

    /**
     * Overriding navigate for this simple router (Backbone routing is not used)
     * @param path
     */
    navigate: function(path) {
        if (!path) {
            window.location.href = '/';
            return;
        }

        path = path.replace(/^\/?#?\/?/, '');
        window.location.href = path;
    },

    isRouterEnabled: function() {
        return false;
    }
}));
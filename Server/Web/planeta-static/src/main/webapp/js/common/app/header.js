/*globals JobManager, Modal, OAuthUtils, DefaultContentScrollListView, StringUtils, TemplateManager,
 CookieProvider, JsonAuth*/
/**
 * Javascript that renders header.
 *
 * TODO: Return title functional
 */


var Header = {
    validateForm: function (form, response) {
        if (response.success) {
            return true;
        }
        if (response.fieldErrors) {
            $.map(response.fieldErrors, function (value, key) {
                var el = $(form).find('input[name=' + key + ']');
                $(el).closest('.fieldset').addClass('error');
                $(el).after('<span class="help-inline error-message"><span>' + value + '</span></span>');
            });
        } else {
            workspace.appView.showErrorMessage(response.errorMessage);
        }
        return false;
    },
    // 5 minutes period
    UPDATE_ONLINE_STATUS_PERIOD: 300 * 1000,
    IS_RESIZED: false,
    bindForm: function (form, success, error) {
        var self = this;
        $(form).submit(function (e) {
            e.preventDefault();
            var data = $(this).serialize();
            var url = $(this).attr('action');
            var method = $(this).attr('method');

            $(form).find('.fieldset').removeClass('error');
            $(form).find('span.error-message').remove();

            $.ajax({
                url: url,
                data: data,
                type: method,
                dataType: 'json',
                success: function (response) {
                    if (self.validateForm(form, response)) {
                        if (success) {
                            success();
                        }
                    } else if (error) {
                        error();
                    }
                },
                error: error
            });
//            return false;
        });
    },
    /**
     * Initializes header's javascript.
     *
     * Usage:
     * Header.init({
     *     search: {
     *         searchType: 'users'
     *     }
     * });
     * @param options
     */
    init: function (options) {
        this.initProfileMenu();
        this.initOnlineTracking();
        this.checkRegistrationConfirmedBubble();
        this.authParameters = options.auth;
        loadModule('advertising').fail(function () {
            if (workspace.stats) {
                workspace.stats.trackHasReclameTrimmer();
            }
        });
    },
    initOnlineTracking: function () {
        if (workspace.isAuthorized) {
            //online status
            JobManager.schedule('update-online-status', function () {
                var img = new Image();
                img.src = workspace.serviceUrls.statusServiceUrl + '/count/track-online?u=' + workspace.appModel.get('myProfile').get('profileId');
            }, 0, Header.UPDATE_ONLINE_STATUS_PERIOD, {
                eternal: true
            });
        }
    },
    /**
     * Initializes profile menu
     */
    initProfileMenu: function () {
        $('.pln-dropdown').dropPopup({
            trigger: '.pln-d-switch',
            popup: '> .pln-d-popup',
            closeTrigger: '.pln-d-close',
            activeClass: 'open',
            bottomUp: true,
            open: function (el) {
                el.elem.find('.modal-scroll-content').scrollbar('repaint');
                el.elem.find('.scroll-content').scrollbar('repaint');
            }
        });

        // bind avatar click handler
        $('.js-h-user-dropdown.pln-dropdown').dropPopup({
            trigger: ' .pln-d-switch',
            popup: '> .pln-d-popup',
            activeClass: 'open',
            closeTrigger: ' .h-user_popup',
            bottomUp: true
        });
    },
    showAuthForm: function (action, options) {
        options = options || {};
        var model = new Header.RegistrationModel({
            action: action || 'signup',
            email: options.email
        });
        var view = new Header.RegistrationView({model: model});
        Modal.showDialog(view).done(function () {
            var pass = view.$('input[name=password]');
            if (options.focusPassword || pass.parent().parent().hasClass("error")) {
                pass.focus();
            }

            if (options.email) {
                view.$('[name=username]').val(options.email);
            }
        });
        return view;
    },
    showPasswordRecoveryForm: function () {
        // Check if we have come to recover our password
        if (Header.authParameters && Header.authParameters.passwordRecovery) {
            var ChangePasswordModal = Modal.OverlappedView.extend({
                template: '#change-password-dialog-template',
                save: function () {
                    //nothing to do
                },
                afterRender: function () {
                    Modal.OverlappedView.prototype.afterRender.apply(this, arguments);
                    this.$('form').each(function () {
                        Header.bindForm(this, function () {
                            document.location.href = '/';/*"/welcome/cas-redirect.html?" + $.param({successUrl: Header.authParameters.passwordRecoveryRedirectUrl;});*/
                        });
                    });
                }
            });
            var view = new ChangePasswordModal({
                model: new BaseModel({regCode: Header.authParameters.regCode})
            });
            view.render();
        }
    },
    /**
     *
     */
    showConfirmRegistrationForm: function () {
        $('html, body').animate({scrollTop: 0}, 'slow');
        var confirmRegistration = $('.h-reg-confirm').removeClass('hidden');

        var deactivate = function () {
            confirmRegistration.addClass('hidden');
        };
        confirmRegistration.find('.js-h-reg-close-link').click(deactivate);
        var resendConfirmationEMail = function (e) {
            if (e) {
                e.preventDefault();
            }
            workspace.appModel.resendConfirmationEmail({
                error: function () {
                    workspace.appView.showErrorMessage("Ошибка при отправке письма.");
                },
                success: function (resp) {
                    if (!resp.success) {
                        workspace.appView.showErrorMessage("Ошибка при отправке письма.");
                    } else {
                        workspace.appView.showSuccessMessage("Письмо отправлено.");
                    }
                }
            });
            deactivate();
        };
        confirmRegistration.find('.js-h-reg-confirm-link').click(resendConfirmationEMail);
    },
    showReminderForm: function () {
        var view = new Header.RegistrationReminder({model: new BaseModel({})});
        view.render();
    },
    checkRegistrationConfirmedBubble: function () {
        if (document.location.href.indexOf('/interactive') > 0) {
            return true;
        }
        if (!workspace.appModel.isRegistrationConfirmed()) {
            if (workspace.appModel.hasEmail()) {
                this.showConfirmRegistrationForm();
            }
            return false;
        }
        return true;
    },
    checkRegistrationConfirmed: function () {
        if (!workspace.appModel.isRegistrationConfirmed()) {
            if (workspace.isAuthorized) {
                this.showReminderForm();
            }
            return false;
        }
        return true;
    },
    checkAuthorization: function () {
        return this.checkNotAnonymous() && this.checkRegistrationConfirmed();
    },
    checkNotAnonymous: function () {
        if (!workspace.isAuthorized) {
            this.showAuthForm('signup');
            return false;
        }
        return true;
    }
};

Header.RegistrationModel = BaseModel.extend({
    fetch: function (options) {
        // do nothing on fetch
        if (options && options.success) {
            options.success();
        }
    },
    rememberPassword: function (form) {
        var self = this;
        var options = {
            url: form.attr('action'),
            data: $(form).serialize(),
            success: function (response) {
                if (response) {
                    if (response.success) {
                        self.set({action: 'signup'});
                        workspace.appView.showSuccessMessage('На ваш адрес отправлено письмо');
                    } else {
                        form.find('.js-error-able').each(function (index, elem) {
                            $(elem).addClass('error');
                        });
                        form.find('.js-error-email').text(response.errorMessage);
                    }
                }
            }
        };

        return Backbone.sync('update', this, options);
    }
});


Header.RegistrationReminder = Modal.View.extend({
    template: '#anonymous-reminder-dialog',
    className: 'modal reg-confirm',
    events: {
        'click .close': 'cancel',
        'click .cancel': 'cancel',
        'click .repeat-email': 'onRepeatEmail'
    },
    onRepeatEmail: function (e) {
        if (e) {
            e.preventDefault();
        }
        workspace.appModel.resendConfirmationEmail({
            error: function () {
                workspace.appView.showErrorMessage("Ошибка при отправке письма.");
            },
            success: function () {
                workspace.appView.showSuccessMessage("Письмо отправлено.");
            }
        });
        this.cancel();
    }
});

Header.RegistrationView = Modal.View.extend({
    template: '#auth-modal-dialog',
    className: 'modal modal-login',
    events: {
        'click .close': 'cancel',
        'click .signup-link': 'onLogInLinkClicked',
        'click .registration-link': 'onRegistrationLinkClicked',
        'click .remember-link': 'onRememberLinkClicked',
        'keydown input': 'onKeyDown',
        'click .back-from-remember': 'onLogInLinkClicked',
        'click .btn-primary:not(.disabled)': 'onPrimaryButtonClicked',
        'click [data-action=suggestion]': 'onSuggestionClicked',
        'submit': 'onSubmit'
    },
    getFormParamsArray: function () {
        var formDataJson = {};
        this.$('form').serializeArray().map(function (item) {
            formDataJson[item.name] = item.value;
        });
        return formDataJson;
    },
    onSubmit: function (e) {
        e.preventDefault();
        this.actionHandler();
    },
    removeErrors: function () {
        this.$('[class*=js-error-able]').each(function (index, elem) {
            $(elem).removeClass('error');
        });
        this.$('.js-error-email').text('');
        this.$('.js-error-password').text('');
    },
    onSuggestionClicked: function (e) {
        var $email = this.$('[name=email]');
        $email.val($(e.target).text());
        this.removeErrors();
    },
    actionHandler: function () {
        var formParamsArray = this.getFormParamsArray();
        var action = this.model.get('action');
        if (action) {
            this.removeErrors();
            var self = this;
            if (action === 'signup') {
                this.$('.btn-primary').addClass('disabled');
                JsonAuth.loginDfd(formParamsArray.username, formParamsArray.password, /localhost/.test(location.href))
                        .done(function (response) {
                            if (response && response.success === false) {
                                self.$('.js-error-able').each(function (index, elem) {
                                    $(elem).addClass('error');
                                });
                                self.$('.js-error-password').text(response.errorMessage);
                            }
                        }).fail(function () {
                    workspace.appView.showErrorMessage('Нет ответа от сервера. Повторите попытку.');
                }).complete(function () {
                    self.$('.btn-primary').removeClass('disabled');
                });
            }
            if (action === 'registration') {
                this.$('.btn-primary').addClass('disabled');
                JsonAuth.registerDfd(formParamsArray.email, formParamsArray.password)
                        .done(function (response) {
                            if (response && response.success === false) {
                                if (response.fieldErrors && response.fieldErrors.email) {
                                    self.$('.js-error-email').text(response.fieldErrors.email);
                                    self.$('.js-error-able-email').addClass('error');
                                }
                                if (response.fieldErrors && response.fieldErrors.password) {
                                    self.$('.js-error-password').text(response.fieldErrors.password);
                                    self.$('.js-error-able-password').addClass('error');
                                }
                            }
                        }).fail(function () {
                    workspace.appView.showErrorMessage('Нет ответа от сервера. Повторите попытку.');
                }).complete(function () {
                    self.$('.btn-primary').removeClass('disabled');
                }).success(function () {
                    if (window.gtm) {
                        var event = CookieProvider.get('gtmRegistration');
                        window.gtm.trackUniversalEvent(event);
                    }
                });
            }
            if (action === 'remember') {
                this.$('.btn-primary').addClass('disabled');
                var form = this.$('form');
                this.model.rememberPassword(form).complete(function () {
                    self.$('.btn-primary').removeClass('disabled');
                });
            }
        }
    },
    onPrimaryButtonClicked: function (e) {
        e.preventDefault();
        this.actionHandler();
    },
    mailCheck: _.debounce(function () {
        var $email = this.$('[name=email]');
        var $error = this.$('.js-error-email');

        $email.mailcheck({
            suggested: function (element, suggestion) {
                if ($email.val() !== suggestion.full) {
                    $error.html('Может быть, вы имели в виду <a data-action="suggestion" href="javascript:void(0)">' + suggestion.full + '</a>?').fadeIn();
                }
            }//,

//            empty: function (element) {
//                console.log(element);
//            }
        });
    }, 500),
    onKeyDown: function (e) {
        var code = e.keyCode || e.which;
        this.removeErrors();
        this.mailCheck();
        if (code === $.ui.keyCode.ENTER) {
            e.preventDefault();
            this.actionHandler();
        }
    },
    onRememberLinkClicked: function (e) {
        e.preventDefault();
        this.model.set({action: 'remember'});
    },
    onLogInLinkClicked: function (e) {
        e.preventDefault();
        this.model.set({action: 'signup'});
    },
    onRegistrationLinkClicked: function (e) {
        e.preventDefault();
        this.model.set({action: 'registration'});
    },
    renderView: function () {
        // Returning previous form to the hidden div
        this.$('form').appendTo('#auth-forms');

        var result = Modal.View.prototype.renderView.call(this);
        var self = this;
        result.done(function () {
            self._afterRenderView();
        });
        return result;
    },
    _afterRenderView: function () {
        var $email = this.$('form').find('[name=email]');
        var emailVal = $email.val();
        // Appending new form
        var action = this.model.get('action');
        // auth form came from jsp and after rerender will be disposed, so use its copy
        var form = $('#' + action + '-form').clone();
        this.$('.modal-body').append(form);
        this.updateFormUrls(form);
        this.bindSocialAuth();

        if (!emailVal) {
            form.find('input[name=email]').focus();
        } else {
            form.find('input[name=username]').val(emailVal);
            form.find('input[name=password]').focus();
        }
        var self = this;
        this.$('#registration-form input').keypress(function (e) {
            if (e.which === 13) {
                $(this).blur();
                self.$('#registration-form .btn').click();
                return false;
            }
        });
    },
    updateFormUrls: function (form) {
        var authUrlMatches = $.getUrlVar('afterAuthorizationRedirectUrl');
        var redirectUrl = authUrlMatches || window.location.href;

        //indexOf returns the position of the string in the other string. If not found, it will return -1:
        if (redirectUrl.indexOf("planeta.ru") !== -1 || redirectUrl.indexOf("localhost") !== -1) {
            form.find('input[name=redirectUrl]').val(redirectUrl);
            form.find('input[name=failureUrl]').val('https://' + workspace.serviceUrls.mainHost + '/?afterAuthorizationRedirectUrl=' + encodeURIComponent(redirectUrl));
            var successUrlInput = form.find('input[name=successUrl]');
            if (!successUrlInput.val()) {
                successUrlInput.val(redirectUrl);
            }
        }
    },
    bindSocialAuth: function () {
        $(this.el).find('.sei-vk').bind('click', function (e) {
            e.stopPropagation();
            e.preventDefault();
            OAuthUtils.vkontakteClick();
        });
        $(this.el).find('.sei-ya').bind('click', function (e) {
            e.stopPropagation();
            e.preventDefault();
            OAuthUtils.yandexClick();
        });
        $(this.el).find('.sei-ok').bind('click', function (e) {
            e.stopPropagation();
            e.preventDefault();
            OAuthUtils.odnoklassnikiClick();
        });
        $(this.el).find('.sei-fb').bind('click', function (e) {
            e.stopPropagation();
            e.preventDefault();
            OAuthUtils.facebookClick();
//            FacebookUtils.drawLoginViaFBButton('social-frame', {type: 1});
        });

        $(this.el).find('.sei-mail').bind('click', function (e) {
            e.stopPropagation();
            e.preventDefault();
            OAuthUtils.mailClick();
        });
    },
    dispose: function () {
        // Returning form to the hidden div
        $(this.el).find('form').appendTo('#auth-forms');
        Modal.View.prototype.dispose.call(this);
    },
    afterRender: function () {
        Modal.View.prototype.afterRender.call(this);
        var form = $('#' + this.model.get("action") + '-form');
        var el = form.find('input[data-autofocus]');
        if (el.val()) {
            form.find("input[name=password]").focus();
        } else {
            el.focus();
        }
    }
});

/*
TODO not used now; if we'll not use dialogs & notifications, remove it
 */
Header.BaseModel = BaseModel.extend({
    // 5 seconds delay before load notifications
    REFRESH_DELAY: 5 * 1000,
    // 2 minutes period
    REFRESH_PERIOD: 120 * 1000,
    REFRESH_INC_PERIOD: 0,
    // Maximum number of events shown in popup
    EVENTS_LIMIT: 100,
    defaults: {
        open: false,
        loading: false,
        loaded: false,
        empty: true,
        unreadCount: null,
        collectionUrl: null,
        collectionModel: null
    },
    initialize: function () {
        this.bind('change:open', this.onOpened, this);
        if (this.collectionUrl) {
            this.set({
                collection: new BaseCollection([], {url: this.collectionUrl, limit: 150}),
                loadingCollection: new BaseCollection([], {url: this.collectionUrl, limit: -1})
            });
        }
        this._initJob();
    },
    _initJob: function () {
        // TODO: Add authorized check
        // Scheduling count update

        JobManager.schedule(this.getJobName(), this.loadCount, this.REFRESH_DELAY, this.REFRESH_PERIOD, {
            eternal: true,
            context: this,
            periodFunc: _.bind(this._jobPeriodFunc, this)
        });
    },
    getJobName: function () {
        return 'events-loader-' + this.cid;
    },
    _jobPeriodFunc: function (period) {
        var newPeriod = _.isNumber(period) ? period : this.REFRESH_PERIOD;
        if (this.REFRESH_INC_PERIOD) {
            newPeriod += this.REFRESH_INC_PERIOD;
        }
        return newPeriod;
    },
    restartJob: function () {
        JobManager.removeJob(this.getJobName());
        this._initJob();
    },
    /**
     * Called when popup has been opened
     */
    onOpened: function () {
        if (!this.get('open')) {
            return;
        }

        if (this.get('empty') && !this.get('loaded')) {
            this.set({loading: true, loaded: true});
            this.load();
        }
    },
    /**
     * Called when events collection has been loaded
     */
    onLoaded: function (loadingCollection) {
        loadingCollection = loadingCollection || this.get('loadingCollection');

        var collection = this.get('collection');
        var self = this;
        var filtered = collection.filter(function (model) {
            return !loadingCollection.find(function (currentModel) {
                return self.isEqualModels(model, currentModel);
            });
        });
        var models = [];
        loadingCollection.each(function (model) {
            if (models.length < self.EVENTS_LIMIT) {
                models.push(model);
            }
        });
        _(filtered).each(function (model) {
            if (models.length < self.EVENTS_LIMIT) {
                models.push(model);
            }
        });
        collection.reset(models);

        var unreadCount = loadingCollection.size();
        this.set({
            unreadCount: unreadCount,
            empty: collection.size() === 0
        });
    },
    /**
     * Method for loading elements list
     */
    load: function () {
        var loadingCollection = this.get('loadingCollection');

        var self = this;
        if (this.loadLessThen) {
            loadingCollection.limit = this.loadLessThen;
        }
        loadingCollection.load({
            success: function () {
                self.isNotAllLoaded = self.loadLessThen && loadingCollection.length === self.loadLessThen;
                self.onLoaded();
                self.set({
                    loading: false,
                    loaded: true
                });

            }
        });
    },
    /**
     * Checks if left model is equal to right model.
     * User when events collection is reloading.
     * This method must be overridden.
     *
     * @param left
     * @param right
     */
    isEqualModels: function () {
        throw 'Must be overridden';
    },
    /**
     * Method for loading elements count
     */
    loadCount: function () {
        // This method is the same for
        this.load();
    },
    decrementUnreadCount: function () {
        if (this.get("isNotAllLoaded")) {
            return;
        }
        var unreadCount = this.get('unreadCount');
        if (unreadCount > 0) {
            this.set({
                unreadCount: unreadCount - 1
            });
        }
        if (this.get("collection").length === 0) {
            this.set("empty", true);
        }
    }
});

/*
TODO not used now; if we'll not use dialogs & notifications, remove it
 */
Header.MessagesModel = Header.BaseModel.extend({
    REFRESH_DELAY: 3 * 1000,
    REFRESH_PERIOD: 30 * 1000,
    REFRESH_INC_PERIOD: 1000,
    collectionUrl: function () {
        return '//' + workspace.serviceUrls.imServiceUrl + '/dialogs-unread.json';
    },
    getHeaderText: function (unreadsCount) {
        if (unreadsCount) {
            return StringUtils.declOfNumWithNum(unreadsCount, ['новое', 'новыx', 'новых']) + ' ' + StringUtils.declOfNum(unreadsCount, ['сообщение', 'сообщения', 'сообщений']);
        }
        return 'Нет новых сообщений';
    },
    isEqualModels: function (left, right) {
        if (!left.get('dialog') && !right.get('dialog')) {
            return false;
        }
        return left.get('dialog').dialogId == right.get('dialog').dialogId;
    },
    onLoaded: function () {
        var loadingCollection = this.get('loadingCollection');
        if (loadingCollection.length) {
            workspace.appModel.get('dialogsController').onUnreadMessagesLoaded(loadingCollection);
        }

        Header.BaseModel.prototype.onLoaded.call(this);
//        this.set({unreadCount: this.get('collection').size()});
    },
    onDialogsFriendsLoaded: function (loadedCollection) {
        var self = this;
        var collection = this.get('collection');
        var filtered = collection.filter(function (model) {
            return !loadedCollection.find(function (loadedModel) {
                return self.isEqualModels(model, loadedModel);
            });
        });


        var loadingCollection = this.get('loadingCollection');
        loadingCollection.reset();
        loadingCollection.add(filtered);
        loadedCollection.each(function (model) {
            loadingCollection.add(model);
        });

        loadingCollection.comparator = function (model1, model2) {
            var time1 = model1.get('lastMessage').timeAdded;
            var time2 = model2.get('lastMessage').timeAdded;
            return time1 == time2 ? 0 : time1 > time2 ? 1 : -1;
        };

        loadingCollection.sort();

        this.onLoaded();
    }
});

/*
TODO not used now; if we'll not use dialogs & notifications, remove it
 */
Header.EventsModel = BaseModel.extend({
    initialize: function () {

        this.isActive = true;
        this.unreadsCount = 0;
        this.messagesModel = new Header.MessagesModel({});

        //this.requestsModel.bind('change:open', this.onIconOpened, this);
        this.messagesModel.bind('change:open', this.onIconOpened, this);

        var self = this;

        $(window).focus(function () {
            self.isActive = true;
            self.displayUnreadsCount(self.unreadsCount);
        });

        $(window).blur(function () {
            self.isActive = false;
            self.displayUnreadsCount(self.unreadsCount);
        });
    },
    displayUnreadsCount: function (unreadsCount) {
        if (!window.pageTitle) {
            return;
        }
        if (this.isActive || !unreadsCount) {
            document.title = window.pageTitle;
        } else {
            document.title = '(' + unreadsCount + ') ' + window.pageTitle;
        }
    },
    onIconOpened: function (model) {
        if (!model.get('open')) {
            return;
        }
//        _.each([this.requestsModel, this.messagesModel], function (childModel) {
//            if (childModel != model) {
//                childModel.set({
//                    open: false
//                });
//            }
//        });
    },
    lastMessageHasBeenRead: function (dialogId) {
        var self = this;
        var collection = this.messagesModel.get('collection');
        collection.each(function (model) {
            if (model.get('dialog').dialogId == dialogId) {
                collection.remove(model);
                self.messagesModel.decrementUnreadCount();
            }
        });
    }
});

/**
 * Main view that controls small icon-views (friends, messages and notifications)
 */
/*
TODO not used now; if we'll not use dialogs & notifications, remove it
 */

Header.EventsView = BaseView.extend({
    el: '.header-info-action',
    construct: function () {
        this.notificationsView = new Header.IconView({
            el: $('.js-notifications-container'),
            model: this.model.notificationsModel
        });
        this.messagesView = new Header.IconView({
            el: $('.js-messages-container'),
            model: this.model.messagesModel
        });

//        this.requestsView = new Header.IconView({
//            el: $('.js-requests-container'),
//            model: this.model.requestsModel
//        });


        var globalNotificationModel = new BaseModel();
        globalNotificationModel.set({
            notificationsModel: this.model.notificationsModel,
            messagesModel: this.model.messagesModel
                    //requestsModel: this.model.requestsModel
        });
        this.globalNotificationsView = new Header.GlobalNotificationView({
            el: $('.js-h-btn_count'),
            model: globalNotificationModel
        });
    }
});

/*
TODO not used now; if we'll not use dialogs & notifications, remove it
 */
Header.IconView = BaseView.extend({
    modelEvents: {
        'change:unreadCount': 'onUnreadCountsChange',
        'change:loading': 'onLoadingChanged'
    },
    // events: {
    //     'click .h-user_menu_link, .m-menu_link': 'onLinkClicked'
    // },
    onLinkClicked: function (e) {
        if ($(e.currentTarget).hasClass('js-messages-link')) {
            workspace.appModel.get('dialogsController').showDialogsSearch();
        } else {
            if ($(e.target).is('a')) {
                if (!workspace.isRouterEnabled()) {
                    window.location.href = $(e.target).attr("href");
                }
            } else {
                $(e.target).find('a').click();
            }
        }
    },
    onLoadingChanged: function () {
        var topDropBlock = this.$('.top-drop-block-body');

        if (this.model.get('loading')) {
            topDropBlock.show();
            topDropBlock.find('.loading').remove();
            topDropBlock.append($('<div class="top-drop-block-loader"><i class="icon-load"></i> Подождите, идет загрузка...</div>'));
        } else {
            topDropBlock.find('.top-drop-block-loader').remove();
        }
    },
    render: function () {
        var unreadCount = this.model.get('unreadCount');
        if (this.model.isNotAllLoaded) {
            unreadCount = (this.model.loadLessThen - 1) + "+";
        }
        if (unreadCount) {
            this.$('.h-user_menu_notif').show().text(unreadCount);
        } else {
            this.$('.h-user_menu_notif').hide();
        }
    },
    onUnreadCountsChange: function () {
        this.render();
    }
});

/*
TODO not used now; if we'll not use dialogs & notifications, remove it
 */
Header.GlobalNotificationView = BaseView.extend({
    construct: function () {
        var self = this;
        this.model.get('messagesModel').bind('change', self.render, this);
    },
    render: function () {
        var msg = this.model.get('messagesModel');
        var unreadCount = msg.get('unreadCount');
        if (unreadCount) {
            this.$el.show();
            this.$el.text(unreadCount);
        } else {
            this.$el.hide();
        }
    }
});

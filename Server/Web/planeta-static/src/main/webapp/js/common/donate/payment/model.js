/*globals CampaignDonate, UserPayment*/
/**
 *
 * @author Andrew.Arefyev@gmail.com
 * @date 19.08.13, 15:33
 */
if (!window.CampaignDonate) {
    var CampaignDonate = {};
}
CampaignDonate.Models = CampaignDonate.Models || {};

CampaignDonate.Models.Payment = UserPayment.Models.OrderPayment.extend({

    initialize: function () {
        
        this.controller = this.get('controller');
        this.unset('controller');

        UserPayment.Models.OrderPayment.prototype.initialize.call(this, arguments);

        this.set({
            parent: this.controller.attributes,
            planetaPurchase: false
        });
    }
});
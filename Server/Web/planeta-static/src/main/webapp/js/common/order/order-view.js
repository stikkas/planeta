/*globals TemplateManager, Orders, Order*/

Orders.Views = {

    togglePopup: function(toggleEl, toggleClass, oneActive) {
        var el = $(toggleEl);
        if (el.hasClass(toggleClass)) {
            el.removeClass(toggleClass);
        } else {
            if (_.isBoolean(oneActive) && oneActive) {
                $('.' + toggleClass).removeClass(toggleClass);
            }
            el.addClass(toggleClass);
        }
    }

};

Orders.Views.OrderStatusChanger = BaseView.extend({
    className: 'dropdown-popup',
    template: '#order-status-changer-template',
    events: {
        'click .cancel-change': 'togglePopup'
    },
	viewEvents: {
        'click .perform-change': 'onChangeStatus'
	},

    construct: function(options) {
        Orders.initProperty(options, this, 'hasCaret', true);
        Orders.initProperty(options, this, 'batchChanger', false);
    },

    afterRender: function() {
        this.$el.find('select.status').dropDownSelect();
        if (!this.hasCaret) {
            this.$el.find('.dropdown-popup-caret').remove();
        }
    },

    togglePopup: function() {
        var parent = this.$el.parent();
        var height = $(window).height();
        var scroll = window.scrollY;
        var position = parent.position();
        if (position.top > scroll + height / 2) {
            parent.addClass('bottom-up');
        } else {
            parent.removeClass('bottom-up');
        }
        Orders.Views.togglePopup(parent, 'popup-open', true);
    },

    getStatuses: function() {
        var result = {};
        _.each($('.status', this.$el), function(statusSelect) {
            var name = _.find($(statusSelect).attr('class').split(/\s+/), function (clazz) {
                return clazz !== 'status';
            });
            var value = $('option:selected', statusSelect).val();
            var status = {};
            status[name] = value;
            _.extend(result, status);
        });
        return result;
    },

    isBatchChanger: function() {
        return this.batchChanger;
    }

});

Orders.Views.BatchActionsPanel = BaseView.extend({
    template: '#order-batch-actions-panel-template',
    statusChangerType: Orders.Views.OrderStatusChanger,
    modelEvents: {
        'change' : 'updateVisibility'
    },
    events: {
        'click .batch-status-change': 'onShowChangeStatusPopup',
        'click .batch-send-mail': 'onSendEmailClicked'
    },

    construct: function(options) {
        Orders.initProperty(options, this, 'model', new BaseModel());
        Orders.initProperty(options, this, 'orders');
        this.orders.bind('selectionChanged afterOrdersLoading', this.updateVisibility, this);

        this.statusChanger = this.addChildAtElement('.dropdown', new this.statusChangerType({
            model: new BaseModel(_.extend(Order.Utils.getStatuses('COMPLETED', 'PENDING'), {batchChanger: true})),
            batchChanger: true,
            hasCaret: false
        }));
    },

    afterRender: function() {
        this.updateVisibility();
    },

    updateVisibility: function() {
        if (this.isActionEnabled()) {
            this.$el.find('a').removeClass('disabled');
        } else {
            this.$el.find('a').addClass('disabled');
        }
    },

    isActionEnabled: function() {
        return !this.orders.isEmpty() && this.orders.hasSelected();
    },

    onShowChangeStatusPopup: function() {
        if (!this.isActionEnabled()) {
	        return;
        }
        this.statusChanger.togglePopup();
    },

    onSendEmailClicked: function() {
        if (!this.isActionEnabled()) {
	        return;
        }
        this.model.sendEmail();
    }

});

Orders.Views.BaseFilter = BaseView.extend({
    className: 'noice-bg',
	events: {
		'click .period': 'onPeriodClicked',
        'change #orderStatus': 'onStatusChanged',
		'keyup #searchString': 'onSearchStringChanged',
        'input #searchString': 'onSearchStringChanged',
        'paste #searchString': 'onSearchStringChanged',
        'drop #searchString': 'onSearchStringChanged'
	},

	afterRender: function() {
		this.$el.find('select.status').dropDownSelect();
		this.bindDatePickers(this.$el.find('input.dates'));
	},

    updateDatePickers: function() {
        var ids = _.keys(this.datePickers);
        var datePickers = this.datePickers;
        var model = this.model;

        _.each(ids, function(id) {
            if (model.has(id)) {
                datePickers[id].datepicker('setDate', new Date(model.get(id)));
            }
        });
    },

    bindDatePickers: function(inputDates) {
        this.datePickers = {};
        var datePickers = this.datePickers;
        var model = this.model;

		_.each(inputDates, function(inputDate) {
            var id = $(inputDate).attr('id');
            datePickers[id] = $(inputDate).datepicker({
                onSelect: function() {
                    var date = datePickers[id].datepicker('getDate');
                    model.silentSetDate(id, date);
                },
                beforeShow: function() {
                    setTimeout(function(){
                        $('#ui-datepicker-div').css({'zIndex': ''})
                    }, 0);
                }
            });
        });
        this.updateDatePickers();
    },

    onPeriodClicked: function(e) {
        e.preventDefault();
        this.model.setTimeFilter(e.target.id);
        this.updateDatePickers();

        $('#periods .active').removeClass('active');
        $(e.target).parent().addClass('active');
    },

    onSearchStringChanged: _.debounce(function(e) {
        var value = $(e.currentTarget).val();
        this.model.setSearchString(value);
    }, 300),

    onStatusChanged: function(e) {
        e.preventDefault();
        this.model.setStatus(e.target.id, $(e.target).find('option:selected').val());
    }
});

Orders.Views.Order = BaseView.extend({
    tagName: 'tr',
	template: '#order-template',
    statusChangerType: Orders.Views.OrderStatusChanger,
    options: [],
    events: {
        'click .change-status': 'onShowChangeStatusPopup',
        'click .details': 'onShowDetails'
    },

	viewEvents: {
		'change input[type="checkbox"]': 'onOrderItemClicked'
	},

	construct: function(options) {
		this.statusChanger = this.addChildAtElement('.dropdown', new this.statusChangerType({
            model: this.model
        }));
	},

    afterRender: function() {
		this.$('input:checkbox').checkbox();
		if (!this.model.get('even')) {
		    this.$el.addClass('even-tr');
        }
	},

    onShowChangeStatusPopup: function() {
        this.statusChanger.togglePopup();
    },

    onShowDetails: function() {
        this.nextGroupView.togglePopup();
    }

});

/*global Audios, StorageUtils, PlayerState, workspace, Form, Modal*/
Audios.Models.Fade = BaseModel.extend({
    INTERVAL: 20,
    DURATION: 300,

    initialize: function () {
        BaseModel.prototype.initialize.apply(this, arguments);
    },

    _clear: function () {
        if (this.timer) {
            clearInterval(this.timer);
            delete this.timer;
        }
        if (this.startFadeIn) {
            delete this.startFadeIn;
        }
        this.isFadeOut = false;
    },

    fadeOut: function (track, volume) {
        var sm = Audios.getSoundManager();
        this._clear();
        this.track = track;

        var self = this;
        var playId = track.get('playId');
        var sound = sm.getSoundById(playId);
        var position = sound.position;
        var curVolume = volume;
        this.isFadeOut = true;
        var startTime;

        this.timer = setInterval(function () {
            if (!startTime) {
                startTime = new Date().getTime() - self.INTERVAL;
            }
            curVolume = volume * (self.DURATION - (new Date().getTime() - startTime)) / self.DURATION;

            if (curVolume <= 0) {
                self._clear();
                sound.pause();
                sound.setPosition(position);
                sm.setVolume(playId, volume);
            } else {
                sm.setVolume(playId, curVolume);
            }
        }, this.INTERVAL);
    },

    fadeIn: function (track, volume) {
        var sm = Audios.getSoundManager();
        this._clear();
        var self = this;
        var playId = track.get('playId');
        var curVolume = 0;
        var sound = sm.getSoundById(playId);
        sm.setVolume(playId, 0);
        sound.play();

        this.startFadeIn = function () {
            delete this.startFadeIn;
            self.timer = setInterval(function () {
                curVolume += self.INTERVAL / self.DURATION * volume;
                if (curVolume >= volume) {
                    self._clear();
                    sm.setVolume(playId, volume);
                } else {
                    sm.setVolume(playId, curVolume);
                }
            }, this.INTERVAL);

        };
    }
});

Audios.Models.Player = BaseModel.extend({
    playlistShuffle: false,
    initialize: function (options) {
        var self = this;
        this.profileId = options.profileId;
        this.fade = new Audios.Models.Fade();

        this._initPlayInfo();

        this.set({
            playingPlaylist: null
        });

        $(window).bind('storage', function (e) {

            var event = e.originalEvent;
            if (event.key != 'audio.player.state') {
                return true;
            }
            var value = StorageUtils.getAudioPlayerState();
            var audioStatus = value.audioStatus;
            if (audioStatus && audioStatus == 'interrupt') {
                self.interrupt();
            } else if (audioStatus && audioStatus == 'resume') {
                self.resume();
            } else if (self.isPlaying()) {
                self.playPause();
            }
        });
    },

    _initPlayInfo: function () {
        this.playInfo = new BaseModel({
            track: null,
            trackPosition: 0,
            trackLoaded: 0,
            volume: 100,
            playerState: PlayerState.STOP,
            isInterrupted: false
        });
    },

    isPlaying: function () {
        return this.playInfo.get('playerState') == PlayerState.PLAY;
    },

    isInterrupted: function () {
        return this.playInfo.get('isInterrupted');
    },

    toggleInterrupted: function () {
        if (!this.isInterrupted() && this.isPlaying()) {
            this.playInfo.set('isInterrupted', true, {silent: true});
            this.playPause();
        } else if (this.isInterrupted() && !this.isPlaying()) {
            this.playInfo.set('isInterrupted', false, {silent: true});
            this.playPause();
        }
    },

    interrupt: function () {
        if (!this.isInterrupted() && this.isPlaying()) {
            this.playInfo.set('isInterrupted', true, {silent: true});
            this.playPause();
        }
    },

    resume: function () {
        if (this.isInterrupted() && !this.isPlaying()) {
            this.playInfo.set('isInterrupted', false, {silent: true});
            this.playPause();
        }
    },

    loadTrack: function (profileId, trackId, success, error) {
        var track = new Audios.Models.Track({
            profileId: profileId,
            trackId: trackId
        });
        var errorCallback = function (errorMessage) {
            if (error) {
                error();
            } else {
                if (errorMessage) {
                    workspace.appView.showErrorMessage(errorMessage);
                } else {
                    workspace.appView.showErrorMessage('Ошибка загрузки аудиотрека');
                }
            }
        };
        track.fetch({
            success: function (model, response) {
                if (!response.success) {
                    errorCallback(response.errorMessage);
                } else {
                    success(new Audios.Models.Track(model.get('result')));
                }
            },
            error: errorCallback
        });
    },

    playTrack: function (track, tracks) {
        StorageUtils.setAudioPlayerState({tabName: window.name});
        if (this.playInfo.get('track')) {
            Audios.getSoundManager().destroySound(this.playInfo.get('track').get('playId'));
            // TODO: why?  aus
            this.playInfo.get('track').set({
                selected: false
            });
            this.playInfo.get('track').trigger('change');
        }

        tracks = tracks || new BaseCollection([track]);
        tracks = this.createPlaylist(tracks).get("tracks");

        this.set({
            playingPlaylist: tracks.playlist
        });
        this.play(track, 0);
        this.playInfo.set({
            track: track
        });

    },

    createPlaylist: function (tracks, options) {
        var playingPlaylist = tracks.playlist;

        if (!tracks.playlist) {
            playingPlaylist = new Audios.Models.Playlist({
                tracks: tracks
            }, options);
        }
        return playingPlaylist;
    },

    play: function (track, position, paused) {
        var self = this;
        var playId = 'play' + track.get('trackId') + (new Date().getTime() % 100000);
        track.set({
            playId: playId,
            selected: true
        });
        if (!track.get('trackUrl')) {
            console.error('not defined trackUrl', track);
            return;
        }
        Audios.getSoundManager().onready(function () {
            var sound = Audios.getSoundManager().createSound({
                id: track.get('playId'),
                volume: self.playInfo.get('volume'),
                url: track.get('trackUrl'),
                onfinish: function () {
                    setTimeout(function () {
                        if (self.playInfo.get('repeat')) {
                            self.play(track, 0);
                        } else {
                            self.next(track, true);
                        }
                    }, 0);
                },
                onload: function (isSuccess) {
                    if (!isSuccess) {
                        track.set({
                            selected: false,
                            isNotFound: true
                        });
                    }
                    //console.log(isSuccess, this.readyState);
                },
                whileloading: _.throttle(function () {
                    self.playInfo.set({
                        trackLoaded: this.bytesLoaded / this.bytesTotal * 100
                    });
                    self.tick(this);
                }, 50),
                whileplaying: function () {
                    if (self.fade.startFadeIn) {
                        self.fade.startFadeIn();
                    }
                    if (!self.fade.isFadeOut) {
                        self.playInfo.set({
                            trackPosition: this.position / this.durationEstimate * 100,
                            trackPositionSec: this.position / 1000
                        });
                    }
                    self.tick(this);
                }
            });
            sound.setPosition(parseInt(this.durationEstimate / 100 * position, 10));

            var playerState = PlayerState.PLAY;
            if (paused) {
                playerState = PlayerState.PAUSE;
            } else {
                try {
                    sound.play();
                } catch (e) {
                    setTimeout(function () {
                        sound.resume();
                    }, 500);
                }
            }
            self.playInfo.set({
                track: track,
                trackPosition: position,
                trackLoaded: sound.bytesLoaded / sound.bytesTotal * 100,
                playerState: playerState
            });
            workspace.stats.trackAudioListen(self.playInfo.get('track'));
        });
    },
    tick: function (sound) {
        if (this.playInfo.get('track').get('playId') != sound.sID) {
            Audios.getSoundManager().destroySound(sound.sID);
        }
    },

    next: function (track, finished) {
        if (track === undefined) {
            track = this.getActiveTrack();
        }
        if (!track) {
            return;
        }
        var playingPlaylist = this.get('playingPlaylist');
        var playingTracks = playingPlaylist && playingPlaylist.get('tracks');
        var trackIndex = playingTracks.indexOf(track);
        if (trackIndex == -1) {
            this.stop(track);
            return;
        }

        var nextTrack;
        if (this.playInfo.get("shuffle") && playingTracks.length > 1) {
            nextTrack = this.nextRandom(playingPlaylist, track);
        } else {
            nextTrack = playingTracks.at(trackIndex + 1);
        }


        if (nextTrack) {
            this.playInfo.get('track').trigger('change');
            this.playInfo.get('track').set({
                selected: false
            });
            Audios.getSoundManager().destroySound(track.get('playId'));
            this.play(nextTrack, 0, this.playInfo.get('playerState') == PlayerState.PAUSE);
        } else {
            if (finished) {
                this.stop(track);
            }
        }
    },

    nextRandom: function (playlist, track) {

        var trackId = track.get('trackId');
        var tracks = playlist.get('tracks');
        var playedTrackIds = playlist.get('playedTrackIds') || [];

        playedTrackIds = _.without(playedTrackIds, trackId);
        playedTrackIds.push(trackId);
        var trackIds = tracks.pluck('trackId');
        var nonPlayedTrackIds = _.difference(trackIds, playedTrackIds);

        if (playlist.get("playlistType") === Audios.PlaylistType.SEARCH) {
            if (nonPlayedTrackIds.length < playedTrackIds.length && !tracks.allLoaded) {
                tracks.loadNext();
            }
            if (tracks.allLoaded && nonPlayedTrackIds.length === 0) {
                playedTrackIds = [trackId];
            }
        } else {
            if (playedTrackIds.length > 5) {
                playedTrackIds = _.rest(playedTrackIds, playedTrackIds.length - 5);
            }
            if (!tracks.allLoaded) {
                tracks.loadNext();
            }
        }
        if (nonPlayedTrackIds.length > 0) {
            trackIds = nonPlayedTrackIds;
        } else {
            if (trackIds.length > 1) {
                trackIds = _.without(trackIds, trackId);
            }
        }

        var nextTrackId = trackIds[Math.floor(Math.random() * trackIds.length)];

        var nextTrack = tracks.find(function (trk) {
            return trk.get('trackId') === nextTrackId;
        });

        playlist.set('playedTrackIds', playedTrackIds);
        return nextTrack;
    },

    stop: function (track) {
        if (this.playInfo.get('playerState') == PlayerState.PLAY) {
            var sound = Audios.getSoundManager().getSoundById(track.get('playId'));
            sound.pause();
            sound.setPosition(0);
            this.playInfo.set({
                playerState: PlayerState.STOP,
                trackPosition: 0,
                trackPositionSec: 0
            });
        }

    },

    prev: function (track) {
        if (track === undefined) {
            track = this.getActiveTrack();
        }
        if (!track) {
            return;
        }

        var playingPlaylist = this.get('playingPlaylist');
        var playingTracks = playingPlaylist && playingPlaylist.get('tracks');
        var trackIndex = playingTracks.indexOf(track);
        if (trackIndex <= 0) {
            return;
        }
        var nextTrack = playingTracks.at(trackIndex - 1);
        this.playInfo.get('track').trigger('change');
        this.playInfo.get('track').set({
            selected: false
        });
        if (nextTrack) {
            Audios.getSoundManager().destroySound(track.get('playId'));
            this.play(nextTrack, 0, this.playInfo.get('playerState') == PlayerState.PAUSE);
        } else {
            this.playInfo.set({
                playerState: PlayerState.STOP
            });
        }
    },
    rewind: function (track, position) {
        var sound = Audios.getSoundManager().getSoundById(track.get('playId'));
        this.playInfo.set({
            trackPosition: position
        });
        if (sound) {
            sound.setPosition(parseInt(sound.durationEstimate / 100 * position, 10));
        }
    },
    setVolume: function (track, volume) {
        this.playInfo.set({
            volume: volume
        });
        if (track) {
            Audios.getSoundManager().setVolume(track.get('playId'), volume);
        }
    },
    playPause: function (track, tracks) {
        var activeTrack = this.playInfo.get('track');
        if (track && (track.get("selected") ? activeTrack.get('trackId') !== track.get('trackId') : activeTrack !== track)) {
            this.playTrack(track, tracks);
            return;
        }
        var sound = null;
        if (!track && this.playInfo.get('playerState') == PlayerState.STOP) {
            if (this.get('playingPlaylist')) {
                this.playTrack(this.get('playingPlaylist').get('tracks').at(0), this.get('playingPlaylist').get('tracks'));
            }
        } else if (this.playInfo.get('playerState') !== PlayerState.PLAY) {
            this.fade.fadeIn(this.playInfo.get('track'), this.playInfo.get("volume"));
            if (this.isInterrupted()) {
                this.resume();
            }
            StorageUtils.setAudioPlayerState({audio: window.name});
            this.playInfo.set({
                playerState: PlayerState.PLAY
            });
        } else {
            this.fade.fadeOut(this.playInfo.get('track'), this.playInfo.get("volume"));
            this.playInfo.set({
                playerState: PlayerState.PAUSE
            });
        }
    },

    getActiveTrack: function () {
        return this.playInfo.get('track');
    },

    onTrackChanged: function (track) {
        this.trigger("onTrackChanged", track);
    }

});

Audios.Models.Playlist = BaseModel.extend({
    modelEvents: {
        "playInfo change:playerState,track": "onPlayerStateChanged"
    },

    initialize: function (attrs, options) {
        var tracks = attrs.tracks, self = this;

        if (attrs && !attrs.playlistType) {
            if (tracks && workspace.appModel.isMyProfile(tracks.profileId) && !tracks.playlistId) {
                this.set({
                    playlistType: Audios.PlaylistType.ALL_MY_TRACKS,
                    name: "Мои аудиозаписи"
                });
            } else {
                this.set("playlistType", Audios.PlaylistType.PLAY_LIST);
            }
        }
        var parentObjectId = tracks && tracks.parentObjectId;
        if (!(options && options.noCreateTracks)) {
            if (!(tracks instanceof Audios.Models.BaseTracks)) {
                if (tracks instanceof Backbone.Collection) {
                    tracks = tracks.models;
                }
                tracks = new Audios.Models.Tracks(tracks, {
                    profileId: this.get('profileId'),
                    playlistId: this.get('playlistId')
                });
            }
        }

        tracks.parentObjectId = parentObjectId;

        BaseModel.prototype.initialize.apply(this, arguments);

        this.controller = (options && options.controller) || workspace.appModel.audioPlayer;
        this.playInfo = this.controller.playInfo;
        this.playInfo.bind("change:playerState change:track", this._setPlayerStateToModels, this);
        tracks.bind("onLoaded add remove reset", this._setPlayerStateToModels, this);
        tracks.bind("add", function () {
            self.set("tracksCount", self.get("tracksCount") + 1);
        });
        tracks.bind("remove", function () {
            self.set("tracksCount", self.get("tracksCount") - 1);
        });
        this.controller.bind("onTrackChanged", this.onTrackChanged, this);
        tracks.playlist = this;
        this.set({
            tracks: tracks
        });
    },

    destroy: function () {
        BaseModel.prototype.destroy.apply(this, arguments);
        this.playInfo.unbind("change:playerState change:track", this._setPlayerStateToModels, this);
        var tracks = this.get("tracks");
        if (tracks) {
            tracks.unbind("onLoaded onAdd onRemove onReset", this._setPlayerStateToModels, this);
        }
        this.controller.unbind("onTrackChanged", this.onTrackChanged, this);
    },

    equals: function (playlist) {
        if (!playlist) {
            return false;
        }
        if (playlist === this) {
            return true;
        }
        var playlistType = this.get('playlistType');
        if (playlistType === Audios.PlaylistType.ALL_MY_TRACKS || playlistType === Audios.PlaylistType.HISTORY) {
            return playlistType === playlist.get('playlistType');
        }

        if (this.get("tracks").parentObjectId && this.get("tracks").parentObjectId == playlist.get("tracks").parentObjectId) {
            return true;
        }
        if (playlistType === Audios.PlaylistType.PLAY_LIST) {
            var tracks = this.get('tracks');
            return (tracks && tracks == playlist.get('tracks'));
        }
        return false;
    },

    _setPlayerStateToModels: function () {
        var playerState = this.playInfo.get('playerState');
        var activeTrack = this.playInfo.get('track');
        var activeTrackId = activeTrack && activeTrack.get('trackId');
        var playingPlaylist = this.controller.get("playingPlaylist");
        var tracks = this.get('tracks');
        var areTheseTracksActive = playingPlaylist && playingPlaylist.equals(tracks.playlist);

        var isThisHistory = tracks.playlist && tracks.playlist.get("playlistType") === Audios.PlaylistType.HISTORY;
        var selectedTrackInHistory;

        if (isThisHistory && tracks.indexOf(activeTrack) >= 0) {
            selectedTrackInHistory = activeTrack;
        }

        var activeTrackOrderNumber;
        if (tracks.filterByAttr("trackId", activeTrackId).length > 1) {
            activeTrackOrderNumber = activeTrack.get("orderNumber");
        }


        _(tracks.toArray().reverse()).each(function (track) {
            var isThisTrackActive = areTheseTracksActive && activeTrackId && track.get('trackId') === activeTrackId;
            if (activeTrackOrderNumber && isThisTrackActive && activeTrackOrderNumber != track.get('orderNumber')) {
                isThisTrackActive = false;
            }
            var selected = false;
            if (isThisHistory) {
                if (selectedTrackInHistory) {
                    selected = track === selectedTrackInHistory;
                } else {
                    if (activeTrackId && track.get('trackId') === activeTrackId) {
                        selectedTrackInHistory = track;
                        selected = true;
                    }
                }
            } else {
                selected = isThisTrackActive;
            }

            track.set({
                selected: selected,
                playerState: selected ? playerState : PlayerState.PAUSE
            });
        });


    },

    onTrackChanged: function (newTrack) {
        var tracks = this.get('tracks');
        if (tracks) {
            tracks.each(function (track) {
                if (track.get("trackId") === newTrack.get("trackId")) {
                    newTrack.unset('orderNumber');
                    track.set(newTrack.attributes);
                }
            });
        }
    }
});

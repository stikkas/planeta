/*globals PrivacyUtils, ProfileUtils, ImageUtils, ImageType, Form, Modal, Comments, StringUtils*/

//noinspection MagicNumberJS
var Photo = {
    COMMENTS_LIMIT: 4,
    Models: {},
    Views: {},
    NUMBER_OF_NEARBY_PRELOADED_PHOTOS: 3,
    SHOW_MESSAGE_DELAY: 2500,
    // see modals.less .photo-block .photo-fullscreen .transition(all .3s ease);
    FULL_SCREEN_CSS_EASING_DELAY: 300,
    imageUID: 1
};

Photo.Models.CommentedPhotoItem = BaseModel.extend({
    initialize: function (options) {
        BaseModel.prototype.initialize.call(this, options);
        this.set('commentsModel', new Comments.Models.Comments({
            profileId: this.get('profileId'),
            objectId: this.get('photoId'),
            objectType: 'PHOTO',
            limit: Photo.COMMENTS_LIMIT
        }, {
            commentableObject: this
        }));
        if (!this.has('nearbyPhotos')) {
            this.set('nearbyPhotos', []);
        }
    },

    url: function () {
        return '/api/public/photo.json?profileId=' + this.get('profileId') + '&photoId=' + this.get('photoId');
    },

    fetch: function (options) {
        if (this.has('imageUrl')) {
            if (options && options.success && _.isFunction(options.success)) {
                options.success.call(this, this, this);
            }
            return;
        }
        BaseModel.prototype.fetch.call(this, options);
        if (!this.get('commentsModel').commentsList.allLoaded) {
            this.get('commentsModel').fetch();
        }
    },

    setImage: function ($img) {
        if (!$.support.leadingWhitespace) {// ie 8 DispHtmlImg object crashes Backbone _.isEquals
            this.set({
                loadedImgSrc: $img[0].src
            }, {silent: true});
        } else {
            this.set({
                loadedImgSrc: $img[0].src,
                loadedImg: $img
            }, {silent: true});
        }
    },

    loadImage: function (options) {
        var success = options.success || options;
        var error = options.error || options;
        var imageUrl = this.get('imageUrl');
        if (!imageUrl) {
            error();
            return;
        }
        if (this.has('loadedImgSrc')) {
            success();
        }
        var $img = $('<img>', {
            src: ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.REAL, ImageType.PHOTO)
        });

        var self = this;
        if ($img[0].complete) {
            this.setImage($img);
            success();
        } else {
            $img.load(function count() {
                self.setImage($img);
                success();
            });
        }
    }
});

/**
 * @example
 * var carousel = Photo.Models.PhotoModal({
 *     photos: [{profileId: 196, photoId: 666}, {profileId: 196, photoId: 667}],
 *     activeIndex: 1
 * })
 */
Photo.Models.PhotoModal = BaseModel.extend({
    initialize: function (options) {

        var collection = [];
        if (options) {
            // specify collection as options
            if (_.isArray(options)) {
                collection = options;
                // or by 'photos' key
            } else {
                collection = options.photos || [];
            }
            if (options.collection) {
                this.collection = options.collection;
            }
        }
        this.photos = _.map(collection, function (value) {
            return new Photo.Models.CommentedPhotoItem(value);
        });
        var activePhotoIndex = 0;
        if (options && options.activeIndex) {
            activePhotoIndex = options.activeIndex;
        }
        var activePhoto = collection[activePhotoIndex];
        Photo.Models.CommentedPhotoItem.prototype.initialize.call(this, activePhoto);
        this.currentPhoto = this.photos[activePhotoIndex];
        this.setPhotoIndex(activePhotoIndex);
    },

    fetch: function (options) {
        var self = this;
        this.currentPhoto.fetch({
            success: function (model, resp) {
                model.loadImage({
                    success: function () {
                        self.set({photo: model.toJSON()});
                        self.loadNearbyPhotos();
                        if (options && options.success && _.isFunction(options.success)) {
                            options.success(self, resp);

                        }
                        if (self.collection) {
                            var photoList = self.collection;
                            photoList.limit = 0;
                            photoList.offset = 0;
                            photoList.load({
                                success: function () {
                                    self.photos = _.map(photoList.models, function (value) {
                                        return new Photo.Models.CommentedPhotoItem(value);
                                    });
                                    self.set('photos', self.photos, {silent: true});
                                    //                                self.setPhotoIndex(self.get('activeItemIndex'));
                                }
                            });
                        } else {
                            self.parallel(self.photos);
                        }
                    },
                    error: function () {
                        if (options && options.error && _.isFunction(options.error)) {
                            options.error(self, resp);

                        } else {
                            options.success(self, resp);
                        }
                    }
                });
            }
        });

    },
    /**
     * Switch active photo by index
     * @param {Number} index
     */
    setPhotoIndex: function (index) {
        this.set({activeItemIndex: index}, {silent: true});
        var currentPhoto = this.photos[index];
        var self = this;
        if (!currentPhoto.has('imageUrl')) {
            return;
        }
        currentPhoto.loadImage({
            success: function () {
                self.currentPhoto = currentPhoto;
                self.set('photo', currentPhoto.toJSON()); // for easy template
                self.loadNearbyPhotos();
            },
            error: function () {
            }
        });
    },

    getNearbyPhotos: function () {
        var indexes = [];
        var idx = this.get('activeItemIndex');
        var i;
        for (i = -Photo.NUMBER_OF_NEARBY_PRELOADED_PHOTOS; i <= Photo.NUMBER_OF_NEARBY_PRELOADED_PHOTOS; i += 1) {
            if (i !== 0) {
                indexes.push((idx - i + this.photos.length) % this.photos.length);
            }
        }

        return _.filter(this.photos, function (value, index) {
            return _.contains(indexes, index);
        });
    },

    loadNearbyPhotos: function () {
        var self = this;
        _.each(self.getNearbyPhotos(), function (photo) {
            photo.loadImage({
                success: function () {
                    var isLoaded = function (el) {
                        return el === photo.get('loadedImgSrc');
                    };
                    if (!_.find(self.get('nearbyPhotos'), isLoaded)) {
                        self.get('nearbyPhotos').push(photo.get('loadedImgSrc'));
                    }
                },
                error: function () {
                }
            });
        });

    },

    pageData: function () {
        var currentPhoto = this.currentPhoto;
        return {
            title: _.template('<%=albumName%> | фото <%=photoDescription%>', {
                albumName: currentPhoto.get('photoAlbumName'),
                photoDescription: currentPhoto.get('description') || ""
            }),
            description: _.template('Фотография из фотоальбома «<%=albumName%>». Ещё больше фотографий в сообществе «<%=groupName%>» на Planeta.ru', {
                albumName: currentPhoto.get('photoAlbumName'),
                groupName: currentPhoto.get('ownerName')
            })
        };
    }
});
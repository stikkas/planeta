/*globals ProfileInfoHover, DateUtils, StatisticService, StaticNodesService, TemplateManager, FileTemplates,console,$,jQuery*/
/**
 * Planeta js library initialization
 *
 * Usage:
 * Planeta.init({
 *     myProfile: {
 *         constructor: ProfileModel,
 *         profile: ${hf:toJson(myProfile)}, // mandatory
 *         myBalance: ${myBalance},
 *         isAdmin: ${isAdmin},
 *         isAuthorized: ${isAuthorized}
 *     },
 *     appModel: {
 *         constructor: AppModel,
 *         profileConstructor: ProfileModel,
 *         profile: ${hf:toJson(profileModel)}
 *     },
 *     configuration: {
 *         fileTemplates: FileTemplates,
 *         countries: ${hf:toJson(countriesList)},
 *         mainHost: '${properties["application.host"]}'
 *         staticNode: '${properties["server.path"]}',
 *         statServiceUrl: '${properties["statistics.service.url"]}',
 *         statusServiceUrl: '//${properties["status.service.url"]}',
 *         tvAppUrl: '${properties["tv.application.host"]}',
 *         shopAppUrl: '${properties["shop.application.host"]}',
 *         mobileAppUrl: '${properties["mobile.application.host"]}',
 *         concertsAppUrl: '${properties["concerts.application.host"]}',
 *         resourcesHost: '${hf:getStaticBaseUrl("")}'
 *     },
 *     routing: {
 *         navigationMap: NavigationMap,
 *         router: PlanetaRouter
 *     }
 * });
 */
var workspace;
var Planeta = {

    init: function (options) {
        
        try {//removeTryCatch
            DateUtils.serverTimeZoneOffset = -180;
            var now = new Date();
            DateUtils.timeDiff = (now.getTime() - options.configuration.serverTime + (1000 * 60) * (now.getTimezoneOffset() - DateUtils.serverTimeZoneOffset));
            DateUtils.serverTime = options.configuration.serverTime;
            DateUtils.millsDiff = options.configuration.serverTime - now.getTime();
            // Initializing template manager with the specified file templates
            var myProfile = this.initMyProfile(options);
            var appModel = this.initAppModel(options, myProfile);
            var workspace = this.initRouter(options, appModel, myProfile);

            workspace.currentLanguage = options.currentLanguage;
            // Initializing plugins
            try {
                this.initPlugins();
            } catch(e) {

            }
            return workspace;
        } catch (ex) {
            if (console && console.log) {
                console.log('Error on planeta init.', ex);
            }
            throw ex;
        }
    },

    initRouter: function (options, appModel, myProfile) {
        var workspace = new options.routing.router({
            appModel: appModel,
            navigationMap: options.routing.navigationMap
        });
        workspace.serviceUrls = {
            adminHost: options.configuration.adminHost,
            mainHost: options.configuration.mainHost,
            tvAppUrl: options.configuration.tvAppUrl,
            shopAppUrl: options.configuration.shopAppUrl,
            mobileAppUrl: options.configuration.mobileAppUrl,
            concertsAppUrl: options.configuration.concertsAppUrl,
            concertsNewAppUrl: options.configuration.concertsNewAppUrl,
            widgetsAppUrl: options.configuration.widgetsAppUrl,
            affiliatesAppUrl: options.configuration.affiliatesAppUrl,
            resourcesUrl: options.configuration.resourcesHost,
            staticNode: options.configuration.staticNode,
            statServiceUrl: options.configuration.statServiceUrl,
            statusServiceUrl: options.configuration.statusServiceUrl,
            imServiceUrl: options.configuration.imServiceUrl,
            casHost: options.configuration.casHost,
            schoolUrl: options.configuration.schoolUrl,
            libraryUrl: options.configuration.libraryUrl,
            charityUrl: options.configuration.charityUrl,
            promoUrl: options.configuration.promoUrl
        };

        workspace.supportContacts = {
            supportPhoneNumber: options.configuration.supportPhoneNumber,
            supportEmail: options.configuration.supportEmail
        };

        workspace.staticNodesService = options.staticNodesService;
        // TODO: Handle unauthorized user here (or clientId will be 0 always)
        workspace.stats = StatisticService.init({
            statServiceUrl: options.configuration.statServiceUrl,
            clientId: myProfile.get('profileId')
        });

        // TODO: Move to myProfile
        workspace.isAuthorized = options.myProfile.isAuthorized;

        workspace.isBotRequest = options.configuration.isBotRequest;
        return workspace;
    },

    initMyProfile: function (options) {
        var myProfile = new options.appModel.profileConstructor(options.myProfile.profile);
        myProfile.set({
            myBalance: options.myProfile.myBalance,
            email: options.myProfile.email,
            isAdmin: options.myProfile.isAdmin,
            isAuthorized: options.myProfile.isAuthorized,
            isAuthor: options.myProfile.isAuthor
        });
        return myProfile;
    },

    initAppModel: function (options, myProfile) {
        // Init current page profile model
        var profileModel;
        if (options.appModel && options.appModel.profile) {
            profileModel = new options.appModel.profileConstructor(options.appModel.profile);
        } else {
            profileModel = myProfile;
        }

        // TODO: Handle unauthorized somewhere here
        var appModel = new options.appModel.constructor({
            myProfile: myProfile,
            profileModel: profileModel,
            staticNode: options.configuration.staticNode,
            statServiceUrl: options.configuration.statServiceUrl,
            statusServiceUrl: options.configuration.statusServiceUrl,
            imServiceUrl: options.configuration.imServiceUrl,
            projectType: options.configuration.projectType
        });

        return appModel;
    },

    initPlugins: function () {
        $('.profile-link').HoverInfo({
            modelConstructor: ProfileInfoHover.Models.InfoWithActions,
            viewConstructor: ProfileInfoHover.Views.InfoWithActions,
            getElementData: function (element) {
                //get profileId from attribute
                var profileId = element.attr('profile-link-data');
                //do not show for self
                if (!profileId || workspace.appModel.isMyProfile(profileId)) {
                    return null;
                }
                return {
                    profileId: profileId
                };
            }
        });

        function removeTooltip(view) {
            var tooltips = view.$('.tooltip');
            if(tooltips > 0) {
                tooltips.forEach(function(item) {
                    if(item.data('tooltipOption')) {
                        item.data('tooltipOption').tooltip.remove();
                    }
                });
            }
        }

        Backbone.GlobalEvents.bind('onDisposeView', removeTooltip);
        Backbone.GlobalEvents.bind('onTemplateApplied', removeTooltip);
    }

};
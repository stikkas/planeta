/*global CampaignDonate, AccountMerge, LazyHeader, Modal*/
/**
 *
 * @author Andrew
 * Date: 15.05.2014
 * Time: 16:50
 */


CampaignDonate.Views.DeliveryPaymentPage = CampaignDonate.Views.SharePaymentAndDeliveryPage.extend({
    template: '#delivery-payment-template',
    construct: function (options) {
        this.addChildAtElement('.js-project-payment-details-container', new CampaignDonate.Views.Payment({
            model: this.model.get('payment')
        }), true);
        if (options && options.controller) {
            options.controller.pageData = this.pageData();
        }
    },

    emailAlreadyExistCheck: function () {
        if (this.model.get('hasEmail')) {
            return;
        }
        var emailBlock = this.$('.js-anchr-email-block');
        var $emailInput = this.$('[name="email"]');

        var signupCallback = function () {
            LazyHeader.showAuthForm('signup', {
                email: $emailInput.val(),
                focusPassword: true
            });
        };

        $emailInput.checkEmail({
            $submit: this.$('.js-purchase-order'),
            $email: $emailInput,
            $hint: emailBlock.find('#hint'),
            $hintContainer: emailBlock.find('.dropdown-popup'),
            disableButtons: false,
            disableButtonOnAlreadyExists: false,
            signupCallback: signupCallback
        });
    },

    afterRender: function () {
        this.emailAlreadyExistCheck();
    },

    pageData: function () {
        var title = "Оплата доставки вознаграждения";
        var result = {
            title: title
        };

        return result;
    }

});


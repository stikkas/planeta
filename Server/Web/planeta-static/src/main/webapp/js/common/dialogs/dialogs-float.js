/*globals Dialogs,TemplateManager*/
Dialogs.Views.FloatView = BaseView.extend({

    initialize: function (options) {
        _.bindAll(this);
        BaseView.prototype.initialize.call(this, options);
        this.layoutManager = options.layoutManager;
    },
    events: {
        "click .clickHereForCollapse": "onCollapse",
        "click .icon-close": "onClose",
        "mousedown": "onClick"
    },

    onClick: function () {
        this.layoutManager.moveOnTop(this);
    },

    onClose: function () {
        Backbone.Events.trigger.call(this, "onClose");
    },
    onCollapse: function () {
        if (this.dragging) {
            this.dragging = false;
            return;
        }
        this.$el.find(".icon-ww-size").toggleClass("icon-ww-size-full");
        this.$el.toggleClass("collapsed");
        if (this.$el.hasClass("collapsed")) {
            return;
        }

        var windowHeight = $(window).height();
        var height = this.$el.outerHeight(true);
        if (height > windowHeight) {
            this.$el.css({
                top: "0",
                bottom: ""
            });
        } else {
            var maxTop = windowHeight - height;
            if (this.$el.offset().top >= maxTop) {
                this.$el.css({
                    top: "",
                    bottom: "0"
                });
            }
        }
    },

    appendToBody: function () {
        var self = this;
        TemplateManager.fetchCss("/css-generated/message.css").done(function () {
            $("body").append(self.render().el);
            self.$el.draggable({
                snap: 'body',
                handle: ".clickHereForCollapse",
                scroll: false,
                start: self.dragStart,
                drag: self.dragDrag,
                stop: self.dragStop
            });
            self.$el.droppable({
                tolerance: "pointer",
                drop: self.drop,
                scope: "attachToDialog"
            });
        });
    },

    drop: function (e, ui) {
        if (ui.helper.message && this.dialog) {
            this.dialog.sendMessage(ui.helper.message);
        }
    },

    dragStart: function () {
        this.moveFrom = this.$el.offset();
        this.$el.css({
            bottom: "",
            right: ""
        });
        this.dragging = true;
    },
    dragDrag: function (e, ui) {
        var maxLeft = $(window).width() - this.$el.outerWidth(true);
        var maxTop = $(window).height() - this.$el.outerHeight(true);

        if (ui.position.left > maxLeft) {
            ui.position.left = maxLeft;
        }
        if (ui.position.left < 0) {
            ui.position.left = 0;
        }
        if (ui.position.top > maxTop) {
            ui.position.top = maxTop;
        }
        if (ui.position.top < 0) {
            ui.position.top = 0;
        }
    },
    dragStop: function (e, ui) {
        var maxLeft = $(window).width() - this.$el.outerWidth(true);
        var maxTop = $(window).height() - this.$el.outerHeight(true);

        var newPosition = {
            left: ui.position.left,
            top: ui.position.top,
            right: "",
            bottom: ""
        };

        if (ui.position.left >= maxLeft) {
            newPosition.right = 0;
            newPosition.left = "";
            this.$el.css({
                left: "",
                right: "0"
            });
        }
        if (ui.position.top >= maxTop) {
            newPosition.bottom = 0;
            newPosition.top = "";
            this.$el.css({
                top: "",
                bottom: "0"
            });
        }

        this.layoutManager.onPositionChanged(this, newPosition, this.moveFrom);

        var self = this;
        setTimeout(function () {
            self.dragging = false;
        }, 100);
    }

});


Dialogs.Views.FloatDialogsSearch = Dialogs.Views.FloatView.extend({
    className: "dml-drop-block", 
    afterRender: function() {
        this.$el.css('z-index', '');
    }
});

Dialogs.Views.FloatDialogBox = Dialogs.Views.FloatView.extend({
    className: "dlg-drop-block",
    events: {
        "mousemove": "onMouseMove"
    },

    initialize: function () {
        _.extend(this.events, Dialogs.Views.FloatView.prototype.events);
        Dialogs.Views.FloatView.prototype.initialize.apply(this, arguments);
    },


    onMouseMove: function () {
        this.dialogBox.contentPanel.messagesList.onUserActivity();
    }
});


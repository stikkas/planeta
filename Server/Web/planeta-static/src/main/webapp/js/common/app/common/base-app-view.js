/*global Audios, Modal, JobManager, console,moduleLoader,HeaderSearch,TemplateManager*/
var BaseAppView = BaseView.extend({

    el: 'body',

    jobServiceCheckName: 'service-availability-check',
    JOB_SERVICE_CHECK_PERIOD: 30000,
    errorTemplate: '#error-message-template',
    infoTemplate: '#info-message-template',
    successTemplate: '#success-message-template',

    // Do not binding on appModel change event
    modelEvents: {
        'destroy': 'dispose'
    },

    events: {
        'click .js-open-main-nav': 'toggleMainNav',
        'click .header_search-results-overlay': 'clickOverlay',
        'keydown': 'keypress',
        'click a': 'closeOnHref',
        'resize': 'initMainNav',
        'click .main-nav-heads_link': 'mainNavClick',
        'changed.owl.carousel': 'changeOwl',
        'click .js-create-button': 'chooseDraftCampaign',
        'click .main-nav-menu_link': 'mainNavMenuClick'
    },

    construct: function () {
        moduleLoader.loadModule("header-search").done(function () {
            var searchView = new HeaderSearch.Views.Header({
                el: $(".js-header"),
                model: new HeaderSearch.Models.ContainerModel()
            });

            searchView.renderAsync();
        });

        Backbone.onServiceUnavailable = _.bind(this.onServiceUnavailable, this);
        Backbone.onLostConnection = _.bind(this.onLostConnection, this);
        Backbone.onRenderError = _.bind(this.onRenderError, this);

        this.navHeadsInit();
        this.navInit();
    },

    chooseDraftCampaign: function (e) {
        if (workspace.isAuthorized) {
            e.preventDefault();
        }
        Welcome.Views.SelectDraftCampaignList.onCreateProjectClicked(e);
    },

    initMainNav: _.debounce(function () {
        this.navHeadsInit();
        this.navInit();
    }, 1000),

    mainNavMenuClick: function (e) {
        $(document).trigger('urlChangedFromHamburger', [$(e.currentTarget).attr('href')]);
    },

    navHeadsInit: function () {
        var navHeads = $('.main-nav-heads');

        if ($(window).width() > 767) {
            navHeads.trigger('destroy.owl.carousel')
                .removeClass('owl-carousel owl-hidden')
                .removeAttr('tabindex');
        } else {
            navHeads.addClass('owl-carousel').owlCarousel({
                dots: false,
                autoWidth: true,
                margin: 5
            });
            navHeads.find('.owl-stage').width('+=1');
            $('.main-nav-heads_link[data-id=0]').closest('.main-nav-heads_i ').addClass('current')
        }
    },

    navInit: function () {
        var nav = $('.main-nav_nav-list');

        if ($(window).width() > 767) {
            nav.trigger('destroy.owl.carousel')
                .removeClass('owl-carousel  owl-hidden')
                .removeAttr('tabindex');
        } else {
            nav.addClass('owl-carousel').owlCarousel({
                items: 1,
                dots: false
            });
        }
    },

    mainNavClick: function (e) {
        var id = $(e.currentTarget).data('id');
        $('.main-nav_nav-list').trigger('to.owl.carousel', [id]);
    },

    changeOwl: function () {
        var nav = $('.main-nav_nav-list');

        setTimeout(function () {
            var active = $('.owl-item.active:eq(0) .main-nav_nav-i', nav);
            var id = active.data('id');

            $('.main-nav-heads_i.current').removeClass('current');
            $('.main-nav-heads_i').eq(id).addClass('current');
            id--;
            if (id < 0) id = 0;
            $('.main-nav-heads').trigger('to.owl.carousel', [id]);
        });
    },

    clickOverlay: function () {
        $('.header').removeClass('header__search header__search-results');
    },

    changeRootHeight: function () {
        var headerSpecial = $('.h-special');
        var footer = $('.footer');
        var header = $('.header');
        var headerHeight = headerSpecial.outerHeight() + header.outerHeight(true);
        var footerHeight = footer.outerHeight();
        var winHeight = $(window).height();
        var rootHeight = winHeight - headerHeight - footerHeight;
        var rootContainer = $('body > #root-container, body > #global-container');

        if ($('html').hasClass('main-nav-open')) {
            setTimeout(function () {
                rootContainer.css({
                    height: rootHeight > 0 ? rootHeight : 0
                });
            }, 250);
        }
    },

    keypress: function (e) {
        // only ESC
        if ($('html').hasClass('main-nav-open') && e.keyCode === 27) {
            this.toggleMainNav();
        }
    },

    closeOnHref: function () {
        if ($('html').hasClass('main-nav-open')) {
            this.toggleMainNav();
        }
    },

    toggleMainNav: function () {
        if (!this.model) {
            return;
        }
        var self = this;
        var rootContainer = $('body > #root-container, body > #global-container');
        var contentScroll = 0;

        if (!$('html').hasClass('main-nav-open')) {
            contentScroll = $(window).scrollTop();

            $('html').addClass('main-nav-open');
            if ($(window).width() < 768) $('html, body').scrollTop(0);

            self.changeRootHeight();
            $(window).on('resize.main-nav', _.debounce(self.changeRootHeight, 1000));
        } else {
            $(window).off('resize.main-nav');

            $('html').removeClass('main-nav-open');

            rootContainer.css({
                height: ''
            });

            $('html, body').scrollTop(contentScroll);
        }
    },

    showMessageUnexpectedError: function (timeout, context) {
        return BaseAppView.prototype.showErrorMessage("Непредвиденная ошибка", timeout, context);
    }
    ,

    showErrorMessage: function (message, timeout, context) {
        if (message) {
            this.showAlert(message, this.errorTemplate, timeout, context);
        }
    }
    ,

    showDebugMessage: function (message) {
        if (window.debugMode) {
            if (message) {
                this.showErrorMessage(message);
            }
            console.error.apply(console, arguments);
        }
    }
    ,

    showSuccessMessage: function (message, timeout, context) {
        if (message) {
            this.showAlert(message, this.successTemplate, timeout, context);
        }

    }
    ,

    showInfoMessage: function (message, timeout) {
        this.showAlert(message, this.infoTemplate, timeout);
    }
    ,

    showAlert: function (message, template, timeout, context) {
        var self = this;
        TemplateManager.tmpl(template, {
            messageHtml: message
        }).done(function (alertEl) {
            self.closeAlert();
            timeout = _.isUndefined(timeout) ? 5000 : timeout;

            self.alertEl = alertEl;
            $(alertEl).find(".close").click(function (e) {
                e.preventDefault();
                self.closeAlert();
            });
            if (timeout) {
                _.delay(function () {
                    self.closeAlert();
                }, timeout);
            }
            var $context = context || $('body');
            $context.prepend(alertEl);
        });
    }
    ,

    closeAlert: function () {
        if (this.alertEl) {
            $(this.alertEl).remove();
            this.alertEl = null;
        }
    }
    ,

    closeModalDialog: function () {
        $('#modal-dialog').remove();
        $(".modal-backdrop").remove();
        $(".modal").remove();
    }
    ,

    onServiceUnavailable: function (status) {
        this.closeAlert();
        Modal.closeModal();
        Modal.showServiceUnavailableStub(status);
        var self = this;

        var checkAvailability = function () {
            $.ajax({
                url: '/api/util/http-check.json',
                dataType: 'json',
                success: function (response) {
                    if (response && response.success) {
                        JobManager.removeJob(self.jobServiceCheckName);
                        location.reload();
                    }
                }
            });
        };

        JobManager.schedule(self.jobServiceCheckName, checkAvailability, self.JOB_SERVICE_CHECK_PERIOD, self.JOB_SERVICE_CHECK_PERIOD);
    }
    ,

    onLostConnection: function () {
        if (window.debugMode) {
            this.showAlert('lost connection');
            console.log("lost connection");
        }
    }
    ,

    onRenderError: function (ex) {
        console.error((this.backboneClassName ? this.backboneClassName + ':' : '') + this.cid + ' Render error exception', ex);
        if (this.template) {
            console.log('template: ' + this.template);
        }
        this.showErrorMessage('Ошибка при отрисовке');
    }
    ,

    removeTooltips: function () {
        $('.twipsy').remove();
        $('.users-card').remove();
    }
    ,

    renderSearchResults: function (model) {
        this.addChildAtElement('.h-search_ico', new HeaderSearch.Views.LargeView({
            model: model
        }));
    }
});
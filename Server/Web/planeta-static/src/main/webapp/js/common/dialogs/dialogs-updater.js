/*globals BaseUpdater*/
var DialogsUpdater = _.extend(new BaseUpdater(), {
    url: function () {
        return '//' + workspace.serviceUrls.imServiceUrl + '/dialog-last-messages-for-several-dialogs.json';
    },
    schedulerName: 'dialogs-update-job',
    objectIdName: "dialogId",
    newObjectEventName: "newMessage",
    initUpdatePeriod: 3000,
    incUpdatePeriod: 1000,
    dataType: 'jsonp',

    fetchData: {
        objectsName: "dialogLastMessageObjects",
        dialogId: "&",
        lastMessageId: "&getLastMessageId()"
    }
});

/**
 * Views for attaching link
 */

Attach.Views.Link = Modal.OverlappedView.extend({
	template:'#attachment-link-template',
	renderTemplate:false,

	events:{
		'click input':'checkClick',
		'paste input':'check',
		'keypress input':'check',
		'keyup input':'check',
		'input input':'check',
		'click a.close':'cancel',
		'click button[type=reset]':'cancel',
		'click button[type=submit]':'save'
	},

	checkClick:function (event) {
		event.preventDefault();
		this.check(event);
	},

	check:function (event) {
		if (event.target.value.length > 0) {
			$('[type="submit"]', this.el).attr('disabled', false);
		} else {
			$('[type="submit"]', this.el).attr('disabled', true);
		}
		if (event.keyCode === 10 || event.keyCode === 13) {
			if (!$('[type="submit"]', this.el).attr('disabled')) {
				this.save();
			}
			return false;
		}
	},
	save:function () {
		var text = $(this.el).find('#link-text').val();
		this.model.select(text);
		return false;
	},

    afterRender:function(){
        Modal.OverlappedView.prototype.afterRender.call(this);

		if(this.enteredUrl){
            $('#link-text').val(this.enteredUrl);
        }
        $('#link-text').focus();
    }
});




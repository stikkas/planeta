/*globals JobManager*/
var Comments = {
    /**
     * Some constants
     */
    limit: 10,
    Models: {},
    Views: {}
};

Comments.Models.Item = BaseModel.extend({
    idAttribute: "commentId",
    defaults: {
        //newComment: false,
        inProcess: false,
        canBeDeleted: false,
        showCloseButton: false,
        restored: false // to show 30 sec comment-deleteByProfileId-timeout only for not-restored comments
    }
});

Comments.Models.List = BaseCollection.extend({
    url: '/api/public/comments-with-child-and-profile-relation.json',
    model: Comments.Models.Item,

    getOwnerProfile: function () {
        return this.ownerProfile || workspace.appModel.get('profileModel');
    },

    initialize: function (models, options) {
        this.ownerProfile = options.ownerProfile;
        this.extendCommentsWithOwnerProfile(models);
        BaseCollection.prototype.initialize.call(this, models, options);
    },

    parse: function (response) {
        var parsedResponse = [];
        if (response && response.success === false) {
            workspace.appView.showErrorMessage(response.errorMessage);
            return parsedResponse;
        }

        _.each(response, function (item) {
            if (item.comment) {
                item.comment.relationStatuses = item.relationStatuses;
                parsedResponse.push(item.comment);
            } else {
                parsedResponse.push(item);
            }
        });
        this.extendCommentsWithOwnerProfile(parsedResponse);
        return parsedResponse;
    },

    extendCommentsWithOwnerProfile: function (commentsArray) {
        var ownerProfile = this.getOwnerProfile();
        _.each(commentsArray, function (c) {
            if (!c.profile) {
                c.profile = new BaseModel(ownerProfile);
            }
        });
    }
});

/**
 * Returns collection of comments starting from specified startCommentId
 */
Comments.Models.LastComments = Comments.Models.List.extend({
    url: '/api/public/last-comments.json',

    parse: function (response) {
        response = Comments.Models.List.prototype.parse.call(this, response);
        _.each(response, function (r) {
            r.newComment = true;
        });
        return response;
    }
});

Comments.Models.Comments = BaseModel.extend({
    DESTROY_COMMENT_DELAY: 30 * 1000,
    COMMENT_BECOMES_UNDELETABLE_DELAY: 30 * 1000,

    addUrl: '/api/profile/add-comment.json',
    defaults: {
        objectType: 'WALL',
        profileId: null,
        objectId: null,
        parentCommentId: undefined,
        commentsListOpen: false,
        showOnEmptyList: true,
        showCounters: true
    },
    initialize: function (attributes, options) {
        BaseModel.prototype.initialize.call(this, attributes, options);

        this.commentableObject = (options && options.commentableObject) ? options.commentableObject : new BaseModel();
        var commentsCount = this.commentableObject.get('commentsCount') || 0;
        var limit = attributes.limit || Comments.limit;
        var data = {
            data: this.toJSON(),
            limit: limit
        };
        // feed viewerProfile
        if (this.commentableObject.get('profile')) {
            data.ownerProfile = this.commentableObject.get('profile');
        }
        var commentsList = new Comments.Models.List(options.comments || [], data);


        if (commentsCount == limit) {
            commentsList.limit = commentsCount + 1;
        }
        this.set({
            commentsListOpen: commentsCount > 0,
            limit: limit
        }, {silent: true});
        this.set({
            commentsList: commentsList
        }, {silent: true});
        this.commentsList = commentsList;
    },

    getData: function () {

        return {
            objectType: this.get('objectType'),
            profileId: this.get('profileId'),
            objectId: this.get('objectId'),
            limit: this.get('limit'),
            offset: this.get('offset')
        };
    },
    fetch: function (options) {
        this.commentsList.data = this.getData();
        this.commentsList.limit = this.get('limit');
        this.commentsList.load(options);
    },

    send: function (text, callback) {
        var self = this;

        var options = {
            url: this.addUrl,
            data: {
                profileId: self.get('profileId'),
                objectId: self.get('objectId'),
                objectType: self.get('objectType'),
                parentCommentId: self.get('parentCommentId'),
                text: text
            },
            success: function (response) {
                if (response.success) {
                    callback();
                    self.commentsList.add(response.result, {
                        at: 0
                    });
                    var model = self.commentsList.get(response.result.commentId);
                    model.set("canBeDeleted", true);
                    JobManager.scheduleOnce("comment-undeletable-" + model.id, function () {
                        model.set("canBeDeleted", false);
                    }, self.COMMENT_BECOMES_UNDELETABLE_DELAY, {
                        context: this
                    });
                    JobManager.schedule('comment-undeletable-counter-' + model.id, function () {
                        if (model.get("canBeDeleted") === false) {
                            JobManager.removeJob('comment-undeletable-counter-' + model.id);
                        }
                        var $el = $("div#comment" + model.id);
                        var secondsLast = parseInt($el.find("span.comment-undeletable-counter > b").text(), 10);
                        if (isNaN(secondsLast)) {
                            return;
                        }
                        secondsLast += -1;
                        model.set('secondsLast', secondsLast, {silent: true});
                        $el.find("span.comment-undeletable-counter > b").text(secondsLast);
                    }, 1000, 1000, {
                        context: this
                    });
                    self.commentableObject.set({
                        commentsCount: self.commentableObject.get('commentsCount') + 1
                    });

                    if (self.get('feedItemModel')) {
                        self.get('feedItemModel').set({
                            commentsCount: self.commentableObject.get('commentsCount')
                        });
                    }

                } else {
                    workspace.appView.showErrorMessage(response.errorMessage);
                }
            },

            complete: function () {
                self.unset('sending');
            }
        };

        this.set({sending: true}, {silent: true});
        Backbone.sync('create', this, options);
    },

    initComments: function () {
        var commentsList = this.commentsList;
        var commentableObject = this.commentableObject;
        var commentsCount = commentableObject.get('commentsCount');

        if (commentsCount <= this.get('limit')) {
            commentsList.allLoaded = true;
        }

        if (!commentableObject || !commentableObject.has('commentsCount') ||
            (commentableObject.get('commentsCount') > 0 && commentsList.length == 0)) {

            commentsList.load({
                success: function () {
                    if (commentsCount <= this.get('limit')) {
                        commentsList.allLoaded = true;
                    }
                }
            });
        }
    },

    loadLast: function () {
        var commentsList = this.commentsList;
        var updatesList = new Comments.Models.LastComments();

        var lastComment = commentsList.at(0);
        var startCommentId = lastComment ? lastComment.get('commentId') : 0;

        updatesList.data = _.extend({
            startCommentId: startCommentId,
            sort: 'asc'
        }, commentsList.data);

        var self = this;
        updatesList.load({
            success: function (response, collection) {
                collection.each(function (comment) {
                    commentsList.add(comment, {
                        at: 0
                    });
                });
                self.commentableObject.set({
                    commentsCount: self.commentableObject.get('commentsCount') + collection.length
                });
                self.set({commentsListOpen: true});
            }
        });
    },

    markNewCommentsAsShown: function () {
        this.commentsList.each(function (comment) {
            comment.unset("newComment");
        });
    },

    triggerChangeCommentsCount: function (newValue, options) {
        options = options || {};
        this.commentableObject.set({
            commentsCount: newValue
        }, options);
        if (options.silent) {
            this.commentableObject.trigger('change:commentsCount', newValue, options);
        }
    },

    deleteComment: function (comment) {
        comment.set({
            inProcess: true
        });
        var options = {
            url: '/api/profile/deleteByProfileId-comment.json',
            data: {
                profileId: comment.get('profileId'),
                commentId: comment.get('commentId')
            },
            context: this,
            success: function (response) {
                if (response.success) {
                    comment.set({
                        deleted: true
                    });
                    var commentsCount = this.commentableObject.get('commentsCount') - 1;
                    this.triggerChangeCommentsCount(commentsCount, {silent: true});
                    //destroy with timeout
                    var destroy = function () {
                        if (this.commentableObject.get('commentsCount') == 0) {
                            this.set({
                                commentsListOpen: false
                            }, {silent: true});
                        }
                        comment.destroy();
                    };
                    JobManager.scheduleOnce('destroy-comment-' + comment.get('commentId'), destroy, this.DESTROY_COMMENT_DELAY, {
                        context: this
                    });
                } else {
                    workspace.appView.showErrorMessage(response.errorMessage);
                }
            },
            complete: function () {
                comment.set({
                    inProcess: false
                });
            }
        };

        Backbone.sync('delete', comment, options);
    },

    restoreComment: function (comment) {
        comment.set({
            inProcess: true
        });
        //clear destroy timeout
        JobManager.removeJob('destroy-comment-' + comment.get('commentId'));

        var options = {
            url: '/api/profile/restore-comment.json',
            data: {
                profileId: comment.get('profileId'),
                commentId: comment.get('commentId')
            },
            context: this,
            success: function (response) {
                if (response.success) {
                    comment.set({
                        deleted: false,
                        restored: true
                    });
                    var commentsCount = this.commentableObject.get('commentsCount') + 1;
                    this.triggerChangeCommentsCount(commentsCount, {silent: true});

                } else {
                    workspace.appView.showErrorMessage(response.errorMessage);
                }
            },
            complete: function () {
                comment.set({
                    inProcess: false
                });
            }
        };

        Backbone.sync('update', comment, options);
    },

    isCommentsListOpen: function () {
        return this.get('showOnEmptyList') || this.get('commentsListOpen');
    },

    toggleCommentsList: function () {
        this.set({
            commentsListOpen: !this.get('commentsListOpen')
        });
    },

    copyFrom: function (another, count) {
        var models = count ? another.commentsList.first(count) : another.commentsList.models;
        this.commentsList.reset(models);
        this.commentsList.trigger('copyFrom');
    }
});
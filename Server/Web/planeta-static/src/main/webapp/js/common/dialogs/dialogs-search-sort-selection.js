/*globals Dialogs*/
Dialogs.DialogsSearchSortSelectionModels = [
    {
        id: 0,
        text: workspace && workspace.currentLanguage && workspace.currentLanguage === 'en' ? "Sort by name" : "Сортировать по имени",
        comparator: function (dialog1, dialog2) {
            var online1 = !!dialog1.get("onlineStatus");
            var online2 = !!dialog2.get("onlineStatus");
            if (online1 === online2) {
                var name1 = (dialog1.get("companionName") || "").toUpperCase();
                var name2 = (dialog2.get("companionName") || "").toUpperCase();

                return name1 == name2 ? 0 : name1 > name2 ? 1 : -1;
            }
            return online1 ? -1 : 1;
        }
    },
    {
        id: 1,
        showByDefault: true,
        text: workspace && workspace.currentLanguage && workspace.currentLanguage === 'en' ? "Sort by date" : "Сортировать по дате",
        comparator: function (dialog1, dialog2) {
            var time1 = dialog1.get("timeUpdated") || 0;
            var time2 = dialog2.get("timeUpdated") || 0;

            if (time1 == time2) {
                var name1 = (dialog1.get("companionName") || "").toUpperCase();
                var name2 = (dialog2.get("companionName") || "").toUpperCase();

                return name1 == name2 ? 0 : name1 > name2 ? -1 : 1;
            }
            return time1 > time2 ? -1 : 1;
        }
    }
];

Dialogs.Views.DialogsSearchSortSelection = BaseView.extend({
    template: "#dialog-search-sort-selection-template",
    className: "dm-footer-action dropdown",
    events: {
        "click .fa-btn": "onClick"
    },

    onClick: function (e) {
        var self = this;
        this.$el.toggleClass("open");
        if (this.$el.hasClass("open")) {
            this.resizeHeightBy();
            $(e.currentTarget).clickOff(function () {
                self.$el.removeClass('open');
            });
        }
    },
    hideDropDown: function () {
        this.$el.removeClass("open");
    },

    resizeHeightBy: function () {
        var $dropBlock = this.$el.find(".dm-dropdodown-menu-contacts");
        if ($dropBlock.length > 0) {
            $dropBlock.removeClass('bottom-up');
            var dropBlockBottom = $dropBlock.offset().top + $dropBlock.outerHeight(true);
            if (dropBlockBottom > $(window).height()) {
                $dropBlock.addClass('bottom-up');
            }
        }
    }



});

Dialogs.Models.DialogsSearchSortSelectionCollection = BaseCollection.extend({

    selectItem: function (modelId) {
        var model = this.findByAttr("id", modelId);
        if (!model) {
            return null;
        }

        var selectedModel = this.findByAttr("isSelected", true);
        if (selectedModel) {
            selectedModel.set("isSelected", false);
        }
        model.set("isSelected", true);
        this.trigger("selectedItemChanged", model);

        return model;
    }

});

Dialogs.Views.DialogsSearchSortSelectionList = BaseListView.extend({
    tagName: 'ul',
    className: 'dmc-list'
});

Dialogs.Views.DialogsSearchSortSelectionListItem = BaseView.extend({
    tagName: 'li',
    className: 'dmc-item',
    template: '#dialog-search-sort-selection-list-item-template',
    events: {
        "click": "onClick"
    },
    afterRender: function () {
        if (this.model.get("isSelected")) {
            this.$el.addClass("active");
        } else {
            this.$el.removeClass("active");
        }
    },
    onClick: function () {
        this.model.collection.selectItem(this.model.get("id"));
    }


});

/*globals Dialogs,Audios,CookieProvider,moduleLoader*/
Dialogs.Models.PopupController = Backbone.Model.extend({
    MAX_POPUP_MESSAGES: 3,
    DISPLAY_MESSAGE_DURATION: 5000,

    initialize: function (options) {
        this.popupCollection = new Dialogs.Models.PopupCollection();
        this.unreadMessages = [];
        this.popupListView = new Dialogs.Views.PopupList({
            collection: this.popupCollection,
            itemViewType: Dialogs.Views.PopupMessage
        });
        this.popupListView.on("onAppendView", this.onPopupMessageShown, this);
        this.popupCollection.on("remove", this.onPopupMessageRemoved, this);

        this.popupCollection.on('add', function () {
            options.dialogsController.onDialogActivity();
        });

        $("body").append(this.popupListView.render().el);

    },


    _playSound: function () {
        var self = this;
        if (!this.sound) {
            moduleLoader.loadModule('audio').done(function () {
                self.sound = Audios.getSoundManager().createSound({
                    id: "newPopupsMessagehasArrived",
                    url: workspace.staticNodesService.getResourceUrl("/audio/message.mp3"),
                    autoLoad: true,
                    autoPlay: true
                });
            });
        } else {
            this.sound.play();
        }
    },

    _playSoundIfNecessary: function () {
//        if (workspace.appModel.get("profileModel").get("profileSettings").get("playSoundWhenNewMessageArrives")) {
            this._playSound();
//        }
    },

    onUnreadMessagesLoaded: function (unreadMessages) {
        if (!_.isArray(unreadMessages)) {
            unreadMessages = [].push(unreadMessages);
        }


        //we show only messages which has been sent after enter on the site
        var lastPopupMessageId = this.getLastPopupMessageId();
        if (!lastPopupMessageId) {
            var maxUnreadMessageId = _.max(_.pluck(unreadMessages, 'messageId'));
            this.setLastPopupMessageId(maxUnreadMessageId);
        } else {
            unreadMessages = _.filter(unreadMessages, function (unreadMessage) {
                return unreadMessage.messageId > lastPopupMessageId;
            });

            unreadMessages.sort(function (mess1, mess2) {
                return mess1.messageId === mess2.messageId ? 0 : mess1.messageId > mess2.messageId ? 1 : -1;
            });

            _.each(unreadMessages, function (message) {
                this.unreadMessages.push(new Dialogs.Models.Message(message));
            }, this);

            this.showNextPopupMessage();
        }
    },

    onPopupMessageRemoved: function (message) {
        this.setLastPopupMessageId(message.get("messageId"));
        this.showNextPopupMessage();
    },

    showNextPopupMessage: function () {
        if (this.unreadMessages.length === 0 || this.popupCollection.length >= this.MAX_POPUP_MESSAGES) {
            return;
        }

        var message = this.unreadMessages.shift();
        this.popupCollection.add(message);
    },

    onPopupMessageShown: function (messageView) {
        var self = this;
        // self._playSoundIfNecessary();
        messageView.$el.slideDown('slow', function () {
            self.showNextPopupMessage();
            setTimeout(function () {
                messageView.closeMessage();
            }, self.DISPLAY_MESSAGE_DURATION);
        });

    },

    getLastPopupMessageId: function () {
        return CookieProvider.get("popupLastMessageId") || 0;
    },

    setLastPopupMessageId: function (id) {
        var lastPopupMessageId = this.getLastPopupMessageId();
        if (id > lastPopupMessageId) {
            CookieProvider.set("popupLastMessageId", id);
        }
    },
    removeAllDialogMessages: function (dialogId) {
        var i;
        this.unreadMessages = _.filter(this.unreadMessages, function (message) {
            return message.get('dialogId') != dialogId;
        });

        for (i = this.popupCollection.length - 1; i >= 0; i--) {
            var message = this.popupCollection.at(i);
            if (message.get('dialogId') == dialogId) {
                this.popupCollection.remove(message);
            }
        }
    }

});


Dialogs.Models.PopupCollection = BaseCollection.extend({
    url: function () {
        return '//' + workspace.serviceUrls.imServiceUrl + "/dialog-messages.json";
    },

    model: Dialogs.Models.PopupMessage
});

Dialogs.Views.PopupList = BaseListView.extend({
    className: "quick-notif-block",
    _appendChild: function (view) {
        view.$el.hide();
        var self = this;
        var dfd = BaseListView.prototype._appendChild.call(this, view);
        $.when(dfd).done(function () {
            Backbone.Events.trigger.call(self, "onAppendView", view);
        });
        return dfd;
    }
});

Dialogs.Views.PopupMessage = BaseView.extend({
    className: "quick-notif",
    template: "#dialog-popup-message-template",
    events: {
        "click .icon-close": "onClose",
        "click": "onClick"
    },

    onClose: function (e) {
        e.preventDefault();
        this.closeMessage();
    },

    closeMessage: function (e) {
        this.model.destroy();
    },

    onClick: function (e) {
        if (e.isDefaultPrevented()) {
            return;
        }
        e.preventDefault();
        workspace.appModel.get('dialogsController').showDialog(this.model.get('dialogId'), {loadLastMessagesBeforeOpen: true});
    }
});


Dialogs.Models.PopupMessage = BaseModel.extend({

    initialize: function (options) {
        BaseModel.prototype.initialize.call(this, options);
        this.id = options.messageId;
    },

    destroy: function (options) {
        this.trigger('destroy', this, this.collection, options);
    }

});

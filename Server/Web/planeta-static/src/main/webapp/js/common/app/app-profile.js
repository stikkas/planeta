/*global BaseProfileModel, Form, Modal, EventListModel, ProfileInfoHover*/
/*****************************************************************************
 * Model for the profile.
 * Profile may be user, group or event.
 ****************************************************************************/

var ProfileModel = BaseProfileModel.extend({
    checkOnlineStatus: function () {
        ProfileInfoHover.Models.Info.prototype.checkOnlineStatus.call(this);
    }
});

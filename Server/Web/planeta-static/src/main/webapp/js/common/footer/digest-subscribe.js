var DigestSubscribe = {
    Views: {}
};


DigestSubscribe.Views.DigestSubscribeForm = BaseView.extend({
    template: '#digest-subscribe-form',

    events: {
        'click .js-digest-subscribe-btn': 'subscribeDigest'
    },

    showSuccessMessage: function (lang) {
        lang === 'ru' ?
            workspace.appView.showSuccessMessage('Вы успешно подписаны на рассылки'):
            workspace.appView.showSuccessMessage('You successfully subscribed to weekly digest');
    },

    showErrorMessage: function (lang) {
        lang === 'ru' ?
            workspace.appView.showErrorMessage('Пожалуйста, проверьте введенные данные'):
            workspace.appView.showErrorMessage('Please check the input data');
    },

    subscribeDigest: function (e) {
        var lang = workspace && workspace.currentLanguage ? workspace.currentLanguage : 'ru';
        var self = this;

        var email = this.$('.js-digest-subscribe-input').val();
        Backbone.sync('update', null, {
            url: '/api/public/mailer-subscribe',
            contentType: 'application/json',
            data: {
                'email': email
            }
        }).done(function (response) {
            if (response && response.success) {
                self.showSuccessMessage(lang);
            } else {
                self.showErrorMessage(lang);
            }

        }).fail(function () {
            self.showErrorMessage(lang);
        });
    }
});
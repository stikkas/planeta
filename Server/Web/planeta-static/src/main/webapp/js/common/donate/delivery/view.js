/*globals CampaignDonate, Wizard*/
/**
 *
 * @author Andrew.Arefyev@gmail.com
 * Date: 13.08.13
 * Time: 14:40
 */


CampaignDonate.Views.DeliveryServicesItem = BaseView.extend({
    template: '#donate-share-details-delivery-header-item-template',
    className: 'radio-row pln-payment-box_radio',
    viewEvents: {
        'click .delivery-item:not(.active)': 'deliverySelected'
    }
});

CampaignDonate.Views.DeliveryServicesList = BaseListView.extend({
    itemViewType: CampaignDonate.Views.DeliveryServicesItem
});

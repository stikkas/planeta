/**
 *
 * @author Andrew.Arefyev@gmail.com
 * Date: 05.04.13
 * Time: 17:30
 */
var Breadcrumbs = {
    titleLengthLimit: 45,
    titleLengthLimitShorter: 40,

    Models: {},
    Views: {}
};
/**
 *  bind it to .top-breads.out-container
 * @type {*}
 */
Breadcrumbs.Views.Start2 = BaseView.extend({
    template: "#breadcrumbs-start2-template",
    className: 'top-breads breadcrumbs-container', //out-container
    construct: function() {
        if (this.model.hasCampaignModel && this.model.hasCampaignModel()) {
            this.model.getCampaignModel().on('change', this.render, this);
        }
    }
});

Breadcrumbs.Views.CampaignPage = BaseView.extend({
    template: "#breadcrumbs-campaign-page-template",
    className: 'wrap',
    construct: function () {
        if (workspace) {
            if(workspace.currentLanguage) {
                this.model.set('useEng', workspace.currentLanguage === "en", {silent: true});
            }
        }
    }
});
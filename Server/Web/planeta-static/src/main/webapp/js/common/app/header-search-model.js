/*global  Search*/
var HeaderSearch = {
    HEADER_SEARCH_QUERY_CHANGE_DELAY: 500,
    SEARCH_RESULT_CROSSFADE_TO_STUB_DELAY: 1000,
    Models: {},
    Views: {}
};

HeaderSearch.Models.ContainerModel = BaseModel.extend({
    defaults: {
        isSearching: false,
        notFetched: true,
        query: '',
        numberOfEmpty: 0
    },

    initialize: function () {
        var self = this;

        var searchBlocks = {
            projects: {
                limit: 5
            },
            products: {
                limit: 5
            },
            shares: {
                limit: 5
            }
        };

        var resultModels = {};
        var ModelType = HeaderSearch.Models.HeaderSearchModel.extend({
            onFetchSuccess: function () {
                self.set({notFetched: false});
                var numberOfFetched = 0;
                var numberOfEmpty = 0;
                _.each(self.resultModels, function (model) {
                    if (!model.isFetching) {
                        numberOfFetched++;
                    }
                    if (model.get('estimatedCount') === 0 && model.isFetching === 0) {
                        numberOfEmpty++;
                    }

                });

                var resultModelsCount = _.size(self.resultModels);

                self.set({
                    numberOfEmpty: numberOfEmpty,
                    isEmptyResult: numberOfEmpty === resultModelsCount,
                    isSearching: numberOfFetched < resultModelsCount
                });

                self.trigger('onFetchSuccessDone');
            }
        });

        _.each(searchBlocks, function (value, key) {
            resultModels[key] = new ModelType(_.extend({}, value, {subsection: key}));
        });

        this.resultModels = resultModels;
        this.set('collections', resultModels);
    },

    /**
     * @param {Boolean} forceSearch
     * @param {String} query
     */
    smartSearch: function (query, forceSearch) {
        if (this.get('query') !== query || forceSearch) {
            $('.h-search-results_loader').addClass('load');
            this.set({isSearching: true, query: query});
            _.each(this.resultModels, function (model) {
                model.query.set({query: query});
                var params = model.query.getParam();
                model.changeQuery(params);
            });
        }
    }

});

HeaderSearch.Models.HeaderSearchModel = Search.Models.Search.extend({
    defaults: {
        isFetchingKey: true,
        limit: 3,
        moreResultsText: 'Показать все',
        resultsListName: 'Элементы',
        moreResultsUrl: null,
        estimatedCount: 0,
        size: 0
    },
    initialize: function (options) {
        Search.Models.Search.prototype.initialize.call(this, options);
        if (options && options.limit) {
            this.searchResults.limit = options.limit;
        }
        this.isFetching = 0;
    },

    fetch: function (options) {
        this.isFetching++;
        var self = this;
        this.set({estimatedCount: 0, size: 0});
        _.delay(function () {
            if (self.isFetching) {
                self.set({isFetchingKey: true});
            }
        }, HeaderSearch.SEARCH_RESULT_CROSSFADE_TO_STUB_DELAY);
        var headerOptions = _.extend({}, options, {
            success: function (model, resp) {
                if (options && options.success && _.isFunction(options.success)) {
                    options.success(model, resp);
                }
                self.set({estimatedCount: self.searchResults.estimatedCount, size: self.searchResults.size});
                self.trigger('changeResults');
                self.onFetchSuccess(self);
            },
            complete: function () {
                if (--self.isFetching <= 0) {
                    self.set({isFetchingKey: false});
                }
            }
        });
        Search.Models.Search.prototype.fetch.call(this, headerOptions);
    },

    onFetchSuccess: function () {
        // override this
    }
});



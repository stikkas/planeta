/*global CampaignDonate, RegionAutocompleter, AccountMerge, LazyHeader, Modal, UserCallback*/
/**
 *
 * @author Andrew
 * Date: 15.05.2014
 * Time: 16:50
 */

var CampaignDonate = CampaignDonate || {};
CampaignDonate.Views = {
    toggleClasses: function (selector, classes, isEnable, context) {
        var hasClasses,
            element = _.isEmpty(selector) ? context.$el : context.$(selector);
        hasClasses = element.hasClass(classes);
        if (isEnable) {
            if (!hasClasses) {
                element.addClass(classes);
            }
        } else {
            if (hasClasses) {
                element.removeClass(classes);
            }
        }
    },
    getValue: function (selector, context) {
        return context.$(selector).val();
    },
    setValue: function (selector, value, context) {
        context.$(selector).val(value);
    }
};

/**
 */
CampaignDonate.Views.DonateSharePage = BaseView.extend({
    template: '#donate-share-template',
    className: 'project-donate',
    events: {
        'click .js-navigate-to-purchase': 'navigateToPurchaseDetails'
    },
    construct: function () {
        var subs = location.pathname.split('/');
        var shareId = +subs[subs.length - 1];
        if (/donatesingle/.test(location.href) && !isNaN(shareId)) {
            var shareModel = _.find(this.model.get('shares'), function (it) {
                return it.shareId === shareId;
            });
            shareModel.controller = this.model;
            shareModel.singleShare = true;
            shareModel.isSelected = true;
            this.model.set('singleShare', true);
            this.addChildAtElement('.js-action-card-list', new CampaignDonate.Views.Share({
                model: new CampaignDonate.Models.Share(shareModel)
            }), false);
        } else {
            this.addChildAtElement('.js-action-card-list', new CampaignDonate.Views.ShareList({
                collection: this.model.shares,
                controller: this.model
            }), true);
        }

        this.addChildAtElement('.donate-action.js-manual-input-0', new CampaignDonate.Views.ManualDonateInput({
            model: this.model
        }));

        this.model.trackCampaignSocialTargeting();
    },
    afterRender: function () {
        this.$(".donate-info").stick_in_parent({
            parent: 'body',
            offset_top: 15
        });
    },
    dispose: function () {
        $(window).unbind('scroll.eventView' + this.cid);
        BaseView.prototype.dispose.call(this);
    },
    navigateToPurchaseDetails: function () {
        if (!this.isValid()) {
            return false;
        }
        var share = this.model.shares.getSelectedShare();
        var shareId = share.get('shareId');
        SessionStorageProvider.extendByKey('commonInfo', {orderType: 'SHARE'});
        SessionStorageProvider.extendByKey('sharePurchase_' + shareId, {
            donateAmount: this.model.get('donateAmount'),
            quantity: share.get('quantity')
        });
        if (window.gtm) {
            window.gtm.trackSelectShare(share);
        }
        var campaignAlias = this.controller.get("webCampaignAlias");
        var donateThisShareUrl = '/campaigns/' + campaignAlias + '/donate/' + shareId + '/payment';
        if (share.get('shareStatus') === 'INVESTING' || share.get('shareStatus') === 'INVESTING_WITHOUT_MODERATION' || share.get('shareStatus') === 'INVESTING_ALL_ALLOWED') {
            donateThisShareUrl += 'investfiz';
        } else {
            $.ajax('/api/public/purchase-share', {
                contentType: 'application/json',
                type: 'POST',
                data: JSON.stringify({
                    shareId: share.get('shareId'),
                    quantity: share.get('quantity'),
                    donateAmount: parseInt(this.model.get('donateAmount'))
                })
            }).done(function (response) {
                if (response.success) {
                    var prefix = '';
                    if ((/localhost:/).test(document.location.origin)) {
                        prefix = 'http://localhost:4200';
                    }
                    document.location = prefix + '/campaigns/' + campaignAlias + '/donate/' + response.result + '/payment';
                } else {
                    console.log(response.result)
                    workspace.navigate(donateThisShareUrl);
                }
            }).fail(function (err) {
                console.log(err);
                workspace.navigate(donateThisShareUrl);
            });
        }
    },
    isValid: function () {
        var errMessage;
        if (!this.model.isValidDonateAmount()) {
            if (this.model.shares.getSelectedShare().get('isDonate')) {
                errMessage = 'Сумма не может быть меньше одного рубля.';
            } else {
                errMessage = 'Сумма не может быть меньше цены выбранного вознаграждения.';
            }
            this.$('.js-donate-amount-error').html(errMessage);
            this.$('[name="donateAmount"]')[0].focus();
            return false;
        }
        return true;
    }
});
CampaignDonate.Views.ManualDonateInput = BaseView.extend({
    template: '#manual-donate-input-template',
    modelEvents: _.extend({}, BaseView.prototype.modelEvents, {
        'model change:quantity': 'updateDonateAmount',
        'model change:donateAmount': 'updateDonateAmountFromModel'
    }),
    events: {
        'keyup [name="donateAmount"]': 'onDonateAmountManualChange',
        'mouseup [name="donateAmount"]': 'onDonateAmountManualChange'
    },
    getController: function () {
        return this.controller || this.model;
    },
    onDonateAmountManualChange: _.debounce(function (e) {
        var amount = $(e.currentTarget).val().replace(/\s|\D/g, '');
        this.getController().tryChangeDonateAmount(amount.length === 0 ? amount : this.getParsedDonateAmount(amount), true);
        this.updateDonateAmountFromModel();
    }, 300),
    updateDonateAmountFromModel: function (donateAmount) {
        this.setDonateAmount(donateAmount || this.getController().get('donateAmount'));
    },
    setDonateAmount: function (donateAmount) {
        var $donateAmount = this.$('[name="donateAmount"]');
        if ($donateAmount.val() != donateAmount) { // not "!=="!
            $donateAmount.val(StringUtils.humanNumber(donateAmount));
        }

        var alias = this.getController().get('sponsorAlias');
        var inj = window.injection;
        if (alias && inj && inj.setDonateAmount && inj.setDonateAmount[alias]) {
            inj.setDonateAmount[alias](this.$el, this.model, donateAmount);
        }
    },
    getParsedDonateAmount: function (amount) {
        return DonateUtils.parseDonateAmount(amount);
    },
    updateDonateAmount: function () {
        var donateAmount = this.model.getTotalPrice();
        this.getController().tryChangeDonateAmount(donateAmount, false);
        this.setDonateAmount(donateAmount);
    },
    afterRender: function () {
        var input = this.$el.find('[name="donateAmount"]');
        if (ProfileUtils.isInvestingProject(this.model.get('shareStatus'))) {
            input.attr('disabled', true);
        } else {
            var share = this.model.get('selectedShare');
            if (share && (ProfileUtils.isInvestingProject(share.get('shareStatus')))) {
                input.attr('disabled', true);
            } else {
                input.removeAttr('disabled');
            }
        }
    }

});

CampaignDonate.Views.ModalDeliveryPublicNote = Modal.View.extend({
    className: 'modal modal-delivery-info',
    template: '#delivery-service-public-note-modal-template'

});

CampaignDonate.Views.DeliveryAddressSection = BaseView.extend({
    className: 'pln-payment-box_field-group',
    viewEvents: {
        'keyup [name]': 'onFieldChange'
    },
    template: '#donate-share-details-delivery-section-address-template',
    openPublicNote: function () {
        var dialogView = new CampaignDonate.Views.ModalDeliveryPublicNote({
            model: this.model
        });
        dialogView.render();
    },
    //this method call from parent view when buying share
    setupEmailBullshiting: function () {
        if (this.model.get('hasEmail')) {
            return;
        }
        var mainModel = this.controller.model || this.model;
        var emailBlock = this.$('.js-anchr-email-block');
        var $emailInput = this.$('[name="email"]');
        var selectedShare = this.model;
        var self = this;

        var logoutCallback = function (email) {
            mainModel.backupAll();
            var view = new CampaignDonate.Views.HELLSOMETHING({
                model: new AccountMerge.Models.Content({
                    email: email,
                    alreadyHaveEmail: true,
                    selectedShare: selectedShare
                })
            });
            view.render();

        };

        var afterCheckCallback = function (response) {
            var status = response && response.result;
            self.model.set({
                //maybe we should check for positive statuses, cus we can receive error response
                isEmailValid: status !== 'ALREADY_EXIST' && status !== 'NOT_VALID'
            }, {silent: true});
        };

        var signupCallback = function () {
            mainModel.backupAll();
            LazyHeader.showAuthForm('signup', {
                email: $emailInput.val(),
                focusPassword: true
            });
        };
        // Эта форма используется и в магазине ТОЖЕ, а там не так, там кнопки в другой форме.
        // Поэтому надо использовать глобальный jquery вместо this.$ (см. ниже)
        var submit = this.$('.js-purchase-order');

        $emailInput.checkEmail({
            $submit: submit.length ? submit : $('.shop-order-action .js-submit.shop-btn-primary'),
            $email: $emailInput,
            $hint: emailBlock.find('#hint'),
            $hintContainer: emailBlock.find('.dropdown-popup'),
            disableButtons: false,
            disableButtonOnAlreadyExists: true,
            checkCallback: afterCheckCallback,
            signupCallback: signupCallback,
            logoutCallback: logoutCallback
        });
    },
    afterRender: function () {
        this.setupEmailBullshiting();
    }
});

CampaignDonate.Views.CampaignOrderCallback = UserCallback.Views.BaseView.extend({
    template: '#order-user-callback-template',
    className: 'pln-payment-loyal',
    events: _.extend({}, UserCallback.Views.BaseView.prototype.events, {
        'click .pln-d-switch:not(.disabled)': 'togglePopup'
    }),
    onCallbackClicked: function () {
        var self = this;
        UserCallback.Views.BaseView.prototype.onCallbackClicked.apply(this, arguments).done(function () {
            self.togglePopup();
            self.$el.find('.pln-d-switch').addClass('disabled');
            self.$el.find(".js-callback-state1").addClass("hidden");
            self.$el.find(".js-callback-state2").removeClass("hidden");
        });
    },
    togglePopup: function () {
        this.$el.find('.pln-d-switch').parent().toggleClass('open bottom-up');
    }
});

CampaignDonate.Views.CampaignOrderSupport = BaseView.extend({
    template: '#order-user-support-template',
    className: 'pln-payment-loyal pln-payment-loyal__support'
});


//---Investing Views ------------------------------------------------
CampaignDonate.Views.InvestingPaymentPage = BaseView.extend({
    isExistsPlanetaUiElement: true,
    isSmsSent: false,
    events: {
        'click .js-face-switch': 'changeUserType',
        'click .js-address-toggle': 'realAddressToggle',
        'click .js-return-to-share-selection': 'returnToShareSelection',
        'click .js-purchase-order': '_order',
        'change [name^="reg"]': 'regLocationChanged',
        'click .js-send-sms:not(.disabled)': 'sendSms'
    },
    // Call function only one time
    justCallOne: function me(fn) {
        if (me.count === 2) {
            me.count = 1;
        } else if (!me.count) {
            me.count = 1;
        } else {
            me.count++;
        }
        // two calls case trigger on downloading (may be) I launch only second
        if (me.count === 1)
            return;
        fn.call(this);
    },
    init: function () {
        var self = this;
        if (!ProfileUtils.isInvestingProject(self.model.get('shareStatus'))) {// mistake-proofing
            workspace.navigate('/campaigns/' + self.model.get('campaignId') + '/donate/' + self.model.get('shareId') + "/payment");
        }
        self.model.fetchX({
            url: "/api/orders/invest-order-info.json",
            data: {userType: self._ut},
            method: 'read',
            success: function (data) {
                if (data)
                    self.model.set('investInfo', data);
                else
                    self.model.get('investInfo').userType = self._ut;
//                self.setSavedData('CampaignDonate.Views.InvestingPaymentPage');
            }
        });
    },
    _order: function () {
        var view = this;
        var actionButton = view.$('.js-purchase-order');
        var data = view.model.get('investInfo');
        actionButton.addClass('disabled');
        actionButton.attr('disabled', true);
        view.$('.pln-payment-box_field-row.error').removeClass('error');
        view.$('.ppb_fi_error').remove();

        view.$el.find('[name]').each(function () {
            var $this = $(this);
            data[$this.attr('name')] = $this.val();
        });

        view.model.prePurchase().done(function () {
            var valueCur = view.$el.find('[name="email"]').val(),
                valueOld = view.model.get('email');
            if (!valueOld || (valueCur && valueCur != valueOld)) {
                view.model.set('email', valueCur, {silent: true});
            }
            view.purchaseOrder();
        }).fail(function (_$, errorMessage, fieldErrors) {
            if (errorMessage) {
                workspace.appView.showErrorMessage(errorMessage);
            }
            var deliveryError = false;
            _(fieldErrors).each(function (errorText, fieldName) {
                var field = view.$('[name="' + fieldName + '"]');
                field.closest('.pln-payment-box_field-row').addClass('error');
                field.parent().append('<div class="ppb_fi_error tooltip" data-tooltip="' + errorText + '">?</div>');
                //check by text is a shit, but no way out
                if (fieldName === "email") {
                    var ch1 = errorText === "Пользователь с таким e-mail уже зарегистрирован";
                    if (ch1 || errorText === "session expired") {
                        var value;
                        view.model.backupAll();
                        if (ch1) {
                            value = field.val();
                        } else {
                            value = workspace.appModel.get("myProfile").get("email") || "";
                        }
                        LazyHeader.showAuthForm('signup', {
                            email: value,
                            focusPassword: true
                        });
                    }
                }
            });
            view.doJumpToError();
            actionButton.removeClass('disabled');
            actionButton.attr('disabled', false);
        });
    },
    changeUserType: function () {
        if (!this.model.get('hasEmail')) {
            var profile = workspace.appModel.get('myProfile'),
                email = this.$('[name="email"]').val();
            if (email) {
                profile.set('email', email, {silent: true});
            }
        }
        workspace.navigate('/campaigns/' + this.model.get('campaignId') + '/donate/' + this.model.get('shareId') +
            "/paymentinvest" + this._tgl_url, {data: {userType: this._ut}});
    },
    regLocationChanged: function (e) {
        var name, target;
        if (e instanceof $.Event) {
            name = e.target.name;
            target = $(e.target);
        } else {
            name = e.name;
            target = $(e);
        }
        if (this.regEqLive) {
            var vl = target.val();
            if (target.is('select')) {
                this.setCountry(target, vl, name);
            } else {
                this.$el.find('[name="live' + name.substr(3) + '"]').val(vl);
            }
        }
    },
    afterRender: function fn() {
        var self = this;
        self.regEqLive = self.$el.find('.js-address-toggle > .checkbox').is('.active');
        self.setupEmailBullshiting();
        var gender = self.model.get('investInfo').gender;
        if (gender) {
            $('.pln-payment-box_ui-control .radiobox').each(function (i, it) {
                var me = $(it);
                if (me.attr('data-value') == gender)
                    me.addClass('active');
                else
                    me.removeClass('active');
            });
        }
        self.$el.find('.js-phone').mask('+7 (999) 999-99-99');
        self.$el.find('.js-date').mask('99.99.9999');
    },
    realAddressToggle: function (e) {
        var target = $(e.currentTarget).find('.checkbox'),
            self = this;
        target.toggleClass('active');
        self.regEqLive = !self.regEqLive;
        self.$el.find('#js-address-block').slideToggle(300);
        if (self.regEqLive) {
            self.$el.find('[name^="reg"]').each(function (i, item) {
                self.regLocationChanged(item);
            });
        }
    },
    _addChilds: function () {
        var pmodel = this.model.get('payment');
        this.addChildAtElement('.js-payment-total', new CampaignDonate.Views.PaymentTotal({
            model: pmodel
        }));
    },
    beforeSendSms: function (context) {
        context.$('.js-send-sms').addClass('disabled');
        context.$('.js-phone').addClass('disabled');
    },
    afterSendSmsSuccess: function (context) {
        context.$('.js-confirmation-code').removeClass('hidden');
        workspace.appView.showInfoMessage("Вам отправлено sms с кодом подтверждения");
        setTimeout(function () {
            context.$('.js-send-sms').removeClass('disabled');
            context.$('.js-send-sms').html("Выслать код заново");
        }, 10000);
        isSmsSent = true;
    },
    afterSendSmsError: function (context, errorMessage) {
        context.$('.js-send-sms').removeClass('disabled');
        context.$('.js-phone').removeClass('disabled');
        if (errorMessage) {
            workspace.appView.showErrorMessage(errorMessage);
        }
        isSmsSent = false;
    },
    sendSms: function (e) {
        var self = this;
        e.preventDefault();
        if (self.$('.js-send-sms').attr('disabled')) {
            return;
        }
        self.beforeSendSms(self);

        var phone = self.$('.js-phone').val();
        self.model.confirmPhone(phone)
            .done(function (response) {
                self.afterSendSmsSuccess(self);
            })
            .fail(function (response, errorMessage) {
                self.afterSendSmsError(self, errorMessage);
            });
    },
    setCountry: function (el, newValue, fieldName) {
        var found = el.find('option').filter(function () {
            return $(this).val() == newValue;
        })[0];
        if (!found)
            return;
        var text = found.text;
        this.$el.find('[name="live' + fieldName.substr(3) + '"]')
            .next().find('ul > li > a').each(function (i, it) {
            if (it.text == text) {
                $(it).trigger('click');
                return false;
            }
        });
    }
});
CampaignDonate.Views.InvestingPaymentFizPage = CampaignDonate.Views.InvestingPaymentPage.extend({
    template: '#donate-invest-fiz-template',
    _tgl_url: 'ul',
    _ut: "INDIVIDUAL",
    events: {
        'click .pln-payment-box_ui-control .radio-row': 'changeSex'
    },
    construct: function () {
        this.init();
        var methods = this.model.get('payment').get('methods');
        this.model.set('methsCount', methods.length, {silent: true});
        if (methods.length > 1)
            this.addChildAtElement('.js-project-payment-details-container', new CampaignDonate.Views.Payment({
                model: this.model.get('payment')
            }), true);
        else if (methods.length === 1)
            this.model.paymentMethodId = methods.models[0].get('id');
        this._addChilds();
        _.extend(this.events, CampaignDonate.Views.InvestingPaymentPage.prototype.events);
    },
    changeSex: function (e) {
        var target = $(e.currentTarget).children().first();
        if (target.is('.active')) {
            return;
        }
        var parent = target.parent().parent();
        parent.find('.active').removeClass('active');
        target.addClass('active');
        parent.next().children().first().val(target.attr('data-value'));
    },
    afterRender: function () {
        //    this.justCallOne(function() {
        this.$el.find('.js-passport-number').mask('9999 999999');
        CampaignDonate.Views.InvestingPaymentPage.prototype.afterRender.call(this);
        //    });
    }
});

CampaignDonate.Views.InvestingPaymentUlPage = CampaignDonate.Views.InvestingPaymentPage.extend({
    template: '#donate-invest-ul-template',
    _tgl_url: 'fiz',
    _ut: "LEGAL_ENTITY",
    afterRender: function () {
//        this.justCallOne(function() {
        this.$el.find('.js-inn').mask('9999999999?99');
        CampaignDonate.Views.InvestingPaymentPage.prototype.afterRender.call(this);
//        });
    },
    construct: function () {
        var self = this;
        self.init();
        self.model.paymentMethodId = 12;
        self._addChilds();
    }
});
//-----------------------------------------------------Investing Views---

CampaignDonate.Views.SharePaymentAndDeliveryPage = BaseView.extend({
    template: '#donate-share-details-template',
    isExistsPlanetaUiElement: true,
    events: {
        'change [name]': 'silentUpdateField',
        'onFieldChange': 'silentUpdateField',
        'click .js-return-to-share-selection': 'returnToShareSelection',
        'click .js-return-to-shopping-cart': 'returnToSoppingCart',
        'click .js-purchase-order': 'purchaseOrder',
        'result input[name="customerContacts.city"]': 'silentUpdateField',
        'change input[name="customerContacts.city"]': 'onLocationChange',
        'change select[name="customerContacts.countryId"]': 'onLocationChange',
        'change input.js-anchr-address-cityId': 'onLocationChange',
        'deliverySelected': 'deliveryMethodChanged',
        'afterPaymentGroupChange': 'afterPaymentGroupChange'
    },
    construct: function (options) {
        this.deliveryAddress = this.addChildAtElement('.js-anchor-delivery-services', new CampaignDonate.Views.DeliveryAddressSection({
            controller: this,
            model: this.model.deliverySectionModel
        }), true);
        this.deliveryServices = this.addChildAtElement('.js-project-donate-condition-container div', new CampaignDonate.Views.DeliveryServicesList({
            collection: new BaseCollection(this.model.get('linkedDeliveries'))
        }));

        var el = this.deliveryServices.collection.first();
        if (el) {
            this.deliveryMethodChanged(null, el);
        }

        this.addChildAtElement('.js-project-payment-details-container', new CampaignDonate.Views.Payment({
            model: this.model.get('payment')
        }), true);

        this.addChildAtElement('.js-payment-total', new CampaignDonate.Views.PaymentTotal({
            model: this.model.get('payment')
        }));

        var callback = this.model.get('callback');
        if (callback.get('available')) {
            this.addChildAtElement('.js-right-sidebar', new CampaignDonate.Views.CampaignOrderCallback({
                model: callback
            }));
        }

        this.addChildAtElement('.js-right-sidebar', new CampaignDonate.Views.CampaignOrderSupport({
            model: new BaseModel({
                phone: workspace.supportContacts.supportPhoneNumber,
                email: workspace.supportContacts.supportEmail
            })
        }));

        try {
            if (this.model.get('campaign') && this.model.get('campaign').campaignId) {
                CampaignUtils.trackPixel(this.model.get('campaign').campaignId);
            }
        } catch (ex) {
        }

        if (options && options.controller) {
            options.controller.pageData = this.pageData();
        }
    },
    //https://planeta.atlassian.net/browse/PLANETA-13345
    afterPaymentGroupChange: function (e, val) {
        this.model.set({paymentType: val});
    },
    silentUpdateField: function (e, _view, childEvent) {
        var $input = childEvent && childEvent.currentTarget ? $(childEvent.currentTarget) : $(e.currentTarget);
        this.removeError($input);
        var _names = $input.attr('name').split('.');
        if (_names.length == 1) {
            this.model.set(_names[0], $input.val(), {silent: true});
        } else {
            this.model.get(_names[0])[_names[1]] = $input.val();
        }
    },
    returnToShareSelection: function () {
        this.model.backupAll();
        workspace.navigate('/campaigns/' + this.model.get('campaignId') + '/donate/' + this.model.get('shareId'));
    },
    returnToSoppingCart: function () {
        this.model.backupAll();
        location = '/payment/shopping-cart';
    },
    purchaseOrder: function () {
        var view = this;
        var $purchaseButton = this.$('.js-purchase-order');
        $purchaseButton.addClass('disabled');
        $purchaseButton.attr('disabled', true);
        view.$('.pln-payment-box_field-row.error').removeClass('error');
        view.$('.ppb_fi_error').remove();

        this.model.purchase().fail(function (_$, errorMessage, fieldErrors) {
            if (errorMessage) {
                workspace.appView.showErrorMessage(errorMessage);
            }
            var deliveryError = false;
            _(fieldErrors).each(function (errorText, fieldName) {
                var field = view.$('[name="' + fieldName + '"]');
                field.closest('.pln-payment-box_field-row').addClass('error');
                field.parent().append('<div class="ppb_fi_error tooltip" data-tooltip="' + errorText + '">?</div>');
                //check by text is a shit, but no way out
                if (fieldName == "email" && errorText == "Пользователь с таким e-mail уже зарегистрирован") {
                    view.model.backupAll();
                    LazyHeader.showAuthForm('signup', {
                        email: field.val(),
                        focusPassword: true
                    });
                }

                if (fieldName == "email" && errorText == "session expired") {
                    view.model.backupAll();
                    LazyHeader.showAuthForm('signup', {
                        email: workspace.appModel.get("myProfile").get("email") || "",
                        focusPassword: true
                    });
                }
            });
            view.doJumpToError();
            $purchaseButton.removeClass('disabled');
            $purchaseButton.attr('disabled', false);
        });
    },
    doJumpToError: function () {
        var errorEl = this.$(".error:first");
        if (errorEl.length) {
            new AppView().doJump(errorEl[0], 10);
        }
    },
    removeError: function ($field) {
        $field.closest('.pln-payment-box_field-row').removeClass('error');
        $field.parent().find('.ppb_fi_error').remove();
    },
    deliveryMethodChanged: function (e, modelOrView) {
        var model;
        if (modelOrView instanceof BaseView) {
            model = modelOrView.model;
        } else if (modelOrView instanceof BaseModel) {
            model = modelOrView;
        }

        this.selectNewDeliveryService(model);

        var deliveryPrice = this.currentDeliveryService.get('price');
        var serviceType = this.currentDeliveryService.get('serviceType');
        this.model.set({
            serviceId: this.currentDeliveryService.get('serviceId'),
            deliveryType: serviceType,
            deliveryPrice: deliveryPrice
        });
        this.model.deliverySectionModel.set({
            description: this.currentDeliveryService.get('description'),
            deliveryType: serviceType,
            address: this.currentDeliveryService.get('address') || {}
        });
        var donateAmount = this.model.get('donateAmount');
        this.model.get('payment').setPrice(deliveryPrice + donateAmount);
    },
    selectNewDeliveryService: function (newService) {
        if (this.currentDeliveryService) {
            this.currentDeliveryService.set({
                isSelected: false
            });
        }
        this.currentDeliveryService = newService;

        this.$(".js-error-not-exists-delivery").toggle(!newService);

        if (!newService) {
            this.model.set({
                serviceId: null,
                deliveryType: null,
                deliveryPrice: 0
            }, {silent: true});
            return;
        }

        this.currentDeliveryService.set({
            isSelected: true
        });
    },
    onLocationChange: function (e) {
        var contact = this.model.get('customerContacts');

        contact.country = this.$('[name="customerContacts.countryId"] option[selected]').text() || contact.country;
        contact.countryId = _.isNumber(e) ? e : this.$('[name="customerContacts.countryId"] option[selected]').val() || 0;
        contact.cityId = this.$('[name="customerContacts.cityId"]').val() || 0;
    },
    afterRender: function () {
        var replyTextarea = this.$('textarea[name="reply"]'),
            replyLabel = this.$('.pln-payment-box_author-quest');
        replyTextarea.css('min-height', replyLabel.height());
        replyTextarea.css('resize', 'vertical');
        this.deliveryAddress && this.deliveryAddress.setupEmailBullshiting.call(this);
    },
    getData: function () {
        var data = {},
            model = this.model,
            paymentModel = this.model.get('payment');
        this.$el.find('selectCampaignById[name], input[name]').each(function (i, el) {
            var item = $(el),
                value = item.val();
            if (value)
                data[item.attr('name')] = value;
        });
        var method = paymentModel.get('methods').getSelected();
        if (method) {
            var submethod = paymentModel.get('childrenMethods').getSelected();
            if (submethod)
                method = submethod;
            data.paymentMethodId = method.get('id');
        }
        return data;
    },
    pageData: function () {
        return {
            title: _.template('Выбор способа доставки и оплаты | Проект «<%=projectName%>»', {
                projectName: this.model.get('campaign') ? this.model.get('campaign').name : ''
            }),
            description: _.template('Ответьте на вопросы автора проекта и выберите способ доставки вознаграждения «<%=shareName%>».', {
                shareName: this.model.get('name')
            })
        };
    }
});

['purchaseOrder', 'returnToShareSelection', 'doJumpToError', 'getData'].forEach(function (method) {
    CampaignDonate.Views.InvestingPaymentPage.prototype[method] = CampaignDonate.Views.SharePaymentAndDeliveryPage.prototype[method];
});
CampaignDonate.Views.InvestingPaymentPage.prototype.setupEmailBullshiting = CampaignDonate.Views.DeliveryAddressSection.prototype.setupEmailBullshiting;

CampaignDonate.Views.PaymentTotal = BaseView.extend({
    template: '#donate-share-details-delivery-payment-total-template',
    construct: function () {
        this.model.on('paymentChanged', this.render, this);
    }
});

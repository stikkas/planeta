/*globals JobManager*/
/**
 * usage: var Updater = _.extend(new BaseUpdater(),{options});
 *
 * url
 * schedulerName - Name of the scheduler which will be started when first object will be added
 * objectIdName - name of the model property that holds object id  ( objectId = object.get(objectIdName) ). Default objectId
 * responseObjectIdName - name of the property that holds object id in the objects received from the server. Default: it equals objectIdName
 * newObjectEventName - name of the event which will be triggered after the response from the server (one event for each element of the array). Default: newObject
 * addInReverseOrder - if true the array (received from the server) will be reversed
 * initUpdatePeriod - initial delay before sending the request to the server (in milliseconds). Default 30 seconds
 * incUpdatePeriod - the delay before sending the next request will be previous delay plus this value (in milliseconds). Default 1 second
 * fetchData - describes the data which will be sent to the server
 *    objectsName - name of the array. Default objects
 *    further lists the properties that need to be sent to the server. format key:value
 *    if "value" is function that the function will be call in the context of the current object with one parameter: the current object. The result of the function will be sent as "value"
 *    if "value" begins with '&' and ends with '()' that the method of the object, with name that is between symbols "&" and "(", will be call in the context of the object. The result of the object method will be sent as "value"
 *    if "value" begins with '&' that the property of the current object, with name that is value without symbol "&",  will be sent ( object.get(value) ) as "value"
 *    if "value" equals "&" that name of property is the name of the key ( object.get(key) )
 *    in other case "value" will be sent as is
 *
 */
var BaseUpdater = function () {
    /**
     * Map of objects models
     * key is object identifier
     * value is object model
     */
    this.objects = {};
};

_.extend(BaseUpdater.prototype, {

    DEFAULT_PERIOD: 1000 * 30,
    DEFAULT_INC_PERIOD: 1000,
    dataType: 'json',

    /**
     * Bind to object comment updater
     *
     * @param object
     * @param callback  - call after the response from the server (for each element of array)
     *
     */
    bindToUpdater: function (object, callback, context) {

        this.objectIdName = this.objectIdName || "objectId";
        this.newObjectEventName = this.newObjectEventName || "newObject";
        this.responseObjectIdName = this.responseObjectIdName || this.objectIdName;


        if (_.size(this.objects) === 0) {
            this._initScheduler();
        }

        var objectId = object.get(this.objectIdName);
        if (this.objects[objectId]) {
            return;
        }

        if (callback) {
            object.bind(this.newObjectEventName, callback, context || this);
        } else if (_.isFunction(object[this.newObjectEventName])) {
            object.bind(this.newObjectEventName, object[this.newObjectEventName], object);
        }

        this.objects[objectId] = object;
    },

    unbindFromUpdater: function (object) {
        var objectId = object.get(this.objectIdName);
        var obj = this.objects[objectId];
        if (obj) {
            obj.unbind(this.newObjectEventName);
            delete this.objects[objectId];
            if (_.size(this.objects) === 0) {
                this._removeScheduler();
            }
        }
    },

    unbindAll: function () {
        _.map(this.objects, function (object) {
            this.unbindFromUpdater(object);
        }, this);
    },
    restartJob: function () {
        this._removeScheduler();
        if (_.size(this.objects) > 0) {
            this._initScheduler();
        }
    },

    _initScheduler: function () {
        var self = this;

        var options = {
            type: 'POST',
            success: function (response) {
                if (response.success === false || response.length === 0) {
                    return;
                }

                if (self.addInReverseOrder) {
                    response.reverse();
                }

                _.each(response, function (responseObject) {
                    var responseObjectId = responseObject[self.responseObjectIdName];
                    var object = self.objects[responseObjectId];
                    if (object) {
                        object.trigger(self.newObjectEventName, responseObject);
                    }
                });
            }
        };

        options.dataType = this.dataType;
        if (this.jsonp) {
            options.jsonp = this.jsonp;
        }
        options.url = _.isFunction(this.url) ? this.url() : this.url;

        var checkUpdates = function () {
            options.data = this._getPostData();
            $.ajax(options);
        };

        var initUpdatePeriod = this.initUpdatePeriod || this.DEFAULT_PERIOD;
        JobManager.schedule(this.schedulerName, checkUpdates, initUpdatePeriod, initUpdatePeriod, {
            context: this,
            periodFunc: _.bind(this.periodFunc, this)
        });
    },
    _removeScheduler: function () {
        JobManager.removeJob(this.schedulerName);
    },

    periodFunc: function (period) {
        var newPeriod = _.isNumber(period) ? period : _.isNumber(this.initUpdatePeriod) ? this.initUpdatePeriod : this.DEFAULT_PERIOD;
        newPeriod += _.isNumber(this.incUpdatePeriod) ? this.incUpdatePeriod : this.DEFAULT_INC_PERIOD;
        return newPeriod;
    },

    _getPostData: function () {
        var index = 0;
        var query = "";
        _.map(this.objects, function (object) {
            if (!object) {
                return;
            }


            var fetchData = _.extend({objectsName: "objects"}, this.fetchData);
            var name = fetchData.objectsName + '[' + (index++) + ']';
            delete fetchData.objectsName;

            query += _.reduce(fetchData, function (memo, value, key) {
                if (_.isFunction(value)) {
                    value = value.call(object, object);
                } else {
                    if (value.slice(0, 1) === "&") {
                        if (value.slice(-2) !== "()") {
                            value = object.get(value.slice(1) || key);
                        } else {
                            var fn = object[value.slice(1, -2)];
                            if (fn) {
                                value = _.isFunction(fn) ? fn.call(object) : fn;
                            }
                        }
                    }
                }
                return memo + '&' + name + '.' + key + '=' + value;
            }, "", this);


        }, this);

        return query;
    }
});

/*globals Dialogs,DialogsUpdater,BaseModel,BaseCollection,workspace,Header,Modal,JobManager*/
/*********************************************************************************************
 *
 * Dialogs controller.
 * Contains all info on the current dialogs state.
 *
 *********************************************************************************************/
var DialogsController = BaseModel.extend({
    MIN_FLOAT_DIALOGS_SCREEN_WIDTH: 1280,
    defaults: {
        /**
         * Array of dialogs user participates in now
         */
        dialogs: null,
        unreadDialogs: null,
        activeDialog: null,
        lastOnlineUpdate: null,
        onlineUsers: []
    },
    /**
     * Initializes dialogs controller
     */
    initialize: function (options) {
        BaseModel.prototype.initialize.call(this, options);
        this.set({
            dialogs: new Dialogs.Models.DialogsCollection()
        });
        this.floatDialogViews = {};

        this.popupController = new Dialogs.Models.PopupController({dialogsController: this});
        this.layoutManager = new Dialogs.Models.LayoutManager({appModel: options.appModel});
    },
    /**
     * Asynchronously loads dialog with the specified id.
     */
    loadDialog: function (options) {
        var dialogId = options.dialogId;

        var dialogs = this.get('dialogs');

        // searching for the dialog among active dialogs
        var dialog = dialogs.get(dialogId);
        if (dialog) {
            if (options.loadLastMessagesBeforeOpen) {
                dialog.loadLastMessages(options);
            } else {
                if (options.success) {
                    options.success(dialog);
                }
            }
            return;
        }

        // active dialog not found -- loading it from the server
        dialog = new Dialogs.Models.Dialog();
        dialog.fetch({
            data: {
                dialogId: dialogId
            },
            success: function (model) {
                //Dialog not found or smth is wrong
                if (!model || !model.has('id')) {
                    workspace.navigate('/', true);
                    return false;
                }

                var existingDialog = dialogs.get(model.id);

                if (existingDialog) {
                    if (options.success) {
                        options.success(existingDialog);
                    }
                } else {
                    dialogs.add(model);
                    if (options.success) {
                        options.success(model);
                    }
                }
            },
            error: options.error
        });
    },
    /**
     * Removes specified dialog from the dialogs list.
     *
     * @param dialog Dialog to remove
     * @return Currently active dialog or null (if no dialogs left)
     */
    removeDialog: function (dialog) {
        var dialogs = this.get('dialogs');
        dialogs.remove(dialog);
        if (dialogs.length === 0) {
            this.set({
                activeDialog: null
            });
        } else {
            if (this.get('activeDialog') == dialog) {
                this.set({
                    activeDialog: dialogs.at(0)
                });
            }
        }

        return this.get('activeDialog');
    },
    /**
     * Shows dialog window for sending new message to the user (profileId)
     */
    showSendMessageDialog: function (profileIds) {
        if (!LazyHeader.checkNotAnonymous()) {
            return false;
        }
        var model = new Dialogs.Models.NewMessage({
            profileIds: _.isArray(profileIds) ? profileIds : [profileIds]
        });
        var view = new Dialogs.Views.NewMessage({
            model: model
        });
        Modal.showDialog(view);
    },
    getDialog: function (dialogId) {
        return this.get('dialogs').get(dialogId);
    },
    showDialogsSearch: function () {
        this._showFloatDialogsSearch();
    },
    _showFloatDialogsSearch: function () {
        if (this.floatSearchView) {
            return;
        }
        this.floatSearchView = new Dialogs.Views.FloatDialogsSearch({
            layoutManager: this.layoutManager,
            id: this.layoutManager.DIALOGS_SEARCH_ID
        });
        this.floatSearchView.appendToBody();

        this.layoutManager.setPosition(this.floatSearchView);

        var model = new Dialogs.Models.DialogsSearch({isFloat: true});
        this.floatSearchView.addChild(new Dialogs.Views.DialogsSearch({
            model: model
        }));
        this.floatSearchView.on("onClose", function () {
            this._closeFloatDialogsSearch({byUser: true});
            this._closeAllFloatDialogs({byUser: true});
        }, this);

        var self = this;
        model.fetch({
            error: function () {
                workspace.appView.showErrorMessage('При загрузке "Моих диалогов" произошла ошибка');
                self._closeFloatDialogsSearch();
            }
        });

        this.layoutManager.onViewOpened();
    },
    _closeFloatDialogsSearch: function (options) {
        if (options && options.byUser) {
            this.layoutManager.onViewClosed(this.floatSearchView);
        }
        this.floatSearchView.dispose();
        delete this.floatSearchView;
    },
    showDialog: function (dialogId, options) {
        this._showFloatDialog(dialogId, _.extend({byUser: true}, options));
        this.onDialogActivity();
        this.popupController.removeAllDialogMessages(dialogId);
    },
    _showFloatDialog: function (dialogId, options) {
        options = options || {};
        var floatView = this.floatDialogViews[dialogId];
        if (floatView) {
            if (floatView.dialogBox && options.byUser) {
                floatView.dialogBox.focus();
            }
            return;
        }

        floatView = new Dialogs.Views.FloatDialogBox({
            layoutManager: this.layoutManager,
            id: dialogId
        });
        floatView.appendToBody();
        this.floatDialogViews[dialogId] = floatView;

        floatView.on("onClose", function () {
            this._closeFloatDialog(dialogId, {byUser: true});
        }, this);

        var model = new Dialogs.Models.DialogBox({
            isFloat: true,
            dialogId: dialogId
        });

        var self = this;
        model.fetch({
            success: function () {
                floatView.dialogBox = new Dialogs.Views.DialogBox({
                    model: model
                });

                floatView.dialog = model.get("dialog");
                floatView.addChildAsync(floatView.dialogBox).always(function () {
                    self.layoutManager.setPosition(floatView);
                    floatView.dialogBox.focus();
                });
            },
            error: function () {
                workspace.appView.showErrorMessage("При загрузке диалога произошла ошибка");
                self._closeFloatDialog(dialogId);
            },
            loadLastMessagesBeforeOpen: options.loadLastMessagesBeforeOpen
        });

    },
    _closeFloatDialog: function (dialogId, options) {
        var floatView = this.floatDialogViews[dialogId];
        if (!floatView) {
            return;
        }

        if (options && options.byUser) {
            this.layoutManager.onViewClosed(floatView);
        }

        delete this.floatDialogViews[dialogId];

        floatView.dispose();
    },
    /**
     * Change all periods to default values (by just reschedule jobs)
     * This method should be called from onPageChanged and after any message has been added
     * to a dialog
     */
    onDialogActivity: _.debounce(function () {
        DialogsUpdater.restartJob();
        LazyHeader.loadHeader(LazyHeader.TIMEOUT).done(function () {
            if (Header.eventsModel) {
                Header.eventsModel.messagesModel.restartJob();
            }
        });
    }, 500),
    onPageChanged: function () {
        this.onDialogActivity();
        var self = this;

        this.layoutManager.restoreDialogs(function (id) {
            if (id == self.layoutManager.DIALOGS_SEARCH_ID) {
                self._showFloatDialogsSearch();
            } else {
                self._showFloatDialog(id);
            }
        }, this);
    },
    _closeAllFloatDialogs: function (options) {
        var self = this;
        _.each(this.floatDialogViews, function (view, dialogId) {
            self._closeFloatDialog(dialogId, options);
        });
    },
    isDialogOpen: function (dialogId) {
        return !!this.floatDialogViews[dialogId];
    },
    onUnreadMessagesLoaded: function (loadedCollection) {
        var self = this, i;

        for (i = loadedCollection.length - 1; i >= 0; i--) {
            var dialogInfo = loadedCollection.at(i);
            if (dialogInfo.get('dialog') == null) {
                return;
            }
            var dialogId = dialogInfo.get('dialog').dialogId;
            if (this.isDialogOpen(dialogId)) {
                var dialog = this.getDialog(dialogId);
                if (!this.isExecutingOnDialogsFriendsLoaded || (dialog && dialog.getUnreadMessagesCount() === 0)) {
                    loadedCollection.remove(dialogInfo);
                }
            }
        }

        if (!this.isExecutingOnDialogsFriendsLoaded) {
            if (this.get('dialogsSearchModel')) {
                this.get('dialogsSearchModel').onUnreadMessagesLoaded(loadedCollection);
            }
        }

        if (loadedCollection.length > 0) {
            var unreadMessagesForInvisibleDialogs = loadedCollection.chain().filter(function (dialogInfo) {
                return !self.isDialogOpen(dialogInfo.get('dialog').dialogId);
            }).pluck('attributes').pluck("lastMessage").value();
            this.popupController.onUnreadMessagesLoaded(unreadMessagesForInvisibleDialogs);
        }
    },
    onDialogsFriendsLoaded: function (dialogsCollection) {
        this.isExecutingOnDialogsFriendsLoaded = true;

        dialogsCollection.each(function (item) {
            var dialogId = item.get('dialogId');
            if (this.isDialogOpen(dialogId)) {
                var dialog = this.getDialog(dialogId);
                if (dialog) {
                    item.set('unreadMessagesCount', dialog.getUnreadMessagesCount());
                }
            }
        }, this);

        var unreadMessagesCollection = dialogsCollection.reduce(function (collection, dialog) {
            var dialogInfo = dialog.get('dialogInfo');
            if (dialogInfo.unreadMessagesCount > 0 && dialogInfo.lastMessage) {
                collection.add(dialogInfo);
            }
            return collection;
        }, new BaseCollection());

        LazyHeader.loadHeader(LazyHeader.TIMEOUT).done(function () {
            if (Header.eventsModel) {
                Header.eventsModel.messagesModel.onDialogsFriendsLoaded(unreadMessagesCollection);
            }
        });

        this.isExecutingOnDialogsFriendsLoaded = false;
    },
    onUnreadMessagesChanged: function (dialogId, unreadMessagesCount, lastMessage) {
        if (this.get('dialogsSearchModel')) {
            this.get('dialogsSearchModel').onUnreadMessagesChanged(dialogId, unreadMessagesCount, lastMessage);
        }
        if (unreadMessagesCount === 0) {
            LazyHeader.loadHeader(LazyHeader.TIMEOUT).done(function () {
                if (Header.eventsModel) {
                    Header.eventsModel.lastMessageHasBeenRead(dialogId);
                }
            });
        }
    },
    hasAnyOpenDialog: function () {
        return _.size(this.floatDialogViews);
    }
});

/*global StringUtils, Attach, Modal*/

Attach.Views.ExternalVideo = Modal.OverlappedView.extend({
    template: '#youtube-attachment-link-dialog-template',
    lastChange: 0,
    validImageUrl: null,
    events: {
        'click a.close': 'cancel',
        'click button[type=reset]': 'cancel',
        'click button[type=submit]': 'save',
        'change #img-url': 'onUrlChanged',
        'focus #img-url': 'onUrlChanged',
        'paste #img-url': 'onUrlChanged',
        'keyup #img-url': 'onUrlChanged',
        'keypress #img-url': 'onKeyPress',
        'keydown': 'checkCommandKeys'

    },
    construct: function (options) {
        this.img = new Image();
    },
    onKeyPress: function (event) {
        if (event.keyCode === 10 || event.keyCode === $.ui.keyCode.ENTER) {
            if (!$('button[type=submit]', this.el).attr('disabled')) {
                this.save(event);
            }
            return false;
        }
    },
    onUrlChanged: _.debounce(function (event) {
        var url = $('#img-url').val();
        this.lastChange = new Date().getTime();
        var self = this;

        if (url != this.lastValue && url != '') {
            this.lastValue = url;
            $('.upload-pic-option .video-name', self.el).text('');
            $('button[type=submit]', self.el).attr('disabled', 'disabled');
            $('.upload-pic-option .video-duration', self.el).html('');
            $('.upload-pic-option .video-name', self.el).html(
                '<i class="icon-load"></i>'
            );
            this.lastValue = url;
            // prevent empty url send
            if (url) {
                $.ajax({
                    url: '/api/public/video-external.json',
                    data: {url: url},
                    type: 'GET',
                    success: function (response) {
                        if (response && response.videoUrl && response.imageUrl) {
                            // this value can be passed into HtmlValidator and tried to be parsed with new URL()
                            self.videoId = response.cachedVideoId;
                            self.validValue = response.videoUrl; // @author s.kalmykov
                            $('button[type=submit]', self.el).attr('disabled', null);
                            self.validImageUrl = response.imageUrl;
                            $('.upload-pic-option .video-name', self.el).text('Название: ' + response.name);
                            self.validDuration = response.duration;
                            $('.upload-pic-option .video-duration', self.el).text('Продолжительность: ' + StringUtils.humanDuration(self.validDuration));
                            $('.upload-pic-img img', self.el)[0].src = self.validImageUrl;
                            self.videoType = response.videoType;
                        } else {
                            $('.upload-pic-img img', self.el).attr('src', workspace.staticNodesService.getResourceUrl("/images/defaults/upload-pic.png"));
                            $('.upload-pic-option .video-name', self.el).text('Не удалось получить информацию о видео.');
                            $('.upload-pic-option .video-duration', self.el).text('');
                        }
                    }
                });
            }
        }
    }, 750),
    save: function (event) {
        event.preventDefault();
        event.stopImmediatePropagation();
        var tube = new BaseModel({
            videoId: this.videoId,
            imageUrl: this.validImageUrl,
            tubeId: this.validValue,
            duration: this.validDuration,
            videoType: this.videoType,
            external: true
        });

        this.model.selectPhoto(tube);
        this.dispose();
    },

    afterRender: function () {
        Modal.OverlappedView.prototype.afterRender.call(this);
        $('#img-url').focus();
    }
});


/*globals FileUploader,Modal,FileUploadStatuses,FileUploadListView,FileUploadListCollection,FileUploadModel,FileUploadView,ImageUtils,FileUploadImage,StringUtils,FileUploadMsg,UploaderProgressView,FileListOptionView,DefaultContentScrollListView*/

var UploadSeveralFilesView = Modal.OverlappedView.extend({
    template: '#upload-several-files',
    modelEvents: {
        "change:isOkEnable": "onOkEnable",
        "change:isCancelEnable": "onCancelEnable",
        "change:isUploadEnable": "onUploadEnable"
    },
    afterRender: function () {
        $('.modal-dialog').addClass('modal-lg');
        Modal.OverlappedView.prototype.afterRender.apply(this, arguments);
        this.model.onAfterRenderView();
        this.addChildAtElement(".modal-scroll-content", this.model.fileUploadListView);

        var msg = this.model.get("dropText");
        if (msg) {
            msg += '<br/>';
        }
        msg += this.model.get("text");
        this.$(".uploader-droptext").html(msg);
    },
    cancel: function () {
        this.dispose();
        this.model.cancelUpload();
    },
    save: function () {
        this.dispose();
        this.model.saveResult();
    },
    onOkEnable: function () {
        this.$('button[type=submit]').attr('disabled', !this.model.get("isOkEnable"));
    },
    onCancelEnable: function () {
        this.$('button[type=reset]').attr('disabled', !this.model.get("isCancelEnable"));
        if (this.model.get("isCancelEnable")) {
            this.$('a.close').show();
        } else {
            this.$('a.close').hide();
        }
    },
    onUploadEnable: function () {
        if (this.model.get("isUploadEnable")) {
            this.$('.fileinput-button').show();
        } else {
            this.$('.fileinput-button').hide();
        }
    }
});

var UploadSeveralFilesModel = BaseModel.extend({
    defaults: {
        title: 'Загрузка файла',
        text: 'Это текст с описанием этой загрузки. Его нужно поменять в модели.',
        successCallback: null,
        autoSaveResult: false,
        saveEnabled: false,
        uploadPath: '/uploadImage',
        fileType: 'image',
        uploadResult: null,
        uploadSettings: {
            file_upload_limit: 0,
            file_post_name: 'userfile1',
            post_params: {
                clientId: 0,
                ownerId: 0
            },
            file_size_limit: '10 MB',
            file_types: '*.png;*.jpg;*.jpeg;*.gif',
            file_types_description: 'Image files'
        },
        isOkEnable: true,
        isCancelEnable: true,
        isUploadEnable: true,
        canDropFiles: false
    },
    initialize: function (options) {
        BaseModel.prototype.initialize.call(this, options);

        // Binding all methods to this object context
        _.bindAll(this);

        // Setting handlers functions
        var settings = _.clone(this.defaults.uploadSettings);

        // Overriding settings fields with properties passed in constructor
        if (options && options.uploadSettings) {
            _.extend(settings, options.uploadSettings);
        }

        // Setting new upload settings
        this.set({
            uploadSettings: settings
        });

        if (!this.get("dropText")) {
            this.set("dropText",
                this.isOneFileMode() ? this.translate('dragAndDropFileHere', this.getCurrentLang()) : this.translate('dragAndDropFilesHere', this.getCurrentLang()));
        }

    },
    getCurrentLang: function () {
        return window.workspace && (workspace.currentLanguage === "en") ? "en" : "ru";
    },
    L10n: {
        _dictionary:{
            "ru":{
                "dragAndDropFileHere":"Перетащите файл сюда",
                "dragAndDropFilesHere":"Перетащите файлы сюда",
                "tooManyFiles":"Вы пытаетесь закачать слишком много файлов.\nВы можете выбрать только один файл.",
                "notSupportedFileType":"Вы пытаетесь закачать файл неподходящего типа: ",
                "tooBigFile":"Вы пытаетесь закачать слишком большой файл: ",
                "fileSizeLimit":". Файл должен быть не более ",
                "videoIsConverting":"Видео конвертируется",
                "uploadingIsComplete":"Загрузка завершена"
            },
            "en":{
                "dragAndDropFileHere":"Drag and drop file here",
                "dragAndDropFilesHere":"Drag and drop files here",
                "tooManyFiles":"Too many files.\nPlease choose only one file.",
                "notSupportedFileType":"Unsupported file format: ",
                "tooBigFile":"You are trying to upload too big file: ",
                "fileSizeLimit":". File size must be less then ",
                "videoIsConverting":"The video is converting",
                "uploadingIsComplete":"Uploading is complete"
            }
        }
    },
    translate: function (word, lang) {
        if (!lang)
            throw "language is empty.";
        return this.L10n._dictionary[lang][word] || word;
    },
    isOneFileMode: function () {
        return this.get("uploadSettings").file_upload_limit == 1;
    },
    removeNotUploadedFiles: function () {
        var i = this.fileUploadListCollection.length;
        var fileUploadModel;
        while (i-- > 0) {
            fileUploadModel = this.fileUploadListCollection.at(i);
            if (fileUploadModel.get("status") !== FileUploadStatuses.SAVED) {
                this.fileUploadListCollection.remove(fileUploadModel);
            }
        }
    },
    cancelUpload: function () {
        this.removeNotUploadedFiles();
        if (!this.isOneFileMode()) {
            this.saveResult();
        }

    },
    saveResult: function () {
        this.removeNotUploadedFiles();
        if (this.get("successCallback")) {
            if (this.fileUploadListCollection.length > 0) {
                this.get("successCallback")(this.fileUploadListCollection);
            }
        }

    },

    setButtonStatus: function () {
        if (this.isOneFileMode()) {
            this.set('isOkEnable', this.fileUploadListCollection.length == 1
                && this.fileUploadListCollection.at(0).get('status') === FileUploadStatuses.SAVED);

            this.setEnableAddFile(this.fileUploadListCollection.length == 0);
        } else {
            var areAllFilesUploaded = this.fileUploadListCollection.length
                && this.haveAllFilesTheRequiredStatus([FileUploadStatuses.SAVED, FileUploadStatuses.ERROR, FileUploadStatuses.CANCELED]);
            this.set("isOkEnable", areAllFilesUploaded);
            this.set("isCancelEnable", !areAllFilesUploaded);
        }
    },
    haveAllFilesTheRequiredStatus: function (requiredStatus) {
        return this.fileUploadListCollection.all(function (fileUploadModel) {
            if (typeof requiredStatus === "string") {
                return fileUploadModel.get('status') === requiredStatus;
            }

            return _.include(requiredStatus, fileUploadModel.get('status'));

        });
    },
    setEnableAddFile: function (enabled) {
        this.set('isUploadEnable', enabled);
        this.fileUploader.setDropZone(enabled ? this.get("dropZone") : null);
    },
    onAfterRenderView: function () {
        this.fileUploader = new FileUploader({
            url: 'https://' + workspace.staticNodesService.getStaticNode() + this.get("uploadPath"),
            paramName: this.get("uploadSettings").file_post_name,
            dropZone: $(".uploader-container"),
            headers: this.get("uploadSettings").post_params,
            listener: this
        });

        this.set("canDropFiles", this.fileUploader.canDropFiles());

        this.fileUploadListCollection = new FileUploadListCollection();
        this.fileUploadListView = new FileUploadListView({
            itemModel: FileUploadModel,
            itemViewType: FileUploadView,
            collection: this.fileUploadListCollection
        });


        this.fileUploadListCollection.on('remove', function (fileUploadModel) {
            this.fileUploader.cancel(fileUploadModel.get("file"));
            this.setButtonStatus();
        }, this);

        this.setButtonStatus();
    },
    onAdd: function (file) {
        var fileUploadModel = new FileUploadModel({
            fileName: file.name,
            fileSize: file.size,
            uploadedSize: 0,
            file: file,
            allowRemoveFileAfterUpload: this.isOneFileMode()
        });

        file.fileUploadModel = fileUploadModel;
        this.fileUploadListCollection.add(fileUploadModel);

        var msg;
        if (this.isOneFileMode() && this.fileUploadListCollection.length > 1) {
            msg = this.translate('tooManyFiles', this.getCurrentLang());
        } else if (!FileUploader.isGoodFileExtention(file.name, this.get("uploadSettings").file_types)) {
            msg = this.translate('notSupportedFileType', this.getCurrentLang()) + file.name;
        } else if (((this.get("uploadSettings").file_size_limit > 0) || (this.get("uploadSettings").file_size_limit.length > 0))
                && (file.size > FileUploader.parseFileLimitSize(this.get("uploadSettings").file_size_limit))) {
            msg = this.translate('tooBigFile', this.getCurrentLang()) + file.name + this.translate('fileSizeLimit', this.getCurrentLang()) + this.get("uploadSettings").file_size_limit;
        }

        if (msg) {
            workspace.appView.showErrorMessage(msg);
            fileUploadModel.set({
                status: FileUploadStatuses.ERROR,
                errorMessage: msg
            });
        } else {
            fileUploadModel.set("status", FileUploadStatuses.PENDING);

            fileUploadModel.on('change:status', function () {
                this.setButtonStatus();
            }, this);
            this.fileUploader.send(file);
        }

    },
    onProgress: function (file, loadedSize) {
        file.fileUploadModel.set("uploadedSize", loadedSize);
        if (loadedSize >= file.size) {
            file.fileUploadModel.set("status", FileUploadStatuses.FINISHED);
        }
    },
    onDone: function (file, result) {

        if (result.success) {
            var imgSrc;
            var uploadResultText;
            if (result.result.imageUrl) {
                imgSrc = ImageUtils.getThumbnailUrl(result.result.imageUrl, ImageUtils.ALBUM_COVER);
            } else if (this.get('fileType') === 'video') {
                imgSrc = workspace.staticNodesService.getResourceUrl("/images/upload/file-uploaded.png");
                uploadResultText = this.translate('videoIsConverting', this.getCurrentLang());
            } else if (this.get('fileType') === 'audio') {
                imgSrc = workspace.staticNodesService.getResourceUrl("/images/upload/audio-uploaded.png");
                uploadResultText = result.result.trackName;
            } else {
                imgSrc = workspace.staticNodesService.getResourceUrl("/images/upload/file-uploaded.png");
                uploadResultText = this.translate('uploadingIsComplete', this.getCurrentLang());
            }

            file.fileUploadModel.set({
                uploadedSize: file.size,
                status: FileUploadStatuses.SAVED,
                uploadResult: result.result,
                src: imgSrc,
                uploadResultText: uploadResultText
            });
            if (this.get("autoSaveResult") && (this.isOneFileMode() || this.haveAllFilesTheRequiredStatus(FileUploadStatuses.SAVED))) {
                this.set("isUploadComplete", true);
            }
        } else {
            file.fileUploadModel.set({
                status: FileUploadStatuses.ERROR
            });
            console.log(result.errorMessage);
        }
    },
    onError: function (file) {
        if (file.fileUploadModel) {
            file.fileUploadModel.set("status", FileUploadStatuses.ERROR);
        }
    },
    onCancel: function (file) {
    }
});

var FileUploadView = BaseView.extend({
    tagName: "li",
    className: "filelist-item",
    template: "#file-upload-template",
    modelEvents: {
        'change:status': 'onStatusChanged',
        'change:uploadedSize': 'onUploadedSizeChanged'
    },
    onStatusChanged: function () {
        var $img = this.$(".filelist-img > img");
        var imgPath = this.model.get("status") == FileUploadStatuses.SAVED ? this.model.get('src') : workspace.staticNodesService.getResourceUrl(FileUploadImage[this.model.get('status')]);

        if (imgPath === "") {
            $img.attr("src", "").css("display", "none");
        } else {
            $img.attr("src", imgPath).css("display", "");
        }

        var status = this.model.get('status');
        if (status === FileUploadStatuses.FINISHED || status === FileUploadStatuses.SAVED) {
            this.removeProgressView();
            this.removeToolbar();
        } else {
            this.addToolbar();
        }

        if (status === FileUploadStatuses.STARTED) {
            this.addProgressView();
        }

        if (status === FileUploadStatuses.ERROR) {
            this.removeProgressView();
        }
        this.setTooltipText();
    },
    onUploadedSizeChanged: function () {
        if (this.model.get('status') === FileUploadStatuses.PENDING) {
            this.model.set('status', FileUploadStatuses.STARTED);
        }
        this.setTooltipText();
    },

    setTooltipText: function () {
        var line = [];
        line.push(this.model.get('fileName'));

        if (this.model.get('status') === FileUploadStatuses.STARTED) {
            line.push(StringUtils.humanFileSize(this.model.get("uploadedSize")) + " / " + StringUtils.humanFileSize(this.model.get("fileSize")));
        } else if (this.model.get('status') === FileUploadStatuses.ERROR && this.model.get("errorMessage")) {
            line.push(this.model.get("errorMessage"));
        } else {
            var lang = workspace && workspace.currentLanguage ? workspace.currentLanguage : 'ru';
            line.push(this.model.get("uploadResultText") || FileUploadMsg._translate([this.model.get('status')], lang));
        }

        this.$(".filelist-img").attr("data-html", "true").attr("data-tooltip", line.join('<br/>'));
    },

    addProgressView: function () {
        if (this.progressView) {
            return;
        }
        this.progressView = new UploaderProgressView({model: this.model});
        this.addChild(this.progressView);
    },
    removeProgressView: function () {
        if (this.progressView) {
            this.removeChild(this.progressView);
            this.progressView = null;
        }
    },
    addToolbar: function () {
        if (this.toolbarView) {
            return;
        }
        this.toolbarView = new FileListOptionView({model: this.model});
        this.addChild(this.toolbarView);
    },

    removeToolbar: function () {
        if (this.model.get("allowRemoveFileAfterUpload")) {
            return;
        }
        if (this.toolbarView) {
            this.removeChild(this.toolbarView);
            this.toolbarView = null;
        }
    }

});

var FileListOptionView = BaseView.extend({
    className: "filelist-option",
    template: "#filelist-option-template",
    events: {
        "click .icon-close": "close"
    },
    close: function () {
        this.model.destroy();
    }
});

var UploaderProgressView = BaseView.extend({
    className: "uploader-filelist-progress",
    template: "#uploader-filelist-progress-template",
    modelEvents: {
        'change:uploadedSize': 'onUploadedSizeChanged'
    },
    onUploadedSizeChanged: function () {
        var percent = (this.model.get("uploadedSize") / this.model.get("fileSize") * 100).toFixed();
        this.$(".uploader-filelist-load").css("width", percent + '%');
    }
});

var FileUploadListView = DefaultContentScrollListView.extend({
    tagName: "ul",
    className: "uploader-filelist",
    pagerLoadingTemplate: ""
});

var FileUploadListCollection = BaseCollection.extend({
    model: FileUploadModel,
    destroy: function () {
        _.each(this.toArray(), function (item) {
            item.destroy();
        });
    }
});


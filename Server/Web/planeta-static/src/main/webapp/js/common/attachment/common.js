/*globals UploadController,Photo,DefaultContentScrollListView,StringUtils*/

/**
 * Attachment common
 */

var Attach = {
    showEditorLink: function (selected, previousUrl) {
        var model = new Attach.Models.Link({
            selected: selected
        });
        var view = new Attach.Views.Link({
            model: model
        });
        view.enteredUrl = previousUrl;
        Modal.showDialog(view);
    },
    showPhotoSource: function (profileId, selected) {
        var model = new BaseModel({
            profileId: profileId,
            selected: selected,
            shortcut: true
        });

        var view = new Attach.Views.PhotoSource({
            model: model
        });
        Modal.showDialog(view);
    },
    showPhotoLink: function (nothing, selected, underlyingModal) {
        var model = new Attach.Models.PhotoSelect({
            selected: selected,
            step: 'showPhotoLink'
        });
        var view = new Attach.Views.PhotoLink({
            model: model,
            underlyingModal: underlyingModal
        });
        Modal.showDialog(view);
    },
    showUploadSinglePhotoDialog: function (selected, underlyingModal) {
        var model = new BaseModel({
            profileId: workspace.appModel.myProfileId(),
            selected: selected,
            shortcut: true
        });

        var view = new Attach.Views.SinglePhotoSource({
            model: model,
            underlyingModal: underlyingModal
        });
        underlyingModal && underlyingModal.unbindKeyClose();
        Modal.showDialog(view);
    },
    showPhotoUpload: function (profileId, selected, shortcut, underlyingModal, limit) {
        var ModelPrototype = shortcut ? Attach.Models.Photo : Attach.Models.PhotoSelect;
        var model = new ModelPrototype({
            selected: selected,
            profileId: profileId,
            step: 'showPhotoUpload'
        });

        var L10n = {
            _dictionary:{
                "ru":{
                    "addPhoto":"Загрузка фотографии",
                    "photoSupportedFormats":"Поддерживаются форматы JPG, PNG и GIF."
                },
                "en":{
                    "addPhoto":"Add image",
                    "photoSupportedFormats":"Supported formats: JPG, PNG и GIF."
                }
            }
        };

        var translate = function (word, lang) {
            if (!lang)
                throw "language is empty.";
            return L10n._dictionary[lang][word] || word;
        };

        var lang = window.workspace && (workspace.currentLanguage === "en") ? "en" : "ru";

        var uploadOptions = {
            title: translate('addPhoto', lang),
            text: translate('photoSupportedFormats', lang),
            uploadPath: '/uploadImage',
            uploadSettings: {
                post_params: {
                    clientId: profileId,
                    ownerId: profileId,
                    albumId: 0,
                    albumTypeId: AlbumTypes.ALBUM_COMMENT
                },
                file_upload_limit: limit || 0
            },
            autoSaveResult: true,
            underlyingModal: underlyingModal,
            successCallback: function(filesUploaded) {
                filesUploaded.each(function(fileUploaded) {
                    var uploadResult = fileUploaded.get('uploadResult');
                    if (uploadResult) {
                        model.selectPhoto(new BaseModel(uploadResult));
                    }
                });
            }
        };
        UploadController.exec("showUploadModalDialog", uploadOptions);
    },

    /**
     * Possible options:<br/>
     * <b>selected</b>: mandatory, callback function, called when user has selected external video<br/>
     * <b>playerSize</b>: size of video player embed-code. Possible values: 'big', 'small'. Default: 'big'<br/>
     * <b>addVideoToProfileId</b>: should we automatically add parsed video to user videos. Default: true.<br/>
     * @param options
     */
    showExternalVideo: function (options) {
        options = _.extend({
            selected: function() {
                alert('selected callback is not set!');
            },
            playerSize: 'big',
            addVideoToProfileId: workspace.appModel.myProfileId()
        }, options || {});

        var model = new Attach.Models.ExternalVideo({
            selected: options.selected,
            playerSize: options.playerSize,
            addVideoToProfileId: options.addVideoToProfileId,
            step: 'showExternalVideo'
        });
        Modal.showDialogByViewClass(Attach.Views.ExternalVideo, model, null, true, null, null, true);
    }
};

Attach.Models = {};
Attach.Views = {};

/*****************************************************************
 * Base model for attachments
 *****************************************************************/
Attach.Models.BaseModel = BaseModel.extend({

    /**
     * Generates bbcode for the selected tag and calls
     * selectedCallback. Then destroys model.
     */
    select: function (bbcodeTag, attributes, value) {
        var attached = this.getAttached(bbcodeTag, attributes, value);
        var callback = this.get('selected');
        if (!callback) {
            alert('No selected callback specified!');
            return;
        }

        var cid = callback(attached);
        this.destroy();
        return cid;
    },

    getDuration: function (duration) {
        return duration ? StringUtils.humanDuration(duration) : '00:00';
    },

    getAttached: function (bbcodeTag, attributes, value) {
        var bbcode = this.getBbcode(bbcodeTag, attributes, value);

        return $.extend({bbcode: bbcode, value: value}, attributes);
    },

    getBbcode: function (bbcodeTag, attributes, value, id, owner, url, duration) {
        var bbcode = '[' + bbcodeTag;
        _.each(attributes, function (value, name) {
            bbcode += ' ' + name +'=' + value;
        });
        bbcode += ']';
        if (value) {
            bbcode += value;
        }
        bbcode += '[/' + bbcodeTag + ']';
        return bbcode;
    }
});

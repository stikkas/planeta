/*globals  JobManager, console*/
/**
 * Javascript that renders banners.
 *
 * Usage:
 * Banner.init('https://planeta.ru', true);
 * Banner.dispose();
 *
 */

var Banner = function () {


    var UPDATE_REFRESH_PERIOD = 30000;
    var SHOP_UPDATE_REFRESH_PERIOD = 7000;
    var banners = [];
    var maxErrorCount = 2;

    var disposeOneBanner = function (banner) {
        var anchor = $(banner.selector);
        anchor.empty();
        banner.previousIds = [];
        banner.errorCount = 0;
        JobManager.removeJob(banner.taskName);
    };


    var BannerPeriod = {
        STOPABLE: "STOPABLE",
        RANDOM: "RANDOM",
        COHERENT: "COHERENT",
        ONCE: "ONCE"
    };

    var createBanner = function (type,
                                 selector,
                                 period,
                                 REFRESH_PERIOD,
                                 taskName) {
        var banner = {};

        banner.bannerType = type || 'TOP_LINE';
        banner.selector = selector || '.js-banner-container-' + banner.bannerType.toLowerCase();
        banner.period = period || BannerPeriod.STOPABLE;
        banner.REFRESH_PERIOD = REFRESH_PERIOD || UPDATE_REFRESH_PERIOD;
        banner.taskName = taskName || 'job-name' + banner.bannerType;
        banner.previousIds = [];
        banner.errorCount = 0;
        return banner;
    };


    var handleEvents = function (banner, bannerId) {
        var anchor = $(banner.selector);
        anchor.find('.js-close-banner').click(function (e) {
            e.preventDefault();
            disposeOneBanner(banner);
        });
        anchor.unbind("click");
        anchor.click(function (e) {
            workspace.stats.trackBannerClick(bannerId);
        });
        workspace.stats.trackBannerAppear(bannerId);
    };

    var parseResponse = function (banner, response) {
        var hiddenBannerId;
        var anchor = $(banner.selector);
        try {
            hiddenBannerId = $(response).find('.js-hidden-banner-id').val();
            if (!hiddenBannerId) {
                JobManager.removeJob(banner.taskName);
                return;
            }
            if (banner.period === BannerPeriod.COHERENT) {
                banner.previousIds = [hiddenBannerId];
            } else {
                banner.previousIds.push(hiddenBannerId);
            }
            anchor.animate({opacity: 0}, 150, function () {
                anchor.empty().append(response);
                handleEvents(banner, hiddenBannerId);
                anchor.animate({opacity: 1}, 150);
            });

        } catch (e) {
            JobManager.removeJob(banner.taskName);
        }
    };


    var getRandomBanner = function (banner, host, isAuthor) {
        var anchor = $(banner.selector);
        if (anchor.size() === 0) {
            if (banner.errorCount === 0) {
                console.log("No anchor on page " + banner.bannerType);
            }
            if (banner.errorCount > maxErrorCount) {
                disposeOneBanner(banner);
            } else {
                banner.errorCount = banner.errorCount + 1;
            }
            return;
        }

        var url = (host || "") + "/api/util/random-banner.html";
        var data = {
            bannerType: banner.bannerType,
            requestUrl: encodeURI(window.location.href),
            isMobile: !!window.isMobileDev,
            isAuthor: isAuthor
        };
        if (banner.period !== BannerPeriod.RANDOM && banner.previousIds.length > 0) {
            data['previousIds'] = banner.previousIds;
        }
        $.get(url, data).done(function (response) {
            parseResponse(banner, response);
        }).fail(function () {
            disposeOneBanner(banner);
        });
    };

    var initOneBanner = function (banner, host, isAuthor) {
        disposeOneBanner(banner);
        if (banner.period === BannerPeriod.ONCE) {
            JobManager.scheduleOnce(banner.taskName, getRandomBanner, 200, {
                args: [banner, host, isAuthor]
            });
            return;
        }
        JobManager.schedule(banner.taskName, getRandomBanner, 200, banner.REFRESH_PERIOD, {
            args: [banner, host, isAuthor]
        });
    };

    var init = function (host, isAuthor) {

        banners = [
            createBanner("TOP_LINE", '.js-banner-container-top'),
            createBanner('TEASER', '.sidebar-banner-js', BannerPeriod.COHERENT, SHOP_UPDATE_REFRESH_PERIOD),
            createBanner('SHOP', '.js-banner-shop-main-big', BannerPeriod.COHERENT, SHOP_UPDATE_REFRESH_PERIOD),
            createBanner("LOW_LINE", '.js-projects-welcome-banner-container-new'),
            createBanner('PROFILE', '.banner-profile-sidebar'),
            createBanner('CAMPAIGN_LOWLINE', '.lowline-campaign-banner-js'),
            createBanner('CAMPAIGN_SIDEBAR', '.sidebar-campaign-banner-js')
        ];

        banners.forEach(function (banner) {
            initOneBanner(banner, host, isAuthor);
        });
    };

    var dispose = function () {
        banners.forEach(function (banner) {
            disposeOneBanner(banner);
        });
    };
    return {
        init: init,
        dispose: dispose
    }
}();

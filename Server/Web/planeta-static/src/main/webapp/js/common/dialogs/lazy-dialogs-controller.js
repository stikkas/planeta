/*globals moduleLoader, StorageUtils, DialogsController*/
var LazyDialogsController = BaseModel.extend({
    initialize: function (options) {
        BaseModel.prototype.initialize.apply(this, arguments);
        if (StorageUtils.get("dialogs.positionInfo." + options.appModel.get('myProfile').get('profileId'))) {
            this.loadAndRun('onPageChanged');
        }
    },

    loadAndRun: function (funcName, args, ms) {
        var self = this;

        if (window.isMobileDev) {
            return;
        }

        return moduleLoader.loadModule('dialogs-common', ms).done(function () {
            if (!self.dialogsController) {
                self.dialogsController = new DialogsController({appModel: workspace.appModel});
                workspace.appModel.set('dialogsController', self.dialogsController);
            }
            self.dialogsController[funcName].apply(self.dialogsController, args);
        });
    },
    onPageChanged: function () {
        //nothing to do
    }
});

_(["onUnreadMessagesLoaded", "showDialogsSearch", "showSendMessageDialog"]).each(function (funcName) {
    LazyDialogsController.prototype[funcName] = function () {
        return LazyDialogsController.prototype.loadAndRun(funcName, arguments);
    };
});
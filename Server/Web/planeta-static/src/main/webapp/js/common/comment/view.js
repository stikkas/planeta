/*globals Comments, Likes, PrivacyUtils, BaseRichView,BaseView*/
Comments.Views.FootView = BaseView.extend({
    tagName: 'div',
    className: 'attach-comment-textarea',
    template: '#comment-last-template',
    lineHeight: 0,
    restCount: 400,

    events: {
        'focusin textarea': 'open',
        'click textarea': 'open',
        'click a.send-comment': 'send',
        'keypress textarea': 'onKeypress',
        'keydown textarea': 'onKeypress'
    },

    construct: function () {
        //define comment author attributes

        this.model.set({
            myImageUrl: workspace.appModel.get('myProfile').get('smallImageUrl'),
            myProfileId: workspace.appModel.myProfileId(),
            myProfileAlias: workspace.appModel.myProfileAlias(),
            myProfileName: workspace.appModel.get('myProfile').get('displayName')
        }, {
            silent: true
        });
    },

    bindClickOff: function () {
        var self = this;
        this.$el.clickOff(function (e) {
            self.close(e);
        });
    },

    open: function (e) {
        if (e) {
            e.preventDefault();
            e.stopPropagation();
        }

        if (!LazyHeader.checkNotAnonymous() || this.model.get('sending') || this.$el.hasClass('open')) {
            return;
        }

        this.$el.addClass('open');
        this.bindClickOff();
        this.model.markNewCommentsAsShown();
        this.bindTextarea();
        //this.$('.js-comment-textarea').limitInput('show');
    },

    setTextValue: function (text) {
        var textarea = this.getTextarea();
        if (textarea) {
            textarea.val($.trim(text));
            textarea.trigger('update');//for $.elastic
        }
    },
    getTextValue: function () {
        var textarea = this.getTextarea();
        return $.trim(textarea ? textarea.val() : '');
    },

    close: function (e) {

        if (e) {
            e.preventDefault();
            e.stopPropagation();
        }
        var text = this.getTextValue();
        this.setTextValue(text);
        if (text.length == 0) {
            this.$el.removeClass('open');
            this.model.unset('parentCommentId');
        } else {
            this.bindClickOff();
        }
        //this.$('.js-comment-textarea').limitInput('hide');
    },

    send: function (e) {
        if (e) {
            e.preventDefault();
            e.stopPropagation();
        }
        if ($(e.currentTarget).hasClass('disabled')) {
            return;
        }
        this.sendComment();
    },

    onKeypress: function (e) {
        if (e.keyCode != $.ui.keyCode.ESCAPE) {
            e.stopPropagation();
        }
        if (this.model.get('sending')) {
            e.stopPropagation();
            e.preventDefault();

            return false;
        }

        if (e.ctrlKey && (e.keyCode == 13 || e.keyCode == 10)) {
            e.preventDefault();
            e.stopPropagation();
            this.sendComment();
        }
    },

    sendCommentInternal: function (callback) {
        if (!LazyHeader.checkNotAnonymous()) {
            return false;
        }
        var text = this.getTextValue();
        var self = this;
        if (text.length > 0) {
            this.model.send(text, function () {
                self.setTextValue('');
                if (callback) {
                    callback();
                }
            });
        }
    },

    sendComment: function () {
        var self = this;
        this.sendCommentInternal(function () {
            self.close();
        });

    },

    beforeRender: function () {
        this.saveText();
    },

    saveText: function () {
        this.text = this.getTextValue();
    },
    restoreText: function () {
        this.setTextValue(this.text);
    },

    bindTextarea: function () {
        this.textarea = this.$('textarea');
        if (this.textarea) {
            this.textarea.elastic(true);
        }
    },
    getTextarea: function () {
        if (!this.textarea) {
            this.bindTextarea();
        }
        return this.textarea;
    },

    afterRender: function () {
        this.bindTextarea();
        this.restoreText();
        this.$('.js-comment-textarea').limitInput({
            limit: this.restCount,
            remText: '%n',
            enableNegative: true,
            remTextEl: this.$('.rest-count'),
            buttonToDisable: this.$('.btn.btn-primary.send-comment'),
            toggleCounter: {
                show: "focusin",
                hide: "focusout"
            }
        });
    }
});

Comments.Views.FixedFootView = Comments.Views.FootView.extend({
    className: 'photo-dlg',
    template: '#comment-last-template-fixed',

    sendComment: function () {
        this.sendCommentInternal();
    },
    setTextValue: function (text) {
        var textarea = this.getTextarea();
        if (textarea) {
            textarea.val($.trim(text));
        }
    },
    bindTextarea: function () {
        this.textarea = this.$('textarea');
    }
});

Comments.Views.Item = BaseRichView.extend({
    tagName: 'li',
    className: 'attach-comment-item',
    template: '#comment-template-item',
    attributes: {
        itemprop: "comment",
        itemscope: "itemscope",
        itemtype: "http://schema.org/UserComments"
    },
    mediaTemplates: {
        video: '#comment-video-template',
        audio: '#common-audio-template',
        photo: '#common-photo-template'
    },
    viewEvents: {
        'click .close': 'onDeleteCommentClicked',
        'click .time-delete-comment > a': 'onDeleteCommentClicked',
        'click .restore-comment': 'onRestoreCommentClicked',
        'click .answer-link': 'onReplyClicked'
    },

    afterRender: function () {
        if (this.model.get('newComment')) {
            this.$el.addClass('new-comment');
        } else {
            this.$el.removeClass('new-comment');
        }
        $.updateScrollItems();
        BaseRichView.prototype.afterRender.call(this);
    },
    /**
     * Here is permission logic (cannot be moved to model, uses links with external commentsModel)
     */
    beforeRender: function () {
        var ownerProfile = this.model.get('profile');

        var isObjectOwnerOrAuthor = PrivacyUtils.isObjectOwnerOrAuthor(
                this.model.get('profileId'),
                this.model.get('authorProfileId')
                );
        var canBeDeleted = this.model.get('canBeDeleted');
        var showCloseButton = (isObjectOwnerOrAuthor && canBeDeleted || PrivacyUtils.isGlobalAdmin()) && !this.model.get('inProcess');

        this.model.set('showCloseButton', showCloseButton, {silent: true});
        this.model.set('show30SecCloseButton', !PrivacyUtils.isAdmin() && (isObjectOwnerOrAuthor && canBeDeleted), {silent: true});
        var commentUrl;
        switch (this.model.get('objectType')) {
            case 'CAMPAIGN':
                commentUrl = this.getCommentUrl(this.model.get('objectId'), this.model.get('commentId'));
                break;
        }

        this.model.set('commentUrl', commentUrl, {silent: true});
        this.model.set('parentCommentUrl', this.getCommentUrl(this.model.get('objectId'), this.model.get('parentCommentId')), {silent: true});
        if (this.model.get('parentCommentId') > 0) {
            var self = this;
            var parentComment = this.model.collection.find(function (model) {
                return self.model.get('parentCommentId') == model.get('commentId');
            });
            if (parentComment) {
                self.model.set('parentCommentAuthor', parentComment.get('authorName'), {silent: true});
            } else {
                if (workspace.navigationState && workspace.navigationState.get('anchor') && workspace.navigationState.get('anchor').indexOf('comment') >= 0) {
                    this.model.collection.loadAll({
                        success: function () {
                            setTimeout(function () {
                                workspace.appView.jumpToAnchor();
                            }, 1000);
                        }
                    })
                }
            }
        }
    },

    dispose: function () {
        BaseView.prototype.dispose.call(this);
        $.updateScrollItems();
    },
    getCommentUrl: function (objectId, commentId) {
        return '/campaigns/' + objectId + '/comments!comment' + commentId;
    }
});

Comments.Views.CampaignFootView = Comments.Views.FootView.extend({
    restCount: 5000
});

Comments.Views.CampaignItem = Comments.Views.Item.extend({
    template: '#campaign-comment-item-template',
    events: {
        'click .anchor': 'anchorJump',
        'click .comment-more': 'toggleSpoiler'
    },
    anchorJump: function (event) {
        event.preventDefault();
        event.stopImmediatePropagation();

        var link = $(event.currentTarget);
        var targetComment = $('#' + link.attr('href').split('!')[1]);

        this.trigger('backanchor:remove');

        if (link.hasClass('replyto')) {
            var currentHref = $(this.el).children('.lip').find('.self.anchor').attr('href');
            var targetLink = targetComment.children('.lip').find('.back.anchor');
            targetLink.attr('href', currentHref);
            this.targetLink = targetLink;
            this.trigger('backanchor:add');
        }

        workspace.navigate(link.attr('href'), false);
        workspace.appView.doJump(targetComment);
        targetComment.closest('li').effect("highlight", {color: "#b1b1b1"}, 1000);
    },

    toggleSpoiler: function () {
        var expanded = this.model.get('expanded');
        this.model.set('expanded', !expanded);
    },

    afterShow: function () {
        var contentMore = this.$('.comment-more');
        var $content = this.$('.content');
        if ($content.find('img').length || this.model.get('expanded')) {
            contentMore.removeClass('hidden');
            return;
        }
        var contentHeight = $content.height();
        var contentInnerHeight = this.$('.js-content-h').height();
        if (contentInnerHeight > contentHeight) {
            contentMore.removeClass('hidden');
        }
    },

    afterRender: function () {
        Comments.Views.Item.prototype.afterRender.call(this);
        this.afterShow();
    }
});

Comments.Views.List = BaseListView.extend({
    tagName: 'ul',
    className: 'attach-comment-block',
    sortDirection: 'desc',
    pagerTemplate: '#comment-pager-template',
    pagerLoadingTemplate: '#comment-pager-loading-template',
    modelEvents: _.extend({}, BaseListView.prototype.modelEvents, {
        'model change:commentsCount': 'renderPager'
    }),
    construct: function (options) {
        var self = this;
        self.limit = self._limit = options.limit;
        self.offset = self._offset = options.offset;
        self.remain = self.model.get('commentsCount') - self.offset;
        self.model.set('nextCount', self.remain < self.limit ? self.remain : self.limit, {silent: true});
        self.collection.on('copyFrom', self.modalClosed, self);
    },
    modalClosed: function () {
        this.collection.allLoaded = this.collection.length >= this.model.get('commentsCount');
        this.offset = this._offset;
        this.limit = this._limit;
        if (!this.collection.allLoaded) {
            this.remain = this.model.get('commentsCount') - this.offset;
            this.model.set('nextCount', this.remain < this.limit ? this.remain : this.limit, {silent: true});
        }
    },
    onPagerClicked: function (e) {
        var self = this,
                allCount = self.model.get('commentsCount');
        if (self.offset < allCount) {
            self.remain = allCount - self.offset - self.limit;
            self.model.set('nextCount', self.remain < self.limit ? self.remain : self.limit, {silent: true});
        }
        self.collection.data.offset = self.offset;
        self.collection.data.limit = self.limit;

        self.renderLoaderElement();

        self.loadNext({
            success: function () {
                self.offset += self.limit;
                if (self.offset >= allCount)
                    self.collection.allLoaded = true;
            }
        });
    },

    /** Decides when draw and when do not Pager.
     *  Shows pager if there is nothing left on server. (CommentsCount and comments that are not ordered for deletion.)
     */
    renderPager: function () {
        var commentsOnPage = this.collection.filterByAttr('deleted', false).length;
        if (this.model.get('commentsCount') > commentsOnPage && this.collection.limit > 0) {
            this.renderPagerElement();
        } else {
            this.renderAllLoadedElement();
        }
        $.updateScrollItems();
    }
});


Comments.Views.Comments = BaseView.extend({
    anchor: '.attach-comment',
    events: {
        'onDeleteCommentClicked': 'onDeleteCommentClicked',
        'onRestoreCommentClicked': 'onRestoreCommentClicked',
        'onReplyClicked': 'onReplyClicked'
    },

    commentItemViewType: function () {
        return Comments.Views.Item;
    },

    getFootViewClass: function () {
        return Comments.Views.FootView;
    },

    construct: function (options) {
        var col = new Comments.Views.List({
            collection: this.model.commentsList,
            model: this.model.commentableObject,
            itemViewType: this.commentItemViewType(),
            offset: options.model.get('limit'),
            limit: 20
        });
        this.listView = this.addChild(col);

        this.model.initComments();
        this.model.set('writePermission', true);
        if (!workspace.appModel.isAuthorized() || PrivacyUtils.noComment()) {
            this.footer = this.addChild(new Comments.Views.CommentDenied({
                model: this.model
            }));
        } else {
            var footerClass = this.getFootViewClass();
            this.footer = this.addChild(new footerClass({
                model: this.model,
                className: "attach-comment-textarea"
            }));
        }
    },

    onReplyClicked: function (event, view) {
        event.preventDefault();
        event.stopImmediatePropagation();
        this.footer.model.set('parentCommentId', view.model.get('commentId'));
        var textarea = this.$el.find('.js-comment-textarea')[0];
        if (textarea) {
            textarea.focus();
            $(textarea).val($.trim(view.model.get('authorName')) + ", ");
        }
    },

    onDeleteCommentClicked: function (e, view) {
        view.$('.tooltip').twipsy('hide');   //.close
        if ($.contains(view.$el, workspace.appModel.get('videoManager').currentElement)) {
            workspace.appModel.get('videoManager').pauseOldVideo();
        }
        this.model.deleteComment(view.model);
    },

    onRestoreCommentClicked: function (e, view) {
        this.model.restoreComment(view.model);
    },

    afterRender: function () {
        if (this.model.isCommentsListOpen()) {
            $(this.el).closest('.attach-comment').show();
        } else {
            $(this.el).closest('.attach-comment').hide();
        }

    }
});

Comments.Views.CampaignComments = Comments.Views.Comments.extend({
    commentItemViewType: function () {
        return Comments.Views.CampaignItem;
    },

    modelEvents: {
        'commentsshow': 'commentsShow'
    },

    getFootViewClass: function () {
        return Comments.Views.CampaignFootView;
    },

    commentsShow: function () {
        var self = this;
        _.delay(function () {
            _.each(self.listView.getViews(), function (view) {
                if (view.afterShow && _.isFunction(view.afterShow)) {
                    view.afterShow();
                }
            });

        }, 1000);
    }
});

Comments.Views.CommentsCounter = BaseView.extend({
    template: "#comments-counter-template",

    modelEvents: _.extend({}, BaseView.prototype.modelEvents, {
        'model change:commentsCount': 'render'
    }),

    initialize: function (options) {
        this.sharingData = options.sharingData || {};
        BaseView.prototype.initialize.call(this, options);
    },

    afterRender: function () {
        this.$('.share-cont').show();
        if (!this.sharingData.modal) {
            this.$('a.js-share-link').share(this.sharingData);
        } else {
            var self = this;
            this.$('a.js-share-link').mouseenter(function () {
                $(this).share(self.sharingData);
            });
        }

    }

});

Comments.Views.ModalComments = BaseView.extend({

    template: '#modal-comment-template-fixed',

    events: {
        'click .toggle-comment': 'toggleComment'
    },

    construct: function (options) {
        var commentModel = this.model;
        if (commentModel.get('showCounters')) {
            this.addChildAtElement('.attach-post-option', new Comments.Views.CommentsCounter({
                model: commentModel.commentableObject
            }));
        }
        this.addChildAtElement('.attach-comment-placeholder', new Comments.Views.CampaignComments({
            model: commentModel,
            commentClassName: options.commentClassName
        }));
        this.model = commentModel.commentableObject;
    },

    toggleComment: function (e) {
        e.preventDefault();
        this.$('.attach-comment').toggle();
    }
});

Comments.Views.CommentDenied = BaseView.extend({
    template: "#comment-denied-template",
    className: 'pln-ww',
    events: {
        'click .registration-link': 'showRegistrationDialog',
        'click .signup-link': 'showAuthDialog'
    },

    showAuthDialog: function () {
        LazyHeader.showAuthForm('signup');
    },

    showRegistrationDialog: function () {
        CookieProvider.set('gtmRegistration', 'comments_success_registration', {path: '/', expires: 'Session'});
        LazyHeader.showAuthForm('registration');
    }
});


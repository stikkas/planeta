/*global Dialogs,DialogsUpdater,Async,console*/
/*********************************************************************************************
 *
 * Dialog model (constructed from the big dialog json we get from the server)
 *
 *********************************************************************************************/
Dialogs.Models.Dialog = BaseModel.extend({
    ASYNC_QUEUE_NAME: 'dialog-async-queue',
    MAX_MESSAGE_SIZE: 256,

    url: function () {
        return '//' + workspace.serviceUrls.imServiceUrl + '/dialog.json';
    },

    initialize: function (options) {
        this.set(this.parse(options));
    },

    /**
     * This method is called before dialog box has been rendered.
     * We're checking if we need to load messages or not.
     */
    prepareMessages: function () {
        var dialog = this;
        var startDialogsUpdater = function () {
            DialogsUpdater.bindToUpdater(dialog);
        };
        var messages = this.get('messages');
        if (messages && messages.length === 0) {
            this.loadNextMessages('bottom', startDialogsUpdater, startDialogsUpdater);
        } else {
            startDialogsUpdater();
        }
    },

    _createNewMessage: function (messageText) {
        var myProfile = workspace.appModel.get('myProfile').attributes;
        return new Dialogs.Models.Message({
            deleted: false,
            dialogId: this.dialogId,
            messageId: this.getLastMessageId() + 1000,
            messageText: "",
            messageTextHtml: _.escape(messageText),
            newMessage: false,
            timeAdded: 0,
            userAlias: myProfile.alias,
            userDisplayName: myProfile.userDisplayName,
            userId: myProfile.profileId,
            userImageUrl: myProfile.smallImageUrl
        });

    },

    /**
     * Sends new message to the dialog
     *
     * @param messageText Text to send
     */
    sendMessage: function (messageText, success) {
        if (messageText.bbcode) {
            this._sendMessage(messageText.bbcode, success);
        } else {
            while (messageText.length > 0) {
                if (messageText.length <= this.MAX_MESSAGE_SIZE) {
                    this._sendMessage(messageText, success);
                    return;
                }
                var i = messageText.lastIndexOf('/n', this.MAX_MESSAGE_SIZE - 1);
                if (i < 0) {
                    i = messageText.lastIndexOf(' ', this.MAX_MESSAGE_SIZE - 1);
                }
                if (i < 0) {
                    i = this.MAX_MESSAGE_SIZE - 1;
                }
                this._sendMessage(messageText.substr(0, i + 1));
                messageText = messageText.substr(i + 1);
            }
        }


    },

    _sendMessage: function (messageText, success) {
        var self = this;
        var messages = this.get('messages');
        var newMessage = this._createNewMessage(messageText);
        messages.scrollTo = 'bottom';
        messages.add(newMessage, {at: 0});
        Async.append(function () {

            var dfd = this;
            var error = function () {
                messages.remove(newMessage);
                dfd.resolve();
            };
            var options = {
                url: '//' + workspace.serviceUrls.imServiceUrl + '/dialog-send-message.json',
                dataType: 'jsonp',
                data: {
                    dialogId: self.get('dialogId'),
                    messageText: messageText
                },
                success: function (response) {
                    if (response.success) {
                        var receivedNewMessage = new Dialogs.Models.Message(response.result);
                        var index = messages.indexOf(newMessage);
                        messages.remove(newMessage);
                        messages.add(receivedNewMessage, {at: index});

                        if (success) {
                            success(receivedNewMessage);
                        }
                        dfd.resolve();
                    } else {
                        error();
                        workspace.appView.showErrorMessage(response.errorMessage || "Непредвиденная ошибка при отправке сообщения");
                    }
                },
                error: function (ex) {
                    error();
                    console.log(ex);
                    workspace.appView.showErrorMessage('Непредвиденная ошибка при отправке сообщения');
                    self.destroy();
                }
            };


            $.ajax(options);
        }, this.ASYNC_QUEUE_NAME);
    },

    loadNextMessages: function (scrollTo, successCallback, errorCallback) {
        var messages = this.get('messages');
        messages.scrollTo = scrollTo;
        messages.loadNext({
            success: successCallback,
            error: errorCallback
        });
    },

    deleteMessage: function (model) {
        var options = {
            url: '//' + workspace.serviceUrls.imServiceUrl + '/dialog-deleteByProfileId-message.json',
            data: {
                dialogId: model.get('dialogId'),
                messageId: model.get('messageId')
            },
            context: this,
            success: function (response) {
                if (response.success) {
                    var messages = this.get('messages');
                    var index = messages.indexOf(model);
                    var next = messages.at(index - 1);
                    messages.remove(model);
                    if (next) {
                        next.trigger('change');
                    }
                } else {
                    workspace.appView.showErrorMessage(response.errorMessage);
                }
            }
        };

        Backbone.sync('delete', model, options);
    },

    loadLastMessages: function (options) {
        var self = this, data;
        Backbone.sync('read', this, {
            url: '//' + workspace.serviceUrls.imServiceUrl + '/dialog-last-messages.json',
            data: {
                startMessageId: self.getLastMessageId(),
                dialogId: self.get('dialogId')
            },
            success: function (result) {
                _.each(result, self.newMessage, self);
                if (options.success) {
                    options.success(self);
                }
            },
            error: options.error
        });
    },

    parse: function (response) {
        if (!response || !response.dialog) {
            return {};
        }

        // Setting dialog main properties
        var dialog = {};
        dialog.dialogInfo = response;
        dialog.dialogId = response.dialog.dialogId;
        dialog.id = dialog.dialogId;
        dialog.name = response.dialog.name;
        dialog.messagesCount = response.dialog.messagesCount;
        dialog.timeAdded = response.dialog.timeAdded;
        if (response.lastMessage) {
            dialog.timeUpdated = response.lastMessage.timeAdded;
        }

        dialog.unreadMessagesCount = response.lastMessage ? response.unreadMessagesCount : 0;

        // Setting last message
        if (response.lastMessage) {
            dialog.lastMessage = new Dialogs.Models.Message(response.lastMessage);
        }

        var companion;
        if (response.dialogUsers) {
            companion = response.dialogUsers[0];
            if (response.dialogUsers[0].profileId == workspace.appModel.myProfileId()) {
                companion = response.dialogUsers[1];
            }
            if (companion) {
                dialog.imageUrl = companion.smallImageUrl;
                dialog.companionId = companion.profileId;
                dialog.companionAlias = companion.alias;
                dialog.companionName = companion.displayName;
                dialog.companionGender = companion.userGender;
            }

        } else {
            dialog.imageUrl = response.companionImageUrl;
            dialog.companionId = response.companionId;
            dialog.companionAlias = response.companionAlias;
            dialog.companionName = response.companionName;
            dialog.companionGender = response.userGender;
        }

        if (!dialog.name) {
            dialog.name = dialog.companionName;
        }

        // Setting messages collection
        dialog.messages = new Dialogs.Models.Messages([], {
            dialogId: dialog.dialogId
        });


        return dialog;
    },

    buildDialogName: function (dialogUsers) {

        if (dialogUsers.length === 1) {
            return dialogUsers.at(0).get('displayName');
        }

        var dialogName = '';
        dialogUsers.each(function (user, index) {
            if (index > 0) {
                dialogName += ' ';
            }
            dialogName += user.get('displayName');
        });
        return dialogName;
    },

    getLastMessageId: function () {
        return this.get("messages").getLastMessageId();
    },

    newMessage: function (messageObject) {
        this.get('messages').onNewMessage(messageObject);
    },

    getUnreadMessagesCount: function () {
        return this.get('messages').unreadMessagesCount;
    }
});

Dialogs.Models.DialogsCollection = BaseCollection.extend({
    url: function () {
        return '//' + workspace.serviceUrls.imServiceUrl + '/dialogs.json';
    },
    model: Dialogs.Models.Dialog
});





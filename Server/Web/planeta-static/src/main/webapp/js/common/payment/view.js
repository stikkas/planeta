/*globals Modal, UserPayment,$,BaseView,_,BaseListView,workspace*/

UserPayment.Views = {
    PAYMENT_LIMIT: 1000000
};

UserPayment.Views.SelectableItem = BaseView.extend({
    viewEvents: {
        'click': 'onItemSelected'
    },

    afterRender: function () {
        this.$el.toggleClass('active', !!this.model.get('selected'));
    }
});

UserPayment.Views.SelectableListView = BaseListView.extend({
    construct: function () {
        this.bind('onItemSelected', this.onItemSelected, this);
    },

    onItemSelected: function (view) {
        this.collection.select(view.model);
    }
});

UserPayment.Views.PaymentMethodItem = UserPayment.Views.SelectableItem.extend({
    template: '#new-payment-system-template',
    className: 'ppt-item'
});

UserPayment.Views.PaymentMethodListView = UserPayment.Views.SelectableListView.extend({
    className: 'pln-payment-type',
    itemViewType: UserPayment.Views.PaymentMethodItem,
    onCollectionLengthChanged: function () {
        this.trigger('onCollectionLengthChanged');
    }
});


UserPayment.Views.AmountEditor = BaseView.extend({
    template: '#payment-amount-editor-template',
    className: 'pln-payment_enter-sum',
    events: {
        'keyup .js-amount': 'onAmountChanged',
        'change .js-amount': 'onAmountChanged'
    },

    onAmountChanged: _.debounce(function (e) {
        e.stopPropagation();
        var el = $(e.currentTarget);
        var amountAsString = el.val();
        var parsed = parseInt(amountAsString, 10);
        var amount = _.isNaN(parsed) ? 1 : Math.max(1, Math.abs(parsed));

        var paymentLimit = UserPayment.Views.PAYMENT_LIMIT;
        if (amount > paymentLimit) {
            workspace.appView.showErrorMessage('Сумма платежа не должна превышать ' + paymentLimit + ' руб.');
            amount = paymentLimit;
        }

        this.model.setAmount(amount);
        if (!_.isEqual(amountAsString, amount.toString())) {
            el.val(amount);
        }
    }, 750)
});

UserPayment.Views.PaymentBlock = BaseView.extend({
    template: '#payment-block-template',
    className: 'pln-payment-box_field-group',
    formattedPhone: '',
    events: {
        'change .js-payment-phone': 'onPhoneChanged',
        'keyup .js-payment-phone': 'onPhoneChanged'
    },

    construct: function () {
        this.addChildAtElement('.js-payment-methods', new UserPayment.Views.PaymentMethodListView({
            collection: this.model.get('methods')
        }));

        this.paymentMethodListView = new UserPayment.Views.PaymentMethodListView({
            collection: this.model.get('childrenMethods')
        });
        this.addChildAtElement('.js-child-payment-methods', this.paymentMethodListView);
        this.paymentMethodListView.bind('onCollectionLengthChanged', this.slideChildPaymentMethods, this);

        this.model.on('paymentChanged', this.onPaymentChanged, this);

        this.checkTls();
    },
    toggleMethodsView:function(bool){
       this.$(".js-payment-methods").toggle(bool);
    },

    onPhoneChanged: _.debounce(function (e) {
        e.stopPropagation();
        var el = this.$('.js-payment-phone');
        var formattedPhone = el.val();
        var processed = formattedPhone.replace(/\D/g, '');
        this.formattedPhone = formattedPhone;
        this.model.setPhone(processed);
    }, 750),

    slidePhone: function () {
        var $phoneContainer = this.$('.js-phone-container');
        if (this.model.get('needPhone') ^ $phoneContainer.is(':visible')) {
            if (this.model.get('needPhone')) {
                $phoneContainer.find('.js-payment-phone').mask('+7 (999) 999-99-99').val(this.formattedPhone);
                $phoneContainer.show();
            } else {
                $phoneContainer.hide();
            }
        }
        this.slideSubMenu();
    },
    onChildPaymentSystemClicked: function (view) {
        this.model.get('childrenSystems').select(view.model);
    },

    slideChildPaymentMethods: function () {
        this.$('.js-child-payment-methods').toggle(!!this.model.get('childrenMethods').length);
        this.slideSubMenu();
    },

    slideSubMenu: function () {
        this.$('.js-sub-menu').toggle(this.isVisibleSubMenu());
        this.$('.js-payment-sub-menu').toggle(this.isVisiblePaymentSubMenu());
    },

    checkTls: function () {
        if (!this.model.has('tlsSupported')) {
            TlsUtils.checkClientTlsSupport(this.model);
        }
    },

    onPaymentChanged: _.throttle(function () {
        this.slidePhone();
    }, 500, {leading: false}),

    isVisibleSubMenu: function () {
        return this.isVisiblePaymentSubMenu();
    },

    isVisiblePaymentSubMenu: function () {
        return !!this.model.get('childrenMethods').length || this.model.get('needPhone');
    },

    afterRender: function () {
        this.slideSubMenu();
    }

});

UserPayment.Views.Payment = BaseView.extend({
    template: '#payment-template',
    events: {
        'click .js-pay:not(.disabled)': 'onPayClicked'
    },

    construct: function (options) {
        this.customParams = options.customParams || [];
        this.addChildAtElement('.js-payment', new UserPayment.Views.PaymentBlock({
            model: this.model
        }));
        this.model.on('readinessChanged', this.onReadinessChanged, this);
    },

    onPayClicked: function (e) {
        //e.stopPropagation();
        if (!this.model.get('ready')) {
            return;
        }

        this.onReadinessChanged(false);
        var form = this.model.createPaymentForm(this.customParams);
        if (window.Vkapp) {
            form.attr('target', '_blank');
        }
        form.submit();
    },

    onReadinessChanged: function (isReady) {
        this.$el.find('.js-pay').toggleClass('disabled', !isReady);
    }
});


UserPayment.Views.OrderPayment = UserPayment.Views.Payment.extend({
    template: '#order-payment-block-template',
    formattedPhone: '',
    events: {
        'click .js-planeta-purchase': 'onPlanetaPurchaseClicked',
        'click .purchase': 'onPayClicked',
        'click .js-payment-group-item': 'onPaymentGroupChange'
    },
    viewEvents: {
        'afterPaymentGroupChange afterPaymentGroupChange': 'afterPaymentGroupChange'
    },
    modelEvents: _.extend({}, BaseView.prototype.modelEvents, {
        'error:phone-required': 'remindFulfillPhone'
    }),

    construct: function (options) {
        this.customParams = options.customParams || [];

        this.payment = this.addChildAtElement('.js-payment', new UserPayment.Views.PaymentBlock({
            model: this.model
        }));
    },

    onPlanetaPurchaseClicked: function (e) {
        var isPlanetaPurchase = this.model.togglePlanetaPurchase();
    },

    isCashEnabled: function () {
        return false;
    },

    beforeRender: function () {
        var attrs = {
            cashEnabled: this.isCashEnabled()
        };
        if (!attrs.cashEnabled) {
            attrs.paymentGroup = 'PAYMENT_SYSTEM';
            attrs.paymentType = 'PAYMENT_SYSTEM';
            if (this.model.get('planetaPurchase') && this.model.get('fromBalance')) {
                try {
                    this.model.get('methods').clearSelection();
                } catch (e) {
                    console.log(e);
                }
            }
        }
        this.model.silentSet(attrs);
    },

    onPaymentGroupChange: function (e) {
        var $el = $(e.currentTarget);
        $el.parent().find('.radiobox').removeClass('checked');
        $el.find('.radiobox').addClass('checked');
        var paymentType = $el.data('type');

        this.model.set({
            paymentType: paymentType,
            paymentGroup: paymentType
        });
        this.trigger("afterPaymentGroupChange", this.model.get("paymentType"));
        //this.payment.toggleMethodsView(paymentType != 'CASH');
    },

    remindFulfillPhone: function () {
        this.$('.buy-payment-type-phone').addClass('error');
    }
});

UserPayment.Views.EditablePaymentBlock = UserPayment.Views.PaymentBlock.extend({

    construct: function () {
        UserPayment.Views.PaymentBlock.prototype.construct.call(this, arguments);
        this.addChildAtElement('.js-sub-menu', new UserPayment.Views.AmountEditor({
            model: this.model
        }));
    },
    isVisibleSubMenu: function () {
        return true;
    }

});

UserPayment.Views.BalancePaymentDialog = Modal.View.extend({
    template: '#balance-payment-dialog-template',
    className: 'modal modal-balance',
    urlAnchor: 'increase_balance',
    events: {
        'click a.close': 'cancel',
        'click button[type=reset]': 'cancel',
        'click .js-pay:not(.disabled)': 'onPayClicked'
    },

    construct: function () {
        this.addChildAtElement('.js-payment', new UserPayment.Views.EditablePaymentBlock({
            model: this.model
        }));
        this.model.on('readinessChanged', this.onReadinessChanged, this);
    },

    onPayClicked: function () {
        UserPayment.Views.Payment.prototype.onPayClicked.call(this, arguments);
    },

    onReadinessChanged: function () {
        UserPayment.Views.Payment.prototype.onReadinessChanged.call(this, arguments);
    }
});

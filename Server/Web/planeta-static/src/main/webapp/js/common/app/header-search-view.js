/*globals $,workspace,Header,BaseView,_,Audios,Search,Blog,BlogUtils,Campaign,BaseListView,console,HeaderSearch*/

HeaderSearch.Views.Header = BaseView.extend({
    modelEvents: {
        change: 'afterRender',
        destroy: 'dispose',
        'onFetchSuccessDone': 'onFetchSuccessDone'
    },

    events: {
        'keyup .js-h-search_input': 'inputKeyPress',
        'click .js-header-search-button-close': 'closeSearch',
        'click .js-header-search-button-open': 'openSearch'
    },

    construct: function () {
        this.headerResultModel = new BaseModel({
            products: this.model.resultModels.products,
            projects: this.model.resultModels.projects,
            shares: this.model.resultModels.shares
        });

        this.addChildAtElement(".js-header-result-block", new HeaderSearch.Views.HeaderResultBlock({
            model: this.headerResultModel
        }));

        HeaderSearch.searchView = this;

        this.callbacks = {
            close: [],
            open: []
        };
    },

    closeSearch: function () {
        $('.header').removeClass('header__search header__search-results');
        this.$el.find('.js-h-search_input').val('');
    },

    openSearch: function () {
        $('.header').addClass('header__search');

        //TODO: Это хуй пойми что, без таймаута не работает фокус. Решение взято со StackOverflow. Придумать, как   сделать по Фен Шую
        setTimeout(function () {
            $('.js-h-search_input').focus();
        }, 200);
    },

    inputKeyPress: function () {
        this.search();
    },

    searchDirect: function (forceSearch) {
        $('.header').addClass('header__search-results');

        var query = this.$el.find('.js-h-search_input').val();
        this.model.smartSearch(query, forceSearch);
    },

    onFetchSuccessDone: function () {
        $('.h-search-results_loader').removeClass('load');
        this.headerResultModel.change();
    },

    search: _.debounce(function () {
        this.searchDirect();
    }, HeaderSearch.HEADER_SEARCH_QUERY_CHANGE_DELAY)
});


HeaderSearch.Views.HeaderResultBlock = BaseView.extend({
    template: '#search-results-block-template',
    className: 'h-search-results',
    firstRender: true,

    events: {
        'changeResults': 'render',
        'destroy': 'dispose',
        'click .js-results-all': 'resultsAll'
    },

    construct: function (options) {
        if (workspace) {
            if (workspace.currentLanguage) {
                this.model.set('lang', workspace.currentLanguage);
            } else {
                this.model.set('lang', "ru");
            }
        } else {
            this.model.set('lang', "ru");
        }

        this.addChildAtElement('.js-search-results', new HeaderSearch.Views.ResultListView( {
            collection: this.model.get('projects').searchResults,
            model: this.model
        }));
    },

    resultsAll: function (e) {
        $('.header').removeClass('header__search header__search-results');
    }
});


HeaderSearch.Views.ResultItem = BaseView.extend({
    template: '#search-results-list-item-template',
    className: 'h-search-results_i',

    events: {
        'click .h-search-results_link': 'clickItem'
    },

    clickItem: function () {
        $('.header').removeClass('header__search header__search-results');
        window.location.href = this.$el.find('.h-search-results_link').attr('data-href');
    }
});


HeaderSearch.Views.ResultListView = DefaultListView.extend({
    itemViewType: HeaderSearch.Views.ResultItem,
    emptyListTemplate: '#search-results-empty-template',
    pagerTemplate: '#search-results-pager-template'
});
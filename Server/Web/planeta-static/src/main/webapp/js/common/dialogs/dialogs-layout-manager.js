/*global Dialogs,StorageUtils*/
Dialogs.Models.LayoutManager = Backbone.Model.extend({
    DIALOGS_SEARCH_ID: -1,
    views: {},
    defaultZindex: 0,
    defaultDialogHeight: 0,
    SPACE_BETWEEN_VIEWS: 10,
    SPACE_BETWEEN_LINES: 30,
    movingView: null,

    initialize: function (options) {
        _.bindAll(this);
        $(window).bind('resize', this._onResizeWindow);
        this.appModel = options.appModel;
    },

    _onResizeWindow: _.debounce(function () {

        var windowWidth = $(window).width();
        var windowHeight = $(window).height();

        _.each(this.views, function (view) {
            var viewOffset = view.$el.offset();
            var viewHeight = view.$el.outerHeight(true);
            var viewWidth = view.$el.outerWidth(true);
            var isChanged = false;

            var isOutLeft = viewOffset.left < 0;
            var isOutRight = viewOffset.left + viewWidth > windowWidth;
            var isOutTop = viewOffset.top < 0;
            var isOutBottom = viewOffset.top + viewHeight > windowHeight;

            var position = this._getViewPosition(view);
            var isStickedAtLeft = position.left === 0;
            var isStickedAtRight = position.right === 0;
            var isStickedAtTop = position.top === 0;
            var isStickedAtBottom = position.bottom === 0;

            if (!isStickedAtLeft && !isStickedAtRight) {
                if (isOutRight) {
                    position.left = '';
                    position.right = 0;
                    isChanged = true;
                } else if (isOutLeft) {
                    position.left = 0;
                    position.right = '';
                    isChanged = true;
                }
            }

            if (!isStickedAtTop && !isStickedAtBottom) {
                if (isOutBottom) {
                    position.top = '';
                    position.bottom = 0;
                    isChanged = true;
                } else if (isOutTop) {
                    position.top = 0;
                    position.bottom = '';
                    isChanged = true;
                }
            }

            if (isChanged) {
                view.$el.css(position);
                this._storePosition(view.id, position);
            }
        }, this);
    }, 200),

    onPositionChanged: function (view, newPosition, moveFrom) {
        view.isnotMovedByUser = false;

        this._storePosition(view.id, newPosition);
        if (this._doesViewCrossFirstLineOfTheDialogs(view, moveFrom.top)) {
            this._moveNearest(moveFrom.left);
        }
    },

    moveOnTop: function (topView) {
        var self = this;
        var zindices = [];
        var dialogsPositionInfo = this._getDialogsPositionInfo();
        var isChanged = false;

        if (_.size(this.views) > 1) {
            _.each(this.views, function (view, id) {
                var zindex = parseInt(view.$el.css("z-index"), 10);
                if (zindex != self.defaultZindex && id != topView.id) {
                    zindices.push({
                        id: id,
                        zindex: zindex
                    });
                }
            });

            zindices.sort(function (item1, item2) {
                return item1.zindex - item2.zindex;
            });

            var ids = _.pluck(zindices, "id") || [];
            ids.push(topView.id);
            _.each(ids, function (id, index) {
                var newZindex = self.defaultZindex + index + 1;
                var oldZindex = self.views[id].$el.css("z-index");
                if (newZindex != oldZindex) {
                    self.views[id].$el.css("z-index", newZindex);
                    if (dialogsPositionInfo[id]) {
                        dialogsPositionInfo[id].position['z-index'] = newZindex;
                        isChanged = true;
                    }
                }
            });
        }

        if (isChanged) {
            this._setDialogsPositionInfo(dialogsPositionInfo);
        }
    },

    _getViewPosition: function (view) {
        var props = ['left', 'right', 'top', 'bottom'];
        var pos = {};
        _.each(props, function (prop) {
            var value = view.$el.css(prop);
            pos[prop] = value === 'auto' ? "" : parseInt(value, 10);
        });
        return pos;
    },

    _storePosition: function (id, position) {
        var view = this.views[id];
        if (!position) {
            position = this._getViewPosition(view);
        }

        var zindex = view.$el.css("z-index");
        if (zindex) {
            position["z-index"] = zindex;
        }

        var dialogsPositionInfo = this._getDialogsPositionInfo();
        dialogsPositionInfo[id] = {
            isnotMovedByUser: view.isnotMovedByUser,
            position: position
        };
        this._setDialogsPositionInfo(dialogsPositionInfo);
    },

    _removePosition: function (id) {
        var dialogsPositionInfo = this._getDialogsPositionInfo();
        delete dialogsPositionInfo[id];
        this._setDialogsPositionInfo(dialogsPositionInfo);
    },

    onViewOpened: function () {

    },

    onViewClosed: function (view) {
        delete this.views[view.id];
        this._removePosition(view.id);

        if (this._doesViewCrossFirstLineOfTheDialogs(view)) {
            var viewLeft = view.$el.offset().left;
            /*move view out of the window*/
            view.$el.css({
                left: -10000,
                right: ""
            });
            this._moveNearest(viewLeft);
        }

        if (!_.size(this.views)) {
            this._removeDialogsPositionInfo();
        }

    },

    _getDialogsPositionInfo: function () {
        return StorageUtils.get("dialogs.positionInfo." + this._getProfileId()) || {};
    },

    _setDialogsPositionInfo: function (dialogsPositionInfo) {
        if (dialogsPositionInfo && this._getProfileId()) {
            StorageUtils.set("dialogs.positionInfo." + this._getProfileId(), dialogsPositionInfo);
        }
    },

    _removeDialogsPositionInfo: function () {
        if (this._getProfileId()) {
            StorageUtils.remove("dialogs.positionInfo." + this._getProfileId());
        }
    },

    restoreDialogs: function (createDialogFunction) {
        var posInfo = this._getDialogsPositionInfo();
        var ids = _.keys(posInfo);
        ids.sort(function (id1, id2) {
            if (id1 === this.DIALOGS_SEARCH_ID) {
                return 1;
            }
            if (id2 === this.DIALOGS_SEARCH_ID) {
                return -1;
            }
            var zindex1 = posInfo[id1].position['z-index'];
            var zindex2 = posInfo[id2].position['z-index'];
            return zindex1 == zindex2 ? 0 : zindex1 > zindex2 ? 1 : -1;

        }, this);
        _.each(ids, function (id) {
            createDialogFunction(id);
        });
    },

    setPosition: function (view) {
        this.views[view.id] = view;
        var zindex = view.$el.css("z-index");
        if (!this.defaultZindex) {
            this.defaultZindex = parseInt(zindex, 10);
        }
        if (!this.defaultDialogHeight && view.id != this.DIALOGS_SEARCH_ID) {
            this.defaultDialogHeight = view.$el.outerHeight(true);
        }

        var posInfo = (this._getDialogsPositionInfo())[view.id];
        if (posInfo && !this._isViewOutOfWindow(view, posInfo.position)) {
            view.$el.css(posInfo.position);
            view.isnotMovedByUser = posInfo.isnotMovedByUser;
        } else {
            this._setNewPosition(view);
        }
    },

    _setNewPosition: function (view) {
        view.isnotMovedByUser = true;
        this.moveOnTop(view);
        if (view.id == this.DIALOGS_SEARCH_ID) {
            this._setNewDialogsSearchPosition(view);
        } else {
            this._setNewDialogPosition(view);
        }
    },

    _isViewOutOfWindow: function (view, pos) {

        var width = view.$el.outerWidth(true);
        var height = view.$el.outerHeight(true);
        return (pos.left && (pos.left < 0 || pos.left + width > $(window).width()))
            || (pos.right && (pos.right < 0 || pos.right + width < 0))
            || (pos.top && (pos.top < 0 || pos.top + height > $(window).height()))
            || (pos.bottom && (pos.bottom < 0 || pos.bottom + height < 0));

    },

    _findNearestViewToMove: function (left) {

        var foundView = null;
        var foundViewLeft;
        _.each(this.views, function (view) {
            if (view.$el.css("bottom") !== "0px" || view.id === this.DIALOGS_SEARCH_ID || !view.isnotMovedByUser) {
                return;
            }

            var viewLeft = view.$el.offset().left;
            if (viewLeft < left) {
                if (!foundView || viewLeft > foundViewLeft) {
                    foundView = view;
                    foundViewLeft = viewLeft;
                }
            }
        }, this);

        return foundView;
    },

    _doesViewCrossFirstLineOfTheDialogs: function (view, viewTop) {
        var viewHeight = view.$el.outerHeight(true);
        viewTop = viewTop || view.$el.offset().top + $('html').offset().top;

        var viewBottom = viewTop + viewHeight;
        var windowHeight = $(window).height();

        return (viewTop <= windowHeight && viewTop >= windowHeight - this.defaultDialogHeight) || (viewBottom <= windowHeight && viewBottom >= windowHeight - this.defaultDialogHeight);
    },

    _findWhereToMove: function (viewToMove) {
        var viewToMoveLeft = viewToMove.$el.offset().left;
        var foundLeft = $(window).width() + this.SPACE_BETWEEN_VIEWS;
        _.each(this.views, function (view) {

            var viewLeft = view.$el.offset().left;
            if (viewLeft <= viewToMoveLeft || !this._doesViewCrossFirstLineOfTheDialogs(view)) {
                return;
            }

            if (foundLeft === 0 || viewLeft < foundLeft) {
                foundLeft = viewLeft;
            }
        }, this);

        return foundLeft;
    },

    _moveNearest: function (left) {
        if (this.movingView) {
            this.movingView.$el.stop();
            this.movingView = null;
        }

        var nearestView = this._findNearestViewToMove(left);
        if (!nearestView) {
            return;
        }

        var windowWidth = $(window).width();
        var nearestViewWidth = nearestView.$el.outerWidth(true);
        var nearestViewLeft = nearestView.$el.offset().left;

        var freeSpaceLeft = this._findWhereToMove(nearestView) - this.SPACE_BETWEEN_VIEWS;
        var moveToRight = windowWidth - freeSpaceLeft;

        if (moveToRight >= 0 && moveToRight + nearestViewWidth < windowWidth && nearestViewLeft < freeSpaceLeft - nearestViewWidth) {
            var self = this;
            this.movingView = nearestView;
            nearestView.$el.animate({
                right: moveToRight
            }, 500, function () {
                self.movingView = null;
                self._storePosition(nearestView.id);
                self._moveNearest(nearestView.$el.offset().left);
            });
        }
    },

    _findSpaceAtTheBottom: function (placedView, lineNum) {
        var i;
        var necessaryWidth = lineNum === 0 ? placedView.$el.outerWidth(true) : this.SPACE_BETWEEN_LINES;
        var necessaryHeight = placedView.$el.outerHeight(true);
        var windowWidth = $(window).width();
        var windowHeight = $(window).height();
        var htmlPos = $('html').offset();
        var lineBottom = lineNum * this.SPACE_BETWEEN_LINES;

        var arr = [];

        _.each(this.views, function (view) {
            if (view.id == placedView.id) {
                return;
            }

            var top = view.$el.offset().top + htmlPos.top;
            var bottom = windowHeight - top - view.$el.outerHeight(true);
            var left = view.$el.offset().left + htmlPos.left;

            if (lineNum === 0) {
                if (!this._doesViewCrossFirstLineOfTheDialogs(view)) {
                    return;
                }
                arr.push({
                    left: left - this.SPACE_BETWEEN_VIEWS,
                    right: left + view.$el.outerWidth(true) + this.SPACE_BETWEEN_VIEWS
                });

            } else {
                if (view.id == this.DIALOGS_SEARCH_ID || bottom === lineBottom) {
                    arr.push({
                        left: left - this.SPACE_BETWEEN_VIEWS,
                        right: left + view.$el.outerWidth(true) + this.SPACE_BETWEEN_VIEWS
                    });
                } else {
                    var right = left + view.$el.outerWidth(true);
                    arr.push({
                        left: right - this.SPACE_BETWEEN_LINES,
                        right: right
                    });
                }

            }
        }, this);
        arr.push({
            left: 0,
            right: 0
        });
        arr.push({
            left: windowWidth,
            right: windowWidth
        });


        var union = [];
        while (arr.length > 0) {
            var item = arr.pop();
            var left = item.left - necessaryWidth;
            var right = item.right + necessaryWidth;
            for (i = arr.length - 1; i >= 0; i--) {
                var curitem = arr[i];

                if ((curitem.left >= left && curitem.left <= right) || (curitem.right >= left && curitem.right <= right)) {
                    curitem.left = Math.min(curitem.left, item.left);
                    curitem.right = Math.max(curitem.right, item.right);
                    item = null;
                    break;
                }
            }
            if (item) {
                union.push(item);
            }
        }

        if (union.length === 0) {
            return 0;
        }

        union.sort(function (item1, item2) {
            return item2.left - item1.left;
        });
        return windowWidth - union[0].left;

    },

    _setNewDialogPosition: function (view) {
        var viewWidth = view.$el.outerWidth(true);
        var viewHeight = view.$el.outerHeight(true);
        var windowWidth = $(window).width();
        var windowHeight = $(window).height();

        var bottom = 0;
        var right = 0;
        var line = 0;
        var isnotFound = true;

        while (bottom < windowHeight - viewHeight) {
            right = this._findSpaceAtTheBottom(view, line);
            if (right >= 0 && right + viewWidth <= windowWidth) {
                isnotFound = false;
                break;
            }
            line++;
            bottom = line * this.SPACE_BETWEEN_LINES;
        }
        if (isnotFound) {
            right = 0;
            bottom = 0;
        }


        var pos = {
            bottom: bottom,
            right: right,
            top: "",
            left: ""
        };
        this._storePosition(view.id, pos);
        view.$el.css(pos);
    },

    _setNewDialogsSearchPosition: function (view) {
        var pos = {
            right: 0,
            bottom: "",
            left: "",
            top: 0
        };
        this._storePosition(this.DIALOGS_SEARCH_ID, pos);
        view.$el.css(pos);

    },

    _getProfileId: function () {
        return this.appModel.get('myProfile').get('profileId');
    }
});

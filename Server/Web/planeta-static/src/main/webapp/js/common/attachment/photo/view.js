/*global ImageUtils, Attach, Modal, ImageType*/
/**
 *  Views for attaching photos
 **/

Attach.Views.PhotoLink = Modal.OverlappedView.extend({
    template: '#photo-attachment-link-dialog-template',
    lastChange: 0,
    validImageUrl: null,
    events: {
        'click a.close': 'cancel',
        'click button[type=reset]': 'cancel',
        'click button[type=submit]': 'save',
        'input #img-url': 'onUrlChanged',
        'focus #img-url': 'onUrlChanged',
        'paste #img-url': 'onUrlChanged',
        'keyup #img-url': 'onUrlChanged',
        'keypress #img-url': 'onKeyPress'
    },
    construct: function (options) {
        this.img = new Image();
    },
    onKeyPress: function (event) {
        if (event.keyCode == 10 || event.keyCode == 13) {
            if (!$('button[type=submit]', this.el).attr('disabled')) {
                this.save(event);
            }
            return false;
        }
    },
    onUrlChanged: _.debounce(function (event) {
        if (this._isImageLoading) return;
        var url = event.currentTarget.value;
        var self = this;
        var loaded = false;
        this.img.onload = function () {
            loaded = true;
            self._isImageLoading = false;
            self.validImageUrl = self.img.src;
            $('.upload-pic-img img', self.el).attr('src', workspace.staticNodesService.getResourceUrl("images/defaults/upload-pic.png"));
            $('.upload-pic-option .photo-name', self.el).html('');
            self.model.addImageToAlbum(function (photo) {
                $('button[type=submit]', self.el).attr('disabled', null);
                $('.upload-pic-img img', self.el).attr('src', ImageUtils.getThumbnailUrl(self.img.src, ImageUtils.PHOTO_PREVIEW, ImageType.PHOTO));
                self.model.set({
                    photo: new BaseModel({
                        imageUrl: photo.url,
                        objectId: photo.objectId,
                        ownerId: photo.ownerId,
                        naturalWidth: self.img.naturalWidth,
                        external: false
                    })
                }, {
                    silent: true
                });
                $('.upload-pic-option .photo-name', self.el).html('Изображение загружено.');
            }, self.validImageUrl);

        };
        this.img.onerror = function () {
            self._isImageLoading = false;
            if (!loaded) {
                $('.upload-pic-img img', self.el).attr('src', workspace.staticNodesService.getResourceUrl("/images/defaults/upload-pic.png"));
                $('.upload-pic-option .photo-name', self.el).html('Не удалось загрузить изображение.');
            }
        };
        var value = url;
        if (value != this.lastValue) {
            if (value && value.length > 8) {
                this.lastValue = value;
                $('button[type=submit]', self.el).attr('disabled', 'disabled');
                $('.upload-pic-option .photo-name', self.el).html(
                    //'<img src="' + workspace.staticNodesService.getResourceUrl('/images/ajax-loader.gif') + '"/>'
                    '<i class="icon-load"></i>'
                );
                self._isImageLoading = true;
                self.img.src = value;
            }
        }
    }, 750),
    save: function (event) {
        event.preventDefault();
        this.model.selectPhoto(this.model.get('photo'));
    },

    afterRender: function () {
        Modal.OverlappedView.prototype.afterRender.call(this);
        $('#img-url').focus();
    }
});

Attach.Views.PhotoSource = Modal.OverlappedView.extend({
    template: '#photo-attachment-multi-dialog-template',

    showPhotoLink: function () {
        this.cancel();
        Attach.showPhotoLink(0, this.model.get('selected'), this.options.underlyingModal);
    },

    showPhotoUpload: function () {
        this.cancel();
        Attach.showPhotoUpload(workspace.appModel.myProfileId(), this.model.get('selected'), this.model.get('shortcut'), this.options.underlyingModal);
    }
});

Attach.Views.SinglePhotoSource = Attach.Views.PhotoSource.extend({
    showPhotoUpload: function () {
        Attach.showPhotoUpload(this.model.get('profileId'), this.model.get('selected'), this.model.get('shortcut'), this.options.underlyingModal, 1);
        this.disposeSilent();
    }
});


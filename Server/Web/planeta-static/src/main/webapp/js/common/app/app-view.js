/*globals PrivacyUtils,HelpBlocks,LazyHeader,SidebarBannerRotatorView,counters,JobManager,Banner,BaseAppView,Welcome, CustomMetaTagsUtils,ImageUtils,ImageType,Modal,TemplateManager,moduleLoader*/
var AppView = BaseAppView.extend({
    views: {},

    onRouteChanged: function (profile, navItem, model) {

        $('html, body').scrollTop(0);

        this.changeBackground(profile);

        // Replacing views
        this.replaceViews(navItem, model);

    },

    replaceLayout: function (layoutId) {
        var dfd = $.Deferred();
        var DATA_LAYOUT = 'data-layout';
        var useDataView = false;
        layoutId = layoutId || 'default-layout';
        var selector = '[' + DATA_LAYOUT + '=' + layoutId + ']';

        if (layoutId === 'default-layout') {
            console.warn("Don't use default layout");
        }
        // if layout already in DOM do nothing, but otherwise
        if ($(selector).length) {
            dfd.resolve();
        } else {
            var layoutSelector = '#' + layoutId;

            function _replaceLayout() {
                var $layout = $(layoutSelector);
                if ($layout.length === 0) {
                    return false;
                }
                useDataView = $layout.data("useDataView");
                var $layoutHtml = $($layout.html());
                $layoutHtml.attr(DATA_LAYOUT, layoutId);
                $('#root-container').empty().append($layoutHtml);
                return true;
            }

            if (_replaceLayout()) {
                dfd.resolve(useDataView);
            } else {
                TemplateManager.fetchTemplate(layoutSelector).done(function (responce) {
                    _replaceLayout();
                    dfd.resolve();
                });
            }
        }

        return dfd.promise();
    },

    prefetchModelAndInsertView: function (viewName, view, model, controller, contentModel, anchor) {
        var self = this;
        model.prefetch({
            success: function () {
                self.replaceView(viewName, view, model, controller, contentModel, anchor);
            }
        });
    },

    replaceViews: function (navItem, model) {
        function fail(ex) {
            workspace.appView.showErrorMessage('Непредвиденная ошибка отрисовки');
            console.log('Ошибка при замене контента.', ex || '');
        }

        try {//removeTryCatch
            var self = this;
            this.dfdCalcLayout = $.Deferred();
            var anchor = workspace.navigationState.get('anchor');
            if (anchor) {
                workspace.navigationState.set('anchor', anchor);
            }

            this.replaceLayout(navItem.layout).fail(fail).done(function (useDataView) {

                _.forEach(self.views, function (view) {
                    if (view) {
                        if (view.dispose) {
                            view.dispose();
                        } else if (view.$el) {
                            view.$el.remove();
                        }
                    }
                });
                self.views = {};
                if ($('#' + navItem.layout).data("useDataView")) {
                    var $rootContainer = $('#root-container');
                    _.each(navItem, function (obj, nameView) {
                        if (nameView.indexOf('View') !== nameView.length - 4 || !obj || nameView === 'urlAnchorView') {
                            return;
                        }
                        if (_.isString(obj)) {
                            var resolvedObj = moduleLoader.findClass(obj);
                            if (window.debugMode) {
                                if (!resolvedObj || !resolvedObj.prototype instanceof BaseView) {
                                    workspace.appView.showErrorMessage('Не найдено view "' + nameView + '":  "' + obj + '"');
                                    console.error('Не найдено view "' + obj + '"', nameView, navItem);
                                }
                            }
                            obj = resolvedObj;
                        }
                        if (obj && obj.prototype instanceof BaseView) {
                            var $container = $rootContainer.find('[data-view=' + nameView + ']');
                            var name = nameView.substring(0, nameView.length - 4);
                            var modelName = name + 'Model';
                            if (navItem[modelName]) {
                                var NewModelClass = navItem[modelName];
                                if (_.isString(NewModelClass)) {
                                    NewModelClass = moduleLoader.findClass(NewModelClass);
                                }
                                var newModel = new NewModelClass();
                                newModel.contentModel = model;
                                self.prefetchModelAndInsertView(nameView, obj, newModel, newModel, model, $container);
                            } else {
                                self.replaceView(nameView, obj, model, model, model, $container);
                            }
                        }
                    });
                } else {
                    self.replaceView('sidebarView', navItem.sidebarViewType, model, model, model, '#left-sidebar');
                    self.replaceView('headerView', navItem.headerViewType, model, model, model, '#header-container');
                    self.replaceView('menuView', navItem.menuViewType, model, model, model, '#menu-container');
                    self.replaceView('contentView', navItem.contentViewType, model, model, model, '#content-view');
                    self.replaceView('contentExtraView', navItem.contentExtraViewType, model, model, model, '#section-container-extra');
                    self.replaceView('rightSidebarView', navItem.rightSidebarViewType, model, model, model, '#right-sidebar');
                    self.replaceView('footerView', navItem.footerViewType, model, model, model, '#footer-container');
                }

                // removing all tooltips
                self.removeTooltips();
                self.calculateLayout(navItem, self.dfdCalcLayout);
                self.showUrlAnchorView(navItem, model);
                self.changePageData(model);
                self.jumpToAnchor();

                setTimeout(function () {
                    if (!window.backToTopDisabled && !window.isMobileDev) {
                        $.backToTop();
                    }
                }, 500);
            });
        } catch (ex) {
            fail(ex);
        }
    },

    showUrlAnchorView: function (navItem, model) {
        var findViewWithMethod = function (view, methodName) {
            if (view) {
                if (_.isFunction(view[methodName])) {
                    return view;
                }
                //find method in child views
                return _.reduce(view._childViews, function (memo, childView) {
                    return !_.isEmpty(memo) ? memo : findViewWithMethod(childView, methodName);
                }, {});
            }
        };
        if (navItem.urlAnchorView) {
            if (_.isString(navItem.urlAnchorView)) {
                var arr = navItem.urlAnchorView.split(':');
                //[moduleName]:viewName methodName
                arr = arr[arr.length - 1].split(' ');
                var viewName = arr[0];
                var methodName = arr[1];
                var view = this.views[viewName];
                if (view) {
                    var viewWithMethod = findViewWithMethod(this.views[viewName], methodName);
                    if (viewWithMethod) {
                        viewWithMethod[methodName](model);
                    } else if (window.debugMode) {
                        workspace.appView.showErrorMessage('Не определен метод "' + methodName + ' для якоря "' + navItem.urlAnchorView + '"');
                        console.error('Не определен метод "' + methodName + ' для якоря "' + navItem.urlAnchorView + '"');
                    }
                } else if (window.debugMode) {
                    workspace.appView.showErrorMessage('Не определено view "' + arr[0] + ' для якоря "' + navItem.urlAnchorView + '"');
                    console.error('Не определенo view "' + arr[0] + '" для якоря "' + navItem.urlAnchorView + '"');
                }
            } else {
                this.replaceView('urlAnchorView', navItem.urlAnchorView, model, model, model);
            }
        }
    },


    replaceView: function (viewName, ViewType, model, controller, contentModel, anchor, removeCurrentView) {
        var view = null;

        if (ViewType) {
            view = new ViewType({
                model: model,
                controller: controller,
                contentModel: contentModel
            });
        }

        if (removeCurrentView && this.views[viewName]) {
            this.views[viewName].dispose();
        }
        this.views[viewName] = view;

        if (view) {
            if ($(anchor).length) {
                return this.addChildAtElement(anchor, view);
            }
            if (window.debugMode && viewName !== "urlAnchorView") {
                var errorMsg = 'Не элемент "' + anchor + '"';
                workspace.appView.showErrorMessage(errorMsg);
                console.error(errorMsg);
            }
        }
    },


    /**
     * Jumps to the specified anchor element.
     * If element does not exist -- wait for 250ms, than try again.
     * Maximum number of retries is 10.
     *
     * @param tryCount
     */
    jumpToAnchor: function (tryCount) {
        var jumpDelay = 250;
        var tryCountLimit = 100;

        tryCount = _.isUndefined(tryCount) ? 0 : tryCount;
        if (tryCount >= tryCountLimit) {
            return;
        }
        var self = this;
        _.delay(function () {
            try {
                var anchor = workspace.navigationState.get('anchor');
                if (!anchor) {
                    return;
                }
                var element = $('#' + anchor);

                if (!element || element.size() === 0) {
                    self.jumpToAnchor(tryCount + 1);
                } else {
                    if (tryCount > 0) {
                        if (console && console.log) {
                            console.log('tryCount:' + tryCount);
                        }
                    }
                    self.doJump(element);
                }
            } catch (e) {
                workspace.appView.showErrorMessage('Произошла незначительная ошибка, не обращайте внимания:)');
            }
        }, jumpDelay);
    },

    doJump: function (element, tryCount) {
        var offsetCheckPeriod = 250;
        var tryCountLimit = 20;
        var headerHeight = 70;

        tryCount = _.isUndefined(tryCount) ? 0 : tryCount;
        if (tryCount >= tryCountLimit) {
            return;
        }

        var offset = $(element).offset();
        if (offset !== null) {
            window.scrollTo(0, offset.top - headerHeight);
        }

        var self = this;
        _.delay(function () {
            if (offset.top !== $(element).offset().top) {
                self.doJump(element, tryCount + 1);
            }
        }, offsetCheckPeriod);
    },

    /**
     * Calculates main container classes depending on
     * existence of views (rightsidebar, leftsidebar, content)
     */
    calculateLayout: function (options, dfdCalcLayout) {
        options = options || {};

        if (!options.layout) {
            this.expandColumns(options);
        }

        var containerClassSelectors = {
            body: {
                selector: 'body',
                defaultClass: ''
            }
        };
        _.each(containerClassSelectors, function (value, key) {
            var $el = $(value.selector);
            $el.removeAttr('class');
            var className = options[key + 'CssClass'];
            $el.addClass((className === null) ? value.defaultClass : className);
        });
        Modal.finishLongOperation();

        dfdCalcLayout.resolve();
    },

    expandColumns: function () {
        var rightSidebarClass = this.rightSidebarView ? 'span5' : 'span0';
        if (this.rightSidebarView && this.rightSidebarView.communicate) {
            rightSidebarClass = 'span6';
        }
        var rightSidebarEl = $('#right-sidebar');
        var leftSidebarEl = $('#left-sidebar');
        var centerContainerEl = $('#center-container');

        rightSidebarEl.removeClass('span5 span0 span1 span6');
        rightSidebarEl.addClass(rightSidebarClass);
        leftSidebarEl.removeClass('span4 span2 span0');
        centerContainerEl.removeAttr('class');
    },

    changeBackground: function (model) {

        var changeBgUrl = function (url) {
            var $body = $('body');

            if (url) {
                var backgroundImageUrl = ImageUtils.getThumbnailUrl(url, ImageUtils.CAMPAIGN_BACKGROUND, ImageType.PHOTO);

                if ($body.css('backgroundImage') != backgroundImageUrl) {
                    $('<img />').attr('src', backgroundImageUrl).load(function (e) { // late attach trick
                        $body.css('background-image', 'url(' + e.currentTarget.src + ')').css('background-color', '#000');
                    });
                }
            } else {
                $body.css('background-image', '').css('background-color', '');
            }
            return $body;
        };

        if (this.isCampaignSectionMain()) {
            if (this.isCampaignSubsectionMain()) {
                //changeBgUrl(model.get('backgroundImageUrl'));
                return;
            }
        } else if (model.has('group') && model.get('group').has('backgroundUrl')) {
            //changeBgUrl(model.get('group').get('backgroundUrl'));
            return;
        }
        //changeBgUrl();
    },

    isCampaignSectionMain: function () {
        return (workspace.navigationState.get('section') === 'campaigns');
    },

    isCampaignSubsectionMain: function () {
        var subsection = workspace.navigationState.get('subsection');
        return subsection === null || subsection === 'faq' || subsection === 'updates' || subsection === 'backers' || subsection === 'comments';
    },

    changePageData: function (model) {
        var self = this;
        var curModel = model;
        // custom meta tags have greater priority than others
        CustomMetaTagsUtils.getCustomTagForThisPage().done(function (response) {
            if (response || (!response && !window.customMetaTagsFromDb)) { //window.customMetaTagsFromDb - флаг, показывающий были ли custom meta tags взяты первый раз из бд
                var pageData = {};
                if (response && response.success && response.result) {
                    var customTags = response.result;
                    _.each(_.keys(customTags), function (key) {
                        if (!_.isEmpty(customTags[key])) {
                            pageData[key] = customTags[key];
                        }
                    });
                } else {
                    if (curModel && curModel.pageData) {
                        pageData = _.isFunction(curModel.pageData) ? curModel.pageData() : curModel.pageData;
                    } else if (curModel && curModel.pageTitle) {
                        pageData.title = _.isFunction(curModel.pageTitle) ? curModel.pageTitle() : curModel.pageTitle;
                    } else if (self.views.contentView && self.views.contentView.pageTitle) {
                        pageData.title = _.isFunction(self.views.contentView.pageTitle) ? self.views.contentView.pageTitle() : self.views.contentView.pageTitle;
                    }

                    var ogTitle = pageData.ogTitle || pageData.title;
                    if (ogTitle) {
                        pageData.ogTitle = ogTitle;
                    }
                    var ogDescription = pageData.ogDescription || pageData.description;
                    if (ogDescription) {
                        pageData.ogDescription = ogDescription;
                    }
                }
                if (!pageData.ogDescription && pageData.description) {
                    pageData.ogDescription = pageData.description;
                }
                if (!pageData.ogTitle && pageData.title) {
                    pageData.ogTitle = pageData.title;
                }

                self.pageDataObject.update(pageData);

                if (model && model.additionalPageData) {
                    var imageArray = _.difference(model.additionalPageData().imageArray, pageData.imageList, pageData.image);
                    $.each(imageArray, function (key, string) {
                        var metaTagString = '<meta property="og:image" content="' + string + '" data-temporary="true">';
                        $('head').append(metaTagString);
                    });
                } else {
                    $('head [data-temporary]').remove();
                }
            }
            // Tracking page view
            if (workspace && workspace.stats) {
                if (workspace.urlQuery) {
                    var location = window.location.origin + window.location.pathname + (workspace.urlQuery ? '?' + workspace.urlQuery : '');
                    workspace.stats.trackPageViewCampaign(location);
                } else {
                    workspace.stats.trackPageView();
                }
            }
        });
    },

    /**
     * Object for encapsulating page data
     */
    pageDataObject: (function () {
        var selectors = {
            title: ['meta[name=title]'],
            robots: ['meta[name=robots]'],
            //relap_showtill: ['meta[name=relap-showtill]'],
            ogTitle: ['meta[property=og\\:title]'],
            description: ['meta[name=description]'],
            ogDescription: ['meta[property=og\\:description]'],
            image: ['meta[property=og\\:image]', 'link[rel=image_src]'],
            url: ['meta[property=og\\:url]'],
            canonical: ['link[rel=canonical]']
        };
        var defaults = {
            title: 'Planeta - Российская краудфандинговая платформа',
            ogTitle: 'Planeta - Российская краудфандинговая платформа',
            image: '',
            description: '',
            ogDescription: '',
            url: '',
            canonical: '',
            robots: 'noyaca, noodp' // ,
            // relap_showtill: ''
        };

        return {
            /**
             * Updates page data
             */
            update: function (pageData) {
                if (_.isUndefined(pageData) || _.isEmpty(pageData)) {
                    pageData = defaults;
                }
                _.each(_.keys(defaults), function (key) {
                    var value = pageData[key] || defaults[key];
                    $(selectors[key].join(',')).each(function () {
                        var $el = $(this);
                        if ($el.is('link')) {
                            $el.attr('href', value);
                        } else {
                            $el.attr('content', value);
                        }
                    });
                });

                if (pageData.imageList) {
                    $(selectors.image.join(',')).remove();
                    var $head = $('head');
                    _.each(pageData.imageList, function (image) {
                        $head.append('<meta property="og:image" content="' + image + '">');
                        $head.append('<link rel="image_src" href="' + image + '"/>');
                    });
                }


                window.pageTitle = pageData.title || window.pageTitle;
                document.title = pageData.title || document.title;
            }
        };
    }())

});
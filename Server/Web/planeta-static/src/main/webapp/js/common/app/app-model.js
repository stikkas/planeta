/*global BaseAppModel, ProfileOnlineChecker, LazyDialogsController*/
/**
 * Global application model
 */
window.AppModel = BaseAppModel.extend({

    defaults: {
        myProfile: null,
        staticNode: null,
        notifications: null,
        dialogsController: null,
        // Current viewing profile
        profileModel: null,
        // Current viewing page model
        contentModel: null
    },

    initialize: function (options) {

        BaseAppModel.prototype.initialize.call(this, options);

        var onlineChecker = new ProfileOnlineChecker();

        this.set({
            dialogsController: new LazyDialogsController({appModel: this}),
            onlineChecker: onlineChecker
        });
    }
});
/*globals AlbumTypes,UploadModel,Modal,UploadView,UploadSeveralFilesModel,UploadSeveralFilesView*/
var UploadController = {
    L10n: {
        _dictionary:{
            "ru":{
                "uploadAvatar":"Загрузка аватара",
                "uploadAvatarText":"Загрузите изображение для использования в качестве Вашего аватара. После Вы сможете отредактировать или загрузить уменьшенную копию.",
                "uploadVideo":"Загрузка видео",
                "uploadVideoText":"Вы можете загрузить видео-файл размером до 1 Гб. Поддерживаются все популярные видео-форматы. Видео должно быть длиннее 5 секунд",
                "uploadAudio":"Загрузка аудиотрека",
                "uploadAudioText":"Вы можете загрузить аудиотреки размером до 50 Мб. Поддерживаются форматы mp3, wav, wma, aac, ogg, flac.",
                "uploadFiles":"Загрузка файлов",
                "uploadFilesText":"Вы можете загрузить сюда любые файлы."
            },
            "en":{
                "uploadAvatar":"Add avatar",
                "uploadAvatarText":"Add image to use as your avatar. After that you can edit or add a small copy.",
                "uploadVideo":"Add video",
                "uploadVideoText":"You can upload video file up to 1GB. Supported all the popular video formats. Video must be longer than 5 seconds.",
                "uploadAudio":"Add music",
                "uploadAudioText":"You can upload audio file up to 50MB. Supported formats: mp3, wav, wma, aac, ogg, flac.",
                "uploadFiles":"Add files",
                "uploadFilesText":"You can add any files here."
            }
        }
    },
    translate: function (word, lang) {
        if (!lang)
            throw "language is empty.";
        return this.L10n._dictionary[lang][word] || word;
    },
    getCurrentLang: function () {
        return window.workspace && (workspace.currentLanguage === "en") ? "en" : "ru";
    },
    showUploadProfileAvatar: function (profileId, successCallback) {
        this.showUploadModalDialog({
            title: this.translate('uploadAvatar', this.getCurrentLang()),
            text: this.translate('uploadAvatarText', this.getCurrentLang()),
            uploadPath: '/uploadImage',
            autoSaveResult: true,
            uploadSettings: {
                file_upload_limit: 1,
                post_params: {
                    clientId: workspace.appModel.get('myProfile').get('profileId'),
                    ownerId: profileId,
                    albumId: 0,
                    albumTypeId: AlbumTypes.ALBUM_AVATAR
                }
            },
            profileId: profileId,
            successCallback: successCallback

        });
    },

    showUploadVideo: function (profileId, successCallback) {

        this.showUploadModalDialog({
            title: this.translate('uploadVideo', this.getCurrentLang()),
            text: this.translate('uploadVideoText', this.getCurrentLang()),
            uploadPath: '/uploadVideo',
            fileType: 'video',
            uploadSettings: {
                file_size_limit: "1024 MB",
                file_types: "*.3g2;*.3gp;*.asf;*.avi;*.flv;*.mj2;*.mp4;*.mpeg;*.mpg;*.mpeg1video;*.mpeg2video;*.mpegvideo;*.psp;*.rm;*.swf;*.vob;*.wmv;*.mov",
                file_types_description: "Video Files",
                post_params: {
                    clientId: workspace.appModel.get('myProfile').get('profileId'),
                    ownerId: profileId
                }
            },
            autoSaveResult: true,
            successCallback: successCallback
        });
    },

    showUploadOneVideo: function (profileId, successCallback) {

        this.showUploadModalDialog({
            title: this.translate('uploadVideo', this.getCurrentLang()),
            text: this.translate('uploadVideoText', this.getCurrentLang()),
            uploadPath: '/uploadVideo',
            fileType: 'video',
            uploadSettings: {
                file_size_limit: "1024 MB",
                file_types: "*.3g2;*.3gp;*.asf;*.avi;*.flv;*.mj2;*.mp4;*.mpeg;*.mpg;*.mpeg1video;*.mpeg2video;*.mpegvideo;*.psp;*.rm;*.swf;*.vob;*.wmv;*.mov",
                file_types_description: "Video Files",
                file_upload_limit: 1,
                post_params: {
                    clientId: workspace.appModel.get('myProfile').get('profileId'),
                    ownerId: profileId
                }
            },
            autoSaveResult: true,
            successCallback: successCallback
        });
    },

    showUploadAudioTrack: function (profileId, albumId, successCallback) {
        this.showUploadModalDialog({
            title: this.translate('uploadAudio', this.getCurrentLang()),
            text: this.translate('uploadAudioText', this.getCurrentLang()),
            uploadPath: '/uploadAudio',
            fileType: "audio",
            uploadSettings: {
                file_size_limit: "50 MB",
                file_types: "*.mp3;*.wav;*.wma;*.aac;*.ogg;*.flac",
                file_types_description: "Audio Files",
                file_upload_limit: 1,
                post_params: {
                    clientId: workspace.appModel.get('myProfile').get('profileId'),
                    ownerId: profileId,
                    albumId: albumId
                }
            },
            autoSaveResult: true,
            successCallback: successCallback
        });
    },

    /**
     * shop product-group edit - common page
     * @param profileId
     * @param albumId
     * @param successCallback
     */
    showUploadGroupProductImage: function (profileId, albumId, successCallback) {
        this.showUploadModalDialog({
            title: 'Загрузка изображения группы товаров',
            text: 'Вы можете загрузить сюда изображении для группы товаров. Поддерживаются форматы JPG, PNG и GIF.',
            uploadPath: '/uploadImage',
            uploadSettings: {
                file_upload_limit: 1,
                post_params: {
                    clientId: workspace.appModel.get('myProfile').get('profileId'),
                    ownerId: profileId,
                    albumId: albumId,
                    albumTypeId: AlbumTypes.ALBUM_PRODUCT
                }
            },
            autoSaveResult: true,
            successCallback: successCallback
        });
    },
    /**
     * used in shop product edit
     * @param profileId
     * @param albumId
     * @param successCallback
     */
    showUploadProductImages: function (profileId, albumId, successCallback) {
        this.showUploadModalDialog({
            title: 'Загрузка изображений товара',
            text: 'Вы можете загрузить сюда изображения для товара. Поддерживаются форматы JPG, PNG и GIF.',
            uploadPath: '/uploadImage',
            uploadSettings: {
                post_params: {
                    clientId: workspace.appModel.get('myProfile').get('profileId'),
                    ownerId: profileId,
                    albumId: albumId,
                    albumTypeId: AlbumTypes.ALBUM_PRODUCT
                }
            },
            autoSaveResult: true,
            successCallback: successCallback
        });
    },
    showUploadProductTableImage: function (profileId, albumId, successCallback) {
        this.showUploadModalDialog({
            title: 'Загрузка изображения информационной таблицы',
            text: 'Вы можете загрузить сюда изображения для товара. Поддерживаются форматы JPG, PNG и GIF.',
            uploadPath: '/uploadImage',
            uploadSettings: {
                file_upload_limit: 1,
                post_params: {
                    clientId: workspace.appModel.get('myProfile').get('profileId'),
                    ownerId: profileId,
                    albumId: albumId,
                    albumTypeId: AlbumTypes.ALBUM_PRODUCT
                }
            },
            autoSaveResult: true,
            successCallback: successCallback
        });
    },

    showUploadSeminarBackgroundImage: function (profileId, successCallback) {
        this.showUploadModalDialog({
            title: 'Загрузка картинки фона занятия',
            text: 'Вы можете загрузить сюда любую фотографию. Поддерживаются форматы JPG, PNG и GIF.',
            uploadPath: '/uploadImage',
            autoSaveResult: true,
            uploadSettings: {
                file_upload_limit: 1,
                post_params: {
                    clientId: workspace.appModel.get('myProfile').get('profileId'),
                    ownerId: profileId,
                    albumId: 0,
                    albumTypeId: AlbumTypes.ALBUM_HIDDEN
                }
            },
            profileId: profileId,
            successCallback: successCallback
        });
    },

    showUploadWelcomePromoImage: function (profileId, successCallback, text, title) {
        this.showUploadModalDialog({
            title: title || 'Загрузка картинки для промо',
            text: text,
            uploadPath: '/uploadImage',
            autoSaveResult: true,
            uploadSettings: {
                file_upload_limit: 1,
                post_params: {
                    clientId: workspace.appModel.get('myProfile').get('profileId'),
                    ownerId: profileId,
                    albumId: 0,
                    albumTypeId: AlbumTypes.ALBUM_HIDDEN
                }
            },
            profileId: profileId,
            successCallback: successCallback
        });
    },

    showUploadImages: function (profileId, successCallback) {
        this.showUploadModalDialog({
            title: 'Загрузка изображений',
            text: 'Вы можете загрузить сюда изображения для товара. Поддерживаются форматы JPG, PNG и GIF.',
            uploadPath: '/uploadImage',
            uploadSettings: {
                post_params: {
                    clientId: workspace.appModel.get('myProfile').get('profileId'),
                    ownerId: profileId,
                    albumId: 0,
                    albumTypeId: AlbumTypes.ALBUM_HIDDEN
                }
            },
            autoSaveResult: true,
            successCallback: successCallback
        });
    },

    onCropClick: function (url, photoId, profileId, callBackFunction) {
        var viewType = CrowdFund.Views.ImageField.ModalCropView.extend({
            onSuccess: callBackFunction
        });

        var model = new CrowdFund.Models.ImageField({
            originalImage: {
                imageUrl: url,
                photoId: photoId
            },
            profileId: profileId,
            albumTypeId: AlbumTypes.ALBUM_COMMENT,
            thumbnail: {
                imageConfig: ImageUtils.ORIGINAL,
                imageType: ImageType.PHOTO
            },
            aspectRatio: 640 / 360,
            title: 'Обложка',
            description: ''
        });

        var view = new viewType({
            model: model
        });

        view.render();
    },

    showUploadFiles: function (profileId, successCallback) {
        this.showUploadModalDialog({
            title: this.translate('uploadFiles', this.getCurrentLang()),
            text: this.translate('uploadFilesText', this.getCurrentLang()),
            uploadPath: '/uploadFile',
            previewEnabled: false,
            fileType: '',
            uploadSettings: {
                file_size_limit: 0,
                file_types: '',
                file_types_description: '',
                post_params: {
                    clientId: workspace.appModel.get('myProfile').get('profileId'),
                    ownerId: profileId
                }
            },
            autoSaveResult: true,
            successCallback: successCallback
        });
    },

    showUploadContractorFiles: function (profileId, successCallback) {
        this.showUploadModalDialog({
            title: this.translate('uploadFiles', this.getCurrentLang()),
            text: this.translate('uploadFilesText', this.getCurrentLang()),
            uploadPath: '/uploadFile',
            previewEnabled: false,
            fileType: 'scanDocuments',
            uploadSettings: {
                file_size_limit: '50 MB',
                file_types: '*.bmp;*.pdf;*.jpg;*.png;*.jpeg;*.jpe',
                file_types_description: '',
                post_params: {
                    clientId: workspace.appModel.get('myProfile').get('profileId'),
                    ownerId: profileId
                }
            },
            autoSaveResult: true,
            successCallback: successCallback
        });
    },

    showUploadBroadcastAvatar: function (profileId, successCallback) {
        this.showUploadModalDialog({
            title: 'Загрузка изображения трансляции',
            text: 'Вы можете загрузить сюда любую фотографию. Поддерживаются форматы JPG, PNG и GIF.',
            uploadPath: '/uploadImage',
            previewEnabled: false,
            uploadSettings: {
                file_upload_limit: 1,
                post_params: {
                    clientId: workspace.appModel.get('myProfile').get('profileId'),
                    ownerId: profileId,
                    albumId: 0,
                    albumTypeId: AlbumTypes.ALBUM_BROADCAST
                }
            },
            autoSaveResult: true,
            profileId: profileId,
            successCallback: successCallback
        });
    },

    showUploadBroadcastStreamAvatar: function (profileId, successCallback) {
        this.showUploadModalDialog({
            title: 'Загрузка заглушки потока',
            text: 'Вы можете загрузить любую фотографию. Поддерживаются форматы JPG, PNG и GIF.',
            uploadPath: '/uploadImage',
            previewEnabled: false,
            uploadSettings: {
                file_upload_limit: 1,
                post_params: {
                    clientId: workspace.appModel.get('myProfile').get('profileId'),
                    ownerId: profileId,
                    albumId: 0,
                    albumTypeId: AlbumTypes.ALBUM_BROADCAST_STREAM
                }
            },
            autoSaveResult: true,
            profileId: profileId,
            successCallback: successCallback
        });
    },

    uploadByFlash: function () {
        return !$.support.xhrFormDataFileUpload;
    },

    showUploadModalDialog: function (options) {
        var view, model;
        if (this.uploadByFlash()) {
            model = new UploadModel(options);
            view = new UploadView({
                model: model
            });
        } else {
            model = new UploadSeveralFilesModel(options);
            view = new UploadSeveralFilesView({
                model: model
            });
        }
        view.options.underlyingModal = options.underlyingModal;
        Modal.showDialog(view);
    },
    exec: function (funcName) {
        var args = Array.prototype.slice.call(arguments, 1);
        return UploadController[funcName].apply(UploadController, args);
    }
};

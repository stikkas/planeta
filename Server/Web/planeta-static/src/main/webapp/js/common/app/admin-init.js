
(function() {
    var c = workspaceInitParameters.configuration;
    var staticNodesService = new StaticNodesService(c.staticNode, c.staticNodes, c.resourcesHost,
        c.jsBaseUrl, c.flushCache, c.compress);
    TemplateManager.init(workspaceInitParameters.currentLanguage, staticNodesService);


    $(document).ready(function () {



        var Planeta = {

            init: function(options) {

                try {
                    var now = new Date();
                    DateUtils.timeDiff = (now.getTime() - options.configuration.serverTime);
                    DateUtils.serverTime = options.configuration.serverTime;
                    DateUtils.millsDiff = options.configuration.serverTime - now.getTime();


                    // Initializing template manager with the specified file templates
                    var myProfile = this.initMyProfile(options);
                    var appModel = this.initAppModel(options, myProfile);
                    return this.initRouter(options, appModel, myProfile);
                } catch (ex) {
                    if (console && console.log) {
                        console.log(ex);
                    }
                    throw ex;
                }
            },

            initRouter: function(options, appModel, myProfile) {
                var workspace = {
                    appModel: appModel
                };

                workspace.serviceUrls = {
                    adminHost: options.configuration.adminHost,
                    mainHost: options.configuration.mainHost,
                    tvAppUrl: options.configuration.tvAppUrl,
                    shopAppUrl: options.configuration.shopAppUrl,
                    mobileAppUrl: options.configuration.mobileAppUrl,
                    concertsAppUrl: options.configuration.concertsAppUrl,
                    widgetsAppUrl: options.configuration.widgetsAppUrl,
                    affiliatesAppUrl: options.configuration.affiliatesAppUrl,
                    resourcesUrl: options.configuration.resourcesHost,
                    staticNode: options.configuration.staticNode,
                    statServiceUrl: options.configuration.statServiceUrl,
                    statusServiceUrl: options.configuration.statusServiceUrl,
                    imServiceUrl: options.configuration.imServiceUrl,
                    casHost: options.configuration.casHost
                };

                workspace.staticNodesService = new StaticNodesService(options.configuration.staticNode,
                    options.configuration.staticNodes, options.configuration.resourcesHost,
                    options.configuration.jsBaseUrl,
                    options.configuration.flushCache, options.configuration.compress);

                // TODO: Move to myProfile
                workspace.isAuthorized = options.myProfile.isAuthorized;

                return workspace;
            },

            initMyProfile: function(options) {
                var myProfile = new options.appModel.profileConstructor(options.myProfile.profile);
                return myProfile;
            },

            initAppModel: function(options, myProfile) {
                // Init current page profile model
                var profileModel;
                if (options.appModel && options.appModel.profile) {
                    profileModel = new options.appModel.profileConstructor(options.appModel.profile);
                } else {
                    profileModel = myProfile;
                }

                // TODO: Handle unauthorized somewhere here
                var appModel = new options.appModel.constructor({
                    myProfile: myProfile,
                    profileModel: profileModel,
                    countriesList: options.configuration.countries,
                    staticNode: options.configuration.staticNode,
                    statServiceUrl: options.configuration.statServiceUrl,
                    statusServiceUrl: options.configuration.statusServiceUrl,
                    imServiceUrl: options.configuration.imServiceUrl,
                    projectType: options.configuration.projectType
                });

                return appModel;
            }
        };

        workspace = Planeta.init({
            myProfile: workspaceInitParameters.myProfile,
            appModel:{
                constructor: AdminAppModel,
                profileConstructor: BaseProfileModel,
                profile: workspaceInitParameters.profile
            },
            configuration: workspaceInitParameters.configuration,
            staticNodesService: staticNodesService
        });
        workspace.appView = new BaseAppView({
            model: workspace.appModel
        });

        var $page = $('.pagination a');
        var events = $page.data('events');
        if (_.isEmpty(events) || _.isEmpty(events.click)) {
            $page.click(function(e) {
                var url = document.location.href;
                _.each($(this).data(), function(value, key) {
                    url = StringUtils.addParamToUrl(url, key, value);
                });
                document.location.href = url;
            });
        }
    });
})();

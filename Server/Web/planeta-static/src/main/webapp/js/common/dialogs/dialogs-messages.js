/*globals Dialogs,DefaultListView,ResizableScrollListView*/
//max length for message preview
Dialogs.MESSAGE_LENGTH = 300;

/**
 * Dialog message model
 */
Dialogs.Models.Message = BaseModel.extend({

    initialize: function (options) {
        BaseModel.prototype.initialize.call(this, options);
        var attributes = this.parse(options);
        this.set(attributes);
        this.id = options.messageId;
    },

    parse: function (resp) {
        resp.dateAdded = Math.floor(new Date(parseInt(resp.timeAdded, 10)) / (1000 * 60 * 60 * 24));
        return resp;
    },

    destroy: function (options) {
        this.trigger('destroy', this, this.collection, options);
    },


    toJSON: function () {
        var json = BaseModel.prototype.toJSON.call(this);
        json.continueMessage = this.isContinue();
        json.isNewDate = this.isNewDate();
        json.isFloat = !!this.collection.isFloat;
        return json;
    },

    _getPrevModel: function () {
        var index = this.collection.indexOf(this);
        return this.collection.at(index + 1);
    },

    isContinue: function () {
        var prev = this._getPrevModel();
        return prev && prev.get('userId') == this.get('userId');
    },

    isNewDate: function () {
        var prev = this._getPrevModel();
        return !prev || prev.get('dateAdded') != this.get('dateAdded');
    },

    isFirstUnreadMessage: function () {
        var prev = this._getPrevModel();
        return (!prev || !prev.get('newMessage')) && this.get('newMessage');
    },

    isMessageReading: function () {
        return !!this.readingTimeout;
    }
});

/**
 * Model for the list of dialog messages.
 * You can either load next messages
 */
Dialogs.Models.Messages = BaseCollection.extend({
    MESSAGE_IS_READ_AFTER_DELAY: 5000,
    pageSize: 20,
    loading: false,
    model: Dialogs.Models.Message,
    hasUnreadMessage: false,
    unreadMessagesCount: 0,

    url: function () {
        return '//' + workspace.serviceUrls.imServiceUrl + '/dialog-messages.json';
    },

    initialize: function (models, options) {
        BaseCollection.prototype.initialize.call(this, models, options);
        this.dialogId = options.dialogId;
        this.data = {
            dialogId: this.dialogId
        };
        this.on("remove add reset", function () {
            this._onUnreadMessagesChanged();
        }, this);
    },


    onNewMessage: function (messageObject) {
        var message = new this.model(messageObject);
        message.set('newMessage', true);
        this.add(message, {at: 0});
        this.trigger("newMessage", message);
    },

    _onUnreadMessagesChanged: function () {
        this.unreadMessagesCount = this.getUnreadMessages().length;
        this.hasUnreadMessage = this.unreadMessagesCount > 0;
        this.trigger("unreadMessagesChanged", this.dialogId, this.unreadMessagesCount, this.getLastMessage());
    },

    getUnreadMessages: function () {
        return this.filter(function (message) {
            return message.get('newMessage');
        });
    },

    readMessagesImmediately: function (messages) {
        _.each(messages, function (message) {
            this._readMessageImmediately(message.model);
        }, this);
    },

    readAllMessagesImmediately: function () {
        this.each(function (message) {
            this._readMessageImmediately(message);
        }, this);
    },

    _clearReadingTimer: function (message) {
        if (message.readingTimeout) {
            clearTimeout(message.readingTimeout);
            delete message.readingTimeout;
        }
    },

    _readMessageImmediately: function (message) {
        this._clearReadingTimer(message);
        this._readMessage(message);
    },

    readMessagesAfterDelay: function (messages) {
        _.each(messages, function (message) {
            this._readMessageAfterDelay(message.model);
        }, this);
    },

    _readMessageAfterDelay: function (message) {
        var self = this;
        if (!message.isMessageReading()) {
            message.readingTimeout = setTimeout(function () {
                self._readMessage(message);
            }, this.MESSAGE_IS_READ_AFTER_DELAY);
        }
    },

    _readMessage: function (message) {
        if (message.get('newMessage')) {
            message.set('newMessage', false);
            this._onUnreadMessagesChanged();
        }
    },

    cancelReadingMessage: function () {
        this.each(function (message) {
            this._clearReadingTimer(message);
        }, this);
    },


    getFirstUnreadMessage: function () {
        return this.find(function (message) {
            return message.isFirstUnreadMessage();
        });
    },

    loadNext: function (options) {
        this.limit = this.length + this.pageSize;
        this.load(options);
    },

    getLastMessageId: function () {
        var message = this.getLastMessage();
        return message ? message.get('messageId') : 0;
    },

    getLastMessage: function () {
        return this.length === 0 ? null : this.at(0);

    }
});

Dialogs.Views.MessageItem = BaseRichView.extend({
    tagName: 'li',
    className: 'dlg-content-list-item',
    template: '#dialog-message-template',
    mediaTemplates: {
        audio: '#common-audio-template',
        video: '#common-small-video-template',
        photo: '#common-big-photo-template'
    },

    viewEvents: {
        'click .delete': 'onDeleteMessageClicked'
    },

    afterRender: function () {
        if (this.model.isContinue()) {
            this.$el.addClass('continue');
        } else {
            this.$el.removeClass('continue');
        }
        this.renderMedia();
    }
});

Dialogs.Views.MessagesList = ResizableScrollListView.extend({
    tagName: 'ul',
    className: 'dlg-content-list-block',
    anchor: '.dlg-content-list',
    itemViewType: Dialogs.Views.MessageItem,
    sortDirection: 'desc',
    progressiveLoad: true,
    stickToBottom: true,

    initialize: function (options) {
        ResizableScrollListView.prototype.initialize.apply(this, arguments);
        this.isFloat = !!(options && options.isFloat);
        _.bind(this._scroll, this);
    },

    onScroll: function () {
        ResizableScrollListView.prototype.onScroll.apply(this, arguments);

        this.readMessageOverTop();
        this.readVisibleMessage();
    },

    onUserActivity: function () {
        this.readVisibleMessage();
    },

    readVisibleMessage: function () {
        if (this.collection.hasUnreadMessage && this.hasNoReadingVisibleMessage) {
            var noReadingVisibleMessage = this.getNoReadingVisibleMessage();
            this.hasNoReadingVisibleMessage = false;
            this.collection.readMessagesAfterDelay(noReadingVisibleMessage);
        }
    },

    readMessageOverTop: function () {
        if (this.collection.hasUnreadMessage) {
            this.hasNoReadingVisibleMessage = this.getNoReadingVisibleMessage().length > 0;
            this.collection.readMessagesImmediately(this.getUnreadMessageOverTop());
        }
    },

    getNoReadingVisibleMessage: function () {
        var views = this.getViews();

        return _.filter(views, function (view) {
            return view.model.get('newMessage') && !view.model.isMessageReading() && this.isViewVisible(view);
        }, this);
    },

    getUnreadMessageOverTop: function () {
        var views = this.getViews();

        return _.filter(views, function (view) {
            return view.model.get('newMessage') && this.isViewOverTop(view);
        }, this);
    },

    onAdd: function (itemModel) {
        /* don't call repaintScroll in DefaultContentScrollListView */
        var dfd = DefaultListView.prototype.onAdd.apply(this, arguments);

        if (itemModel.get('newMessage')) {
            this.hasNoReadingVisibleMessage = true;
        }

        return dfd;
    },

    _isScrollInExtremeBottomPosition: function () {
        var pane = this.scrollContent.find(".scrollbar-pane");
        return !parseInt(pane.css('bottom'), 10);
    },

    _scroll: function () {
        this.ensureScroll();
        if (!this._isScrollInExtremeBottomPosition()) {
            this.repaintScroll('noscroll');
        } else {
            var firstUnreadMessageModel = this.collection.getFirstUnreadMessage();
            var firstUnreadMessageView = this.findViewByModel(firstUnreadMessageModel);
            if (firstUnreadMessageView) {
                this.scrollTo(firstUnreadMessageView.$el);
            } else {
                this.scrollTo('bottom');
            }
        }
    },

    _appendChild: function (apendedView) {
        var self = this;

        this._debounceScroll = this._debounceScroll || _.debounce(this._scroll, 500);

        var dfd = ResizableScrollListView.prototype._appendChild.call(this, apendedView);

        $.when(dfd).done(function () {
            var img = apendedView.$el.find('.bb-image-preview');
            if (img.length > 0) {
                img.load(self._debounceScroll());
            }
            self._debounceScroll();
        });
        return dfd;
    },

    getDirectionToScroll: function () {
        return this.collection.scrollTo;
    },

    modelJSON: function () {
        var json = ResizableScrollListView.prototype.modelJSON.call(this);
        json.isFloat = this.isFloat;
        return json;
    }
});
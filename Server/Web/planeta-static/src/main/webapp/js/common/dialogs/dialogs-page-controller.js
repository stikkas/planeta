/*globals DefaultContentScrollListView*/

var DialogsPageController = Backbone.Model.extend({
    initialize: function (options) {
        _.bindAll(this);
    },

    onResizeWindow: function () {
        this.trigger('dialogsPageResize');
    },

    /**
     * @param view - view whose bottom edge sticks to window bottom
     * @param resizableView - view which method resizeHeightBy will be called
     */
    addListener: function (view, resizableView) {
        var resizeHeightBy = function () {
            var bottom = view.$el.offset().top + view.$el.outerHeight(true);
            resizableView.resizeHeightBy($(window).height() - bottom);
        };
        this.on("dialogsPageResize", resizeHeightBy);

        var self = this;
        var oldAfterRender = view.afterRender;
        view.afterRender = function () {
            resizeHeightBy();
            if (oldAfterRender) {
                view.afterRender = oldAfterRender;
                view.afterRender();
            } else {
                delete view.afterRedner;
            }
        };
    }
});

var dialogsPageController = new DialogsPageController();


var ResizableScrollListView = DefaultContentScrollListView.extend({
    dontScrollPage: true,

    resizeHeightBy: function (delta) {
        if (!delta) {
            return;
        }

        this.ensureScroll();
        if (!this.scrollContent) {
            return;
        }

        var oldHeight = this.scrollContent.height();
        var newHeight = oldHeight + delta;
        if (this.maxHeight && newHeight > this.maxHeight) {
            newHeight = this.maxHeight;
        }
        if (newHeight == oldHeight) {
            return;
        }
        if (newHeight < 0) {
            newHeight = 0;
        }
        this.scrollContent.height(newHeight);
        this.repaintScroll();
    }
});



/*globals Attach, Photo, ImageUtils*/
Attach.Models.Photo = Attach.Models.BaseModel.extend({
    defaults: {
        imageUrl: null
    },

    getDuration: function (duration) {
        return null;
    },

    selectPhoto: function (photo) {
        //use original size of image
        var imageUrl = ImageUtils.getThumbnailUrl(photo.get('imageUrl'), ImageUtils.ORIGINAL);
        var self = this;
        self.select('photo', {id: photo.get('objectId'), owner: photo.get('profileId')}, photo.get('imageUrl'));
    },

    selectUrl: function (url) {
        var bbcode = '[img]' + url + '[/img]';
        var callback = this.get('selected');
        if (!callback) {
            alert('No selected callback specified!');
            return;
        }
        callback(bbcode);
        this.destroy();
    }
});


Attach.Models.PhotoSelect = Attach.Models.BaseModel.extend({
//	getBbcode:function (bbcodeTag, id, owner, imageUrl, duration) {
//		return '[img]' + imageUrl + '[/img]';
//	},
    defaults: {
        imageUrl: null
    },

    getDuration: function (duration) {
        return null;
    },

    selectPhoto: function (photo) {
        var imageUrl = ImageUtils.getThumbnailUrl(photo.get('imageUrl'), ImageUtils.ORIGINAL);
        this.select('photo', {id: photo.get('objectId'), owner: workspace.appModel.get('profileModel').get('profileId')}, imageUrl);
    },

    /**
     * Wrap external image with proxy and add record to photo db
     * @param {Function} callback (proxyUrl, imageId)
     * @param {String} externalValidImageUrl - external image url
     */
    addImageToAlbum: function (callback, externalValidImageUrl) {
        // client browser may have access to some image, but planeta-server may have no access,
        // so pass image through our proxy as GParser does
        var url = '/api/parse.json';
        var dfd = $.ajax({
            url: url,
            type: "POST",
            data: {
                message: externalValidImageUrl,
                profileId: workspace.appModel.get('profileModel').get('profileId')
            },
            timeout: Backbone.timeout,
            retryMax: Backbone.retryMax
        });
        $.when(dfd).then(function (parsed) {
            if (!parsed.imageAttachments.length) {
                workspace.appView.showErrorMessage('Невозможно загрузить фотографию');
                return;
            }
            var image = parsed.imageAttachments[0];
            callback(image);
        });
    }
});

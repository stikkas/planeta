/**
 * Created by asavan on 30.04.2017.
 */

(function() {

    var c = workspaceInitParameters.configuration;
    var staticNodesService = new StaticNodesService(c.staticNode, c.staticNodes, c.resourcesHost,
        c.jsBaseUrl, c.flushCache, c.compress, workspaceInitParameters.currentLanguage);

    TemplateManager.init(workspaceInitParameters.currentLanguage, staticNodesService);

    $(document).ready(function () {
        workspace = Planeta.init({
            myProfile: workspaceInitParameters.myProfile,
            appModel: {
                constructor: AppModel,
                profileConstructor: BaseProfileModel,
                profile: workspaceInitParameters.profile
            },
            configuration: workspaceInitParameters.configuration,
            staticNodesService: staticNodesService,
            routing: {
                router: SimpleRouter
            },
            currentLanguage: workspaceInitParameters.currentLanguage
        });
        LazyHeader.init({
            auth: headerInitParameters.auth
        });

        if(window.location.hostname.indexOf('localhost') >= 0) {
            var $links = $('a');
            for(var i = 0; i < $links.length; i++) {
                if($links[i].href.indexOf('https') >= 0) {
                    $links[i].href = $links[i].href.replace(/https/g, 'http');
                }
            }
        }
    });
})();


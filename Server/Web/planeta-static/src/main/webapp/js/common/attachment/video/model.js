/*globals ImageUtils,Attach,UserGroups,StringUtils,Backbone*/

Attach.Models.BaseVideo = Attach.Models.BaseModel.extend({

    selectVideoCode: function(video, bbcodeTagName) {
        // objectId and ownerId are here for external video parse support (see Attach.Models.ExternalVideoPlayer.addExternal method)
        //TODO: add type here to store it in bbcode and custom tags
        var id = video.get('videoId') || video.get('objectId');
        var owner = video.get('profileId') || video.get('ownerId');
        var imageUrl = video.get('imageUrl');
        var duration = video.get('duration');
        var name = video.get('name');
        var url = video.get('url');
        var attributes = {
            id: id,
            owner: owner,
            duration: duration,
            name: name,
            url: url
        };
        Attach.Models.BaseModel.prototype.select.call(this, bbcodeTagName, attributes, imageUrl);
    }
});

Attach.Models.ExternalVideo = Attach.Models.BaseVideo.extend({

    selectVideoProperties: function(url, image, duration, callback) {
        var bbcode = {
            imageUrl: image,
            duration: duration,
            videoUrl: url
        };
        callback(bbcode);
        this.destroy();
    },

    select: function (videoType, url, image, duration) {
        var addVideoToUserProfileId = this.get('addVideoToProfileId');
        var playerSize = this.get('playerSize');
        var callback = this.get('selected');
        if (!callback) {
            alert('No selected callback specified!');
            return;
        }

        if (!addVideoToUserProfileId) {
            // We do not need bbcode in this usecase -- only video properties, so we just return them
            // This is used in profile/videos page while adding external video
            this.selectVideoProperties(url, image, duration, callback);
            return;
        }

        var self = this;
        this.addExternalVideo(function (video) {
            video = new BaseModel(video);
            self.selectVideoCode(video, 'video');
        }, url, addVideoToUserProfileId);
    },

    selectPhoto: function (photo) {
        this.select(photo.get('videoType'), photo.get('tubeId'), photo.get('imageUrl'), photo.get('duration'));
    },

    /**
     * Wrap external video with proxy and add record to video db
     * @param {Function} callback (proxyUrl, imageId)
     * @param {String} externalValidVideoUrl - external video url
     */
    addExternalVideo: function (callback, externalValidVideoUrl, addVideoToUserProfileId) {
        var url = '/api/parse.json';
        var dfd = $.ajax({
            url: url,
            type: "POST",
            data: {
                message: externalValidVideoUrl,
                profileId: addVideoToUserProfileId
            },
            timeout: Backbone.timeout,
            retryMax: Backbone.retryMax
        });
        $.when(dfd).then(function (parsed) {
            var video = parsed.videoAttachments[0];
            callback(video);
        });
    }
});

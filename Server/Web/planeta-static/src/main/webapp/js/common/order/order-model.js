/*globals DateUtils, Order*/

var Orders = {

    hasProperty: function(object, propName) {
        return Object.prototype.hasOwnProperty.call(object, propName);
    },

    initProperty: function(source, target, propName, defaultValue) {
        target[propName] = this.hasProperty(source, propName) ? source[propName] : defaultValue;
    }
};

Orders.Models = {};

Orders.Models.BaseFilter = BaseModel.extend({

	defaults: {
		timeFilter: 'today',
		timeStart: DateUtils.getCurrentTime(),
		timeFinish: DateUtils.getCurrentTime(),
		paymentStatus: 'ALL',
        deliveryStatus: 'ALL',
		searchString: null,
        aggregationType: 'DATE',
        loading: false
	},

    getParams: function() {
        var paymentStatus = this.get('paymentStatus');
        var deliveryStatus = this.get('deliveryStatus');
        var searchString = this.get('searchString');

        var result = 'campaignId=' + this.get('campaignId');

        if (this.has('timeStart')) {
            result += '&dateFrom=' + this.get('timeStart');
        }
        if (this.has('timeFinish')) {
            result += '&dateTo=' + this.get('timeFinish');
        }
        if (!_.isEmpty(searchString)) {
            result += '&searchString=' + encodeURIComponent(searchString);
        }
        if (paymentStatus !== 'ALL') {
            result += '&paymentStatus=' + paymentStatus;
        }
        if (deliveryStatus !== 'ALL') {
            result += '&deliveryStatus=' + deliveryStatus;
        }
        if (this.has('aggregationType')) {
            result += '&aggregationType=' + this.get('aggregationType');
        }
        return result;
    },

	setTimeFilter: function(filter) {
		var timeStart = null;
		var timeFinish = null;
		var now = new Date();
		switch (filter) {
			case 'today':
				timeStart = now.getTime();
				timeFinish = now.getTime();
				break;
			case 'yesterday':
				now.setDate(now.getDate() - 1);
				timeStart = now.getTime();
				timeFinish = now.getTime();
				break;
			case 'week':
				timeFinish = now.getTime();
				now.setDate(now.getDate() - 7);
				timeStart = now.getTime();
				break;
		}

		this.silentSetAttrs({
			timeStart: timeStart,
			timeFinish: timeFinish,
			timeFilter: filter
		});
	},

    setStatus: function(statusName, status) {
		this.silentSet(statusName, _.isUndefined(status) ? 'ALL' : status.toUpperCase());
    },

	setSearchString: _.debounce(function(value) {
		this.silentSet('searchString', value);
	}, 500),

    silentSetDate: function(attrName, date) {
        this.silentSet(attrName, date ? date.getTime() : null);
    },

    silentSet: function(attrName, attrValue) {
        if (_.isEqual(this.get(attrName), attrValue)) {
            return;
        }

        var attr = {};
        attr[attrName] = attrValue;
        this.silentSetAttrs(attr);
    },

    silentSetAttrs: function(attrs) {
        this.set(attrs, {silent: true});
        this.trigger('filterChanged');
    }

});

Orders.Models.BaseReportDownloader = BaseModel.extend({

    isSetFormat: function() {
        return this.has('format');
    },

    setFormat: function(format) {
        this.set('format', format);
    },

    getParams: function() {
        return 'reportType=' + this.get('format') + '&' + this.controller.getFilter().getParams();
    }

});

Orders.Models.OrderCollection = BaseCollection.extend({

    initialize: function(options) {
        BaseCollection.prototype.initialize.call(this, options);
        this.allSelector = new BaseModel({
		    allSelected: false
        });
    },

	url: function() {
		return this.controller.getFilter().getUrl();
	},

	parse: function(response) {
        var self = this;
        if (response.success === false) {
            return { success: false };
        }
        var newArray = [];
		_.each(response, function(order, index) {
            var newOrder = _.clone(order);
            newOrder.orderObjectsInfo = Order.Utils.joinOrderObjects(order.orderObjectsInfo, {});
            newOrder.selected = false;
            newOrder.objectsPreview = _.first(order.orderObjectsInfo, 2);
            newArray.push(newOrder);
		});
		return newArray;
	},

    setAllSelected: function(selected) {
        this.allSelector.set('allSelected', selected);
    },

    isAllSelected: function() {
        return this.allSelector.get('allSelected');
    },

    hasSelected: function() {
        var selected = this.find(function(order) {
            return order.get('selected');
        });
        return !_.isUndefined(selected);
    },

    getSelected: function() {
        return this.filter(function(order) {
            return order.get('selected');
        });
    },

    toggleSelection: function(order) {
        var selected = !order.get('selected');
        order.set('selected', selected);
        if (!selected) {
            this.setAllSelected(selected);
        }
        this.trigger('selectionChanged');
    },

    toggleAllSelection: function() {
        var selected = !this.isAllSelected();
        this.setAllSelected(selected);
        this.each(function(order) {
            order.set('selected', selected);
        });
        this.trigger('selectionChanged');
    }

});
window.JsonAuth = {
    /**
     * @param {string} username - valid email address
     * @param {string} password - raw password
     * @param {boolean} [noCasRedirect] - override default redirect behaviour
     */
    registerDfd: function (username, password, noCasRedirect) {
        return this._sendAuthDfd(
                {
                    email: username,
                    username: username,
                    password: password
                },
                '/api/public/signup.json', noCasRedirect);
    },
    /**
     * @param {string} username - raw username
     * @param {string} password - raw password
     * @param {boolean} [noCasRedirect] - override default redirect behaviour
     */
    loginDfd: function (username, password, noCasRedirect) {
        return this._sendAuthDfd(
                {
                    username: username,
                    password: password
                },
                '/api/public/login.json', noCasRedirect);
    },
    /**
     * Just send json data and do default redirect
     * @param {object} data to serialize
     * @param {string} url to send to server
     * @param {boolean} [noCasRedirect] - override default redirect behaviour
     * @returns {$.Deferred}
     * @private
     */
    _sendAuthDfd: function (data, url, noCasRedirect) {
        var $dfd = $.ajax({
            url: url,
            data: data,
            type: 'POST'
        });
        var self = this;
        return $dfd.done(function (response) {
            if (response && response.success) {
                // if (!noCasRedirect) {
                //     self._defaultCasRedirect(response);
                // } else {
                    location.reload();
                // }
            }
        });
    },
    /**
     * @param response
     * @private
     */
    _defaultCasRedirect: function (response) {
        this.globalLogin(response.result.email, response.result.password, document.location.pathname);
    },
    /**
     * Emulates cas-redirect with inline js form
     * Use only after autologin
     * @param {string} username
     * @param {string} password
     * @param {string} successUrl - path (no protocol or host)
     * @private
     */
    globalLogin: function (username, password, successUrl) {
        var jForm = $('<form style="display:none" action="' + workspace.serviceUrls.casHost.replace(/https?:/, '') + '/login" method="post">' +
                '<input name="username" value="' + username + '"/>' +
                '<input name="password" value="' + password + '"/>' +
                '<input name="rememberMe" type="checkbox" value="true" checked="1"/>' +
                '<input name="successUrl" type="text" value="' + location.protocol + '//' + location.host + successUrl + '"/>' +
                '<input name="service" value="' + location.protocol + '//' + location.host + '/login/cas" />' +
                '</form>');
        jForm.appendTo('body').submit();
    }
};

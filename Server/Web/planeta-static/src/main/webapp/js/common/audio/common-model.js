/*globals Form, ProfileUtils*/
/*****************************************************************************
 * Namespaces and global objects
 ****************************************************************************/

var Audios = {

    Models: {},
    Views: {
        Player: {
            Playlist: {},
            Tracklist: {}
        }
    },

    /**
     * Returns configured sound manager
     */
    getSoundManager: function () {
        if(document.readyState !== 'complete'){
            console.error("Sound manager can not start")
        }
        if (this.soundManager) {
            return this.soundManager;
        }
        this.soundManager = window.soundManager || {
            setup: function () {
            }
        };
        this.soundManager.setup({
            useHTML5Audio: true,
            url: '/flash/',
            flashVersion: 9,
            debugMode: false,
            debugFlash: false,
            useHighPerformance: false,
            ontimeout: function () {
                console.log('Loaded OK, but unable to start: unsupported/flash blocked, etc.');
            }
        });
        return this.soundManager;
    }
};


/**
 * Enum for audioplayer states
 */
var PlayerState = {
    STOP: 'stop',
    PLAY: 'play',
    PAUSE: 'pause'
};

Audios.PlaylistType = {
    SEARCH: 'search',
    HISTORY: 'history',
    PLAY_LIST: 'play list',
    ALL_MY_TRACKS: 'all my tracks'
};

/*****************************************************************************
 * Common models used everywhere
 ****************************************************************************/

Audios.Models.Track = BaseModel.extend({

    url: '/api/public/audio-track.json',

    defaults: {
        profileId: 0,
        trackId: 0,
        isConverting: false
    },

    initialize: function (options) {
        var self = this;
        if (!options.hasMp3 && options.profileId > 0) {
            self.set("isConverting", true);
            var count = 0;
            var func = function () {
                self.fetch().done(function (data) {
                    if (!data.result || !data.result.hasMp3) {
                        if (++count < 20) {
                            setTimeout(func, 2000);
                        }
                    } else {
                        self.set(_.extend(data.result, {isConverting: false}));
                    }
                });
            };
            setTimeout(func, 2000);
        }
        BaseModel.prototype.initialize.call(this, options);
    },

    fetch: function (options) {
        options = _.extend({
            data: {
                profileId: this.get('profileId'),
                trackId: this.get('trackId')
            }
        }, options);
        return BaseModel.prototype.fetch.call(this, options);
    },

    destroy: function (options) {
        if (this._isDestroying) {
            return;
        }
        this._isDestroying = true;
        var self = this;
        self.trigger('destroy', self);
    },

    edit: function (options) {
        var self = this;

        var model = new Form.Model({
            itemModel: self,
            url: '/api/public/audio-track.json',
            saveCallback: function (itemModel) {
                if (options && options.success) {
                    options.success(itemModel);
                }
                workspace.appModel.audioPlayer.onTrackChanged(itemModel);
            }
        });
        var view = new Form.View({
            model: model,
            template: '#audio-track-form-template',
            successMessage: 'Трек сохранен'
        });
        view.render();
    }
});

/**
 * Ordered Collection for tracks. It makes possible to render tracks numbers
 * after moving
 *
 * @type {*}
 */
Audios.Models.BaseTracks = BaseCollection.extend({});

Audios.Models.Tracks = Audios.Models.BaseTracks.extend({
    url: '/api/public/audio-tracks.json',
    model: Audios.Models.Track,

    initialize: function (models, attributes) {
        BaseCollection.prototype.initialize.apply(this, arguments);

        var attrs = attributes || {};
        if (attrs.url) {
            this.url = attrs.url;
        }
        var data = attrs.data || {};
        this.profileId = attrs.profileId || data.profileId;
        this.playlistId = attrs.playlistId || data.playlistId;
    },

    load: function () {
        var self = this;
        this.isLoading = true;
        var dfd = Audios.Models.BaseTracks.prototype.load.apply(this, arguments);

        dfd.always(function () {
            self.isLoading = false;
            self.trigger('onLoaded');
        });
        return dfd;
    }
});

Audios.Models.SeveralTracks = Audios.Models.BaseTracks.extend({
    url: '/api/public/several-audio-tracks.json',
    initialize: function (models, options) {
        BaseCollection.prototype.initialize.call(this, [], options);
        var data = {};
        _.each(models, function (model, idx) {
            data['trackObjects[' + idx + '].profileId'] = model.profileId;
            data['trackObjects[' + idx + '].trackId'] =  model.trackId;
        });
        this.data = data;
        this.audios = models;
    },

    parse: function (resp) {
        var self = this;
        if (resp && resp.success) {
            return _.map(resp.result, function (json, idx) {
                if (idx < self.length) {
                    var oldTrack = self.at(idx);
                    _.extend(json || {}, {
                        selected: oldTrack.get("selected"),
                        playerState: oldTrack.get("playerState")
                    });
                }
                return new Audios.Models.Track(json);
            });
        }
    }
});

/*globals Form, UserPayment, Wizard, Campaign, SessionStorageProvider, Campaign, CampaignDonate, SessionStorageProvider, Order, UserCallback*/

var DonateUtils = {
    parseDonateAmount: function (amount) {
        var maxAmount = 1000000000;
        var minAmount = 1;
        var parsed = parseFloat(amount);
        if (_.isNaN(parsed)) {
            return minAmount;
        }
        parsed = Math.abs(Math.round(parsed));
        return parsed > maxAmount ? maxAmount : parsed;
    }
};
if (!window.CampaignDonate) {
    var CampaignDonate = {};
}
CampaignDonate.Models = CampaignDonate.Models || {};

if (!window.CommonDonate) {
    var CommonDonate = {};
}

CommonDonate.Models = {
    silentSet: function (attrName, attrValue, context) {
        var attr = {};
        attr[attrName] = attrValue;
        context.set(attr, {silent: true});
        context.trigger('change:' + attrName, attrValue, context);
    },
    createPayment: function (owner) {
        var attrs = {
            controller: owner,
            myBalance: workspace.appModel.get('myProfile').get('myBalance'),
            price: owner.get('donateAmount') || 0,
            phone: owner.get('phoneNumber') || null,
            paymentType: "PAYMENT_SYSTEM",
            paymentGroup: "PAYMENT_SYSTEM"
        };
        if (owner.isOrderType('INVEST')) {
            attrs.shareId = owner.get('shareId');
        }
        var payment = new CampaignDonate.Models.Payment(attrs);
        payment.on('readinessChanged', owner.triggerValidation, owner);
        owner.set({
            payment: payment,
            callback: UserCallback.Models.createOrderingProblem(Order.Type.SHARE,
                    owner.get('shareId'), payment.get('price'))
        });
    },
    fetchCampaignInfo: function (owner) {
        return _.debounce(function (options) {
            BaseModel.prototype.fetchX.call(new BaseModel(), _.extend({}, options, {
//                url: '/api/campaign/campaign.json?objectId=' + owner.get('campaignId')
                url: '/api/campaign/get-campaign.json?campaignAlias=' + owner.get('campaignId')
            })).done(function (fetchedCampaign) {
                _.extend(owner.get('campaign'), fetchedCampaign);
                owner.trigger('change campaign');
            });
        }, 0, true);
    }
};

CommonDonate.Models.PaymentAndDeliveryPage = BaseModel.extend({
    defaults: {
        campaign: {
            name: 'none',
            campaignId: 0
        },
        linkedDeliveries: [],
        superRegions: [],
        deliveryType: false,
        customerContacts: {},
        reply: '',
        email: '',
        address: {},
        deliveryPrice: 0,
        discountAmount: 0,
        comment: ''
    },
    url: function () {
        return '/api/public/campaign/detailed-share.json?shareId=' + this.get('shareId');
    },
    initialize: function () {
        BaseModel.prototype.initialize.apply(this, arguments);
        this.restoreCommonFields();

        var self = this;
        var myProfile = workspace.appModel.get('myProfile');
        var attrs = {
            errors: {},
            profileId: this.get('profileModel').get('profileId'),
            hasEmail: workspace.isAuthorized,
            email: myProfile.has('email') ? myProfile.get('email') : '',
            customerName: this.has('customerName') ? this.get('customerName') : myProfile.has('displayName') ? myProfile.get('displayName') : '',
            phoneNumber: this.has('phoneNumber') ? this.get('phoneNumber') : myProfile.has('phoneNumber') ? myProfile.get('phoneNumber') : ''
        };
        switch (this.get('orderType')) {
            case 'SHARE':
                _.extend(attrs, {
                    shareId: this.get('navigationState').get('arg4'),
                    campaignId: this.get('objectId'),
                    doRequireDelivery: function () {
                        return _.any(self.get('linkedDeliveries'), function (it) {
                            return it.enabled;
                        });
                    }
                });
                break;
            case 'INVEST':
                _.extend(attrs, {
                    shareId: this.get('navigationState').get('arg4'),
                    campaignId: this.get('objectId')
                });
                break;
            case 'PRODUCT':
                _.extend(attrs, {
                    doRequireDelivery: function () {
                        return self.get('shoppingCartModel').doRequireDelivery();
                    },
                    cartItems: function () {
                        return workspace.shoppingCart.get('cartItems').toJSON();
                    },
                    canFreeDelivery: function() {
                        return self.get('shoppingCartModel').canFreeDelivery();
                    }
                });
                break;
        }

        this.set(attrs);
        var cc = this.get('customerContacts');
        if (myProfile.has('phoneNumber') && !cc.phone)
            cc.phone = myProfile.get('phoneNumber');
        this.restoreCoreFields();
        //============ DELIVERY =======================
        this.deliverySectionModel = new BaseModel({});
        this.deliverySectionModel.attributes = this.attributes;
        CommonDonate.Models.createPayment(this);
        this.get('callback').fetch();
    },
    hasDigitalTrades: function () {
        return this.get('shoppingCartModel').hasDigitalTrades();
    },
    hasProductWithDisabledCash: function () {
        return this.get('shoppingCartModel').hasProductWithDisabledCash();
    },
    restoreCoreFields: function () {
        if (this.isOrderType('SHARE') || this.isOrderType('INVEST')) {
            var shareId = this.get('shareId');
            var sharePurchase = SessionStorageProvider.get('sharePurchase_' + shareId);
            if (sharePurchase) {
                this.set(sharePurchase);
            } else {
                var href = window.location.href;
                if (href.slice(-1) === '/') {
                    href = href.slice(0, -1);
                }
                workspace.navigate(href.substring(0, href.lastIndexOf('/')));
            }
        }
    },
    restoreCommonFields: function () {
        this.set(SessionStorageProvider.get('commonInfo'));
    },
    clearCache: function () {
        SessionStorageProvider.remove("commonInfo");
        if (this.isOrderType('SHARE') || this.isOrderType('INVEST')) {
            SessionStorageProvider.extendByKey('sharePurchase_' + this.get('shareId'));
        }
    },
    backupAll: function () {
        SessionStorageProvider.extendByKey('commonInfo', {
            customerContacts: this.get('customerContacts'),
            customerName: this.get('customerName'),
            orderType: this.get('orderType'),
            delServiceId: this.get('delServiceId'),
            phoneNumber: this.get('phoneNumber'),
            deliveryType: this.get('deliveryType')
        });
        if (this.get('orderType') === 'SHARE' || this.get('orderType') === 'INVEST') {
            var shareId = this.get('shareId');
            SessionStorageProvider.extendByKey('sharePurchase_' + shareId, {
                donateAmount: this.get('donateAmount'),
                quantity: this.get('quantity'),
                serviceId: this.get('serviceId'),
                reply: this.get('reply') || ''
            });
        }
    },
    prefetch: function (options) {
        options = options || {};

        var loadingObjects = [this.get('payment')];
        var loadingOpts = options;

        var self = this;
        switch (this.get('orderType')) {
            case 'INVEST':
            case 'SHARE':
                if (!(this.has('donateAmount') && this.has('quantity'))) {
                    options.error('Адрес не доступен');
                    return;
                }

                loadingObjects = _.union(loadingObjects, [this, CommonDonate.Models.fetchCampaignInfo(this)]);
                loadingOpts = {
                    success: function () {
                        var storedAddress = self.get('storedAddress');
                        if (storedAddress) {
                            _.extend(self.get('customerContacts'), storedAddress);
                            self.set({
                                customerName: storedAddress.customerName,
                                phoneNumber: storedAddress.phone
                            });
                            console.log('customerContacts');
                            console.log(self.get('customerContacts'));
                        }
                        options.success();
                    }
                };
                break;
            case 'PRODUCT':
                loadingObjects = _.union(loadingObjects, [this.get('shoppingCartModel')]);
                loadingOpts = {
                    success: function () {
                        var totalSum = self.get('shoppingCartModel').get('totalSum');
                        self.get('payment').setPrice(totalSum + self.get('deliveryPrice'));
                        self.set({
                            'donateAmount': totalSum,
                            'totalSum': totalSum
                        });
                        options.success();
                    }
                };
                break;
            case 'DELIVERY':
                break;
        }

        try {
            this.parallel(loadingObjects, loadingOpts);
        } catch (e) {
            options.success({success: false});
        }
    },
    fetch: function (options) {
        return this.fetchX(options).fail(function (_$, errorMessage) {
            workspace.appView.showErrorMessage(errorMessage);
        });
    },
    getContactInfo: function () {
        return this.get('customerContacts');
    },
    getAddresseeInfo: function () {
        var address = this.get('customerContacts');

        return (this.get('deliveryType') == 'DELIVERY' || this.get('deliveryType') == 'RUSSIAN_POST') ? {
            city: address.city || '',
            country: address.countryId,
            street: address.street || '',
            phone: address.phone || '',
            zipCode: address.zipCode || '',
            name: this.get('customerName') || ''
        } : {
            addresseeId: -1
        };
    },
    purchase: function () {
        var self = this,
                payment = self.get('payment').toJSON(),
                orderType = self.get('orderType');

        switch (orderType) {
            case 'INVEST':
                var investInfo = self.get('investInfo'),
                        paymentMethodId = self.paymentMethodId,
                        needPhone = false;
            case 'SHARE':
                var purchaseData = {
                    shareId: self.get('shareId'),
                    count: self.get('quantity'),
                    serviceId: self.get('serviceId'),
                    reply: self.get('reply') || '',
                    donateAmount: self.get('donateAmount'),
                    customerName: self.get('customerName'),
                    campaignId: self.get('campaignId')

                },
                purchase = {
                    shareId: self.get('shareId'),
                    quantity: self.get('quantity'),
                    reply: self.get('reply') || '',
                    serviceId: self.get('serviceId'),
                    donateAmount: self.get('donateAmount'),
                    customerName: self.get('customerName'),
                    address: self.get('customerContacts'),
                    email: self.get('email') || '',
                    paymentMethodId: paymentMethodId || payment.paymentMethodId,
                    paymentPhone: payment.phone,
                    needPhone: needPhone == false ? false : payment.needPhone,
                    shareStatus: self.get("shareStatus"),
                    authorized: workspace.isAuthorized
                },
                purchaseJsonOptions = {
                    contentType: 'application/json',
                    method: 'update'
                },
                customerContacts = self.get('customerContacts');
                purchaseJsonOptions.url = '/api/public/campaign/validate-share-purchase.json';
                purchaseJsonOptions.data = JSON.stringify(purchase);

                return self.fetchX(purchaseJsonOptions).done(function () {
                    var purchaseOptions = {
                        amount: payment.amount,
                        fromBalance: payment.fromBalance,
                        phone: payment.phone,
                        paymentMethodId: paymentMethodId || payment.paymentMethodId,
                        isAuthorized: workspace.isAuthorized,
                        email: self.get('email') || ''
                    };
                    if (self.has('tlsSupported')) {
                        purchaseOptions.tlsSupported = self.get('tlsSupported');
                    }
                    if (purchaseData) {
                        _.extend(purchaseOptions, purchaseData);
                    }
                    if (investInfo) {
                        purchaseOptions.investInfo = JSON.stringify(investInfo);
                    }
                    if (customerContacts) {
                        _.extend(purchaseOptions, {
                            zipCode: customerContacts.zipCode,
                            country: customerContacts.country,
                            city: customerContacts.city,
                            cityId: customerContacts.cityId,
                            countryId: customerContacts.countryId,
                            street: customerContacts.street,
                            contactPhone: customerContacts.phone
                        });
                    }
                    if (self.get('deliveryType')) {
                        purchaseOptions.deliveryType = self.get('deliveryType');
                    }

                    if (window.gtm) {
                        window.gtm.trackChoicePaymentProvider(self, paymentMethodId || payment.paymentMethodId);
                    }
                    $.form("/payment-create.html", purchaseOptions).submit();
                });

            case 'DELIVERY':
                var params = {
                    amount: payment.amount,
                    price: payment.price,
                    fromBalance: payment.fromBalance,
                    paymentMethodId: payment.paymentMethodId,
                    phone: payment.phone,
                    isAuthorized: workspace.isAuthorized,
                    email: self.get('email') || ''
                };

                return this.fetchX({
                    url: "/api/public/delivery-payment-create.json",
                    data: params,
                    method: 'update',
                    success: function (url) {
                        window.location = /*'/welcome/cas-redirect.html?successUrl=' + encodeURIComponent(*/url/*)*/;
                    }
                });

            case 'PRODUCT':
                var promoCodeId = self.get('promoCodeModel').get('promoCodeId');
                return this.fetchX({
                    url: "/payment/purchase-shopping-cart.json",
                    dataType: 'json',
                    contentType: 'application/json',
                    data: $.extend({
                        fio: this.get('customerName'),
                        email: this.get('email'),
                        onlyDigitalProducts: !self.doRequireDelivery()
                    }, {
                        amount: payment.amount,
                        fromBalance: payment.fromBalance,
                        paymentMethodId: payment.paymentMethodId,
                        paymentType: payment.paymentType,
                        paymentPhone: payment.phone,
                        needPhone: payment.needPhone
                    },
                            this.getAddresseeInfo(),
                            this.getDeliveryInfo(),
                            this.getContactInfo(), {
                        phone: this.get('phoneNumber'),
                        promoCodeId: promoCodeId
                    }),
                    method: 'update',
                    success: function (object) {
                        if (window.gtm) {
                            window.gtm.trackChoicePaymentProviderForCart(this.attributes.cartItems(), payment.totalSum, payment.paymentMethodId);
                        }
                        self.clearCache();
                        window.location = payment.paymentType === 'CASH'
                                ? /*'/welcome/cas-redirect.html?successUrl=*/'/payment-success.html?orderId=' + object.orderId
                                : /*'/welcome/cas-redirect.html?successUrl=' + encodeURIComponent(*/object/*)*/;
                    }
                });
        }
    },
    isOrderType: function (orderType) {
        return _.isEqual(this.get('orderType'), orderType);
    },
    canPurchaseDelivery: function () {
        return this.isOrderType('DELIVERY') && this.get('payment').get('ready') && (this.get('hasEmail') || this.get('isEmailValid'));
    },
    getDeliveryInfo: function () {
        return {
            deliveryServiceId: this.get('serviceId') || 0,
            deliveryPrice: this.get('deliveryPrice') || 0,
            deliveryType: this.get('deliveryType') || 'NOT_SET'
        };
    },
    triggerValidation: function () {
        this.trigger('checkPurchase');
    }
});

CommonDonate.Models.SharePaymentAndDeliveryPage = CommonDonate.Models.PaymentAndDeliveryPage.extend({
    restoreCommonFields: function () {
        CommonDonate.Models.PaymentAndDeliveryPage.prototype.restoreCommonFields.apply(this, arguments);
        this.set('orderType', "SHARE");
    }
});

CommonDonate.Models.InvestingPaymentPage = CommonDonate.Models.PaymentAndDeliveryPage.extend({
    defaults: _.extend({}, CommonDonate.Models.PaymentAndDeliveryPage.prototype.defaults, {
        smsVerificationEnabled: true
    }),
    restoreCommonFields: function () {
        CommonDonate.Models.PaymentAndDeliveryPage.prototype.restoreCommonFields.apply(this, arguments);
        this.set('orderType', "INVEST");
        this.set('investInfo', {
            gender: 'NOT_SET'
        });
    },
    prePurchase: function () {
        return this.fetchX({
            contentType: 'application/json',
            method: 'update',
            url: '/campaign/validate-invest-purchase.json',
            data: JSON.stringify(this.get('investInfo'))
        });
    },
    confirmPhone: function (phone) {
        return this.fetchX({
            contentType: 'application/json',
            method: 'update',
            url: '/api/confirm-phone.json',
            data: {'phoneNumber': phone}
        });
    }
});



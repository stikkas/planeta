var FileUploader = function (options) {
    var listeners = options.listener ? [options.listener] : [];
    var $fileupload = options.container || $('#fileupload');


    $fileupload.each(function () {
        $(this).attr('id', 'fileupload-' + Math.random().toString().replace(/\./, ''));
    });

    var params = _.clone(options);
    _.defaults(params, {
        type: 'POST',
        dataType: 'json',
        limitConcurrentUploads: 0,
        sequentialUploads: true,
        replaceFileInput: false,
        multipart: false
    });
    if (_.isFunction(params.url)) {
        params.url = params.url();
    }

    var isOneFileMode = !!options.isOneFileMode;
    if (isOneFileMode) {
        params.singleFileUploads = false;
    }

    var lastEventAdd;
    var lastEventProgress;
    var handlers = {
        onAdd: function (listener, e, data) {
            _.each(isOneFileMode ? [_.first(data.files)] : data.files, function (file) {
                if (listener.onAdd) {
                    listener.onAdd(file);
                }
            });
        },

        onProgress: function (listener, e, data) {
            _.each(data.files, function (file) {
                if (listener.onProgress) {
                    listener.onProgress(file, data.loaded);
                }
            });
        },

        onDone: function (listener, e, data) {
            _.each(data.files, function (file) {

                if (listener.onDone) {
                    listener.onDone(file, data.result);
                }
            });
        },

        onFail: function (listener, e, data) {
            _.each(data.files, function (file) {
                if (data.errorThrown === "abort") {
                    if (listener.onCancel) {
                        listener.onCancel(file);
                    }
                } else {
                    if (listener.onError) {
                        listener.onError(file);
                    }
                }
            });
        }
    };

    $fileupload.fileupload(_.extend(params, {
            add: function (e, data) {
                lastEventAdd = {
                    name: 'onAdd',
                    e: e,
                    data: data
                };
                _.each(listeners, function (listener) {
                    handlers.onAdd(listener, e, data);
                });
            },
            progress: function (e, data) {
                lastEventProgress = {
                    name: 'onProgress',
                    e: e,
                    data: data
                };
                _.each(listeners, function (listener) {
                    handlers.onProgress(listener, e, data);
                });
            },
            done: function (e, data) {
                lastEventProgress = {
                    name: 'onDone',
                    e: e,
                    data: data
                };
                _.each(listeners, function (listener) {
                    handlers.onDone(listener, e, data);
                });
            },
            fail: function (e, data) {
                lastEventProgress = {
                    name: 'onFail',
                    e: e,
                    data: data
                };
                _.each(listeners, function (listener) {
                    handlers.onFail(listener, e, data);
                });
            },
            dragover: function (e, data) {
                _.each(listeners, function (listener) {
                    if (listener.onDragover) {
                        listener.onDragover(e, data);
                    }
                });
            },

            drop: function (e, data) {
                _.each(listeners, function (listener) {
                    if (listener.onDrop) {
                        listener.onDrop(e, data);
                    }
                });
            }
        })
    );

    //disable the default browser action for file drops on the document
    $(document).bind('drop dragover dragenter dragleave', function (e) {

        e.stopPropagation();
        e.preventDefault();

        var dt = e.originalEvent.dataTransfer;
        if (dt) {
            dt.dropEffect = 'none';
        }

        return false;

    });

    this.send = function (file) {
        file.jqXHR = $fileupload.fileupload('send', {files: [file]});
    };



    this.cancel = function (files) {
        function cancelUploadFile(file) {
            if (file && file.jqXHR && _.isFunction(file.jqXHR.abort)) {
                try {
                    file.jqXHR.abort();
                } catch (ignore) {
                }
            }
        }
        if (_.isArray(files)) {
            _.each(files, function (file) {
                cancelUploadFile(file);
            });
        } else {
            cancelUploadFile(files);
        }
    };

    this.isXHRUpload = function () {
        return !($fileupload.fileupload('option', 'forceIframeTransport'))
            && ((!$fileupload.fileupload('option', 'multipart') && $.support.xhrFileUpload)
            || $.support.xhrFormDataFileUpload);
    };

    this.isIframeTransport = function () {
        return !this.isXHRUpload();
    };

    this.canDropFiles = function () {
        return this.isXHRUpload();
    };

    this.setDropZone = function (dropZone) {
        $fileupload.fileupload('option', 'dropZone', dropZone);
    };

    this.setOptions = function (varName, value) {
        $fileupload.fileupload('option', varName, value);
    };

    this.addListener = function (listener) {
        listeners = _.union(listeners, listener);
        if (lastEventAdd) {
            handlers[lastEventAdd.name](listener, lastEventAdd.e, lastEventAdd.data);
        }
        if (lastEventProgress) {
            handlers[lastEventProgress.name](listener, lastEventProgress.e, lastEventProgress.data);
        }
    };

    this.removeListener = function (listener) {
        listeners = _.without(listeners, listener);
    };

    this.removeAllListeners = function () {
        listeners = {};
    };

};


FileUploader.parseFileLimitSize = function (fileLimitSize) {
    var size = fileLimitSize.toUpperCase().replace(/ /g, '');
    var mul = {"KB": 1024, "MB": 1024 * 1024, "GB": 1024 * 1024 * 1024}[size.replace(/[1234567890]/g, '')];
    return parseInt(size, 10) * (mul || 1);
};

FileUploader.isGoodFileExtention = function (filename, fileTypes) {
    if (!fileTypes || fileTypes == '') {
        return true;
    }

    //'*.png;*.jpg;*.jpeg;*.gif' -> '(.*\.png)$|(.*\.jpg)$|(.*\.jpeg)$|(.*\.gif)$'
    var s = '(' + fileTypes.replace(/\./g, '\\.').replace(/\*/g, '.*').replace(/;/g, ")$|(") + ')$';
    var regexp = new RegExp(s, 'i');
    return regexp.test(filename);
};
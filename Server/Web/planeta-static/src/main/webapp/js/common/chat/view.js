/*global Chat,workspace,confirm,DefaultContentScrollListView,Modal,BaseView,$*/
/**
 * Chat views
 */

/**
 * Chat container view
 */
Chat.Views.ChatContainer = BaseView.extend({
    tagName: 'div',

    modelEvents: {
        'destroy': 'dispose'
    },

    construct: function () {
        this.model.loadNextMessages('bottom');
        this.addChild(new Chat.Views.Chat({model: this.model}));
    },

    afterRender: function () {
        var liveBlock = $('.broadcast-live-block');
        var liveChat = $('.broadcast-live');
        var posBtn = $('.js-undock-button', liveChat);
        posBtn.click(function () {
            if (liveChat.hasClass('ui-draggable')) {
                liveChat.draggable('destroy');
                liveChat.removeAttr('style').appendTo(liveBlock);
            } else {
                liveChat.css({
                    position: 'fixed',
                    right: 0,
                    top: 0
                }).appendTo($('body'));
                liveChat.draggable({
                    handle: $('.bl-h-title', liveChat),
                    containment: 'window',
                    scroll: false,
                    snap: "html",
                    snapMode: "inner"
                });
            }
        });
    },

    dispose: function () {
        this.model.leaveChat();
        BaseView.prototype.dispose.call(this);
    }
});

/**
 * Chat main views
 */
Chat.Views.Chat = BaseView.extend({
    template: '#chat-template',
    tagName: 'div',
    className: 'broadcast-live',

    modelEvents: {
        'destroy': 'dispose'
    },

    construct: function () {
/*        this.addChildAtElement('.users-info', new Chat.Views.UsersInfo({
            model: this.model
        }));*/

        this.views = {};
        this.views.postMessage = this.addChildAtElement('.post-new-message-div', new Chat.Views.PostMessage({
            model: workspace.appModel.get('myProfile')
        }));

        this.views.messages = this.addChildAtElement('.chat-messages-div', new Chat.Views.Messages({
            collection: this.model.get('messages'),
            itemViewType: Chat.Views.Message
        }));

        this.bind('postMessage', this.postMessage, this);
        this.bind('deleteMessage', this.deleteMessage, this);
        this.bind('banUser', this.banUser, this);
    },

    postMessage: function () {
        var messageTextEl = $(this.views.postMessage.el).find('textarea');
        var messageText = messageTextEl.val();
        if (messageText) {
            this.model.postMessage(messageText, function () {
                messageTextEl.val('');
            });
        }
    },

    deleteMessage: function (view) {
        var self = this;
        if (Modal) {
            Modal.showConfirm("Вы действительно хотите удалить данное сообщение?", "Подтверждение действия", {
                success: function () {
                    self.model.deleteMessage(view.model);
                }
            });
        } else {
            if (confirm("Вы действительно хотите удалить данное сообщение?")) {
                self.model.deleteMessage(view.model);
            }
        }
    },

    banUser: function (view) {
        var self = this;
        if (Modal) {
            Modal.showConfirm("Вы действительно хотите забанить данного пользователя?", "Подтверждение действия", {
                success: function () {
                    self.model.banUser(view.model);
                }
            });
        } else {
            if (confirm("Вы действительно хотите забанить данного пользователя?")) {
                self.model.banUser(view.model);
            }
        }
    }
});

/**
 * Chat messages views
 */
Chat.Views.Messages = DefaultContentScrollListView.extend({
    pagerTemplate: null,
    pagerLoadingTemplate: null,
    stickToBottom: true,

    tagName: 'ul',
    className: 'bl-dlg-list',
    sortDirection: 'asc'
});

Chat.Views.Message = BaseView.extend({
    template: '#chat-message-template',
    tagName: 'li',
    className: 'bl-dlg-item',

    viewEvents: {
        'click .js-delete-comment': 'deleteMessage',
        'click .js-ban-user': 'banUser'
    }
});


/**
 * Chat post message view
 */
Chat.Views.PostMessage = BaseView.extend({

    template: '#chat-post-message-template',

    afterRender: function () {
        var textarea = this.$el.find('textarea');
        //textarea.limitInput({limit: 160, remTextEl: this.$el.find('.pln-ww-content-symbol-count')});
        textarea.elastic(true);
        this.toggleCancel();
        textarea.val('');
    },

    events: {
        'click div.close': 'cancel',
        'keypress textarea': 'postMessageOnEnter',
        'keyup textarea': 'toggleCancel',
        'click .btn-primary': 'postMessage'
    },

    postMessageOnEnter: function (e) {
        if (e.keyCode == 13 || e.keyCode == 10) {
            e.preventDefault();

            if (!e.ctrlKey) {
                this.postMessage(e);
            } else {
                var messageTextEl = this.$el.find('textarea');
                var messageText = messageTextEl.val();
                messageText += '\r\n';
                messageTextEl.val(messageText);
            }
        }
    },

    postMessage: function (e) {
        e.preventDefault();

        if (!LazyHeader.checkNotAnonymous()) {
            e.stopPropagation();
            return false;
        }

        var messageTextEl = this.$el.find('textarea');
        var messageText = messageTextEl.val();
        if (messageText) {
            this.trigger('postMessage');
        }

        return true;
    },

    toggleCancel: function() {
        var textLength = this.$el.find('textarea').val().length;
        if (textLength > 0) {
            this.$el.find('.close').show();
        } else {
            this.$el.find('.close').hide();
        }
    },

    cancel: function (e) {
        e.preventDefault();
        //this.$el.find('textarea').limitInput('update', '');
        this.toggleCancel();
    }
});

Chat.Views.UsersInfo = BaseView.extend({
    template: '#chat-users-info-template'
});

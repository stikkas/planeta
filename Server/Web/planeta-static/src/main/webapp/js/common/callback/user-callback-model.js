var UserCallback = {
    PHONE_NUMBER_LENGTH: 11,
    Types: {
        FAILURE_PAYMENT: 'FAILURE_PAYMENT',
        ORDERING_PROBLEM: 'ORDERING_PROBLEM'
    },
    Models: {
        createFailurePayment: function(paymentId, amount) {
            return new UserCallback.Models.Callback({
                type: UserCallback.Types.FAILURE_PAYMENT,
                paymentId: paymentId,
                amount: amount
            });
        },

        createOrderingProblem: function(orderType, objectId, amount) {
            return new UserCallback.Models.Callback({
                type: UserCallback.Types.ORDERING_PROBLEM,
                orderType: orderType,
                objectId: objectId,
                amount: amount
            });
        }
    },
    Views: {}
};

UserCallback.Models.Callback = BaseModel.extend({
    url: '/api/public/callback/available',
    defaults: {
        phone: ''
    },

    setPhone: function(phone) {
        this.set({phone: phone.replace(/\s/g, '')}, {silent: true});
    },

    isReady: function() {
        var phone = this.get('phone');
        return phone && _.isEqual(_.size(phone.replace(/\D/g, '')), UserCallback.PHONE_NUMBER_LENGTH);
    },

    fetch: function() {
        var self = this;
        return Backbone.sync('read', this, {
            data: {
                amount: this.get('amount')
            },
            success: function(resp) {
                self.set('available', resp.result);
            },
            error: function () {
                self.set('available', false);
            }
        });
    },

    send: _.throttle(function() {
        var self = this;
        if (!self.get('completed')) {
            return Backbone.sync('create', this, {
                url: '/api/public/callback/create'
            }).done(function() {
                self.set({completed: true}, {silent: true});
            });
        }

        return $.Deferred().resolve().promise();
    }, 3000)
});

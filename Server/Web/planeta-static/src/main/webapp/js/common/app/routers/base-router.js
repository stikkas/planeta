/*globals CookieProvider,StringUtils,jQuery,counters,Header,ProfileModel,ProfileUtils,PrivacyUtils,JobManager,dialogsPageController,Backbone,$,_,Modal,workspace,console,BaseModel,moduleLoader*/
/**
 * Base planeta router
 */
var BaseRouter = Backbone.Router.extend({
    reserved: new RegExp('^about.*|^account.*|^admin.*|^administrator.*|^album.*|^artist.*|^audio.*|^blog.*|^dialogs.*|^event.*|^fanblog.*|^friend.*|^funding-rules.*|^group.*|^info.*|^members.*|^news.*|^notifications.*|^objectId.*|^payments.*|^photo.*|^product.*|^promotions.*|^purchases.*|^search.*|^section.*|^settings.*|^statistics.*|^subsection.*|^user.*|^userfriends.*|^video.*|^top|^profile|^contacts|^posts.*|^faq|^spirit|^campaigns.*|^delivery.*|^interactive.*|^quiz.*'),
    routes: {
        '*path': 'routeDoesNotExists'
    },

    openImmediately: function (path) {
        if (!path) {
            path = '';
        }
        if (StringUtils.isAbsoluteUrl(path)) {
            // This is absolute url
            window.location.href = path;
        } else {
            // This is relative url
            window.location.href = '/' + path;
        }
    },

    routeDoesNotExists: function (path) {
        if (!path) {
            path = '';
        }
        if (StringUtils.isAbsoluteUrl(path)) {
            // This is absolute url
            window.location.href = path;
        } else {
            // This is relative url
            var enroute = (path.indexOf('?') >= 0 ? '&' : '?') + 'enroute=true';
            window.location.href = '/' + path + enroute;
        }
    },

    /**
     * To not to write navigate(url, true) every time
     * @param url relative path
     * @param options trigger|silent|replace
     * @deprecated  use onClickNavigate instead
     * @return undefined
     */
    navigate: function (url, options) {
        /*var urlToGtm = url;
        if (this.removedUrlQuery) {
            urlToGtm += "?" + this.removedUrlQuery;
        }*/
        if (options !== false && !(options && _.isBoolean(options.trigger))) {
            options = _.extend(options || {}, {trigger: true});
        }


        /*if (workspace && workspace.stats) {
            workspace.stats.trackPageView(urlToGtm);
        }*/

        return Backbone.Router.prototype.navigate.call(this, url, options);
    },

    changeUrl: function (url) {
        if (this._routerEnabled) {
            Backbone.Router.prototype.navigate.call(workspace, url, {
                trigger: false,
                replace: true
            });
        } else if (window.history) {
            window.history.replaceState({}, document.title, url);
        }
    },

    addAnchor: function (anchor) {
        if (this.navigationState) {
            this.navigationState.set('anchor', anchor, {silent: true});
        }
        Backbone.Router.prototype.navigate.call(workspace, decodeURIComponent(window.location.pathname).replace(/!.*/, '') + "!" + anchor, {
            trigger: false,
            replace: true
        });
    },

    removeAnchor: function () {
        if (this.navigationState) {
            this.navigationState.set('anchor', null, {silent: true});
        }
        Backbone.Router.prototype.navigate.call(workspace, decodeURIComponent(window.location.pathname).replace(/!.*/, ''), {
            trigger: false,
            replace: true
        });
    },

    addParam: function (param, value) {
        var newUrl = StringUtils.addParamToUrl(window.location.pathname + window.location.search, param, value);
        Backbone.Router.prototype.navigate.call(workspace, newUrl, {
            trigger: false,
            replace: true
        });
    },

    removeParam: function (param, value) {
        var newUrl = StringUtils.removeParamFromUrl(window.location.pathname + window.location.search, param, value);
        Backbone.Router.prototype.navigate.call(workspace, newUrl, {
            trigger: false,
            replace: true
        });
    },

    reloadPage: function () {
        var fragment = Backbone.history.fragment;
        Backbone.history.fragment = "";
        Backbone.Router.prototype.navigate.call(this, fragment, {
            trigger: true,
            replace: true
        });
    },


    /**
     * usage from anywhere width workspace.isRouterEnabled()
     * @return {Boolean} is Backbone.Router used for routing?
     */
    isRouterEnabled: function () {
        return (this._routerEnabled === true);
    },

    /**
     * Replaces native browser link following with Backbone.Router.navigate()
     * @param href - url to navigate
     * @see Modal.View.redirectToUnderlyingPage
     */
    onClickNavigate: function (e, href) {

        //route to static content
        if (href && (href.indexOf('.html') >= 0 || href.indexOf('.ru/account') >= 0)) {
            return;
        }

        if (!href || href.indexOf('javascript:') === 0 || href === '#' || (e && e.isDefaultPrevented())) {
            if (e && !e.isDefaultPrevented()) {
                if (!href || !href.match(/javascript\s*:\s*workspace/)) {
                    e.preventDefault();
                }
            }
            return;
        }
        if (e) {
            var target = $(e.currentTarget).attr('target');
            if (target && (target === '_blank' || target === 'popup' || target === 'top' || target === '_self')) {
                return;
            }
            if (href.indexOf('mailto:') === 0) {
                return;
            }
            if (e.metaKey || e.ctrlKey) {
                return;
            }

            e.preventDefault();
        }
        var host = StringUtils.extractHost(href);

        var route = href;
        // common js code can call this method even with backbone switched off
        if (!this.isRouterEnabled() || (host != null && host != document.location.host)) {
            // This link is to another host, navigating without backbone
            this.navigateToHost(href, host);
            return;
        }

        if (host != null) {
            route = href.substring(href.indexOf(host) + host.length);
        }
        route = route.replace(/^\/?#?\/?/, '');
        this.navigate('/' + route);
        return;
    },

    /**
     * This method is used when click on the link has been intercepted
     * and the url is targeting another host.
     *
     * @param url Target url
     * @param host Target host
     */
    navigateToHost: function (url, host) {
        var serviceUrls = _.values(workspace.serviceUrls);
        var isInternalService = _.find(serviceUrls, function (serviceUrl) {
            return serviceUrl.indexOf(host) !== -1;
        });
        if (isInternalService) {
            document.location = url;
        } else {
            // This link is to another domain. Opening it in another tab.
            var otherWindow = window.open();
            otherWindow.opener = null;
            otherWindow.location = url;
        }
    },
    /*ie Hack. Store hash to restore it after redirection (in index.jps)
     */
    ieHackToStoreHash: function () {
        Backbone.History.prototype._loadUrl = Backbone.History.prototype.loadUrl;
        Backbone.History.prototype.loadUrl = function () {
            if (document.location.hash) {
                try {
                    CookieProvider.set('ie-stored-hash', document.location.hash);
                } catch (ex) {
                    console.log(ex);
                }
            }

            Backbone.History.prototype._loadUrl.apply(this, arguments);
        };
    },

    /**
     * Starts Backbone.history
     * @param pushState
     */
    start: function (pushState) {
        var self = this;
        $('a').live('click', function (e) {
            var href = $(this).attr('href');
            return self.onClickNavigate(e, href);
        });

        if (!(pushState && window.history && window.history.pushState)) {
            this.ieHackToStoreHash();
        }

        // change root for IE browser
        var root = "/";
        this._routerEnabled = true;
        if (jQuery.browser.msie && parseFloat(jQuery.browser.version) < 10) {
            root = document.location.pathname;
        }

        var loc = document.location;
        var hash = loc.hash;
        if (loc.search.indexOf("?;") === 0) {
            loc.search = '';
        }
        if (hash === "#!") {
            if (window.history.pushState) {
                window.history.replaceState("", document.title, loc.pathname + loc.search);
            } else {
                loc.href = loc.href.replace('#!', '');
                return;
            }
        }
        if (hash === "#_=_") { // facebook hash fix
            if (window.history.pushState) {
                window.history.replaceState("", document.title, loc.pathname + loc.search);
            } else {
                loc.href = loc.href.replace('#_=_', '');
                return;
            }
        }
        if (loc.hash) {
            if (/#(ym_playback|single)/.test(loc.hash)) {
                var path = loc.pathname;
                Backbone.history.start({silent: true, root: root});
                // Backbone.history.history['pushState']({}, document.title, path);
                // loc.hash = '';
                Backbone.history.loadUrl(path);
            } else {
                loc.hash = '';
                Backbone.history.start({pushState: pushState, silent: true, root: root});
                this.navigate(hash, true);
            }
        } else {
            Backbone.history.start({pushState: pushState, root: root});
        }
    },
    /**
     * Handles navigation from 'fatal' errors
     * if you want page replacing in such cases
     * remove descendant AppRouter.showIndex and add here {replace: true} option
     */
    showIndex: function () {
        if (!this.appModel) {
            return;
        }

        if (!workspace.isAuthorized) {
            document.location.href = '/';
            return;
        }

        this.navigate('/news', true);
    },

    stripQueryString: function (lastUrlPart) {
        //noinspection JSLint
        try {
            var jsessionIndex = (String(lastUrlPart)).indexOf(";jsessionid");
            if (jsessionIndex > 0) {
                lastUrlPart = (String(lastUrlPart)).substring(0, jsessionIndex);
            }
        } catch (e) {
            console.log(e);
            console.log(lastUrlPart);
        }
        var match = (String(lastUrlPart)).match(/(.*)\?(.*)/);
        if (match && match[2]) {
            return match[1];
        }
        return lastUrlPart;
    },

    /**
     * state contains additional arguments arg0 - argN
     * @param id   profileId/groupId/eventId owner id
     * @param section
     * @param objectId
     * @param subsection
     * @param anchor
     */
    changeRoute: function (id, section, objectId, subsection, anchor) {
        var self = this;
        var state = {
            id: id,
            section: section || 'default',
            objectId: objectId || null,
            subsection: subsection || null,
            anchor: anchor || null
        };
        _(arguments).each(function (value, index) {
            state['arg' + index] = value;
        });
        $.each(state, function (key, value) {
            if (value) {
                state[key] = self.stripQueryString(value);
            }
        });
        this.navigationState.set(state);
        //console.log(window.location.href, this.navigationState.getNavigationKey(), id, section, objectId, subsection, anchor);
        this.doNavigate(section);
    },

    checkAnonymousPageAccess: function (profileId) {
        var sectionName = this.navigationState.get('section');
        var isGlobalSection = _.indexOf(['search', 'funding-rules', 'faq', 'delivery-payment', 'welcome', 'news', 'campaigns', 'interactive'], sectionName) >= 0;
        var isAuthorizedUsersSection = _.indexOf(['quiz'], sectionName) >= 0;
        if (this.appModel.myProfileId() == -1 && isAuthorizedUsersSection) {
            return this.doAuthorizeModalLogicAndGetResponse();
        }
        if (!this.appModel.isMyProfile(profileId) && !isGlobalSection) {

            if (profileId === 'welcome') {
                window.location.href = '/404.html';
            }

            if (sectionName === 'account' || sectionName === "settings") {
                return this.doAuthorizeModalLogicAndGetResponse();
            }

            return true;
        }

        return true;
    },

    doAuthorizeModalLogicAndGetResponse: function () {
        Modal.finishLongOperation();
        if (workspace.isAuthorized) {
            return false;
        }
        $(function () {
            LazyHeader.loadHeader().done(function () {
                var view = Header.showAuthForm('signup');
                var bindCloseModal = function () {
                    $(view.el).find('.close,.cancel').unbind('click');
                    $(view.el).find('.close,.cancel').click(function () {
                        // Returning user to welcome page if
                        // he chose not to log in
                        document.location.href = '/';
                    });
                };
                var parentAfterRender = view.afterRender;

                view.afterRender = function () {
                    parentAfterRender.call(view);
                    bindCloseModal();
                };

                bindCloseModal();
            });

        });
        return false;
    },

    doNavigate: function (section) {
        Modal.closeAllModal();
        Modal.startLongOperation();
        if (this.appModel && this.appModel.audioPlayer && this.appModel.audioPlayer.isPlaying()) {
            this.appModel.audioPlayer.playPause();
        }
        try {//removeTryCatch
            $(window).trigger('stateChanged');
            this.specialProject();
            if (section === 'campaigns') {
                this.doNavigateCampaign();
            } else if (section === 'quiz') {
                this.doNavigateQuiz();
            } else {
                this.appModel.unset('campaignModel');
                this.doNavigateProfile();
            }
        } catch (ex) {
            Modal.finishLongOperation();
            this.appView.showErrorMessage('Непредвиденная ошибка');
        }
    },

    doNavigateProfile: function () {
        var profileId = this.navigationState.get('id');
        if (this.checkAnonymousPageAccess(profileId)) {
            if (this.appModel.isSameProfile(profileId)) {
                this.onProfileChanged(this.appModel.get('profileModel'));
            } else if (this.appModel.isMyProfile(profileId) || profileId === -1) {
                this.onProfileChanged(this.appModel.get('myProfile'));
            } else {
                this.changeProfile(profileId);
            }
        }
    },

    doNavigateQuiz: function () {
        this.doNavigateProfile();
    },

    doNavigateCampaign: function em() {
        function replaceCampaignId(url, campaignAlias) {
            var st = url.indexOf("/campaigns/");
            if (st >= 0) {
                var oldurl = url.substring(st);
                url = oldurl;

                st = "/campaigns/".length;
                var en = st;
                var ln = url.length;
                while (en < ln && url[en] != '/' && url[en] != '?' && url[en] != '!') {
                    en++;
                }

                url = url.substring(0, st) + campaignAlias + url.substring(en);
                if (url !== oldurl) {
                    return url;
                }
            }
        }
        var campaign = this.appModel.get('campaignModel');
        var objectId = this.navigationState.get('objectId');
        if (!campaign || (campaign.get('campaignId') != objectId && campaign.get('campaignAlias') != objectId)) {
            this.onCampaignChange(objectId);
        } else {
            var subsection = this.navigationState.get('subsection');
            var isEditSection = _.isString(subsection) && subsection.indexOf('edit') === 0;

            if (campaign.get('status') === 'DRAFT' && !isEditSection) {
                this.navigate(campaign.get('canChangeCampaign') ? "campaigns/" + campaign.get('campaignId') + '/edit' : '' + campaign.get('creatorProfileId'), {
                    trigger: true,
                    replace: true
                });
                return;
            }

            if (isEditSection && !campaign.get('canChangeCampaign')) {
                this.navigate("campaigns/" + campaign.get('webCampaignAlias'), {
                    trigger: true,
                    replace: true
                });
                return;
            }
            if (campaign.get('campaignAlias')) {
                var newHref = replaceCampaignId(window.location.href, isEditSection ? campaign.get('campaignId') : campaign.get('webCampaignAlias'));
                if (newHref) {
                    this.navigate(newHref, {trigger: false, replace: true});
                }
            }
            this.navigationState.set({
                id: campaign.get('creatorProfileId'),
                objectId: campaign.get('campaignId')
            });
            this.doNavigateProfile();
        }
    },

    onCampaignChange: function () {
        var self = this;

        function fail(message) {
            self.appView.showErrorMessage(message);
            Modal.finishLongOperation();
            window.location.href = '/404.html';
        }

        Backbone.sync("read", null, {
            url: '/api/public/campaign/campaign-short-info.json',
            data: {webCampaignAlias: this.navigationState.get('objectId')}
        }).done(function (response) {
            if (response && response.success) {
                var campaignModel = new BaseModel(response.result);
                self.appModel.set('campaignModel', campaignModel);
                self.navigationState.set({
                    objectId: campaignModel.get('campaignId')
                });
                self.doNavigateCampaign();
            } else {
                fail("Ошибка при загрузке проекта.");
            }
        }).fail(function () {
            fail("Проект не найден.");
        });
    },

    specialProject: function () {
        var $specialProject = $('#js-header-special-project');
        $specialProject.find('.js-create-button-block');
        var $createButton = $specialProject.find('.js-create-button');
        $createButton.click(function (e) {
            if (workspace.isAuthorized) {
                e.preventDefault();
            }
            moduleLoader.loadModule('welcome').done(function () {
                Welcome.Views.SelectDraftCampaignList.onCreateProjectClicked(e);
            });
        });

    },

    getNavigationMap: function (profileType) {
        var navigationMap = null;
        switch (profileType) {
            case 'USER':
                navigationMap = this.navigationMap.User;
                break;
            case 'GROUP':
            case 'HIDDEN_GROUP':
                navigationMap = this.navigationMap.Group;
                break;
            default:
                this.appView.showErrorMessage('There is no such profile type: ' + profileType);
                return null;
        }
        return navigationMap;
    },

    getNavigationMapItem: function (profile) {
        var key = this.navigationState.getNavigationKey();
        var navigationMap = this.getNavigationMap(profile.get('profileType'));

        var findNavSection = function () {
            return _(navigationMap).find(function (section) {
                return !(!section[key]);
            });
        };
        var navSection = findNavSection();

        if (!navSection && this.navigationState.get('section') === 'campaigns') {
            navigationMap = this.getNavigationMap("GROUP");
            navSection = findNavSection();
        }
        if (!navSection) {
            return null;
        }

        var navItem = _.extend({}, navSection.defaults, navSection[key]);
        if (this.navigationState.get('anchor') && navItem.urlAnchorView) {
            navItem.urlAnchorView = navItem.urlAnchorView[this.navigationState.get('anchor')];
        } else {
            delete navItem.urlAnchorView;
        }
        return navItem;

    },

    changeProfile: function (profileId) {
        var profile = new ProfileModel({profileId: profileId});

        var self = this;
        profile.fetch({
            data: {
                profileId: profileId
            },
            success: function (model) {
                // TODO: Check what will be returned for non-existing profile
                if (model.get('success') != undefined && !model.get('success')) {
                    self.appView.showErrorMessage(model.get('errorMessage'));
                    Modal.finishLongOperation();
                    setTimeout(function () {
                        self.showIndex();
                    }, 1000);
                } else {
                    self.appView.closeAlert();
                    self.onProfileChanged(model);
                }
            },
            error: function () {
                self.appView.showErrorMessage('Заданный профиль не найден.');
                Modal.finishLongOperation();
                window.history.back();
            }
        });
    },

    onProfileChanged: function (profileModel) {
        this.appModel.set({
            profileModel: profileModel
        });

        if (this.navigationState.get('section') === 'default') {
            // TODO: remove default tabs for groups and users
            var defaultSection = null;
            this.navigationState.set({
                section: defaultSection || 'info'
            });
        }

        var navItem = this.getNavigationMapItem(profileModel);

        if (!navItem) {
            this.appView.showErrorMessage('Введен некорректный адрес страницы.');
            Modal.finishLongOperation();
            window.location.href = "/404.html";
            return;
        }

        this.onNavigationStateChanged(navItem, profileModel);
    },

    evalNavigationItem: function (navItem) {
        if (navItem.module) {
            navItem.modules = navItem.module.split(',');
        } else {
            navItem.modules = [];
        }
        var self = this;
        _.each(navItem, function (value, key) {
            if (typeof navItem[key] == 'string' && (StringUtils.endsWith(key, 'Type') || StringUtils.endsWith(key, 'View'))) {
                var bbClass, module, clazz = value;
                if (value.indexOf(':') >= 0) {
                    module = value.split(':')[0];
                    clazz = value.split(':')[1];

                }
                bbClass = moduleLoader.findClass(clazz);
                if (bbClass) {
                    navItem[key] = bbClass;
                } else {
                    if (key != 'urlAnchorView') {
                        navItem.failed = true;
                        navItem.clazz = clazz;
                    }
                    if (module) {
                        navItem.modules.push(module);
                    }
                }
            }
        });
        return navItem;
    },


    createContentModel: function (navItem, profile, navigationState) {
        var $dfd = new $.Deferred();
        var self = this;

        var _createModel = function (modelType) {
            modelType = modelType || BaseModel;
            if (!(_.isEqual(modelType, BaseModel) || modelType.prototype instanceof BaseModel)) {
                workspace.appView.showDebugMessage('modelType "' + (modelType.prototype && modelType.prototype.backboneClassName) + '" must be instanceof BaseModel');
                return null;
            }
            var ModelType = modelType.extend({
                // These fields should be set before initialize is called
                objectId: navigationState.has('objectId') ? navigationState.get('objectId') : null,
                profileId: profile.get('profileId'),
                alias: profile.get('alias'),
                profileLink: ProfileUtils.getUserLink(profile.get('profileId'), profile.get('alias'))
            });

            var emptyModel = new ModelType({
                profileModel: profile,
                objectId: navigationState.has('objectId') ? navigationState.get('objectId') : null,
                section: navigationState.has('section') ? navigationState.get('section') : null,
                subsection: navigationState.has('subsection') ? navigationState.get('subsection') : null,
                id: ProfileUtils.getUserLink(profile.get('profileId'), profile.get('alias')),
                navigationState: navigationState
            });

            $dfd.resolve(emptyModel);
        };

        var evaluatedNavItem = this.evalNavigationItem(navItem);
        // TODO try to eval while all strings become resolved (success) or undefined (logic error)

        if (!evaluatedNavItem.failed && _.isEmpty(evaluatedNavItem.modules)) {
            navItem = evaluatedNavItem;
            _createModel(navItem.modelType);
        } else {
            var dfd;
            if (!_.isEmpty(navItem.modules)) {
                dfd = moduleLoader.loadModules(navItem.modules);
            }
            dfd.done(function () {
                navItem = self.evalNavigationItem(evaluatedNavItem);
                _createModel(navItem.modelType);
            });
        }
        return $dfd;
    },

    onNavigationStateChanged: function (navItem, profile) {
        var section = this.navigationState.get('section');

        if (profile.get('profileType') === 'HIDDEN_GROUP') {
            this.navigate('/' + (profile.get('alias') || profile.get('creatorProfileId')), true);
            return;
        }

        if (!PrivacyUtils.checkSubSectionAccess(section, profile, this.navigationState.get('subsection'), this.navigationState.get('objectId'))) {
            // NOTE: Access to this subsection is denied, navigating to the main section of this profile
            this.navigate('/' + PrivacyUtils.getNavigateString(section, profile), true);
            return;
        }

        //clear jobs
        JobManager.clearQueue();

        var self = this;
        this.createContentModel(navItem, profile, this.navigationState).done(function (model) {
            model.prefetch({
                success: function (result) {
                    try {//removeTryCatch

                        if (!self.checkContentModelFetchResult(result)) {
                            Modal.finishLongOperation();
                            self.navigate('/' + PrivacyUtils.getDefaultSection(section, profile), true);
                            return;
                        }

                        // Replacing current viewing model
                        self.appModel.set({
                            contentModel: model
                        });

                        self.appView.onRouteChanged(profile, navItem, model);

                        if (self.navigationState.get('section') === "welcome") {
                            if (self.navigationState.get('anchor') === "error") {
                                console.log("Authorization popup showed from base-router");
                                LazyHeader.showAuthForm('signup');
                            } else if (self.navigationState.get('anchor') === "registrationError") {
                                console.log("Authorization popup showed from base-router");
                                LazyHeader.showAuthForm('registration');
                            } else if (self.navigationState.get('anchor') === "passwordRecover") {
                                LazyHeader.showPasswordRecoveryForm();
                            }
                        }
                    } catch (ex) {
                        self.appView.showErrorMessage('Ошибка отрисовки страницы');
                        console.log(ex);
                        Modal.finishLongOperation();
                    }
                },
                error: function (errorMessage) {
                    errorMessage = typeof errorMessage === 'string' ? errorMessage : 'При открытии страницы произошла непредвиденная ошибка.';
                    self.appView.showErrorMessage(errorMessage);
                    Modal.finishLongOperation();
                    self.navigate('/' + PrivacyUtils.getDefaultSection(section, profile), true);
                }
            });

            workspace.appModel.get('dialogsController').onPageChanged();
        });


    },

    checkContentModelFetchResult: function (result) {
        if (result) {
            if (result.attributes) {
                if (result.get('success') != undefined && !result.get('success')) {
                    this.appView.showErrorMessage(result.get('errorMessage'));
                    return false;
                }
            }
            if (result.success != undefined && !result.success) {
                this.appView.showErrorMessage(result.errorMessage);
                return false;
            }
        }

        return true;
    }
});

/**
 * Class containing current Planeta-Web navigation state
 * Custom NavigateState
 */
var NavigationState = BaseModel.extend({

    defaults: {
        id: null,
        section: null,
        subsection: null,
        objectId: null
    },

    getNavigationKey: function () {
        var key = this.get('section');
        if (this.get('section') === 'search') {
            if (this.has('subsection')) {
                switch (this.get('subsection')) {
                    case 'projects':
                    case 'users':
                    case 'blogs':
                    case 'shares':
                        key += ':' + this.get('subsection');
                        break;
                    default:
                        key += ':object';
                }
            }
            if (this.has('objectId')) {
                key += ':object';
            }
            return key;
        }
        if (this.has('subsection')) {
            if ((this.get('section') === 'campaigns') &&
                (_(['about', 'comments', 'backers', 'updates', 'faq']).indexOf(this.get('subsection')) >= 0)) {
            } else if ((this.get('section') === 'interactive') &&
                (_(['start', 'name', 'email', 'proceed', 'info', 'duration', 'price', 'video', 'description', 'reward', 'counterparty', 'check', 'final', 'final-draft']).indexOf(this.get('subsection')) < 0)) {
                key += ':start';
            } else {
                key += ':' + this.get('subsection');
            }
        }
        if (this.get('section') === 'quiz') {
            if (this.has('objectId')) {
                key += ":quizId";
            }
            return key;
        }
        if (this.get('section') === 'campaigns') {
            if (this.has('objectId')) {
                if (this.get('objectId') === 'create') {
                    key += ':create';
                } else {
                    key += ':campaignId';
                }
            }
            if (['donate', 'donatesingle'].indexOf(this.get('subsection')) > -1 && this.has('arg4') && this.get('arg4')) {
                key += ':shareId';
                var it = this.get('arg5');
                if (it === 'payment' || it === 'paymentinvestul' || it === 'paymentinvestfiz')
                    key += ':' + it;
            }
        } else {
            if (this.has('objectId')) {
                key += ":object";
            }
        }
        if (this.get('section') === 'contractor') {
            return 'contractor';
        }
        return key;
    },

    set: function (key, value, options) {
        this.routeHistory = this.routeHistory || [];
        this.routeHistory.push(_.clone(this.attributes));
        BaseModel.prototype.set.call(this, key, value, options);
    },

    historyGo: function (step) {
        if (step == null) {
            return this.routeHistory;
        }
        var index = (step + this.routeHistory.length) % this.routeHistory.length;
        return this.routeHistory[index];
    }
});

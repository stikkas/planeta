/*global StringUtils, PrivacyUtils*/

var FAQ = {Models: {}, Views: {}};
FAQ.isEdit = function () {
    return PrivacyUtils.hasAdministrativeRole() && workspace.navigationState.get('subsection') === "edit";
};

FAQ.Models.CategoryCollection = BaseCollection.extend({
    url: '/api/public/faq-category-list.json'
});

FAQ.Models.ArticleCollection = BaseCollection.extend({
    url: '/api/public/faq-article-list.json'
});

FAQ.Models.ParagraphCollection = BaseCollection.extend({
    url: '/api/public/faq-paragraph-list.json'
});


FAQ.Models.Main = BaseModel.extend({
    prefetch: function (options) {
        this.faqCategories = new FAQ.Models.CategoryCollection([]);
        this.parallel([this.faqCategories], options);
    },
    /**
     * This fucking magic needed to make transition from main to search with no cursor left
     * @override
     * @param query
     */
    search: function (query) {
        this.set({subsectionName: 'Результаты поиска'});
        var model = new FAQ.Models.Search({query: query});
        model.fetch();
        workspace.appView.replaceView('contentView', FAQ.Views.SearchResults, model, model, model, '#section-container', true);
        workspace.navigate("/faq/search/" + StringUtils.hashEscape(encodeURIComponent(query)), false);
    }
});

FAQ.Models.BaseSearch = BaseModel.extend({
    search: function (query) {
        this.set('query', query);
        workspace.navigate("/faq/search/" + StringUtils.hashEscape(encodeURIComponent(query)), false);
        this.fetch();
    }
});


FAQ.Models.Search = FAQ.Models.BaseSearch.extend({
    defaults: {
        query: '',
        subsectionName: 'Результаты поиска'
    },
    initialize: function (options) {
        if (options.objectId) {
            this.set('query', decodeURIComponent(StringUtils.hashUnescape(options.objectId)));
        }
        this.snippets = new FAQ.Models.SearchSnippets([], {
            model: this
        });
        this.snippets.queryModel = this;
    },
    fetch: function (options) {
        this.snippets.load(options);
    }
});
FAQ.Models.SearchSnippet = BaseModel.extend({
    initialize: function () {
        BaseModel.prototype.initialize.apply(this, arguments);
        this.set({highlightedCroppedAnnotationHtml: StringUtils.searchSnippet(this.get('highlightedAnnotationHtml'), 50, 400)});
    }
});
FAQ.Models.SearchSnippets = BaseCollection.extend({
    model: FAQ.Models.SearchSnippet,
    url: function () {
        return "/api/public/faq-search.json?" + $.param({query: this.queryModel.get('query')});
    }
});

FAQ.Models.Article = BaseModel.extend({
    url: function () {
        return '/api/public/faq-paragraph-list-with-article.json?' + $.param({faqArticleId: this.get('objectId')});
    }
});

/*globals RichMediaUtils, Attach, ScrollableListView, PrivacyUtils, ImageUtils, console,workspace,BaseListView*/
/*jslint regexp:true*/
/*****************************************************************************
 * Default views that could be used everywhere
 ****************************************************************************/

var DefaultListView = BaseListView.extend({
    pagerTemplate: '#pager-template',
    pagerLoadingTemplate: '#pager-loading-template',

    initialize: function () {
        BaseListView.prototype.initialize.apply(this, arguments);
        this._isRendered = false;
    },
    afterRender: function () {
        BaseListView.prototype.afterRender.call(this);
        if (!this._isRendered && this.collection.length === 0 && !this.collection.allLoaded) {
            this.renderLoaderElement();
            this._isRendered = true;
        }
    }

});

var DefaultMutableListView = DefaultListView.extend({
    onAdd: function () {
        DefaultListView.prototype.onAdd.apply(this, arguments);
        this.renderPager();
    },

    onRemove: function () {
        DefaultListView.prototype.onRemove.apply(this, arguments);
        this.renderPager();
    }
});

var DefaultScrollableListView = ScrollableListView.extend({
    pagerLoadingTemplate: '#scrollable-list-loader-template',

    initialize: function () {
        ScrollableListView.prototype.initialize.apply(this, arguments);
    },

    onScrollDown: function (e) {
        ScrollableListView.prototype.onScrollDown.call(this);
        if (e && this.collection) {
            this.collection.onLoadSuccess = function () {
                if (workspace && workspace.stats) {
                    if (document.location.pathname.indexOf('/search/') >= 0) {
                        if(workspace.campaignListTrackedFlag) {
                            workspace.campaignListTrackedFlag = false;
                        } else {
                            workspace.stats.trackPageView();
                        }
                    }
                }
            };
        }
    }
});

var DefaultContentScrollListView = DefaultListView.extend({
    progressiveLoad: false,

    initialize: function (options) {
        DefaultListView.prototype.initialize.apply(this, arguments);
        if (this.progressiveLoad) {
            this.pagerTemplate = "";
        }
        _.each(['stickToBottom', 'dontScrollPage', 'autoAdjustContainerSize', 'handleSideOffset'], function (param) {
            if (options[param]) {
                this[param] = options[param];
            }
        }, this);
    },

    onAdd: function (itemModel, collection, options) {
        var dfd = DefaultListView.prototype.onAdd.call(this, itemModel, collection, options);
        var self = this;
        dfd.always(function () {
            self.repaintScroll();
        });
        return dfd;
    },

    onRemove: function (itemModel) {
        var retValue = DefaultListView.prototype.onRemove.call(this, itemModel);
        this.repaintScroll();
        return retValue;
    },

    onReset: function () {
        var retValue = DefaultListView.prototype.onReset.call(this);
        this.scrollContent = null;
        return retValue;
    },

    findScrollbarPane: function () {
        return this.$el.closest('.scrollbar-pane');
    },

    ensureScroll: function () {
        if (this.scrollContent && !this._isElementInDom(this.scrollContent)) {
            this.scrollContent = null;
        }
        if (this.scrollContent) {
            return;
        }
        if (!this.scrollContent && this.findScrollbarPane().length > 0) {
            this.scrollContent = this.$el.closest('.modal-scroll-content');
            return;
        }

        this.scrollContent = this.$el.closest('.modal-scroll-content');
        if (this.scrollContent.length > 0) {
            this.scrollContent.scrollbar(_.extend({
                arrows: false,
                stickToBottom: !!this.stickToBottom,
                dontScrollPage: !!this.dontScrollPage,
                autoAdjustContainerSize: !!this.autoAdjustContainerSize,
                handleSideOffset: this.handleSideOffset || 0
            }, this.scrollOptions));
            this.scrollContent.scrollbar('bind', 'onScroll', this.onScroll, this);
        } else {
            this.scrollContent = null;
        }
    },

    repaintScroll: function (arg) {
        this.ensureScroll();
        var $scrollPane = this.findScrollbarPane();
        this.scrollPaneHeight = $scrollPane && $scrollPane.height();
        if (this.scrollContent) {
            this.scrollContent.scrollbar('repaint', arg);
            this.checkScrollVisibility();
        }
    },

    repaintScrollIfScrollPaneChanged: function (arg) {
        var $scrollPane = this.findScrollbarPane();
        if ($scrollPane && $scrollPane.height() !== this.scrollPaneHeight) {
            this.repaintScroll(arg);
        }
    },

    renderPager: function () {
        DefaultListView.prototype.renderPager.call(this);
        this.repaintScroll();
    },

    onPageLoaded: function () {
        DefaultListView.prototype.onPageLoaded.call(this);
        this.repaintScroll();
    },

    checkScrollVisibility: function () {
        var height = $(this.el, this.scrollContent).height();
        if (height > this.scrollContent.height()) {
            $('.scrollbar-handle-container', this.scrollContent).removeClass('word-hidden');
        } else {
            $('.scrollbar-handle-container', this.scrollContent).addClass('word-hidden');
        }
    },

    onDispose: function () {
        this.scrollContent = null;
    },

    scrollTo: function (direction) {
        this.repaintScroll();
        this.scrollContent.scrollbar('scrollto', direction);

    },

    scrollToMakeViewVisible: function (view) {
        this.repaintScroll();
        var $pane = this.scrollContent.find(".scrollbar-pane");
        if (!$pane.length) {
            return;
        }
        var contentTop = this.scrollContent.offset().top;
        var viewTop = view.$el.offset().top;
        var contentBottom = contentTop + this.scrollContent.height();
        var viewBottom = viewTop + view.$el.outerHeight(true);
        if (viewBottom > contentBottom) {
            this.scrollContent.scrollbar('scrollto', 'diff', viewBottom - contentBottom);
        } else if (viewTop < contentTop) {
            this.scrollContent.scrollbar('scrollto', 'diff', viewTop - contentTop);
        }
    },

    isViewUnderBottom: function (view) {
        var contentBottom = this.scrollContent.offset().top + this.scrollContent.height();
        var viewBottom = view.$el.offset().top + view.$el.outerHeight(true);
        return viewBottom > contentBottom;
    },

    isViewVisible: function (view) {
        return !this.isViewOverTop(view) && !this.isViewUnderBottom(view);
    },

    isViewOverTop: function (view) {
        var contentTop = this.scrollContent.offset().top;
        var viewTop = view.$el.offset().top;
        var viewHeight = view.$el.outerHeight(true);
        return viewTop + viewHeight / 2 < contentTop;
    },

    onScroll: function (extremePosition) {
        this.repaintScrollIfScrollPaneChanged();
        if (this.progressiveLoad && extremePosition) {
            if (((this.sortDirection === 'asc') && (extremePosition === 'bottom')) || ((this.sortDirection !== 'asc') && (extremePosition === 'top'))) {
                if (!(this.collection && this.collection.allLoaded)) {
                    this.loadNext();
                }
            }
        }
    }
});


/*global Video, swfobject, Modal, Likes, ProfileUtils*/

/*jslint regexp:true*/
Video.Views = {
    /**
     * Shows modal dialog with video and comments
     * @param profileAlias
     * @param videoId
     */
    showVideo: function (profileAlias, videoId, self, options) {
        options = options || {};
        videoId = parseFloat(videoId) || videoId;
        workspace.appModel.get('videoManager').pauseOldVideo();
        var model = new Video.Models.VideoPlayer({
            profileId: profileAlias,
            videoId: videoId,
            videoWidth: 720,
            videoHeight: 405

        });
        if (!$.browser.msie) {
            StorageUtils.setVideoPlayerState({tagName: window.name});
            $(window).bind('storage', function (e) {
                var event = e.originalEvent;
                if (event.key == 'video.player.state') {
                    workspace.appModel.get('videoManager').pauseOldVideo();
                }
            });
        }

        Modal.showDialogByViewClass(Video.Views.ModalVideoPlayer, model, options.error, null, null, function () {
            if (options.changePageData) {
                workspace.appView.changePageData(model);
            }
        });
    },

    showVideoPlayer: function (profileId, videoId, element, options) {
        var settings = $.extend({
            width: $(element).width(),
            height: $(element).height(),
            autostart: true
        }, options || {});
        this._showVideoPlayer(profileId, videoId, element, settings);
    },

    /**
     * Embeds video player instead of specified element
     * Params passed in options override defaults
     * @param profileId
     * @param videoId
     * @param element
     * @param options
     */
    _showVideoPlayer: function (profileId, videoId, element, settings) {
        var model = new Video.Models.VideoPlayer({
            profileId: profileId,
            videoId: videoId,
            videoWidth: settings.width,
            videoHeight: settings.height,
            autostart: settings.autostart,
            fromCampaign: settings.fromCampaign
        });
        var $element = $(element);
        $element.data('model', model);

        var loadAndShow = function () {
            model.fetch({
                complete: function () {
                    var view = new Video.Views.EmbeddedVideoPlayer({
                        model: model
                    });
                    if ($.browser.msie) {
                        StorageUtils.setVideoPlayerState({tagName: window.name});

                        if (!settings.ignoreStorageEvents) {
                            $(window).bind('storage', function (e) {
                                var event = e.originalEvent;
                                if (event.key == 'video.player.state') {
                                    workspace.appModel.get('videoManager').pauseOldVideo();
                                }
                            });
                        }
                    }

                    if (!settings.ignoreVideoManager) {
                        workspace.appModel.get('videoManager').showVideo(view, element);
                    }

                    $element.html('');
                    $element.append(view.el);
                    view.render();
                    $element.parent('.bb-video-player-cover').find('.bb-video-time,.bb-video-icon').hide();
                }
            });
        };

        if (settings.fromCampaign && settings.imageUrl) {
            model.set('imageUrl', settings.imageUrl);
            model.set('isYouTube', !settings.imageId);
            var view = new Video.Views.StubbedVideoPlayer({
                model: model,
                callback: loadAndShow
            });
            $element.html('');
            $element.append(view.el);
            view.render();
            return;
        } else {
            loadAndShow();
        }

        App.Mixins.Media.audioPlayerPause();

    },
    /**
     * @param element - use this
     * @param url - url of external video
     * @see VideoService for server part
     *
     * for XSS security reasons it is better to
     * use it only like showExternal('http://...', this)
     * */
    showExternal: function (url, element) {
        if (element) {
            this.showEmbeddedExternal(url, element);
        } else {
            this.showModalExternal(url);
        }
    },

    showAttachedLinkVideo: function (url, profileId, displayName, avatarUrl) {
        this.showModalExternalVideo(url, profileId, displayName, avatarUrl);
    },

    showModalExternalVideo: function (url, profileId, displayName, imageUrl) {
        this.showModalExternal(url, profileId, displayName, imageUrl);
    },

    /**
     * @param url - url of external video
     * @param element - div to play video in
     * */
    showEmbeddedExternal: function (url, element) {
        var options = {
            url: url,
            videoWidth: 400,
            videoHeight: 300
        };
        this._showEmbeddedExternal(element, options);
    },

    showEmbeddedExternalBig: function (url, element) {
        var options = {
            url: url,
            videoWidth: 640,
            videoHeight: 480
        };
        this._showEmbeddedExternal(element, options);
    },

    _showEmbeddedExternal: function (element, options) {
        App.Mixins.Media.audioPlayerPause();

        var model = new Video.Models.VideoPlayer(options);
        $(element).data('model', model);
        model.fetch({
            success: function () {
                var view = new Video.Views.EmbeddedVideoPlayer({
                    model: model
                });
                workspace.appModel.get('videoManager').showVideo(view, element);
                $(element).html('');
                $(element).append(view.el);
                view.render();
                $(element).parent('.bb-video-player-cover').find('.bb-video-time,.bb-video-icon').hide();
            }
        });
    },

    showModalExternal: function (url, profileId, displayName, imageUrl) {
        workspace.appModel.get('videoManager').pauseOldVideo();
        var options = {
            url: url,
            videoWidth: 720,
            videoHeight: 405
        };

        var model = new Video.Models.VideoPlayer(options);
        if (profileId && displayName && imageUrl) {
            model.set({
                attacherName: displayName,
                attacherProfileId: profileId,
                attacherImageUrl: imageUrl
            });
        }
        Modal.showDialogByViewClass(Video.Views.ModalVideoPlayer, model);
    }

};

/**
 * This player is used for embedded video player
 */
Video.Views.EmbeddedVideoPlayer = BaseView.extend({
    modelEvents: {
        'destroy': 'dispose'
    },

    template: '#video-player-template',
    construct: function () {
        var audioPlayer = workspace.appModel.audioPlayer;
        if (audioPlayer && audioPlayer.isPlaying()) {
            audioPlayer.toggleInterrupted();
        }
        workspace.appModel.get('videoManager').interruptAudio();
        workspace.appModel.get('videoManager').increaseVideosCount();
        // now traked in iframe
        // workspace.stats.trackVideoView(this.model);

    },

    dispose: function () {
        var audioPlayer = workspace.appModel.audioPlayer;
        if (audioPlayer && !audioPlayer.isPlaying() && audioPlayer.isInterrupted()) {
            audioPlayer.toggleInterrupted();
        }

        workspace.appModel.get('videoManager').resumeAudio();
        workspace.appModel.get('videoManager').decreaseVideosCount();
        BaseView.prototype.dispose.call(this);
    }
});

Video.Views.ModalVideoPlayer = Modal.View.extend({
    template: '#video-view-template',
    className: '',

    modelEvents: {
        'destroy': 'dispose'
    },

    construct: function () {
        this.insertMetaRobots();

        this.addChildAtElement('.video-player', new Video.Views.EmbeddedVideoPlayer({
            model: this.model
        }));
        var self = this;

        if (!$.browser.msie) {
            $(window).bind('storage', function (e) {
                var event = e.originalEvent;
                if (event.key === 'video.player.state') {
                    self.dispose();
                }
            });

        }

    },

    afterRender: function () {
        var videoType = this.model.get('videoType');
        var qualityModesUrls = this.model.get('qualityModesUrls');
//        var userInfo = $().html();
        if (videoType === 'PLANETA' && (!qualityModesUrls || _.isEmpty(qualityModesUrls))) {
            workspace.appView.showInfoMessage('Видео обрабатывается');
            Modal.closeModal();
        } else {
            Modal.View.prototype.afterRender.call(this);
        }
    },

    insertMetaRobots: function () {
        this.$metaRobots = $('<meta name="robots" content="noindex, nofollow"/>');
        $('head').prepend(this.$metaRobots);
    },

    dispose: function () {
        if (this.$metaRobots) {
            this.$metaRobots.remove();
        }
        Modal.View.prototype.dispose.apply(this, arguments);
    }
});


Video.Views.StubbedVideoPlayer = BaseView.extend({
    template: '#stubbed-video-player-template',
    events: {
        'click .video-stub': 'showPlayer'
    },

    construct: function (options) {
        this.callback = options.callback;
    },
    showPlayer: function () {
        var closest = this.$el.closest('.central-block');
        closest.height('100%').width('100%');
        this.dispose();
        this.model.set({autostart: true}, {silent: true});
        this.callback();
    }
});

/*global Audios*/
(function () {
    var appModel = workspace.appModel;
    appModel.audioPlayer = new Audios.Models.Player({
        controller: appModel,
        profileId: appModel.myProfileId()
    });
}());
/*globals Dialogs,dialogsPageController,DialogsUpdater,BaseModel,BaseView,workspace,Attach*/
/*********************************************************************************************
 *
 * Dialog views and models. Used for rendering your dialog box.
 *
 *********************************************************************************************/
Dialogs.Models.DialogBox = BaseModel.extend({

    initialize: function (options) {
        BaseModel.prototype.initialize.call(this, options);
        this.dialogsController = workspace.appModel.get('dialogsController');
    },

    pageData: function () {
        var pageTitle = 'Диалоги | Planeta';
        if (this.dialogsController.has('activeDialog')) {
            pageTitle = 'Диалог с пользователем ' + this.dialogsController.get('activeDialog').get('name');
        }

        return {
            title: pageTitle,
            description: 'Диалоги и личные сообщения. Для отправки сообщений авторизуйтесь или зарегистрируйтесь на Planeta.ru'
        };
    },

    fetch: function (options) {
        var dialogId = this.get('dialogId');
        if (!dialogId) {
            var activeDialog = this.dialogsController.get('activeDialog');
            if (activeDialog) {
                dialogId = activeDialog.id;
            } else {
                workspace.appView.showErrorMessage("При загрузке диалога произошла ошибка");
            }
        }
        var self = this;
        this.dialogsController.loadDialog(_.defaults({
            dialogId: dialogId,
            success: function (dialog) {
                self.set({
                    dialog: dialog
                });
                self.dialogsController.set({
                    activeDialog: dialog
                });

                if (options && options.success) {
                    options.success();
                }
            }
        }, options));
    },

    /**
     * Closes specified dialog
     */
    closeDialog: function (dialog) {
        var activeDialog = this.dialogsController.removeDialog(dialog);
        if (activeDialog) {
            workspace.navigate('/dialogs/' + activeDialog.id, true);
        } else {
            workspace.navigate('/dialogs', true);
        }
    }
});

Dialogs.Views.DialogBox = BaseView.extend({
    template: "#dialog-box-template",

    construct: function (options) {
        if (!(this.model instanceof Dialogs.Models.DialogBox)) {
            this.model = this.model.dialogBox;
        }

        var dialog = this.model.get('dialog');

        this.isFloat = !!this.model.get("isFloat");

        dialog.prepareMessages();

        this.contentPanel = new Dialogs.Views.DialogContentPanel({
            model: dialog,
            isFloat: this.isFloat
        });
        this.addChildAtElement(".dialog-content-panel", this.contentPanel, true);
        this.footerPanel = new Dialogs.Views.FooterPanel({
            model: dialog,
            isFloat: this.isFloat
        });

        this.addChildAtElement(".dialog-footer-panel", this.footerPanel, true);

        if (!this.isFloat) {
            dialogsPageController.addListener(this, this.contentPanel);
        }
        this.footerPanel.on("resizeTextarea", this.resizeTextarea, this);

        this.footerPanel.on("onKeyup", function () {
            this.contentPanel.messagesList.onUserActivity();
        }, this);

        var messages = dialog.get("messages");

        messages.isFloat = this.isFloat;
        messages.on("unreadMessagesChanged", this.model.dialogsController.onUnreadMessagesChanged, this.model.dialogsController);
        messages.on("newMessage", this.model.dialogsController.onDialogActivity, this.model.dialogsController);
    },

    onDispose: function () {
        DialogsUpdater.unbindFromUpdater(this.model.get('dialog'));
        this.model.get('dialog').get("messages").readAllMessagesImmediately();
    },

    focus: function () {
        this.footerPanel.focus();
    },
    resizeTextarea: function ($textarea) {
        var contentPanelHeight = this.contentPanel.$el.outerHeight(true);
        var textareaScrollHeight = $textarea.get(0).scrollHeight;
        var textareaHeight = $textarea.height();
        if (!this.minTextareaHeight) {
            this.minTextareaHeight = textareaHeight;
            this.diffBetweenHeightAndScrollHeight = textareaHeight - textareaScrollHeight;
        }
        textareaScrollHeight += this.diffBetweenHeightAndScrollHeight;
        if (textareaScrollHeight <= this.minTextareaHeight && textareaScrollHeight > textareaHeight) {
            return;
        }
        if (textareaScrollHeight == textareaHeight) {
            $textarea.height(this.minTextareaHeight);
            textareaScrollHeight = $textarea.get(0).scrollHeight + this.diffBetweenHeightAndScrollHeight;
        }
        $textarea.height(textareaScrollHeight);

        if (textareaScrollHeight - textareaHeight > contentPanelHeight) {
            textareaScrollHeight = contentPanelHeight + textareaHeight;
            $textarea.height(textareaScrollHeight);
        }
        var maxHeight = parseInt($textarea.css('max-height'), 10);
        if (maxHeight && maxHeight < textareaScrollHeight) {
            textareaScrollHeight = maxHeight;
        }

        this.contentPanel.resizeHeightBy(textareaHeight - textareaScrollHeight);
    }
});

Dialogs.Views.DialogContentPanel = BaseView.extend({
    className: 'dlg-content',
    template: '#dialog-content-template',
    events: {
        'click .btn-load-previous-messages': 'loadPrevious'
    },
    modelEvents: {
        'destroy': 'dispose'
    },
    construct: function (options) {
        this.messagesList = this.createChildView(Dialogs.Views.MessagesList, {
            collection: this.model.get('messages'),
            isFloat: !!options.isFloat
        });
        this.bind('onDeleteMessageClicked', this.deleteMessage, this);
    },
    loadPrevious: function (e) {
        e.preventDefault();
        // Loading previous messages
        this.model.loadNextMessages('top');
    },

    deleteMessage: function (view) {
        this.model.deleteMessage(view.model);
    },
    resizeHeightBy: function (delta) {
        this.messagesList.resizeHeightBy(delta);
    }
});

Dialogs.Views.FooterPanel = BaseView.extend({
    className: 'dlg-post',
    template: '#dialog-footer-template',

    construct: function (options) {
        _.bindAll(this);
        this.isFloat = !!(options && options.isFloat);
    },

    modelEvents: {
        'destroy': 'dispose'
        //to prevent textarea from losing focus  don't render view when model has been changed
    },

    events: {
        'click .send-wall-message': 'sendMessageBtnClicked',
        'keypress #wall-textarea': 'onKeypressed',
        'keyup #wall-textarea': 'onKeyup',
        'click .dropdown': 'toggleDropdownClicked',
        'click .dropdown a': 'closeDropdownClicked'
    },

    modelJSON: function () {
        return _.extend(this.model.toJSON(), {
            myImageUrl: workspace.appModel.get('myProfile').get('smallImageUrl'),
            isFloat: this.isFloat
        });
    },

    toggleDropdownClicked: function (e) {
        e.preventDefault();

        var el = $(e.currentTarget);
        if (el.is('.open')) {
            this.closeDropdown(el);
        } else {
            $(document).click();
            var self = this;
            var clickOff = function () {
                self.closeDropdown(el);
                $(document).unbind('click', clickOff);
            };
            $(document).bind('click', clickOff);

            var btn = $('.action-btn', el);
            btn.addClass('active');
            $('i', btn).addClass('icon-white');
            el.addClass('open');
        }
        e.stopPropagation();
    },

    closeDropdownClicked: function (e) {
        e.preventDefault();
        var el = $(e.currentTarget).closest('.dropdown');
        this.closeDropdown(el);
    },

    closeDropdown: function (el) {
        var btn = $('.action-btn', el);
        btn.removeClass('active');
        $('i', btn).removeClass('icon-white');
        el.removeClass('open');
    },

    sendMessageBtnClicked: function (e) {
        e.preventDefault();
        this.sendMessage();
    },

    messageTextOnKeyEnter: function () {
        var textarea = this.$el.find("#wall-textarea").get(0);
        var val = textarea.value;
        if (typeof textarea.selectionStart == "number" && typeof textarea.selectionEnd == "number") {
            var start = textarea.selectionStart;
            textarea.value = val.slice(0, start) + "\n" + val.slice(textarea.selectionEnd);
            textarea.selectionStart = textarea.selectionEnd = start + 1;
        } else if (document.selection && document.selection.createRange) {
            textarea.focus();
            var range = document.selection.createRange();
            range.text = "\r\n";
            range.collapse(false);
            range.select();
        }
    },

    onKeypressed: function (e) {
        if ((e.keyCode == 13 || e.keyCode == 10)) {
            if (this.isFloat) {
                if (e.ctrlKey) {
                    this.messageTextOnKeyEnter();
                } else {
                    e.preventDefault();
                    this.sendMessage();
                }
            } else {
                if (e.ctrlKey) {
                    e.preventDefault();
                    this.sendMessage();
                }
            }
        }
        this.resizeTextarea();
    },
    onKeyup: function () {
        this.resizeTextarea();
        Backbone.Events.trigger.call(this, 'onKeyup');
    },

    resizeTextarea: function () {
        Backbone.Events.trigger.call(this, "resizeTextarea", this.$el.find("#wall-textarea"));
    },

    afterRender: function () {
        this.resizeTextarea();
    },

    sendMessage: function () {
        var messageTextEl = $(this.el).find('#wall-textarea');
        var messageText = $.trim(messageTextEl.val());

        if (messageText) {
            messageTextEl.val('');
            this.resizeTextarea();
            this.model.sendMessage(messageText);
        }
    },

    focus: function () {
        this.$el.find("#wall-textarea").focus();
    }
});




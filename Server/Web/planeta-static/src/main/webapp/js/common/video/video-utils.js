VideoUtils = {

    /**
     * @param {string} html - stub html for target video type
     * @param {string} type  - video provide type @see OembedProviderName
     * @return {string} resized html object
     * */
    resize: function (html, type, width, height, autoplay) {
        if (html && typeof autoplay != 'undefined' && autoplay == false) {
            html = html.replace(/(&|&amp;)?autoplay=1/ig, '');
        }
        var obj = $('<div/>').html(html);
        obj.find('object, embed, iframe').attr('width', width);
        obj.find('object, embed, iframe').attr('height', height);

        if (obj.find('iframe').length > 0) {
            return obj.html();
        }
        //************************************************
        //stupid IE cannot jquery-selectCampaignById inside object tag
        html = obj.html();
        var t = $('<div/>').html(html).find('object');
        var tt = $('<div/>').html(t.html());
        tt.find('embed').attr('width', width);
        tt.find('embed').attr('height', height);
        var res = $('<div />').html('<object>' + tt.html() + '</object>');
        //************************************************
        return res.html();
    },

    setSize: function (element, type, html) {
        var height, width;
        if (element) {
            height = $(element).height();
            width = $(element).width();
        } else {
            height = 405;
            width = 720;
        }
        return this.resize(html, type, width, height);

    },

    getCurrentFormat: function (iframe, type) {
        var height, width;
        if (type) {
            height = 300;
            width = 400;
        } else {
            height = 405;
            width = 720;
        }
        var element = $('<div/>').html(iframe);
        element.find("iframe").attr("width", width);
        element.find("iframe").attr("height", height);
        return element.html();
    }
};
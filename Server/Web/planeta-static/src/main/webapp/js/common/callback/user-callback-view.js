/*globals UserCallback*/
UserCallback.Views.BaseView = BaseView.extend({
    events: {
        'change .js-phone': 'onPhoneChanged',
        'keyup .js-phone': 'onPhoneChanged',
        'click .js-callback:not(.disabled)': 'onCallbackClicked',
        'click .fic-mistake-a': 'onFeedbackClicked'
    },

    afterRender: function () {
        var phoneNumber = (workspace.appModel.get('myProfile').get('phoneNumber') || "").replace(/\D/g, '');

        if (phoneNumber.length === 10) {
            this.$el.find('.js-phone').mask('+7 (999) 999-99-99').val(phoneNumber);
            this.$el.find('.js-phone').focus();
            this.$el.find('.js-callback').toggleClass('disabled');
        } else {
            this.$el.find('.js-phone').mask('+7 (999) 999-99-99').val(null);
        }

        if (phoneNumber.length === 11) {
            this.$el.find('.js-phone').mask('+7 (999) 999-99-99').val(phoneNumber.substring(1));
            this.$el.find('.js-phone').focus();
            this.$el.find('.js-callback').toggleClass('disabled');
        } else {
            this.$el.find('.js-phone').mask('+7 (999) 999-99-99').val(null);
        }
    },


    onFeedbackClicked: function (e) {
        if (e) e.preventDefault();
        FeedbackHelper.showFeedBackDialog({theme: 'payment'});
    },

    onPhoneChanged: function (e) {
        var $el = $(e.currentTarget);
        var phone = $el.val();

        this.model.setPhone(phone);
        this.$el.find('.js-callback').toggleClass('disabled', !this.model.isReady());
    },

    onCallbackClicked: function () {
        var self = this;
        return this.model.send().done(function () {
            //workspace.appView.showSuccessMessage("с Вами свяжутся в течении 5 минут.");
            self.$el.find(".js-callback-state1").addClass("hidden");
            self.$el.find(".js-callback-state2").removeClass("hidden");
        }).promise();
    }
});
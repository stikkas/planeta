/*globals moduleLoader*/
if (!window.UploadController) {
    var UploadController = {};
}
_.extend(UploadController, {
    exec: function (funcName) {
        var args = Array.prototype.slice.call(arguments, 1);
        return moduleLoader.loadModule('upload').done(function () {
            UploadController[funcName].apply(UploadController, args);
        });
    }
});
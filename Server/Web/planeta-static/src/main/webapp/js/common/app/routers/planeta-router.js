/*global App,AppView,BaseRouter,NavigationState,counters,Dialogs,StringUtils*/
/**
 * Application controller / router
 */
var PlanetaRouter = BaseRouter.extend(_.extend({}, App.Mixins.Media, {
    initialize: function (options) {
        this.appModel = options.appModel;
        this.appView = new AppView({
            model: this.appModel
        });
        if (window.NavigationState) {
            this.navigationMap = options.navigationMap;
            this.navigationState = new NavigationState();
        }

        // this.appView.render();
    },

    routes: {
        'logout': 'logout',
        'stories/*': 'openStories',
        'about': 'openAbout',
        'contacts': 'openContacts',
        'advertising': 'openAdvertising',
        'logo': 'openLogo',
        'partners': 'openPartners',
        'interactive': 'openInteractive',
        'interactive/:section': 'openInteractive',
        'quiz/:quizAlias?*params': 'openQuiz',
        'quiz/:quizAlias': 'openQuiz',
        'welcome/confirm-recover.html*params': 'showWelcomeRecover',
        'welcome/account-merge.html*params': 'showWelcome',
        '?*params': 'showWelcome',
        '': 'showWelcome',
        'search/:section?*objectId': 'openSearch',
        '*path?*params': 'navigateWithRemovedQuery',
        'settings': 'openSettings',
        'settings/:subsection': 'openSettings',
        'search': 'openSearch',
        'search/:section': 'openSearch',
        'search/:section/:objectId': 'openSearch',
        'faq/:subsection/:objectId!:anchor': 'openFAQ',
        'faq/:subsection/:objectId': 'openFAQ',
        'faq/:subsection': 'openFAQ',
        'faq': 'openFAQ',
        'delivery-payment': 'openDeliveryPayment',
        'dialogs': 'openDialogs',
        'dialogs/:objectid': 'openDialogs',
        // 'account/:section!:anchor': 'openAccountSection',
        // 'account/:section': 'openAccountSection',
        // 'account!:anchor': 'openAccount',
        // 'account': 'openAccount',
        'news': 'openNews',
        'news/:subsection': 'openNews',
        'news/tag/:tagId': 'openNews',
        'photo/:profileId/:photoId': 'showPhotoRouter',
        'video/:profileId/:videoId': 'showVideoRouter',
        'campaigns/:campaignAlias!:anchor': 'openCampaignAnchor',
        'campaigns/:campaignAlias': 'openCampaign',
        'campaigns/:campaignAlias/comments!:anchor': 'openCampaignComments',
        'campaigns/:campaignAlias/updates!:anchor': 'openCampaignUpdates',
        'campaigns/:campaignAlias/:action': 'openCampaign',
        'campaigns/:campaignAlias/donate/:shareId': 'openCampaignDonate',
        'campaigns/:campaignAlias/donatesingle/:shareId': 'openCampaignSingleDonate',
        'campaigns/:campaignAlias/donate/:shareId/:action': 'openCampaignDonate',
        'funding-rules': 'openFundingRules',
        ':id!:anchor': 'openProfileAnchor',
        ':id': 'openProfile',
        ':profileAlias/blog/:blogId': 'redirectToNewsBlog',
        ':profileAlias/fanblog/:blogId': 'redirectToNewsFanblog',
        ':profileAlias/posts/:blogId': 'redirectToNewsPosts',
        ':id/settings': 'openSettingsById',
        ':id/campaigns': 'openCampaignsById',
        ':id/:section!:anchor': 'openSectionAnchor',
        ':id/:section': 'openProfile',
        ':id/settings/:subsection': 'openSettingsById',
        ':id/:section/:objectId!:anchor': 'openProfileAnchor',
        ':id/:section/:objectId': 'openProfile',
        ':id/:section/:objectId/:subsection': 'openProfile',
        ':id/:section/:$2/:$3/:$4/:$5': 'changeRoute',
        '*path': 'routeDoesNotExists'
    },

    logout: function () {
        this.openImmediately('logout');
    },

    openStories: function () {
        this.openImmediately('stories/');
    },

    openAbout: function () {
        this.openImmediately('about');
    },

    openContacts: function () {
        this.openImmediately('contacts');
    },

    openAdvertising: function () {
        this.openImmediately('advertising');
    },

    openLogo: function () {
        this.openImmediately('logo');
    },

    openPartners: function () {
        this.openImmediately('partners');
    },

    openInteractive: function (subsection) {
        this.navigationState.set({
            id: this.appModel.myProfileId(),
            section: 'interactive',
            objectId: null,
            subsection: subsection || 'start'
        });
        this.doNavigate();
    },

    openQuiz: function (quizAlias) {
        this.navigationState.set({
            id: this.appModel.myProfileId(),
            section: 'quiz',
            objectId: quizAlias,
            subsection: null
        });
        this.doNavigate('quiz');
    },

    navigateWithRemovedQuery: function (path, params) {
        if (window.counters) {
            counters.urlQuery = params;
        }
        this.removedUrlQuery = workspace.urlQuery = params;
        this.navigate(path, {replace: true});
    },

    showWelcome: function () {
        var profileId = this.appModel.myProfileId();
        this.changeRoute(profileId, 'welcome', null, '', '');
    },

    showWelcomeRecover: function () {
        var profileId = this.appModel.myProfileId();
        this.changeRoute(profileId, 'welcome', null, '', 'passwordRecover');
    },

    openProfile: function (id, section, objectId, subsection, anchor) {
        this.changeRoute(id, section, objectId, subsection, anchor);
    },

    openProfileAnchor: function (id, section, objectId, anchor) {
        if (anchor) {
            this.openProfile(id, section, objectId, null, anchor);
        } else {
            this.openProfile(id, null, null, null, section);
        }
    },

    openSectionAnchor: function (id, section, anchor) {
        this.openProfile(id, section, null, null, anchor);
    },

    openSettings: function (subsection) {
        var profileId = this.appModel.myProfileId();
        var section = 'settings';
        this.openProfile(profileId, section, null, subsection || 'general');
    },

    /**
     * Parsing routes for groups and events settings
     * @param id
     * @param subsection
     */
    openSettingsById: function (id, subsection) {
        var profileId = id || this.appModel.myProfileId();
        var section = 'settings';
        this.openProfile(profileId, section, null, subsection || 'general');
    },

    openCampaignsById: function (id) {
        this.navigate('/' + (id || this.appModel.myProfileId()));
    },

    _isItTheSameSectionAndSubsection: function (section, subsection) {
        var prevSection = this.navigationState.get('section');
        var prevSubsection = this.navigationState.get('subsection');
        return prevSection == section && prevSubsection == subsection;
    },

    openSearch: function (subsection, objectId) {
        var changeQuery = false;
        if (this._isItTheSameSectionAndSubsection('search', subsection)) {
            changeQuery = true;
        }
        this.navigationState.set({
            id: this.appModel.myProfileId(),
            section: 'search',
            objectId: objectId || null,
            subsection: subsection || 'users'
        });
        if (changeQuery) {
            if (window.gtm) {
                if(workspace) {
                    workspace.campaignListTrackedFlag = true;
                }
                window.gtm.trackUniversalEvent('pageView');
            }
            this.appModel.get('contentModel').changeQuery(objectId);
        } else {
            this.doNavigate();
        }
    },

    openDialogs: function (objectId) {
        var self = this;
        var profileId = this.appModel.myProfileId();
        var dialogsController = workspace.appModel.get('dialogsController');
        this.redirectToProfileById(profileId);
        dialogsController.showDialogsSearch();
    },


    openAccount: function (anchor) {
        this.openAccountSection('purchases', anchor);
    },

    openAccountSection: function (section, anchor) {
        var profileId = this.appModel.myProfileId();
        this.openProfile(profileId, 'account', null, section, anchor);
    },

    openCampaignCreate: function () {
        var profileId = this.appModel.myProfileId();
        this.changeRoute(profileId, 'campaigns', 'create');
    },

    openCampaignSingleDonate: function (campaignAlias, shareId, action) {
        this.openCampaign(campaignAlias, 'donatesingle', shareId, action);
    },

    openCampaignDonate: function (campaignAlias, shareId, action) {
        this.openCampaign(campaignAlias, 'donate', shareId, action);
    },

    openCampaign: function (campaignAlias, subsection, shareId, action) {
        this.changeRoute(0, "campaigns", campaignAlias, subsection, shareId, action);
    },

    openCampaignAnchor: function (campaignAlias, anchor) {
        this.openCampaign(campaignAlias, null, anchor, null);
    },

    openCampaignComments: function (campaignAlias, anchor) {
        this.openCampaign(campaignAlias, 'comments', anchor, null);
    },

    openCampaignUpdates: function (campaignAlias, anchor) {
        this.openCampaign(campaignAlias, 'updates', anchor, null);
    },

    openNews: function (subsection) {
        if(this.appModel.isAuthorized()){
            this.navigate("/" + this.appModel.myProfileId() + "/news" + (subsection ? "/" + subsection : ""), {trigger: false, replace: true});
        }
        var profileId = this.appModel.myProfileId();
        this.openProfile(profileId, 'news');
    },
    openDeliveryPayment: function () {
        this.changeRoute(this.appModel.myProfileId(), 'delivery-payment');
    },

    openFundingRules: function () {
        this.changeRoute(this.appModel.myProfileId(), 'funding-rules');
    },

    showPhotoRouter: function (profileId, photoId) {
        this.showPhoto(profileId, photoId, null, {
            error: function () {
                workspace.appView.showErrorMessage("Фотография не найдена");
                workspace.navigate('/');
            },
            changePageData: true
        });
    },

    showVideoRouter: function (profileId, videoId) {
        this.showVideo(profileId, videoId, null, {
            error: function () {
                workspace.appView.showErrorMessage("Видео не найдено");
                workspace.navigate('/');
            },
            changePageData: true
        });
    },

    openFAQ: function (subsection, objectId, anchor) {
        var profileId = this.appModel.myProfileId();
        this.openProfile(profileId, 'faq', objectId, subsection, anchor);
    },

    redirectToProfile: function (section) {
        var profileId = section || this.appModel.myProfileId();
        this.redirectToProfileById(profileId);
    },

    redirectToProfileById: function (id) {
        this.navigate("/" + id, {trigger: false, replace: true});
        var profileId = id || this.appModel.myProfileId();
        this.openProfile(profileId);
    },

    redirectToNewsBlog: function (profileAlias, id) {
        window.location.href = "/" + profileAlias + "/blog/" + id;
    },
    redirectToNewsFanblog: function (profileAlias, id) {
        window.location.href = "/" + profileAlias + "/fanblog/" + id;
    },
    redirectToNewsPosts: function (profileAlias, id) {
        window.location.href = "/" + profileAlias + "/posts/" + id;
    }
}));
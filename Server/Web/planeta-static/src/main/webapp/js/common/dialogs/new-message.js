/*globals Async, Modal, Form, workspace */
/**
 * Namespaces
 */

var Dialogs = {
    Models: {},
    Views: {}
};

/*********************************************************************************************
 *
 * Model and view of "New message" dialog window
 *
 *********************************************************************************************/
Dialogs.Models.NewMessage = BaseModel.extend({
    MAX_MESSAGE_SIZE: 256,
    ASYNC_QUEUE_NAME: 'dialog-send-message-to-users-dialog-async-queue',
    defaults: {
        profileIds: []
    },
    /**
     * Sends message to the specified user (profileId).
     */
    sendMessage: function (data, success) {
        var messageText = data.text;
        while (messageText.length > 0) {
            if (messageText.length <= this.MAX_MESSAGE_SIZE) {
                this._sendMessage(data, messageText, success);
                return;
            }
            var i = messageText.lastIndexOf('/n', this.MAX_MESSAGE_SIZE - 1);
            if (i < 0) {
                i = messageText.lastIndexOf(' ', this.MAX_MESSAGE_SIZE - 1);
            }
            if (i < 0) {
                i = this.MAX_MESSAGE_SIZE - 1;
            }
            this._sendMessage(data, messageText.substr(0, i + 1));
            messageText = messageText.substr(i + 1);
        }

        var self = this;
        Async.append(function () {
            setTimeout(function () {
                self.destroy();
            }, 1000);
        }, this.ASYNC_QUEUE_NAME);
    },
    _sendMessage: function (data, messageText, success) {
        var self = this;
        Async.append(function () {
            var dfd = this;
            data.text = messageText;
            var options = {
                url: '//' + workspace.serviceUrls.imServiceUrl + '/dialog-send-message-to-users.json',
                type: 'POST',
                dataType: 'jsonp',
                data: data,
                success: function (response, status) {
                    var result;
                    if (status && status === 'success') {
                        if (response.success === false) {
                            workspace.appView.showErrorMessage(response.errorMessage);
                            dfd.reject();
                            return;
                        }
                        result = response;
                    } else if (response.success) {
                        result = response.result;
                    } else {
                        dfd.reject();
                        workspace.appView.showErrorMessage(response.errorMessage);
                    }
                    if (result) {
                        if (success) {
                            success(response.result);
                        }
                        dfd.resolve();
                    }
                },
                error: function (ex) {
                    console.log(ex);
                    workspace.appView.showErrorMessage('Непредвиденная ошибка при отправке сообщения');
                    dfd.reject();
                    self.destroy();
                }
            };

            $.ajax(options);
        }, this.ASYNC_QUEUE_NAME);
    },
    saveForm: function (data, success) {
        this.sendMessage(data, success);
    }

});

Dialogs.Views.NewMessage = Form.View.extend({
    successMessage: 'Сообщение отправлено',
    template: '#message-form-template',
    className: 'modal modal-new-message',
    events: {
        'keypress #text': 'sendMessageOnEnter',
        'keyup #text': 'isValidInput',
        'click a.close': 'cancel',
        'click button[type=reset]': 'cancel',
        'click button[type=submit]': 'save'
    },
    afterRender: function () {
        Modal.View.prototype.afterRender.call(this);
        $('button[type=submit]', this.el).attr('disabled', 'disabled');
    },
    isValidInput: function () {
        if ($('#text', this.el).val() == '') {
            $('button[type=submit]', this.el).attr('disabled', 'disabled');
            return false;
        } else {
            $('button[type=submit]', this.el).removeAttr('disabled');
            return true;
        }
    },
    sendMessageOnEnter: function (e) {
        if (e.ctrlKey && (e.keyCode == 13 || e.keyCode == 10) && this.isValidInput()) {
            e.preventDefault();
            this.save(e);
        }
    },
    save: function (e) {
        e.preventDefault();
        if (this.isValidInput()) {
            $('button[type=submit]', this.el).attr('disabled', 'disabled');
            var form = $(this.el).find("form");
            var data = form.serializeObject();
            var self = this;
            this.dispose();
            this.model.saveForm(data, function () {
                workspace.appView.showSuccessMessage(self.successMessage);
            });
        }
    }

});
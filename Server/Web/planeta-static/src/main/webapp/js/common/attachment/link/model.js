/**
 * Models for attaching link
 */
Attach.Models.Link = Attach.Models.BaseModel.extend({
	getBbcode:function (url, text) {
        if(text === null) {
            text = url;
        }
		return '[url=' + url + ']' + text + '[/url]';
	},
	select:function (url) {
		var url = this.getBbcode(url),
		callback = this.get('selected');
		callback(url);
		this.destroy();
	},
	fetch:function (options) {
		if (options && options.success) {
			options.success();
		}
	}
});


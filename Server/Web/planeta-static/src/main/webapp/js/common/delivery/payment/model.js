CommonDonate.Models.DeliveryPaymentPage = CommonDonate.Models.PaymentAndDeliveryPage.extend({
    initialize: function () {
        CommonDonate.Models.PaymentAndDeliveryPage.prototype.initialize.apply(this, arguments);
        var amount = Number(CommonUtils.urlParam('amount')) || Number(SessionStorageProvider.get('delivery-cost'));
        if (amount) {
            SessionStorageProvider.set('delivery-cost', amount);
        }

        this.get('payment').set({
            orderType: 'DELIVERY',
            deliveryCost: amount
        });
        this.get('payment').setPrice(amount);
        this.set('shareStatus', 'ACTIVE');
    },

    restoreCommonFields: function () {
        CommonDonate.Models.PaymentAndDeliveryPage.prototype.restoreCommonFields.apply(this, arguments);
        this.set('orderType', "DELIVERY");
    },

    url: function () {

    }
});
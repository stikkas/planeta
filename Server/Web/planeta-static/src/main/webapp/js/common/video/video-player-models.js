/*global Video, Comments*/
var Video = {
    Models: {},
    Views: {}
};
Video.Models.VideoPlayer = BaseModel.extend({

    defaults: {
        profileId: null,
        videoId: null,
        url: null,
        autostart: true
    },

    initialize: function (options) {
        // This is external video model
        this.isExternalVideo = options.url != undefined;

        if (!this.isExternalVideo) {
            this.commentsModel = new Comments.Models.Comments({
                profileId: options.profileId,
                objectId: options.videoId,
                objectType: 'VIDEO'
            }, {
                commentableObject: this
            });
        }
    },

    url: function () {
        return this.isExternalVideo ?
            // This is external video, getting it's details by url
            '/api/public/video-external.json' :
            // This is user video, getting it's details by profileId and videoId
            '/api/public/video.json';
    },

    fetch: function (options) {

        var data = {};
        if (this.isExternalVideo) {
            data.url = this.get('url');
        } else {
            data.profileId = this.get('profileId');
            data.videoId = this.get('videoId');
        }

        var fetchOptions = _.extend({}, options, {
            data: data
        });
        // prevent empty url send
        if (this.isExternalVideo && !fetchOptions.url) {
            return;
        }
        return BaseModel.prototype.fetch.call(this, fetchOptions);
    },

    pageData: function () {
        return {
            title: this.get('name') +' | Planeta'
        };
    }
});
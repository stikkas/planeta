/*globals Header, moduleLoader, CookieProvider, StringUtils*/
var LazyHeader = {
    TIMEOUT: 3000,
    init: function (options) {
        this.handleMobile();
        this.handleSignUpRegistrationForms(options.auth);
        return this.loadAndRun("init", arguments, LazyHeader.TIMEOUT);
    },

    /**
     * Handles actions on sign up and registration forms on the page
     */
    handleSignUpRegistrationForms: function (authParameters) {
        var self = this;
        // Handle signup link click
        $('.header .signup-link, .js-signup-link').click(function (e) {
            e.preventDefault();
            e.stopPropagation();
            self.showAuthForm('signup');
            CookieProvider.set('gtmRegistration', 'planeta_header_success_registration', {path: '/', expires: 'Session'});
        });

        // Open registration link click
        $('.header .registration-link, .js-registration-link').click(function (e) {
            e.preventDefault();
            e.stopPropagation();
            self.showAuthForm('registration');
        });

        // Check if we have come to recover our password and remember auth info for router
        if (authParameters && authParameters.passwordRecovery) {
            this.authParameters = authParameters;
        }

        // Check error code and show corresponding error message
        if (authParameters && authParameters.errorCode) {
            var L10n = {
                _dictionary: {
                    "ru": {
                        "wrongLoginOrPassword": "Неверный логин или пароль",
                        "registrationCodeWrong": "Ваш код подтверждеиня недействителен",
                        "registrationCodeTimeExpired": "Время действия регистрационного кода истекло",
                        "registrationCodeNotWorkingMore" : "Ваш код подтверждения больше не действителен",
                        "unexpectedError" : "Непредвиденная ошибка. Пожалуйста, повторите попытку позже."
                    },
                    "en": {
                        "wrongLoginOrPassword": "Wrong login or password",
                        "registrationCodeWrong": "Your confirmation code is incorrect.",
                        "registrationCodeTimeExpired": "The time of registration code has expired",
                        "registrationCodeNotWorkingMore" : "Your confirmation code is not working more",
                        "unexpectedError" : "There is unexpected error. Please try later."
                    }
                }
            };

            var translate = function (word, lang) {
                if (!lang)
                    throw "language is empty.";
                return L10n._dictionary[lang][word] || word;
            };

            var lang = window.workspace && (workspace.currentLanguage === "en") ? "en" : "ru";
            var title;
            switch (authParameters.errorCode) {
                case '101':
                    if (workspace.isAuthorized) {
                        var redirectUrl = StringUtils.extractQueryStringParamValue(window.location.href, 'afterAuthorizationRedirectUrl');
                        if (redirectUrl == "") {
                            redirectUrl = '/';
                        }
                        window.location.href = redirectUrl;
                    } else {
                        title = translate('wrongLoginOrPassword', lang);
                        workspace.appView.showErrorMessage(title);
                    }
                    break;
                case 'registration.error.confirm.regcode.expired':
                case 'registration.current.regcode.expired':
                    title = translate('registrationCodeTimeExpired', lang);
                    workspace.appView.showErrorMessage(title);
                    break;
                case 'registration.current.regcode.not.exists':
                    title = translate('registrationCodeNotWorkingMore', lang);
                    workspace.appView.showErrorMessage(title);
                    break;
                case 'password.recovery.error':
                    title = translate('registrationCodeWrong', lang);
                    workspace.appView.showErrorMessage(title);
                    break;
                default:
                    title = translate('unexpectedError', lang);
                    workspace.appView.showErrorMessage(title);
                    break;
            }

            var pageData = function (title) {
                var tagTitle = '<title>' + title + '</title>';
                if ($('head>title').length > 0) {
                    $('head>title').html(title);
                } else {
                    $('head').append(tagTitle);
                }
            };
            if (title) {
                pageData(title);
            }
        }
    },

    handleMobile: function () {
        if (window.isMobileDev) {
            window.mobileCssLoadedDfd = TemplateManager.fetchCss("/css-generated/common-mobile.css", true);

            mobileCssLoadedDfd.done(function() {
                $('meta[name="viewport"]').attr('content', "width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0");
                $('#global-container').removeClass('hidden');
            });
        } else {
            $('#js-header-special-project').removeClass('hidden');
            $('#global-container').removeClass('hidden');
        }

        $('#header-exit, #m-header-exit').click(function (e) {
            var isEngLangOnPage = workspace && workspace.currentLanguage && workspace.currentLanguage === 'en';
            var title;
            var message;

            if(isEngLangOnPage) {
                message = "Are you sure you want to log out?";
                title = "Confirmation";
            } else {
                message = "Вы действительно хотите выйти?";
                title = "Подтверждение действия";
            }

            var exitUrl = '/logout?service=' + encodeURIComponent(document.location.href);

            e.preventDefault();
            if (Modal) {
                Modal.showConfirm(message, title, {
                    success: function () {
                        document.location = exitUrl;
                    }
                });
            } else {
                if (confirm(message)) {
                    document.location = exitUrl;
                }
            }
        });

        $('.pln-d-popup_right').click(function(){$('.h-user.pln-dropdown').removeClass('open');});
    },

    checkNotAnonymous: function () {
        if (!workspace.isAuthorized) {
            this.loadAndRun("showAuthForm", arguments);
            return false;
        }
        return true;
    },

    loadAndRun: function (funcName, args, ms) {
        return this.loadHeader(ms).done(function () {
            Header[funcName].apply(Header, args);
        });
    },

    loadHeader: function (ms) {
        return moduleLoader.loadModule('header', ms);
    }
};
_(["showAuthForm", "showPasswordRecoveryForm", "checkAuthorization"]).each(function (funcName) {
    LazyHeader[funcName] = function () {
        return LazyHeader.loadAndRun(funcName, arguments);
    };
});

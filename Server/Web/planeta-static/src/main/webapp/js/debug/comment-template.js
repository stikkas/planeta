/*globals jQuery*/
(function (jQuery) {
    var oldTemplate = jQuery.template, oldTmpl = jQuery.tmpl, callCount = 0;

    jQuery.template = function (name, tmpl) {
        var el = tmpl, tmplName, result;
        if (!window.debugMode) {
            result = oldTemplate.apply(this, arguments);
        } else {
            //adds before the template comment <!-- Template : templateName : 0/1 --> (1 - inner template {{tmpl }})
            //adds after the template comment <!-- End template : templateName -->
            //to avoid adding comment, it needs to add attribute data-skip-comment-template="1" to the script tag
            //(<script id="..." type="text/x-jquery-template" data-skip-comment-template="1">)
            if (tmpl) {
                if (el.nodeType) {
                    el = $(tmpl);
                }
                try {
                    if (el instanceof jQuery) {
                        el = el.first();
                        tmplName = el.attr('id');
                        if (el.data && _.isFunction(el.data) && el.data() && !el.data().skipCommentTemplate) {
                            try {
                                el.prepend("<!-- Template : " + tmplName + " : " + callCount + " -->");
                                el.append("<!-- End template : " + tmplName + " -->");
                            } catch (e) {
                                //ie8 throws an exception
                            }
                        }
                    }
                } catch (e) {
                    //ignore
                }
            }
            callCount++;
            result = oldTemplate.apply(this, arguments);
            try {
                result = oldTemplate.apply(this, arguments);
                if (result && tmplName) {
                    result.templateName = tmplName;
                }
            } catch (ex) {
                console.log('Error in template: "' + tmplName + '"', 'Exception:', ex);
            }
            callCount--;
        }
        return result;

    };

    jQuery.tmpl = function (tmpl, data, options, parentItem) {
        var i, l, result, el, tmplItem, commFld, templates = [];

        result = oldTmpl.apply(this, arguments);
        if (window.debugMode) {
            //appends to the tmplItem, associated with tags, field templateName
            for (i = 0, l = result.length; i < l; i++) {
                el = result[i];
                if (el.nodeType === 8) {/*comment*/
                    commFld = el.nodeValue.split(' : ');
                    if (commFld[0] === ' Template') {
                        templates.push(commFld[1]);
                    } else if (commFld[0] === ' End template') {
                        templates.pop();
                    }
                } else if (el.nodeType === 1) {
                    tmplItem = $(el).data('tmplItem');
                    if (tmplItem) {
                        tmplItem.templateName = templates[templates.length - 1];
                    }
                }
            }
        }
        return result;

    };

    _.extend(jQuery.tmpl, oldTmpl);
}(jQuery));

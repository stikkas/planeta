/*!
 * add file_name_selector to each css file
 */

'use strict';

var fs = require('fs');
var path = require('path');


module.exports = function (grunt) {

    grunt.registerMultiTask('addCssFilePointer', 'asd', function() {

        this.files.forEach(function(file) {

            if ( grunt.file.exists(file.src.join()) ) {
                var source = grunt.file.read(file.src);
                var fileName = file.src[0].split('/');
                fileName = fileName[fileName.length - 1].replace('.css', '');

                var output = source + '#_' + fileName + '_css_{display:none;}';

                try {
                    grunt.file.write(file.dest, output);
                } catch (err) {
                    grunt.fail.warn(err);
                }
                grunt.log.writeln('File ' + file.dest.cyan + ' created.');
            }

        });

    });


};

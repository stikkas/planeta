<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!--include css /css-generated/network.css-->

<script id="user-settings-contacts-input-template" type="text/x-jquery-template">
    <div class="fieldset fieldset-lg">
        <div class="fieldset-lbl col-3 {{if errorMessage}}error{{/if}}">
            <label class="fieldset-lbl-title">
                {{= title}}
                {{if icon}}
                <span class="f-social-{{= icon}} n-settings_title-soc"></span>
                {{/if}}
                <div class="js-error"></div>
            </label>
        </div>

        <div class="fieldset-val col-9">
            <div class="input-url">
                <table class="input-url_table">
                    <tbody>
                    <tr>
                        <td class="input-url_lbl">{{= urlPrefix}}</td>
                        <td class="input-url_val">
                            <input class="form-control control-xlg" type="text" placeholder="{{= placeholder}}"
                                   name="{{= fieldName}}"
                                   value="{{if value}}{{= value.replace(urlPrefix, '')}}{{else}}{{/if}}"
                                   data-event-change="onFieldValueChange"
                                   url-prefix="{{= urlPrefix}}">
                        </td>
                    </tr>
                    </tbody>
                </table>

                {{if $data.hint}}
                <div class="fieldset-help-tooltip tooltip" data-tooltip="{{= hint}}"></div>
                {{/if}}
            </div>
        </div>
    </div>
</script>

<script id="user-settings-password-input-template" type="text/x-jquery-template">
    <div class="fieldset fieldset-lg" field="{{= name}}">
        <div class="fieldset-lbl col-3">
            <label class="fieldset-lbl-title">{{= title}}
                <div class="fieldset_warning tooltip js-error hide" data-tooltip="">
                    <div class="fieldset-action-warning"></div>
                </div>
            </label>
        </div>

        <div class="fieldset-val col-9">
            <input class="form-control control-xlg" type="password" name="{{= name}}">
        </div>
    </div>
</script>

<script id="user-views-settings-summary-tiny-mce-template" type="text/x-jquery-template">
    <div class="edit-blog-body" style="display: none;">
        <div class="tinymci">
            <div class="tinymce-body" style="overflow: hidden;">
                {{html summary}}
            </div>
        </div>
    </div>
</script>

<script id="user-settings-layout" type="text/x-layout-template" data-use-data-view="true">
    <div class="wrap-container">
        <div id="center-container">
            <div class="cont-header">
                <div class="cont-header_row">
                    <div class="wrap">
                        <div class="col-12">
                            <div class="cont-header_title">
                                <spring:message code="user-settings.jsp.propertie.1" text="default text"> </spring:message>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="wrap">
            <div class="col-12">
                <div data-view="tabsView"></div>
                <div data-view="contentView"></div>
            </div>
        </div>
    </div>
</script>

<script id="user-views-settings-tabs-template" type="text/x-layout-template">
    <div class="tabs">
        <div class="tabs_i {{if $data.subsection === 'general'}}active{{/if}}">
            <a class="tabs_link" href="{{if !workspace.appModel.isCurrentProfileMine()}}/{{= id}}{{/if}}/settings">
                <spring:message code="user-settings.jsp.propertie.2" text="default text"> </spring:message>
            </a>
        </div>

        <div class="tabs_i {{if $data.subsection === 'contacts'}}active{{/if}}">
            <a class="tabs_link" href="{{if !workspace.appModel.isCurrentProfileMine()}}/{{= id}}{{/if}}/settings/contacts">
                <spring:message code="user-settings.jsp.propertie.3" text="default text"> </spring:message>
            </a>
        </div>

        <div class="tabs_i {{if $data.subsection === 'password'}}active{{/if}}">
            <a class="tabs_link" href="{{if !workspace.appModel.isCurrentProfileMine()}}/{{= id}}{{/if}}/settings/password">
                <spring:message code="user-settings.jsp.propertie.4" text="default text"> </spring:message>
            </a>
        </div>
    </div>
</script>

<script id="group-views-settings-tabs-template" type="text/x-layout-template">
    <div class="tabs">
        <div class="tabs_i {{if $data.subsection === 'general'}}active{{/if}}">
            <a class="tabs_link" href="{{if !workspace.appModel.isCurrentProfileMine()}}/{{= id}}{{/if}}/settings">
                <spring:message code="user-settings.jsp.propertie.5" text="default text"> </spring:message>
            </a>
        </div>

        <div class="tabs_i {{if $data.subsection === 'contacts'}}active{{/if}}">
            <a class="tabs_link" href="{{if !workspace.appModel.isCurrentProfileMine()}}/{{= id}}{{/if}}/settings/contacts">
                <spring:message code="user-settings.jsp.propertie.6" text="default text"> </spring:message>
            </a>
        </div>
    </div>
</script>

<script id="user-views-settings-contacts-template" type="text/x-layout-template">
    <div class="n-settings_cont">
        <div class="fieldset-group fieldset-group__grid">
            {{tmpl '#user-settings-contacts-input-template', {
            title: '<spring:message code="user-settings.jsp.propertie.7" text="default text"> </spring:message>',
            icon: '',
            value: $data.siteUrl,
            fieldName: 'siteUrl',
            urlPrefix: 'http://',
            placeholder: 'example.com',
            errorMessage:  $data.fieldErrors ? $data.fieldErrors.siteUrl : ''
            } }}

            {{tmpl '#user-settings-contacts-input-template', {
            title: '<spring:message code="user-settings.jsp.propertie.8" text="default text"> </spring:message>',
            icon: 'vk',
            value: $data.vkUrl,
            fieldName: 'vkUrl',
            urlPrefix: 'https://vk.com/',
            placeholder: 'yourname',
            errorMessage:  $data.fieldErrors ? $data.fieldErrors.vkUrl : ''
            } }}

            {{tmpl '#user-settings-contacts-input-template', {
            title: 'Facebook',
            icon: 'fb',
            value: $data.facebookUrl,
            fieldName: 'facebookUrl',
            urlPrefix: 'https://facebook.com/',
            placeholder: 'yourname',
            errorMessage:  $data.fieldErrors ? $data.fieldErrors.facebookUrl : ''
            } }}

            {{tmpl '#user-settings-contacts-input-template', {
            title: 'Twitter',
            icon: 'tw',
            value: $data.twitterUrl,
            fieldName: 'twitterUrl',
            urlPrefix: 'https://twitter.com/',
            placeholder: 'yourname',
            errorMessage:  $data.fieldErrors ? $data.fieldErrors.twitterUrl : ''
            } }}

            {{tmpl '#user-settings-contacts-input-template', {
            title: 'Google+',
            icon: 'gp',
            value: $data.googleUrl,
            fieldName: 'googleUrl',
            urlPrefix: 'https://plus.google.com/',
            placeholder: 'yourname',
            errorMessage:  $data.fieldErrors ? $data.fieldErrors.googleUrl : ''
            } }}
        </div>
    </div>

    <div class="n-settings_action cf">
        <span class="btn btn-primary btn-xlg pull-right" data-event-click="onSaveContactsClicked">
            <spring:message code="user-settings.jsp.propertie.9" text="default text"> </spring:message>
        </span>

        <span class="action-status pull-right js-success-save" style="display: none;">
            <spring:message code="user-settings.jsp.propertie.10" text="default text"> </spring:message>
        </span>
    </div>
</script>

<script id="user-settings-password-template" type="text/x-layout-template">
    <div class="n-settings_cont">
        <div class="fieldset-group fieldset-group__grid">
            <form style="margin: 0px;">
                {{tmpl '#user-settings-password-input-template', {title:'<spring:message code="user-settings.jsp.propertie.12" text="default text"> </spring:message>', name:'currentPassword'} }}
                {{tmpl '#user-settings-password-input-template', {title:'<spring:message code="user-settings.jsp.propertie.13" text="default text"> </spring:message>', name:'password'} }}
                {{tmpl '#user-settings-password-input-template', {title:'<spring:message code="user-settings.jsp.propertie.14" text="default text"> </spring:message>', name:'confirmPassword'} }}
            </form>
        </div>
    </div>

    <div class="n-settings_action cf">
        <span class="btn btn-primary btn-xlg pull-right" data-event-click="onSavePasswordClicked">
            <spring:message code="user-settings.jsp.propertie.11" text="default text"> </spring:message>
        </span>
    </div>
</script>

<script id="user-views-settings-general-template" type="text/x-layout-template">
    <div class="n-settings_cont">
        <div class="fieldset-group fieldset-group__grid js-children">
            <div class="fieldset fieldset-lg">
                <div class="fieldset-lbl col-3">
                    <label class="fieldset-lbl-title">
                        <spring:message code="user-settings.jsp.propertie.15" text="default text"> </spring:message>
                        <div class="js-error"></div>
                    </label>
                </div>

                <div class="fieldset-val col-9">
                    <input class="form-control control-xlg" type="text" data-planeta-ui="input" name="displayName">
                </div>
            </div>

            <div class="fieldset fieldset-lg">
                <div class="fieldset-lbl col-3">
                    <label class="fieldset-lbl-title">
                        <spring:message code="user-settings.jsp.propertie.16" text="default text"> </spring:message>
                        <div class="js-error"></div>
                    </label>
                </div>

                <div class="fieldset-val col-9">
                    <div class="input-url">
                        <table class="input-url_table">
                            <tbody>
                                <tr>
                                    <td class="input-url_lbl">https://planeta.ru/</td>

                                    <td class="input-url_val">
                                        <input class="form-control control-xlg" type="text" placeholder="{{= profileId}}" name="alias" value="{{= alias}}" {{if $data.alias}}disabled="1"{{/if}}>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        {{if $data.hint}}
                            <div class="fieldset-help-tooltip tooltip" data-tooltip="{{= hint}}"></div>
                        {{/if}}
                    </div>
                </div>
            </div>

            {{if profileType == 'USER'}}
                <div class="fieldset fieldset-lg js-gender">
                    <div class="fieldset-lbl col-3">
                        <label class="fieldset-lbl-title">
                            <spring:message code="user-settings.jsp.propertie.17" text="default text"> </spring:message>
                        </label>
                    </div>

                    <div class="fieldset-val col-9">
                        <select class="pln-select pln-select-xlg" data-planeta-ui="dropDownSelect" name="userGender">
                            <option value="NOT_SET">- <spring:message code="user-settings.jsp.propertie.18" text="default text"> </spring:message> -</option>
                            <option value="MALE"><spring:message code="user-settings.jsp.propertie.19" text="default text"> </spring:message></option>
                            <option value="FEMALE"><spring:message code="user-settings.jsp.propertie.20" text="default text"> </spring:message></option>
                        </select>
                    </div>
                </div>


                <div class="fieldset fieldset-lg">
                    <div class="fieldset-lbl col-3">
                        <label class="fieldset-lbl-title">
                            <spring:message code="user-settings.jsp.propertie.21" text="default text"> </spring:message>
                            <div class="js-error"></div>
                        </label>
                    </div>

                    <div class="fieldset-val col-9">
                        <input class="form-control control-xlg" type="text"  data-planeta-ui="dateSelect" name="userBirthDate">
                    </div>
                </div>

                <div class="fieldset fieldset-lg">
                    <div class="fieldset-lbl col-3">
                        <label class="fieldset-lbl-title">
                            <spring:message code="user-settings.jsp.propertie.22" text="default text"> </spring:message>
                        </label>
                    </div>

                    <div class="fieldset-val col-9">
                        <select class="pln-select pln-select-xlg" data-planeta-ui="country" name="countryId"></select>
                    </div>
                </div>

                <div class="fieldset fieldset-lg">
                    <div class="fieldset-lbl col-3">
                        <label class="fieldset-lbl-title">
                            <spring:message code="user-settings.jsp.propertie.23" text="default text"> </spring:message>
                        </label>
                    </div>

                    <div class="fieldset-val col-9">
                        <input class="form-control control-xlg" data-planeta-ui="city" type="text" placeholder="<spring:message code="user-settings.jsp.propertie.24" text="default text"> </spring:message>" value="{{= cityName}}" >
                        <input class="hide" name="cityId" value="{{= cityId}}">
                    </div>
                </div>

                <div class="fieldset fieldset-lg">
                    <div class="fieldset-lbl col-3">
                        <label class="fieldset-lbl-title">
                            <spring:message code="user-settings.jsp.propertie.25" text="default text"> </spring:message>
                            <div class="js-error"></div>
                        </label>
                    </div>

                    <div class="fieldset-val col-9">
                        <input class="form-control control-xlg" type="text" data-planeta-ui="input" data-field-name="phoneNumber" name="phoneNumber" value="{{= phoneNumber}}">
                    </div>
                </div>
            {{/if}}

            <div class="fieldset fieldset-lg">
                <div class="fieldset-lbl col-3">
                    <label class="fieldset-lbl-title">
                        {{if profileType == 'USER'}}
                            <spring:message code="user-settings.jsp.propertie.26" text="default text"> </spring:message>
                        {{else}}
                            <spring:message code="user-settings.jsp.propertie.27" text="default text"> </spring:message>
                        {{/if}}

                        <div class="js-error"></div>
                    </label>
                </div>

                <div class="fieldset-val col-9 fieldset_ww-block">
                    <div id="description-editor-stub" class="campaign-tinymce-loader"><div class="campaign-tinymce-loader-in"></div></div>
                    <div data-anchor="tiny-mce" class="pln-tiny-mce-block"></div>
                </div>
            </div>

            {{if profileType == 'USER'}}
                <div class="fieldset fieldset-lg">
                    <div class="fieldset-val col-12">
                        <div class="fieldset-val_cont">
                            <label>
                                <input class="checkbox-row flat-ui-control" data-planeta-ui="checkbox" name="receiveMyCampaignNewsletters"/>
                                <spring:message code="user-settings.jsp.propertie.30" text="default text"> </spring:message>
                            </label>
                        </div>
                    </div>
                </div>
            {{/if}}
        </div>
    </div>


    <div class="n-settings_action cf">
        <span class="btn btn-primary btn-xlg pull-right" data-event-click="onSaveSettingsGeneralClicked">
            <spring:message code="user-settings.jsp.propertie.9" text="default text"> </spring:message>
        </span>
    </div>

    <div class="wrap-row">
    <div class="n-settings_action col-6">
        <span class="btn btn-xlg btn-block js-subscribe" data-event-click="onSubscribe">
            <spring:message code="user-settings.jsp.propertie.29" text="default text"> </spring:message>
        </span>
    </div>

    <div class="n-settings_action col-6">
        <span class="btn btn-xlg btn-block col-6 js-unsubscribe" data-event-click="onUnsubscribe">
            <spring:message code="user-settings.jsp.propertie.32" text="default text"> </spring:message>
        </span>
    </div>
    </div>

</script>




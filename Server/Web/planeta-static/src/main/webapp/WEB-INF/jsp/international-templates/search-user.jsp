<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script id="search-user-block-template" type="text/x-jquery-template">
    <div class="wrap">
        <div class="col-12">
            <input id="query" class="n-search_input" value="{{= queryModel.query}}" type="text" placeholder="<spring:message code="enter.username" text="default text"> </spring:message>" data-emoji_font="true" style="font-family: ProximaNovaLight, Arial, sans-serif, 'Segoe UI Emoji', 'Segoe UI Symbol', Symbola, EmojiSymbols !important;">
        </div>
    </div>
</script>

<script id="search-user-list-items" type="text/x-jquery-template">
    <div class="n-search_ava" style="overflow:hidden">
        <a href="/{{= ProfileUtils.getUserLink(profileId, alias)}}">
            <img src="{{= ImageUtils.getThumbnailUrl(smallImageUrl, ImageUtils.AVATAR, ImageType.GROUP)}}"
                 alt="{{= name}}" profile-link-data="{{= profileId}}"/>
        </a>
    </div>
    <div class="n-search_info">
        {{if backedCount == 0 && projectsCount == 0 && subscribersCount == 0 }}
        <div class="n-search_info_list n-search_info_list__empty"></div>
        {{else}}
        <div class="n-search_info_list
                {{if showBackedCampaigns && backedCount && projectsCount && subscribersCount}}n-search_info_list__3{{/if}}
                {{if !!(showBackedCampaigns && backedCount) + !!projectsCount + !!subscribersCount === 1}}n-search_info_list__1{{/if}}">
            {{if showBackedCampaigns && backedCount > 0}}
            <div class="n-search_info_i">
                <div class="n-search_info_val">{{= backedCount }}</div>
                <div class="n-search_info_lbl"><spring:message code="sponsored" text="default text"> </spring:message></div>
            </div>
            {{/if}}
            {{if projectsCount > 0}}
            <div class="n-search_info_i">
                <div class="n-search_info_val">{{= projectsCount }}</div>
                <div class="n-search_info_lbl"><spring:message code="created" text="default text"> </spring:message></div>
            </div>
            {{/if}}
            {{if subscribersCount > 0}}
            <div class="n-search_info_i">
                <div class="n-search_info_val">{{= subscribersCount }}</div>
                <div class="n-search_info_lbl"><spring:message code="subscribers" text="default text"> </spring:message></div>
            </div>
            {{/if}}
        </div>
        <div class="n-search_action">
            <div class="row-fluid">
                {{if profileTypeCode != 2 && (workspace.appModel.get("myProfile").get("isAdmin") || canSendMsg) }}
                <div class="span6">
                    <span class="btn btn-primary btn-lg btn-block js-subscribe {{if subscribed}} disabled {{/if}}" data-profile-id="{{= profileId }}">{{if subscribed}}<spring:message code="subscribed" text="default text"> </spring:message>{{else}}<spring:message code="subscribe" text="default text"> </spring:message>{{/if}}</span>
                </div>
                <div class="span6">
                    <span class="btn btn-lg btn-block js-message" data-profile-id="{{= profileId }}"><spring:message code="message" text="default text"> </spring:message></span>
                </div>
                {{else}}
                <div class="span12">
                    <span class="btn btn-primary btn-lg btn-block js-subscribe {{if !workspace.isAuthorized || subscribed}} disabled {{/if}}" data-profile-id="{{= profileId }}">{{if subscribed}}<spring:message code="subscribed" text="default text"> </spring:message>{{else}}<spring:message code="subscribe" text="default text"> </spring:message>{{/if}}</span>
                </div>
                {{/if}}
            </div>
        </div>
        {{/if}}
    </div>
    <div class="n-search_cont">
        <div class="n-search_name"><a href="/{{= ProfileUtils.getUserLink(profileId, alias)}}">{{= displayName}}</a></div>
        <div class="n-search_meta">
            {{if countryName}}
            <span class="n-search_meta_i">{{= countryName}}{{if cityName}}, {{= cityName}}{{/if}}</span>
            {{/if}}
            {{if userBirthDate}}
            <span class="n-search_meta_i"><spring:message code="decl.birth.year" text="default text"> </spring:message></span>{{/if}}
        </div>
        {{if summary}}
        <div class="n-search_descr">{{html summary}}</div>
        {{/if}}
    </div>
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!--include css /css-generated/account.css-->
<!--===================== Account details page ============================-->
<script id="personal-cabinet-template" type="text/x-jquery-template">
    <div class="private-office-header">
        <div class="cont-header_row">
            <div class="wrap">
                <div class="col-12">
                    <div class="private-office-header_title"><spring:message code="account" text="default text"> </spring:message></div>
                    <div class="private-office-balance private-office-balance__hold">
                        <div class="private-office-balance_value">
                            <div class="private-office-balance_balance">
                                <spring:message code="your.balance" text="default text"> </spring:message> {{= StringUtils.humanNumber(balance)}} <span class="b-rub">Р</span>
                            </div>

                            {{if frozenAmount > 0}}
                            <div class="private-office-balance_hold">
                                <spring:message code="frozen" text="default text"> </spring:message> {{= StringUtils.humanNumber(frozenAmount)}} <span class="b-rub">Р</span>
                            </div>
                            {{/if}}
                        </div>

                        <div class="private-office-balance_btn">
                            <span class="btn btn-primary"><spring:message code="deposit.funds" text="default text"> </spring:message></span>
                        </div>
                    </div>
                    <div class="private-office-search">
                        <input type="text" placeholder="<spring:message code="search" text="default text"> </spring:message>" name="searchCriteria" value='{{= Account.searchString}}'  class="form-control control-lg">
                        <i class="icon-search icon-gray"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="wrap">
        <div class="col-12">
            <div class="private-office_support">
                <spring:message code="questions" text="default text"> </spring:message> <b><spring:message code="support.phone.number" text="default text"> </spring:message></b>
                <br>
                <spring:message code="time" text="default text"> </spring:message> <a href="mailto:<spring:message code="support.email" text="default text"> </spring:message>"><b><spring:message code="support.email" text="default text"> </spring:message></b></a>
            </div>
            <div class="tabs_wrap">
                <div class="tabs">
                    <div class="tabs_i js-tab active" data-tab="purchases">
                        <a class="tabs_link" href="javascript:void(0);"><spring:message code="purchases2" text="default text"> </spring:message></a>
                    </div>
                <c:if test="${false}"><%-- properties['lk.cards.enabled'] (but it doesnt work here) --%>
                    <div class="tabs_i js-tab" data-tab="cards">
                        <a class="tabs_link" href="javascript:void(0);">
                            <spring:message code="linked.payments" text="default text"> </spring:message>
                            <span class="tabs_badge new-badge">3</span>
                        </a>
                    </div>
                            <%--When lk.cards would in production, then remove following block!!!--%>
                    {{if isMobileDev}}
                            <div class="tabs_i js-tab">
                                <a class="tabs_link" href="javascript:void(0);">
                                    Привязанные карты
                                    <span class="tabs_badge new-badge">3</span>
                                </a>
                            </div>

                            <div class="tabs_i js-tab">
                                <a class="tabs_link" href="javascript:void(0);">
                                    Привязанные карты
                                    <span class="tabs_badge new-badge">3</span>
                                </a>
                            </div>

                            <div class="tabs_i js-tab">
                                <a class="tabs_link" href="javascript:void(0);">
                                    Привязанные карты
                                    <span class="tabs_badge new-badge">3</span>
                                </a>
                            </div>
                            <div class="tabs_i js-tab">
                                <a class="tabs_link" href="javascript:void(0);">
                                    Привязанные карты
                                    <span class="tabs_badge new-badge">3</span>
                                </a>
                            </div>
                            <div class="tabs_i js-tab">
                                <a class="tabs_link" href="javascript:void(0);">
                                    Привязанные карты
                                    <span class="tabs_badge new-badge">3</span>
                                </a>
                            </div>
                    {{/if}}
                    <%-- And me remove, нах--%>
                </c:if>
                <c:if test="${false}"><%-- properties['lk.regulars.enabled'] (but it doesnt work here) --%>
                    <div class="tabs_i js-tab" data-tab="regulars">
                        <a class="tabs_link" href="javascript:void(0);"><spring:message code="autopayments" text="default text"> </spring:message></a>
                    </div>
                </c:if>
                <c:if test="${properties['lk.bills.enabled']}">
                    {{if billCount > 0}}
                    <div class="tabs_i js-tab" data-tab="billings">
                        <a class="tabs_link" href="javascript:void(0);"><spring:message code="billings" text="default text"> </spring:message></a>
                    </div>
                    {{/if}}
                </c:if>
                </div>
            </div>

            <div class="private-office-block js-private-office-block">
                <div class="list-loader js-loader hide">
                    <a class="load" href="javascript:void(0)"><i class="icon-load"></i> <spring:message code="loading.dots" text="default text"> </spring:message></a>
                </div>
                <div id="list-items"></div>
                <div class="purchases-load-placeholder js-pager hide">
                    <div class="purchases-pager list-loader">
                        <a href="javascript:void(0)"><i class="icon-load-cont"></i> <spring:message code="load.more" text="default text"> </spring:message></a>
                    </div>
                </div>
                <div class="list-loader js-pager-loader hide">
                    <a class="load" href="javascript:void(0)"><i class="icon-load"></i> <spring:message code="loading.dots" text="default text"> </spring:message></a>
                </div>
            </div>

            <div class="n-empty pdg-t-50 pdg-b-40 mrg-t-20 mrg-b-40 js-empty hide">
                <div class="n-empty_cover">
                    <div class="n-empty_img"></div>
                </div>
                <div class="n-empty_text"> <spring:message code="not.found" text="default text"> </spring:message> </div>
            </div>
        </div>
    </div>
</script>

<%-- Tab of purchases --%>
<script id="account-purchases-template" type="text/x-jquery-template">
    <div class="private-office-purchases"></div>
</script>

<script id="account-purchase-template" type="text/x-jquery-template">
<div class="private-office-purchases_info">
    <div class="private-office-purchases_num">№{{= orderId}}</div>
    <div class="private-office-purchases_status">
        {{if status == 'new'}}<spring:message code="new.order" text="default text"> </spring:message>
        {{else status == 'paid'}}<spring:message code="paid" text="default text"> </spring:message>
        {{else status == 'sent'}}<spring:message code="sent" text="default text"> </spring:message>
        {{else status == 'delivered'}}<spring:message code="delivered" text="default text"> </spring:message>
        {{else status == 'canceled'}}<spring:message code="cancelled" text="default text"> </spring:message>
        {{else status == 'given'}}<spring:message code="given" text="default text"> </spring:message>
        {{/if}}
    </div>
    <div class="private-office-purchases_date">
        <div class="private-office-purchases_date_val">{{= DateUtils.formatDate(timeAdded)}}</div>
    </div>
    <div class="private-office-purchases_action">
        {{if canPrint}}
            {{if isBiblio}}
            <a href="/api/welcome/biblio/cert-pdf.html?transactionId={{= topayTransactionId}}" class="btn btn-default btn-block"> <i class="icon-print"></i> <spring:message code="download.cert" text="default text"> </spring:message></a>
            {{else}}
            <a href="/profile/print-order.html?orderId={{= orderId}}" class="btn btn-default btn-block"> <i class="icon-print"></i> <spring:message code="print" text="default text"> </spring:message></a>
            {{/if}}
        {{/if}}
    </div>
</div>

<div class="private-office-purchases_cont">
    <div class="private-office-purchases_products">
        <div class="private-office-purchases_common">
            <div class="private-office-purchases_common_title"><spring:message code="order.cost" text="default text"> </spring:message></div>
            <div class="private-office-purchases_common_cost">{{= StringUtils.humanNumber(totalPrice)}}  <span class="b-rub">Р</span></div>
        </div>
        <div class="private-office-purchases_list">
            {{if isBiblio}}
            <div class="private-office-purchases_list-i">
                <div class="private-office-purchases_list-price">
                    {{= libs.count}} × <span class="private-office-purchases_price-val-b">{{= StringUtils.humanNumber(totalPrice / libs.count)}}</span> <span class="b-rub">Р</span>
                </div>
                <div class="private-office-purchases_list-product">
                    <div class="private-office-product">
                        <spring:message code="publications.sent" text="default text"> </spring:message> <span class="private-office-product_collapse-link">{{= libs.count}} <spring:message code="libraries.for.account" text="default text"> </spring:message></span>
                        <div class="private-office-product_collapse" style="display: none;">
                            <div class="private-office-product_biblio-list">
                            {{each libs.items}}
                                <span><b>{{= objectName}}</b>, {{= info}}</span><br>
                            {{/each}}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            {{/if}}
            {{each orderObjectsInfo}}
            <div class="private-office-purchases_list-i">
                <div class="private-office-purchases_list-price">{{if count > 1}}{{= count}} x{{/if}} {{if price==0}}{{= StringUtils.humanNumber(totalPrice)}}{{else}}{{= StringUtils.humanNumber(price)}}{{/if}} <span class="b-rub">Р</span></div>
                <div class="private-office-purchases_list-product">
                    <div class="private-office-product">
                        <div class="private-office-product_ava">
                        {{if isProduct}}
                        <a href="https://${properties['shop.application.host']}/products/{{= objectId}}">
                        {{/if}}
                        {{if isBiblio}}
                        <img src="{{= ImageUtils.getThumbnailUrl(objectImageUrl, ImageUtils.MEDIUM, ImageType.PHOTO)}}">
                        {{else}}
                        <img src="{{= ImageUtils.getThumbnailUrl(objectImageUrl, ImageUtils.ALBUM_COVER, ImageType.PRODUCT)}}">
                        {{/if}}
                        {{if isProduct}}
                        </a>
                        {{/if}}
                    </div>
                        <div class="private-office-product_cont">
                            <div class="private-office-product_name">
                                {{if isBonus}}
                                <spring:message code="bonus" text="default text"> </spring:message> <a href="https://${properties['application.host']}/welcome/bonuses.html">&laquo;{{= StringUtils.shrinkToLength(objectName, 64)}}&raquo;</a>
                                {{else isDonate}}
                                <spring:message code="community.support" text="default text"> </spring:message> <a href="{{= ProfileUtils.getAbsoluteUserLink(merchantId, merchantAlias)}}">&laquo;{{= merchantName}}&raquo;</a>
                                {{else isProduct}}
                                <a href="https://${properties['shop.application.host']}/products/{{= objectId}}">{{= StringUtils.shrinkToLength(objectName, 64)}}</a>
                                {{else isShare || isInvesting}}
                                <spring:message code="share.noun" text="default text"> </spring:message> <a href="https://${properties['application.host']}/campaigns/{{= ownerId}}">&laquo;{{= StringUtils.shrinkToLength(objectName, 64)}}&raquo;</a>
                                {{else isBiblio}}
                                <spring:message code="magazine" text="default text"> </spring:message> <a href="https://${properties['biblio.application.host']}/?bookId={{= objectId}}">&laquo;{{= StringUtils.shrinkToLength(objectName, 64)}}&raquo;</a>
                                {{/if}}
                            </div>
                            {{if downloadUrls && isCompleted}}
                            <div class="private-office-product_action">
                                {{if DateUtils.afterNow(startSaleDate)}}
                                <a class="btn btn-primary btn-block disabled" href="javascript:void(0);"><spring:message code="download.digital" text="default text"> </spring:message></a>
                                <span class="pobpbl-item-digit-label"><spring:message code="product.will.be.available" text="default text"> </spring:message> {{= DateUtils.formatDateTime(startSaleDate,"d.m.y")}}</span>
                                {{else}}
                                    {{each(index, url) downloadUrls}}
                                    <a class="btn btn-primary btn-block" href="{{= url}}"> <spring:message code="download.product" text="default text"> </spring:message>
                                        {{if url.lastIndexOf('%2F') == -1}}
                                            {{= url.substring(url.lastIndexOf("=")+1, url.length)}}
                                        {{else}}
                                            {{= url.substring(url.lastIndexOf("%2F")+3, url.length)}}
                                        {{/if}}
                                    </a><br>
                                    {{/each}}
                                {{/if}}
                            </div>
                            {{/if}}
                            <div class="private-office-purchases_project">
                                {{if isInvesting || isShare}}
                                {{if isDonate}}
                                <spring:message code="campaign.c.l" text="default text"> </spring:message> <a href="https://${properties['application.host']}/campaigns/{{= ownerId}}">&laquo;{{= StringUtils.shrinkToLength(ownerName, 64)}}&raquo;</a>
                                {{else}}
                                <spring:message code="campaign.c.l" text="default text"> </spring:message> <a href="https://${properties['application.host']}/campaigns/{{= ownerId}}">&laquo;{{= StringUtils.shrinkToLength(ownerName, 64)}}&raquo;</a>
                                <spring:message code="of.author" text="default text"> </spring:message> <a href="{{= ProfileUtils.getAbsoluteUserLink(merchantId, merchantAlias)}}">&laquo;{{= StringUtils.shrinkToLength(merchantName, 64)}}&raquo;</a>
                                {{/if}}
                                {{else isBiblio && info != ""}}
                                <spring:message code="book.bonus" text="default text"> </spring:message> {{= info}}
                                {{/if}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{/each}}
        </div>
    </div>
    {{if promoCodeId > 0 && discount > 0}}
    <div class="private-office-purchases_promocode">
        <div class="private-office-purchases_promocode-price">
            &ndash;{{= discount}}&nbsp;<span class="b-rub">Р</span>
        </div>
        <div class="private-office-purchases_promocode-name">
            <b><spring:message code="discount.promotional.code" text="default text"> </spring:message></b>
        </div>
    </div>
    {{/if}}

    {{if deliveryType != 'NOT_SET' && deliveryAddress.city || trackingCode}}
    <div class="private-office-purchases_delivery-wrap">
        {{if deliveryPrice > 0}}
        <div class="private-office-purchases_delivery-price"> {{= StringUtils.humanNumber(deliveryPrice)}} <span class="b-rub">Р</span></div>
        {{/if}}
        <div class="private-office-purchases_delivery">
            {{if deliveryType != 'NOT_SET' && deliveryAddress.city}}
            <div class="private-office-purchases_delivery_i"> <b><spring:message code="delivery.type" text="default text"> </spring:message>: </b> {{if deliveryType == 'DELIVERY' || deliveryType == 'RUSSIAN_POST'}}<spring:message code="delivery" text="default text"> </spring:message>{{else deliveryType == 'CUSTOMER_PICKUP'}}<spring:message code="pickup" text="default text"> </spring:message>{{/if}} </div>
            {{/if}}
            {{if delAddress}}
            <div class="private-office-purchases_delivery_i"> <b><spring:message code="address" text="default text"> </spring:message>: </b> {{= delAddress}} </div>
            {{/if}}
            {{if phone}}
            <div class="private-office-purchases_delivery_i"> <b><spring:message code="phone.number" text="default text"> </spring:message>: </b> {{= phone}} </div>
            {{/if}}
            {{if deliveryAddress.fio}}
            <div class="private-office-purchases_delivery_i"> <b><spring:message code="addressee" text="default text"> </spring:message>: </b> {{= deliveryAddress.fio}} </div>
            {{/if}}
            {{if trackingCode}}
            <div class="private-office-purchases_delivery_i"> <b><spring:message code="post.code" text="default text"> </spring:message>: </b> {{= trackingCode}} </div>
            {{/if}}
            {{if cashOnDeliveryCost}}
            <div class="private-office-purchases_delivery_i"> <b><spring:message code="c.o.d" text="default text"> </spring:message>: </b> {{= cashOnDeliveryCost}} руб.</div>
            {{/if}}
        </div>
    </div>
    {{/if}}
</div>
</script>
<script id="account-card-template" type="text/x-jquery-template">
    <div class="pob-purchases_card-img">
        <img src="/images/planeta/office-mastercard.jpg">
    </div>
    <div class="pob-purchases_card-info">
        <div class="pob-purchases_card-info_num">1234 56** **** 7890</div>
        <div class="pob-purchases_card-info_type">Оплата через Яндекс</div>
    </div>
    <div class="pob-purchases_card-del">
        <span class="pob-purchases_card-del_link">Удалить</span>
    </div>
</script>
<script id="account-regular-template" type="text/x-jquery-template">
    <div class="pob-purchases-block-info">
        <div class="pobpb-info-num">10 000 <span class="b-rub">Р</span></div>
        <div class="pobpb-info-type">ежемесячно</div>
        <div class="pobpb-info-date">начало 3 января 2014</div>
    </div>
    <div class="pob-purchases-block-list">
        <div class="pobpb-list-item">
            <div class="pobpbl-item-ava"><img src="/images/content/ava00.jpg"></div>
            <div class="pobpbl-item-info-wrap">
                <div class="pobpbl-item-name">Проект <a href="#">Пиломатериалы ЛЕСС-700</a></div>
                <div class="pobpbl-item-info">Магазин «<a href="#">Пиломатериалы и Ко</a>»</div>
                <div class="pobpbl-item-data">Всего <b>3</b> платежа на сумму <b>30 000 <span class="b-rub">Р</span></b> с карты <b>4665 **** **89 2348</b></div>
            </div>
        </div>
    </div>
    <div class="pob-purchases-block-action">
        <div class="pobpba-links">
            <span class="pobpba-link pobpba-link__cancel">Отклонить</span>
        </div>
    </div>
</script>
<script id="account-billing-template" type="text/x-jquery-template">
<div class="private-office-purchases_info">
    <div class="private-office-purchases_num">№{{= orderId}}</div>
    <div class="private-office-purchases_status">
    {{if status == 'NEW' || status == 'ERROR' || status == 'DECLINE'}}Не оплачен
    {{else status == 'CANCELED'}}Аннулирован
    {{else status == 'DONE'}}Оплачен
    {{/if}}
    </div>
    <div class="private-office-purchases_date">
        <div class="private-office-purchases_date_lbl">Выставлен</div>
        <div class="private-office-purchases_date_val">{{= DateUtils.formatDate(timeAdded)}}</div>
        {{if status == 'DONE'}}
        <div class="private-office-purchases_date_lbl">Оплачен</div>
        <div class="private-office-purchases_date_val">{{= DateUtils.formatDate(timeUpdated)}}</div>
        {{/if}}
    </div>
    <div class="private-office-purchases_action">
        <a href="/welcome/invoice/pdf.html?transactionId={{= transactionId}}" class="btn btn-default btn-block"> <i class="icon-print"></i> Скачать счет </a>
    </div>
</div>
{{if projectType.startsWith('INVEST')}} 
    {{tmpl '#account-billing-template_invest_cont'}}
{{else projectType == 'BIBLIO'}} 
    {{tmpl '#account-billing-template_biblio_cont'}}
{{/if}}
</script>
<script id="account-billing-template_invest_cont" type="text/x-jquery-template">
<div class="private-office-purchases_cont">
    <div class="private-office-purchases_list-price">{{= StringUtils.humanNumber(amountNet)}} <span class="b-rub">Р</span></div>
    <div class="private-office-product">
        <div class="private-office-product_ava">
            <img src="{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.ALBUM_COVER, ImageType.PRODUCT)}}">
        </div>
        <div class="private-office-product_cont">
            <div class="private-office-product_name">Покупка доли в проекте 
                <a href="https://${properties['application.host']}/campaigns/{{= campaignId}}">&laquo;{{= StringUtils.shrinkToLength(name, 64)}}&raquo;</a>, счет №{{= transactionId}}
            </div>
            <div class="private-office-product_project">
                Проект <a href="https://${properties['application.host']}/campaigns/{{= campaignId}}">&laquo;{{= StringUtils.shrinkToLength(name, 64)}}&raquo;</a>
                автора <a href="{{= ProfileUtils.getAbsoluteUserLink(merchantId, merchantAlias)}}">&laquo;{{= StringUtils.shrinkToLength(merchantName, 64)}}&raquo;</a>
            </div>
        </div>
    </div>
    <div class="private-office-purchases_delivery-wrap">
        <div class="private-office-purchases_delivery">
            <div class="private-office-purchases_delivery_i">Счет выставлен для {{if orgName}}{{= orgName}}{{else}}{{= lastName}} {{= firstName}} {{= middleName}}{{/if}}</div>
        </div>
    </div>
</div>
</script>
<script id="account-billing-template_biblio_cont" type="text/x-jquery-template">
<div class="private-office-purchases_cont">
    <div class="private-office-purchases_list-price">{{= StringUtils.humanNumber(amountNet)}} <span class="b-rub">Р</span></div>
    <div class="private-office-product">
        <div class="private-office-product_cont">
            <div class="private-office-product_name">Поддержка библиотек, счет №{{= transactionId}}
            </div>
        </div>
    </div>
    <div class="private-office-purchases_delivery-wrap">
        <div class="private-office-purchases_delivery">
            <div class="private-office-purchases_delivery_i">Счет выставлен для {{if orgName}}{{= orgName}}{{else}}{{= lastName}} {{= firstName}} {{= middleName}}{{/if}}</div>
        </div>
    </div>
</div>
</script>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script id="message-form-template" type="text/x-jquery-template">
    <div class="modal-header">
        <a href="javascript:void(0)" class="close">×</a>
        <div class="modal-title"><spring:message code="new.message" text="default text"> </spring:message></div>
        <div class="modal-header-shadow"></div>
    </div>
    <div class="modal-body">
        <form>
            <div class="modal-select-header short-label">
                <div class="fieldset">
                    <div class="full-textarea">
                        <textarea placeholder="<spring:message code="enter.your.message" text="default text"> </spring:message>" id="text" name="text" rows="9" class="form-control control-lg"></textarea>
                    </div>
                </div>
            </div>
            {{if profileIds}}
                {{each(index, profileId) profileIds}}
                    <input type="hidden" name="profileIds[]" value="{{= profileId}}"/>
                {{/each}}
            {{/if}}
        </form>
    </div>
    <div class="modal-footer">
        <div class="modal-footer-cont">
            <button type="submit" class="btn btn-primary"><spring:message code="send.c.l" text="default text"> </spring:message></button>
            <button type="reset" class="btn"><spring:message code="cancel" text="default text"> </spring:message></button>
        </div>
    </div>
</script>

<script id="dialog-search-input-template" type="text/x-jquery-template">
    <input id="dialogs-search" type="text" value="" placeholder="<spring:message code="enter.username" text="default text"> </spring:message>">
    <i class="icon-search icon-gray"></i>
</script>

<script id="dialog-search-template" type="text/x-jquery-template">
    <div class="dialog-members">
        <div class="dm-header">
            <div class="dm-h-block clickHereForCollapse">
                <div class="dm-h-opt">
                    <i class="icon-ww-size"></i>
                    <i class="icon-close"></i>
                </div>
                <div class="dm-h-block-name link-icon cont-va-middle">
                    <i class="icon-dlg-bubble"></i><span class="text-icon"><spring:message code="my.dialogs" text="default text"> </spring:message></span>
                </div>
            </div>


            <div class="dm-search">
                <div id="dialog-search-filter"></div>
            </div>
        </div>
        <div class="right-dialog-wrap">
            <div class="rdw-top-shadow"></div>
            <div class="right-dialog modal-scroll-content"></div>
            <div class="rdw-bottom-shadow"></div>
        </div>
        <div class="dm-footer">

        </div>
    </div>
</script>

<!-- Template for the popup dialog message -->
<script id="dialog-popup-message-template" type="text/x-jquery-template">
    <div class="qn-close"><i class="icon-close"></i></div>
    <div class="qn-content">
        <div class="qn-message">
            <div class="qn-m-ava">
                <a href="{{= ProfileUtils.getAbsoluteUserLink(userId, userAlias)}}">
                    <img src="{{= ImageUtils.getThumbnailUrl(userImageUrl, ImageUtils.SMALL_AVATAR, ImageType.USER)}}" alt="{{= userDisplayName}}">
                </a>
            </div>
            <div class="qn-m-cont">
                <div class="qn-m-name">
                    <a href="{{= ProfileUtils.getAbsoluteUserLink(userId, userAlias)}}" class="profile-link">{{= userDisplayName}}</a>
                </div>
                <div class="qn-m-descr">{{html StringUtils.getShortDialogMessage(messageTextHtml, Dialogs.MESSAGE_LENGTH)}}</div>
                <div class="qn-m-date"><spring:message code="message.time" text="default text"> </spring:message></div>
            </div>
        </div>
    </div>
</script>
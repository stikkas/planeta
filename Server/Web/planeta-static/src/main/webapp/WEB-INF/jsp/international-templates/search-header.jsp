<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<script id="search-results-block-template" type="text/x-jquery-template">
    <diV class="h-search-results_list js-search-results"></diV>

    <div class="h-search-results_all">
        {{if projects.attributes.estimatedCount && projects.attributes.estimatedCount > projects.attributes.size}}
            <a href="{{if projects.attributes.moreResultsUrl}}{{= projects.attributes.moreResultsUrl}}{{if queryModel}}?query={{= queryModel.query}}{{/if}}{{else}}{{= projects.attributes.searchResults.query.getSearchUrl()}}{{/if}}" class="h-search-results_all-link js-results-all">
                <spring:message code="search-header.jsp.property.02"/>
            </a>
        {{else}}
            <a href="https://{{= workspace.serviceUrls.mainHost}}/search/projects" class="h-search-results_all-link js-results-all">
                <spring:message code="search-header.jsp.property.12"/>
            </a>
        {{/if}}

        {{if products.attributes.estimatedCount && products.attributes.estimatedCount > products.attributes.size}}
            <a href="{{if products.attributes.moreResultsUrl}}{{= products.attributes.moreResultsUrl}}{{if queryModel}}?query={{= queryModel.query}}{{/if}}{{else}}{{= products.attributes.searchResults.query.getSearchUrl()}}{{/if}}" class="h-search-results_all-link js-results-all">
                <spring:message code="search-header.jsp.property.11"/>
            </a>
        {{else}}
            <a href="https://{{= workspace.serviceUrls.shopAppUrl}}/products/search.html" class="h-search-results_all-link js-results-all">
                <spring:message code="search-header.jsp.property.13"/>
            </a>
        {{/if}}

        {{if shares.attributes.estimatedCount && shares.attributes.estimatedCount > shares.attributes.size}}
            <a href="{{if shares.attributes.moreResultsUrl}}{{= shares.attributes.moreResultsUrl}}{{if queryModel}}?query={{= queryModel.query}}{{/if}}{{else}}{{= shares.attributes.searchResults.query.getSearchUrl()}}{{/if}}" class="h-search-results_all-link js-results-all">
                <spring:message code="search-header.jsp.property.15"/>
            </a>
        {{else}}
            <a href="https://{{= workspace.serviceUrls.mainHost}}/search/shares" class="h-search-results_all-link js-results-all">
                <spring:message code="search-header.jsp.property.14"/>
            </a>
        {{/if}}
    </div>

    <div class="h-search-results_loader">
        <div class="md-loader"><svg class="md-loader_circular" viewBox="25 25 50 50"><circle class="md-loader_line" cx="50" cy="50" r="20"></circle><circle class="md-loader_spinner" cx="50" cy="50" r="20"></circle></svg></div>
    </div>
</script>

<script id="search-results-list-item-template" type="text/x-jquery-template">
    <a href="javascript:void(0)" data-href="https://{{= workspace.serviceUrls.mainHost}}/campaigns/{{= webCampaignAlias}}" class="h-search-results_link" tabindex="0">
        <span class="h-search-results_img">
            <img src="{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.SMALL_AVATAR, ImageType.PRODUCT)}}" alt="{{= name}}">
        </span>

        <span class="h-search-results_name">{{= StringUtils.cutString(name, 120)}}</span>
    </a>
</script>

<script id="search-results-empty-template" type="text/x-jquery-template">
    <div class="h-search-results_no-results">
        <spring:message code="search-header.jsp.property.07"/>
    </div>
</script>

<script id="search-results-pager-template" type="text/x-jquery-template">

</script>
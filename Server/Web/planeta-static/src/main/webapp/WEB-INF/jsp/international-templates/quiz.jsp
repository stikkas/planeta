<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script id="quiz-template" type="text/x-jquery-template">
    {{if workspace.isAuthorized}}
    <div class="polling_head">
        <h1>{{= name}}</h1>
        <p>{{html description}}</p>
    </div>

        <div class="polling_body">
            <ol id="quiz-questions-list" class="polling-list">
            </ol>

            <div class="polling-action" style="display: none;">
                <button class="btn btn-primary js-send-quiz-form">Отправить</button>
            </div>
        </div>

        <div class="polling_success js-already-answered" style="border-top: 1px solid #ddd; display: none;">
            <div class="polling_success-head">
                Спасибо
            </div>

            <div class="polling_success-text">
                Вы уже приняли участие в опросе.
            </div>
        </div>
        <div class="polling_success js-quiz-closed" style="border-top: 1px solid #ddd; display: none;">
            <div class="polling_success-head">
                Извините
            </div>

            <div class="polling_success-text">
                Опрос уже завершился.
            </div>
        </div>
    {{else}}
    <div class="polling_success">
        <div class="polling_success-head">
            Ой!
        </div>

        <div class="polling_success-text">
            Для того чтобы пройти опрос, пожалуйста, войдите или зарегистрируйтесь.
        </div>
    </div>
    {{/if}}
</script>

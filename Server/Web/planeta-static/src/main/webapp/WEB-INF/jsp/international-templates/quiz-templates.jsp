<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<script id="quiz-question-item-template" type="text/x-jquery-template">
    <li class="polling-list_i {{if $data.error}}error{{/if}}">
        <div class="polling-list_question">
            <h2>{{html questionText}}</h2>
            {{if questionDescription}}
                {{html questionDescription}}
            {{/if}}
        </div>
        {{if type == "CHECKBOX_SIMPLE"}}
        <div class="polling-list_answers">
            <div class="polling-list_answers-i">
                <div class="form-ui form-ui-default">
                    <input class="form-ui-control" id="role-1-{{= quizQuestionId}}" type="radio" name="answer{{= quizQuestionId}}" value="true" quiz-answer-field="answerBoolean" quiz-question-id="{{= quizQuestionId}}">
                    <label class="form-ui-label" for="role-1-{{= quizQuestionId}}">
                        <span class="form-ui-txt">
                            <span class="form-ui-val">Да</span>
                        </span>
                    </label>
                </div>
            </div>
            <div class="polling-list_answers-i">
                <div class="form-ui form-ui-default">
                    <input class="form-ui-control" id="role-2-{{= quizQuestionId}}" type="radio" name="answer{{= quizQuestionId}}" value="false" quiz-answer-field="answerBoolean" quiz-question-id="{{= quizQuestionId}}">
                    <label class="form-ui-label" for="role-2-{{= quizQuestionId}}">
                        <span class="form-ui-txt">
                            <span class="form-ui-val">Нет</span>
                        </span>
                    </label>
                </div>
            </div>
        </div>
        {{else type == "TEXT"}}
        <div class="polling-list_answers">
            <div class="polling-list_answers-i">
                <input type="text" class="form-control" placeholder="Введите сюда ответ..." name="answer{{= quizQuestionId}}" quiz-answer-field="answerCustomText" quiz-question-id="{{= quizQuestionId}}">
            </div>
        </div>
        {{else type == "NUMBER"}}
        <div class="polling-list_answers">
            <div class="polling-list_answers-i">
                <input type="number" class="form-control" placeholder="Введите сюда ответ..." name="answer{{= quizQuestionId}}" quiz-answer-field="answerInteger" quiz-question-id="{{= quizQuestionId}}">
            </div>
        </div>
        {{else type == "TEXT_BIG"}}
        <div class="polling-list_answers">
            <div class="polling-list_answers-i">
                <textarea rows="3" class="form-control" placeholder="Введите сюда ответ..." name="answer{{= quizQuestionId}}" quiz-answer-field="answerCustomText" quiz-question-id="{{= quizQuestionId}}"></textarea>
            </div>
        </div>
        {{else type == "RADIO_NUMBER_RANGE"}}
        <div class="polling-list_answers polling-list_answers__horiz">
            {{each(index, value) Array.apply(null, Array(endValue)).map(function (_, i) {return i;})}}
            <div class="polling-list_answers-i">
                <div class="form-ui form-ui-default">
                    <input class="form-ui-control" type="radio" id="answer{{= quizQuestionId}}{{= index}}" name="answer{{= quizQuestionId}}" value="{{= index+1}}" quiz-answer-field="answerInteger" quiz-question-id="{{= quizQuestionId}}">
                    <label class="form-ui-label" for="answer{{= quizQuestionId}}{{= index}}">
                        <span class="form-ui-txt">
                            <span class="form-ui-val">{{= index+1}}</span>
                        </span>
                    </label>
                </div>
            </div>
            {{/each}}
        </div>
        {{else type == "RADIO_OPTIONS"}}
        <div class="polling-list_answers">
            {{each(index, quizQuestionOption) quizQuestionOptionList}}
            <div class="polling-list_answers-i">
                <div class="form-ui form-ui-default">
                    <input class="form-ui-control" id="answer{{= quizQuestionId}}{{= index}}" type="radio" name="answer{{= quizQuestionId}}" value="{{= quizQuestionOption.optionText}}" quiz-answer-field="answerCustomText" quiz-question-id="{{= quizQuestionId}}">
                    <label class="form-ui-label" for="answer{{= quizQuestionId}}{{= index}}">
                        <span class="form-ui-txt">
                            <span class="form-ui-val">{{html quizQuestionOption.optionText}}</span>
                        </span>
                    </label>
                </div>
            </div>
            {{/each}}
            {{if hasCustomRadio}}
            <div class="polling-list_answers-i">
                <div class="form-ui form-ui-default">
                    <input class="form-ui-control" id="answer{{= quizQuestionId}}{{= index}}" type="radio" name="answer{{= quizQuestionId}}" value="" quiz-answer-field="customRadioText" quiz-question-id="{{= quizQuestionId}}" quiz-custom-radio="true">
                    <label class="form-ui-label" for="answer{{= quizQuestionId}}{{= index}}">
                        <span class="form-ui-txt">
                            <span class="form-ui-val">{{html customRadioText}}</span>
                        </span>
                    </label>
                </div>
            </div>
            <div class="polling-list_answers-i">
                <div class="form-ui form-ui-default">
                    <textarea rows="3" class="form-control" placeholder="Введите сюда ответ..." name="answer{{= quizQuestionId}}" quiz-answer-field="answerCustomText" quiz-question-id="{{= quizQuestionId}}" disabled></textarea>
                </div>
            </div>
            {{/if}}
        </div>
        {{else}}
            <%--{{= parent.indexOf(this)}} {{html questionText}}--%>
        {{/if}}
        <div class="polling-list_error">
            Нам очень важно, чтобы Вы ответили на этот вопрос
        </div>
    </li>
</script>
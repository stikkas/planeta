<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!--include css /css-generated/network.css-->

<script id="user-profile-card-template" type="text/x-jquery-template">
    <div class="wrap-indent-right-half">
        <div class="n-card_main">
            <div class="n-card_ava">
                {{if profileType == 'USER'}}
                <img src="{{= ImageUtils.getUserAvatarUrl(smallImageUrl, ImageUtils.AVATAR, userGender)}}" alt="{{= displayName}}">
                {{/if}}
                {{if profileType == 'GROUP'}}
                <img src="{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.AVATAR, ImageType.GROUP)}}" alt="{{= displayName}}">
                {{/if}}
                {{if profileType == 'EVENT'}}
                <img src="{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.AVATAR, ImageType.EVENT)}}" alt="{{= displayName}}">
                {{/if}}
                {{if PrivacyUtils.isAdmin()}}
                {{if imageUrl}}
                <div class="n-card_ava_del">
                    <span class="s-n-card-del " data-event-click="onRemoveAvatarClicked"></span>
                </div>
                {{/if}}
                <div class="n-card_ava_change">
                    <span class="n-card_ava_change_link" data-event-click="onChangeAvatarClicked">↑ <spring:message code="users.jsp.propertie.1" text="default text"> </spring:message></span>
                </div>
                {{/if}}
            </div>

            <div class="n-card_cont">
                <div class="n-card_name">{{= displayName}}</div>
                <div class="n-card_meta">
                    {{if cityName}}
                    <div class="n-card_meta_i">{{= cityName}}</div>
                    {{/if}}
                    {{if userBirthDate}}
                    <div class="n-card_meta_i"><spring:message code="declension.word.year" text="default text"> </spring:message></div>
                    {{/if}}
                    {{if typeof(onlineStatus) !== 'undefined'}}
                    <div class="n-card_meta_i">
                        {{if onlineStatus == true}}Online{{else}}
                            {{if lastOnline}}
                                <spring:message code="users.jsp.propertie.3" text="default text" />{{if userGender=='FEMALE'}}<spring:message code="users.jsp.propertie.4" text="default text" />{{/if}} {{= DateUtils.formatUserLoginInfo(lastOnline)}}
                            {{else userLoginInfo}}
                                <spring:message code="users.jsp.propertie.5" text="default text" />{{if userGender=='FEMALE'}}<spring:message code="users.jsp.propertie.4" text="default text" />{{/if}} {{= DateUtils.formatUserLoginInfo(userLoginInfo.timeLogin)}}
                            {{/if}}
                        {{/if}}
                    </div>
                    {{/if}}
                </div>

                <div class="n-card_descr">
                    {{if summaryWithoutHtml}}
                        {{if summaryWithoutHtml.length <= 100}}
                            {{= summaryWithoutHtml}}
                            {{if (summary || 0).length > summaryWithoutHtml.length}}
                                <span class="n-card_descr-link" data-event-click="onMoreInfoClicked">
                                    <spring:message code="users.jsp.propertie.6" text="default text"> </spring:message>
                                </span>
                            {{/if}}
                        {{else}}
                            {{= StringUtils.cutString(summaryWithoutHtml,120)}}...
                            <span class="n-card_descr-link" data-event-click="onMoreInfoClicked">
                                <spring:message code="users.jsp.propertie.6" text="default text"> </spring:message>
                            </span>
                        {{/if}}
                    {{else}}
                        {{if (summary || 0).length > summaryWithoutHtml.length}}
                            <span class="n-card_descr-link" data-event-click="onMoreInfoClicked">
                                <spring:message code="users.jsp.propertie.6" text="default text"> </spring:message>
                            </span>
                        {{/if}}
                    {{/if}}
                </div>
            </div>

            {{if PrivacyUtils.isAdmin()}}
                <div class="n-card_opt">
                    <a href="{{if !workspace.appModel.isCurrentProfileMine()}}/{{= profileId}}{{/if}}/settings" class="s-n-card-opt"></a>
                </div>
            {{/if}}
        </div>
    </div>
</script>

<script id="user-info-template" type="text/x-jquery-template">
    <div class="n-card_info-block">
        {{if workspace.appModel.isCurrentProfileMine()}}
            <div class="card_balance cf">
                <div class="card_balance_val">
                    {{= myBalance}} <span class="b-rub">Р</span>
                </div>

                <div class="card_balance_action">
                    <span class="btn btn-primary btn-lg btn-block" data-event-click="onIncreaseBalanceClicked">
                        <spring:message code="deposit.funds" text="default text"> </spring:message>
                    </span>
                </div>
            </div>
        {{else}}
            <div class="n-card_action">
                <div class="row-fluid">
                    <div class="span12">
                        {{if PrivacyUtils.doISubscribeYou()}}
                            <span class="btn btn-lg btn-block n-card_subscr-btn js-toggle-subscribe-button {{if !allowToggleSubscribe}}disabled{{/if}}" data-event-click="onUnsubscribeClicked">
                                <spring:message code="subscribed" text="default text"> </spring:message>
                            </span>
                        {{else}}
                            <span class="btn btn-lg btn-block js-toggle-subscribe-button {{if !allowToggleSubscribe}}disabled{{/if}}" data-event-click="onSubscribeClicked">
                                <spring:message code="subscribe" text="default text"> </spring:message>
                            </span>
                        {{/if}}
                    </div>
                </div>
            </div>
        {{/if}}

        <div class="n-card_info {{if !$data.subscriptionsCount}}n-card_info__one{{/if}}">
            <div class="n-card_info_i" data-event-click="debounce immediate:onShowSubscriberListClicked">
                <div class="n-card_info_val">{{= $data.subscribersCount}}</div>

                <div class="n-card_info_lbl">
                    <spring:message code="users.jsp.propertie.12" text="default text"> </spring:message>
                </div>

                {{if workspace.appModel.isCurrentProfileMine() && $data.newSubscribersCount > 0}}
                    <div class="n-card_info_count">{{= $data.newSubscribersCount > 20 ? "20+" : $data.newSubscribersCount}}</div>
                {{/if}}
            </div>

            {{if $data.subscriptionsCount > 0}}
                <div class="n-card_info_i" data-event-click="debounce immediate:onShowSubscriptionListClicked">
                    <div class="n-card_info_val">{{= $data.subscriptionsCount}}</div>

                    <div class="n-card_info_lbl">
                        <spring:message code="users.jsp.propertie.13" text="default text"> </spring:message>
                    </div>
                </div>
            {{/if}}
        </div>
    </div>
</script>

<script id="user-tabs-template" type="text/x-jquery-template">
    <div class="tabs_i {{if $data.section === 'info'}}active{{/if}}">
        <a class="tabs_link" href="{{= hrefAlias}}">
            <spring:message code="users.jsp.propertie.14" text="default text"> </spring:message>
        </a>
    </div>

    <div class="tabs_i {{if $data.section === 'news'}}active{{/if}}">
        <a class="tabs_link" href="{{= hrefAlias}}/news"> <spring:message code="users.jsp.propertie.15" text="default text"> </spring:message>
            {{if $data.newNewsCount && $data.section !== 'news'}}
                <span class="tabs_badge new-badge">
                    {{if $data.newNewsCount > 20}}
                        20+
                    {{else}}
                        {{= $data.newNewsCount}}
                    {{/if}}
                </span>
            {{/if}}
        </a>
    </div>
</script>



<script id="user-social-template" type="text/x-jquery-template">
    {{if $data.siteUrl}}
        <div class="n-social">
            <div class="n-sidebar_head">
                <spring:message code="users.jsp.propertie.16" text="default text"> </spring:message>
            </div>

            <div class="n-sidebar_cont">
                <a href="{{= $data.siteUrl}}">{{= $data.siteUrl.split('//')[1]}}</a>
            </div>
        </div>
    {{/if}}

    {{if $data.vkUrl || $data.facebookUrl || $data.twitterUrl || $data.googleUrl}}
        <div class="n-social">
            <div class="n-social_head n-sidebar_head">{{= profile.displayName}} <spring:message code="users.jsp.propertie.17" text="default text"> </spring:message></div>

            <div class="n-social_links">
                {{if vkUrl}}<a class="n-social_link" href="{{= vkUrl}}"><span class="s-n-social-vk"></span></a>{{/if}}
                {{if facebookUrl}}<a class="n-social_link" href="{{= facebookUrl}}"><span class="s-n-social-fb"></span></a>{{/if}}
                {{if twitterUrl}}<a class="n-social_link" href="{{= twitterUrl}}"><span class="s-n-social-tw"></span></a>{{/if}}
                {{if googleUrl}}<a class="n-social_link" href="{{= googleUrl}}"><span class="s-n-social-gp"></span></a>{{/if}}
            </div>
        </div>
    {{/if}}
</script>

<script id="user-views-backed-projects-stat" type="text/x-jquery-template">
    {{if $data.campaignTagCountList && campaignTagCountList.length}}
        <div class="n-project-stat_head n-sidebar_head">{{= displayName}} <spring:message code="users.jsp.propertie.18" text="default text"> </spring:message></div>

        <div class="n-project-stat_list">
            {{each campaignTagCountList}}
                <div class="n-project-stat_i">
                    <div class="n-project-stat_lbl">
                        {{if lang == "ru"}}
                            {{= $value.tagName}}
                        {{else}}
                            {{= $value.engTagName}}
                        {{/if}}
                    </div>
            {{if $value}}
                    <div class="n-project-stat_val cat-color__{{= $value.mnemonicName.toLowerCase()}}" style="width: {{= $value.percent}}%; background-color: {{= StringUtils.randomColor()}}"></div>
            {{/if}}
                </div>
            {{/each}}
        </div>
    {{/if}}
</script>

<script id="user-modal-summary-template" type="text/x-jquery-template">
    <div class="modal-profile-info">
        <div class="modal-header">
            <a class="close" data-dismiss="modal">×</a>

            <div class="modal-title">
                <spring:message code="users.jsp.propertie.19" text="default text"> </spring:message>
            </div>

            <div class="modal-header-shadow"></div>
        </div>
        <div class="modal-body">
            {{html summary}}
        </div>
    </div>
</script>

<script id="user-admin-actions-views-template" type="text/x-jquery-template">
    {{if PrivacyUtils.hasAdministrativeRole()}}
        <div class="n-sidebar-plate">
            <div class="n-sidebar_head">
                <spring:message code="users.jsp.propertie.20" text="default text"> </spring:message>
            </div>

            <div class="n-sidebar_plate">
                <span class="btn btn-primary btn-lg btn-block" data-event-click="onCreateProjectClicked">
                    <spring:message code="users.jsp.propertie.21" text="default text"> </spring:message>
                </span>
            </div>
        </div>
    {{/if}}
</script>

<script id="user-views-moderator-alert-template" type="text/x-jquery-template">
    {{if $data.length}}
        <div class="cont-header">
            {{each $data.list}}
                <div class="cont-header_row">
                    <div class="wrap">
                        <div class="col-12">
                            <div class="alert-block {{if $value.campaignStatus === 8}}alert-block__warning{{/if}}">
                                <div class="alert-block_close" data-event-click="onAlertCloseClicked" data-campaign-id="{{= $value.campaignId}}" data-status="{{= $value.campaignStatus}}"></div>
                                <div class="alert-block_ico"></div>
                                <div class="alert-block_cont">
                                    <div class="alert-block_head">
                                        <spring:message code="users.jsp.propertie.22" text="default text"> </spring:message>
                                    </div>

                                    <div class="alert-block_text">
                                        {{if $value.campaignStatus === 8}}
                                            <spring:message code="campaign" text="default text"> </spring:message> <a href="https://{{= workspace.serviceUrls.mainHost}}/campaigns/{{= $value.campaignId}}"><b>{{= $value.name}}</b></a> <spring:message code="users.jsp.propertie.24" text="default text"> </spring:message>
                                        {{else}}
                                            <spring:message code="campaign" text="default text"> </spring:message> <a href="https://{{= workspace.serviceUrls.mainHost}}/campaigns/{{= $value.campaignId}}"><b>{{= $value.name}}</b></a> <spring:message code="users.jsp.propertie.25" text="default text"> </spring:message>
                                        {{/if}}

                                        {{if $value.message}}<br/>{{= $value.message}}{{/if}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            {{/each}}
        </div>
    {{/if}}
</script>

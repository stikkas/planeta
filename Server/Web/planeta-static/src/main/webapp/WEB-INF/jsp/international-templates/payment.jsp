<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script id="payment-amount-editor-template" type="text/x-jquery-template">
    <div class="pln-payment_enter-sum_lbl"><spring:message code="payment.jsp.propertie.1" text="default text"> </spring:message></div>
    <div class="pln-payment_enter-sum_val">
        <input class="pln-payment_enter-sum_input form-control control-lg js-amount" type="text" value="{{= amount}}" maxlength="8">
        <span class="pln-payment_enter-sum_val_currency b-rub">Р</span>
    </div>
</script>

<script id="payment-block-template" type="text/x-jquery-template">
    <div class="pln-payment-box_field-row pln-payment-menu-wrap">
        <div class="pln-payment-type-wrap js-payment-methods"></div>
    </div>

    <div class="pln-payment-box_field-row pln-payment-sub-menu-wrap js-sub-menu">
        <div class="pln-payment-sub-menu js-payment-sub-menu" style="display:none;">
            <div class="pln-payment-box_field-row">
                <div class="pln-payment-type-wrap js-child-payment-methods" style="display:none;"></div>
            </div>
            <div class="fieldset pln-payment-box_field-row js-phone-container" style="display:none;">
                <div class="pln-payment-box_field-label"><spring:message code="payment.jsp.propertie.3" text="default text"> </spring:message></div>
                <div class="pln-payment-box_field-value">
                    <div class="input-field">
                        <input type="hidden" value="{{= needPhone || false}}" name="needPhone"/>
                        <input class="pln-payment-box_field-input js-payment-phone" placeholder="+7 (123) 456-78-90" type="text" value="{{= phone || '' }}" name="paymentPhone"/>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="pln-payment-box_field-row pln-payment-box_field-row_hidden fieldset">
        <div class="hidden">
            <input name="paymentMethodId" type="hidden" />
        </div>

        <div class="pln-payment-box_field-text-error">
            <spring:message code="payment.jsp.propertie.21" text="default text"> </spring:message>
        </div>
    </div>

    <div class="pln-payment-box_field-row js-payment-cards"></div>
</script>

<script id="balance-payment-dialog-template" type="text/x-jquery-template">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">×</a>
        <div class="modal-title"><spring:message code="payment.jsp.propertie.4" text="default text"> </spring:message></div>
    </div>

    <div class="pln-payment-box">
        <div class="pln-payment-box_field-group js-payment"></div>
    </div>

    <div class="pln-payment-terms">
        <spring:message code="payment.jsp.propertie.22" text="default text"> </spring:message> <a href="https://{{= workspace.serviceUrls.mainHost}}/welcome/projects-agreement.html"><spring:message code="payment.jsp.propertie.5" text="default text"> </spring:message></a>
    </div>

    <div class="pln-payment-action cf">
        <a class="flat-btn pull-right disabled js-pay" href="javascript:void(0);"><spring:message code="payment.jsp.propertie.6" text="default text"> </spring:message></a>
    </div>
</script>

<script id="order-payment-block-template" type="text/x-jquery-template">
    <div class="pln-payment-box_head"><spring:message code="payment.jsp.propertie.7" text="default text"> </spring:message></div>

    <div class="pln-payment-box_field-group">
    {{if cashEnabled}}
        <div class="pln-payment-box_field-row">
            <div class="pln-payment-box_delivery-type js-payment-group-selection">
                <div class="radio-row pln-payment-box_radio js-payment-group-item" data-type="PAYMENT_SYSTEM">
                    <span class="radiobox{{= ($data.paymentGroup != 'CASH') ? ' active' : ''}}"></span>
                    <span class="radiobox-label"><spring:message code="payment.jsp.propertie.8" text="default text"> </spring:message></span>
                </div>

                <div class="radio-row pln-payment-box_radio js-payment-group-item" data-type="CASH">
                    <span class="radiobox{{= ($data.paymentGroup == 'CASH') ? ' active' : ''}}"></span>
                    <span class="radiobox-label"><spring:message code="payment.jsp.propertie.9" text="default text"> </spring:message></span>
                </div>
            </div>
        </div>
    {{/if}}
    {{if ($data.paymentGroup != 'CASH')}}
        {{if myBalance && isPlanetaPurchaseEnabled}}
        <div class="pln-payment-box_field-row pln-payment-box_field-pdg pln-payment-order-type">
            <div class="checkbox-row js-planeta-purchase pln-payment-box_checkbox">
                <span class="checkbox{{if planetaPurchase}} active{{/if}}"></span>
                {{if myBalance >= price}}
                <span class="checkbox-label"><spring:message code="payment.jsp.propertie.10" text="default text"> </spring:message></span>
                {{else}}
                <span class="checkbox-label"><spring:message code="payment.jsp.propertie.11" text="default text"> </spring:message><b>{{= myBalance}} <span
                        class="b-rub">Р</span></b></span>
                {{/if}}
            </div>
        </div>
        {{/if}}
        {{if !planetaPurchase || myBalance < price}}
            <div class="js-payment pln-payment-box_field-row"></div>
        {{/if}}
        {{if $data.orderType == 'DELIVERY'}}
            <div class="pln-payment-box_field-row pln-payment-sub-menu-wrap js-delivery-payment-total" style="display: block;"></div>
        {{/if}}
    {{/if}}
    </div>
</script>

<script id="payment-template" type="text/x-jquery-template">
    <div class="pln-payment-box js-payment">
        <div class="pln-payment-box_head"><spring:message code="payment.jsp.propertie.13" text="default text"> </spring:message></div>
    </div>

    <div class="pln-payment-terms">
        <spring:message code="payment.jsp.propertie.22" text="default text"> </spring:message> <a href="https://{{= workspace.serviceUrls.mainHost}}/welcome/projects-agreement.html"><spring:message code="payment.jsp.propertie.14" text="default text"> </spring:message></a>
    </div>
    <div class="pln-payment-action cf mrg-b-20">
        <a class="flat-btn pull-right disabled js-pay" href="javascript:void(0);"><spring:message code="payment.jsp.propertie.15" text="default text"> </spring:message></a>
        <div class="pln-payment-total pull-left"><spring:message code="payment.jsp.propertie.16" text="default text"> </spring:message><b>{{= amount}}<span class="b-rub">Р</span></b></div>
    </div>
</script>

<script id="payment-fail-template" type="text/x-jquery-template">
    <div class="pln-payment-box js-payment">
        <div class="pln-payment-box_head"><spring:message code="payment.jsp.propertie.17" text="default text"> </spring:message></div>
    </div>

    <div class="pln-payment-terms">
        <spring:message code="payment.jsp.propertie.22" text="default text"> </spring:message> <a href="https://{{= workspace.serviceUrls.mainHost}}/welcome/projects-agreement.html"><spring:message code="payment.jsp.propertie.18" text="default text"> </spring:message></a>
    </div>
    <div class="pln-payment-action cf mrg-b-20">
        <a class="flat-btn pull-right disabled js-pay" href="javascript:void(0);"><spring:message code="payment.jsp.propertie.19" text="default text"> </spring:message></a>
        <div class="pln-payment-total pull-left"><spring:message code="payment.jsp.propertie.20" text="default text"> </spring:message><b>{{= amount}}<span class="b-rub">Р</span></b></div>
    </div>
</script>

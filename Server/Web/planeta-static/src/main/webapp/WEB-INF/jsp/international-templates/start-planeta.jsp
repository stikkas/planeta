<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!--include css /css-generated/project.css-->

<script id="empty-backers-template" type="text/x-jquery-template">
    <div class="backers-none">
        {{if status == 'ACTIVE'}}
            <spring:message code="start-planeta.jsp.propertie.1" text="default text"> </spring:message> <br>
            <a href="/campaigns/{{= webCampaignAlias}}/donate">
                <spring:message code="start-planeta.jsp.propertie.2" text="default text"> </spring:message>
            </a>
        {{else}}
            <spring:message code="start-planeta.jsp.propertie.3" text="default text"> </spring:message>
        {{/if}}
    </div>
</script>

<script id="backer-template-new" type="text/x-jquery-template">
    <div class="backers-list-ava">
        <a href="{{= ProfileUtils.getAbsoluteUserLink(profileId, alias)}}" class="profile-link" profile-link-data="{{= profileId}}">
            <img src="{{= ImageUtils.getUserAvatarUrl(imageUrl, ImageUtils.ALBUM_COVER, userGender)}}" alt="{{= displayName}}">
        </a>
    </div>

    <div class="backers-list-cont">
        <div class="backers-list-date">
            {{= DateUtils.formatDate(lastPurchaseTime)}}
        </div>

        <div class="backers-list-name">
            <a  href="{{= ProfileUtils.getAbsoluteUserLink(profileId, alias)}}" class="profile-link" profile-link-data="{{= profileId}}">{{= displayName}}</a>
        </div>

        {{if  countryName}}
            <div class="backers-list-text">
                <i class="icon-place icon-gray"></i>
                {{if lang == "ru"}}
                    {{= countryName}}{{if cityName}}, {{= cityName}}{{/if}}
                {{else lang == "en"}}
                    {{= countryNameEng}}{{if cityNameEng}}, {{= cityNameEng}}{{/if}}
                {{/if}}
            </div>
        {{/if}}

        {{if supportedProjectsCount > 1}}
            <div class="backers-list-text">
                <i class="icon-backing icon-gray"></i>
                {{= StringUtils.getSupportString(userGender)}} <spring:message code="start-planeta.jsp.propertie.4" text="default text"> </spring:message>
            </div>
        {{/if}}
    </div>
</script>
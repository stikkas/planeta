<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script id="flash-alternative-content" type="text/x-jquery-template">
    <p>
        <a href="https://www.adobe.com/go/getflashplayer">
            <img src="https://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
        </a>
    </p>
</script>

<!-- Template for upload file campaignProgress -->
<script id="upload-progress-template" type="text/x-jquery-template">
    <div class="audio-upload-name clearfix">
        {{= statusDescription}}
        <span>{{= fileName}}</span>
    </div>
    <div class="audio-upload-block">
        <div class="audio-upload-bar clearfix">
            <div class="audio-upload-bar-load" style="width: {{= uploadedSize * 100 / fileSize}}%;">
                <div class="audio-upload-bar-load-crl"></div>
            </div>
        </div>
    </div>
    <div class="audio-upload-size clearfix">
        Загружено {{= StringUtils.humanFileSize(uploadedSize)}} из {{= StringUtils.humanFileSize(fileSize)}}
    </div>
</script>

<!-- Template for image upload preview -->
<script id="upload-image-preview" type="text/x-jquery-template">
    <img src="{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.PHOTO_PREVIEW, ImageType.PHOTO)}}" alt="<spring:message code='upload.uploaded.photo' text='default text'/>" />
</script>

<!-- Template for standard upload form -->
<script id="upload-form-template" type="text/x-jquery-template">
    <div class="modal-header">
        <a href="javascript:void(0)" class="close">×</a>
        <div class="modal-title">{{= title}}</div>
        <div class="modal-header-shadow"></div>
    </div>

    <div class="modal-body">
        <form method="post"
              enctype="multipart/form-data"
              id="upload-form">

              <div class="row clearfix">
                  <div class="span10">
                    <p>{{= text}}</p>
                  </div>
              </div>

              <!-- Files status -->
              <div id="divStatus"></div>

              <!-- Used for uploaded file preview (if previewEnabled==true) -->
              <div class="uploaded-preview"></div>

              <input id="btnCancel"
                     type="hidden"
                     value="Cancel All Uploads"
                     disabled="disabled"
                     style="margin-left: 2px; font-size: 8pt; height: 29px;"/>
        </form>
    </div>

    <div class="modal-footer">
        <div class="modal-footer-cont">
            {{if autoSaveResult==false}}
            <button type="submit" {{if saveEnabled==false}}disabled="true"{{/if}} class="btn btn-primary"><spring:message code="upload.save" text="default text" /></button>
            {{/if}}
            <button type="reset" class="btn"><spring:message code="upload.cancel" text="default text" /></button>
            <span id="spanButtonPlaceHolder"></span>
        </div>
    </div>
</script>


<script id="upload-several-files" type="text/x-jquery-template">
    <div class="modal-header">
        <a data-dismiss="modal" class="close">×</a>
        <div class="modal-title">{{= title}}</div>
    </div>
    <div class="modal-body">
        <div class="upload-photo-modal">
            <div class="uploader-container">
                <div class="uploader-droptext"></div>
                <div class="modal-scroll">
                    <div class="modal-scroll-content"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="modal-footer-cont">
            <div class="btn-group pull-right">
                <button type="reset" class="btn btn-default"><spring:message code="upload.cancel" text="default text" /></button>
                <button type="submit" class="btn btn-primary"><spring:message code="upload.ok" text="default text" /></button>
            </div>

            <div class="uploader-upload-btn">
                <span class="btn btn-primary fileinput-button">
                    <i class="fa fa-upload"></i>
                    <span><spring:message code="upload.choose" /> {{if uploadSettings.file_upload_limit == 1}}<spring:message code="upload.file" />{{else}}<spring:message code="upload.files" />{{/if}} <spring:message code="upload.from.computer" /> </span>
                    <input type="file" name="files[]" id="fileupload"  {{if uploadSettings.file_upload_limit != 1}} multiple="true" {{/if}}>
                </span>
            </div>
        </div>
    </div>

</script>

<script id="file-upload-template" type="text/x-jquery-template">
    <div class="filelist-img filelist-cover tooltip">
        <img src='{{= workspace.staticNodesService.getResourceUrl("/images/defaults/upload.png")}}' alt="upload.png">
    </div>
</script>
<script id="filelist-option-template" type="text/x-jquery-template">
    <i class="icon-close icon-white"></i>
</script>
<script id="uploader-filelist-progress-template" type="text/x-jquery-template">
    <div class="uploader-filelist-load" style="width: 0"></div>
</script>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!--include css /css-generated/interactive.css-->
<script id="interactive-wide-layout" type="text/x-layout-template">
    <div id="global-container">
        <div id="main-container" class="wrap-container">
            <div id="center-container">
                <div class="main">
                    <div id="header-container" class="header js-interactive-header">
                    </div>

                    <div class="main-video">
                        <div class="video-container">
                            <video class="video-js" controls preload="none"></video>
                        </div>

                        <div id="menu-container"></div>

                        <div id="section-container" class="video-form">
                            <div id="content-view"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="footer-container"></div>
</script>

<script id="interactive-video-wide-layout" type="text/x-layout-template">
    <div id="global-container">
        <div id="main-container" class="wrap-container">
            <div id="center-container">
                <div class="main">
                    <div id="header-container" class="header js-interactive-header">
                    </div>

                    <div class="main-video">

                        <div class="video-container video-container__small">
                            <video class="video-js" controls preload="none"></video>
                        </div>

                        <div id="menu-container"></div>
                        <div id="section-container-extra" class="video-form"></div>
                    </div>

                    <div id="section-container">
                        <div id="content-view"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="footer-container"></div>
</script>

<script id="campaign-interactive-lesson-material-template" type="text/x-jquery-template">
    <a href="{{= lessonMaterialUrl}}" target="_blank" class="docs-link">
        <span class="docs-link_ico">
            <span class="docs-link-ico-pdf"></span>
        </span>
        <span class="docs-link_cont">
            <span class="docs-link_name">
                Скачать материалы к уроку
            </span>
            <span class="docs-link_meta">
                PDF
            </span>
        </span>
    </a>
    <div class="lesson-materials_mail">
        <div class="form-ui lesson-mail-form-ui">
            <input class="form-ui-control js-agree-receive-lessons-on-email" type="checkbox" id="box_padding_same" name="" {{if editorData.agreeReceiveLessonsMaterialsOnEmail}}checked="checked"{{/if}}>
            <label class="form-ui-label" for="box_padding_same">
                    <span class="form-ui-txt">
                        отправить мне все материалы
                        <br>
                        на почту в конце курса
                    </span>
            </label>
        </div>
    </div>
</script>

<script id="campaign-interactive-write-down-button-template" type="text/x-jquery-template">
    {{if showForm}}
    <div class="wrap-row">
        <div class="col-4 col-offset-4">
            <button class="intr-btn intr-btn-primary btn-block intr-btn__sm js-write-down-btn">
                <span class="intr-btn_in intr-btn-primary_in">Заполнить</span>
            </button>
        </div>
    </div>
    {{/if}}
</script>


<script id="campaign-interactive-header-template" type="text/x-jquery-template">
    <div class="header_cont">
        <div class="header_logo">
            <a ui-sref="base" class="logo" href="/interactive"></a>
        </div>

        <div class="header_right">
            <div class="header-links">
                <div class="header-links_i">
                    <a href="https://s2.planeta.ru/f/390/prakticheskoe_posobie_po_crowdfundingu_planeta.ru.pdf" target="_blank" class="docs-link">
                    <span class="docs-link_ico">
                        <span class="docs-link-ico-paper"></span>
                        <span class="docs-link_ico-size">54 мб</span>
                    </span>
                        <span class="docs-link_cont">
                        <span class="docs-link_name">
                            Практическое<br>пособие
                        </span>
                    </span>
                    </a>
                </div>
                <div class="header-links_i pln-dropdown js-services-popup">
                    <span class="docs-link pln-d-switch">
                        <span class="docs-link_ico">
                            <span class="docs-link-ico-rocket"></span>
                        </span>
                        <span class="docs-link_cont">
                            <span class="docs-link_name">
                                Платные<br>услуги
                            </span>
                        </span>
                    </span>
                    <div class="header-links-popup pln-d-popup pln-d-popup_right">
                        <div class="header-links-popup_cont">
                            <div class="header-links-popup_text">
                                <b>Planeta.ru</b> предоставляет дополнительные услуги по созданию и продвижению крауд-проектов. Для получения полной информации об услугах и их стоимости отправьте заявку с описанием проекта и перечнем задач на <b>leo@planeta.ru</b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="header_steps-progress">
            <div class="steps-progress">
                <div class="steps-progress_block">
                    <div class="steps-progress_bar">
                    </div>
                    <div class="steps-progress_cont">
                        <span class="steps-progress_lbl js-open-navigation-bar">
                            Пройдено:
                        </span>
                        <span class="steps-progress_val ">
                            {{= editorData.reachedStepNum }} из <span class="">12</span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="header_steps-nav">
        <div class="steps-nav">
            <div class="steps-nav_wrap">
                <div class="steps-nav_list">
                    <div class="steps-nav_i {{if editorData.reachedStepNum == 0}}current{{/if}} {{if activeIndex == 0}}active{{/if}}">
                        <a class="steps-nav_name" href="/interactive/start">Введение</a>
                    </div>
                    
                    <div class="steps-nav_i {{if editorData.reachedStepNum == 1}}current{{/if}} {{if activeIndex == 1}}active{{/if}}">
                        <a class="steps-nav_name" href="/interactive/name">Знакомство</a>
                    </div>
                    
                    <div class="steps-nav_i {{if editorData.reachedStepNum == 2}}current{{/if}} {{if activeIndex == 2}}active{{/if}}">
                        <a class="steps-nav_name" href="/interactive/email">E-mail</a>
                    </div>
                    
                    <div class="steps-nav_i {{if editorData.reachedStepNum == 3}}current{{/if}} {{if activeIndex == 3}}active{{/if}}">
                        <a class="steps-nav_name" href="/interactive/proceed">Приступим</a>
                    </div>
                    
                    <div class="steps-nav_i {{if editorData.reachedStepNum == 4}}current{{/if}} {{if activeIndex == 4}}active{{/if}}">
                        <a class="steps-nav_name" href="/interactive/info">Короткая информация о проекте</a>
                    </div>
                    
                    <div class="steps-nav_i {{if editorData.reachedStepNum == 5}}current{{/if}} {{if activeIndex == 5}}active{{/if}}">
                        <a class="steps-nav_name" href="/interactive/duration">Сроки проекта</a>
                    </div>
                    
                    <div class="steps-nav_i {{if editorData.reachedStepNum == 6}}current{{/if}} {{if activeIndex == 6}}active{{/if}}">
                        <a class="steps-nav_name" href="/interactive/price">Бюджет проекта</a>
                    </div>
                    
                    <div class="steps-nav_i {{if editorData.reachedStepNum == 7}}current{{/if}} {{if activeIndex == 7}}active{{/if}}">
                        <a class="steps-nav_name" href="/interactive/video">Видеообращение</a>
                    </div>
                    
                    <div class="steps-nav_i {{if editorData.reachedStepNum == 8}}current{{/if}} {{if activeIndex == 8}}active{{/if}}">
                        <a class="steps-nav_name" href="/interactive/description">Текстовое описание</a>
                    </div>
                    
                    <div class="steps-nav_i {{if editorData.reachedStepNum == 9}}current{{/if}} {{if activeIndex == 9}}active{{/if}}">
                        <a class="steps-nav_name" href="/interactive/reward">Вознаграждения</a>
                    </div>
                    
                    <div class="steps-nav_i {{if editorData.reachedStepNum == 10}}current{{/if}} {{if activeIndex == 10}}active{{/if}}">
                        <a class="steps-nav_name" href="/interactive/counterparty">Контрагент</a>
                    </div>
                    
                    <div class="steps-nav_i {{if editorData.reachedStepNum == 11}}current{{/if}} {{if activeIndex == 11}}active{{/if}}">
                        <a class="steps-nav_name" href="/interactive/check">Финальная проверка</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>

<script id="campaign-interactive-main-template" type="text/x-jquery-template">
    <div class="main">
        <div class="header js-interactive-header">
        </div>

        <div class="main-video">
            <div class="video-container js-video-container">
                <video class="video-js vjs-fill" controls preload="none"></video>
            </div>

            <div class="video-form"></div>
        </div>

    </div>
</script>

<script id="campaign-interactive-next-button-template" type="text/x-jquery-template">
    <button class="intr-btn intr-btn-primary btn-block js-next-step"><span class="intr-btn_in intr-btn-primary_in">Перейти к следующему шагу</span></button>
</script>

<script id="campaign-interactive-start-template" type="text/x-jquery-template">
    {{if showForm}}
    <div class="video-form_cont">
        <div class="video-form_row">
            <div class="wrap-row">
                <div class="col-6">
                    <button class="intr-btn intr-btn-primary btn-block js-next-step-b"><span class="intr-btn_in intr-btn-primary_in">Давайте повторим</span></button>
                </div>
                <div class="col-6">
                    <button class="intr-btn intr-btn-default btn-block js-next-step"><span class="intr-btn_in intr-btn-default_in">Нет, я знаю, что такое краудфандинг</span></button>
                </div>
            </div>
        </div>
    </div>
    {{/if}}
</script>

<script id="campaign-interactive-name-template" type="text/x-jquery-template">
    {{if showForm}}
    <div class="video-form_cont">
        <form id="interactive-fio-form">
            <div class="video-form_row">
                <div class="video-form_field ">
                    <div class="video-form_field-in">
                        <div class="video-form_lbl">
                            ФИО
                            <span class="video-form_warning tooltip js-tooltip-name" >
                                <svg width="4px" height="14px"><path fill="#ff501a" d="M3.485,0.461 L0.778,0.461 L1.077,9.329 L3.186,9.329 L3.485,0.461 ZM1.107,11.100 C0.847,11.355 0.716,11.676 0.716,12.063 C0.716,12.449 0.847,12.769 1.107,13.021 C1.368,13.272 1.709,13.398 2.131,13.398 C2.559,13.398 2.903,13.272 3.164,13.021 C3.425,12.769 3.555,12.449 3.555,12.063 C3.555,11.670 3.425,11.348 3.164,11.096 C2.903,10.844 2.559,10.718 2.131,10.718 C1.709,10.718 1.368,10.845 1.107,11.100 Z"/></svg>
                            </span>
                        </div>
                        <div class="video-form_val">
                            <input type="text" class="form-control video-form_input" placeholder="Представьтесь, пожалуйста" name="name" value="{{= name}}">
                        </div>
                    </div>
                </div>
            </div>

            <div class="video-form_row">
                <div class="video-form_field video-form_field__blank ">
                    <div class="form-ui intr-form-ui">
                        <input class="form-ui-control" id="chck-1" type="checkbox" name="agree" {{if editorData.agreeStorePersonalData}}checked{{/if}}>
                        <label class="form-ui-label tooltip js-tooltip-agree"  for="chck-1">
                            <span class="form-ui-txt">Я согласен на сбор, хранение и обработку моих персональных данных</span>
                        </label>
                    </div>
                </div>
            </div>


            <div class="video-form_row">
                <button class="intr-btn intr-btn-primary btn-block js-next-step"><span class="intr-btn_in intr-btn-primary_in">Перейти к следующему шагу</span></button>
            </div>
        </form>
    </div>
    {{/if}}
</script>

<script id="campaign-interactive-email-template" type="text/x-jquery-template">
    {{if showForm}}
    <div class="video-form_cont">
        <form id="interactive-email-form">
            <div class="video-form_row">
                <div class="video-form_field">
                    <div class="video-form_field-in">
                        <div class="video-form_lbl">
                            E-mail
                            <span class="video-form_warning tooltip js-tooltip-email" >
                                <svg width="4px" height="14px"><path fill="#ff501a" d="M3.485,0.461 L0.778,0.461 L1.077,9.329 L3.186,9.329 L3.485,0.461 ZM1.107,11.100 C0.847,11.355 0.716,11.676 0.716,12.063 C0.716,12.449 0.847,12.769 1.107,13.021 C1.368,13.272 1.709,13.398 2.131,13.398 C2.559,13.398 2.903,13.272 3.164,13.021 C3.425,12.769 3.555,12.449 3.555,12.063 C3.555,11.670 3.425,11.348 3.164,11.096 C2.903,10.844 2.559,10.718 2.131,10.718 C1.709,10.718 1.368,10.845 1.107,11.100 Z"/></svg>
                            </span>
                        </div>
                        <div class="video-form_val">
                            <input type="text" name="email" class="form-control video-form_input" placeholder="Укажите контактный электронный адрес" value="{{= email}}">
                        </div>
                    </div>
                </div>
            </div>


            <div class="video-form_row">
                <button class="intr-btn intr-btn-primary btn-block js-next-step"><span class="intr-btn_in intr-btn-primary_in">Перейти к следующему шагу</span></button>
            </div>
        </form>
    </div>
    {{/if}}
</script>

<script id="campaign-interactive-proceed-template" type="text/x-jquery-template">
    {{if showForm}}
    <div class="video-form_cont">
        <div class="video-form_row">
            <button class="intr-btn intr-btn-primary btn-block js-next-step"><span class="intr-btn_in intr-btn-primary_in">Приступим</span></button>
        </div>
    </div>
    {{/if}}
</script>

<script id="campaign-interactive-info-template" type="text/x-jquery-template">
    {{if showForm  || showFormAlways}}
    <div class="video-form_cont video-form-row_list">
        <div class="video-form_row">
            <div class="wrap-row">
                <div class="col-6 js-campaign-image">
                </div>
                <div class="col-6 js-short-description">
                </div>
            </div>
        </div>
    </div>
    {{/if}}
</script>

<script id="campaign-interactive-info-short-description-template" type="text/x-jquery-template">
    <div class="video-form_field-in">
        <div class="video-form_lbl video-form_lbl__sm">
            Краткая информация о&nbsp;вашем проекте
            <span class="video-form_warning tooltip tooltip js-tooltip-shortDescription" >
                <svg width="4px" height="14px"><path fill="#ff501a" d="M3.485,0.461 L0.778,0.461 L1.077,9.329 L3.186,9.329 L3.485,0.461 ZM1.107,11.100 C0.847,11.355 0.716,11.676 0.716,12.063 C0.716,12.449 0.847,12.769 1.107,13.021 C1.368,13.272 1.709,13.398 2.131,13.398 C2.559,13.398 2.903,13.272 3.164,13.021 C3.425,12.769 3.555,12.449 3.555,12.063 C3.555,11.670 3.425,11.348 3.164,11.096 C2.903,10.844 2.559,10.718 2.131,10.718 C1.709,10.718 1.368,10.845 1.107,11.100 Z"/></svg>
            </span>
        </div>
        <div class="video-form_val">
            <textarea name="shortDescription" class="form-control video-form_input" data-limit="180" rows="5">{{= shortDescription}}</textarea>
            <div class="rest-count">Осталось 180 символов</div>
        </div>
    </div>
</script>

<script id="campaign-interactive-info-tags-template" type="text/x-jquery-template">
    <div class="video-form_field">
        <div class="video-form_field-in">
            <div class="video-form_lbl">
                Выберите категорию
                <span class="video-form_warning tooltip tooltip js-tooltip-tags" >
                    <svg width="4px" height="14px"><path fill="#ff501a" d="M3.485,0.461 L0.778,0.461 L1.077,9.329 L3.186,9.329 L3.485,0.461 ZM1.107,11.100 C0.847,11.355 0.716,11.676 0.716,12.063 C0.716,12.449 0.847,12.769 1.107,13.021 C1.368,13.272 1.709,13.398 2.131,13.398 C2.559,13.398 2.903,13.272 3.164,13.021 C3.425,12.769 3.555,12.449 3.555,12.063 C3.555,11.670 3.425,11.348 3.164,11.096 C2.903,10.844 2.559,10.718 2.131,10.718 C1.709,10.718 1.368,10.845 1.107,11.100 Z"/></svg>
                </span>
            </div>
            <div class="video-form_val">

                <div class="video-form-select2">
                    <input name="tags" multiple="multiple" class="select2-offscreen select-tags js-select2"
                           placeholder="<spring:message code="campaigns-edit.jsp.propertie.42" text="default text"> </spring:message>" data-field-name="{{if useEng}}engName{{else}}name{{/if}}" tabindex="-1">

                </div>

            </div>
        </div>
    </div>
</script>

<script id="campaign-interactive-info-name-template" type="text/x-jquery-template">
    <div class="video-form_field">
        <div class="video-form_field-in">
            <div class="video-form_lbl">
                Название
                <span class="video-form_warning tooltip tooltip js-tooltip-name" >
                    <svg width="4px" height="14px"><path fill="#ff501a" d="M3.485,0.461 L0.778,0.461 L1.077,9.329 L3.186,9.329 L3.485,0.461 ZM1.107,11.100 C0.847,11.355 0.716,11.676 0.716,12.063 C0.716,12.449 0.847,12.769 1.107,13.021 C1.368,13.272 1.709,13.398 2.131,13.398 C2.559,13.398 2.903,13.272 3.164,13.021 C3.425,12.769 3.555,12.449 3.555,12.063 C3.555,11.670 3.425,11.348 3.164,11.096 C2.903,10.844 2.559,10.718 2.131,10.718 C1.709,10.718 1.368,10.845 1.107,11.100 Z"/></svg>
                </span>
            </div>
            <div class="video-form_val">
                <input name="name" type="text" class="form-control video-form_input" data-limit="180" value="{{= name}}">
            </div>
        </div>
    </div>
</script>

<script id="campaign-interactive-info-image-and-description-template" type="text/x-jquery-template">
<div class="wrap-row">
    <div class="col-6 js-campaign-image">
    </div>
    <div class="col-6 js-short-description">
    </div>
</div>
</script>

<script id="campaign-interactive-info-image-template" type="text/x-jquery-template">
    <div class="video-form_field-in" name="imageUrl">
        <div class="video-form_lbl">
            Фото
            <span class="video-form_warning tooltip tooltip js-tooltip-imageUrl" >
                <svg width="4px" height="14px"><path fill="#ff501a" d="M3.485,0.461 L0.778,0.461 L1.077,9.329 L3.186,9.329 L3.485,0.461 ZM1.107,11.100 C0.847,11.355 0.716,11.676 0.716,12.063 C0.716,12.449 0.847,12.769 1.107,13.021 C1.368,13.272 1.709,13.398 2.131,13.398 C2.559,13.398 2.903,13.272 3.164,13.021 C3.425,12.769 3.555,12.449 3.555,12.063 C3.555,11.670 3.425,11.348 3.164,11.096 C2.903,10.844 2.559,10.718 2.131,10.718 C1.709,10.718 1.368,10.845 1.107,11.100 Z"/></svg>
            </span>
        </div>
        <div class="video-form_val video-form-upload js-val">
        </div>
    </div>
</script>

<script id="campaign-interactive-duration-template" type="text/x-jquery-template">
    {{if showForm}}
    <div class="video-form_cont">
        <div class="video-form_row">
            <div class="video-form_field">
                <div class="video-form_field-in">
                    <div class="video-form_lbl">
                        Срок проекта
                    </div>
                    <div class="video-form_val">
                        <div class="video-form_slider">
                            <div id="slider" duration-slider>
                                <div id="slider-handle" class="ui-slider-handle">
                                    <div class="ui-slider-handle-tooltip"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="video-form_row">
            <button class="intr-btn intr-btn-primary btn-block js-next-step"><span class="intr-btn_in intr-btn-primary_in">Перейти к следующему шагу</span></button>
        </div>
    </div>
    {{/if}}
</script>

<script id="campaign-interactive-price-template" type="text/x-jquery-template">
    {{if showForm}}
    <div class="video-form_cont">
        <div class="video-form_row">
            <div class="price-calc">

                <div class="js-desired-target-amount">
                </div>

                <div class="price-calc_sep">
                    +
                </div>

                <div class="js-desired-collected-amount-percent">
                </div>

                <div class="price-calc_sep">
                    +
                </div>

                <div class="js-contractor-type-percent">
                </div>

                <div class="price-calc_sep">
                    =
                </div>

                <div class="js-visible-target-amount">
                </div>


            </div>
        </div>

        <div class="video-form_row">
            <button class="intr-btn intr-btn-primary btn-block js-next-step"><span class="intr-btn_in intr-btn-primary_in">Перейти к следующему шагу</span></button>
        </div>
    </div>
    {{/if}}
</script>

<script id="campaign-interactive-price-desired-collected-amount-percent-template" type="text/x-jquery-template">
    <div class="price-calc_lbl">
        Коммиссия Planeta.ru
    </div>
    <div class="price-calc_val">
        <div class="video-form_field video-form_field__blank">
            <div class="form-ui slide-form-ui">
                <input class="form-ui-control" id="chck-desired-collected-amount-percent" type="checkbox" name="desiredCollectedAmountPercent" {{if $data.desiredCollectedAmountPercent == 15}} checked{{/if}}>
                <label class="form-ui-label" for="chck-desired-collected-amount-percent">
                    <span class="form-ui-slide"></span>
                    <span class="form-ui-val form-ui-val-first tooltip" data-tooltip="Комиссия Planeta.ru и платежных&lt;br&gt;агрегаторов при сборе <b>от 100%</b>" data-tooltip-position="bottom center" data-tooltip-theme="price-calc-tooltip">10%</span>
                    <span class="form-ui-val form-ui-val-second tooltip" data-tooltip="Комиссия Planeta.ru и платежных&lt;br&gt;агрегаторов при сборе &lt;nobr&gt;<b>от 50% до 99%</b>&lt;/nobr&gt;" data-tooltip-position="bottom center" data-tooltip-theme="price-calc-tooltip">15%</span>
                </label>
            </div>
        </div>
    </div>
</script>

<script id="campaign-interactive-price-contractor-type-percent-template" type="text/x-jquery-template">
    <div class="price-calc_lbl">
        Налог с дохода
    </div>
    <div class="price-calc_val">
        <div class="form-ui slide-form-ui">
            <input class="form-ui-control" id="chck-contractor-type" type="checkbox" name="contractorType" {{if $data.contractorTypePercent == 6}} checked{{/if}}>
            <label class="form-ui-label" for="chck-contractor-type">
                <span class="form-ui-slide"></span>
                <span class="form-ui-val form-ui-val-first tooltip" data-tooltip="Взимается <b>13%</b> НДФЛ" data-tooltip-position="bottom center" data-tooltip-theme="price-calc-tooltip">Физ.лицо</span>
                <span class="form-ui-val form-ui-val-second tooltip" data-tooltip="Расчет производится&lt;br&gt;при учете <b>6%</b> налога УСН" data-tooltip-position="bottom center" data-tooltip-theme="price-calc-tooltip">Юр.лицо</span>
            </label>
        </div>
    </div>
</script>

<script id="campaign-interactive-price-desired-target-amount-template" type="text/x-jquery-template">
    <div class="price-calc_lbl">
        Желаемая сумма сбора
    </div>
    <div class="price-calc_val">
        <div class="video-form_field">
            <div class="video-form_field-in">
                <div class="video-form_val">
                    <input id="interactive-step-price-desired-target-amount" name="desiredTargetAmount" type="text" class="form-control video-form_input" value="{{= InteractiveUtils.calculateTargetAmountAfterTax(contractorTypePercent, desiredCollectedAmountPercent, targetAmount)}}" data-parse="number">
                </div>
            </div>
        </div>
    </div>
</script>

<script id="campaign-interactive-price-visible-target-amount-template" type="text/x-jquery-template">
    <div class="price-calc_lbl">
        Сумма, отображаемая в проекте
    </div>
    <div class="price-calc_val">
        <div class="video-form_field">
            <div class="video-form_field-in">
                <div class="video-form_val">
                    <input id="interactive-step-price-target-amount" name="targetAmount" type="text" class="form-control video-form_input" value="{{= targetAmount}}">
                    <span class="video-form_warning tooltip tooltip js-tooltip-targetAmount" >
                        <svg width="4px" height="14px"><path fill="#ff501a" d="M3.485,0.461 L0.778,0.461 L1.077,9.329 L3.186,9.329 L3.485,0.461 ZM1.107,11.100 C0.847,11.355 0.716,11.676 0.716,12.063 C0.716,12.449 0.847,12.769 1.107,13.021 C1.368,13.272 1.709,13.398 2.131,13.398 C2.559,13.398 2.903,13.272 3.164,13.021 C3.425,12.769 3.555,12.449 3.555,12.063 C3.555,11.670 3.425,11.348 3.164,11.096 C2.903,10.844 2.559,10.718 2.131,10.718 C1.709,10.718 1.368,10.845 1.107,11.100 Z"/></svg>
                    </span>
                </div>
            </div>
        </div>
    </div>
</script>

<script id="campaign-interactive-video-template" type="text/x-jquery-template">
    {{if showForm || showFormAlways}}
    <div class="wrap">

        <div class="project-view-wrap cf">

            <div class="col-12">


                <div class="project-view-container">
                    <div class="project-view-name js-campaign-name">
                    </div>


                    <div class="project-view-header">
                        <div class="wrap-row">
                            <div class="col-8 js-campaign-image video-form-upload-project">
                            </div>

                            <div class="col-4 js-campaign-card">
                            </div>
                        </div>
                    </div>


                </div>


                <div class="project-view-container">
                    <div class="project-view-block">
                        <div class="wrap-row">
                            <div class="col-8">
                                <div class="wrap-indent-right-half">

                                    <div class="project-tab-wrap hold-block">
                                        <ul class="project-tab">
                                            <li class="project-tab-i active">
                                                <a class="project-tab-link" href="?project-tab=descr">
                                                    Описание
                                                </a>
                                            </li>
                                            <li class="project-tab-i">
                                                <a class="project-tab-link" href="?project-tab=faq">
                                                    FAQ
                                                </a>
                                            </li>
                                            <li class="project-tab-i">
                                                <a class="project-tab-link" href="?project-tab=update">
                                                    Новости проекта
                                                </a>
                                            </li>
                                            <li class="project-tab-i">
                                                <a class="project-tab-link" href="?project-tab=comments">
                                                    Комментарии
                                                </a>
                                            </li>
                                            <li class="project-tab-i">
                                                <a class="project-tab-link" href="?project-tab=users">
                                                    Спонсоры
                                                </a>
                                            </li>
                                        </ul>
                                    </div>


                                    <div class="donate-action-view about js-hold-block-white">
                                        <div class="project-description" data-anchor="tiny-mce" name="descriptionHtml">
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-4 js-interactive-sidebar">
                                <div class="project-author hold-block-transparent js-project-author">
                                </div>
                                <div class="js-interactive-shares interactive-shares"></div>
                            </div>

                        </div>
                    </div>
                </div>


                <div class="project-edit-container_action">
                    <div class="project-edit-container_action-fixed">
                        <div class="project-edit-container_action-fixed-cont">
                            <button class="intr-btn intr-btn-primary btn-block js-next-step"><span class="intr-btn_in intr-btn-primary_in">Перейти к следующему шагу</span></button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    {{/if}}
</script>

<script id="campaign-interactive-video-description-plug-template" type="text/x-jquery-template">
    Описание проекта Вы сможете заполнить на следующем шаге!
</script>

<script id="campaign-interactive-video-description-template" type="text/x-jquery-template">
    {{html descriptionHtml}}
</script>

<script id="campaign-interactive-video-campaign-image-template" type="text/x-jquery-template">
    <h1>{{= name}}</h1>
</script>

<script id="campaign-interactive-add-share-button-template" type="text/x-jquery-template">
    <button class="intr-btn intr-btn__sm intr-btn-primary btn-block js-add-share-button"><span class="intr-btn_in intr-btn-primary_in">Добавить еще вознаграждение</span></button>
</script>

<script id="campaign-card-template" type="text/x-jquery-template">
    <div class="pv-card-block">
        <div class="project-view-card">
            <div class="pvc-common">
                <div class="pvc-head">
                    {{if $data.targetStatus == "SUCCESS" && targetAmount}}
                    <span class="pvc-head-success"><spring:message code="successful" text="default text"></spring:message> </span>
                    {{/if}}<spring:message code="collected.amount" text="default text"></spring:message><span class="hidden-mobile">, <spring:message code="rur" text="default text"></spring:message></span>
                </div>

                <div class="pvc-counter-wrap">
                    <div class="pvc-counter js-campaign-odometer odometer-auto-theme"></div>
                    <div class="pvc-counter-currency visible-mobile-ib">
                        <span class="b-rub">Р</span>
                    </div>
                </div>
            </div>



            {{if $data.targetAmount > 0}}
            <div class="pvc-progress-block">
                <div class='pvc-progress {{if status=="FINISHED"}}closed-progress{{else completionPercentage >= 100}}over-progress{{/if}}'>
                    <div class="pvc-bar" style="width: {{= completionPercentage %100}}%;">
                        <div class="pvc-bar-ctrl"></div>
                    </div>
                </div>
            </div>
            {{/if}}

            <div class="pvc-info">
                {{if targetAmount}}
                <div class="pvc-info-field">
                    <div class="pvc-info-name">
                        <spring:message code="target.of.project" text="default text"> </spring:message>
                    </div>

                    <div class="pvc-info-value">{{= StringUtils.humanNumber(targetAmount)}}
                        <span class="b-rub">Р</span>
                    </div>
                </div>
                {{/if}}

                {{if (!finishOnTargetReach && timeFinish != null) && status != "FINISHED"}}
                <div class="pvc-info-field tooltip" data-tooltip='<spring:message code="campaigns-edit.jsp.propertie.66" text="default text"></spring:message> {{html DateUtils.formatDate(timeFinish, "D MMMM YYYY", false, true)}}'>
                    <div class="pvc-info-name">
                        {{if DateUtils.getHoursTo(timeFinish) > 48}}
                        <spring:message code="declension.word.stay2.with.variable" text="default text"> </spring:message>
                        {{else DateUtils.getMinutesTo(timeFinish) >= 60}}
                        <spring:message code="declension.word.stay3.with.variable" text="default text"> </spring:message>
                        {{else DateUtils.getMinutesTo(timeFinish) > 0}}
                        <spring:message code="declension.word.stay6.with.variable" text="default text"> </spring:message>
                        {{/if}}
                    </div>

                    <div class="pvc-info-value">
                        {{if DateUtils.getHoursTo(timeFinish) > 48}}
                        {{= DateUtils.getDaysTo(timeFinish)}}
                        <span class="pvc-info-value_lbl"><spring:message code="declension.word.day2.with.variable" text="default text"> </spring:message></span>
                        {{else DateUtils.getMinutesTo($data.timeFinish) >= 60}}
                        {{= DateUtils.getHoursTo(timeFinish)}}
                        <span class="pvc-info-value_lbl"><spring:message code="declension.word.hour2.with.variable" text="default text"> </spring:message></span>
                        {{else DateUtils.getMinutesTo($data.timeFinish) > 0}}
                        {{= DateUtils.getMinutesTo(timeFinish)}}
                        <span class="pvc-info-value_lbl"><spring:message code="declension.word.minutes.with.variable" text="default text"> </spring:message></span>
                        {{/if}}
                    </div>
                </div>
                {{/if}}

                <div class="pvc-info-field pvc-info-field__last">
                    <div class="pvc-info-name">
                        <spring:message code="purchase" text="default text"> </spring:message>
                    </div>

                    <div class="pvc-info-value">
                        {{= StringUtils.humanNumber(purchaseCount)}}
                        <span class="pvc-info-value_lbl"><spring:message code="declension.word.time" text="default text"> </spring:message></span>
                    </div>
                </div>

                {{if status == "FINISHED" && timeFinish != null}}
                <div class="pvc-info-field hidden-mobile">
                    <div class="pvc-info-name">
                        <spring:message code="project.is.finished" text="default text"> </spring:message>
                    </div>

                    <div class="pvc-info-value">{{html DateUtils.campaignFormatDateWithYear(timeFinish)}}</div>
                </div>
                {{else timeStart != null}}
                <div class="pvc-info-field hidden-mobile">
                    <div class="pvc-info-name">
                        <spring:message code="project.is.started" text="default text"> </spring:message>
                    </div>

                    <div class="pvc-info-value">{{html DateUtils.campaignFormatDateWithYear(timeStart)}}</div>
                </div>
                {{/if}}
            </div>

            <div class="pvc-place">
                {{if cityId != 0}}
                <span class="pvc-place_lbl"><spring:message code="location" text="default text"> </spring:message></span> <a href="/search/projects?query=&cityId={{= cityId}}" class="pvc-place_val">{{if lang == "ru"}}{{= cityNameRus}}{{else}}{{= cityNameEng}}{{/if}}</a>
                {{else regionId != 0}}
                <span class="pvc-place_lbl"><spring:message code="location" text="default text"> </spring:message></span> <a href="/search/projects?query=&regionId={{= regionId}}" class="pvc-place_val">{{if lang == "ru"}}{{= regionNameRus}}{{else}}{{= regionNameEng}}{{/if}}</a>
                {{else countryId != 0}}
                <span class="pvc-place_lbl"><spring:message code="location" text="default text"> </spring:message></span> <a href="/search/projects?query=&countryId={{= countryId}}" class="pvc-place_val">{{if lang == "ru"}}{{= countryNameRus}}{{else}}{{= countryNameEng}}{{/if}}</a>
                {{/if}}
            </div>

            <div class="pvc-footer">
                <div class="pvc-footer-cont">
                    <div class="pvc-share-btn-block cf">
                        <div class="pvc-share-btn js-subscription-container"></div>
                    </div>

                    <div class="pvc-donate-btn-block">
                        {{if status == "ACTIVE"}}
                        <a class="flat-btn btn-block"
                           href="/campaigns/{{= webCampaignAlias}}/donate">
                            {{if $data.tags && $data.tags[0] && tags[0].mnemonicName == "CHARITY"}}
                            <spring:message code="donate" text="default text"> </spring:message>
                            {{else}}
                            <spring:message code="support.the.project.infinitive" text="default text"> </spring:message>
                            {{/if}}
                        </a>
                        {{else status != "FINISHED"}}
                        <div class="flat-btn btn-block disabled">
                            {{if $data.tags && $data.tags[0] && tags[0].mnemonicName == "CHARITY"}}
                            <spring:message code="donate" text="default text"> </spring:message>
                            {{else}}
                            <spring:message code="support.the.project.infinitive" text="default text"> </spring:message>
                            {{/if}}
                        </div>
                        {{else}}
                        <a class="flat-btn btn-block pvc-donate-btn-success">
                            {{if targetStatus == "SUCCESS"}}
                            <i></i>
                            <spring:message code="project.is.successfully.finished" text="default text"> </spring:message>
                            {{else}}
                            <spring:message code="project.is.finished" text="default text"> </spring:message>
                            {{/if}}
                        </a>
                        {{/if}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>

<script id="campaign-interactive-campaign-view-image-template" type="text/x-jquery-template">
    <div class="project-create_col-val">
        <div class="project-create_upload js-val" name="videoContent">
            <%--<div class="project-create_upload_va vert-align">
                <div class="vert-align-center "></div>
            </div>--%>
        </div>
    </div>
</script>

<script id="campaign-creator-template" type="text/x-jquery-template">
    <div class="project-author_ava">
        {{tmpl "#profile-small-avatar"}}
    </div>
    <div class="project-author_cont">
        <div class="project-author_name-label">Автор проекта</div>
        <div class="project-author_name"><a href="#">{{= displayName}}</a></div>
    </div>
</script>

<script id="campaign-interactive-description-view-image-template" type="text/x-jquery-template">
    <div class="project-media-view">
        <div class="pmv-cont">
            <span class="central-block">
                {{if $data.videoId && $data.videoUrl}}
                    <div class="video-content" data-gaEvent-click="gaSend$" data-gaParams="'event', 'video','click', 'project_video_view'"></div>
                {{else}}
                    {{if viewImageUrl}}
                        <img itemprop="image" class="pmv-img" src="{{= ImageUtils.getThumbnailUrl(viewImageUrl, ImageUtils.CAMPAIGN_VIDEO, ImageType.PHOTO)}}" alt="{{= name}}">
                    {{else imageUrl}}
                        <img itemprop="image" class="pmv-img" src="{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.CAMPAIGN_VIDEO, ImageType.PHOTO)}}" alt="{{= name}}">
                    {{/if}}
                {{/if}}
                <span class="pmv-watermark"></span>
            </span>
        </div>
    </div>
</script>

<script id="campaign-interactive-counterparty-template" type="text/x-jquery-template">
    {{if showForm || showFormAlways}}
    <div class="wrap">

        <div class="project-view-wrap cf">

            <div class="col-12">
                <div class="project-create_cont"></div>

                <div class="project-edit-container_action">
                    <button class="intr-btn intr-btn-primary btn-block js-next-step"><span class="intr-btn_in intr-btn-primary_in">Перейти к следующему шагу</span></button>
                </div>

            </div>
        </div>
    </div>
    {{/if}}
</script>

<script id="campaign-interactive-check-template" type="text/x-jquery-template">
    {{if showForm || showFormAlways}}
    <div class="video-form_cont">
        <div class="video-form_row">
            <div class="wrap-row">
                <div class="col-6">
                    <button class="intr-btn intr-btn-primary btn-block js-send-for-moderation"><span class="intr-btn_in intr-btn-primary_in">Отправить на модерацию</span></button>
                </div>
                <div class="col-6">
                    <button class="intr-btn intr-btn-default btn-block js-show-campaign"><span class="intr-btn_in intr-btn-default_in">Посмотреть проект</span></button>
                </div>
            </div>
        </div>
    </div>
    {{/if}}
</script>

<script id="campaign-interactive-sharing-modal-dialog-template" type="text/x-jquery-template">
    <div class="modal-dialog" id="modal-intr-share">
        <div class="modal modal-intr-share">


            <div class="modal-intr-share_cont">
                <div class="modal-intr-share_close">
                    <a class="close" data-dismiss="modal">×</a>
                </div>

                <div class="modal-intr-share_head">
                    Создай свой проект за 60 минут
                </div>

                <div class="modal-intr-share_body">
                    <div class="js-interactive-sharing"></div>
                </div>
            </div>


        </div>
    </div>
    <div class="modal-backdrop fade in"></div>
</script>

<script id="campaign-interactive-final-template" type="text/x-jquery-template">
    {{if showForm}}
    <div class="video-form_cont">
        <div class="video-form_row">
        </div>
    </div>
    {{/if}}
</script>
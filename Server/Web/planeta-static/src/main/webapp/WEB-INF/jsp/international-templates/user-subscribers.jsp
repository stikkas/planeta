<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script id="user-views-subscription-template" type="text/x-jquery-template">
        <div class="modal-header">
            <a class="close" data-dismiss="modal">×</a>

            <div class="n-followers_tab">
                <div class="n-followers_tab_i {{if !$data.isAdminTab}}active{{/if}}"
                     data-event-click="onSubscriptionTabClicked">
                    <span class="n-followers_tab_name">{{if workspace.appModel.isCurrentProfileMine()}}<spring:message code="user.subscription.my" /> {{/if}}{{if $data.isSubscription}}<spring:message code="user.subscription.subscription" />{{else}}<spring:message code="user.subscription.subscribers" />{{/if}}</span>
                    <span class="n-followers_tab_count">{{= count}}</span>
                </div>
                {{if PrivacyUtils.isAdmin() && $data.adminsCount}}
                    <div class="n-followers_tab_i {{if $data.isAdminTab}}active{{/if}}"
                         data-event-click="onAdminTabClicked">
                        <span class="n-followers_tab_name">{{if workspace.appModel.isCurrentProfileMine()}}<spring:message code="user.subscription.my" /> {{/if}}{{if $data.isSubscription}}<spring:message code="user.subscription.profiles" />{{else}}<spring:message code="user.subscription.admins" />{{/if}}</span>
                        <span class="s-n-follow-admin"></span>
                        <span class="n-followers_tab_count">{{= adminsCount}}</span>
                    </div>
                {{/if}}
            </div>

        </div>

        <div class="n-followers_search">
            <input class="n-followers_search_input form-control" type="text" placeholder="<spring:message code='user.subscription.search' />" data-event-keyup="debounce:onSearchQueryChanged" value="{{= query}}">
        </div>

        <div class="modal-body">
            <div class="n-followers">
                {{if isAdminTab}}
                    <div class="js-admin-list-view"></div>
                {{else}}
                    <div class="js-user-list-view"></div>
                {{/if}}
            </div>
        </div>
</script>

<script id="user-views-subscription-item-template" type="text/x-jquery-template">
    <div class="n-followers_i {{if isAdmin}}active{{/if}} {{if isSubscription}}not-change{{/if}}">
        <div class="n-followers_ava">


            <a href="/{{= alias || profileId}}"><img
                    src="{{= ImageUtils.getUserAvatarUrl(imageUrl, ImageUtils.SMALL_AVATAR, userGender)}}"></a>
            {{if $data.isNew}}
                <div class="n-followers_new">+1</div>
            {{/if}}
        </div>
        <div class="n-followers_cont">
            <div class="n-followers_name">
                <a href="/{{= alias || profileId}}">{{= displayName}}</a>

                {{if PrivacyUtils.isAdmin()}}
                    {{if isSubscription}}
                        {{if isAdmin}}
                            <div class="n-followers_action">
                                <span class="s-n-follow-admin"></span>
                            </div>
                        {{/if}}
                    {{else}}
                        <div class="n-followers_action tooltip" data-tooltip="{{if isAdmin}}<spring:message code='user.subscription.admin.unset' />{{else}}<spring:message code='user.subscription.admin.set' />{{/if}}" data-event-click="onSetAdminClicked">
                            <span class="s-n-follow-admin"></span>
                        </div>
                    {{/if}}
                {{/if}}

            </div>
        </div>
    </div>
</script>

<script id="user-views-subscription-pager-template" type="text/x-jquery-template">
    <div class="n-followers_more">
        <spring:message code="user.subscription.show.more" />
    </div>
</script>

<script id="user-views-subscription-list-empty-template" type="text/x-jquery-template">
    <div class="n-empty">
        <div class="n-empty_cover">
            <div class="n-empty_img"></div>
        </div>
        <div class="n-empty_text">
            <spring:message code="user.subscription.no.subscribers" />
        </div>
    </div>
</script>

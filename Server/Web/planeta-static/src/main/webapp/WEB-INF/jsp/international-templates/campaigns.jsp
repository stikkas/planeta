<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script id="campaign-card-widget-template" type="text/x-jquery-template"><!--className: "shop-catalogue-item"-->
<div class="item-pic-wrap">
    <div class="item-pic-bg-cover"></div>
    <div class="item-pic">
        <a class="item-pic-link" href="javascript:void(0);">
            <img class="central-block lazy"
                 data-original="{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.BIG, ImageType.PRODUCT)}}"
                 alt="{{= name}}">
            <span class="vertical-align"></span>
        </a>
    </div>
    <div class="item-pic-bg"></div>
</div>
<div class="item-cont">
    <div class="item-cont-bg"></div>
    {{if ordered}}
    <div class="thanks-to-pay"></div>
    {{/if}}
    <div class="item-info">
        <div class="item-name">
            <div class="item-name-cont">{{= StringUtils.shrinkToLength(name, 128)}}</div>
        </div>
        <div class="item-descr">
            <div class="item-descr-cont">{{html shortDescriptionHtml}}</div>
        </div>
    </div>
    <div class="item-action">
        <div class="action-info">
            <div class="clearfix btn-block">
                <a href="{{= status == 'ACTIVE' ? '//' + workspace.serviceUrls.mainHost + '/campaigns/' + webCampaignAlias : 'javascript:void(0);'}}"
                   class="btn btnhovered">
                    <spring:message code="more.about.project" text="default text"> </spring:message>
                </a>
            </div>

            {{if collectedAmount}}
            <div class="clearfix link-icon cont-va-middle">
                <i class="icon-project icon-gray"></i>
                <span class="text-icon">
                            <spring:message code="collected" text="default text"> </spring:message>:
                            <span class="hl price-count">{{= StringUtils.humanNumber(collectedAmount)}}</span>
                            <spring:message code="russian.rubles" text="default text"> </spring:message>
                        </span>
            </div>
            {{/if}}

            {{if targetAmount && (collectedAmount == 0 || timeFinish == null)}}
            <div class="clearfix link-icon cont-va-middle">
                <i class="icon-project icon-gray"></i>
                <span class="text-icon">
                            <spring:message code="target" text="default text"> </spring:message>
                            <span class="hl price-count">{{= StringUtils.humanNumber(targetAmount)}}</span>
                            <spring:message code="russian.rubles" text="default text"> </spring:message>
                        </span>
            </div>
            {{/if}}

            {{if timeFinish}}
            {{if daysDiff }}
            <div class="clearfix link-icon cont-va-middle">
                <i class="icon-alarm-clock icon-gray"></i>
                <span class="text-icon">
                                <%--think about how to fix--%>
                                <spring:message code="declension.word.stay.with.variable"
                                                text="default text"> </spring:message>
                                <span class="hl">${daysDiff}</span>
                                <spring:message code="declension.word.day.with.variable"
                                                text="default text"> </spring:message>
                            </span>
            </div>
            {{else hoursDiff }}
            <div class="clearfix link-icon cont-va-middle">
                <i class="icon-alarm-clock icon-gray"></i>
                <span class="text-icon">
                                <%--think about how to fix--%>
                                <spring:message code="declension.word.stay.with.variable"
                                                text="default text"> </spring:message>
                                <span class="hl">${hoursDiff}</span>
                                <spring:message code="declension.word.hour.with.variable"
                                                text="default text"> </spring:message>
                            </span>
            </div>
            {{/if}}
            {{else}}
            <div class="clearfix link-icon cont-va-middle">
                <i class="icon-alarm-clock icon-gray"></i>
                <span class="text-icon">
                            <spring:message code="project.is.finished" text="default text"> </spring:message>
                        </span>
            </div>
            {{/if}}

            {{if targetAmount }}
            <div class="clearfix">
                <div class="progress-bar">
                    <div class="pln-progress-block">
                        <div class="pln-progress">
                            <div class="prs-progress">
                                <div class="pr-bar" style="width:{{= progress}}%;"></div>
                                {{each over100ProgressBars}}
                                <div class="pr-bar-over" style="width:{{= $value}}%;"></div>
                                {{/each}}
                            </div>
                            <div class="pr-value">{{= progressText}}%</div>
                        </div>
                    </div>
                </div>
            </div>
            {{else}}
            <div class="clearfix link-icon cont-va-middle">
                <i class="icon-action icon-gray"></i>
                <span class="text-icon">
                            <spring:message code="declension.word.sell" text="default text"> </spring:message>
                            <span class="hl price-count">${purchaseCount}</span>
                            <spring:message code="declension.word.time" text="default text"> </spring:message>
                        </span>
            </div>
            {{/if}}
        </div>
    </div>
</div>
</script>

<script id="campaign-subscription-template" type="text/x-jquery-template">
    {{if isActive}}
    <span class="pvc-share-link {{if isSubscribed}} active{{/if}}">
            {{if isSubscribed}}
                <i class="pvc-share-tick"></i>
                <spring:message code="signed" text="default text"> </spring:message>
            {{else}}
                <spring:message code="subscribe.to.author" text="default text"> </spring:message>
            {{/if}}
        </span>
    {{/if}}
</script>

<script id="group-list-template" type="text/x-jquery-template">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">×</a>
        <div class="modal-title">
            <spring:message code="create.new.project" text="default text"> </spring:message>
        </div>
    </div>

    <div class="modal-body">
        <p><spring:message code="select.the.profile" text="default text"> </spring:message></p>
        <div class="modal-project-create-container">
            <div class="modal-scroll">
                <div class="modal-scroll-content">
                </div>
            </div>
        </div>
    </div>

    <div class="modal-footer">
        <div class="modal-footer-cont">
            <button type="reset" href="javascript:void(0)" class="btn">
                <spring:message code="cancel" text="default text"> </spring:message>
            </button>
        </div>
    </div>
</script>

<script id="group-item-template" type="text/x-jquery-template">
    {{if profileType == "USER"}}
    <div class="avatar">
        <img src="{{= ImageUtils.getThumbnailUrl(smallImageUrl, ImageUtils.SMALL_AVATAR, ImageType.USER)}}"
             alt="{{= displayName}}">
    </div>

    <div class="content">
        <div class="name">{{= displayName}}</div>
    </div>
    {{else}}
    <div class="avatar">
        <img src="{{= ImageUtils.getThumbnailUrl(smallImageUrl, ImageUtils.SMALL_AVATAR, ImageType.GROUP)}}"
             alt="{{= displayName}}">
    </div>

    <div class="content">
        <div class="name">{{= displayName}}</div>
        <div class="members-count">{{= usersCount}} <spring:message code="declension.word.member"
                                                                    text="default text"> </spring:message></div>
    </div>
    {{/if}}
</script>

<script id="campaigns-list-loading-template" type="text/x-jquery-template">
    <div class="list-loader">
        <a class="load" href="javascript:void(0)">
            <spring:message code="loading" text="default text"> </spring:message>...
        </a>
    </div>
</script>

<script id="campaign-feedback-form-template" type="text/x-jquery-template">
    <a class="close">×</a>
    <div class="modal-project-feedback-step-1 fade{{if step}} hide{{else}} in{{/if}}">
        <div class="m-project-feedback-title">
            <spring:message code="who.do.you.want.to.apply" text="default text"> </spring:message>
        </div>

        <div class="m-project-feedback-questions clearfix">
            <div class="mp-feedback-questions">
                <div class="mpf-questions-inner">
                    <div class="mpf-questions-inner-title">
                        <spring:message code="questions.for.author" text="default text"> </spring:message>
                    </div>

                    <div class="mpf-questions-inner-item">
                        <spring:message code="if.you.have.questions" text="default text"> </spring:message><br>
                    </div>

                    <a class="btn next-step" data-target="curators" href="javascript:void(0);">
                        <spring:message code="contact.curator" text="default text"> </spring:message>
                    </a>
                </div>
            </div>

            <div class="mp-feedback-questions">
                <div class="mpf-questions-inner mp-fi-right">
                    <div class="mpf-questions-inner-title">
                        <spring:message code="example.of.questions" text="default text"> </spring:message>
                    </div>

                    <div class="mpf-questions-inner-item">
                        <spring:message code="if.you.need.more.information" text="default text"> </spring:message>
                    </div>

                    <a class="btn btn-primary next-step" data-target="author" href="javascript:void(0);">
                        <spring:message code="contact.author" text="default text"> </spring:message>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <form class="modal-project-feedback-form fade{{if $data.step == 2}} in{{else}} hide{{/if}}">
        <div class="js-curators-feedback-info modal-question-info{{if $data.target != 'curators'}} hide{{/if}}">
            <spring:message code="write.message.for.curator" text="default text"> </spring:message>
        </div>

        <div class="modal-select-header">
            <div class="fieldset">
                <label><spring:message code="write.your.question" text="default text"> </spring:message></label>
                <div class="input-field input-field-l">
                    <textarea class="input-l" rows="14" id="message" name="message"
                              placeholder="<spring:message code="write.here" text="default text"> </spring:message>"></textarea>
                </div>
            </div>

            {{if askEmail}}
            <div class="fieldset">
                <label><spring:message code="enter.your.email" text="default text"> </spring:message></label>
                <div class="input-field input-field-l">
                    <input class="input-l" type="text" id="email" name="email">
                </div>
            </div>
            {{/if}}

            <input type="hidden" id="campaignId" name="campaignId" value="{{= campaignId}}"/>
        </div>

        <div class="text-center">
            <button type="submit" class="btn btn-primary">
                <spring:message code="ask.question" text="default text"> </spring:message>
            </button>
        </div>
    </form>

    <div class="modal-feedback-success clearfix fade hide">
        <div class="content-alert-success">
            <i class="three-types-alerts-success"></i>
            <p class="big">
                <spring:message code="message.sent" text="default text"> </spring:message>
            </p>
        </div>
    </div>
</script>

<script id="campaign-admin-capability" type="text/x-jquery-template">
    <div class="project-author-controls">
        <div class="wrap">
            <div class="project-author-controls_list">
                {{if canSendNews}}
                <div class="project-author-controls_i">
                    <span class="project-author-controls_link js-add-update">
                    <span class="project-author-controls_ico">
                        <svg width="60" height="60" viewBox="0 0 60 60">
                            <path d="M38.212 17.494H13.823c-1.557 0-2.823 1.302-2.823 2.903v20.167c0 1.601 1.267 2.903 2.823 2.903h9.18l-1.044 1.156h-1.578a.665.665 0 0 0-.655.674c0 .373.293.674.655.674h19.346a.664.664 0 0 0 .655-.674.665.665 0 0 0-.655-.674h-1.578l-1.044-1.156h9.18c1.557 0 2.823-1.302 2.823-2.903V20.397c0-1.601-1.266-2.903-2.823-2.903h-8.073zM12.419 41.166V20.143h35.269v21.023H12.419zm1.404-22.324h32.462c.626 0 1.164.537 1.393 1.301H12.43c.229-.764.768-1.301 1.393-1.301zm22.536 25.781h-12.61l1.044-1.156h10.522l1.044 1.156zm9.926-2.504H13.823a1.514 1.514 0 0 1-1.393-.952h35.248a1.513 1.513 0 0 1-1.393.952z"
                                  class="project-author-controls_c-b"></path><path
                                d="M45.476 30.632c-6.886 0-12.458-5.709-12.458-12.737 0-7.029 5.572-12.737 12.458-12.737 6.887 0 12.459 5.708 12.459 12.737 0 7.028-5.572 12.737-12.459 12.737z"
                                fill="#FFF"></path><path
                                d="M45.59 27.539c5.129 0 9.297-4.27 9.297-9.552s-4.168-9.553-9.297-9.553c-5.129 0-9.298 4.271-9.298 9.553 0 5.282 4.169 9.552 9.298 9.552zm0 1.593c-6.008 0-10.878-4.99-10.878-11.145 0-6.155 4.87-11.145 10.878-11.145 6.007 0 10.878 4.99 10.878 11.145 0 6.155-4.871 11.145-10.878 11.145z"
                                class="project-author-controls_c-a"></path><path
                                d="M40.945 19.019c-.136 0-.221-.024-.255-.071-.033-.047-.05-.131-.05-.252v-1.051c0-.108.017-.192.05-.253.034-.061.119-.091.255-.091h3.76v-3.84c0-.135.027-.219.081-.253a.476.476 0 0 1 .244-.05h1.118c.108 0 .186.017.233.05.048.034.071.118.071.253v3.84h3.76c.122 0 .2.03.234.091a.522.522 0 0 1 .051.253v1.051c0 .121-.017.205-.051.252-.034.047-.112.071-.234.071h-3.76v3.86c0 .135-.023.219-.071.253-.047.033-.125.05-.233.05H45.03a.476.476 0 0 1-.244-.05c-.054-.034-.081-.118-.081-.253v-3.86h-3.76z"
                                class="project-author-controls_c-a"></path></svg>
                    </span>
                        <span class="project-author-controls_name"
                              title="<spring:message code="write.to.blog" text="default text"> </spring:message>"> <spring:message
                                code="add.new" text="default text"> </spring:message> </span>
                    </span>
                </div>
                {{/if}}
                {{if canChangeCampaign}}
                <div class="project-author-controls_i">
                    <a href="/campaigns/{{= webCampaignAlias}}/edit" class="project-author-controls_link">
                    <span class="project-author-controls_ico">
                        <svg width="60" height="60" viewBox="0 0 60 60">
                            <path d="M37.05 12.714L24.498 25.055a.779.779 0 0 0-.206.355l-1.077 4.079a.77.77 0 0 0 .206.746.796.796 0 0 0 .758.203l4.149-1.059a.796.796 0 0 0 .361-.203L41.24 16.835l.001-.001v-.001l2.527-2.484a.77.77 0 0 0 0-1.101l-3.071-3.02a.8.8 0 0 0-1.119 0l-2.527 2.484-.001.001v.001zm-9.328 15.212l-2.637.673.685-2.593 11.84-11.642 1.952 1.92-11.84 11.642zm12.415-16.047l1.952 1.92-1.408 1.384-1.952-1.92 1.408-1.384z"
                                  class="project-author-controls_c-a"></path><path
                                d="M42.841 18.574c-.431 0-.591.35-.591.781v25.257H17.346V14.436h14.67c.431 0 .77-.193.77-.624a.771.771 0 0 0-.77-.78H16.781a.78.78 0 0 0-.781.78v31.407c0 .431.35.781.781.781h26.06c.431 0 .781-.35.781-.781V19.355a.781.781 0 0 0-.781-.781z"
                                class="project-author-controls_c-b"></path></svg>
                    </span>
                        <span class="project-author-controls_name"> <spring:message code="edit"
                                                                                    text="default text"> </spring:message> </span>
                    </a>
                </div>
                {{/if}}

                {{if isModerationButtonsVisible}}
                <div class="project-author-controls_i">
                    <a href="/campaigns/{{= webCampaignAlias}}/advertising-tools" class="project-author-controls_link">
                    <span class="project-author-controls_ico">
                        <svg width="60" height="60" viewBox="0 0 60 60">
                            <path d="M40.943 11h-2.357c-1.109 0-2.038.799-2.258 1.86-1.809.919-10.687 5.188-22.378 6.751a2.323 2.323 0 0 0-1.585.962h-2.058C9.035 20.573 8 21.624 8 22.916v4.786c0 1.292 1.035 2.344 2.307 2.344h2.058c.364.511.928.873 1.585.961l.351.049 3.618 13.228A2.32 2.32 0 0 0 20.142 46h5.09a.743.743 0 0 0 .737-.748.743.743 0 0 0-.737-.748h-.225l-3.286-12.012c7.726 1.912 13.226 4.565 14.607 5.266.22 1.061 1.149 1.86 2.258 1.86h2.357c1.272 0 2.307-1.051 2.307-2.343V13.343c0-1.292-1.035-2.343-2.307-2.343zM11.928 28.55h-1.621a.842.842 0 0 1-.834-.848v-4.786c0-.468.374-.848.834-.848h1.621v6.482zm11.55 15.954h-3.336a.84.84 0 0 1-.804-.621l-3.443-12.587c1.45.236 2.85.51 4.192.811l3.391 12.397zm18.299-7.229a.842.842 0 0 1-.835.847h-2.356a.842.842 0 0 1-.835-.847v-17.55a.74.74 0 0 0-.736-.747.742.742 0 0 0-.737.747v16.346c-2.899-1.398-11.351-5.105-22.136-6.547a.854.854 0 0 1-.742-.841v-6.748c0-.423.319-.785.742-.841 10.785-1.442 19.237-5.149 22.136-6.547v7.585c0 .413.33.748.737.748a.742.742 0 0 0 .736-.748v-8.783c0-.473.375-.853.835-.853h2.356c.461 0 .835.38.835.847v23.932z"
                                  class="project-author-controls_c-b"></path><path
                                d="M54.274 24.382H46.53c-.401 0-.726.346-.726.772 0 .427.325.772.726.772h7.744c.401 0 .726-.345.726-.772 0-.426-.325-.772-.726-.772zM53.398 17.135c-.187-.396-.69-.58-1.125-.41l-5.949 2.043c-.435.169-.637.628-.45 1.024.139.296.456.473.788.473a.924.924 0 0 0 .337-.064l5.949-2.042c.435-.17.636-.628.45-1.024zM52.948 32.15l-5.949-2.043c-.435-.169-.939.014-1.125.41-.187.396.015.855.45 1.024l5.949 2.043c.11.043.224.063.337.063.332 0 .649-.177.788-.473.186-.396-.015-.854-.45-1.024z"
                                class="project-author-controls_c-a"></path></svg>
                    </span>
                        <span class="project-author-controls_name"> <spring:message code="advertising.tools"
                                                                                    text="default text"> </spring:message> </span>
                    </a>
                </div>
                {{/if}}

                {{if false && notDeleted}}
                <div class="project-author-controls_i">
                    <a href="#" class="project-author-controls_link js-change-bg">
                    <span class="project-author-controls_ico">
                        <svg width="60" height="60" viewBox="0 0 60 60"><path
                                d="M51.241 11H8.759a.76.76 0 0 0-.759.761v33.478c0 .421.34.761.759.761h42.482a.76.76 0 0 0 .759-.761V11.761a.76.76 0 0 0-.759-.761zm-.758 33.478H9.517V12.522h40.966v31.956z"
                                class="project-author-controls_c-b"></path><path
                                d="M20.128 27.935a4.237 4.237 0 0 0 4.231-4.233 4.237 4.237 0 0 0-4.231-4.234 4.237 4.237 0 0 0-4.231 4.234 4.237 4.237 0 0 0 4.231 4.233zm0-6.947a2.717 2.717 0 0 1 2.712 2.714 2.716 2.716 0 0 1-2.712 2.713 2.715 2.715 0 0 1-2.711-2.713 2.716 2.716 0 0 1 2.711-2.714zM13.269 41.484a.75.75 0 0 0 .499-.19l12.335-10.928 7.79 7.838a.75.75 0 0 0 1.069 0 .762.762 0 0 0 0-1.075l-3.635-3.658 6.942-7.65 8.515 7.855a.754.754 0 0 0 1.069-.048.764.764 0 0 0-.047-1.075l-9.074-8.37a.754.754 0 0 0-1.069.048l-7.406 8.162-3.586-3.608a.753.753 0 0 0-1.034-.034L12.77 40.152a.765.765 0 0 0-.069 1.074c.15.171.359.258.568.258z"
                                class="project-author-controls_c-a"></path></svg>
                    </span>
                        <span class="project-author-controls_name"> <spring:message code="change.background"
                                                                                    text="default text"> </spring:message> </span>
                    </a>
                </div>
                {{/if}}

                {{if PrivacyUtils.isAdmin() && (status == 'DRAFT' || status == 'NOT_STARTED' || status == 'PATCH' ||
                status == 'APPROVED')}}
                <div class="project-author-controls_i project-draft drop-down">
                <span class="project-author-controls_link drop-target">
                    <span class="project-author-controls_ico">
                        <svg width="60" height="60" viewBox="0 0 60 60"><path
                                d="M38.91 26.054a3.622 3.622 0 0 0-5.062-.818L31.3 27.078a3.635 3.635 0 0 0-1.257 4.262l-1.457.984a3.624 3.624 0 0 0-4.695-.385l-2.549 1.843a3.633 3.633 0 0 0-.817 5.068 3.624 3.624 0 0 0 5.062.818l2.549-1.843a3.635 3.635 0 0 0 1.263-4.244l1.464-.989a3.624 3.624 0 0 0 4.682.372l2.548-1.843a3.63 3.63 0 0 0 .817-5.067zM27.279 36.6l-2.584 1.867a2.13 2.13 0 0 1-2.976-.48 2.137 2.137 0 0 1 .48-2.98l2.584-1.867a2.13 2.13 0 0 1 2.544.035l-1.233.833a.748.748 0 0 0 .837 1.24l1.189-.803a2.136 2.136 0 0 1-.841 2.155zm9.957-6.704l-2.584 1.868a2.13 2.13 0 0 1-2.528-.024l1.06-.716a.749.749 0 0 0-.837-1.24l-1.027.694a2.134 2.134 0 0 1 .837-2.174l2.584-1.868a2.128 2.128 0 0 1 2.975.481 2.136 2.136 0 0 1-.48 2.979z"
                                class="project-author-controls_c-a"></path><path
                                d="M12.5 11v35h35V11h-35zm33.511 1.489v5.938H13.989v-5.938h32.022zM13.989 44.511V19.916h32.022v24.595H13.989z"
                                class="project-author-controls_c-b"></path><path
                                d="M35.645 14.952h1.694v1.129h-1.694zM39.032 14.952h1.694v1.129h-1.694zM42.419 14.952h1.694v1.129h-1.694zM15.887 14.952h12.984v1.129H15.887z"
                                class="project-author-controls_c-a"></path></svg>
                    </span>
                    <span class="project-author-controls_name"> <spring:message code="draft.link"
                                                                                text="default text"> </spring:message> </span>
                </span>

                    <div class="drop-content">
                        <div class="project-draft_popup">
                            <div class="project-draft_popup-cont">
                                <div class="project-draft_switch">
                                    <div class="project-draft_switch_lbl"><spring:message code="open.access"
                                                                                          text="default text"> </spring:message></div>
                                    <div class="project-draft_switch_val {{if draftVisible}}on{{/if}}">
                                        <div class="project-draft_switch_val_on"><spring:message code="on"
                                                                                                 text="default text"> </spring:message></div>
                                        <div class="project-draft_switch_val_off"><spring:message code="off"
                                                                                                  text="default text"> </spring:message></div>
                                    </div>
                                </div>
                                <div class="project-draft_footer">
                                    <div class="project-draft_text"
                                         style="{{if draftVisible}}display: none;{{else}}display: block;{{/if}}">
                                        <spring:message code="draft.will.be.able" text="default text"> </spring:message>
                                    </div>
                                    <div class="project-draft_form_wrap"
                                         style="{{if draftVisible}}display: block;{{else}}display: none;{{/if}}">
                                        <div class="project-draft_form">
                                            <div class="project-draft_form_val">
                                                <input type="text" class="form-control" id="js-draft-link-copy"
                                                       value="https://{{= workspace.serviceUrls.mainHost}}/campaigns/{{= campaignId}}"
                                                       readonly="">
                                            </div>
                                            <div class="project-draft_form_btn">
                                                <span class="btn btn-light"
                                                      data-tooltip="<spring:message code="copy" text="default text"> </spring:message>"
                                                      data-tooltip-position="bottom center"
                                                      data-clipboard-text="https://{{= workspace.serviceUrls.mainHost}}/campaigns/{{= campaignId}}">
                                                    <span class="copy-ico"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{/if}}

                {{if PrivacyUtils.isAdmin()}}
                <div class="project-author-controls_i">
                    <a href="/campaigns/{{= webCampaignAlias}}/orders" class="project-author-controls_link">
                                    <span class="project-author-controls_ico">
                                        <svg width="60" height="60" viewBox="0 0 60 60">
                                            <path d="M47.147 45.505h-1.628V21.697a2.005 2.005 0 0 0-1.996-2.009h-3.641a2.004 2.004 0 0 0-1.996 2.009v23.808H33.75V32.484c0-1.108-.895-2.01-1.996-2.01h-3.64a2.005 2.005 0 0 0-1.996 2.01v13.021h-4.136v-6.744a2.005 2.005 0 0 0-1.996-2.009h-3.641c-1.1 0-1.996.901-1.996 2.009v6.744h-1.716a.745.745 0 0 0-.743.747c0 .413.332.748.743.748h34.514a.745.745 0 0 0 .743-.748.745.745 0 0 0-.743-.747zm-26.65 0h-4.663v-6.744c0-.283.229-.514.511-.514h3.641c.281 0 .511.231.511.514v6.744zm11.768 0h-4.662V32.484c0-.284.229-.515.511-.515h3.64c.282 0 .511.231.511.515v13.021zm11.769 0h-4.663V21.697c0-.284.229-.514.511-.514h3.641c.281 0 .511.23.511.514v23.808z"
                                                  class="project-author-controls_c-b"></path><path
                                                d="M45.528 9.785a.748.748 0 0 0-.215-.566.73.73 0 0 0-.561-.217.434.434 0 0 0-.036-.002h-4.474a.743.743 0 0 0-.739.746c0 .413.331.747.739.747h2.763l-7.311 7.379-2.756-2.783a.737.737 0 0 0-1.046 0L18.598 28.507a.753.753 0 0 0 0 1.056.738.738 0 0 0 1.046 0l12.771-12.891 2.756 2.783a.736.736 0 0 0 1.046 0l7.833-7.907v2.524c0 .412.331.746.74.746a.743.743 0 0 0 .739-.746V9.821l-.001-.036z"
                                                class="project-author-controls_c-a"></path></svg>
                                    </span>
                        <span class="project-author-controls_name"> <spring:message code="statistic"
                                                                                    text="default text"> </spring:message> </span>
                    </a>
                </div>
                {{if stage}}
                <div class="project-author-controls_i">
                    <a class="project-author-controls_link" href="/welcome/promotion_{{= stage || 1}}.html"
                       data-gaEvent-click="gaSend$" data-gaParams="'event', 'view','click', 'project_instructions'">
                                    <span class="project-author-controls_ico">
                                        <svg width="60" height="60" viewBox="0 0 60 60">
                                            <path d="M39.688 11h-29.44c-1.879 0-3.408 1.592-3.408 3.55v24.651c0 1.958 1.529 3.55 3.408 3.55h11.081l-1.26 1.413h-1.905c-.437 0-.791.369-.791.824 0 .455.354.824.791.824h23.352c.437 0 .791-.369.791-.824 0-.455-.354-.824-.791-.824h-1.905l-1.26-1.413h11.081c1.879 0 3.408-1.592 3.408-3.55V14.55c0-1.958-1.529-3.55-3.408-3.55h-9.744zM8.118 39.566V13.831h43.444v25.735H8.118zm1.717-27.279h40.01c.771 0 1.434.637 1.717 1.544H8.118c.283-.907.946-1.544 1.717-1.544zm40.01 28.823H9.835c-.771 0-1.434-.637-1.717-1.544h43.444c-.283.907-.946 1.544-1.717 1.544zm-12.394 3.054H22.229l1.261-1.413h12.7l1.261 1.413z"
                                                  class="project-author-controls_c-b"></path><path
                                                d="M22.684 26.443a.936.936 0 0 1-.216.617l-4.688 5.323a.717.717 0 0 1-.505.235.672.672 0 0 1-.523-.256.983.983 0 0 1 .018-1.214l4.147-4.705-4.147-4.706c-.289-.32-.289-.873-.018-1.214.27-.341.739-.341 1.028-.021l4.688 5.323c.144.17.216.383.216.618zM32.907 26.443a.932.932 0 0 1-.217.617l-4.688 5.323a.715.715 0 0 1-.505.235.673.673 0 0 1-.523-.256.983.983 0 0 1 .018-1.214l4.147-4.705-4.147-4.706c-.289-.32-.289-.873-.018-1.214.27-.341.739-.341 1.028-.021l4.688 5.323c.145.17.217.383.217.618zM43.129 26.443a.932.932 0 0 1-.217.617l-4.688 5.323a.715.715 0 0 1-.505.235.673.673 0 0 1-.523-.256.985.985 0 0 1 .018-1.214l4.148-4.705-4.148-4.706c-.288-.32-.288-.873-.018-1.214.271-.341.74-.341 1.028-.021l4.688 5.323c.145.17.217.383.217.618z"
                                                class="project-author-controls_c-a"></path></svg>
                                    </span>
                        <span class="project-author-controls_name"> <spring:message code="campaign.develop"
                                                                                    text="default text"> </spring:message> </span>
                    </a>
                </div>
                {{/if}}
                {{/if}}
            </div>
        </div>
    </div>
    <div class="project-admin-controls">
        <div class="wrap">
            <div class="project-admin-controls_cont">
                <div class="project-admin-controls_info">

                </div>

                <div class="project-admin-controls_action">
                    {{if isModerationButtonsVisible}}
                    <div class="project-admin-controls_action-i">
                        <a href="https://{{= workspace.serviceUrls.adminHost }}/moderator/campaign-moderation-info.html?campaignId={{= campaignId}}"
                           class="btn btn-nd-default">
                            <spring:message code="moderation" text="default text"> </spring:message>
                        </a>
                    </div>
                    {{/if}}

                    {{if canPauseCampaign}}
                    <div class="project-admin-controls_action-i">
                        <span class="btn btn-nd-danger js-pause-campaign">
                            <spring:message code="stop" text="default text"> </spring:message>
                        </span>
                    </div>
                    {{/if}}

                    {{if canStartCampaign}}
                    <div class="project-admin-controls_action-i">
                        <span class="btn btn-nd-success js-start-campaign">
                            <spring:message code="run" text="default text"> </spring:message>
                        </span>
                    </div>
                    {{/if}}
                </div>
            </div>
        </div>
    </div>
</script>

<script id="tag-and-location" type="text/x-jquery-template">
    <div class="project-view-add">
        {{if tags[0] }}
        <div class="project-view-add_i">
            <a href="/search/projects?query=&categories={{= tags[0].mnemonicName}}" class="project-view-add_link">
                {{if lang == "ru"}}{{= tags[0].name}}{{else}}{{= tags[0].engName}}{{/if}}
            </a>
        </div>
        {{/if}}

        {{if cityId != 0 || regionId != 0 || countryId != 0 }}
        <div class="project-view-add_i">
            <a href="/search/projects?query=&{{if cityId != 0}}cityId={{= cityId}}{{else regionId != 0}}regionId={{= regionId}}{{else countryId != 0}}countryId={{= countryId}}{{/if}}"
               class="project-view-add_link">
                {{if cityId != 0}}
                    {{if lang == "ru"}}{{= cityNameRus}}{{else}}{{= cityNameEng}}{{/if}}
                {{else regionId != 0}}
                    {{if lang == "ru"}}{{= regionNameRus}}{{else}}{{= regionNameEng}}{{/if}}
                {{else countryId != 0}}
                    {{if lang == "ru"}}{{= countryNameRus}}{{else}}{{= countryNameEng}}{{/if}}
                {{/if}}
            </a>
        </div>
        {{/if}}
    </div>
</script>

<script id="finish-campaign-traffic-product-card-list-template" type="text/x-jquery-template">
    <div class="project-products">
        <div class="project-products_wrap">
            <div class="project-products_head">
                <div class="status-badge status-badge__m status-badge__success">
                    <spring:message code="project.products.shop" text="default text"> </spring:message>
                </div>
            </div>
            <div class="project-products_list">
                <div class="product-card product-card__small js-list"></div>
            </div>
        </div>
    </div>
</script>

<script id="campaign-base-common-template" type="text/x-jquery-template">
    <div class="wrap">
        {{if status == 'FINISHED'}}
            <div class="js-author-active-campaign"></div>
        {{/if}}

        <div class="project-view-top">
            <div class="wrap-row">
                <div class="m-col-ld-8 col-12">
                    {{if status == 'NOT_STARTED' || status == 'FINISHED' || status == 'PATCH' || status == 'PAUSED' || status == 'APPROVED' || status == 'DECLINED' || status == 'ACTIVE'}}
                        <div class="project-view-status">
                            <div class="status-badge status-badge__m status-badge__{{if status == 'FINISHED' && targetStatus == 'SUCCESS'}}success{{else status == 'FINISHED'}}finish{{else status == 'NOT_STARTED' || status == 'PATCH'}}moderate{{else status == 'PAUSED'}}pause{{else status == 'APPROVED'}}approved{{else status == 'DECLINED'}}canceled{{else status == 'ACTIVE'}}info{{/if}}">
                                {{if status == 'FINISHED' && targetStatus == 'SUCCESS'}}
                                    <spring:message code="project.status.success" text="default text"> </spring:message>
                                {{else status == "FINISHED"}}
                                    <spring:message code="project.status.finished" text="default text"> </spring:message>
                                {{else status == 'NOT_STARTED'}}
                                    <spring:message code="project.status.moderation" text="default text"> </spring:message>
                                {{else status == 'PATCH'}}
                                    <spring:message code="project.status.patch" text="default text"> </spring:message>
                                {{else status == 'PAUSED'}}
                                    <spring:message code="project.status.paused" text="default text"> </spring:message>
                                {{else status == 'APPROVED'}}
                                    <spring:message code="project.status.approved" text="default text"> </spring:message>
                                {{else status == 'DECLINED'}}
                                    <spring:message code="project.status.declined" text="default text"> </spring:message>
                                    {{else}}
                                <spring:message code="project.status.active" text="default text"> </spring:message>
                                {{/if}}
                            </div>
                        </div>
                    {{/if}}

                    <div class="project-view-name"><h1>{{= StringUtils.shrinkToLength(name, 128)}}</h1></div>

                    <div class="project-view-short-description d-tp-block d-none">
                        <span class="project-view-short-description_span">{{html shortDescriptionHtml}}</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="project-view-card {{if status=='FINISHED'}}project-finish{{/if}}">
            <div class="project-view-card_add d-ld-none d-tp-block d-none">
                {{tmpl "#tag-and-location"}}
            </div>

            {{if (status == 'ACTIVE' || status == 'FINISHED')}}
                <div class="project-view-card_share pln-dropdown">
                    <span class="project-view-card_share-switch pln-d-switch">
                        <spring:message code="share" text="default text"> </spring:message>
                    </span>
                    <div class="project-view-card_share-popup pln-d-popup">
                        <div class="project-view-share">
                            {{if targetStatus != 'FAIL' || (targetStatus == 'FAIL' && PrivacyUtils.hasAdministrativeRole()) }}
                                <div class="project-view-share_block">
                                    <div class="project-view-share_lbl"><spring:message code="share"
                                                                                        text="default text"> </spring:message> —
                                    </div>
                                    <div class="project-view-share_val">
                                        <div class="sharing-popup-social sharing-mini"></div>
                                    </div>
                                </div>
                            {{/if}}
                            <div class="project-view-share_code">
                                <span class="project-view-share_code-link js-promo-widgets"> &lt;<spring:message code="campaign.code" text="default text"> </spring:message> &gt; </span>
                            </div>
                        </div>
                    </div>
                </div>
            {{/if}}

            <div class="project-view-card_cont">
                <div class="project-view-card_media">
                    <div class="project-view-card_media-in">
                        {{if videoId && videoUrl}}
                            <div class="video-content" data-gaEvent-click="gaSend$" data-gaParams="'event', 'video','click', 'project_video_view'"></div>
                        {{else}}
                            {{if viewImageUrl}}
                                <img src="{{= ImageUtils.getThumbnailUrl(viewImageUrl, ImageUtils.CAMPAIGN_VIDEO, ImageType.PHOTO)}}" alt="{{= name}}">
                            {{else imageUrl}}
                                <img src="{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.CAMPAIGN_VIDEO, ImageType.PHOTO)}}" alt="{{= name}}">
                            {{/if}}
                        {{/if}}
                    </div>
                </div>

                <div class="project-view-card_meta">
                    <div class="project-view-meta">
                        <div class="project-view-meta_head">
                            <div class="project-view-meta_amount">
                                <div class="project-view-meta_amount-lbl">
                                    <spring:message code="collected.without.colon"
                                                    text="default text"> </spring:message>
                                </div>
                                <div class="project-view-meta_amount-val">
                                    <div class="pvc-counter js-campaign-odometer odometer-auto-theme"></div>
                                    <span class="project-view-meta_amount-cur"> ₽</span>
                                </div>
                            </div>

                            {{if targetAmount && Math.floor(100*collectedAmount/targetAmount) > 0}}
                                <div class="project-view-meta_percent">
                                    <div class="project-view-meta_percent-lbl">
                                        <spring:message code="collected.without.colon"
                                                        text="default text"> </spring:message> %
                                    </div>
                                    <div class="project-view-meta_percent-val">
                                        {{= Math.floor(100*collectedAmount/targetAmount)}}
                                    </div>
                                </div>
                            {{/if}}
                        </div>

                        {{if targetAmount > 0}}
                            <div class="project-view-meta_progress {{if status=='FINISHED'}}closed-progress{{else progress >= 100}}over-progress{{/if}}">
                                <div class="project-view-meta_bar" style='width: {{= progress %100}}%;'>
                                    <div class="pvc-bar-ctrl"></div>
                                </div>
                            </div>
                        {{/if}}

                        <div class="project-view-meta_info">
                            <div class="project-view-info">
                                {{if targetAmount}}
                                    <div class="project-view-info_i">
                                        <div class="project-view-info_lbl">
                                            <spring:message code="target.of.project" text="default text"> </spring:message>, ₽
                                        </div>
                                        <div class="project-view-info_val">{{= StringUtils.humanNumber(targetAmount)}}</div>
                                    </div>
                                {{/if}}

                                {{if (!finishOnTargetReach && timeFinish != null) && !(status == "FINISHED")}}
                                    <div class="project-view-info_i" data-tooltip='<spring:message code="campaigns-edit.jsp.propertie.66" text="default text"></spring:message> {{html DateUtils.formatDate(timeFinish, "D MMMM YYYY", false, true)}}' data-tooltip-position="top left">
                                        <div class="project-view-info_lbl">
                                            {{if DateUtils.getHoursTo(timeFinish) > 48}}
                                                <spring:message code="declension.word.stay2.with.variable" text="default text"> </spring:message>
                                            {{else DateUtils.getMinutesTo(timeFinish) >= 60}}
                                                <spring:message code="declension.word.stay3.with.variable" text="default text"> </spring:message>
                                            {{else DateUtils.getMinutesTo(timeFinish) > 0}}
                                                <spring:message code="declension.word.stay6.with.variable" text="default text"> </spring:message>
                                            {{/if}}
                                        </div>

                                        <div class="project-view-info_val">
                                            {{if DateUtils.getHoursTo(timeFinish) > 48}}
                                                {{= DateUtils.getDaysTo(timeFinish)}}
                                                <spring:message code="declension.word.day2.with.variable" text="default text"> </spring:message>
                                            {{else DateUtils.getMinutesTo(timeFinish) >= 60}}
                                                {{= DateUtils.getHoursTo(timeFinish)}}
                                                <spring:message code="declension.word.hour2.with.variable" text="default text"> </spring:message>
                                            {{else DateUtils.getMinutesTo(timeFinish) > 0}}
                                                {{= DateUtils.getMinutesTo(timeFinish)}}
                                                <spring:message code="declension.word.minutes.with.variable" text="default text"> </spring:message>
                                            {{/if}}
                                        </div>
                                    </div>
                                {{/if}}

                                {{if status == "FINISHED" && timeFinish != null}}
                                    <div class="project-view-info_i">
                                        <div class="project-view-info_lbl">
                                            <spring:message code="project.is.finished" text="default text"> </spring:message>
                                        </div>
                                        <div class="project-view-info_val">
                                            {{html DateUtils.campaignFormatDate2(timeFinish)}}
                                        </div>
                                    </div>
                                {{/if}}

                                {{if timeStart != null && status != "FINISHED" && timeFinish == null}}
                                    <div class="project-view-info_i">
                                        <div class="project-view-info_lbl">
                                            <spring:message code="project.is.started" text="default text"> </spring:message>
                                        </div>
                                        <div class="project-view-info_val">
                                            {{html DateUtils.campaignFormatDate(timeStart)}}
                                        </div>
                                    </div>
                                {{/if}}

                                <div class="project-view-info_i">
                                    <div class="project-view-info_lbl">
                                        <spring:message code="purchase" text="default text"/>
                                    </div>
                                    <div class="project-view-info_val">
                                        {{= StringUtils.humanNumber(purchaseCount)}} <spring:message code="declension.word.time" text="default text"> </spring:message>
                                    </div>
                                </div>

                                <div class="project-view-info_i">
                                    {{if timeStart != null && status != "FINISHED" && timeFinish != null}}
                                        <div class="project-view-info_lbl">
                                            <spring:message code="project.is.started" text="default text"> </spring:message>
                                        </div>
                                        <div class="project-view-info_val">
                                            {{html DateUtils.campaignFormatDate(timeStart)}}
                                        </div>
                                    {{/if}}
                                </div>
                            </div>
                        </div>

                        <div class="project-view-meta_add d-ld-block d-tp-none">
                            {{tmpl '#tag-and-location'}}
                        </div>

                        {{if status != "FINISHED"}}
                            <div class="project-view-meta_action">
                                {{tmpl '#project-action-btn'}}
                            </div>
                        {{/if}}
                    </div>
                </div>
            </div>

            <div class="project-view-card_author">
                <div class="project-author js-project-author"></div>
            </div>
        </div>

        <div class="project-view-oleg lowline-campaign-banner-js"></div>
    </div>
</script>

<script id="project-action-btn" type="text/x-query-template">
    {{if status == "ACTIVE"}}
    <a class="btn btn-nd-primary btn-block" href="/campaigns/{{= webCampaignAlias}}/donate">
        {{if $data.tags && $data.tags[0] && tags[0].mnemonicName == "CHARITY"}}
        <spring:message code="donate" text="default text"> </spring:message>
        {{else}}
        <spring:message code="support.the.project.infinitive" text="default text"> </spring:message>
        {{/if}}
    </a>
    {{else status != "FINISHED"}}
    <div class="btn btn-nd-primary btn-block disabled">
        {{if tags && tags[0] && tags[0].mnemonicName == "CHARITY"}}
        <spring:message code="donate" text="default text"> </spring:message>
        {{else}}
        <spring:message code="support.the.project.infinitive" text="default text"> </spring:message>
        {{/if}}
    </div>
    {{else}}
    <a class="btn btn-nd-primary btn-block pvc-donate-btn-success">
        {{if targetStatus == "SUCCESS"}}
        <i></i>
        <spring:message code="project.is.successfully.finished" text="default text"> </spring:message>
        {{else}}
        <spring:message code="project.is.finished" text="default text"> </spring:message>
        {{/if}}
    </a>
    {{/if}}
</script>

<script id="about-section" type="text/x-query-template">
    <div class="project-view-block_head donate-action-view about">
        <spring:message code="description" text="default text"></spring:message>
    </div>
    <div class="donate-action-view about">
        <div class="project-description">
            <div class="sub-content" itemprop="description">
                {{html descriptionHtml}}
            </div>
        </div>
        <div class="project-short-description">
            <div class="project-short-description_body">
                {{html shortDescriptionHtml}}
            </div>
            <div class="js-toggle-description project-short-description_show">
                <span class="project-short-description_show-link">
                    Читать полностью
                </span>
            </div>
        </div>
    </div>
</script>

<script id="updates-section" type="text/x-query-template">
    <div class="project-view-block_head donate-action-view updates">
        <spring:message code="project.news" text="default text"></spring:message>
    </div>
    <div class="donate-action-view updates">
        <div class="updates-list"></div>
    </div>
</script>

<script id="comments-section" type="text/x-query-template">
    <div class="project-view-block_head donate-action-view comments">
        <spring:message code="comments" text="default text"></spring:message>
    </div>
    <div class="donate-action-view comments">
        <div class="js-comments-list comments-list"></div>
    </div>
</script>

<script id="backers-section" type="text/x-query-template">
    <div class="project-view-block_head donate-action-view backers">
        {{if charity}}
        <spring:message code="helped.draft" text="default text"> </spring:message>
        {{else}}
        <spring:message code="shareholders" text="default text"> </spring:message>
        {{/if}}
    </div>
    <div class="donate-action-view backers">
        <div class="backers-list-wrap"></div>
    </div>
</script>

<script id="faq-section" type="text/x-query-template">
    <div class="project-view-block_head donate-action-view faq">
        FAQ
    </div>
    <div class="donate-action-view faq">
        <div class="sub-content">
            <div class="project-faq js-faq-list"></div>
            {{if PrivacyUtils.isAdmin() || PrivacyUtils.hasAdministrativeRole()}}
            <div class="project-faq_add-answer js-add-faq"></div>
            {{else}}
            <div class="project-faq_not-find-question">
                <div class="mrg-b-10">
                    <spring:message code="dont.find.your.question" text="default text"> </spring:message>
                </div>
                <a class="btn btn-primary btn-lg mrg-b-10 js-click-to-talk" href="javascript:void(0)">
                    <spring:message code="ask.question" text="default text"> </spring:message>
                </a>
                <br>
                <a href="/faq/article/2!paragraph6">
                    <spring:message code="faq.about.crowdfunding" text="default text"> </spring:message>
                </a>
            </div>
            {{/if}}
        </div>
    </div>
</script>

<script id="related-campaigns-template" type="text/x-jquery-template">
    <div class="project-related-head">
        <spring:message code="just.support" text="default text"> </spring:message>
    </div>
    <div class="js-project-card-list-container"></div>
</script>

<script id="profile-sites-template" type="text/x-jquery-template">
    <div class="project-author_social">
        {{if vkUrl}}
        <a href="{{= vkUrl}}" class="project-author_social-i" target="_blank" rel="nofollow noopener">
            <img src="{{= workspace.staticNodesService.getResourceUrl('/images/project/view/author-soc-vk.svg') }}">
        </a>
        {{/if}}

        {{if facebookUrl}}
        <a href="{{= facebookUrl}}" class="project-author_social-i" target="_blank" rel="nofollow noopener">
            <img src="{{= workspace.staticNodesService.getResourceUrl('/images/project/view/author-soc-fb.svg') }}">
        </a>
        {{/if}}

        {{if twitterUrl}}
        <a href="{{= twitterUrl}}" class="project-author_social-i" target="_blank" rel="nofollow noopener">
            <img src="{{= workspace.staticNodesService.getResourceUrl('/images/project/view/author-soc-tw.svg') }}">
        </a>
        {{/if}}
    </div>
    {{if siteUrl}}
    <div class="project-author_links">
        <a href="{{= siteUrl}}"
           target="_blank" rel="nofollow noopener"
           class="project-author_long-link"
           data-text="{{= siteUrl}}">
            <span class="project-author_long-link-text">
                {{= siteUrl}}
            </span>
        </a>
    </div>
    {{/if}}
</script>

<script id="campaign-faq-edit-template" type="text/x-jquery-template">
    <div class="project-faq_add-answer_head">
        {{if campaignFaqId}}
        <spring:message code="edit.answer" text="default text"> </spring:message>
        {{else}}
        <spring:message code="add.answer.on.question" text="default text"> </spring:message>
        {{/if}}
    </div>

    <div class="project-faq_add-answer-form">
        <div class="fieldset {{if questionError}}error{{/if}}">
            <label for="question">
                {{if questionError}}
                {{= questionError}}
                {{else}}
                <spring:message code="question" text="default text"> </spring:message>
                {{/if}}
            </label>

            <div class="input-field input-field-l">
                <input name="question" id="question" class="input-flat input-l" type="text" value="{{= question}}">
            </div>
        </div>

        <div class="fieldset {{if answerError}}error{{/if}}">
            <label for="answer">
                {{if answerError}}
                {{= answerError}}
                {{else}}
                <spring:message code="answer" text="default text"> </spring:message>
                {{/if}}
            </label>

            <div class="input-field input-field-l">
                <textarea name="answer" id="answer" class="input-flat input-l" rows="3">{{= answer}}</textarea>
            </div>
        </div>

        <div class="fieldset">
            <input class="btn btn-red btn-block btn-lg js-add-faq" type="submit"
                   value="{{if campaignFaqId}}<spring:message code="change.answer" text="default text"> </spring:message>{{else}}<spring:message code="add.answer" text="default text"> </spring:message>{{/if}}">
        </div>
    </div>
</script>

<script id="campaign-faq-empty-template" type="text/x-jquery-template">
    <div class="project-faq_not-question">
        <spring:message code="there.are.no.qustions" text="default text"> </spring:message>
    </div>
</script>

<script id="campaign-author-template" type="text/x-jquery-template">
    {{if profileSites.attributes.vkUrl || profileSites.attributes.facebookUrl ||
    profileSites.attributes.twitterUrl || profileSites.attributes.siteUrl || !PrivacyUtils.isAdmin()}}

    <div class="project-author_ava">
        {{tmpl "#profile-small-avatar", creatorProfile.attributes}}
    </div>
    <div class="project-author_cont">
        <div class="project-author_name">
            <a href="/{{= ProfileUtils.getUserLink(creatorProfile.attributes.profileId, creatorProfile.attributes.alias) }}"
               class="project-author_long-link"
               data-text="{{= creatorProfile.attributes.displayName}}">
                    <span class="project-author_long-link-text">
                        {{= creatorProfile.attributes.displayName}}
                    </span>
            </a>
        </div>
        <div class="js-profile-sites"></div>
    </div>
    <div class="project-author_action">
        {{if status == 'ACTIVE' && workspace.isAuthorized}}
        <div class="project-author_action-i">
            <a href="javascript:void(0)" class="js-click-to-subscribe">
                {{if subscriptions }}
                {{if subscriptions.attributes.isSubscribed}}
                <spring:message code="unsubscribe.to.author" text="default text"> </spring:message>
                {{else}}
                <spring:message code="subscribe.to.author" text="default text"> </spring:message>
                {{/if}}
                {{/if}}
            </a>
        </div>
        {{/if}}
        <div class="project-author_action-i">
            <a href="javascript:void(0)" class="js-click-to-talk"> <spring:message code="ask.question"
                                                                                   text="default text"> </spring:message> </a>
        </div>
        {{if PrivacyUtils.isAdmin()}}
        <div class="project-author_action-i">
            <a href="javascript:void(0)" data-event-click="onAddContactsClick">
                <spring:message code="edit" text="default text"> </spring:message>
            </a>
        </div>
        {{/if}}
    </div>
    {{else PrivacyUtils.isAdmin()}}
    <div class="project-author_ava">
        {{tmpl "#profile-small-avatar", creatorProfile.attributes}}
    </div>
    <div class="project-author_cont">
        <div class="project-author_name">
            <a href="/{{= ProfileUtils.getUserLink(creatorProfile.attributes.profileId, creatorProfile.attributes.alias) }}"
               class="project-author_long-link"
               data-text="{{= creatorProfile.attributes.displayName}}">
                    <span class="project-author_long-link-text">
                        {{= creatorProfile.attributes.displayName}}
                    </span>
            </a>
        </div>
        <div class="project-author_empty-head">
            <spring:message code="fill.your.contacts" text="default text"> </spring:message>
        </div>
        <div class="project-author_empty-text">
            <spring:message code="more.trus.projects" text="default text"> </spring:message>
        </div>
    </div>
    <div class="project-author_action">
        <div class="project-author_action-i">
            <a href="javascript:void(0)" data-event-click="onAddContactsClick">
                <spring:message code="fill.contacts" text="default text"> </spring:message>
            </a>
        </div>
    </div>
    <div class="js-profile-sites"></div>
    {{/if}}
</script>

<script id="project-alert-template" type="text/x-jquery-template">
    <div class="wrap">
        <div class="col-12">
            {{if status == "APPROVED"}}
            <div class="alert-block alert-block__text">
                <div class="alert-block_cont">
                    <div class="alert-block_text">
                        <b><spring:message code="project.is.approved" text="default text"> </spring:message></b>
                        <br>
                        <span class="moderator-message"></span>
                    </div>
                </div>
            </div>
            {{/if}}

            {{if status == "NOT_STARTED"}}
            <div class="alert-block alert-block__text">
                <div class="alert-block_cont">
                    <div class="alert-block_text">
                        <b><spring:message code="project.is.in.moderation" text="default text"> </spring:message></b>
                        <br>
                        <span class="moderator-message"></span>
                    </div>
                </div>
            </div>
            {{/if}}

            {{if status == "PATCH"}}
            <div class="alert-block">
                <div class="alert-block_ico"></div>
                <div class="alert-block_cont">
                    <div class="alert-block_head"><spring:message code="attention"
                                                                  text="default text"> </spring:message></div>
                    <div class="alert-block_text">
                        <b><spring:message code="project.needs.some.work" text="default text"> </spring:message></b>
                        <br>
                        <span class="moderator-message"></span>
                    </div>
                </div>
            </div>
            {{/if}}

            {{if status == "PAUSED"}}
            <div class="alert-block alert-block__text">
                <div class="alert-block_cont">
                    <div class="alert-block_text">
                        <b><spring:message code="project.paused" text="default text"> </spring:message></b>
                        <br>
                        <span class="moderator-message"></span>
                    </div>
                </div>
            </div>
            {{/if}}

            {{if status == "DECLINED"}}
            <div class="alert-block alert-block__warning">
                <div class="alert-block_ico"></div>
                <div class="alert-block_cont">
                    <div class="alert-block_head"><spring:message code="attention"
                                                                  text="default text"> </spring:message></div>
                    <div class="alert-block_text">
                        <b><spring:message code="project.hasnt.passed.moderation"
                                           text="default text"> </spring:message></b>
                        <br>
                        <span class="moderator-message"></span>
                    </div>
                </div>
            </div>
            {{/if}}
        </div>
    </div>
</script>

<script id="share-item-template-inner" type="text/x-jquery-template">
    <div class="action-card_meta">
        <div class="action-card_price">{{if isDonate}}100{{else}}{{= StringUtils.humanNumber(price)}}{{/if}} ₽</div>
        <div class="action-card_count">
            <div class="action-card_count-i">
                <span class="action-card_count-lbl">
                    {{if notCharity}}
                    <spring:message code="declension.word.buy" text="default text"> </spring:message>
                    {{else}}
                    <spring:message code="declension.word.help" text="default text"> </spring:message>
                    {{/if}}
                </span>
                <span class="action-card_count-val">{{= StringUtils.humanNumber(purchaseCount)}}</span>
            </div>
            {{if amount > 0}}
            <div class="action-card_count-i">
                <span class="action-card_count-lbl">
                    {{if notCharity}}
                    <spring:message code="declension.word.stay4.with.variable" text="default text"> </spring:message>
                    {{else}}
                    <spring:message code="declension.word.stay5.with.variable" text="default text"> </spring:message>
                    {{/if}}
                </span>
                <span class="action-card_count-val">{{= StringUtils.humanNumber(amount > purchaseCount ? amount - purchaseCount : 0)}}</span>
            </div>
            {{/if}}
        </div>
    </div>
</script>

<script id="interactive-share-item-template-inner" type="text/x-jquery-template">
    <div class="ac-count-block">

        <div class="ac-count-block_wrap">
            {{if notCharity}}
            {{if amount != 0 && amount <= purchaseCount }}
            <div class="ac-count-field">
                <span class="ac-selled-text">
                    <spring:message code="sold.out" text="default text"> </spring:message>
                </span>
            </div>
            {{else amount > 0 && (amount == 0 || amount != purchaseCount)}}
            <div class="ac-count-field">
                <span class="ac-count-name">
                    <spring:message code="declension.word.stay4.with.variable" text="default text"> </spring:message>
                </span>
                <span class="ac-count-value">{{= StringUtils.humanNumber(amount - purchaseCount)}}</span>
            </div>
            {{/if}}

            <div class="ac-count-field">
                <span class="ac-count-name">
                    <spring:message code="declension.word.buy" text="default text"> </spring:message>
                </span>
                <span class="ac-count-value">{{= StringUtils.humanNumber(purchaseCount)}}</span>
            </div>
            {{else}}
            {{if amount > 0 && (amount == 0 || amount != purchaseCount)}}
            <div class="ac-count-field">
                <span class="ac-count-name">
                    <spring:message code="declension.word.stay5.with.variable" text="default text"> </spring:message>
                </span>
                <span class="ac-count-value">{{= StringUtils.humanNumber(amount - purchaseCount < 0 ? 0 : amount - purchaseCount)}}</span>
            </div>
            {{/if}}

            <div class="ac-count-field">
                <span class="ac-count-name">
                    <spring:message code="declension.word.help" text="default text"> </spring:message>
                </span>
                <span class="ac-count-value">{{= purchaseCount}}</span>
            </div>
            {{/if}}
        </div>
    </div>

    {{if isDonate}}
    <div class="ac-donate">
        <spring:message code="any.amount" text="default text"> </spring:message>
    </div>
    {{else}}
    <div class="ac-price">{{= StringUtils.humanNumber(price)}}
        <span class="b-rub">Р</span>
    </div>
    {{/if}}
</script>

<script id="project-contractor-alert-template" type="text/x-jquery-template">
    {{if PrivacyUtils.isAdmin()}}
    <div class="project-admin-info">
        {{if !currentPlanetaManager.attributes.fullName}}
        <div class="project-admin-info_i">
            <div class="project-admin-info_lbl"><spring:message code="attention"
                                                                text="default text"> </spring:message></div>
            <div class="project-admin-info_val"><spring:message code="project.wount.be.sent"
                                                                text="default text"> </spring:message>
                <a href="/campaigns/{{= campaignId}}/edit-contractor" class="project-admin-info_change-link">
                    <spring:message code="legal.information" text="default text"> </spring:message>
                </a>
            </div>
        </div>
        {{else PrivacyUtils.hasAdministrativeRole()}}
        <div class="project-admin-info_i">
            <div class="project-admin-info_lbl"><spring:message code="project.contractor"
                                                                text="default text"> </spring:message></div>
            <div class="project-admin-info_val">
                "{{= name}}", <spring:message code="phone.number" text="default text"> </spring:message> {{= phone}}
                <a href="/campaigns/{{= campaignId}}/edit-contractor" class="project-admin-info_change-link">
                    <spring:message code="change" text="default text"> </spring:message>
                </a>
            </div>
        </div>
        <div class="project-admin-info_i">
            <div class="project-admin-info_lbl">
                <spring:message code="project.manager" text="default text"> </spring:message>
            </div>
            <div class="project-admin-info_val">
                {{= currentPlanetaManager.attributes.fullName}}
                <a href="{{= changeModerationUrl}}" class="project-admin-info_change-link" target="_blank"
                   rel="nofollow noopener">
                    <spring:message code="change" text="default text"> </spring:message>
                </a>
            </div>
        </div>
        {{/if}}
    </div>
    {{/if}}
</script>

<script id="share-item-template" type="text/x-jquery-template">
    {{if amount != 0 && amount <= purchaseCount }}
    <div class="action-card-list_type js-sales">
        <spring:message code='sold.out.rewards' text='default text'/>
    </div>
    {{else (shareStatus == "INVESTING" || shareStatus == "INVESTING_WITHOUT_MODERATION" || shareStatus == "INVESTING_ALL_ALLOWED")}}
    <div class="action-card-list_type js-sales">
        {{if multipleTitle}}
        <spring:message code='investing.rewards' text='default text'/>
        {{else singleTitle}}
        <spring:message code='investing.reward' text='default text'/>
        {{/if}}
    </div>
    {{/if}}

    <div class="action-card {{if isDonate}}without-reward{{/if}} inactive" share-id-attr="{{= shareId}}">
        {{if !isDonate}}
        <div class="action-card_in">
        {{/if}}
            {{if $data.isPurchased}}
            <div class="action-card_supported" data-tooltip="<spring:message code='you.have.bought.this.share' text='default text'/>"></div>
            {{/if}}
            <div class="action-card_name">
                <div class="action-card_check"></div>
                {{if nameHtml}}{{html StringUtils.cutString(nameHtml, 1000)}}{{else}}{{= StringUtils.cutString(name, 1000)}}{{/if}}
            </div>
            {{if imageUrl}}
            <div class="action-card_cover">
                <div class="action-card_cover-in photo-zoom-link" href="{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.CAMPAIGN_VIDEO, ImageType.PHOTO)}}">
                    <img class="action-card_img" src="{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.MEDIUM, ImageType.GROUP)}}">
                </div>
            </div>
            {{/if}}
            <div class="action-card_descr">
                <div class="action-card_descr-text">
                {{if descriptionHtml}}
                {{html descriptionHtml}}
                {{else}}
                {{= description}}
                {{/if}}
                </div>
                <div class="action-card_descr-show">
                   <span class="action-card_descr-show-text"> <spring:message code="roll.down" text="default text"/> </span>
                </div>
            </div>

            {{if ((deliveries=ProfileUtils.getEnabledDeliveries(linkedDeliveries)) && deliveries.length > 0 ) || estimatedDeliveryTime }}
            <div class="action-card_delivery">
                {{if estimatedDeliveryTime}}
                <div class="action-card_delivery-i">
                        <span class="action-card_delivery-lbl">
                            <spring:message code="campaigns-edit-shares.jsp.propertie.31" text="default text"/>
                        </span>
                    <span class="action-card_delivery-val">
                            {{html DateUtils.estimatedDate(estimatedDeliveryTime)}}
                        </span>
                </div>
                {{/if}}

                {{if deliveries && deliveries.length > 0}}
                <div class="action-card_delivery-i">
                        <span class="action-card_delivery-lbl">
                            <spring:message code="delivery" text="default text"/>
                        </span>
                    <span class="action-card_delivery-val">
                            {{each($key, delivery) deliveries}}
                                {{if workspace.currentLanguage == 'en'}}
                                    {{= delivery.nameEn }}
                                {{else}}
                                    {{= delivery.name }}
                                {{/if}}
                                {{if $key != deliveries.length - 1}}, {{/if}}
                            {{/each}}
                        </span>
                </div>
                {{/if}}
            </div>
            {{/if}}

            {{if !isDonate}}
            {{tmpl '#share-item-template-inner'}}
            {{else}}
            <div class="action-card_counter">
                <input type="text" class="form-control action-card_counter-val js-currency-mask" value="{{if donateAmount}}{{= donateAmount}}{{else}}{{= price}}{{/if}}">
            </div>
            {{/if}}
            <div class="action-card_action-wrap">
                <div class="action-card_action">
                    <div class="action-card_action-cont">
                        {{if isDonate}}
                        <div class="action-card_action-bottom">
                            <div class="action-card_action-btn">
                                <button type="button" class="btn btn-nd-primary action-card_btn"><spring:message code="buy.donate" text="default text"/> <span>{{if donateAmount}}{{= StringUtils.humanNumber(donateAmount)}}{{else}}{{= price}}{{/if}}</span> ₽</button>
                            </div>
                        </div>
                        {{else}}
                        <div class="action-card_fix">
                            <div class="action-card_fix-in">
                                <div class="action-card_counter">
                                    <div class="action-card_counter-cont">
                                        <input type="text" class="form-control action-card_counter-val js-currency-mask"
                                               value="{{if donateAmount}}{{= StringUtils.humanNumber(donateAmount)}}{{else}}{{= price}}{{/if}}">
                                        <button type="button" class="action-card_counter-btn plus js-increase">+</button>
                                        <button type="button" class="action-card_counter-btn minus js-decrease">–</button>
                                    </div>
                                    <div class="action-card_counter-error js-donate-amount-error"> </div>
                                </div>
                                <div class="action-card_action-btn">
                                    <button type="button" class="btn btn-nd-primary action-card_btn">
                                        {{if quantity}}
                                        <spring:message code="buy" text="default text"/> ({{= quantity}} <spring:message code="item" text="default text"/>)</button>
                                        {{/if}}
                                </div>
                                <div class="action-card_why">
                                    <span class="action-card_why-text"
                                          data-tooltip="<spring:message code='reward.proceed.rules' text="default text"/>"
                                          data-tooltip-theme="action-card-why-text-tt" data-tooltip-position="bottom center">
                                    <spring:message code="pay.more" text="default text"/></span>
                                </div>

                            </div>
                        </div>
                        <div class="action-card_action-bottom">
                            {{if rewardInstruction}}
                            <div class="action-card_spec">
                                {{= rewardInstruction}}
                            </div>
                            {{/if}}
                            <div class="action-card_share">
                                <div class="sharing-popup-social sharing-mini js-share-share"></div>
                            </div>
                            <div class="action-card_link-block">
                                <a href="/campaigns/{{= campaignId}}/donatesingle/{{= shareId}}" class="action-card_link"><spring:message code="reward.link" text="default text"/></a>
                            </div>
                        </div>
                        {{/if}}
                    </div>
                </div>
            </div>
            {{if campaignStatus != "FINISHED"}}
            <div class="action-card_show">
                <div class="action-card_show-text">
                    <spring:message code="planeta-welcome.jsp.propertie.11" text="default text"/>
                </div>
            </div>
            {{/if}}
        {{if !isDonate}}
        </div>
        {{/if}}
    </div>
</script>

<script id="campaign-edit-share-template" type="text/x-jquery-template">
    <div class="action-card" data-share-id="{{= shareId}}">
        <div class="action-card_name">
            {{html StringUtils.cutString(name, 1000)}}
        </div>

        {{if imageUrl && imageUrl != ''}}
            <div class="action-card_cover">
                <div class="action-card_cover-in">
                    <img class="action-card_img" src="{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.BIG, ImageType.DONATE)}}">
                </div>
            </div>
        {{/if}}

        <div class="action-card_descr">
            <div class="action-card_descr-text">
                {{if descriptionHtml}}
                    {{html descriptionHtml}}
                {{else}}
                    {{html StringUtils.cutString(description, 1000)}}
                {{/if}}
            </div>
        </div>

        {{if estimatedDeliveryTime || linkedDeliveries}}
            <div class="action-card_delivery">
                {{if estimatedDeliveryTime}}
                    <div class="action-card_delivery-i">
                        <span class="action-card_delivery-lbl">
                            <spring:message code="campaigns-edit-shares.jsp.propertie.31" text="default text"/>
                        </span>
                        <span class="action-card_delivery-val">
                            {{html DateUtils.formatDate(estimatedDeliveryTime, "D MMMM YYYY", false, true)}}
                        </span>
                    </div>
                {{/if}}
                {{if (deliveries=ProfileUtils.getEnabledDeliveries(linkedDeliveries)) && deliveries.length > 0}}
                    <div class="action-card_delivery-i">
                        <span class="action-card_delivery-lbl">
                            <spring:message code="delivery" text="default text"/>
                        </span>
                        <span class="action-card_delivery-val">
                            {{each($key, delivery) deliveries}}
                                {{if workspace.currentLanguage == 'en'}}
                                    {{= delivery.nameEn }}
                                {{else}}
                                    {{= delivery.name }}
                                {{/if}}
                            {{if $key != deliveries.length - 1}}, {{/if}}
                            {{/each}}
                        </span>
                    </div>
                {{/if}}
            </div>
        {{/if}}

        {{if price > 0 }}
            {{tmpl '#share-item-template-inner'}}
        {{/if}}

        {{if price > 0 }}
            <div class="ac-action">
                {{if price}}
                {{if purchaseCount === 0}}
                <div class="ac-action-del">
                    <span class="ac-action-link" data-event-click="onDeleteClick">
                        <spring:message code="delete" text="default text"/>
                    </span>
                </div>
                {{/if}}

                <span class="ac-action-link" data-event-click="onEditClick">
                    <spring:message code="edit" text="default text"/>
                </span>

                <span class="ac-action-link" data-event-click="onCloneClick">
                    <spring:message code="clone" text="default text"/>
                </span>
                {{/if}}
            </div>
        {{/if}}

    </div>
</script>

<script id="interactive-campaign-edit-share-template" type="text/x-jquery-template">
    <div class="action-card-link" data-share-id="{{= shareId}}">
        {{tmpl '#interactive-share-item-template-inner'}}

        <div class="ac-name">{{html StringUtils.cutString(name, 1000)}}</div>

        <div class="ac-action">
            {{if price}}
            {{if purchaseCount === 0}}
            <div class="ac-action-del">
                <span class="ac-action-link" data-event-click="onDeleteClick">
                    <spring:message code="delete" text="default text"/>
                </span>
            </div>
            {{/if}}

            <span class="ac-action-link" data-event-click="onEditClick">
                <spring:message code="edit" text="default text"/>
            </span>

            <span class="ac-action-link" data-event-click="onCloneClick">
                <spring:message code="clone" text="default text"/>
            </span>
            {{/if}}
        </div>
    </div>
</script>

<script id="share-modal-inner-template" type="text/x-jquery-template">
    <div class="modal-action-card_info">
        <div class="modal-action-card_close">
            <div class="modal-action-card_close-ico"></div>
        </div>
        <div class="donate-reward">
            <div class="donate-reward_block">
                <div class="donate-reward_count">
                    <div class="donate-reward_count-i">
                        <span class="donate-reward_count_lbl"><spring:message code="declension.word.buy"
                                                                              text="default text"> </spring:message></span>
                        <span class="donate-reward_count_val">{{= StringUtils.humanNumber(purchaseCount)}}</span>
                    </div>

                    {{if amount > 0}}
                    <div class="donate-reward_count-i">
                        <span class="donate-reward_count_lbl"><spring:message code="declension.word.stay5.with.variable"
                                                                              text="default text"> </spring:message></span>
                        <span class="donate-reward_count_val">{{= StringUtils.humanNumber(amount - purchaseCount < 0 ? 0 : amount - purchaseCount)}}</span>
                    </div>
                    {{/if}}
                </div>

                {{if price != 0 && campaignStatus != "FINISHED"}}
                <div class="js-quantity-numberpicker"></div>
                {{/if}}

                <div class="donate-reward_main">
                    {{if imageUrl}}
                    <div class="donate-reward_img">
                        <img src={{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.MEDIUM, ImageType.GROUP) }}>
                    </div>
                    {{/if}}
                    <div class="donate-reward_name">
                        {{if nameHtml}}
                        {{html StringUtils.cutString(nameHtml, 1000)}}
                        {{else}}
                        {{= StringUtils.cutString(name, 1000)}}
                        {{/if}}
                    </div>
                    <div class="donate-reward_descr">
                        <div class="js-descriptionHtml">
                            {{if descriptionHtml}}
                            {{html descriptionHtml}}
                            {{else}}
                            {{= description}}
                            {{/if}}
                        </div>
                    </div>
                    <div class="modal-ac-descr-expand">
                        <span class="modal-ac-descr-expand-link">
                            <spring:message code="open" text="default text"> </spring:message>
                        </span>
                    </div>
                </div>
                <div class="donate-reward_bottom">
                    {{if estimatedDeliveryTime}}
                    <div class="donate-reward_date">
                        <span class="donate-reward_date-lbl"> <spring:message
                                code="campaigns-edit-shares.jsp.propertie.31"
                                text="default text"> </spring:message></span>
                        <span class="donate-reward_date-val"> {{= DateUtils.estimatedDate(estimatedDeliveryTime)}} </span>
                    </div>
                    {{/if}}
                    <div class="donate-reward_share">
                        <div class="donate-reward_share-lbl"><spring:message code="share"
                                                                             text="default text"> </spring:message> –
                        </div>
                        <div class="donate-reward_share-val">
                            <div class="sharing-popup-social sharing-mini js-share-share"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="modal-action-card_action">
        <div class="donate-action">
            <div class="donate-action_title">
                {{if shareStatus.startsWith('INVEST')}}
                <spring:message code="invest.amount.of" text="default text"> </spring:message>
                {{else isDonate}}
                <spring:message code="donate.some.amount" text="default text"> </spring:message>
                {{else}}
                <spring:message code="support.amount.of" text="default text"> </spring:message>
                {{/if}}
            </div>
            <div class="js-manual-input"></div>

            <div class="donate-action_btn">
                <input class="btn btn-nd-primary btn-block{{if campaignStatus != 'ACTIVE' || amount != 0 && amount == purchaseCount}} disabled{{/if}}"
                       type="submit"
                       value="{{if amount != 0 && amount == purchaseCount}}<spring:message code="all.shares.purchased" text="default text"></spring:message>{{else}}{{if shareStatus.startsWith('INVEST')}}<spring:message code="invest.share" text="default text"> </spring:message>{{else}}<spring:message code="campaign-donate.jsp.propertie.28" text="default text"> </spring:message>{{/if}}{{/if}}">
            </div>
            <div class="donate-action_attention">
                <div class="donate-shop-attention">
                    <div class="donate-shop-attention_ico">
                        <svg width="27" height="27" viewBox="0 0 27 27">
                            <g fill="#FFCF0D">
                                <path d="M12.136 12.96v6.124c0 .725.586 1.317 1.303 1.317s1.304-.592 1.304-1.317V12.96c0-.725-.587-1.317-1.304-1.317-.717 0-1.303.592-1.303 1.317zm.163-5.466a1.68 1.68 0 0 0-.489 1.152c0 .428.163.856.489 1.153.293.296.717.494 1.14.494.424 0 .848-.165 1.141-.494a1.69 1.69 0 0 0 .489-1.153 1.55 1.55 0 0 0-.489-1.152A1.653 1.653 0 0 0 13.439 7c-.423 0-.847.198-1.14.494z"></path>
                                <path d="M13.5 27C6.044 27 0 20.956 0 13.5S6.044 0 13.5 0 27 6.044 27 13.5 20.956 27 13.5 27zm0-2C19.851 25 25 19.851 25 13.5S19.851 2 13.5 2 2 7.149 2 13.5 7.149 25 13.5 25z"></path>
                            </g>
                        </svg>
                    </div>
                    <div class="donate-shop-attention_text">
                        Planeta.ru – не интернет-магазин.
                        <br>
                        Это способ реализации идей в жизнь.
                        <br>
                        <a href="/welcome/projects-agreement.html">Подробнее.</a>
                    </div>
                </div>
            </div>
            <div class="js-donate-amount-error"></div>
        </div>
    </div>
</script>

<script id="campaign-edit-tiny-mce-template" type="text/x-jquery-template">
    <div class="edit-blog-body" style="display: none;">
        <div class="tinymci">
            <div id="tinymce-body" style="overflow: hidden;"
                 placeholder="<spring:message code="placeholder.about.project" text="default text"> </spring:message>">
                {{html descriptionHtml}}
            </div>
        </div>
    </div>
    <div class="content-overlay hide"></div>
</script>

<script id="campaign-share-edit-tiny-mce-template" type="text/x-jquery-template">
    <div class="edit-blog-body" style="display: none;">
        <div class="tinymci">
            <div class="tinymce-body" style="overflow: hidden;">
                {{html descriptionHtml}}
            </div>
        </div>
    </div>
    <div class="content-overlay hide"></div>
</script>

<script id="campaign-contacts-edit-email-template" type="text/x-jquery-template">
    <div class="project-create_col-lbl">
        <div class="pc_col-lbl_title">
            E-mail
            <div class="pc_row_help tooltip"
                 data-tooltip="<spring:message code="add.emails" text="default text"> </spring:message>"></div>
        </div>
    </div>

    <div class="project-create_col-val">
        <div class="select2-flat">
            <input name="tags" multiple="multiple" class="select2-offscreen select-tags js-select2"
                   placeholder="<spring:message code="enter.email" text="default text"> </spring:message>" tabindex="-1"
                   data-field-name="emailList">
        </div>
    </div>
</script>

<script id="campaign-contacts-edit-input-template" type="text/x-jquery-template">
    <div class="project-create_row js-contacts-row" name="{{= fieldName}}">
        <div class="project-create_col-lbl">
            <div class="pc_col-lbl_title js-contacts-title" name="{{= fieldName}}">
                {{= title}}
            </div>
        </div>

        <div class="project-create_col-val">
            <input class="form-control prj-crt-input" type="text" placeholder="{{= placeholder}}"
                   value="{{= value}}" data-field-name="{{= fieldName}}">
        </div>
    </div>
</script>
<script id="campaign-contacts-edit-site-input-template" type="text/x-jquery-template">
    <div class="project-create_row js-contacts-row" name="{{= fieldName}}">
        <div class="project-create_col-lbl">
            <div class="pc_col-lbl_title js-contacts-title" name="{{= fieldName}}">
                {{= title}}
            </div>
        </div>

        <div class="project-create_col-val">
            <div class="input-url">
                <table class="input-url_table">
                    <tbody>
                    <tr>
                        <td class="input-url_lbl">{{= urlPrefix}}</td>
                        <td class="input-url_val">
                            <input class="form-control control-xlg js-url-input" type="text"
                                   placeholder="{{= placeholder}}"
                                   value="{{if value}}{{= value.replace(urlPrefix, '')}}{{else}}{{/if}}"
                                   data-field-name="{{= fieldName}}"
                                   url-prefix="{{= urlPrefix}}">
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</script>

<script id="campaign-contacts-edit-template" type="text/x-jquery-template">
    <div class="modal-header">
        <a class="close">×</a>

        <div class="modal-title"><spring:message code="contacts" text="default text"> </spring:message></div>
    </div>

    <div class="project-create_cont">
        <div class="project-create_list">
            {{tmpl '#campaign-contacts-edit-input-template',{
            title: '<spring:message code="team.name" text="default text"> </spring:message>',
            placeholder:'Text',
            value: $data.creatorProfileDisplayName,
            fieldName: 'displayName'
            } }}

            <div class="project-create_row js-edit-email"></div>

            {{tmpl '#campaign-contacts-edit-site-input-template',{
            title: '<spring:message code="site" text="default text"> </spring:message>',
            placeholder: 'http://example.com',
            value: $data.siteUrl,
            fieldName: 'siteUrl',
            urlPrefix: 'http://',
            placeholder: 'example.com',
            errorMessage: $data.fieldErrors ? $data.fieldErrors.siteUrl : ''
            } }}

            {{tmpl '#campaign-contacts-edit-site-input-template',{
            title: '<spring:message code="vk.page" text="default text"> </spring:message>',
            placeholder: 'http://vk.com/yourname',
            value: $data.vkUrl,
            fieldName: 'vkUrl',
            urlPrefix: 'https://vk.com/',
            placeholder: 'yourname',
            errorMessage: $data.fieldErrors ? $data.fieldErrors.vkUrl : ''
            } }}

            {{tmpl '#campaign-contacts-edit-site-input-template',{
            title: '<spring:message code="facebook.page" text="default text"> </spring:message>',
            placeholder: 'http://facebook.com/yourname',
            value: $data.facebookUrl,
            fieldName: 'facebookUrl',
            urlPrefix: 'https://facebook.com/',
            placeholder: 'yourname',
            errorMessage: $data.fieldErrors ? $data.fieldErrors.facebookUrl : ''
            } }}

            {{tmpl '#campaign-contacts-edit-site-input-template',{
            title: '<spring:message code="twitter.page" text="default text"> </spring:message>',
            placeholder: 'http://twitter.com/yourname',
            value: $data.twitterUrl,
            fieldName: 'twitterUrl',
            urlPrefix: 'https://twitter.com/',
            placeholder: 'yourname',
            errorMessage: $data.fieldErrors ? $data.fieldErrors.twitterUrl : ''
            } }}

            <div class="project-create_row js-edit-avatar"></div>

        </div>
    </div>


    <div class="modal-footer cf">
        <div class="pull-right">
            <button class="btn btn-primary" type="submit">
                <spring:message code="save" text="default text"> </spring:message>
            </button>
        </div>

        <button class="btn" type="reset">
            <spring:message code="cancel" text="default text"> </spring:message>
        </button>
    </div>
</script>

<script id="campaign-contractor-list-template" type="text/x-jquery-template">
    <div class="project-create_row">
        <div class="project-create_col-lbl">
            <div class="pc_col-lbl_title">
                <spring:message code="choose.contractor" text="default text"> </spring:message>
            </div>
        </div>
        <div class="project-create_col-val js-type_select">
            <select class="pln-select pln-select-xlg prj-crt-input" data-planeta-ui="dropDownSelect" data-event-change="onContractorChanged">
                {{each contractorCollection}}
                    <option value="{{= $index}}" {{if $data.selectedIndex=== $index}}selected="selected"{{/if}}>{{= $value.name}}</option>
                {{/each}}

                <option value="-1" {{if !($data.selectedIndex >= 0)}}selected="selected"{{/if}}>
                    <spring:message code="new.contractor" text="default text"> </spring:message>
                </option>
            </select>
        </div>
    </div>
</script>

<script id="campaign-contractor-file-template" type="text/x-jquery-template">
    <div class="filelist-img">
        <a href="{{= fileUrl }}" target="_blank">
            <img class="project-card-cover"
                 src="{{if fileUrl.slice(-4) != '.pdf'}}{{= ImageUtils.getThumbnailUrl(fileUrl, {useProxy: true, fileName: 'original', width: 100, height: 100, crop: true})}}{{else}}https://s1.dev.planeta.ru/i/1a50db/1508435362953_renamed.jpg{{/if}}"
                 alt="{{= StringUtils.humanFileSize(size) }}">
        </a>
    </div>
    <div class="filelist-option js-remove-file">
        <i class="icon-close icon-white"></i>
    </div>
</script>

<script id="campaign-contractor-edit-input-template" type="text/x-jquery-template">
    <div class="project-create_row {{if $data.fluidControl}}project-create_fluid-control{{/if}}">
        {{if !$data.hidden}}
        <div class="project-create_col-lbl">
            <div class="pc_col-lbl_title">
                {{= title}}
                <div class="js-error"></div>
                {{if $data.tooltip}}
                <div class="pc_row_help tooltip" data-tooltip="{{= tooltip}}"></div>
                {{/if}}
            </div>
            {{if $data.label}}
            <div class="pc_col-lbl_text">
                {{= label}}
            </div>
            {{/if}}
        </div>
        {{/if}}
        <div class="project-create_col-val">
            {{if $data.rows}}
            <textarea class="form-control prj-crt-input js-save-model" placeholder="{{= placeholder}}" name="{{= name}}"
                      rows="{{= rows}}">{{= value}}</textarea>
            {{else}}
            <input class="form-control control-xlg prj-crt-input js-save-model"
                   type="{{if hidden}}hidden{{else}}text{{/if}}" placeholder="{{= placeholder}}" value="{{= value}}"
                   name="{{= name}}" rows="{{= rows}}"/>
            {{/if}}
        </div>
    </div>
</script>

<script id="campaign-contractor-edit-template" type="text/x-jquery-template">
    <form>
        <div class="project-create_text pdg-b-5">
            <div class="project-create_text_text">
                <spring:message code="campaigns-edit.jsp.propertie.61" text="default text"> </spring:message>
            </div>
        </div>

        <div class="project-create_list">

            <div class="project-create_row js-contractor-list"></div>

            <div class="project-create_row">
                <div class="project-create_col-lbl">
                    <div class="pc_col-lbl_title">
                        <spring:message code="type" text="default text"> </spring:message>
                        <div class="js-error"></div>
                    </div>
                </div>
                <div class="project-create_col-val js-type_select">
                    <select class="pln-select pln-select-xlg prj-crt-input" data-planeta-ui="dropDownSelect" name="type"
                            {{if $data.isCharity}}disabled{{/if}}>
                    {{each $data.contractorTypeList}}
                    <option value="{{= $index}}">{{= $value}}</option>
                    {{/each}}
                    </select>
                    {{if $data.isCharity}}
                    <input name="type" value="CHARITABLE_FOUNDATION" type="hidden">
                    {{/if}}
                </div>
            </div>


            {{if !$data.type || $data.type == 'INDIVIDUAL'}}
            {{tmpl '#campaign-contractor-edit-input-template',{
            title: '<spring:message code="fio" text="default text"> </spring:message>',
            value: $data.contractorId === 0 ? "" : $data.name,
            name: 'name',
            requiredField: true,
            label: '<spring:message code="fio.or.title" text="default text"> </spring:message>',
            fluidControl: true}
            }}
            {{else}}
            {{tmpl '#campaign-contractor-edit-input-template',{
            title: '<spring:message code="title" text="default text"> </spring:message>',
            value: $data.contractorId === 0 ? "" : $data.name,
            name: 'name',
            requiredField: true,
            label: '<spring:message code="fio.or.title" text="default text"> </spring:message>',
            fluidControl: true}
            }}
            {{/if}}

            {{if $data.type && $data.type != 'INDIVIDUAL' && $data.type != 'INDIVIDUAL_ENTREPRENEUR'}}
            <div class="project-create_row">
                <div class="project-create_col-lbl">
                    <div class="pc_col-lbl_title">
                        <spring:message code="position" text="default text"> </spring:message>
                        <div class="js-error"></div>
                    </div>
                </div>
                <div class="project-create_col-val js-type_select">
                    <select class="pln-select pln-select-xlg prj-crt-input" data-planeta-ui="dropDownSelect"
                            name="position">
                        {{each $data.contractorPositionList}}
                        <option value="{{= $index}}">{{= $value}}</option>
                        {{/each}}
                    </select>
                </div>
            </div>
            {{else}}
            <input name="position" value="OTHER" type="hidden">
            {{/if}}

            {{if $data.type && $data.type != 'INDIVIDUAL' && $data.type != 'INDIVIDUAL_ENTREPRENEUR' && $data.position
            == 'OTHER'}}
            {{tmpl '#campaign-contractor-edit-input-template',{
            title: '<spring:message code="position.custom" text="default text"> </spring:message>',
            value: $data.contractorId === 0 ? "" : $data.customPosition,
            name: 'customPosition',
            label: '<spring:message code="position.custom.label" text="default text"> </spring:message>',
            fluidControl: true,
            requiredField: false}
            }}
            {{else}}
            <input name="customPosition" value="{{= customPosition}}" type="hidden">
            {{/if}}

            {{if $data.countryId == 1 && $data.type && ($data.type == 'OOO' || $data.type == 'CHARITABLE_FOUNDATION' ||
            $data.type == 'LEGAL_ENTITY')}}
            {{tmpl '#campaign-contractor-edit-input-template',{
            title: '<spring:message code="responsible.person" text="default text"> </spring:message>',
            value: $data.responsiblePerson,
            name: 'responsiblePerson',
            label: '<spring:message code="with.responsible.person" text="default text"> </spring:message>',
            placeholder: '<spring:message code="with.responsible.person.placeholder"
                                          text="default text"> </spring:message>',
            fluidControl: true,
            requiredField: true
            }
            }}
            {{else}}
            <input type="hidden" id="responsiblePerson" name="responsiblePerson" value="{{= responsiblePerson}}"/>
            {{/if}}

            {{if $data.countryId == 1 && $data.type && ($data.type == 'INDIVIDUAL' || $data.type ==
            'INDIVIDUAL_ENTREPRENEUR' || $data.type == 'OOO' || $data.type == 'CHARITABLE_FOUNDATION' || $data.type ==
            'LEGAL_ENTITY')}}
            {{tmpl '#campaign-contractor-edit-input-template',{
            title: '<spring:message code="initials.with.last.name" text="default text"> </spring:message>',
            value: $data.initialsWithLastName,
            name: 'initialsWithLastName',
            requiredField: true,
            placeholder: '<spring:message code="with.initials.with.last.name" text="default text"> </spring:message>',
            fluidControl: true}
            }}
            {{else}}
            <input type="hidden" id="initialsWithLastName" name="initialsWithLastName"
                   value="{{= initialsWithLastName}}">
            {{/if}}

            {{if $data.countryId == 1 && $data.type && $data.type == 'INDIVIDUAL'}}
            <div class="project-create_row">
                <div class="project-create_col-lbl">
                    <div class="pc_col-lbl_title">
                        <spring:message code="birth.date" text="default text"> </spring:message>
                        <div class="js-error"></div>
                    </div>
                </div>

                <div class="project-create_col-val">
                    <input class="form-control prj-crt-input prj-crt-input__date" type="text" placeholder="20.10.1996"
                           data-planeta-ui="dateSelect" data-field-name="birthDate" name="birthDate">
                </div>
            </div>
            {{else}}
            <input type="hidden" id="birthDate" name="birthDate" value="{{= birthDate}}"/>
            {{/if}}

            <div class="project-create_row">
                <div class="project-create_col-lbl">
                    <div class="pc_col-lbl_title">
                        <spring:message code="country.of.registration" text="default text"> </spring:message>
                    </div>
                </div>
                <div class="project-create_col-val">
                    <select class="pln-select pln-select-xlg prj-crt-input js-country_select" data-planeta-ui="country"
                            name="countryId" value="0"></select>
                </div>
            </div>

            {{if countryId != 1 && type == 'INDIVIDUAL'}}
            <div class="project-create_row">
                <div class="project-create_col-val">
                    <div class="project-create_col-val_cont">
                        <div class="project-create_text-icon">
                            <div class="project-create_text-icon_i">
                                <span class="s-pc-action-warning"></span>
                            </div>
                            <div class="project-create_text-icon_t">
                                <spring:message code="not.resident.for" text="default text"> </spring:message>
                                <a href="http://planeta.ru/faq/article/11!paragraph89">
                                    <spring:message code="special.conditions" text="default text"> </spring:message>
                                </a>.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{/if}}

            <div class="project-create_row">
                <div class="project-create_col-lbl">
                    <div class="pc_col-lbl_title">
                        <spring:message code="city" text="default text"> </spring:message>
                        <div class="js-error"></div>
                    </div>
                </div>
                <div class="project-create_col-val">
                    <input class="form-control control-xlg prj-crt-input js-city-name-input" name="cityNameRus"
                           data-planeta-ui="city" type="text"
                           placeholder="<spring:message code="enter.city" text="default text"> </spring:message>"
                           value="{{= cityNameRus}}">
                    <input class="hide" name="cityId" value="{{= cityId}}">
                </div>
            </div>

            {{tmpl '#campaign-contractor-edit-input-template',{
            title: '<spring:message code="phone.number" text="default text"> </spring:message>',
            value: $data.phone,
            name: 'phone',
            requiredField: true}
            }}

            {{if $data.type && ($data.type == 'INDIVIDUAL_ENTREPRENEUR' || $data.type == 'OOO' || $data.type ==
            'CHARITABLE_FOUNDATION' || $data.type == 'LEGAL_ENTITY' || $data.type == 'CHARITABLE_FOUNDATION')}}
            {{tmpl '#campaign-contractor-edit-input-template',{
            title: '<spring:message code="ogrn" text="default text"> </spring:message>',
            value: $data.ogrn,
            name: 'ogrn',
            requiredField: ($data.type && $data.type != 'INDIVIDUAL') && ($data.countryId == 1 || !$data.countryId),
            label: '<spring:message code="with.ogrn" text="default text"> </spring:message>',
            fluidControl: true}
            }}
            {{else}}
            <input type="hidden" id="ogrn" name="ogrn" value="{{= ogrn}}"/>
            {{/if}}

            {{tmpl '#campaign-contractor-edit-input-template',{
            title: '<spring:message code="inn" text="default text"> </spring:message>',
            value: $data.inn,
            name: 'inn',
            requiredField: ($data.type && $data.type != 'INDIVIDUAL')&&($data.countryId == 1 || !$data.countryId),
            label: '<spring:message code="with.inn" text="default text"> </spring:message>',
            fluidControl: true}
            }}


            {{if $data.countryId == 1 && $data.type && ($data.type == 'OOO' || $data.type == 'CHARITABLE_FOUNDATION' ||
            $data.type == 'LEGAL_ENTITY')}}
            {{tmpl '#campaign-contractor-edit-input-template',{
            title: '<spring:message code="kpp" text="default text"> </spring:message>',
            value: $data.kpp,
            name: 'kpp',
            requiredField: true
            }
            }}
            {{else}}
            <input type="hidden" id="kpp" name="kpp" value="{{= kpp}}"/>
            {{/if}}


            {{if ($data.type == 'INDIVIDUAL' || !$data.type)}}
            {{tmpl '#campaign-contractor-edit-input-template',{
            title: '<spring:message code="passport.number" text="default text"> </spring:message>',
            value: $data.passportNumber,
            name: 'passportNumber',
            requiredField: $data.type == 'INDIVIDUAL' && $data.countryId == 1 && PrivacyUtils.hasAdministrativeRole(),
            label: '<spring:message code="for.physical.persons.from.rf" text="default text"> </spring:message>',
            fluidControl: true}
            }}
            {{/if}}

            {{if $data.countryId == 1 && $data.type && $data.type == 'INDIVIDUAL'}}
            {{tmpl '#campaign-contractor-edit-input-template',{
            title: '<spring:message code="authority" text="default text"> </spring:message>',
            value: $data.authority,
            name: 'authority',
            requiredField: true
            }
            }}
            {{else}}
            <input type="hidden" id="authority" name="authority" value="{{= authority}}"/>
            {{/if}}

            {{if $data.countryId == 1 && $data.type && $data.type == 'INDIVIDUAL'}}
            <div class="project-create_row">
                <div class="project-create_col-lbl">
                    <div class="pc_col-lbl_title">
                        <spring:message code="issue.date" text="default text"> </spring:message>
                        <div class="js-error"></div>
                    </div>
                </div>

                <div class="project-create_col-val">
                    <input class="form-control prj-crt-input prj-crt-input__date" type="text" placeholder="20.10.2016"
                           data-planeta-ui="dateSelect" data-field-name="issueDate" name="issueDate">
                </div>
            </div>
            {{else}}
            <input type="hidden" id="issueDate" name="issueDate" value="{{= issueDate}}"/>
            {{/if}}

            {{if $data.countryId == 1 && $data.type && $data.type == 'INDIVIDUAL'}}
            {{tmpl '#campaign-contractor-edit-input-template',{
            title: '<spring:message code="unit" text="default text"> </spring:message>',
            value: $data.unit,
            name: 'unit',
            requiredField: true
            }
            }}
            {{else}}
            <input type="hidden" id="unit" name="unit" value="{{= unit}}"/>
            {{/if}}

            <div class="project-create_row">
                <div class="project-create_col-lbl">
                    <div class="pc_col-lbl_title">
                        <spring:message code="scan.passport.url" text="default text"> </spring:message>
                        <div class="js-error"></div>
                    </div>
                    <div class="pc_col-lbl_text">
                        {{if $data.type && ($data.type == 'INDIVIDUAL')}}
                        - Скан/фото паспорта основная страница с фото<br>
                        - Скан/фото паспорта страница с пропиской<br>
                        - Скан/фото свидетельство ИНН<br>
                        {{else}}
                        {{if $data.isCharity}}
                        - Свидетельство о государственной регистрации НКО;<br>
                        - Свидетельство о государственной регистрации юридического лица;<br>
                        - Свидетельство о постановке на учет российской организации в налоговом органе по месту ее
                        нахождения;<br>
                        - Приказ и решение о назначении на должность руководителя организации;<br>
                        - Устав организации (все страницы).<br>
                        {{else}}
                        - Скан/фото ИНН<br>
                        - Скан/фото ОГРН<br>
                        {{/if}}
                        {{/if}}
                    </div>
                </div>

                <div class="project-create_col-val">
                    <div class="project-create_col-val_cont">

                        <span class="btn upload-file mrg-t-5"><i class="icon-upload"></i> Загрузить</span>
                        <input type="hidden" id="scanPassportUrl" name="scanPassportUrl" value="{{= scanPassportUrl}}"/>
                        <div id="scanFilesList" style="margin-top: 10px;"><br></div>
                    </div>
                </div>
            </div>


            {{if $data.countryId == 1 && $data.type && ($data.type == 'INDIVIDUAL' || $data.type ==
            'INDIVIDUAL_ENTREPRENEUR' || $data.type == 'OOO' || $data.type == 'CHARITABLE_FOUNDATION' || $data.type ==
            'LEGAL_ENTITY')}}
            {{if $data.type == 'INDIVIDUAL'}}
            {{tmpl '#campaign-contractor-edit-input-template',{
            title: '<spring:message code="legal.address.individual" text="default text"> </spring:message>',
            value: $data.legalAddress,
            name: 'legalAddress',
            requiredField: true,
            rows:2}
            }}
            {{else}}
            {{tmpl '#campaign-contractor-edit-input-template',{
            title: '<spring:message code="legal.address.legal.entity" text="default text"> </spring:message>',
            value: $data.legalAddress,
            name: 'legalAddress',
            requiredField: true,
            rows:1}
            }}
            {{/if}}
            {{else}}
            <input type="hidden" id="legalAddress" name="legalAddress" value="{{= legalAddress}}"/>
            {{/if}}


            {{if $data.countryId == 1 && $data.type && ($data.type == 'INDIVIDUAL_ENTREPRENEUR' || $data.type == 'OOO'
            || $data.type == 'CHARITABLE_FOUNDATION' || $data.type == 'LEGAL_ENTITY')}}
            {{tmpl '#campaign-contractor-edit-input-template',{
            title: '<spring:message code="actual.address" text="default text"> </spring:message>',
            value: $data.actualAddress,
            name: 'actualAddress',
            requiredField: true
            }
            }}
            {{else}}
            <input type="hidden" id="actualAddress" name="actualAddress" value="{{= actualAddress}}"/>
            {{/if}}

            {{if $data.countryId == 1 && $data.type && ($data.type == 'INDIVIDUAL' || $data.type ==
            'INDIVIDUAL_ENTREPRENEUR' || $data.type == 'OOO' || $data.type == 'CHARITABLE_FOUNDATION' || $data.type ==
            'LEGAL_ENTITY')}}
            {{tmpl '#campaign-contractor-edit-input-template',{
            title: '<spring:message code="checking.account" text="default text"> </spring:message>',
            value: $data.checkingAccount,
            name: 'checkingAccount',
            requiredField: true
            }
            }}
            {{else}}
            <input type="hidden" id="checkingAccount" name="checkingAccount" value="{{= checkingAccount}}"/>
            {{/if}}

            {{if $data.countryId == 1 && $data.type && ($data.type == 'INDIVIDUAL' || $data.type ==
            'INDIVIDUAL_ENTREPRENEUR' || $data.type == 'OOO' || $data.type == 'CHARITABLE_FOUNDATION' || $data.type ==
            'LEGAL_ENTITY')}}
            {{tmpl '#campaign-contractor-edit-input-template',{
            title: '<spring:message code="beneficiary.bank" text="default text"> </spring:message>',
            value: $data.beneficiaryBank,
            name: 'beneficiaryBank',
            requiredField: true
            }
            }}
            {{else}}
            <input type="hidden" id="beneficiaryBank" name="beneficiaryBank" value="{{= beneficiaryBank}}"/>
            {{/if}}

            {{if $data.countryId == 1 && $data.type && ($data.type == 'INDIVIDUAL' || $data.type ==
            'INDIVIDUAL_ENTREPRENEUR' || $data.type == 'OOO' || $data.type == 'CHARITABLE_FOUNDATION' || $data.type ==
            'LEGAL_ENTITY')}}
            {{tmpl '#campaign-contractor-edit-input-template',{
            title: '<spring:message code="corresponding.account" text="default text"> </spring:message>',
            value: $data.correspondingAccount,
            name: 'correspondingAccount',
            requiredField: true
            }
            }}
            {{else}}
            <input type="hidden" id="correspondingAccount" name="correspondingAccount"
                   value="{{= correspondingAccount}}"/>
            {{/if}}


            {{if $data.countryId == 1 && $data.type && ($data.type == 'INDIVIDUAL' || $data.type ==
            'INDIVIDUAL_ENTREPRENEUR' || $data.type == 'OOO' || $data.type == 'CHARITABLE_FOUNDATION' || $data.type ==
            'LEGAL_ENTITY')}}
            {{tmpl '#campaign-contractor-edit-input-template',{
            title: '<spring:message code="bic" text="default text"> </spring:message>',
            value: $data.bic,
            name: 'bic',
            requiredField: true
            }
            }}
            {{else}}
            <input type="hidden" id="bic" name="bic" value="{{= bic}}"/>
            {{/if}}


            {{tmpl '#campaign-contractor-edit-input-template',{
            title: '<spring:message code="other.data" text="default text"> </spring:message>',
            value: $data.otherDetails,
            name: 'otherDetails',
            rows:4}
            }}

            <div class="project-create_row">
                <div class="project-create_col-val">
                    <div class="project-create_col-val_cont">
                        <input type="hidden" class="ui-planeta" name="agreePersonalDataProcessing">
                        <div class="checkbox-row flat-ui-control">
                            <span class="checkbox js-agree-personal-data-processing"></span>
                            <span class="checkbox-label js-agree-personal-data-processing" style="cursor: pointer;">
                                <spring:message code="agree.personal.data.processing"
                                                text="default text"> </spring:message>
                            </span>
                            <div class="js-error js-agree-error" style="display: none;">
                                <div class="pc_row_warning" aria-describedby="drop-tooltip-0">
                                    <div class="s-pc-action-warning"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="project-create_row">
                <div class="project-create_col-val">
                    <div class="project-create_col-val_cont">
                        <div class="project-create_terms-msg"><spring:message code="campaigns-edit.jsp.propertie.69"
                                                                              text="default text"> </spring:message> <a
                                href="/welcome/projects-agreement.html"><spring:message
                                code="campaigns-edit.jsp.propertie.89" text="default text"> </spring:message></a>.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</script>

<script id="campaign-promotion-panel-template" type="text/x-jquery-template">
    <div class="project-promotion-bar">
        <div class="project-promotion-bar_head">
            <spring:message code="project.on" text="default text"> </spring:message> <spring:message
                code="campaign.stage" text="default text"/>
            <spring:message code="phase" text="default text"/>
        </div>

        <div class="project-promotion-bar_val">{{= completionPercentage}}%</div>
        <div class="project-promotion-bar_bar">
            <div class="project-promotion-bar_progress-sep sep-25"></div>
            <div class="project-promotion-bar_progress-sep sep-50"></div>
            <div class="project-promotion-bar_progress-sep sep-75"></div>
            <div class="project-promotion-bar_progress" style="width: {{= completionPercentage}}%"></div>
        </div>

        <div class="project-promotion-bar_text">
            <spring:message code="how.to.move" text="default text"> </spring:message>
            {{if stage == 1}}<spring:message code="first.phase" text="default text"> </spring:message>{{/if}}
            {{if stage == 2}}<spring:message code="second.phase" text="default text"> </spring:message>{{/if}}
            {{if stage == 3}}<spring:message code="third.phase" text="default text"> </spring:message>{{/if}}
            {{if stage == 4}}<spring:message code="fourth.phase" text="default text"> </spring:message>{{/if}}
            <spring:message code="phase.without.ending" text="default text"> </spring:message>
        </div>

        <div class="project-promotion-bar_btn">
            <a class="btn btn-primary" href="/welcome/promotion_{{= stage || 1}}.html" data-gaEvent-click="gaSend$"
               data-gaParams="'event', 'view','click', 'project_instructions'">
                <spring:message code="read.instruction" text="default text"> </spring:message>
            </a>
        </div>
    </div>
</script>

<script id="campaign-image-field-template" type="text/x-jquery-template">
    <div class="project-create_col-lbl">
        <div class="pc_col-lbl_title">
            <spring:message code="image" text="default text"> </spring:message>

            {{if errors.imageUrl}}
            <div class="pc_row_warning tooltip" data-tooltip="{{= errors.imageUrl}}">
                <div class="s-pc-action-warning"></div>
            </div>
            {{else}}
            <div class="pc_row_help tooltip"
                 data-tooltip="{{if /edit-shares/.test(window.location.pathname)}}<spring:message code="image.description1" text="default text"> </spring:message>{{else}}<spring:message code="image.description" text="default text"> </spring:message>{{/if}}">

            </div>
            {{/if}}
        </div>

        <div class="pc_col-lbl_text">
            <spring:message code="image.size" text="default text"> </spring:message> {{= sizeSuggestionText}}
        </div>
    </div>

    <div class="project-create_col-val">
        <div class="js-val"></div>
    </div>
</script>

<script id="campaign-load-image-field-template" type="text/x-jquery-template">
    <div class="js-progress"></div>

    <div class="project-create_col-val_cont js-drag-zone">
        <div class="project-create_upload cf">
            <div class="project-create_upload_state-error js-error hide"></div>

            <div class="project-create_upload_va vert-align">
                <div class="vert-align-center">
                    <div class="project-create_upload_list">
                        <div class="project-create_upload_i">
                            <span class="project-create_upload_link fileinput-button">
                                <span class="s-pc-upload-cloud"></span>
                                <span class="project-create_upload_text">
                                    <spring:message code="image.download" text="default text"> </spring:message>
                                    {{if $data.video}}
                                        <spring:message code="or.video" text="default text"> </spring:message>
                                    {{/if}}
                                </span>

                                <input type="file" name="files[]" class="js-input-file">
                            </span>
                        </div>

                        {{if $data.video}}
                        <div class="project-create_upload_i">
                                <span class="project-create_upload_link js-link-to-video">
                                    <span class="s-pc-upload-yt"></span>
                                    <span class="project-create_upload_text">
                                        <spring:message code="youtube.video" text="default text"> </spring:message>
                                    </span>
                                </span>
                        </div>
                        {{/if}}

                        <div class="project-create_upload_i">
                            <span class="project-create_upload_link js-link-to-image">
                                <span class="s-pc-upload-link"></span>
                                <span class="project-create_upload_text">
                                    <spring:message code="photo.url" text="default text"> </spring:message>
                                </span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="project-create_upload_drag">
                <spring:message code="move.image" text="default text"> </spring:message>
            </div>
        </div>
    </div>
</script>

<script id="campaign-crop-image-field-template" type="text/x-jquery-template">
    <div class="project-create_col-val_cont">
        <div class="project-create_media">
            <div class="project-img-view">
                <img class="pmv-img"
                     src="{{if $data.thumbnail}}{{= ImageUtils.getThumbnailUrl(imageUrl, thumbnail.imageConfig, thumbnail.ImageType)}}{{else}}{{= imageUrl}}{{/if}}"
                     {{if width}}style="width: {{= width}}px" {{/if}}>
            </div>
            <div class="project-create_media_action cf">
                <div class="project-create_media_link">
                    <span class="s-pc-action-crop"></span>
                    <span class="project-create_media_link-text" data-event-click="onCropClick">
                        <spring:message code="crop" text="default text"> </spring:message>
                    </span>
                </div>

                <div class="pull-right">
                    <div class="project-create_media_link delete">
                        <span class="s-pc-action-del"></span>
                        <span class="project-create_media_link-text" data-event-click="onRemoveClick">
                            <spring:message code="delete" text="default text"> </spring:message>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>

<script id="campaign-load-image-field-progress-template" type="text/x-jquery-template">
    <div class="project-create_col-val_cont upload-state-loading">
        <div class="project-create_upload_state-loading">
            <div class="state-loading_bar">
                <div class="state-loading_progress"
                     style="width: {{= Math.floor(uploadedSize / fileSize * 100)}}%;"></div>
            </div>

            <div class="state-loading_progress-text">
                {{if status !== FileUploadStatuses.SAVED}}
                <spring:message code="loading" text="default text"> </spring:message>
                {{= Math.floor(uploadedSize / fileSize * 100)}}%
                {{else}}
                <spring:message code="file.processing" text="default text"> </spring:message>
                {{/if}}
            </div>

            {{if status !== FileUploadStatuses.SAVED}}
            <span class="state-loading_link js-cancel">
                    <spring:message code="cancel" text="default text"> </spring:message>
                </span>
            {{/if}}
        </div>
    </div>
</script>

<script id="campaign-modal-crop-image-template" type="text/x-jquery-template">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">×</a>
        <div class="modal-title">
            <spring:message code="image.edit" text="default text"> </spring:message>
        </div>
    </div>

    <div class="modal-body">
        <div class="photo-crop_wrap">
            <img class="photo-crop_img" src="{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.REAL, ImageType.PHOTO)}}">
        </div>
    </div>

    <div class="modal-footer">
        <div class="modal-footer-cont">
            <button type="submit" class="btn btn-primary">
                <spring:message code="save" text="default text"> </spring:message>
            </button>

            <button type="reset" class="btn">
                <spring:message code="cancel" text="default text"> </spring:message>
            </button>
        </div>
    </div>
</script>

<script id="campaign-modal-crop-bg-template" type="text/x-jquery-template">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">×</a>
        <div class="modal-title">
            <spring:message code="crop.background" text="default text"> </spring:message>
        </div>
    </div>

    <div class="modal-body">
        <div class="image-cropper">
            <div class="bg-crop_controls cf">
                {{if backgroundImageUrl}}
                <div class="bg-crop_upload" id="js-bg-crop-upload-other" style="">
                        <span class="bg-crop_link btn-upload">
                            <span class="s-bg-crop-upload"></span>
                            <span class="bg-crop_link_text">
                                <spring:message code="other.background" text="default text"> </spring:message>
                            </span>
                            <input type="file" class="cropit-image-input" accept="image/*">
                        </span>
                </div>

                <div class="bg-crop_del" class="js-bg-crop-del">
                        <span class="bg-crop_link js-bg-crop-del">
                            <span class="s-bg-crop-del"></span>
                            <span class="bg-crop_link_text">
                                <spring:message code="refresh.background" text="default text"> </spring:message>
                            </span>
                        </span>
                </div>
                {{else}}
                <div class="bg-crop_upload" id="js-bg-crop-upload">
                        <span class="bg-crop_link btn-upload">
                            <span class="s-bg-crop-upload"></span>
                            <span class="bg-crop_link_text">
                                <spring:message code="download.background" text="default text"> </spring:message>
                            </span>
                            <input type="file" class="cropit-image-input" accept="image/*">
                        </span>
                </div>
                {{/if}}
            </div>

            <div class="cropit-image-preview"></div>

            <div class="image-cropper-zoom-block">
                <div class="image-cropper-zoom-minus"></div>
                <div class="image-cropper-zoom-plus"></div>
                <div class="cropit-image-zoom-input ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"
                     min="0" max="1" step="0.01">
                    <span class="ui-slider-handle ui-state-default ui-corner-all ui-state-focus" tabindex="0"
                          style="left: 55%;"></span>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-footer">
        <div class="modal-footer-cont">
            <button type="submit" class="btn btn-primary">
                <spring:message code="save" text="default text"> </spring:message>
            </button>
            <button type="reset" class="btn">
                <spring:message code="cancel" text="default text"> </spring:message>
            </button>
        </div>
    </div>
</script>

<script id="campaign-image-field-video-template" type="text/x-jquery-template">
    <div class="project-create_col-val_cont">
        <div class="project-create_media">
            <div class="project-media-view">
                <div class="pmv-cont">
                    <span class="central-block">
                        <img class="pmv-img"
                             src="{{if $data.video.thumbnail}}{{= ImageUtils.getThumbnailUrl(imageUrl, video.thumbnail.imageConfig, video.thumbnail.ImageType)}}{{else}}{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.ORIGINAL, ImageType.VIDEO)}}{{/if}}">
                    </span>
                    <span class="vertical-align"></span>
                    <div class="video-play"></div>
                </div>
            </div>

            <div class="project-create_media_action cf">
                <div class="pull-right">
                    <div class="project-create_media_link delete">
                        <span class="s-pc-action-del"></span>
                        <span class="project-create_media_link-text js-remove">
                            <spring:message code="delete" text="default text"> </spring:message>
                        </span>
                    </div>
                </div>

                <div class="project-create_media_link">
                    <span class="project-create_media_link-text js-change-cover">
                        <spring:message code="change.stop.frame" text="default text"> </spring:message>
                    </span>
                </div>
            </div>
        </div>
    </div>
</script>

<script id="campaign-image-field-change-cover-template" type="text/x-jquery-template">
    <div class="modal-author-info">
        <div class="modal-header">
            <a class="close">×</a>
            <div class="modal-title">
                <spring:message code="change.cover" text="default text"> </spring:message>
            </div>
        </div>

        <div class="project-create_cont">
            <div class="project-create_list">
                <div class="project-create_row js-edit-cover"></div>
            </div>
        </div>

        <div class="modal-footer cf">
            <div class="pull-right">
                <button class="btn btn-primary" type="submit">
                    <spring:message code="save" text="default text"> </spring:message>
                </button>
            </div>

            <button class="btn" type="reset">
                <spring:message code="cancel" text="default text"> </spring:message>
            </button>
        </div>
    </div>
</script>

<script id="finish-campaign-traffic-alert-panel-template" type="text/x-jquery-template">
    <div class="project-finish">
        <div class="project-finish_check"></div>

        <div class="project-finish_cont">
            {{if viewType == 1}}
            <spring:message code="project.view.type1" text="default text"> </spring:message>
            {{else viewType == 2 || viewType == 3}}
            <spring:message code="project.view.type2" text="default text"> </spring:message>
            {{else viewType == 4}}
            <spring:message code="project.view.type3" text="default text"> </spring:message>
            {{else}}
            <spring:message code="project.other.view.types" text="default text"> </spring:message>
            {{/if}}
            <span class="project-finish_arr"></span>
        </div>
    </div>
</script>

<script id="finish-campaign-traffic-active-campaign-template" type="text/x-jquery-template">
    <div class="project-new">
        <div class="project-new_head">
            <div class="status-badge status-badge__m status-badge__success">
                <spring:message code="author.new.project" text="default text"> </spring:message>
            </div>
        </div>
        <div class="project-new_body">
            <div class="project-unit">
                <div class="project-unit_cover">
                    {{if viewImageUrl}}
                    <a href="//{{= workspace.serviceUrls.mainHost}}/campaigns/{{= webCampaignAlias}}">
                        <img src="{{= ImageUtils.getThumbnailUrl(viewImageUrl, ImageUtils.CAMPAIGN_VIDEO, ImageType.PHOTO)}}"
                             {{if imageId==0}}style="width:640px;margin: -60px 0 0"{{/if}}>
                    </a>
                    {{else imageUrl}}
                    <a href="//{{= workspace.serviceUrls.mainHost}}/campaigns/{{= webCampaignAlias}}">
                        <img src="{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.CAMPAIGN_VIDEO, ImageType.PHOTO)}}"
                             alt="{{= name}}">
                    </a>
                    {{/if}}
                </div>
                <div class="project-unit_main">
                    <div class="project-unit_cont">
                        <div class="project-unit_status">
                            <div class="status-badge status-badge__info"><spring:message code="project.status.active"
                                                                                         text="default text"> </spring:message></div>
                        </div>
                        <div class="project-unit_name">
                            <a href="//{{= workspace.serviceUrls.mainHost}}/campaigns/{{= webCampaignAlias}}"> {{html
                                nameHtml}} </a>
                        </div>
                        <div class="project-unit_descr">{{html shortDescriptionHtml}}</div>
                        <div class="project-unit_progress-stat">
                            {{if Math.floor(100*collectedAmount/targetAmount) > 0}}
                            <div class="project-unit_progress-percent"> {{=
                                Math.floor(100*collectedAmount/targetAmount)}}% от цели
                            </div>
                            {{/if}}
                            {{if (!finishOnTargetReach && timeFinish != null) && !(status == "FINISHED")}}
                            <div class="project-unit_progress-days"
                                 data-tooltip='<spring:message code="campaigns-edit.jsp.propertie.66" text="default text"></spring:message> {{html DateUtils.formatDate(timeFinish, "D MMMM YYYY", false, true)}}'>
                                {{if DateUtils.getHoursTo(timeFinish) > 48}}
                                <spring:message code="declension.word.stay2.with.variable"
                                                text="default text"> </spring:message>
                                {{else DateUtils.getMinutesTo(timeFinish) >= 60}}
                                <spring:message code="declension.word.stay3.with.variable"
                                                text="default text"> </spring:message>
                                {{else DateUtils.getMinutesTo(timeFinish) > 0}}
                                <spring:message code="declension.word.stay6.with.variable"
                                                text="default text"> </spring:message>
                                {{/if}}
                                {{if DateUtils.getHoursTo(timeFinish) > 48}}
                                {{= DateUtils.getDaysTo(timeFinish)}}
                                <spring:message code="declension.word.day2.with.variable"
                                                text="default text"> </spring:message>
                                {{else DateUtils.getMinutesTo(timeFinish) >= 60}}
                                {{= DateUtils.getHoursTo(timeFinish)}}
                                <spring:message code="declension.word.hour2.with.variable"
                                                text="default text"> </spring:message>
                                {{else DateUtils.getMinutesTo(timeFinish) > 0}}
                                {{= DateUtils.getMinutesTo(timeFinish)}}
                                <spring:message code="declension.word.minutes.with.variable"
                                                text="default text"> </spring:message>
                                {{/if}}
                            </div>
                            {{/if}}
                        </div>
                        <div class="project-unit_progress">
                            <div class="project-unit_bar"
                                 style="width:{{= (100*collectedAmount/targetAmount) % 100}}%;"></div>
                        </div>
                    </div>
                    <div class="project-unit_meta">
                        <div class="project-unit_btn">
                            <a href="//{{= workspace.serviceUrls.mainHost}}/campaigns/{{= webCampaignAlias}}"
                               class="btn btn-nd-primary">
                                {{if tags && tags[0] && tags[0].mnemonicName == "CHARITY"}}
                                <spring:message code="donate" text="default text"> </spring:message>
                                {{else}}
                                <spring:message code="support.the.project.infinitive"
                                                text="default text"> </spring:message>
                                {{/if}}
                            </a>
                        </div>
                        <div class="project-unit-meta">
                            <div class="project-unit-meta_i project-unit-meta_target">
                                <div class="project-unit-meta_lbl"><spring:message code="target.of.project"
                                                                                   text="default text"> </spring:message></div>
                                <div class="project-unit-meta_val"> {{= StringUtils.humanNumber(targetAmount)}} ₽</div>
                            </div>
                            <div class="project-unit-meta_i project-unit-meta_rewards">
                                <div class="project-unit-meta_lbl">
                                    <spring:message code="purchase" text="default text"/>
                                </div>
                                <div class="project-unit-meta_val"> {{= StringUtils.humanNumber(purchaseCount)}}
                                    <spring:message code="declension.word.time" text="default text"/></div>
                            </div>
                            {{if (!finishOnTargetReach && timeFinish != null) && !(status == "FINISHED")}}
                            <div class="project-unit-meta_i project-unit-meta_finish"
                                 data-tooltip='<spring:message code="campaigns-edit.jsp.propertie.66" text="default text"></spring:message> {{html DateUtils.formatDate(timeFinish, "D MMMM YYYY", false, true)}}'>
                                <div class="project-unit-meta_lbl">
                                    {{if DateUtils.getHoursTo(timeFinish) > 48}}
                                    <spring:message code="declension.word.stay2.with.variable"
                                                    text="default text"> </spring:message>
                                    {{else DateUtils.getMinutesTo(timeFinish) >= 60}}
                                    <spring:message code="declension.word.stay3.with.variable"
                                                    text="default text"> </spring:message>
                                    {{else DateUtils.getMinutesTo(timeFinish) > 0}}
                                    <spring:message code="declension.word.stay6.with.variable"
                                                    text="default text"> </spring:message>
                                    {{/if}}
                                </div>
                                <div class="project-unit-meta_val">
                                    {{if DateUtils.getHoursTo(timeFinish) > 48}}
                                    {{= DateUtils.getDaysTo(timeFinish)}}
                                    <spring:message code="declension.word.day2.with.variable"
                                                    text="default text"> </spring:message>
                                    {{else DateUtils.getMinutesTo(timeFinish) >= 60}}
                                    {{= DateUtils.getHoursTo(timeFinish)}}
                                    <spring:message code="declension.word.hour2.with.variable"
                                                    text="default text"> </spring:message>
                                    {{else DateUtils.getMinutesTo(timeFinish) > 0}}
                                    {{= DateUtils.getMinutesTo(timeFinish)}}
                                    <spring:message code="declension.word.minutes.with.variable"
                                                    text="default text"> </spring:message>
                                    {{/if}}
                                </div>
                            </div>
                            {{/if}}
                            <div class="project-unit-meta_i project-unit-meta_start">
                                <div class="project-unit-meta_lbl"><spring:message code="project.is.started"
                                                                                   text="default text"> </spring:message></div>
                                <div class="project-unit-meta_val"> {{html DateUtils.campaignFormatDate(timeStart)}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>

<script id="finish-campaign-traffic-product-card-one-template" type="text/x-jquery-template">
    <div class="project-sidebar-product">
        <div class="product-card product-card__project-sidebar">
            {{tmpl '#finish-campaign-traffic-product-card-template'}}
        </div>

        <div class="project-sidebar-product_more">
            <a class="project-sidebar-product_more_link"
               href="{{= 'https://' + workspace.serviceUrls.shopAppUrl + '/products/search.html?referrerIds=' + referrerId}}"
               data-gaEvent-click="gaSend$"
               data-gaParams="'event', 'widget','click', 'project_completed-have_new_goods'">
                <spring:message code="show.all.products" text="default text"> </spring:message>
            </a>
        </div>
    </div>
</script>

<script id="finish-campaign-traffic-product-card-template" type="text/x-jquery-template">
    <div class="product-card_i">
        <a class="product-card_link"
           href="{{= 'https://' + workspace.serviceUrls.shopAppUrl + '/products/' + productId}}"
           data-gaEvent-click="gaSend$"
           data-gaParams="'event', 'widget','click', '{{if $data.hasActiveCampaign}}project_completed-have_new_goods&project_clickgood{{else}}project_completed-have_new_goods{{/if}}'">
            <span class="product-card_cover">
                <img width="208" height="208"
                     src="{{= ImageUtils.getThumbnailUrl(coverImageUrl, ImageUtils.PRODUCT_COVER, ImageType.PRODUCT)}}"
                     alt="{{= name}}"/>
            </span>

            <span class="product-card_cont">
                <span class="product-card_name">
                    {{if nameHtml}}
                        {{html nameHtml}}
                    {{else}}
                        {{html name}}
                    {{/if}}
                </span>

                <span class="product-card_price">
                    {{= StringUtils.humanNumber(price)}}&nbsp; <sup class="b-rub">Р</sup>
                </span>

                <span class="product-card_btn">
                    <span class="product-card_arr"></span>
                </span>
            </span>
        </a>
    </div>
</script>

<script id="finish-campaign-traffic-product-count-template" type="text/x-jquery-template">
    <div class="product-card_i">
        <span class="product-card_all">
            <span class="product-card_all_count-val">{{= productsCount}}</span>
            <span class="product-card_all_count-lbl">
                <spring:message code="declension.word.product" text="default text"> </spring:message>
            </span>
            <span class="product-card_all_more">
                <a class="btn btn-lg btn-block"
                   href="{{= 'https://' + workspace.serviceUrls.shopAppUrl + '/products/search.html?referrerIds=' + creatorProfileId}}"
                   data-gaEvent-click="gaSend$"
                   data-gaParams="'event', 'widget','click', '{{if $data.campaignId}}project_completed-have_new_goods&project_clickgood{{else}}project_completed-have_new_goods{{/if}}'">
                    <spring:message code="show.all.products" text="default text"> </spring:message>
                </a>
            </span>
        </span>
    </div>
</script>

<script id="finish-campaign-traffic-product-card-list-with-campaign-template" type="text/x-jquery-template">
    <div class="wrap-row">
        <div class="col-3">
            <div class="project-finish_project">
                <div class="project-finish_cards-label">
                    <spring:message code="new.project" text="default text"> </spring:message>
                </div>

                <div class="project-card-list js-project-card">
                    {{tmpl '#finish-campaign-traffic-campaign-card-template'}}
                </div>
            </div>
        </div>

        <div class="col-9">
            <div class="project-view-products">
                <div class="project-finish_cards-label">
                    <spring:message code="products.of.finished.project" text="default text"> </spring:message>
                </div>

                <div class="product-card">
                    <span class="js-list"></span>
                    {{tmpl '#finish-campaign-traffic-product-count-template'}}
                </div>
            </div>
        </div>
    </div>
</script>

<script id="finish-campaign-traffic-campaign-card-template" type="text/x-jquery-template">
    <div class="project-card-item" id="projectCard-0">
        <a class="project-card-link" href="/campaigns/{{= webCampaignAlias}}" data-gaEvent-click="gaSend$"
           data-gaParams="'event', 'widget','click', '{{if $data.gaEventLabel}}{{= $data.gaEventLabel}}{{else}}project_completed-have_new_goods&project_clickproject{{/if}}'">
            <span class="project-card-cover-wrap">
                <img class="project-card-cover"
                     src="{{if imageUrl}}{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.PROJECT_ITEM)}}{{/if}}"
                     width="220" height="134" alt="{{= name}}">
            </span>

            <span class="project-card-text">
                <span class="project-card-name hyphenate">{{html nameHtml}}</span>
            </span>

            <span class="project-card-footer">
                <span class="project-card-progress">
                    <span class="pcp-bar" style="width:{{= (100*collectedAmount/targetAmount) % 100}}%;"></span>
                </span>

                <span class="project-card-info">
                    <span class="project-card-info-i">
                        <span class="pci-label">
                            <spring:message code="collected" text="default text"> </spring:message>
                            {{if Math.floor(100*collectedAmount/targetAmount) > 0}}
                                {{= Math.floor(100*collectedAmount/targetAmount)}}%
                            {{/if}}
                        </span>

                        <span class="pci-value">{{= StringUtils.humanNumber(collectedAmount)}}
                            <span class="b-rub">Р</span>
                        </span>
                    </span>

                    <span class="project-card-info-i">
                        <span class="pci-label">
                            <spring:message code="target.without.colon" text="default text"> </spring:message>
                        </span>

                        <span class="pci-value">
                            {{= StringUtils.humanNumber(targetAmount)}}
                            <span class="b-rub">Р</span>
                        </span>
                    </span>
                </span>

                <span class="project-card-link-to-project">
                    <span class="btn btn-primary btn-lg btn-block">
                        <spring:message code="go.to.project" text="default text"> </spring:message>
                    </span>
                </span>
            </span>
        </a>
    </div>
</script>

<script id="campaigns-tab-view-template" type="text/x-jquery-template">
    <ul class="project-tab_list">
        <li class="project-tab_i about {{if tab=='about'}}active{{/if}}" data-event-click="onCampaignTabClicked"
            data-tab="about">
            <a href="/campaigns/{{= webCampaignAlias}}" class="project-tab_link">
                <spring:message code="description" text="default text"> </spring:message>
            </a>
        </li>

        {{if faqCount || PrivacyUtils.isAdmin() || PrivacyUtils.hasAdministrativeRole()}}
        <li class="project-tab_i faq {{if tab=='faq'}}active{{/if}}" data-event-click="onCampaignTabClicked"
            data-tab="faq">
            <a href="/campaigns/{{= webCampaignAlias}}/faq" class="project-tab_link">
                FAQ
                {{if faqCount}}
                <span class="project-tab_badge">{{= faqCount}}</span>
                {{/if}}
            </a>
        </li>
        {{/if}}

        {{if updatesCount}}
        <li class="project-tab_i updates {{if tab=='updates'}}active{{/if}}" data-event-click="onCampaignTabClicked"
            data-tab="updates">
            <a href="/campaigns/{{= webCampaignAlias}}/updates" class="project-tab_link">
                <spring:message code="project.news" text="default text"> </spring:message>
                <span class="project-tab_badge">
                    {{= updatesCount}}
                    {{if !tabVisited.updates && newNewsCount}}
                    <span class="project-tab_badge-new"> + {{= newNewsCount }}</span>
                    {{/if}}
                </span>
            </a>
        </li>
        {{/if}}

        <li class="project-tab_i comments {{if tab=='comments'}}active{{/if}}" data-event-click="onCampaignTabClicked"
            data-tab="comments">
            <a href="/campaigns/{{= webCampaignAlias}}/comments" class="project-tab_link">
                <spring:message code="comments" text="default text"> </spring:message>
                {{if commentsCount > 0}}
                <span class="project-tab_badge">
                    {{= commentsCount}}
                    {{if !tabVisited.comments && newCommentCount}}
                    <span class="project-tab_badge-new"> + {{= newCommentCount}}</span>
                    {{/if}}
                </span>
                {{/if}}
            </a>
        </li>

        <li class="project-tab_i backers {{if tab=='backers'}}active{{/if}}" data-event-click="onCampaignTabClicked"
            data-tab="backers">
            <a href="/campaigns/{{= webCampaignAlias}}/backers" class="project-tab_link">
                {{if isCharity}}
                <spring:message code="helped.draft" text="default text"> </spring:message>
                {{else}}
                <spring:message code="shareholders" text="default text"> </spring:message>
                {{/if}}

                {{if backersCount}}
                <span class="project-tab_badge">
                        {{= backersCount}}
                        {{if !tabVisited.backers && newPurchaseCount}}
                        <span class="project-tab_badge-new"> + {{= newPurchaseCount }}</span>
                        {{/if}}
                    </span>
                {{/if}}
            </a>
        </li>
    </ul>
</script>

<script id="breadcrumbs-campaign-page-template" type="text/x-jquery-template">
    <div class="col-12">

        {{if (!$data.subsection ||
        $data.subsection == "about"
        || $data.subsection == "comments"
        || $data.subsection == "backers"
        || $data.subsection == "updates" ) && $data.objectId > 0 }}
        <div class="cont-header_btn-wrap">
            <div class="cont-header_bread-item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a
                    class="cont-header_btn" itemprop="url" href="/search/projects"><span
                    itemprop="title"><spring:message code="all.projects"
                                                     text="default text"> </spring:message></span></a></div>
            {{if tags[0]}}
            <div class="cont-header_bread-item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a
                    class="cont-header_btn" itemprop="url"
                    href="/search/projects?query=&categories={{= tags[0].mnemonicName}}"><span itemprop="title">{{if useEng}}{{= tags[0].engName}}{{else}}{{= tags[0].name}}{{/if}}</span></a>
            </div>
            {{/if}}
        </div>
        {{/if}}
    </div>
</script>

<script id="sidebar-shares" type="text/x-jquery-template">
    <div class="project-view-block_sidebar">
        <div class="project-view-block_sidebar-oleg sidebar-campaign-banner-js"></div>

        <div class="project-view-block_head"><spring:message code="sidebar.head"
                                                             text="default text"> </spring:message></div>
        <div class="action-card-block action-card-view js-action-card-list"></div>
    </div>
</script>

<script id="campaign-common-template" type="text/x-jquery-template">
    {{if (PrivacyUtils.isAdmin() || PrivacyUtils.hasAdministrativeRole()) && !isPreview }}
    {{tmpl '#campaign-admin-capability'}}
    {{/if}}
    {{if status == 'DECLINED' || status == 'PATCH'}}
    <div class="project-status-info">
        <div class="wrap">
            <div class="project-status-info_cont">
                <div class="project-status-info_head">Причина {{if status == 'DECLINED'}}отклонения{{else}}доработки:{{/if}} </div>
                <div class="project-status-info_body js-reason-declined"></div>
            </div>
        </div>
    </div>
    {{/if}}
    <div class="project-view-container">
        <div class="project-view-header">
            {{if backgroundImageUrl}}
            <div class="project-bg">
                <div class="project-bg_img" style="background-image:url({{= backgroundImageUrlSmall}});"></div>
            </div>
            {{/if}}
            {{tmpl '#campaign-base-common-template'}}
            {{if PrivacyUtils.hasAdministrativeRole()}}
            <div class="js-project-info-messages"></div>
            {{/if}}
        </div>
        {{if status == 'FINISHED'}}
        <div class="js-finish-campaign-products-list"></div>
        {{/if}}
        <div class="project-view-block">
            <div class="project-view-block_nav">
                <div class="project-view-block_wrap">
                    <div class="project-view-block_tabs">
                        <div class="project-tab js-campaign-tabs"> </div>
                    </div>
                    {{if status != "FINISHED"}}
                    <div class="project-view-block_action d-tp-block d-none">
                        <div class="project-view-block_donate d-tl-block d-none">
                            <div class="project-view-block_donate-btn">
                                {{tmpl '#project-action-btn'}}
                            </div>
                        </div>
                        <div class="project-view-block_remind" data-tooltip="Отправим письмо на вашу почту<br>за 2 дня до завершения проекта" data-tooltip-position="bottom center" data-tooltip-theme="project-remind-tooltip">
                            <div class="project-view-block_remind-text"> <spring:message code="campaign.remind.me" text="default text"/> </div>
                            <div class="project-view-block_remind-text-ok"> <spring:message code="campaign.remind.me.ok" text="default text"/> </div>
                        </div>
                    </div>
                    {{/if}}
                </div>
            </div>
            <div class="project-view-block_main">
                <div class="project-view-block_wrap js-project-view-block">
                    <div class="project-view-block_content">
                        <div class="project-view-block_content-in">
                            {{tmpl '#about-section'}}

                            {{if !isPreview}}
                            {{tmpl '#faq-section'}}
                            {{/if}}

                            {{if !isPreview}}
                            {{tmpl '#updates-section'}}
                            {{/if}}

                            {{if !isPreview}}
                            {{tmpl '#comments-section'}}
                            {{/if}}

                            {{if !isPreview}}
                            {{tmpl '#backers-section'}}
                            {{/if}}
                        </div>
                    </div>
                    {{tmpl '#sidebar-shares'}}
                </div>
            </div>
        </div>
    </div>
</script>
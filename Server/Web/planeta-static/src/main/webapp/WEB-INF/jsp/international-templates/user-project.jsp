<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script id="user-projects-template" type="text/x-jquery-template">
    {{if $data.showCreatedProjects}}
        <div class="n-own-project">
            <div class="n-own-project_head"><spring:message code="created.campaigns" text="default text"> </spring:message></div>
            <div class="n-own-project_list">
            </div>
        </div>
    {{/if}}

    {{if $data.showEmpty}}
        <div class="n-empty">
            <div class="n-empty_cover">
                <div class="n-empty_img"></div>
            </div>
            <div class="n-empty_text">
                {{if profileType === "USER"}}
                    <spring:message code="supported.none.of.campaigns" text="default text"> </spring:message>
                {{else}}
                    <spring:message code="no.campaigns" text="default text"> </spring:message>
                {{/if}}
            </div>
        </div>
    {{/if}}

    {{if $data.showPreload}}
        {{tmpl '#scrollable-list-loader-template'}}
    {{/if}}

    {{if $data.showBackedProjects}}
        <div class="n-support-project_head">
            {{if workspace.appModel.isCurrentProfileMine()}}
                <div class="checkbox-row flat-ui-control n-support-project_head_action" data-event-click="onShowBackedCampaignsClicked">
                    <span class="checkbox {{if showBackedCampaigns}}active{{/if}} {{if $data.isSendShowBackedCampaigns}}disabled{{/if}}"></span>
                    <span class="checkbox-label"><spring:message code="show.everyone" text="default text"> </spring:message></span>
                </div>
            {{/if}}

            <div class="n-support-project_head_title"><spring:message code="sponsored.campaigns" text="default text"> </spring:message></div>
        </div>
        <div class="n-support-project_list cf"></div>
    {{/if}}

    {{if $data.showRandomProjects}}
        <div class="n-support-project n-support-project__empty">
            <div class="n-support-project_head">
                <div class="n-support-project_head_title">
                    <spring:message code="sponsor.campaigns" text="default text"> </spring:message>
                </div>
            </div>
            <div class="js-random-projects"></div>
            <div class="more-link">
                <a href="/search/projects" class="more-link_text">
                    <spring:message code="see.all" text="default text"> </spring:message>
                </a>
            </div>
        </div>
    {{/if}}
</script>

<script id="user-created-project-item-template" type="text/x-jquery-template">
    <div class="n-own-project_i">
        <div class="n-own-project_cover {{if status == 'FINISHED'}}n-own-project_i__success{{else}}n-own-project_i__{{= status.toLowerCase()}}{{/if}}">
            <a href="/campaigns/{{= webCampaignAlias}}">
                <img class="n-own-project_img" src="{{if imageUrl}}{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.PROJECT_ITEM)}}{{else}}{{= workspace.staticNodesService.getResourceUrl('/images/defaults/photo-album-cover.jpg')}}{{/if}}">
            </a>
            <div class="n-own-project_state">
                {{if status == "FINISHED"}}
                    {{if targetStatus == "SUCCESS"}}
                        <spring:message code="successful.campaign" text="default text"> </spring:message>
                    {{else}}
                        <spring:message code="finished.campaign" text="default text"> </spring:message>
                    {{/if}}
                {{else status == "APPROVED"}}
                    <spring:message code="approved" text="default text"> </spring:message>
                {{else status == "ACTIVE"}}
                    <spring:message code="collection.on" text="default text"> </spring:message>
                {{else status == "DRAFT"}}
                    <spring:message code="draft" text="default text"> </spring:message>
                {{else status == "NOT_STARTED"}}
                    <spring:message code="on.moderation" text="default text"> </spring:message>
                {{else status == "PAUSED"}}
                    <spring:message code="paused" text="default text"> </spring:message>
                {{else status == "PATCH"}}
                    <spring:message code="on.rework" text="default text"> </spring:message>
                {{else status == "DELETED"}}
                    <spring:message code="deleted" text="default text"> </spring:message>
                {{else status == "DECLINED"}}
                    <spring:message code="declined" text="default text"> </spring:message>
                {{else}}
                    {{= status}}
                {{/if}}
            </div>

            {{if $data.newEventsCount > 0}}
                <div class="n-own-project_badge">{{if newEventsCount <= 20}}{{= newEventsCount}}{{else}}20+{{/if}}</div>
            {{else status == "DRAFT"}}
                <a href="javascript:void(0)" data-event-click="onDelete" class="n-own-project_badge badge-draft">
                    <spring:message code="delete" text="default text"> </spring:message>
                </a>
            {{/if}}
        </div>

        <div class="n-own-project_cont">
            <div class="n-own-project_name"><a href="/campaigns/{{= webCampaignAlias}}">{{html nameHtml}}</a></div>
            <div class="n-own-project_descr">{{html shortDescriptionHtml}}</div>
            <div class="n-own-project_{{if status == 'DRAFT' || status == 'NOT_STARTED'}}no-{{/if}}progress {{if Math.floor(100*collectedAmount/targetAmount) > 100}} over-progress{{/if}}">
                {{if Math.floor(100*collectedAmount/targetAmount) > 100}}
                <div class="n-own-project_bar over-progress" style="width: {{= Math.floor(100*collectedAmount/targetAmount) - 100}}%;"></div>
                {{else Math.floor(100*collectedAmount/targetAmount) > 0}}
                <div class="n-own-project_bar" style="width: {{= Math.floor(100*collectedAmount/targetAmount)}}%;"></div>
                {{else}}
                <div class="n-own-project_bar" style="width: 0%;"></div>
                {{/if}}
            </div>
            <div class="n-own-project_info">
                <div class="n-own-project_info_i">
                    <div class="n-own-project_info_lbl">
                        <spring:message code="collected.without.colon" text="default text"> </spring:message>
                    </div>
                    <div class="n-own-project_info_val">{{= StringUtils.humanNumber(collectedAmount)}} <span class="b-rub">Р</span></div>
                </div>
                <div class="n-own-project_info_i">
                    <div class="n-own-project_info_lbl">
                        <spring:message code="target.without.colon" text="default text"> </spring:message>
                    </div>
                    <div class="n-own-project_info_val">{{= StringUtils.humanNumber(targetAmount)}} <span class="b-rub">Р</span></div>
                </div>
            </div>
        </div>
        <div class="js-statistics"></div>
    </div>
</script>

<script id="user-backed-project-item-pager-template" type="text/x-jquery-template">
    <div class="more-link"><spring:message code="see.all2" text="default text"> </spring:message> <b>{{= collection.totalCount}}</b> <spring:message code="decl.campaign" text="default text"> </spring:message></div>
</script>

<script id="user-created-project-short-stat-template" type="text/x-jquery-template">
    <div class="n-own-project_stat">
        <div class="n-own-project_stat_arr"></div>
        <div class="n-own-project_stat_list">
            <a class="n-own-project_stat_i" href="/campaigns/{{= webCampaignAlias}}">
                <span class="n-own-project_stat_lbl">
                    <spring:message code="collected.without.colon" text="default text"> </spring:message>
                </span>
                <span class="n-own-project_stat_val">{{= StringUtils.humanNumber(allAmount)}}<span class="b-rub">Р</span></span>
                <span class="n-own-project_stat_inc {{if newAmount}} plus{{/if}}">
                    {{if newAmount}}
                        + {{= StringUtils.humanNumber(newAmount)}} <span class="b-rub">Р</span>
                    {{else}}
                        &nbsp;
                    {{/if}}
                </span>
                    <span class="n-own-project_stat_ico">
                        <span class="s-project-stat-amount"></span>
                    </span>
            </a>

            <a class="n-own-project_stat_i" href="/campaigns/{{= webCampaignAlias}}/orders">
                <span class="n-own-project_stat_lbl">
                    <spring:message code="purchases" text="default text"> </spring:message>
                </span>
                <span class="n-own-project_stat_val">{{= StringUtils.humanNumber(allPurchaseCount)}}</span>
                <span class="n-own-project_stat_inc {{if newPurchaseCount}} plus{{/if}}">
                    {{if newPurchaseCount}}
                        + {{= StringUtils.humanNumber(newPurchaseCount)}}
                    {{else}}
                        &nbsp;
                    {{/if}}

                </span>
                    <span class="n-own-project_stat_ico">
                        <span class="s-project-stat-purchase"></span>
                    </span>
            </a>

            <a class="n-own-project_stat_i" href="/campaigns/{{= webCampaignAlias}}/comments">
                <span class="n-own-project_stat_lbl">
                    <spring:message code="comments2" text="default text"> </spring:message>
                </span>
                <span class="n-own-project_stat_val">{{= StringUtils.humanNumber(allCommentCount)}}</span>
                <span class="n-own-project_stat_inc {{if newCommentCount}} plus{{/if}}">
                    {{if newCommentCount}}
                        + {{= StringUtils.humanNumber(newCommentCount)}}
                    {{else}}
                        &nbsp;
                    {{/if}}
                </span>
                    <span class="n-own-project_stat_ico">
                        <span class="s-project-stat-comments"></span>
                    </span>
            </a>

            <a class="n-own-project_stat_i" href="/campaigns/{{= webCampaignAlias}}/updates">
                <span class="n-own-project_stat_lbl">
                    <spring:message code="news" text="default text"> </spring:message>
                </span>
                <span class="n-own-project_stat_val">{{= StringUtils.humanNumber(allNewsCount)}}</span>
                <span class="n-own-project_stat_inc {{if newNewsCount}} plus{{/if}}">
                    {{if newNewsCount}}
                        + {{= StringUtils.humanNumber(newNewsCount)}}
                    {{else}}
                        &nbsp;
                    {{/if}}
                </span>
                    <span class="n-own-project_stat_ico">
                        <span class="s-project-stat-news"></span>
                    </span>
            </a>
        </div>
    </div>
</script>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script id="service-unavailable-template" type="text/x-jquery-template">
    <div class="loader service-unavailable">
        {{if status == 0}}
            <div class="loader-message">
                <span>
                    <spring:message code="modal.jsp.propertie.1" text="default text"> </spring:message>
                </span>
            </div>
        {{else}}
            <div class="loader-message">
                <span>
                    <spring:message code="modal.jsp.propertie.2" text="default text"> </spring:message>
                </span>
            </div>
        {{/if}}
    </div>
</script>

<%-----------------------------------------------------------------------------------------------%>
<%---------------------------------------- Header templates -------------------------------------%>
<%-----------------------------------------------------------------------------------------------%>

<script id="anonymous-reminder-dialog" type="text/x-jquery-template">
    <div class="modal-header">
        <a class="close">×</a>

        <div class="modal-title">
            <spring:message code="modal.jsp.propertie.3" text="default text"> </spring:message>
        </div>

        <div class="modal-header-shadow"></div>
    </div>

    <div class="modal-body">
        <div class="reg-img"></div>
        <div class="reg-cont">
            <div class="reg-cont-head">
                <spring:message code="modal.jsp.propertie.4" text="default text"> </spring:message>
            </div>

            <div class="reg-cont-text">
                <spring:message code="modal.jsp.propertie.5" text="default text"> </spring:message>
                <a href="javascript:void(0)" class="repeat-email">
                    <spring:message code="modal.jsp.propertie.6" text="default text"> </spring:message>
                </a>.
            </div>
        </div>
    </div>

    <div class="modal-footer">
        <div class="modal-footer-cont">
            <a href="javascript:void(0)" class="btn btn-primary cancel">
                <spring:message code="modal.jsp.propertie.7" text="default text"> </spring:message>
            </a>
        </div>
    </div>
</script>

<script id="auth-modal-dialog" type="text/x-jquery-template">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">×</a>
        <div class="modal-title">
            {{if action}}
                {{if action=='registration'}}
                    <spring:message code="modal.jsp.propertie.8" text="default text"> </spring:message>
                {{else action=='signup'}}
                    <spring:message code="modal.jsp.propertie.9" text="default text"> </spring:message>
                {{else}}
                    <spring:message code="modal.jsp.propertie.10" text="default text"> </spring:message>
                {{/if}}
            {{/if}}
        </div>

        <div class="modal-header-shadow"></div>
    </div>

    <div class="modal-body"></div>

    <div class="modal-footer">
        <div class="modal-footer-cont">
            <div class="mlf-soc-enter">
                <span class="mlf-soc-text">
                    <spring:message code="modal.jsp.propertie.11" text="default text"> </spring:message>
                </span>

                <div class="social-enter-block">
                    <div class="social-enter-item">
                        <span class="sei-mail"></span>
                    </div>

                    <div class="social-enter-item">
                        <span class="sei-vk"></span>
                    </div>

                    <div class="social-enter-item">
                        <span class="sei-fb"></span>
                    </div>

                    <div class="social-enter-item">
                        <span class="sei-ok"></span>
                    </div>

                    <div class="social-enter-item">
                        <span class="sei-ya"></span>
                    </div>
                </div>

                <div id="social-frame"></div>
            </div>
        </div>
    </div>
</script>

<%-----------------------------------------------------------------------------------------------%>
<%------------------------------------ Common modal dialogs -------------------------------------%>
<%-----------------------------------------------------------------------------------------------%>

<script id="modal-confirmation" type="text/x-jquery-template">
    <div class="modal-header">
        <a class="close" data-dismiss="modal" href="javascript:void(0)">×</a>

        <div class="modal-title">{{= title}}</div>
        <div class="modal-header-shadow small"></div>
    </div>

    <div class="modal-body">
        <p>{{html text}}</p>
        {{if $data.hasTextArea}}
        <p><textarea class="form-control" id="modalReply" placeholder="{{= $data.placeholderText}}"></textarea></p>
        {{/if}}
    </div>

    <div class="modal-footer">
        <div class="modal-footer-cont">
            {{if $data.checkboxName}}
                <span class="delete-audio-modal-footer">
                    <span class="js-checkbox">
                        <span class="checkbox"></span>
                        <span class="checkbox-label">
                            {{= $data.checkboxText || "<spring:message code="modal.jsp.propertie.12" text="default text"> </spring:message>"}}
                        </span>
                    </span>
                </span>
            {{/if}}

            <button type="submit" class="btn btn-primary">{{= $data.okButtonText || "<spring:message code="modal.jsp.propertie.7" text="default text"> </spring:message>"}}</button>

            {{if hasCancelButton}}
                <button type="reset" class="btn">{{= $data.cancelButtonText || "<spring:message code="modal.jsp.propertie.13" text="default text"> </spring:message>"}}</button>
            {{/if}}
        </div>
    </div>
</script>

<script id="change-password-dialog-template" type="text/x-jquery-template">
    <div class="modal-header">
        <div class="modal-title">
            <span id="title">
                <spring:message code="modal.jsp.propertie.14" text="default text"> </spring:message>
            </span>

            <a href="javascript:void(0)" class="close">×</a>
        </div>
    </div>

    <form action="/welcome/recover-password.json" method="post">
        <div class="modal-body">
            <div class="clearfix">
                <div class="span10">
                    <p>
                        <spring:message code="modal.jsp.propertie.15" text="default text"> </spring:message>
                    </p>

                    <input type="hidden" name="regCode" value="{{= regCode}}"/>

                    <div class="modal-select-header short-label">
                        <div class="fieldset">
                            <div class="label-name">
                                <spring:message code="modal.jsp.propertie.16" text="default text"> </spring:message>
                            </div>

                            <div class="input input-field">
                                <input class="form-control" type="password" name="password"/>
                            </div>
                        </div>
                    </div>

                    <div class="modal-select-header short-label">
                        <div class="fieldset">
                            <div class="label-name">
                                <spring:message code="modal.jsp.propertie.17" text="default text"> </spring:message>
                            </div>

                            <div class="input input-field">
                                <input class="form-control" type="password" name="confirmation"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <div class="modal-footer-cont">
                <button type="submit" class="btn btn-primary primary">
                    <spring:message code="modal.jsp.propertie.18" text="default text"> </spring:message>
                </button>
            </div>
        </div>
    </form>
</script>



<%-----------------------------------------------------------------------------------------------%>
<%---------------------------------------- Footer templates -------------------------------------%>
<%-----------------------------------------------------------------------------------------------%>

<script id="feedback-form-template-login"  type="text/x-jquery-template">
    <a class="close">×</a>
    <div class="modal-project-feedback-form fade in">
        <div class="m-project-feedback-title">
            <spring:message code="modal.jsp.propertie.19" text="default text"> </spring:message>
        </div>

        <div class="modal-select-header">
            <div class="fieldset">
                <label>
                    <spring:message code="modal.jsp.propertie.20" text="default text"> </spring:message>
                </label>

                <div class="select-field select-field-l">
                    <select class="pln-select-l" name="feedback-theme-select">
                        {{if !$data.theme || $data.theme != 'payment'}}
                        <option value="<spring:message code="modal.jsp.propertie.21" text="default text"> </spring:message>" >
                            <spring:message code="modal.jsp.propertie.21" text="default text"> </spring:message>
                        </option>
                        {{/if}}
                        <option value="<spring:message code="modal.jsp.propertie.22" text="default text"> </spring:message>" >
                            <spring:message code="modal.jsp.propertie.22" text="default text"> </spring:message>
                        </option>
                        {{if !$data.theme || $data.theme != 'payment'}}
                        <option value="<spring:message code="modal.jsp.propertie.23" text="default text"> </spring:message>">
                            <spring:message code="modal.jsp.propertie.23" text="default text"> </spring:message>
                        </option>
                        <option value="<spring:message code="modal.jsp.propertie.24" text="default text"> </spring:message>">
                            <spring:message code="modal.jsp.propertie.24" text="default text"> </spring:message>
                        </option>
                        <option value="<spring:message code="modal.jsp.propertie.25" text="default text"> </spring:message>">
                            <spring:message code="modal.jsp.propertie.25" text="default text"> </spring:message>
                        </option>
                        {{/if}}
                        <option value="<spring:message code="modal.jsp.propertie.26" text="default text"> </spring:message>">
                            <spring:message code="modal.jsp.propertie.26" text="default text"> </spring:message>
                        </option>
                        <option value="<spring:message code="modal.jsp.propertie.27" text="default text"> </spring:message>">
                            <spring:message code="modal.jsp.propertie.27" text="default text"> </spring:message>
                        </option>
                    </select>
                </div>
            </div>
            
            <div class="fieldset">
                <label>
                    <spring:message code="modal.jsp.propertie.28" text="default text"> </spring:message>
                </label>

                <div class="input-field input-field-l">
                    <textarea name="message" id="message" class="input-l" rows="8"></textarea>
                </div>
            </div>

            {{if  !isAuthorized}}
                <div class="fieldset">
                    <label>
                        <spring:message code="modal.jsp.propertie.29" text="default text"> </spring:message>
                    </label>

                    <div class="input-field input-field-l">
                        <input name="email" id="email" class="input-l" type="text">
                    </div>
                </div>
            {{/if}}
        </div>

        <div class="text-center">
            <a class="btn btn-primary" type="submit" href="javascript:void(0)">
                <spring:message code="modal.jsp.propertie.30" text="default text"> </spring:message>
            </a>
        </div>
    </div>

    <div class="modal-feedback-success clearfix fade hide">
        <div class="content-alert-success">
            <i class="three-types-alerts-success"></i>

            <p class="big">
                <spring:message code="modal.jsp.propertie.31" text="default text"> </spring:message>
            </p>

            <p>
                <spring:message code="modal.jsp.propertie.32" text="default text"> </spring:message>
            </p>
        </div>
    </div>
</script>




<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script id="user-news-campaign-template" type="text/x-jquery-template">
    {{if type=='CAMPAIGN_STARTED'}}
    <div class="feed_project">
        <div class="feed_project_progress">0%</div>
        <div class="feed_project_cont">
            <spring:message code="campaign.c.l" text="default text"> </spring:message> <a href="/campaigns/{{= webCampaignAlias }}">{{= campaignName }}</a> <spring:message code="started.and.support" text="default text"> </spring:message>
            <span class="feed_project_date">{{= DateUtils.formatDate(timeAdded)}}</span>
        </div>
    </div>
    {{else type=='CAMPAIGN_RESUMED'}}
    <div class="feed_project">
        <div class="feed_project_progress">{{= campaignProgress }}%</div>
        <div class="feed_project_cont">
            <spring:message code="campaign.c.l" text="default text"> </spring:message> <a href="/campaigns/{{= webCampaignAlias }}">{{= campaignName }}</a> <spring:message code="restarted.and.support" text="default text"> </spring:message>
            <span class="feed_project_date">{{= DateUtils.formatDate(timeAdded)}}</span>
        </div>
    </div>
    {{else type=='CAMPAIGN_FINISHED_SUCCESS'}}

    <div class="feed_project feed_project__100">
        <div class="feed_project_progress">{{= campaignProgress }}%</div>
        <div class="feed_project_cont">
            <spring:message code="campaign.c.l" text="default text"> </spring:message> <a href="/campaigns/{{= webCampaignAlias }}">{{= campaignName }}</a> <spring:message code="successfully.completed" text="default text"> </spring:message>
        </div>
    </div>
    {{else type=='CAMPAIGN_FINISHED_FAIL'}}
    <div class="feed_project feed_project__fail">
        <div class="feed_project_progress">{{= campaignProgress }}%</div>
        <div class="feed_project_cont">
            <spring:message code="campaign.c.l" text="default text"> </spring:message> <a href="/campaigns/{{= webCampaignAlias }}">{{= campaignName }}</a> завершен.
            {{if $data.charity}}Собранная сумма будет переведена на счет благотворительной организации.{{else}}Проект не достиг своей цели, деньги будут возвращены спонсорам.{{/if}}
        </div>
    </div>
    {{else type=='CAMPAIGN_REACHED_25'}}
    <div class="feed_project feed_project__50">
        <div class="feed_project_progress">25%</div>
        <div class="feed_project_cont">
            <spring:message code="campaign.c.l" text="default text"> </spring:message> <a href="/campaigns/{{= webCampaignAlias }}">{{= campaignName }}</a> достиг 25%! Сбор средств продолжается.
            <span class="feed_project_date">{{= DateUtils.formatDate(timeAdded)}}</span>
        </div>
    </div>
    {{else type=='CAMPAIGN_REACHED_50'}}
    <div class="feed_project feed_project__50">
        <div class="feed_project_progress">50%</div>
        <div class="feed_project_cont">
            <spring:message code="campaign.c.l" text="default text"> </spring:message> <a href="/campaigns/{{= webCampaignAlias }}">{{= campaignName }}</a> достиг 50%, а значит уже успешен! Сбор средств продолжается.
            <span class="feed_project_date">{{= DateUtils.formatDate(timeAdded)}}</span>
        </div>
    </div>
    {{else type=='CAMPAIGN_REACHED_75'}}
    <div class="feed_project feed_project__50">
        <div class="feed_project_progress">75%</div>
        <div class="feed_project_cont">
            <spring:message code="campaign.c.l" text="default text"> </spring:message> <a href="/campaigns/{{= webCampaignAlias }}">{{= campaignName }}</a> достиг 75%, а значит уже успешен! Сбор средств продолжается.
            <span class="feed_project_date">{{= DateUtils.formatDate(timeAdded)}}</span>
        </div>
    </div>
    {{else type=='CAMPAIGN_REACHED_100'}}
    <div class="feed_project feed_project__100">
        <div class="feed_project_progress">100%</div>
        <div class="feed_project_cont">
            <spring:message code="campaign.c.l" text="default text"> </spring:message> <a href="/campaigns/{{= webCampaignAlias }}">{{= campaignName }}</a> достиг 100%! Сбор средств продолжается.
            <span class="feed_project_date">{{= DateUtils.formatDate(timeAdded)}}</span>
        </div>
    </div>
    {{/if}}
</script>

<script id="empty-user-news-template" type="text/x-jquery-template">
    <div class="n-empty">
        <div class="n-empty_cover">
            <div class="n-empty_img"></div>
        </div>
        <div class="n-empty_text">
            <spring:message code="no.news" text="default text"> </spring:message>
        </div>
    </div>
</script>

<script id="user-edit-post-modal-template" type="text/x-jquery-template">
    <div class="modal-body">
        <a class="close">×</a>
        <div class="post-write post-write__open">
            <div class="post-write_head">
                <input class="post-write_head_input" type="text" value="{{if postName}}{{= postName}}{{/if}}" placeholder="<spring:message code="add.new" text="default text"> </spring:message>">
            </div>
            <div class="post-write_body">
                <div id="description-editor-stub" class="campaign-tinymce-loader"><div class="campaign-tinymce-loader-in"></div></div>
                <div data-anchor="tiny-mce"></div>
            </div>
            <div class="post-write_action">
                <span class="post-write_action_close btn btn-lg" data-event-click="cancel"><spring:message code="close" text="default text"> </spring:message></span>
                <span class="post-write_action_post btn btn-primary btn-lg" data-event-click="onSave"><spring:message code="publish" text="default text"> </spring:message></span>
            </div>
        </div>
    </div>
</script>

<script id="user-create-post-template" type="text/x-jquery-template">
    <div class="post-write_head">
        <input class="post-write_head_input" data-event-focus="onFocus"  type="text" placeholder="<spring:message code="add.new" text="default text"> </spring:message>">
    </div>
    <div class="post-write_body">
        <div id="description-editor-stub" class="campaign-tinymce-loader"><div class="campaign-tinymce-loader-in"></div></div>
        <div data-anchor="tiny-mce"></div>
    </div>
    <div class="post-write_action">
        <span class="post-write_action_close btn btn-lg" data-event-click="onClose" ><spring:message code="close" text="default text"> </spring:message></span>
        <span class="post-write_action_post btn btn-primary btn-lg" data-event-click="onSave"><spring:message code="publish" text="default text"> </spring:message></span>
    </div>
</script>

<script id="user-news-post-item-template" type="text/x-jquery-template">
    <div class="feed_ava">
        <img class="feed_ava_img" src="{{= ImageUtils.getUserAvatarUrl(authorImageUrl, ImageUtils.SMALL_AVATAR, authorGender)}}">
    </div>
    <div class="feed_cont">
        <div class="feed_from"><a href="{{= ProfileUtils.getAbsoluteUserLink(profileId)}}">{{= authorName}}</a></div>
        <div class="feed_post">
            <div class="feed_name" data-event-click="onClick">{{= postName}}</div>
            <div class="feed_meta">
                <a href="{{= postUrl}}" class="feed_meta_i" data-event-click="onClick">{{= DateUtils.formatDate(timeAdded)}}</a>
                {{if campaignId > 0}}
                <span class="feed_meta_i"><spring:message code="campaign" text="default text"> </spring:message> «<a href="https://{{= workspace.serviceUrls.mainHost}}/campaigns/{{= webCampaignAlias }}">{{= campaignName }}</a>»</span>
                {{/if}}
            </div>
            <div class="feed_text" data-event-click="onClick">
                {{if text.length > News.AVAILABLE_POST_LENGTH_WITHOUT_CUT}}
                {{html headingText}}
                {{else}}
                {{html text}}
                {{/if}}
                <div class="feed_more cf">
                    {{if text.length > News.AVAILABLE_POST_LENGTH_WITHOUT_CUT}}
                    <span class="feed_more_link"><spring:message code="read.more" text="default text"> </spring:message></span>
                    {{/if}}
                    <span class="share_link" data-event-click="showSharing"><spring:message code="share" text="default text"> </spring:message></span>
                </div>
                <div class="feed_sharing {{if sharingIsVisible}}visible{{else}}hidden{{/if}}">
                    <div class="sharing-popup-social donate-sharing"></div>
                </div>
            </div>
        </div>
        <div class="feed_comment">
            <div class="comments-list"></div>
        </div>
    </div>
</script>

<script id="blog-search-block-template" type="text/x-jquery-template">
    <div class="n-search">
        <div class="wrap">
            <div class="col-12">
                <input class="n-search_input" type="text" placeholder="<spring:message code="want.find" text="default text"> </spring:message>" value="{{= query}}">
            </div>
        </div>
    </div>

    <div class="wrap">
        <div class="col-12">
            <div class="wrap-row">
                <div class="col-8">
                    <div class="wrap-indent-right-half">
                        <div class="n-content">
                            <div class="feed"></div>
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <div class="n-search_blog-filter">
                        <div class="n-search_blog-filter_head">
                            <spring:message code="result.filter" text="default text"> </spring:message>
                        </div>

                        <div class="n-search_blog-filter_cont">
                            <div class="radiobox-row flat-ui-control">
                                <span class="radiobox active" data-type="ALL"></span>
                                <span class="radiobox-label"><spring:message code="all.notes" text="default text"> </spring:message></span>
                            </div>

                            <div class="radiobox-row flat-ui-control">
                                <span class="radiobox" data-type="CAMPAIGNS"></span>
                                <span class="radiobox-label"><spring:message code="campaign.updates" text="default text"> </spring:message></span>
                            </div>

                            <div class="radiobox-row flat-ui-control">
                                <span class="radiobox" data-type="USERS"></span>
                                <span class="radiobox-label"><spring:message code="users.notes" text="default text"> </spring:message></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>

<script id="user-news-post-content-template" type="text/x-jquery-template">
    {{if postName}}
    <div class="feed_name">{{= postName}}</div>
    <div class="feed_meta">
        <span class="feed_meta_i">{{= DateUtils.formatDate(timeAdded)}}</span>
    </div>
    {{else}}
    <div class="feed_meta">
        <span class="feed_meta_i"><spring:message code="published" text="default text"> </spring:message> {{= DateUtils.formatDate(timeAdded)}}</span>
    </div>
    {{/if}}
    <div class="feed_text">
        {{html text}}
    </div>
</script>

<script id="user-filter-post-template" type="text/x-jquery-template">
    <div class="feed_filter_list">
        <div class="feed_filter_i {{if !onlyMyNews}} active {{/if}}">
            <span class="feed_filter_link" data-event-click="showAllNews">
                {{if !onlyMyNews}}
                    <spring:message code="shown.all" text="default text"> </spring:message>
                {{else}}
                    <spring:message code="show.all.news" text="default text"> </spring:message>
                {{/if}}
            </span>
        </div>

        <div class="feed_filter_i feed_filter_i__right {{if onlyMyNews}} active {{/if}}">
            <span class="feed_filter_link" data-event-click="showMyNews">
                {{if onlyMyNews}}
                    <spring:message code="shown" text="default text"> </spring:message>
                {{else}}
                    <spring:message code="show.my.news" text="default text"> </spring:message>
                {{/if}}
            </span>
        </div>
    </div>
</script>

<script id="user-news-post-campaign-card-template" type="text/x-jquery-template">
    <div class="n-own-project_i">
        <div class="n-own-project_cover">
            <a href="https://{{= workspace.serviceUrls.mainHost}}/campaigns/{{= webCampaignAlias}}"><img class="n-own-project_img" src="{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.PROJECT_ITEM)}}"></a>
            {{if status == 'ACTIVE'}}<div class="n-own-project_state"><spring:message code="gathering" text="default text"> </spring:message></div>{{/if}}
        </div>

        <div class="n-own-project_add">
            <div class="n-own-project_info">
                <div class="n-own-project_info_i">
                    <div class="n-own-project_info_lbl"><spring:message code="collected" text="default text"> </spring:message></div>
                    <div class="n-own-project_info_val" style="overflow: visible;">{{= StringUtils.humanNumber(collectedAmount)}} <span class="b-rub">Р</span></div>
                </div>
            </div>
            {{if status == 'ACTIVE'}}
            <div class="n-own-project_action">
                <span class="btn btn-primary btn-block" data-event-click="onDonate"><spring:message code="support" text="default text"> </spring:message></span>
            </div>
            {{/if}}
        </div>

        <div class="n-own-project_cont">
            <div class="n-own-project_name"><a href="https://{{= workspace.serviceUrls.mainHost}}/campaigns/{{= webCampaignAlias}}">{{html nameHtml}}</a></div>
            <div class="n-own-project_descr">{{= shortDescription}}</div>
            <div class="n-own-project_progress tooltip" data-tooltip="{{= progressText}}%" >
                <div class="pr-value">{{= progressText}}%</div>
                <div class="n-own-project_bar" style="width:{{= progress}}%;"></div>
                {{each over100ProgressBars}}
                <div class="pr-bar-over" style="width:{{= $value}}%;"></div>
                {{/each}}
            </div>
        </div>
    </div>
</script>

<script id="user-edit-post-admin-panel-template" type="text/x-jquery-template">
    <span class="btn pull-right" data-event-click="onDelete"><spring:message code="delete" text="default text"> </spring:message></span>
    <span class="btn" data-event-click="onEdit"><spring:message code="edit" text="default text"> </spring:message></span>
    {{if campaignId}}
    <span class="btn tooltip {{if typeof canSendSpam === 'undefined' || !canSendSpam}}disabled{{/if}}" {{if canSendSpam}}data-event-click="onSpam"{{/if}}
    data-tooltip="<spring:message code="dispatch.limit" text="default text"> </spring:message> {{= notificationDaysLimit}} <spring:message code="decl.word.day" text="default text"> </spring:message>
    {{if notificationTime}}, <spring:message code="last.dispatch" text="default text"> </spring:message>: {{= DateUtils.formatDate(notificationTime)}}{{/if}}">
    <spring:message code="start.dispatch" text="default text"> </spring:message>
    </span>
    {{/if}}
</script>

<script id="post-comment-item-template" type="text/x-jquery-template">
    {{if deleted}}
        {{tmpl '#deleted-comment-template-item'}}
    {{else}}
        {{if showCloseButton}}
            <div class="close tooltip close-small" data-tooltip="<spring:message code="delete" text="default text"> </spring:message>">×</div>
        {{/if}}

        <div class="attach-comment-ava">
            {{tmpl "#profile-author-small-avatar", $data}}
        </div>

        <div class="attach-comment-text" id="comment{{= commentId}}">
            <div class="name">
                <span  itemscope itemtype="http://schema.org/Person" itemprop="creator">
                    {{tmpl "#profile-author-display-name", $data}}
                </span>

                {{if parentCommentAuthor}}
                    {{if parentCommentId > 0}}
                        {{if objectType != "PRODUCT"}}
                            <span class="answer-to-user">{{if authorGender=="FEMALE"}}<spring:message code="answered.female" text="default text"> </spring:message>{{else}}<spring:message code="answered.male" text="default text"> </spring:message>{{/if}}</span>
                        {{else}}
                            <span class="answer-to-user">{{if authorGender=="FEMALE"}}<spring:message code="answered.female" text="default text"> </spring:message>{{else}}<spring:message code="answered.male" text="default text"> </spring:message>{{/if}}</span>
                        {{/if}}
                    {{/if}}
                {{/if}}

                <div class="date updatable-time" data-updatable-time="{{= timeAdded}}">{{=
                    DateUtils.formatDate(timeAdded)}}
                </div>

                <span class="meta-sep">&bull;</span>

                {{if commentUrl}}
                    <span class="link-to-comment">
                        <a class="anchor tooltip" data-tooltip="<spring:message code="link.to.current.comment" text="default text"> </spring:message>" href="{{= commentUrl}}">#</a>
                    </span>
                {{/if}}
            </div>

            <div class="content expandable-content{{if expanded}}expand{{/if}}" itemprop="commentText">
                <div class="js-content-h">{{html textHtml}}</div>
            </div>

            <div class="comment-more hidden">
                <span class="comment-more-link">{{if expanded}}<spring:message code="roll.up" text="default text"> </spring:message>{{else}}<spring:message code="roll.down" text="default text"> </spring:message>{{/if}} <spring:message code="comment" text="default text"> </spring:message></span>
            </div>

            {{if workspace.isAuthorized}}
                <div class="answer">
                    <span class="answer-link"><spring:message code="answer.comment" text="default text"> </spring:message></span>
                </div>
            {{/if}}

            <div class="comment-likes"></div>

            {{if !restored}}
                {{if show30SecCloseButton}}
                    <div class="time-delete-comment">
                        {{if "CAMPAIGN" == objectType}}
                        <spring:message code="comment.response" text="default text"> </spring:message><br>
                        {{/if}}
                        <i class="icon-info icon-gray"></i> <spring:message code="you.can" text="default text"> </spring:message><br> <a href="javascript:void(0)"><spring:message code="delete.comment" text="default text"> </spring:message></a> <spring:message code="within" text="default text"> </spring:message> <span
                            class="comment-undeletable-counter"><b>{{if secondsLast}}{{= secondsLast}}{{else}}30{{/if}}</b> <spring:message code="seconds" text="default text"> </spring:message></span>.
                    </div>
                {{/if}}
            {{/if}}
        </div>
    {{/if}}
</script>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script id="campaign-search-block-template" type="text/x-jquery-template">
    <div class="wrap">
        <input id="query" name="query" class="project-list-search_val form-control" type="text" placeholder="{{= placeholder}}" autofocus>
    </div>
</script>

<script id="campaign-search-results-empty-container-template" type="text/x-jquery-template">
    <div class="project-list-not-found">
        <div><spring:message code="not.found" text="default text"> </spring:message></div>
        <div class="hl"><spring:message code="change.search.criteria" text="default text"> </spring:message></div>
    </div>
</script>

<script id="campaign-type-filter-template" type="text/x-jquery-template">
    <div class="project-list-filter_i">
        <div class="pln-select dropdown project-filter-select js-type-select">
            <div class="select-btn"><i class="icon-select"></i></div>
            <span class="select-cont">&nbsp;</span>
            <ul class="dropdown-menu">
                {{each CampaignUtils.statusTypes}}
                <li class="item {{if queryModel.status}}{{if queryModel.status==$index}} active{{/if}}
                                {{else $index=='ALL'}} active {{/if}}">
                    <a data-status="{{= $index=='ALL'?'':$index}}">{{if lang == "ru"}}{{= $value.name}}{{else}}{{= $value.engName}}{{/if}}</a>
                </li>
                {{/each}}
            </ul>
        </div>
    </div>
</script>

<script id="campaign-search-categories-mobile-template" type="text/x-jquery-template">
    <div class="m-project-category d-tl-none col-12">
        <div class="m-project-category_lbl">
            <spring:message code="projects.search.categories" text="default text"> </spring:message>
        </div>
        <div class="m-project-category_val">
            <div class="pln-select dropdown m-project-category-select js-category-select">
                <div class="select-btn"><i class="icon-select"></i></div>
                <span class="select-cont js-select-categories-text">&nbsp;</span>
                <ul class="dropdown-menu">
                    {{each campaignTags}}
                    <li class="item {{if queryModel.categories}}{{if queryModel.categories==$value.mnemonicName}} active{{/if}}
                                    {{else $value.mnemonicName=='ALL'}} active {{/if}}">
                        <a data-categories="{{= $value.mnemonicName=='ALL'?'':$value.mnemonicName}}">{{if lang == "ru"}}{{= $value.name}}{{else}}{{= $value.engName}}{{/if}}</a>
                    </li>
                    {{/each}}
                </ul>
            </div>
        </div>
    </div>
</script>

<script id="campaign-search-categories-desktop-template" type="text/x-jquery-template">
    <div class="m-col-ld-3 m-col-tl-4 col-12 m-col-ld-push-9 m-col-tl-push-8 d-tl-block d-none">
        <div class="project-category">
            <div class="project-category_head"><spring:message code="special.projects" text="default text"> </spring:message></div>
            <div class="js-by-specials"></div>
            <hr class="project-category_sep">
            <div class="project-category_head"><spring:message code="by.category" text="default text"> </spring:message></div>
            <div class="js-by-category"></div>
        </div>

        <div class="sidebar-banner-js"></div>
    </div>
</script>

<script id="campaign-search-results-container-template" type="text/x-jquery-template">
        <div class="wrap-row">
            <div class="js-mobile-categories"></div>
            <div class="js-desktop-categories"></div>

            <div class="m-col-ld-9 m-col-tl-8 col-12 m-col-ld-pull-3 m-col-tl-pull-4">
                <div class="js-project-list-filter"></div>
                <div class="project-list-block"></div>
            </div>
        </div>
</script>

<script id="nothing-found-template" type="text/x-jquery-template">
    <div class="n-empty">
        <div class="n-empty_cover">
            <div class="n-empty_img"></div>
        </div>
        <div class="n-empty_text">
            <spring:message code="not.found" text="default text"> </spring:message>
        </div>
    </div>
</script>

<script id="campaign-location-filter-template" type="text/x-jquery-template">
    <div class="project-list-filter_i">
        <select class="pln-select project-filter-select js-country_select" data-planeta-ui="country" data-only-exist="true" id="countryId" name="countryId"></select>
    </div>

    <div class="project-list-filter_i">
        {{if lang == "ru"}}
        <input class="form-control project-filter-input {{if !countryId}} hide{{/if}}" name="regionNameRus" data-planeta-ui="region" type="text" placeholder="<spring:message code="enter.region" text="default text"> </spring:message>" value="{{= regionNameRus}}">
        {{else}}
        <input class="form-control project-filter-input {{if !countryId}} hide{{/if}}" name="regionNameEng" data-planeta-ui="region" type="text" placeholder="<spring:message code="enter.region" text="default text"> </spring:message>" value="{{= regionNameEng}}">
        {{/if}}
        <input class="hide" id="regionId" name="regionId" value="{{= regionId}}">
    </div>

    <div class="project-list-filter_i">
        {{if lang == "ru"}}
        <input class="form-control project-filter-input {{if (!countryId || !regionId)}} hide{{/if}}" name="cityNameRus" data-planeta-ui="city" type="text" placeholder="<spring:message code="enter.city" text="default text"> </spring:message>" value="{{= cityNameRus}}">
        {{else}}
        <input class="form-control project-filter-input {{if (!countryId || !regionId)}} hide{{/if}}" name="cityNameEng" data-planeta-ui="city" type="text" placeholder="<spring:message code="enter.city" text="default text"> </spring:message>" value="{{= cityNameEng}}">
        {{/if}}
        <input class="hide" id="cityId" name="cityId" value="{{= cityId}}">
    </div>
</script>



<script id="campaign-top-menu-template" type="text/x-jquery-template">
    <div class="js-campaign-search-injection"></div>

    <div class="project-list-filter">
        {{tmpl '#campaign-type-filter-template'}}
        {{tmpl '#campaign-location-filter-template'}}
    </div>
</script>
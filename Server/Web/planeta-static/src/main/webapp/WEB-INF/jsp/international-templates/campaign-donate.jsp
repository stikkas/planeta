<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!--include css /css-generated/payment.css-->
<!--include css /css-generated/project.css-->
<!--===================== Donate details page ============================-->

<script id="donate-share-details-delivery-header-item-template" type="text/x-jquery-template">
    <div class="delivery-item">

        {{if subjectType == 'SHOP'}}
            {{if price}}
            <div class="pln-payment-box_delivery-type-price">{{= price}}
                <span class="b-rub">Р</span>
            </div>
            {{else}}
            <div class="pln-payment-box_delivery-type-price delivery-type-price_free"><spring:message code="campaign-donate.jsp.propertie.1" text="default text"> </spring:message></div>
            {{/if}}
        {{/if}}
        <span class="radiobox{{if isSelected}} active{{/if}}"></span>
        <span class="radiobox-label">{{= name}} {{if price && (subjectType == 'SHARE')}}({{= price}}<span class="b-rub"> Р</span>){{/if}}</span>
    </div>
</script>

<script id="delivery-service-public-note-modal-template" type="text/x-jquery-template">
    <div class="modal-header">
        <a class="close" data-dismiss="modal" href="javascript:void(0);">×</a>

        <div class="modal-title"><spring:message code="campaign-donate.jsp.propertie.2" text="default text"> </spring:message></div>
        <div class="modal-header-shadow small"></div>
    </div>

    <div class="modal-body">
        {{html description}}
    </div>

    <div class="modal-footer">
        <div class="modal-footer-cont">
            <button type="reset" class="btn" data-dismiss="modal"><spring:message code="campaign-donate.jsp.propertie.3" text="default text"> </spring:message></button>
        </div>
    </div>

</script>

<script id="donate-share-details-delivery-section-address-template" type="text/x-jquery-template">
    {{if deliveryType == "CUSTOMER_PICKUP" }}
    <div class="pln-payment-box_field-row error">
        <div class="pln-payment-box_field-label"><spring:message code="campaign-donate.jsp.propertie.4" text="default text"> </spring:message></div>
        <div class="pln-payment-box_field-value">
            <div class="pln-payment-box_field-text">
                {{= address.city}}, {{= address.street}}
                <br>
                {{= address.phone}}
            </div>
        </div>
    </div>
    {{else deliveryType == "DELIVERY"}}
        <div class="pln-payment-box_field-row">
            <div class="pln-payment-box_field-label"><spring:message code="campaign-donate.jsp.propertie.5" text="default text"> </spring:message></div>
            <div class="pln-payment-box_field-value">
                <div class="input-field">
                    <!--brutal hack to show error message correctly-->
                    <input type="hidden" name="street">

                    <input class="pln-payment-box_field-input" type="text"
                           name="customerContacts.street"
                           placeholder="<spring:message code="campaign-donate.jsp.propertie.6" text="default text"> </spring:message>
                           value="{{= customerContacts.street}}">
                </div>
            </div>
        </div>

        <div class="pln-payment-box_field-row">
            <div class="pln-payment-box_field-label"><spring:message code="campaign-donate.jsp.propertie.7" text="default text"> </spring:message></div>
            <div class="pln-payment-box_field-value">
                <div class="input-field">
                    <!--brutal hack to show error message correctly-->
                    <input type="hidden" name="zipCode">

                    <input class="pln-payment-box_field-input" type="text"
                           name="customerContacts.zipCode"
                           value="{{= customerContacts.zipCode}}"
                           placeholder="<spring:message code="campaign-donate.jsp.propertie.8" text="default text"> </spring:message>">
                </div>
            </div>
        </div>
    {{/if}}
    <div class="pln-payment-box_field-row">
        <div class="pln-payment-box_field-label"><spring:message code="campaign-donate.jsp.propertie.9" text="default text"> </spring:message></div>
        <div class="pln-payment-box_field-value">
            <div class="input-field">
                <input class="pln-payment-box_field-input" type="text"
                       name="customerName"
                       value="{{= customerName}}"
                       placeholder="<spring:message code="campaign-donate.jsp.propertie.10" text="default text"> </spring:message>">
            </div>
        </div>
    </div>

    <div class="pln-payment-box_field-row">
        <div class="pln-payment-box_field-label"><spring:message code="campaign-donate.jsp.propertie.11" text="default text"> </spring:message></div>
        <div class="pln-payment-box_field-value">
            <div class="input-field">
                <input class="pln-payment-box_field-input" type="text"
                       name="customerContacts.phone"
                       value="{{= customerContacts.phone}}"
                       placeholder="<spring:message code="campaign-donate.jsp.propertie.12" text="default text"> </spring:message>">
            </div>
        </div>
    </div>
    {{if deliveryType == "DELIVERY" && (orderType == 'SHARE') && !!$data.description}}
        <div class="pln-payment-box_field-row pln-payment-box_field-pdg">
            <span class="link-dotted" data-event-click="openPublicNote"><spring:message code="campaign-donate.jsp.propertie.13" text="default text"> </spring:message></span>
        </div>
    {{/if}}
</script>
<script id="has-email-template" type="text/x-jquery-template">
    {{if !hasEmail}}
    <div class="pln-payment-box">
        <div class="pln-payment-box_head">
            <spring:message code="campaign-donate.jsp.propertie.32" text="default text"> </spring:message>
        </div>
        <div class="pln-payment-box_field-row">
            <div class="pln-payment-box_field-label">
                <spring:message code="campaign-donate.jsp.propertie.33" text="default text"> </spring:message>
            </div>
            <div class="pln-payment-box_field-value js-anchr-email-block">
                <div class="input-field dropdown input-warning-dropdown">
                    <input class="pln-payment-box_field-input"
                           type="email" required="true"
                           name="email" value="{{= email}}"
                           placeholder="me@example.com">

                    <div class="dropdown-popup">
                        <div class="dropdown-popup-caret"></div>
                        <div class="dropdown-popup-body">
                            <div id="hint" class="warning-message-error" style="display: block;">
                                <spring:message code="campaign-donate.jsp.propertie.34" text="default text"> </spring:message>
                                <br>
                                <a class="signup-link" href="javascript:void(0)">
                                    <spring:message code="campaign-donate.jsp.propertie.14" text="default text"> </spring:message>
                                </a>
                                <br>
                                <sub>
                                    <spring:message code="campaign-donate.jsp.propertie.35" text="default text"> </spring:message>
                                    <a href="mailto:support@planeta.ru">support@planeta.ru</a>
                                </sub>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{/if}}
</script>

<script id="delivery-payment-template" type="text/x-jquery-template">
    <div class="cont-header">
        <div class="cont-header_row">
            <div class="wrap">
                <div class="col-12">
                    <div class="cont-header_title">
                        <spring:message code="campaign-donate.jsp.propertie.31" text="default text"> </spring:message>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wrap">
        <div class="project-view-wrap cf">
            <div class="col-9">
                {{tmpl '#has-email-template'}}
                <div class="js-project-payment-details-container"></div>
                {{tmpl '#pln-payment-terms'}}
                <div class="pln-payment-action cf">
                    <button class="flat-btn pull-right js-purchase-order" href="javascript:void(0);"><spring:message code="campaign-donate.jsp.propertie.16" text="default text"> </spring:message></button>
                    <div class="js-payment-total"></div>
                </div>
            </div>
            <div class="col-3">
                <div class="pln-payment-box">
                    <div class="pln-payment-side">

                        <div class="pln-payment-side_head"><spring:message code="campaign-donate.jsp.propertie.17" text="default text"> </spring:message></div>

                        <div class="pln-payment-side_cont">
                            <spring:message code="campaign-donate.jsp.propertie.36" text="default text"> </spring:message>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>

<script id="donate-campaign-aliashead" type="text/x-jquery-template">
    <div class="project-donate_header">
        <div class="project-donate_title">
            <spring:message code="campaign-donate.jsp.propertie.37" text="default text"/>
        </div>

        <div class="project-donate_name">
            <a href="/campaigns/{{= campaign.webCampaignAlias}}">{{= campaign.name}}</a>
        </div>

        <div class="project-donate_author">
            Автор <a href="/{{= ProfileUtils.getUserLink(group.profileId, group.alias) }}">{{= group.displayName}}</a>
        </div>
    </div>
</script>

<script id="donate-campaign-investor-reglocation" type="text/x-jquery-template">
    <div class="pln-payment-box_field-row">
        <div class="pln-payment-box_field-label">
            <spring:message code="campaign-donate.jsp.propertie.58" text="default text"> </spring:message>
        </div>
        <div class="pln-payment-box_field-value">
            <select class="pln-select pln-select-xlg" data-planeta-ui="country" name="regCountryId" value="{{= investInfo.regCountryId}}">
            </select>
        </div>
    </div>

    <div class="pln-payment-box_field-row">
        <div class="pln-payment-box_field-label">
            <spring:message code="payment.jsp.propertie.38" text="default text"/>
            <div class="ppb_fl_help tooltip" data-tooltip="<spring:message code='payment.jsp.propertie.42' text='default text'/>"></div>
        </div>
        <div class="pln-payment-box_field-value">
            <input class="pln-payment-box_field-input" type="text" name="regRegion" value="{{= investInfo.regRegion}}">
        </div>
    </div>

    <div class="pln-payment-box_field-row">
        <div class="pln-payment-box_field-label">
            <spring:message code="payment.jsp.propertie.39" text="default text"/>
            <div class="ppb_fl_help tooltip" data-tooltip="<spring:message code='payment.jsp.propertie.43' text='default text'/>"></div>
        </div>
        <div class="pln-payment-box_field-value">
            <input class="pln-payment-box_field-input" type="text" name="regLocation" value="{{= investInfo.regLocation}}">
        </div>
    </div>

    <div class="pln-payment-box_field-row">
        <div class="pln-payment-box_field-label">
            <spring:message code="payment.jsp.propertie.40" text="default text"/>
            <div class="ppb_fl_help tooltip" data-tooltip="<spring:message code='payment.jsp.propertie.62' text='default text'/>"></div>
        </div>
        <div class="pln-payment-box_field-value">
            <input class="pln-payment-box_field-input" type="text" name="regAddress" value="{{= investInfo.regAddress}}">
        </div>
    </div>

    <div class="pln-payment-box_field-row">
        <div class="pln-payment-box_field-label"><spring:message code="payment.jsp.propertie.41" text="default text"/></div>
        <div class="pln-payment-box_field-value">
            <input class="pln-payment-box_field-input js-index" type="text" name="regIndex" value="{{= investInfo.regIndex}}">
        </div>
    </div>
</script>
<script id="donate-campaign-investor-livelocation" type="text/x-jquery-template">
    <div class="pln-payment-box_field-row">
        <div class="pln-payment-box_field-label"><spring:message code="campaign-donate.jsp.propertie.58" text="default text"> </spring:message></div>
        <div class="pln-payment-box_field-value">
            <select class="pln-select pln-select-xlg" data-planeta-ui="country" name="liveCountryId" value="{{= investInfo.liveCountryId}}">
            </select>
        </div>
    </div>

    <div class="pln-payment-box_field-row">
        <div class="pln-payment-box_field-label">
            <spring:message code="payment.jsp.propertie.38" text="default text"/>
            <div class="ppb_fl_help tooltip" data-tooltip="<spring:message code='payment.jsp.propertie.42' text='default text'/>"></div>
        </div>
        <div class="pln-payment-box_field-value">
            <input class="pln-payment-box_field-input" type="text" name="liveRegion" value="{{= investInfo.liveRegion}}">
        </div>
    </div>

    <div class="pln-payment-box_field-row">
        <div class="pln-payment-box_field-label">
            <spring:message code="payment.jsp.propertie.39" text="default text"/>
            <div class="ppb_fl_help tooltip" data-tooltip="<spring:message code='payment.jsp.propertie.43' text='default text'/>"></div>
        </div>
        <div class="pln-payment-box_field-value">
            <input class="pln-payment-box_field-input" type="text" name="liveLocation" value="{{= investInfo.liveLocation}}">
        </div>
    </div>

    <div class="pln-payment-box_field-row">
        <div class="pln-payment-box_field-label">
            <spring:message code="payment.jsp.propertie.40" text="default text"/>
            <div class="ppb_fl_help tooltip" data-tooltip="Название улицы, номер дома, корпуса, квартиры"></div>
        </div>
        <div class="pln-payment-box_field-value">
            <input class="pln-payment-box_field-input" type="text" name="liveAddress" value="{{= investInfo.liveAddress}}">
        </div>
    </div>

    <div class="pln-payment-box_field-row">
        <div class="pln-payment-box_field-label"><spring:message code="payment.jsp.propertie.41" text="default text"/></div>
        <div class="pln-payment-box_field-value">
            <input class="pln-payment-box_field-input js-index" type="text" name="liveIndex" value="{{= investInfo.liveIndex}}">
        </div>
    </div>
</script>
<script id="donate-invest-fio-template" type="text/x-jquery-template">
	<div class="pln-payment-box_field-row">
		<div class="pln-payment-box_field-label"><spring:message code="payment.jsp.propertie.31" text="default text"/></div>
		<div class="pln-payment-box_field-value">
			<input class="pln-payment-box_field-input" type="text" name="lastName" value="{{= investInfo.lastName}}">
		</div>
	</div>

	<div class="pln-payment-box_field-row">
		<div class="pln-payment-box_field-label"><spring:message code="payment.jsp.propertie.32" text="default text"/></div>
		<div class="pln-payment-box_field-value">
			<input class="pln-payment-box_field-input" type="text" name="firstName" value="{{= investInfo.firstName}}">
		</div>
	</div>

	<div class="pln-payment-box_field-row">
		<div class="pln-payment-box_field-label"><spring:message code="payment.jsp.propertie.33" text="default text"/></div>
		<div class="pln-payment-box_field-value">
			<input class="pln-payment-box_field-input" type="text" name="middleName" value="{{= investInfo.middleName}}">
		</div>
	</div>
</script>

<script id="donate-invest-ul-template" type="text/x-jquery-template">
    <div class="cont-header">
        <div class="cont-header_row">
            <div class="wrap">
                <div class="col-12">
                    <div class="cont-header_title">
                        <spring:message code="campaign-donate.jsp.propertie.37" text="default text"> </spring:message>
                        <a href="/campaigns/{{= campaign.webCampaignAlias }}">«{{= campaign.name}}»</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wrap">
        <div class="project-view-wrap cf">
            <div class="col-9">
                {{tmpl '#has-email-template'}}
                <div class="pln-payment-box pln-payment-box__bottom-0">
                    <div class="pln-payment-face active">
                        <div class="js-face-switch" style="display:inline">
                            <div class="pln-payment-face_lbl pln-payment-face_one"><spring:message code="payment.jsp.propertie.25" text="default text"/></div>
                            <div class="pln-payment-face_switch"></div>
                        </div>
                        <div class="pln-payment-face_lbl pln-payment-face_two"><spring:message code="payment.jsp.propertie.26" text="default text"/></div>
                    </div>
                    <div class="pln-payment-box_head"><spring:message code="payment.jsp.propertie.27" text="default text"/></div>
                    <div class="pln-payment-box_field-group">
                        {{tmpl '#donate-invest-ul-resident-rf'}}
                        {{tmpl '#donate-invest-pce'}}
                        <div class="pln-payment-box_field-row">
                            <div class="pln-payment-box_field-label"><spring:message code="payment.jsp.propertie.52" text="default text"/></div>
                            <div class="pln-payment-box_field-value">
                                <textarea rows="2" class="pln-payment-box_field-input" name="orgName">{{= investInfo.orgName}}</textarea>
                            </div>
                        </div>

                        <div class="pln-payment-box_field-row">
                            <div class="pln-payment-box_field-label"><spring:message code="payment.jsp.propertie.53" text="default text"/></div>
                            <div class="pln-payment-box_field-value">
                                <textarea rows="2" class="pln-payment-box_field-input" name="orgBrand">{{= investInfo.orgBrand}}</textarea>
                            </div>
                        </div>

                        <div class="pln-payment-box_field-row">
                            <div class="pln-payment-box_field-label"><spring:message code="payment.jsp.propertie.54" text="default text"/></div>
                            <div class="pln-payment-box_field-value">
                                <input class="pln-payment-box_field-input pln-payment-box_field-input__date js-date" placeholder="12.12.1986" type="text"  data-planeta-ui="dateSelect" name="birthDate" value="{{= investInfo.birthDate}}">
                            </div>
                        </div>

                        <div class="pln-payment-box_field-row">
                            <div class="pln-payment-box_field-label">
                                <spring:message code="inn" text="default text"/>
                                <div class="ppb_fl_help tooltip" data-tooltip="<spring:message code='payment.jsp.propertie.61' text='default text'/>"></div>
                            </div>
                            <div class="pln-payment-box_field-value">
                                <input class="pln-payment-box_field-input js-inn" type="text" name="inn" value="{{= investInfo.inn}}">
                            </div>
                        </div>

                        <div class="pln-payment-box_field-row">
                            <div class="pln-payment-box_field-label"><spring:message code="payment.jsp.propertie.55" text="default text"/></div>
                            <div class="pln-payment-box_field-value">
                                <input class="pln-payment-box_field-input" type="text" name="webSite" value="{{= investInfo.webSite}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pln-payment-box pln-payment-box__bottom-0">
                    <div class="pln-payment-box_head"><spring:message code="payment.jsp.propertie.58" text="default text"/></div>
                    <div class="pln-payment-box_field-group">
                        {{tmpl '#donate-campaign-investor-reglocation'}}
                        <div class="pln-payment-box pln-payment-box__bottom-0">
                            <div class="pln-payment-box_head"><spring:message code="payment.jsp.propertie.57" text="default text"/></div>
                        </div>
                        <div class="pln-payment-box_field-row pln-payment-box_field-pdg">
                            <div class="check-row js-address-toggle">
                                <span class="checkbox active"></span>
                                <span class="checkbox-label"><b><spring:message code="payment.jsp.propertie.56" text="default text"/></b></span>
                            </div>
                        </div> 
                    </div>
                </div>
                <div class="pln-payment-box pln-payment-box__bottom-0 hide" id="js-address-block">
                    <div class="pln-payment-box_field-group">
                        {{tmpl '#donate-campaign-investor-livelocation'}}
                    </div>
                </div>
                <div class="pln-payment-box">
                    <div class="pln-payment-box_head"><spring:message code="payment.jsp.propertie.60" text="default text"/></div>
                    <div class="pln-payment-box_field-group">
						{{tmpl '#donate-invest-fio-template'}}

                        <div class="pln-payment-box_field-row">
                            <div class="pln-payment-box_field-label"><spring:message code="payment.jsp.propertie.59" text="default text"/></div>
                            <div class="pln-payment-box_field-value">
                                <input class="pln-payment-box_field-input" type="text" name="bossPosition"  value="{{= investInfo.bossPosition}}">
                            </div>
                        </div>
                    </div>
                </div>
                {{tmpl '#pln-payment-terms'}}
                {{tmpl '#donate-action-buttons'}}
            </div>
			{{tmpl '#donate-invest-right-sidebar'}}
        </div>
    </div>
</script>

<script id="pln-payment-terms" type="text/x-jquery-template">
    <div class="pln-payment-terms">
    <spring:message code="campaign-donate.jsp.propertie.43" text="default text"/>{{if shareStatus == 'INVESTING'}}<spring:message code="campaign-donate.jsp.apply.request" text="default text"/>{{else shareStatus == 'INVESTING_WITHOUT_MODERATION'}}<spring:message code="campaign-donate.jsp.get.bill" text="default text"/>{{else}}<spring:message code="campaign-donate.jsp.propertie.22" text="default text"/>{{/if}}<spring:message code="campaign-donate.jsp.propertie.43.1" text="default text"/>
        <a href="https://{{= workspace.serviceUrls.mainHost}}/welcome/projects-agreement.html"><spring:message code="payment.jsp.propertie.5" text="default text"/></a>
        {{if ProfileUtils.isInvestingProject(shareStatus)}}
            <spring:message code="payment.jsp.propertie.5.1" text="default text"/>
        {{/if}}
    </div>
</script>

<script id="donate-action-buttons" type="text/x-jquery-template">
    <div class="pln-payment-action cf">
        <a class="flat-btn flat-btn-default pull-left js-return-to-share-selection"
           href="javascript:void(0);"><spring:message code="campaign-donate.jsp.propertie.21" text="default text"/></a>
        <button class="flat-btn pull-right js-purchase-order"
           href="javascript:void(0);">
            {{if shareStatus == 'INVESTING'}}<spring:message code="campaign-donate.jsp.apply.request" text="default text"/>
            {{else shareStatus == 'INVESTING_WITHOUT_MODERATION'}}<spring:message code="campaign-donate.jsp.get.bill" text="default text"/>
            {{else}}<spring:message code="campaign-donate.jsp.propertie.22" text="default text"/>{{/if}}</button>
        <div class="js-payment-total"></div>
    </div>
</script>

<script id="donate-invest-right-sidebar" type="text/x-jquery-template">
	<div class="col-3">
        <div class="pln-payment-box">
            <div class="pln-payment-reward">
				<div class="pln-payment-reward_head">Вы инвестируете в проект сумму</div>
                <div class="pln-payment-reward_sum">{{= StringUtils.humanNumber(donateAmount)}}&nbsp;
                    <span class="b-rub">Р</span>
                </div>
            </div>
        </div>

		<div class="pln-payment-trust">
			<div class="pln-payment-trust_ico"><i></i></div>
			<div class="pln-payment-trust_cont"><spring:message code="payment.jsp.propertie.51" text="default text"/></div>
		</div>

        <div class="pln-payment-loyal">
            <div class="pln-payment-loyal_text">
                <spring:message code="campaign-donate.invest.faq.1" text="default text"/>
                    <a href="https://{{= workspace.serviceUrls.mainHost}}/faq"><spring:message code="campaign-donate.invest.faq.2" text="default text"/></a>
            </div>
        </div>
        
		<div class="pln-payment-loyal pln-payment-loyal__support">
            {{tmpl({phone: '+7 495 181-05-05', email: 'payment.support@planeta.ru'}) '#order-user-support-template'}}
		</div>
	</div>
</script>

<script id="donate-invest-fl-resident-rf" type="text/x-jquery-template">
    <div class="pln-payment-box_field-row pln-payment-box_field-pdg">
       <b>Внимание!</b> Будьте внимательны при заполнении: все указанные данные должны соответствовать информации, содержащейся в документе, удостоверяющем личность. Это необходимо для того, чтобы подтвердить и однозначно идентифицировать вас в качестве инвестора и сформировать договор.
    </div>
</script>

<script id="donate-invest-ul-resident-rf" type="text/x-jquery-template">
    <div class="pln-payment-box_field-row pln-payment-box_field-pdg">
        <b>Внимание!</b> Будьте внимательны при заполнении: все указанные данные должны соответствовать документам организации. Это необходимо для того, чтобы подтвердить и однозначно идентифицировать организацию в качестве инвестора и сформировать договор.
    </div>
</script>

<script id="donate-invest-pce" type="text/x-jquery-template">
    <div class="pln-payment-box_field-row">
        <div class="pln-payment-box_field-label">
            <spring:message code="payment.jsp.propertie.28" text="default text"/>
            <div class="ppb_fl_help tooltip" data-tooltip="<spring:message code='payment.jsp.propertie.63' text='default text'/>"></div>
        </div>
        <div class="pln-payment-box_field-value">
            <input class="pln-payment-box_field-input js-phone" placeholder="+7 ___ ___ __ __" type="text" name="phoneNumber" value="{{= investInfo.phoneNumber}}">
            {{if smsVerificationEnabled}}<div class="pln-payment-box_field-input-btn"><button class="js-send-sms btn btn-primary"><spring:message code="payment.jsp.propertie.30" text="default text"/></button></div>{{/if}}
        </div>
    </div>
    <div class="pln-payment-box_field-row js-confirmation-code hidden">
        <div class="pln-payment-box_field-label">
            <spring:message code="payment.jsp.propertie.29" text="default text"/>
            <div class="ppb_fl_help tooltip" data-tooltip="Код подтверждения из SMS"></div>
        </div>
        <div class="pln-payment-box_field-value">
            <input class="pln-payment-box_field-input js-sms-code" type="text" name="confirmationCode">
        </div>
    </div>

    <div class="pln-payment-box_field-row hide">
        <div class="pln-payment-box_field-label">
            Код из СМС
            <div class="ppb_fl_help tooltip" data-tooltip="Код из смс"></div>
        </div>
        <div class="pln-payment-box_field-value">
            <input class="pln-payment-box_field-input" type="text" name="smsCode" value="{{=investInfo.smsCode}}">
        </div>
    </div>
</script>

<script id="donate-invest-fiz-template" type="text/x-jquery-template">
     <div class="cont-header">
        <div class="cont-header_row">
            <div class="wrap">
                <div class="col-12">
                    <div class="cont-header_title">
                        <spring:message code="campaign-donate.jsp.propertie.37" text="default text"> </spring:message>
                        <a href="/campaigns/{{= campaign.webCampaignAlias }}">«{{= campaign.name}}»</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wrap">
        <div class="project-view-wrap cf">
            <div class="col-9">
                {{tmpl '#has-email-template'}}
                <div class="pln-payment-box pln-payment-box__bottom-0">
                <div class="pln-payment-face">
                    <div class="pln-payment-face_lbl pln-payment-face_one"><spring:message code="payment.jsp.propertie.25" text="default text"/></div>
                    <div class="js-face-switch" style="display:inline">
                        <div class="pln-payment-face_switch"></div>
                        <div class="pln-payment-face_lbl pln-payment-face_two"><spring:message code="payment.jsp.propertie.26" text="default text"/></div>
                    </div>
                </div>
                <div class="pln-payment-box_head"><spring:message code="payment.jsp.propertie.27" text="default text"/></div>

                    <div class="pln-payment-box_field-group">
                        {{tmpl '#donate-invest-fl-resident-rf'}}
                        {{tmpl '#donate-invest-pce'}}
						{{tmpl '#donate-invest-fio-template'}}

                        <div class="pln-payment-box_field-row">
                            <div class="pln-payment-box_field-label"><spring:message code="payment.jsp.propertie.34" text="default text"/></div>
                            <div class="pln-payment-box_field-value">
                                <div class="pln-payment-box_ui-control">
                                    <div class="radio-row">
                                        <span class="radiobox" data-value="MALE"></span>
                                        <span class="radiobox-label"><spring:message code="user-settings.jsp.propertie.19" text="default text"/></span>
                                    </div>
                                    <div class="radio-row">
                                        <span class="radiobox" data-value="FEMALE"></span>
                                        <span class="radiobox-label"><spring:message code="user-settings.jsp.propertie.20" text="default text"/></span>
                                    </div>
                                </div>
                                <div class="pln-payment-box_field-row pln-payment-box_field-row_hidden">
                                    <input name="gender" type="hidden" value="{{= investInfo.gender}}"/>
                                    <div class="pln-payment-box_field-text-error">
                                        <spring:message code="payment.jsp.propertie.23" text="default text"> </spring:message>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="pln-payment-box_field-row">
                            <div class="pln-payment-box_field-label"><spring:message code="payment.jsp.propertie.35" text="default text"/></div>
                            <div class="pln-payment-box_field-value">
                                <input class="pln-payment-box_field-input pln-payment-box_field-input__date js-date" placeholder="12.12.1986" type="text"  data-planeta-ui="dateSelect" name="birthDate" value="{{= investInfo.birthDate}}">
                            </div>
                        </div>

                        <div class="pln-payment-box_field-row">
                            <div class="pln-payment-box_field-label"><spring:message code="payment.jsp.propertie.36" text="default text"/></div>
                            <div class="pln-payment-box_field-value">
                                <input class="pln-payment-box_field-input" type="text" name="birthPlace" value="{{= investInfo.birthPlace}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pln-payment-box pln-payment-box__bottom-0">
                    <div class="pln-payment-box_head"><spring:message code="payment.jsp.propertie.37" text="default text"/></div>
                    <div class="pln-payment-box_field-group">
                       {{tmpl '#donate-campaign-investor-reglocation'}}

                        <div class="pln-payment-box pln-payment-box__bottom-0">
                            <div class="pln-payment-box_head"><spring:message code="payment.jsp.propertie.44" text="default text"/></div>
                        </div>
                        <div class="pln-payment-box_field-row pln-payment-box_field-pdg">
                            <div class="check-row js-address-toggle">
                                <span class="checkbox active"></span>
                                <span class="checkbox-label"><b><spring:message code="payment.jsp.propertie.45" text="default text"/></b></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pln-payment-box pln-payment-box__bottom-0 hide" id="js-address-block">
                    <div class="pln-payment-box_field-group">
                        {{tmpl '#donate-campaign-investor-livelocation'}}
                    </div>
                </div>
                <div class="pln-payment-box">
                    <div class="pln-payment-box_head"><spring:message code="payment.jsp.propertie.46" text="default text"/></div>
                    <div class="pln-payment-box_field-group">
                        <div class="pln-payment-box_field-row">
                            <div class="pln-payment-box_field-label">
                                <spring:message code="passport.number" text="default text"/>
                                <div class="ppb_fl_help tooltip" data-tooltip="<spring:message code='payment.jsp.propertie.47' text='default text'/>"></div>
                            </div>
                            <div class="pln-payment-box_field-value">
                                <input class="pln-payment-box_field-input js-passport-number" placeholder="3301 123456" type="text" name="passportNumber" value="{{= investInfo.passportNumber}}">
                            </div>
                        </div>

                        <div class="pln-payment-box_field-row">
                            <div class="pln-payment-box_field-label"><spring:message code="payment.jsp.propertie.48" text="default text"/></div>
                            <div class="pln-payment-box_field-value">
                                <input class="pln-payment-box_field-input" type="text" name="passportIssuer" value="{{= investInfo.passportIssuer}}">
                            </div>
                        </div>

                        <div class="pln-payment-box_field-row">
                            <div class="pln-payment-box_field-label"><spring:message code="payment.jsp.propertie.49" text="default text"/></div>
                            <div class="pln-payment-box_field-value">
                                <input class="pln-payment-box_field-input pln-payment-box_field-input__date js-date" placeholder="12.12.1986" type="text" data-planeta-ui="dateSelect" name="passportIssueDate" value="{{= investInfo.passportIssueDate}}">
                            </div>
                        </div>

                        <div class="pln-payment-box_field-row">
                            <div class="pln-payment-box_field-label"><spring:message code="payment.jsp.propertie.50" text="default text"/></div>
                            <div class="pln-payment-box_field-value">
                                <input class="pln-payment-box_field-input" type="text" name="passportIssuerCode" value="{{= investInfo.passportIssuerCode}}">
                            </div>
                        </div>
                    </div>
                </div>
                {{if methsCount > 1}}
                <div class="js-project-payment-details-container"></div>
                {{/if}}
                {{tmpl '#pln-payment-terms'}}
                {{tmpl '#donate-action-buttons'}}
            </div>
			{{tmpl '#donate-invest-right-sidebar'}}
        </div>
    </div>
</script>

<script id="share-delivery-page-template" type="text/x-jquery-template">
    {{if doRequireDelivery}}
    <div class="pln-payment-box">
        <div class="pln-payment-box_head">
            <spring:message code="payment.jsp.propertie.24" text="default text"> </spring:message>
        </div>

        <div class="pln-payment-box_field-row js-project-donate-condition-container">
            <div class="pln-payment-box_delivery-type {{if $data.orderType != 'SHARE'}} pln-payment-box_delivery-type-vert{{/if}}"></div>
        </div>
        <div class="pln-payment-box_field-row"></div>
        <div class="pln-payment-box_field-row hide js-error-not-exists-delivery">
            <div class="pln-payment-box_field-text-error">
                К сожалению, в этот населенный пункт доставка не осуществляется.
            </div>
        </div>
        <div class="pln-payment-box_field-row pln-payment-box_field-row_hidden">
            <input name="deliveryType" type="hidden">
            <div class="pln-payment-box_field-text-error">
                Не выбран способ доставки
            </div>
        </div>

        <div class="pln-payment-box_field-group">
            {{if deliveryType && deliveryType != "CUSTOMER_PICKUP"}}
            <div class="pln-payment-box_field-row">
                <div class="pln-payment-box_field-label"><spring:message code="campaign-donate.jsp.propertie.58" text="default text"> </spring:message></div>
                <div class="pln-payment-box_field-value">
                    <select class="pln-payment-box_field-select"
                    {{if customerContacts.disableCountry}}disabled{{/if}}
                    data-planeta-ui="country"
                    data-selected-country-id="{{= customerContacts.countryId}}"
                    name="customerContacts.countryId">
                    </select>
                </div>
            </div>

            <div class="pln-payment-box_field-row">
                <div class="pln-payment-box_field-label"><spring:message code="campaign-donate.jsp.propertie.59" text="default text"> </spring:message></div>
                <div class="pln-payment-box_field-value">
                    <div class="input-field">
                        <!--brutal hack to show error message correctly-->
                        <input type="hidden" name="city">

                        <input class="pln-payment-box_field-input" type="text"
                               name="customerContacts.city"
                               value="{{= customerContacts.city}}"
                        {{if customerContacts.disableCity}}disabled{{/if}}
                        placeholder="<spring:message code='campaign-donate.jsp.propertie.60' text='default text'> </spring:message>">
                        <input type="hidden"
                               name="customerContacts.cityId"
                               class="js-anchr-address-cityId" value="{{= customerContacts.cityId}}"/>
                    </div>
                </div>
            </div>
            {{/if}}
        </div>
        <div class="js-anchor-delivery-services"></div>
    </div>
    {{/if}}
</script>

<script id="donate-share-details-template" type="text/x-jquery-template">
    <div class="cont-header">
        <div class="cont-header_row">
            <div class="wrap">
                <div class="col-12">
                    <div class="cont-header_title">
                        <spring:message code="campaign-donate.jsp.propertie.37" text="default text"> </spring:message>
                        <a href="/campaigns/{{= campaign.webCampaignAlias }}">«{{= campaign.name}}»</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wrap">
        <div class="project-view-wrap payment-page-wrap   cf">
            <div class="col-9">
                {{tmpl '#has-email-template'}}
                {{tmpl '#share-delivery-page-template'}}

                {{if questionToBuyer}}
                <div class="pln-payment-box">
                    <div class="pln-payment-box_head">
                        <spring:message code="campaign-donate.jsp.propertie.42" text="default text"> </spring:message>
                    </div>

                    <div class="pln-payment-box_field-group">

                        <div class="pln-payment-box_field-row pln-payment-box_author-quest">
                            <div class="pln-payment-box_field-label js-reply-label">{{if questionToBuyerHtml}}{{html questionToBuyerHtml}}{{else}}{{= questionToBuyer}}{{/if}}
                            </div>
                            <div class="pln-payment-box_field-value">
                                <div class="input-field">
                                    <textarea class="pln-payment-box_field-input" placeholder="<spring:message code="campaign-donate.jsp.propertie.19" text="default text"> </spring:message>" name="reply" rows="2">{{= reply}}</textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                {{/if}}

                <div class="js-project-payment-details-container"></div>
                {{tmpl '#pln-payment-terms'}}
                {{tmpl '#donate-action-buttons'}}
            </div>
            <div class="col-3 js-right-sidebar">
                <div class="pln-payment-box">
                    <div class="pln-payment-reward">

                        <div class="pln-payment-reward_head"><spring:message code="campaign-donate.jsp.propertie.23" text="default text"> </spring:message></div>

                        <div class="pln-payment-reward_sum">{{= StringUtils.humanNumber(donateAmount)}}&nbsp;
                            <span class="b-rub">Р</span>
                        </div>

                        <div class="pln-payment-reward_name">
                            {{if quantity > 1}}<b>{{= quantity}} x</b> {{/if}}{{html nameHtml}}
                        </div>

                        <div class="pln-payment-reward_change">
                            <a class="flat-btn flat-btn-default js-return-to-share-selection"
                               href="javascript:void(0);"><spring:message code="campaign-donate.jsp.propertie.24" text="default text"> </spring:message></a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</script>

<script id="order-user-callback-template" type="text/x-jquery-template">
    <span class="js-callback-state2 hidden">
        <div class="pln-payment-loyal_text">
            <spring:message code="campaign-donate.jsp.propertie.44" text="default text"> </spring:message>
        </div>
    </span>
    <span class="js-callback-state1">
        <div class="pln-payment-loyal_head">
            <spring:message code="campaign-donate.jsp.propertie.45" text="default text"> </spring:message>
        </div>
        <div class="pln-payment-loyal_text">
            <spring:message code="campaign-donate.jsp.propertie.46" text="default text"> </spring:message>
        </div>
        <div class="pln-payment-loyal_link-wrap pln-dropdown">
            <span class="pln-payment-loyal_link pln-d-switch"><spring:message code="campaign-donate.jsp.propertie.25" text="default text"> </spring:message></span>
            <div class="pln-d-popup pln-d-popup_right pln-payment-loyal_popup">
                <div class="pln-d-popup-tail"></div>
                <div class="pln-payment-loyal_popup-cont">
                    <div class="pln-payment-loyal_form-lbl"><spring:message code="campaign-donate.jsp.propertie.26" text="default text"> </spring:message></div>
                    <div class="pln-payment-loyal_form-val">
                        <input class="js-phone form-control control-lg" type="text" placeholder="+7 (123) 456-78-90">
                    </div>
                    <div class="pln-payment-loyal_form-btn">
                        <span class="js-callback btn btn-primary btn-block btn-lg disabled"><spring:message code="campaign-donate.jsp.propertie.27" text="default text"> </spring:message></span>
                    </div>
                </div>
            </div>
        </div>
    </span>
</script>

<script id="order-user-support-template" type="text/x-jquery-template">
    <div class="pln-payment-loyal_head">
        <spring:message code="campaign-donate.jsp.propertie.47" text="default text"> </spring:message>
    </div>
    <div class="pln-payment-loyal_text">
        <spring:message code="campaign-donate.jsp.propertie.48" text="default text"> </spring:message> <b>{{= phone}}</b> <spring:message code="time" text="default text"> </spring:message> <a href="mailto:payment.support@planeta.ru"><b>{{= email}}</b></a>
    </div>
</script>


<script id="donate-share-template" type="text/x-jquery-template">
    {{tmpl '#donate-campaign-aliashead'}}
    <div class="project-donate_main">
        <div class="wrap">
            <div class="wrap-row">
                <div class="m-col-tl-8 col-12">
                    {{if singleShare}}
                    <a href="/campaigns/{{= webCampaignAlias}}/donate" class="project-donate_another">
                        <spring:message code="campaign-donate.jsp.propertie.61" text="default text"> </spring:message>
                    </a>
                    {{/if}}
                    <div class="project-donate_list-title">
                        <spring:message code="campaign-donate.jsp.propertie.52" text="default text"> </spring:message>
                    </div>
                    <div class="project-donate_list">
                        <div class="action-card-block action-card-donate">
                            {{if singleShare}}
                            <ul class="js-action-card-list action-card-list"></ul>
                            {{else}}
                            <div class="js-action-card-list"></div>
                            {{/if}}
                        </div>
                    </div>
                </div>
                <div class="m-col-tl-3 m-col-tl-offset-1 m-col-tp-4 col-12 d-tp-block d-none">
                    <div class="donate-info is_stuck">

                        <div class="donate-info_lbl">
                            <spring:message code="campaign-donate.jsp.propertie.47" text="default text"/>
                        </div>
                        <div class="donate-info_text">
                            <spring:message code="campaign-donate.jsp.propertie.48" text="default text"/>
                            <br>
                            +7 495 181-05-05 <spring:message code="time" text="default text"/>
                            <a href="mailto:support@planeta.ru">support@planeta.ru</a>.
                        </div>

                        <div class="donate-info_attention">
                            <div class="donate-shop-attention">
                                <div class="donate-shop-attention_ico">
                                    <svg width="27" height="27" viewBox="0 0 27 27"><g fill="#FFCF0D"><path d="M12.136 12.96v6.124c0 .725.586 1.317 1.303 1.317s1.304-.592 1.304-1.317V12.96c0-.725-.587-1.317-1.304-1.317-.717 0-1.303.592-1.303 1.317zm.163-5.466a1.68 1.68 0 0 0-.489 1.152c0 .428.163.856.489 1.153.293.296.717.494 1.14.494.424 0 .848-.165 1.141-.494a1.69 1.69 0 0 0 .489-1.153 1.55 1.55 0 0 0-.489-1.152A1.653 1.653 0 0 0 13.439 7c-.423 0-.847.198-1.14.494z"></path><path d="M13.5 27C6.044 27 0 20.956 0 13.5S6.044 0 13.5 0 27 6.044 27 13.5 20.956 27 13.5 27zm0-2C19.851 25 25 19.851 25 13.5S19.851 2 13.5 2 2 7.149 2 13.5 7.149 25 13.5 25z"></path></g></svg>
                                </div>
                                <div class="donate-shop-attention_text">
                                    <spring:message code="planeta.this.is.not.a.store" text="default text"/>
                                    <a href="/welcome/projects-agreement.html">Подробнее.</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>

<script id="campaign-donate-share-template" type="text/x-jquery-template">
    <div class="action-card {{if isDonate}}without-reward{{/if}} inactive {{if isSteelHere}}focus active{{/if}}">
        {{if !isDonate}}
        <div class="action-card_in">
        {{/if}}
            <div class="action-card_name">
                <div class="action-card_check" {{if singleShare}} style="display: none;" {{/if}}></div>
                {{= name}}
            </div>
            {{if imageUrl}}
            <div class="action-card_cover">
                <div class="action-card_cover-in photo-zoom-link" href="{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.ORIGINAL, ImageType.DONATE)}}">
                    <img class="action-card_img" src="{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.MEDIUM, ImageType.DONATE)}}">
                </div>
            </div>
            {{/if}}

            <div class="action-card_descr">
                <div class="action-card_descr-text">
                    {{if descriptionHtml}}
                        {{html descriptionHtml}}
                    {{else}}
                        {{= description}}
                    {{/if}}
                </div>
                <div class="action-card_descr-show">
                    <span class="action-card_descr-show-text">
                        Развернуть
                    </span>
                </div>
            </div>


            {{if isDonate}}
            <div class="js-campaign-donate-share-number-picker">
            </div>
            {{else}}
                {{if ((deliveries=ProfileUtils.getEnabledDeliveries(linkedDeliveries)) && deliveries.length > 0 ) || estimatedDeliveryTime }}
                <div class="action-card_delivery">
                    {{if estimatedDeliveryTime}}
                    <div class="action-card_delivery-i">
                            <span class="action-card_delivery-lbl">
                                <spring:message code="campaigns-edit-shares.jsp.propertie.31" text="default text"/>
                            </span>
                        <span class="action-card_delivery-val">
                                {{html DateUtils.estimatedDate(estimatedDeliveryTime)}}
                            </span>
                    </div>
                    {{/if}}

                    {{if deliveries && deliveries.length > 0}}
                    <div class="action-card_delivery-i">
                            <span class="action-card_delivery-lbl">
                                <spring:message code="delivery" text="default text"/>
                            </span>
                        <span class="action-card_delivery-val">
                            {{each($key, delivery) deliveries}}
                                {{if workspace.currentLanguage == 'en'}}
                                    {{= delivery.nameEn }}
                                {{else}}
                                    {{= delivery.name }}
                                {{/if}}
                                {{if $key != deliveries.length - 1}}, {{/if}}
                            {{/each}}
                        </span>
                    </div>
                    {{/if}}
                </div>
            {{/if}}

                <div class="action-card_meta">
                    <div class="action-card_price">
                        {{= StringUtils.humanNumber(price)}} ₽
                    </div>
                    <div class="action-card_count">
                        <div class="action-card_count-i">
                            {{if isAllSold }}
                            <span class="donate-reward_count_lbl"><spring:message code="sold.out" text="default text"> </spring:message></span>
                            {{else}}
                            <span class="action-card_count-lbl"><spring:message code="declension.word.buy" text="default text"/></span>
                            <span class="action-card_count-val">{{= StringUtils.humanNumber(purchaseCount)}}</span>
                            {{/if}}
                        </div>
                        {{if amount > 0}}
                        <div class="action-card_count-i">
                            <span class="action-card_count-lbl"><spring:message code="declension.word.stay4.with.variable" text="default text"/></span>
                            <span class="action-card_count-val">{{= StringUtils.humanNumber(amount - purchaseCount)}}</span>
                        </div>
                        {{/if}}
                    </div>
                </div>

                <div class="action-card_action-wrap">
                    <div class="action-card_action" style="{{if isSteelHere}}height: auto;{{/if}}">
                        <div class="action-card_action-cont js-campaign-donate-share-number-picker">
                        </div>
                    </div>
                </div>
            {{/if}}

            <div class="action-card_show">
                <div class="action-card_show-text">
                    Выбрать это вознаграждение
                </div>
            </div>
        {{if !isDonate}}
        </div>
        {{/if}}
    </div>
</script>

<script id="manual-donate-input-template" type="text/x-jquery-template">
    <div class="donate-action_title"> <spring:message code="campaign-donate.jsp.propertie.51" text="default text"> </spring:message> </div>
    <div class="donate-action_val"> <input type="tel" name="donateAmount" value="{{if donateAmount}}{{= donateAmount}}{{else}}{{= price}}{{/if}}" class="form-control"> </div>
    <div class="donate-action_btn">
        <a href="javascript:void(0);" class="btn btn-nd-primary btn-block js-navigate-to-purchase">
            <spring:message code="campaign-donate.jsp.propertie.28" text="default text"> </spring:message>
        </a>
        <div class="pd-action-error js-donate-amount-error"></div>
    </div>
    <div class="donate-action_attention">
        <div class="donate-shop-attention">
            <div class="donate-shop-attention_ico">
                <svg width="27" height="27" viewBox="0 0 27 27">
                    <g fill="#FFCF0D">
                        <path d="M12.136 12.96v6.124c0 .725.586 1.317 1.303 1.317s1.304-.592 1.304-1.317V12.96c0-.725-.587-1.317-1.304-1.317-.717 0-1.303.592-1.303 1.317zm.163-5.466a1.68 1.68 0 0 0-.489 1.152c0 .428.163.856.489 1.153.293.296.717.494 1.14.494.424 0 .848-.165 1.141-.494a1.69 1.69 0 0 0 .489-1.153 1.55 1.55 0 0 0-.489-1.152A1.653 1.653 0 0 0 13.439 7c-.423 0-.847.198-1.14.494z"></path><path d="M13.5 27C6.044 27 0 20.956 0 13.5S6.044 0 13.5 0 27 6.044 27 13.5 20.956 27 13.5 27zm0-2C19.851 25 25 19.851 25 13.5S19.851 2 13.5 2 2 7.149 2 13.5 7.149 25 13.5 25z"></path>
                    </g>
                </svg>
            </div>
            <div class="donate-shop-attention_text">
                Planeta.ru – не интернет-магазин.
                <br>
                Это способ реализации идей в жизнь.
                <br>
                <a href="/welcome/projects-agreement.html">Подробнее.</a>
            </div>
        </div>
    </div>
</script>


<script id="donate-share-details-delivery-payment-total-template" type="text/x-jquery-template">
    <div class="pln-payment-total"><spring:message code="campaign-donate.jsp.propertie.29" text="default text"> </spring:message>
        <b>{{= StringUtils.humanNumber(totalSum)}}<span class="b-rub">Р</span></b>
    </div>
</script>

<script id="delivery-payment-total-template" type="text/x-jquery-template">
    <div class="pln-payment_enter-sum">
        <div class="pln-payment_enter-sum_lbl">{{if deliveryCost > 0}}<spring:message code="campaign-donate.jsp.propertie.56" text="default text"> </spring:message>{{else}}<spring:message code="campaign-donate.jsp.propertie.57" text="default text"> </spring:message>{{/if}}</div>
        <div class="pln-payment_enter-sum_val">
            <input class="pln-payment_enter-sum_input form-control control-lg js-amount" type="text" value="{{= totalSum}}" {{if deliveryCost > 0}}disabled=""{{/if}}/>
            <span class="pln-payment_enter-sum_val_currency b-rub">Р</span>
        </div>
    </div>
</script>

<script id="campaign-edit-view-image-template" type="text/x-jquery-template">
    <div class="project-create_col-lbl">
        <div class="pc_col-lbl_title">
            <spring:message code="photo.or.video" text="default text"> </spring:message>

            {{if errors.videoContent}}
            <div class="pc_row_warning tooltip" data-tooltip="{{= errors.videoContent}}">
                <div class="s-pc-action-warning"></div>
            </div>
            {{else}}
                <div class="pc_row_help tooltip" data-tooltip="<spring:message code="photo.or.video.hint" text="default text"> </spring:message>"></div>
            {{/if}}
        </div>

        <div class="pc_col-lbl_text">
            <spring:message code="photo.or.video.description" text="default text"> </spring:message>
        </div>
    </div>

    <div class="project-create_col-val">
        <div class="js-val"></div>
    </div>
</script>

<script id="campaign-share-numberpicker-v2-template" type="text/x-jquery-template">
    {{if isDonate}}
    <div class="action-card_counter">
        <input type="tel" class="form-control action-card_counter-val js-currency-mask" name="donateAmount" value="{{if donateAmount}}{{= donateAmount}}{{else}}{{= price}}{{/if}}">
    </div>

    <div class="action-card_action-wrap">
        <div class="action-card_action" style="{{if isSteelHere}}height: auto;{{/if}}">
            <div class="action-card_action-cont">
                <div class="action-card_action-bottom">
                    <div class="action-card_action-btn">
                        <button type="button" class="btn btn-nd-primary action-card_btn js-navigate-to-purchase"><spring:message code="buy.donate" text="default text"/></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{else}}
    <div class="action-card_fix">
        <div class="action-card_fix-in">
            <div class="action-card_counter">
                <div class="action-card_counter-cont">
                    <input type="tel" class="form-control action-card_counter-val js-currency-mask" name="donateAmount" value="{{if donateAmount}}{{= donateAmount}}{{else}}{{= price}}{{/if}}">
                    <button type="button" class="action-card_counter-btn plus js-increase">+</button>
                    <button type="button" class="action-card_counter-btn minus js-decrease">–</button>
                </div>
                <div class="action-card_counter-error">
                    <spring:message code="reward.proceed.rules.error" text="default text"/>
                </div>
            </div>


            <div class="action-card_action-btn">
                <button type="button" class="btn btn-nd-primary action-card_btn js-navigate-to-purchase"><spring:message code="buy" text="default text"/> ({{= quantity}} <spring:message code="item" text="default text"/>)</button>
            </div>

            <div class="action-card_action-count">
                <div class="action-card_count">
                    <div class="action-card_count-i">
                        {{if isAllSold }}
                        <span class="donate-reward_count_lbl"><spring:message code="sold.out" text="default text"/></span>
                        {{else}}
                        <span class="action-card_count-lbl"><spring:message code="declension.word.buy" text="default text"/></span>
                        <span class="action-card_count-val">{{= StringUtils.humanNumber(purchaseCount)}}</span>
                        {{/if}}
                    </div>
                    {{if amount > 0}}
                    <div class="action-card_count-i">
                        <span class="action-card_count-lbl"><spring:message code="declension.word.stay4.with.variable" text="default text"/></span>
                        <span class="action-card_count-val">{{= StringUtils.humanNumber(amount - purchaseCount)}}</span>
                    </div>
                    {{/if}}
                </div>
            </div>

            <div class="action-card_why">
                <span class="action-card_why-text"
                      data-tooltip="<spring:message code='reward.proceed.rules' text="default text"/>"
                      data-tooltip-theme="action-card-why-text-tt" data-tooltip-position="bottom center">
                    <spring:message code="pay.more" text="default text"/>
                </span>
            </div>
        </div>
    </div>

    <div class="action-card_action-bottom">
        {{if rewardInstruction}}
        <div class="action-card_spec">
            {{= rewardInstruction}}
        </div>
        {{/if}}

        <div class="action-card_share">
            <div class="sharing-popup-social sharing-mini js-share-share"></div>
        </div>

        <div class="action-card_link-block">
            <a href="/campaigns/{{= campaignId}}/donatesingle/{{= shareId}}" class="action-card_link"><spring:message code="reward.link" text="default text"/></a>
        </div>
    </div>
    {{/if}}
</script>

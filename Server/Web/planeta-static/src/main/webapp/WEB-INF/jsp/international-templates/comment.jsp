<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script id="comment-last-template" type="text/x-jquery-template">
    <div class="avatar">
        <a href="/{{= ProfileUtils.getUserLink(myProfileId, myProfileAlias)}}">
            <img src="{{= ImageUtils.getThumbnailUrl(myImageUrl, ImageUtils.SMALL_AVATAR, ImageType.USER) }}"
                 alt="{{= myProfileName}}"/>
        </a>
    </div>

    <div class="text">
        <div class="textarea">
            {{if objectType == 'POST' || objectType == 'CAMPAIGN'}}
            <div class="rest-count">{{= restCount}}</div>
            {{/if}}
            <textarea class="js-comment-textarea" placeholder="<spring:message code="write.your" text="default text"> </spring:message>{{if parentCommentId}}<spring:message code="answer" text="default text"> </spring:message>{{else}}<spring:message code="comment" text="default text"> </spring:message>{{/if}}" autocomplete="off"></textarea>
        </div>

        <div class="pln-ctr-enter ctrl-comment">
            <span>
                Enter - <spring:message code="new.line" text="default text"> </spring:message>
            </span>
            <span>
                Ctrl + Enter - <spring:message code="send" text="default text"> </spring:message>
            </span>
        </div>

        <div class="submit-btn">
            <a class="btn btn-primary send-comment"><spring:message code="send.c.l" text="default text"> </spring:message></a>
        </div>
    </div>
</script>

<script id="campaign-comment-item-template" type="text/x-jquery-template">
    {{if deleted}}
        {{tmpl '#deleted-comment-template-item'}}
    {{else}}
        {{if showCloseButton}}
            <div class="close tooltip close-small" data-tooltip="<spring:message code="delete" text="default text"> </spring:message>">×</div>
        {{/if}}

        <div class="attach-comment-ava">
            {{tmpl "#profile-author-small-avatar", $data}}
        </div>

        <div class="attach-comment-text" id="comment{{= commentId}}">
            <div class="name">
                <span  itemscope itemtype="http://schema.org/Person" itemprop="creator">
                    {{tmpl "#profile-author-display-name", $data}}
                </span>

                {{if $data.relationStatuses}}
                    {{if _.contains(relationStatuses, "ADMIN")}}
                        <span class="author-type tooltip" data-tooltip="<spring:message code="admin.comment" text="default text"> </spring:message>"></span>
                    {{else _.contains(relationStatuses, "SELF") || _.contains(relationStatuses, "PROFILE_ADMIN")}}
                        <span class="author-type author tooltip" data-tooltip="<spring:message code="author.comment" text="default text"> </spring:message>"></span>
                    {{/if}}
                {{/if}}

                {{if parentCommentAuthor}}
                    {{if parentCommentId > 0}}
                        {{if objectType != "PRODUCT"}}
                            <a class="anchor tooltip" data-tooltip="<spring:message code="for.user.comment" text="default text"> </spring:message> {{= parentCommentAuthor}}" href="{{= parentCommentUrl}}">
                                <span class="answer-to-user">{{if authorGender=="FEMALE"}}<spring:message code="answered.female" text="default text"> </spring:message>{{else}}<spring:message code="answered.male" text="default text"> </spring:message>{{/if}}</span>
                            </a>
                        {{else}}
                            <a class="tooltip" data-tooltip="<spring:message code="for.user.comment" text="default text"> </spring:message> {{= parentCommentAuthor}}" href="#comment{{= parentCommentId}}">
                                <span class="answer-to-user">{{if authorGender=="FEMALE"}}<spring:message code="answered.female" text="default text"> </spring:message>{{else}}<spring:message code="answered.male" text="default text"> </spring:message>{{/if}}</span>
                            </a>
                        {{/if}}
                    {{/if}}
                {{/if}}

                <span class="date updatable-time" data-updatable-time="{{= timeAdded}}">{{=
                    DateUtils.formatDate(timeAdded)}}
                </span>

                <span class="meta-sep">&bull;</span>

                {{if commentUrl}}
                    <span class="link-to-comment">
                        <a class="anchor tooltip" data-tooltip="<spring:message code="link.to.current.comment" text="default text"> </spring:message>" href="{{= commentUrl}}">#</a>
                    </span>
                {{/if}}
            </div>

            <div class="content expandable-content{{if expanded}}expand{{/if}}" itemprop="commentText">
                <div class="js-content-h">{{html textHtml}}</div>
            </div>

            <div class="comment-more hidden">
                <span class="comment-more-link">
                    {{if expanded}}
                        <spring:message code="roll.up" text="default text"> </spring:message>
                    {{else}}
                        <spring:message code="roll.down" text="default text"> </spring:message>
                    {{/if}} <spring:message code="comment" text="default text"> </spring:message>
                </span>
            </div>

            {{if workspace.isAuthorized}}
                <div class="answer">
                    <span class="answer-link"><spring:message code="answer.comment" text="default text"> </spring:message></span>
                </div>
            {{/if}}

            <div class="comment-likes"></div>

            {{if !restored}}
                {{if show30SecCloseButton}}
                    <div class="time-delete-comment">
                        {{if "CAMPAIGN" == objectType}}
                            <spring:message code="comment.response" text="default text"> </spring:message><br>
                        {{/if}}
                        <i class="icon-info icon-gray"></i> <spring:message code="you.can" text="default text"> </spring:message>
                        <a href="javascript:void(0)"><spring:message code="delete.comment" text="default text"> </spring:message></a> <spring:message code="within" text="default text"> </spring:message> <span
                            class="comment-undeletable-counter"><b>{{if secondsLast}}{{= secondsLast}}{{else}}30{{/if}}</b> <spring:message code="seconds" text="default text"> </spring:message></span>.
                    </div>
                {{/if}}
            {{/if}}
        </div>
    {{/if}}
</script>

<script id="deleted-comment-template-item" type="text/x-jquery-template">
    <div class="attach-comment-deleted">
        {{if PrivacyUtils.isObjectOwnerOrAuthor(profileId, authorProfileId)}}
            <div class="recovery-link">
                {{if !inProcess}}
                    <a href="javascript:void(0)" class="restore-comment">
                        <spring:message code="reestablish" text="default text"> </spring:message>
                    </a>
                {{/if}}
            </div>
        {{/if}}
        <spring:message code="comment.deleted" text="default text"> </spring:message>
    </div>
</script>

<script id="comment-denied-template" type="text/x-jquery-template">
    <div class="pln-ww-unregister">
        {{if !workspace.isAuthorized}}
            <spring:message code="to.post.comments" text="default text"> </spring:message>
            <a href="javascript:void(0)" class="registration-link"><spring:message code="register" text="default text"> </spring:message></a>
            <spring:message code="or" text="default text"> </spring:message>
            <a href="javascript:void(0)" class="signup-link"><spring:message code="authorize" text="default text"> </spring:message></a>
        {{else}}
            <spring:message code="you.cant.comment" text="default text"> </spring:message>
        {{/if}}
    </div>
</script>

<script id="comment-last-template-fixed" type="text/x-jquery-template">
    <div class="photo-dlg-avatar">
        <a href="/{{= ProfileUtils.getUserLink(myProfileId, myProfileAlias)}}">
            <img src="{{= ImageUtils.getThumbnailUrl(myImageUrl, ImageUtils.SMALL_AVATAR, ImageType.USER) }}"
                 alt="{{= myProfileName}}"/>
        </a>
    </div>
    <div class="photo-dlg-content">
        <div class="photo-dlg-textarea">
            <textarea placeholder="<spring:message code="send.c.l" text="default text"> </spring:message>" autocomplete="off" rows="3"></textarea>
        </div>
        <div class="photo-dlg-option">
            <div class="photo-dlg-send">
                <a class="btn btn-primary send-comment">
                    <spring:message code="write.your.comment" text="default text"> </spring:message>
                </a>
            </div>
        </div>
    </div>
</script>

<script id="comments-counter-template" type="text/x-jquery-template">
    <div class="post-option-item photo-likes"></div>
    <div class="post-option-item">
        {{if !$data.commentsCount}}
            <i class="icon-comment icon-gray"></i> <spring:message code="no.comments" text="default text"> </spring:message>
        {{else}}
            <i class="icon-comment icon-gray"></i>
            <a class="post-option-link">{{= commentsCount}} <spring:message code="decl.comment" text="default text"> </spring:message></a>
        {{/if}}
    </div>
    <div class="post-option-item share-cont" style="display: none;">
        <a href="javascript:void(0);" class="post-option-link js-share-link">
            <spring:message code="share" text="default text"> </spring:message>
        </a>
        <i class="icon-vect-share icon-gray"></i>
    </div>
</script>

<script id="comment-template-item" type="text/x-jquery-template">
    {{if deleted}}
        {{tmpl '#deleted-comment-template-item'}}
    {{else}}
        {{if showCloseButton}}
            <div class="close tooltip close-small" data-tooltip="<spring:message code="delete" text="default text"> </spring:message>">×</div>
        {{/if}}

        <div class="attach-comment-ava">
            {{tmpl "#profile-author-small-avatar", $data}}
        </div>

        <div class="attach-comment-text" id="comment{{= commentId}}">
            <div class="name">
                <div  itemscope itemtype="http://schema.org/Person" itemprop="creator">
                    {{tmpl "#profile-author-display-name", $data}}
                </div>

                <div class="date updatable-time" data-updatable-time="{{= timeAdded}}">{{=
                    DateUtils.formatDate(timeAdded)}}
                </div>
                <span class="hidden" itemprop="commentTime">{{= DateUtils.formatDateSimple(timeAdded, "YYYY-MM-DD", true)}}</span>
            </div>
            <div class="content" itemprop="commentText">{{html textHtml}}</div>
            <div class="comment-likes"></div>
            {{if !restored}}
                {{if show30SecCloseButton}}
                    <div class="time-delete-comment">
                        {{if "CAMPAIGN" == objectType}}
                            <spring:message code="answer.your.comment" text="default text"> </spring:message><br>
                        {{/if}}
                        <i class="icon-info icon-gray"></i><spring:message code="you.can" text="default text"> </spring:message> <a href="javascript:void(0)">
                        <spring:message code="delete.comment" text="default text"> </spring:message></a> <spring:message code="within" text="default text"> </spring:message> <span
                            class="comment-undeletable-counter"><b>{{if secondsLast}}{{= secondsLast}}{{else}}30{{/if}}</b> <spring:message code="seconds" text="default text"> </spring:message></span>.
                    </div>
                {{/if}}
            {{/if}}
        </div>
    {{/if}}
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!--include css /css-generated/network.css-->
<!--include css /css-generated/project.css-->


<script id="share-search-item-template" type="text/x-jquery-template">
    {{if shareOrCampaignImageUrl}}
    <div class="pd-condition-img">
            <span class="photo-zoom-link" href="{{= ImageUtils.getThumbnailUrl(shareOrCampaignImageUrl, ImageUtils.ORIGINAL, ImageType.DONATE)}}">
                <img src="{{= ImageUtils.getThumbnailUrl(shareOrCampaignImageUrl, ImageUtils.PRODUCT_COVER, ImageType.DONATE)}}">
            </span>
    </div>
    {{/if}}

    <div class="pd-condition-cont">
        <div class="cf">
            <div class="pd-condition-sum pln-donate-box_radio">
                <a href="/campaigns/{{= campaignId}}/donate/{{= shareId}}">{{= StringUtils.humanNumber(price)}}</a> <span class="b-rub">Р</span>
            </div>
        </div>


        <div class="pd-condition-name">{{= name}}</div>

        <div class="pd-condition-descr">
            {{if descriptionHtml}}
            {{html descriptionHtml}}
            {{else}}
            {{= description}}
            {{/if}}
        </div>
        {{if estimatedDeliveryTime}}
        <div class="pd-condition-date"><spring:message code="campaign-donate.jsp.propertie.54" text="default text"> </spring:message> {{= DateUtils.estimatedDate(estimatedDeliveryTime)}}</div>
        {{/if}}
    </div>
</script>

<script id="share-search-block-template" type="text/x-jquery-template">
    <div class="wrap">
        <div class="col-12">
            <input id="query" class="n-search_input" value="{{= queryModel.query}}" type="text" placeholder="<spring:message code="find" text="default text"> </spring:message>" data-emoji_font="true" style="font-family: ProximaNovaLight, Arial, sans-serif, 'Segoe UI Emoji', 'Segoe UI Symbol', Symbola, EmojiSymbols !important;">
        </div>
    </div>
</script>


<script id="share-search-results-container-template" type="text/x-jquery-template">
    <div class="col-12">
        <div class="js-campaign-search-injection"></div>
        <div class="project-list-block pln-donate-box"></div>
    </div>
</script>

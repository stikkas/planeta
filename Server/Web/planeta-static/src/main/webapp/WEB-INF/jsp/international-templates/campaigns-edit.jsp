<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!--include css /css-generated/project.css-->

<script id="funding-rules-template" type="text/x-jquery-template">
    <div id="central-c">
        <div class="founding-rules-content">
            <p class="frc-title"><span class="frc-title-first-word"><spring:message code="campaigns-edit.jsp.propertie.1" text="default text"> </spring:message></span><spring:message code="campaigns-edit.jsp.propertie.93" text="default text"> </spring:message></p>

            {{html customHtml}}
            <p class="frc-rules"><spring:message code="campaigns-edit.jsp.propertie.2" text="default text"> </spring:message></p>
            <ul class="frc-rules-list">
                <li class="frc-rules-list-items"><spring:message code="campaigns-edit.jsp.propertie.3" text="default text"> </spring:message><a href="/welcome/projects-agreement.html"><spring:message code="campaigns-edit.jsp.propertie.94" text="default text"> </spring:message></a></li>
                <li class="frc-rules-list-items"><spring:message code="campaigns-edit.jsp.propertie.4" text="default text"> </spring:message></li>
                <li class="frc-rules-list-items"><spring:message code="campaigns-edit.jsp.propertie.5" text="default text"> </spring:message></li>
            </ul>

            <p class="frc-rules"><spring:message code="campaigns-edit.jsp.propertie.6" text="default text"> </spring:message></p>
            <ul class="frc-rules-list">
                <li class="frc-rules-list-items"><spring:message code="campaigns-edit.jsp.propertie.70" text="default text"> </spring:message></li>
                <li class="frc-rules-list-items"><spring:message code="campaigns-edit.jsp.propertie.7" text="default text"> </spring:message></li>
                <li class="frc-rules-list-items"><spring:message code="campaigns-edit.jsp.propertie.8" text="default text"> </spring:message><a href="/welcome/projects-agreement.html"><spring:message code="campaigns-edit.jsp.propertie.95" text="default text"> </spring:message></a></li>
            </ul>

            <p class="frc-rules"><spring:message code="campaigns-edit.jsp.propertie.9" text="default text"> </spring:message></p>
            <ol class="frc-rules-list">
                <li class="frc-rules-list-items"><spring:message code="campaigns-edit.jsp.propertie.10" text="default text"> </spring:message></li>
                <li class="frc-rules-list-items"><spring:message code="campaigns-edit.jsp.propertie.11" text="default text"> </spring:message></li>
                <li class="frc-rules-list-items"><spring:message code="campaigns-edit.jsp.propertie.12" text="default text"> </spring:message></li>
                <li class="frc-rules-list-items"><spring:message code="campaigns-edit.jsp.propertie.13" text="default text"> </spring:message></li>
            </ol>
            <p class="frc-rules-descr"><spring:message code="campaigns-edit.jsp.propertie.14" text="default text"> </spring:message><a href="mailto:support@planeta.ru">support@planeta.ru</a>
                .</p>

            {{if isAuthorized }}
            <div class="frc-i-accept">
                <p class="frc-i-accept-p">
                    <label>
                        <input type="checkbox" name="acceptRules"><spring:message code="campaigns-edit.jsp.propertie.15" text="default text"> </spring:message><a class="frc-i-accept-p-a" href="/welcome/projects-agreement.html"><spring:message code="campaigns-edit.jsp.propertie.89" text="default text"> </spring:message></a>
                    </label>
                </p>
                <a class="btn btn-primary frc-i-accept-btn create-campaign disabled" href="javascript:void(0);"><spring:message code="campaigns-edit.jsp.propertie.16" text="default text"> </spring:message></a>
            </div>
            {{else}}
            <p class="frc-rules-descr">
                <spring:message code="campaigns-edit.jsp.propertie.17" text="default text"> </spring:message>
            </p>

            <div class="frc-i-accept frc-attention">
                <p class="frc-i-accept-p"><spring:message code="campaigns-edit.jsp.propertie.18" text="default text"> </spring:message><a href="javascript:void(0);" onclick="LazyHeader.showAuthForm('signup');"><spring:message code="campaigns-edit.jsp.propertie.90" text="default text"> </spring:message></a><spring:message code="campaigns-edit.jsp.propertie.91" text="default text"> </spring:message><a href="javascript:void(0);" onclick="LazyHeader.showAuthForm('registration');"><spring:message code="campaigns-edit.jsp.propertie.92" text="default text"> </spring:message></a></p>
            </div>
            {{/if}}
        </div>
    </div>
    <div id="right-s">
        <div class="founding-rules-aside">
            <!--<%--<p class="fra-title"><spring:message code="campaigns-edit.jsp.propertie.19" text="default text"> </spring:message></p>--%>-->
            <p class="fra-description"><spring:message code="campaigns-edit.jsp.propertie.20" text="default text"> </spring:message></p>
            <ul class="fra-list">
                <li class="fra-list-items"><spring:message code="campaigns-edit.jsp.propertie.21" text="default text"> </spring:message></li>
                <li class="fra-list-items"><spring:message code="campaigns-edit.jsp.propertie.22" text="default text"> </spring:message></li>
                <li class="fra-list-items"><spring:message code="campaigns-edit.jsp.propertie.23" text="default text"> </spring:message></li>
                <li class="fra-list-items"><spring:message code="campaigns-edit.jsp.propertie.24" text="default text"> </spring:message></li>
                <li class="fra-list-items"><spring:message code="campaigns-edit.jsp.propertie.25" text="default text"> </spring:message></li>
                <li class="fra-list-items"><spring:message code="campaigns-edit.jsp.propertie.26" text="default text"> </spring:message></li>
                <li class="fra-list-items"><spring:message code="campaigns-edit.jsp.propertie.27" text="default text"> </spring:message><a href="/welcome/projects-agreement.html"><spring:message code="campaigns-edit.jsp.propertie.96" text="default text"> </spring:message></a></li>
            </ul>
            <br/>
            <p style="margin: 0 10px 0 20px;"><spring:message code="campaigns-edit.jsp.propertie.28" text="default text"> </spring:message></p>
        </div>
    </div>

</script>


<script id="campaign-edit-header-template" type="text/x-jquery-template">
    <div class="cont-header_name">
        {{if status == "DRAFT"}}
            <spring:message code="campaigns-edit.jsp.propertie.71" text="default text"> </spring:message>
        {{else}}
            <spring:message code="campaigns-edit.jsp.propertie.72" text="default text"> </spring:message>
        {{/if}}
    </div>
    <div class="js-profile-sites hidden"></div>
    <div class="cont-header_shop-links">
        {{if PrivacyUtils.hasAdministrativeRole()}}
        <div class="cont-header_shop-links_i">
            <a class="cont-header_shop-links_link" href="//{{= workspace.serviceUrls.adminHost }}/moderator/campaign-moderation-info.html?campaignId={{= campaignId}}" target="_blank">
                <spring:message code="moderation" text="default text"/>
            </a>
        </div>
        {{/if}}
        <div class="cont-header_shop-links_i">
            <a class="cont-header_shop-links_link" href="javascript:void(0)" data-event-click="onContactsClick"><spring:message code="campaigns-edit.jsp.propertie.29" text="default text"> </spring:message></a>
        </div>
        <div class="cont-header_shop-links_i">
            <a class="cont-header_shop-links_link" href="/faq/article/11"><spring:message code="campaigns-edit.jsp.propertie.30" text="default text"> </spring:message></a>
        </div>
    </div>
</script>

<script id="campaign-edit-footer-template" type="text/x-jquery-template">
    <div class="hidden"></div>


    <div class="project-create_action-left">
        {{if isSaving}}
            <span class="btn project-create_save disabled" id="draft-btn-state"><spring:message code="campaigns-edit.jsp.propertie.36" text="default text"> </spring:message></span>
        {{else (!nextTab && status == 'PATCH') || isStarted || status != 'DRAFT'}}
            <span class="btn project-create_save" data-event-click="validate1"><spring:message code="campaigns-edit.jsp.propertie.33" text="default text"> </spring:message></span>
        {{else}}
            <span class="btn project-create_save" data-event-click="saveCampaign"><spring:message code="campaigns-edit.jsp.propertie.33" text="default text"> </spring:message></span>
        {{/if}}

        {{if !isStarted && prevTab}}
            <a id="prev_tab" href="/campaigns/{{= webCampaignAlias}}/{{= prevTab}}" class="btn project-create_back" tabindex="-1"><spring:message code="campaigns-edit.jsp.propertie.34" text="default text"> </spring:message></a>
        {{/if}}
    </div>


    <div class="project-create_action-right">
        <span class="project-create_preview-link">
            <span class="project-create_preview-link_text js-project-preview"><spring:message code="campaigns-edit.jsp.propertie.31" text="default text"> </spring:message></span>
        </span>

        {{if !isStarted}}
            {{if nextTab}}
                <a id="next_tab" href="/campaigns/{{= webCampaignAlias}}/{{= nextTab}}" class="btn btn-primary project-create_next"><spring:message code="campaigns-edit.jsp.propertie.32" text="default text"> </spring:message></a>
            {{else}}
                {{if status == 'DRAFT'}}
                    <span class="btn btn-primary project-create_next" data-event-click="validate"><spring:message code="campaigns-edit.jsp.propertie.35" text="default text"> </spring:message></span>
                {{/if}}
            {{/if}}
        {{else}}
            <a href="/campaigns/{{= webCampaignAlias}}" class="btn project-create_next"><spring:message code="back.to.campaign" text="default text"> </spring:message></a>
        {{/if}}
    </div>
</script>

<script id="campaign-edit-tabs-template" type="text/x-jquery-template">

    <div class="project-create_support">
        <spring:message code="questions" text="default text"> </spring:message> {{= workspace.supportContacts.supportPhoneNumber}}
        <br>
        <spring:message code="time" text="default text"> </spring:message> <a href="mailto:{{= workspace.supportContacts.supportEmail}}"><b>{{= workspace.supportContacts.supportEmail}}</b></a>
    </div>

    <div class="project-create_tab-wrap">
    <div class="project-create_tab">
        {{= activeTab}}
        {{each($key, tab) tabs}}
            <div class="project-create_tab_i{{if activeTab == $key}} active{{/if}}">
                <a class="project-create_tab_link" href="/campaigns/{{= campaignId}}/{{= $key}}">
                    {{if $key == "edit"}}
                        <spring:message code="basic.tab" text="default text"> </spring:message>
                    {{else $key == "edit-details"}}
                        <spring:message code="details.tab" text="default text"> </spring:message>
                    {{else $key == "edit-shares"}}
                        <spring:message code="shares.tab" text="default text"> </spring:message>
                    {{else $key == "edit-contractor"}}
                        <spring:message code="contractor.tab" text="default text"> </spring:message>
                    {{/if}}
                </a>
            </div>
        {{/each}}
    </div>
    </div>
</script>

<script id="campaign-edit-common-content-template" type="text/x-jquery-template">
    <form class="js-main-campaign-form">
        <div class="project-create_cont">
            <div class="project-create_list"></div>
        </div>
    </form>
</script>


<script id="campaign-edit-categories-template" type="text/x-jquery-template">
    <div class="project-create_list">
        <div class="project-create_row">
            <div class="project-create_col-lbl">
                <div class="pc_col-lbl_title">
                    <spring:message code="campaigns-edit.jsp.propertie.75" text="default text"> </spring:message>
                    {{if errors.tags}}
                    <div class="pc_row_warning tooltip" data-tooltip="{{= errors.tags}}">
                        <div class="s-pc-action-warning"></div>
                    </div>
                    {{else}}
                    <div class="pc_row_help tooltip" data-tooltip="<spring:message code="campaigns-edit.jsp.propertie.41" text="default text"> </spring:message>"></div>
                    {{/if}}
                </div>
            </div>
            <div class="project-create_col-val">
                <div class="select2-flat">
                    <input multiple="multiple" class="select2-offscreen select-tags js-select2"
                           placeholder="<spring:message code="campaigns-edit.jsp.propertie.42" text="default text"> </spring:message>" {{if useEng}}data-field-name="engName"{{else}}data-field-name="name"{{/if}} tabindex="-1">
                </div>
            </div>
        </div>
        {{if isCharity}}
        <div class="project-create_row error-row">
            <div class="project-create_col-val">
                <div class="project-create_col-val_cont">
                    <div class="text-center"><spring:message code="campaigns-edit.jsp.propertie.43" text="default text"> </spring:message></div>
                </div>
            </div>
        </div>
        {{/if}}
        {{if warnings && warnings.length}}
            {{each(i, warn) warnings}}
            <div class="project-create_row error-row">
                <div class="project-create_col-val">
                    <div class="project-create_col-val_cont">
                        <div class="text-center">
                            {{= warn}}
                        </div>
                    </div>
                </div>
            </div>
            {{/each}}
        {{/if}}
    </div>
</script>

<script id="campaign-edit-top-campaign-inner-template" type="text/x-jquery-template">
    <span class="project-card-cover-wrap">
            <img class="project-card-cover lazy" width="220" height="134"
                 data-original="{{if imageUrl}}{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.PROJECT_ITEM)}}{{/if}}"/>
        </span>
    <span class="project-card-text">
            <span class="project-card-name hyphenate">{{= name}}</span>
            <span class="project-card-descr">{{= shortDescription}}</span>
        </span>
    <span class="project-card-footer">
            {{if timeFinish && !targetAmount}}
            <span class="project-card-no-progress"></span>
            <span class="project-card-info">
                <span class="project-card-info-i">
                    <span class="pci-label"><spring:message code="campaigns-edit.jsp.propertie.44" text="default text"> </spring:message></span>
                    <span class="pci-value">{{= StringUtils.humanNumber(collectedAmount)}} <span class="b-rub">Р</span></span>
                </span>
                <span class="project-card-info-i">
                    {{if DateUtils.getDaysTo(timeFinish) > 0}}
                        <span class="pci-label"><spring:message code="campaigns-edit.jsp.propertie.46" text="default text"> </spring:message></span><spring:message code="campaigns-edit.jsp.propertie.46" text="default text"> </spring:message><span
                        class="pci-value"> {{= DateUtils.getDaysTo(timeFinish)}}&nbsp;<spring:message code="campaigns-edit.jsp.propertie.47" text="default text"> </spring:message></span>
                    {{else DateUtils.getHoursTo(timeFinish) > 0}}
                        <span class="pci-label"><spring:message code="campaigns-edit.jsp.propertie.48" text="default text"> </spring:message></span><spring:message code="campaigns-edit.jsp.propertie.48" text="default text"> </spring:message><span
                        class="pci-value"> {{= DateUtils.getHoursTo(timeFinish)}}&nbsp;<spring:message code="campaigns-edit.jsp.propertie.49" text="default text"> </spring:message></span>
                    {{/if}}
                </span>
            </span>
            {{else targetAmount}}
                <span class="project-card-progress{{if collectedAmount >= targetAmount}} over-progress{{else status == "FINISHED"}} finish-fail{{/if}}">
                    <span class="pcp-bar" style="width:{{= (100*collectedAmount/targetAmount) % 100}}%;"></span>
                </span>
                {{if status == "FINISHED" && targetStatus == "SUCCESS"}}
                <span class="project-card-finish">
                                    <spring:message code="campaigns-edit.jsp.propertie.76" text="default text"> </spring:message> {{if Math.floor(100*collectedAmount/targetAmount) > 0}}{{= Math.floor(100*collectedAmount/targetAmount)}}%{{/if}}
                                </span>
                <span class="pci-value project-card-finish-sum">
                                    <span class="pci-value">{{= StringUtils.humanNumber(collectedAmount)}} <span class="b-rub">Р</span></span>
                                </span>
                {{else}}
                <span class="project-card-info">
                                <span class="project-card-info-i">
                                    <span class="pci-label"><spring:message code="campaigns-edit.jsp.propertie.76" text="default text"> </spring:message> {{if Math.floor(100*collectedAmount/targetAmount) > 0}}{{= Math.floor(100*collectedAmount/targetAmount)}}%{{/if}}</span>
                                    <span class="pci-value">{{= StringUtils.humanNumber(collectedAmount)}} <span class="b-rub">Р</span></span>
                                </span>
                                <span class="project-card-info-i">
                                    <span class="pci-label"><spring:message code="campaigns-edit.jsp.propertie.52" text="default text"> </spring:message></span>
                                    <span class="pci-value">{{= StringUtils.humanNumber(targetAmount)}} <span class="b-rub">Р</span></span>
                                </span>
                            </span>
                {{/if}}
            {{/if}}
    </span>
</script>


<script id="campaign-edit-name-template" type="text/x-jquery-template">
    <div class="project-create_row{{if errors.campaignName}} error{{/if}}">
        <div class="project-create_col-lbl">
            <div class="pc_col-lbl_title">
                <spring:message code="campaigns-edit.jsp.propertie.77" text="default text"> </spring:message>
                {{if errors.name}}
                <div class="pc_row_warning tooltip" data-tooltip="{{= errors.name}}">
                    <div class="s-pc-action-warning"></div>
                </div>
                {{else}}
                <div class="pc_row_help tooltip" data-tooltip="<spring:message code="campaigns-edit.jsp.propertie.54" text="default text"> </spring:message>"></div>
                {{/if}}
            </div>
        </div>
        <div class="project-create_col-val">
            <input class="form-control prj-crt-input" data-limit="45" type="text"
                   placeholder="<spring:message code="campaigns-edit.jsp.propertie.78" text="default text"> </spring:message>" value="{{= name}}">
            <div class="rest-count"></div>
        </div>
    </div>
</script>

<script id="campaign-edit-campaign-alias-template" type="text/x-jquery-template">
    <div class="project-create_row">
        <div class="project-create_col-lbl">
            <div class="pc_col-lbl_title">
                <spring:message code="campaigns-edit.jsp.propertie.79" text="default text"> </spring:message>
                {{if errors.campaignAlias}}
                <div class="pc_row_warning tooltip" data-tooltip="{{= errors.campaignAlias}}">
                    <div class="s-pc-action-warning"></div>
                </div>
                {{else}}
                <div class="pc_row_help tooltip" data-tooltip="<spring:message code="campaigns-edit.jsp.propertie.55" text="default text"> </spring:message>">
                </div>
                {{/if}}
            </div>
        </div>
        <div class="project-create_col-val">
            <table class="input-url_table">
                <tbody>
                <tr>
                    <td class="input-url_lbl">https://planeta.ru/campaigns/</td>
                    <td class="input-url_val">
                        <input class="form-control control-xlg borderless"
                               type="text"
                               placeholder="{{= $data.campaignId}}"
                               data-field-name="campaignAlias"
                               value="{{= campaignAlias}}"
                                {{if !PrivacyUtils.hasAdministrativeRole()
                                        && ~[3, 4, 5, 9, 10].indexOf($data.statusCode)
                                }}
                                    disabled="1"
                                {{/if}}
                        >
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</script>

<script id="campaign-edit-short-description-template" type="text/x-jquery-template">
    <div class="project-create_row{{if errors.shortDescription}} error{{/if}}">
        <div class="project-create_col-lbl">
            <div class="pc_col-lbl_title">
                <spring:message code="campaigns-edit.jsp.propertie.80" text="default text"> </spring:message>
                {{if errors.shortDescription}}
                <div class="pc_row_warning tooltip" data-tooltip="{{= errors.shortDescription}}">
                    <div class="s-pc-action-warning"></div>
                </div>
                {{else}}
                <div class="pc_row_help tooltip" data-tooltip="<spring:message code="campaigns-edit.jsp.propertie.81" text="default text"> </spring:message>">
                </div>
                {{/if}}
            </div>
        </div>
        <div class="project-create_col-val">
            <textarea class="form-control prj-crt-input" data-limit="180" rows="4"
                      placeholder="<spring:message code="campaigns-edit.jsp.propertie.80" text="default text"> </spring:message>">{{= shortDescription}}</textarea>
            <div class="rest-count"></div>
        </div>
    </div>
</script>

<script id="campaign-edit-location-template" type="text/x-jquery-template">
    <div class="project-create_row {{if errors.countryId}}error{{/if}}">
        <div class="project-create_col-lbl">
            <div class="pc_col-lbl_title">
                <spring:message code="campaigns-edit.jsp.propertie.101" text="default text"> </spring:message>
                {{if errors.countryId}}
                    <div class="pc_row_warning tooltip" data-tooltip="{{= errors.countryId}}">
                        <div class="s-pc-action-warning"></div>
                    </div>
                {{else}}
                    <div class="pc_row_help tooltip" data-tooltip="<spring:message code="campaigns-edit.jsp.propertie.102" text="default text"> </spring:message>"></div>
                {{/if}}
            </div>
        </div>
        <div class="project-create_col-val">
            <select class="pln-select pln-select-xlg prj-crt-input js-country_select" data-planeta-ui="country" id="countryId" name="countryId"></select>
        </div>
    </div>

    <div data-anchor="countrySelect" class="project-create_row" {{if countryId == 0}}style="display: none"{{else}}style="display: block"{{/if}}>
        <div class="project-create_row{{if errors.regionId}} error{{/if}}">
            <div class="project-create_col-lbl">
                <div class="pc_col-lbl_title">
                    <spring:message code="campaigns-edit.jsp.propertie.105" text="default text"> </spring:message>
                    {{if errors.regionId}}
                        <div class="pc_row_warning tooltip" data-tooltip="{{= errors.regionId}}">
                            <div class="s-pc-action-warning"></div>
                        </div>
                    {{else}}
                        <div class="pc_row_help tooltip" data-tooltip="<spring:message code="campaigns-edit.jsp.propertie.106" text="default text"> </spring:message>"></div>
                    {{/if}}
                </div>
            </div>
            <div class="project-create_col-val">
                {{if lang == "ru"}}
                <input class="form-control control-xlg prj-crt-input" name="regionNameRus" data-planeta-ui="region" type="text" placeholder="<spring:message code="enter.region" text="default text"> </spring:message>" value="{{= regionNameRus}}">
                {{else}}
                <input class="form-control control-xlg prj-crt-input" name="regionNameEng" data-planeta-ui="region" type="text" placeholder="<spring:message code="enter.region" text="default text"> </spring:message>" value="{{= regionNameEng}}">
                {{/if}}
                <input class="hide" id="regionId" name="regionId" value="{{= regionId}}">
            </div>
        </div>

        <div class="project-create_row{{if errors.cityId}} error{{/if}}">
            <div class="project-create_col-lbl">
                <div class="pc_col-lbl_title">
                    <spring:message code="campaigns-edit.jsp.propertie.103" text="default text"> </spring:message>
                    {{if errors.cityId}}
                    <div class="pc_row_warning tooltip" data-tooltip="{{= errors.cityId}}">
                        <div class="s-pc-action-warning"></div>
                    </div>
                    {{else}}
                    <div class="pc_row_help tooltip" data-tooltip="<spring:message code="campaigns-edit.jsp.propertie.104" text="default text"> </spring:message>"></div>
                    {{/if}}
                </div>
            </div>
            <div class="project-create_col-val">
                {{if lang == "ru"}}
                <input class="form-control control-xlg prj-crt-input" name="cityNameRus" data-planeta-ui="city" type="text" placeholder="<spring:message code="enter.city" text="default text"> </spring:message>" value="{{= cityNameRus}}" disabled="disabled">
                {{else}}
                <input class="form-control control-xlg prj-crt-input" name="cityNameEng" data-planeta-ui="city" type="text" placeholder="<spring:message code="enter.city" text="default text"> </spring:message>" value="{{= cityNameEng}}" disabled="disabled">
                {{/if}}
                <input class="hide" id="cityId" name="cityId" value="{{= cityId}}">
            </div>
        </div>
    </div>
</script>

<script id="campaign-edit-target-amount-template" type="text/x-jquery-template">
    <div class="project-create_col-lbl">
        <div class="pc_col-lbl_title"><spring:message code="campaigns-edit.jsp.propertie.56" text="default text"> </spring:message><span class="b-rub"> Р</span>
            {{if errors.targetAmount}}
            <div class="pc_row_warning tooltip" data-tooltip="{{= errors.targetAmount}}">
                <div class="s-pc-action-warning"></div>
            </div>
            {{else}}
            <div class="pc_row_help tooltip" data-tooltip='<spring:message code="campaigns-edit.jsp.propertie.82" text="default text"> </spring:message>'></div>
            {{/if}}
        </div>
    </div>
    <div class="project-create_col-val">
        <input class="form-control prj-crt-input" value="{{= targetAmount}}" type="text" data-parse="number" placeholder="<spring:message code="campaigns-edit.jsp.propertie.57" text="default text" />"
        {{if (!(PrivacyUtils.hasAdministrativeRole() || isDraft) || (targetAmount == 0 && timeFinish != null)) && PrivacyUtils.hasAdministrativeRole()}}disabled{{/if}}>
        <!--
        <div class="project-create_col-val_col" style="width: auto">
            <div class="project-create_col-val_cont" {{if !PrivacyUtils.hasAdministrativeRole()}}style="display: none;"{{/if}}>
                <label>
                    <input type="checkbox" {{if (targetAmount == 0) && (PrivacyUtils.hasAdministrativeRole() && (notFirstChangeTargetAmount || timeFinish != null))}}checked="checked"{{/if}}
                    {{if (timeFinish == null && targetAmount != 0) || !(PrivacyUtils.hasAdministrativeRole() || isDraft) || !PrivacyUtils.hasAdministrativeRole()}}disabled{{/if}}>
                    <div><spring:message code="campaigns-edit.jsp.propertie.99" text="default text"> </spring:message></div>
                </label>
            </div>
        </div>
        -->
    </div>
</script>

<script id="campaign-edit-time-finish-template" type="text/x-jquery-template">
    <div class="project-create_col-lbl">
        <div class="pc_col-lbl_title">
            <spring:message code="campaigns-edit.jsp.propertie.83" text="default text"> </spring:message>
            {{if errors.timeFinish}}
            <div class="pc_row_warning tooltip" data-tooltip="{{= errors.timeFinish}}">
                <div class="s-pc-action-warning"></div>
            </div>
            {{else}}
            <div class="pc_row_help tooltip" data-tooltip="<spring:message code="campaigns-edit.jsp.propertie.84" text="default text"> </spring:message>"></div>
            {{/if}}
        </div>
    </div>
    <div class="project-create_col-val">
        <div class="project-create_col-val_col" style="width: 200px">
            <input class="form-control prj-crt-input prj-crt-input__date" value="{{= timeFinish}}" type="text" data-parse="number"
                   placeholder="<spring:message code="campaigns-edit.jsp.propertie.59" text="default text"> </spring:message>" {{if (!(PrivacyUtils.hasAdministrativeRole() || isDraft) || (targetAmount != 0 && timeFinish == null))  && (PrivacyUtils.hasAdministrativeRole() && notFirstChangeTimeFinish)}}disabled{{/if}}>
        </div>
        <!--
        <div class="project-create_col-val_col" style="width: auto">
            <div class="project-create_col-val_cont" {{if !PrivacyUtils.hasAdministrativeRole()}}style="display: none;"{{/if}}>
                <label>
                    <input type="checkbox" {{if (targetAmount != 0 && timeFinish == null) && (PrivacyUtils.hasAdministrativeRole() && notFirstChangeTimeFinish)}}checked="checked"{{/if}}
                    {{if (targetAmount == 0 && timeFinish != null) || !(PrivacyUtils.hasAdministrativeRole() || isDraft) || !PrivacyUtils.hasAdministrativeRole()}}disabled{{/if}}
                    >
                    <div><spring:message code="campaigns-edit.jsp.propertie.100" text="default text"> </spring:message></div>
                </label>
            </div>
        </div>
        -->
    </div>
</script>

<script id="campaign-edit-check-content-template" type="text/x-jquery-template">
    <div class="project-create_cont"></div>
</script>




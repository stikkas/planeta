<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!--include css /css-generated/project.css-->


<script id="campaigns-list" type="text/x-template">
    <div v-for="item in items">
        <h1>!</h1>
    </div>
</script>

<script id="search-query" type="text/x-template">
    <div class="project-list-search">
        <div class="wrap">
            <input id="query" type="text" class="project-list-search_val form-control" :placeholder="plh" v-model="query_model" @input="changeQuery()" autofocus>
        </div>
    </div>
</script>


<script id="custom-select" type="text/x-template">
    <div class="pln-select custom-pln-select dropdown project-filter-select pln-select__inited" v-click-outside="closeDropdown()">
        <div class="select-btn">
            <i class="icon-select"></i>
        </div>

        <span class="select-cont">{{ getActiveTitle() }}</span>

        <ul class="dropdown-menu">
            <li class="item" v-for="(option, index) in options" @click="select(option.code)" :class="{'active': isActive(option.code)}">
                <a href="#">{{ option.name }}</a>
            </li>
        </ul>
    </div>
</script>

<script id="top-campaigns" type="text/x-template">
    <div class="welcome-projects">
        <div class="welcome-projects_head">
            <div class="welcome-projects_title">
                {{ title }}
            </div>

            <a href="/search/projects" class="welcome-projects_all">
                <spring:message code="planeta-welcome.jsp.propertie.36" text="default text"/>
            </a>
        </div>

        <div class="welcome-projects_carousel">
            <campaigns-list :campaigns="campaigns"></campaigns-list>
        </div>
    </div>
</script>

<script id="reward-card" type="text/x-template">
    <div class="reward-card_i">
        <div class="reward-card_in">
            <div class="reward-card_cover">
                <img :src="getImageUrl(item.shareOrCampaignImageUrl)">
            </div>

            <div class="reward-card_cont">
                <div class="reward-card_project">
                    <div class="reward-card_project-lbl">
                        {{ $t('share_search.share_card.campaign') }}
                    </div>

                    <div class="reward-card_project-name">
                        <a :href="'https://${properties['application.host']}/campaigns/' + reward.campaignId" @click.prevent="clickCampaignName($event)">{{ item.campaignName }}</a>
                    </div>
                </div>

                <div class="reward-card_name">
                    <a :href="'https://${properties['application.host']}/campaigns/' + item.campaignId + '/donate/' + reward.shareId" @click.prevent="clickCard($event)">{{ item.name }}</a>
                </div>

                <div class="reward-card_meta">
                    <div class="reward-card_count">
                        <div class="reward-card_count-i">
                            <span class="reward-card_count-lbl">
                                {{ $t('share_search.share_card.bought') }}
                            </span>

                            <span class="reward-card_count-val">{{ item.purchaseCount }}</span>
                        </div>

                        <div class="reward-card_count-i" v-if="item.amount > 0">
                            <span class="reward-card_count-lbl">
                                {{ $t('share_search.share_card.left') }}
                            </span>
                            <span class="reward-card_count-val">{{ item.amount - item.purchaseCount }}</span>
                        </div>
                    </div>

                    <div class="reward-card_price">
                        {{ getHumanPrice(item.price) }} <span class="reward-card_cur">₽</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>

<!--
<script id="welcome-project-curators-template" type="text/x-jquery-template">
    <div class="project-partners_head">
        <spring:message code="planeta-welcome.jsp.propertie.37" text="default text"> </spring:message>
    </div>

    <div class="project-partners_link">
        <a href="/faq/article/29!paragraph203">
            <spring:message code="planeta-welcome.jsp.propertie.38" text="default text"> </spring:message>
        </a>
    </div>
    <div class="js-curators-list"></div>
</script>
-->

<script id="welcome-project-partners-template" type="text/x-jquery-template">
    <div class="project-partners_head">
        <spring:message code="planeta-welcome.jsp.planeta-partners" text="default text"> </spring:message>
    </div>

    <div class="js-partners-list"></div>
</script>

<script id="welcome-content-box-project-template" type="text/x-jquery-template">
    <%--<div class="project-welcome-main-slider-wrap">--%>
    <%--<div class="js-project-welcome-main-slider-wrap-container"></div>--%>
    <%--<div class="pwms-carousel-progress">--%>
    <%--<div class="pwms-carousel-progress-bar"></div>--%>
    <%--</div>--%>
    <%--<div class="pwms-carousel-arrows">--%>
    <%--<div class="pwms-prev"><i></i></div>--%>
    <%--<div class="pwms-next"><i></i></div>--%>
    <%--</div>--%>
    <%--</div>--%>

    <%--<top-campaigns title="<spring:message code="planeta-welcome.jsp.propertie.2" text="default text" />"></top-campaigns>--%>

    <div class="welcome-projects js-popular-projects">
        <div class="wrap">
            <div class="welcome-projects_head">
                <div class="welcome-projects_title">
                    <spring:message code="planeta-welcome.jsp.propertie.2" text="default text"/>
                </div>

                <a href="/search/projects" class="welcome-projects_all">
                    <spring:message code="planeta-welcome.jsp.propertie.36" text="default text"/>
                </a>
                <div class="welcome-projects_arrows">
                    <div class="welcome-projects_arrow prev"><svg width="16" height="16" viewBox="0 0 100 100"><path d="M69.1 12.4l-37 37.5 37.5 38c2.6 2.8 2.6 7.2-.1 10-2.7 2.7-7.1 2.8-9.9.1L17.2 55c-1.4-1.4-2.2-3.2-2.2-5.2 0-1.9.8-3.8 2.2-5.2l42.4-43c2.9-2.5 7.3-2.3 9.9.6 2.6 2.9 2.4 7.5-.4 10.2z" class="arrow"></path></svg></div>
                    <div class="welcome-projects_arrow next"><svg width="16" height="16" viewBox="0 0 100 100"><path d="M69.1 12.4l-37 37.5 37.5 38c2.6 2.8 2.6 7.2-.1 10-2.7 2.7-7.1 2.8-9.9.1L17.2 55c-1.4-1.4-2.2-3.2-2.2-5.2 0-1.9.8-3.8 2.2-5.2l42.4-43c2.9-2.5 7.3-2.3 9.9.6 2.6 2.9 2.4 7.5-.4 10.2z" class="arrow" transform="translate(100, 100) rotate(180) "></path></svg></div>
                </div>
            </div>
        </div>

        <div class="welcome-projects_carousel">
            <div class="wrap">
                <div class="welcome-projects_carousel-in"></div>
            </div>
        </div>
    </div>


    <div class="welcome-projects js-new-projects">
        <div class="wrap">
            <div class="welcome-projects_head">
                <div class="welcome-projects_title">
                    <spring:message code="planeta-welcome.jsp.recomended-projects" text="default text"/>
                </div>

                <a href="/search/projects" class="welcome-projects_all">
                    <spring:message code="planeta-welcome.jsp.propertie.36" text="default text"/>
                </a>
                <div class="welcome-projects_arrows">
                    <div class="welcome-projects_arrow prev"><svg width="16" height="16" viewBox="0 0 100 100"><path d="M69.1 12.4l-37 37.5 37.5 38c2.6 2.8 2.6 7.2-.1 10-2.7 2.7-7.1 2.8-9.9.1L17.2 55c-1.4-1.4-2.2-3.2-2.2-5.2 0-1.9.8-3.8 2.2-5.2l42.4-43c2.9-2.5 7.3-2.3 9.9.6 2.6 2.9 2.4 7.5-.4 10.2z" class="arrow"></path></svg></div>
                    <div class="welcome-projects_arrow next"><svg width="16" height="16" viewBox="0 0 100 100"><path d="M69.1 12.4l-37 37.5 37.5 38c2.6 2.8 2.6 7.2-.1 10-2.7 2.7-7.1 2.8-9.9.1L17.2 55c-1.4-1.4-2.2-3.2-2.2-5.2 0-1.9.8-3.8 2.2-5.2l42.4-43c2.9-2.5 7.3-2.3 9.9.6 2.6 2.9 2.4 7.5-.4 10.2z" class="arrow" transform="translate(100, 100) rotate(180) "></path></svg></div>
                </div>
            </div>
        </div>

        <div class="welcome-projects_carousel">
            <div class="wrap">
                <div class="welcome-projects_carousel-in"></div>
            </div>
        </div>
    </div>
</script>

<script id="promo-campaign-item-template" type="text/x-jquery-template">
    <div class="pwms-img">
        <a class="js-carouse-button"
           href="{{if window.Vkapp}}//{{= workspace.serviceUrls.mainHost}}/vkapp{{/if}}/campaigns/{{= webCampaignAlias}}?clickFromSource=start_carouse">
            <img src="{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.PROJECT_ITEM_MAIN_SLIDER)}}"
                 alt="{{= name}}">
        </a>
    </div>
    <div class="pwms-wrap">
        <div class="pwms-cont">
            <div class="pwms-name hyphenate">
                <a class="js-carouse-button"
                   href="{{if window.Vkapp}}//{{= workspace.serviceUrls.mainHost}}/vkapp{{/if}}/campaigns/{{= webCampaignAlias}}?clickFromSource=start_carouse">
                    {{html nameHtml}}</a>
            </div>
            <div class="pwms-text">{{html shortDescriptionHtml}}</div>
        </div>

        <div class="pwms-footer">
            <div class="pwms-info cf">
                <div class="pwms-info-i">
                    <div class="pwms-info-label"><spring:message code="planeta-welcome.jsp.propertie.5"
                                                                 text="default text"> </spring:message></div>
                    <div class="pwms-info-value">{{= StringUtils.humanNumber(collectedAmount)}}<span
                            class="b-rub">Р</span></div>
                </div>
                <div class="pwms-info-i">
                    {{if timeFinish && !targetAmount}}
                    {{if DateUtils.getDaysTo(timeFinish) > 0}}
                    <div class="pwms-info-label"><spring:message code="planeta-welcome.jsp.propertie.6"
                                                                 text="default text"> </spring:message></div>
                    <div class="pwms-info-value">{{= DateUtils.getDaysTo(timeFinish)}}&nbsp;<spring:message
                            code="planeta-welcome.jsp.propertie.7" text="default text"> </spring:message></div>
                    {{else DateUtils.getHoursTo(timeFinish) > 0}}
                    <div class="pwms-info-label"><spring:message code="planeta-welcome.jsp.propertie.8"
                                                                 text="default text"> </spring:message></div>
                    <div class="pwms-info-value">{{= DateUtils.getHoursTo(timeFinish)}}&nbsp;<spring:message
                            code="planeta-welcome.jsp.propertie.9" text="default text"> </spring:message></div>
                    {{/if}}
                    {{else}}
                    <div class="pwms-info-label"><spring:message code="planeta-welcome.jsp.propertie.10"
                                                                 text="default text"> </spring:message></div>
                    <div class="pwms-info-value">{{= StringUtils.humanNumber(targetAmount)}}<span class="b-rub">Р</span>
                    </div>
                    {{/if}}
                </div>
            </div>
            <div class="pwms-action">
                <a class="flat-btn js-carouse-button"
                   href="{{if window.Vkapp}}//{{= workspace.serviceUrls.mainHost}}/vkapp{{/if}}/campaigns/{{= webCampaignAlias}}?clickFromSource=start_carouse"><spring:message
                        code="planeta-welcome.jsp.propertie.11" text="default text"> </spring:message></a>
            </div>
        </div>
    </div>
</script>

<script id="welcome-project-about-template-in" type="text/x-jquery-template">
    {{if !isMobileDev}}
    <div class="back-video-wrap">
        <video class="back-video" id="back-video" autoplay loop muted="">
            <source src='{{= workspace.staticNodesService.getResourceUrl("/images/project/rainbow.mp4")}}'
                    type="video/mp4">
            <source src='{{= workspace.staticNodesService.getResourceUrl("/images/project/rainbow.webm")}}'
                    type="video/webm">
        </video>
    </div>
    {{/if}}
    <div class="project-about_cont">
        <div class="wrap">
            <div class="project-about_body">
                <div class="project-about_head">
                    <h1><spring:message code="planeta-welcome.jsp.propertie.30"
                                        text="default text"> </spring:message></h1>
                </div>
                <div class="project-about_post-head">
                    <spring:message code="planeta-welcome.jsp.propertie.31" text="default text"> </spring:message>
                </div>
                <div class="project-about_text">
                    <spring:message code="planeta-welcome.jsp.propertie.31.1" text="default text"> </spring:message>
                </div>

                <div class="project-about_create js-header-special-project">
                    <div class="pwa-create js-create-button-block">
                        <a href="/funding-rules" class="pwa-create-link js-create-button"><spring:message
                                code="planeta-welcome.jsp.propertie.35" text="default text"> </spring:message></a>
                    </div>
                </div>

                <div class="project-about_links">
                    <div class="project-about_links_cont">
                        <div class="project-about_links_i">
                            <spring:message code="planeta-welcome.jsp.propertie.27"
                                            text="default text"> </spring:message>
                            <a href="/welcome/one-hundred-millions.html" class="project-about_links_link">{{=
                                StringUtils.humanNumber(campaignsTotalStats)}}&nbsp;<span class="b-rub">Р</span></a>
                        </div>
                        <div class="project-about_links_i">
                            <a href="/stories" class="project-about_links_link"><spring:message
                                    code="planeta-welcome.jsp.propertie.12" text="default text"> </spring:message></a>
                        </div>
                        <div class="project-about_links_i">
                            <span class="project-about_links_link dotted js-how-create"
                                  data-event-click="onHowCreateClicked"><spring:message
                                    code="planeta-welcome.jsp.propertie.13"
                                    text="default text"> </spring:message></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>

<script id="select-draft-campaign-modal-template" type="text/x-jquery-template">
    <div class="ctb-action-modal" style=" position: static; display: block; margin: 0;">
        <div class="ctb-action-modal-title"><spring:message code="planeta-welcome.jsp.propertie.14"
                                                            text="default text"> </spring:message></div>
        <div class="ctb-action-modal-draft scroll">
            <div class="js-draft-campaign-list modal-scroll-content"></div>
        </div>
        <a class="flat-btn" href="/funding-rules" data-event-click="onCreateClicked"> <spring:message
                code="planeta-welcome.jsp.propertie.32" text="default text"> </spring:message> </a>
        <a class="ctb-action-modal-cancel cancel"><spring:message code="planeta-welcome.jsp.propertie.15"
                                                                  text="default text"> </spring:message></a>
    </div>
</script>


<script id="draft-campaign-item-template" type="text/x-jquery-template">
    <div class="ctbam-draft-item-title">
        {{if name}}
        {{= name}}
        {{else}}
        *<spring:message code="campaign.c.l" text="default text"> </spring:message> {{= campaignId}}*
        {{/if}}
    </div>
    <div class="ctbam-draft-item-date"><spring:message code="planeta-welcome.jsp.propertie.34"
                                                       text="default text"> </spring:message> {{=
        DateUtils.formatDate(timeUpdated, "D MMMM YYYY")}}
    </div>
</script>

<script id="top-campaigns-item-inner-template" type="text/x-jquery-template">
    <span class="project-card-cover-wrap">
        <span class="project-card-cover-cont">
            <img class="project-card-cover lazy lazy-blur" width="370" height="225" alt="{{= name}}" data-lazy-effect="show"
                 src="{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.PROJECT_ITEM_BLUR)}}"
                 {{if (projectCardThumb)}}
                 data-original="{{if imageUrl}}{{= ImageUtils.getThumbnailUrl(imageUrl, projectCardThumb)}}{{/if}}"
                 {{else}}
                 data-original="{{if imageUrl}}{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.PROJECT_ITEM)}}{{/if}}"
                 {{/if}}>
        </span>
        {{if status == 'FINISHED'}}
        <span class="project-card-state {{if targetStatus == 'SUCCESS'}}project-success{{else}}project-fail{{/if}}">
            <span class="project-card-state-lbl">
                {{if targetStatus == 'SUCCESS'}}
                    <spring:message code="planeta-welcome.jsp.propertie.16" text="default text"/>
                {{else}}
                    <spring:message code="planeta-welcome.jsp.propertie.17" text="default text"/>
                {{/if}}
            </span>
        </span>
        {{/if}}
        <span class="project-card-badges">
            {{if isPurchased}}
            <span class="project-card-supported" data-tooltip-text="Вы поддержали">
                <span class="project-card-supported-lbl">Вы поддержали</span>
            </span>
            {{/if}}
        </span>

        {{if status != 'FINISHED'}}
        <span class="project-card-show">
            <span class="project-card-show-text">
                Поддержать проект
            </span>
        </span>
        {{/if}}
    </span>

    <span class="project-card-text">
        <span class="project-card-name hyphenate">{{html nameHtml}}</span>
        <span class="project-card-descr">{{html shortDescriptionHtml}}</span>
    </span>

    <span class="project-card-footer">
        {{if status == 'FINISHED' && !targetAmount}}
            <span class="project-card-no-progress"></span>
            <span class="pci-value project-card-finish-sum">{{= StringUtils.humanNumber(collectedAmount)}} ₽</span>
        {{else timeFinish && !targetAmount}}
            <span class="project-card-no-progress"></span>
            <span class="project-card-info">
                <span class="project-card-info-i">
                    <span class="pci-label">
                        <spring:message code="planeta-welcome.jsp.propertie.18" text="default text"/>
                    </span>
                    <span class="pci-value">
                        {{= StringUtils.humanNumber(collectedAmount)}} ₽
                    </span>
                </span>
                <span class="project-card-info-i">
                    {{if DateUtils.getDaysTo(timeFinish) > 0}}
                        <span class="pci-label">
                            <spring:message code="planeta-welcome.jsp.propertie.6" text="default text"/>
                        </span>
                        <span class="pci-value"> {{= DateUtils.getDaysTo(timeFinish)}}&nbsp;<spring:message
                                code="planeta-welcome.jsp.propertie.7" text="default text"/></span>
                    {{else DateUtils.getHoursTo(timeFinish) > 0}}
                        <span class="pci-label">
                            <spring:message code="planeta-welcome.jsp.propertie.8" text="default text"/>
                        </span>
                        <span class="pci-value"> {{= DateUtils.getHoursTo(timeFinish)}}&nbsp;<spring:message
                                code="planeta-welcome.jsp.propertie.9" text="default text"/></span>
                    {{/if}}
                </span>
            </span>
        {{else targetAmount}}
            <span class="project-card-progress{{if collectedAmount >= targetAmount}} over-progress{{else status == 'FINISHED' && targetStatus === 'FAIL'}} finish-fail{{/if}}">
                <span class="pcp-bar" style="width:{{= (100*collectedAmount/targetAmount) % 100}}%;"></span>
            </span>
            {{if status == "FINISHED"}}
                <span class="pci-value project-card-finish-sum">{{= StringUtils.humanNumber(collectedAmount)}} ₽</span>
                <span class="pci-label project-card-finish">
                    <spring:message code="planeta-welcome.jsp.propertie.27" text="default text"/>
                    {{= Math.floor(100*collectedAmount/targetAmount)}}%
                </span>
            {{else}}
                <span class="project-card-info">
                    <span class="project-card-info-i">
                        <span class="pci-value">
                            {{= Math.floor(100*collectedAmount/targetAmount)}}%
                        </span>
                        <span class="pci-label">
                            <spring:message code="project.status.active" text="default text"/>
                        </span>
                    </span>

                    <span class="project-card-info-i">
                        <span class="pci-value">
                            {{= StringUtils.humanNumber(collectedAmount)}} <span class="b-rub">Р</span>
                        </span>
                        <span class="pci-label">
                            <spring:message code="planeta-welcome.jsp.propertie.18" text="default text"/>
                        </span>
                    </span>
                </span>

                {{if showButton}}
                    <span class="project-card-link-to-project">
                        <span class="btn btn-primary btn-lg btn-block">
                            <spring:message code="planeta-welcome.jsp.propertie.24" text="default text"/>
                        </span>
                    </span>
                {{/if}}
            {{/if}}
        {{/if}}
    </span>
</script>

<script id="rewards-list" type="text/x-template">
    <div class="reward-card">
        <reward-card v-for="reward in rewards" :reward="reward" page="welcome"></reward-card>
    </div>
</script>

<script id="welcome-banner-carousel" type="text/x-template">
    <div class="project-hero-carousel">
        <div class="project-hero-carousel_list">
            <div class="carousel-cell" v-if="!isAutorized()">
                <div class="project-hero-carousel_i

                            hero-slide-main

                            hero-block-width-ld-12
                            hero-block-width-tl-12
                            hero-block-width-tp-12
                            hero-block-width-pl-12
                            hero-block-width-pp-12

                            hero-block-position-ld-center
                            hero-block-position-tl-center
                            hero-block-position-tp-center
                            hero-block-position-pl-center
                            hero-block-position-pp-center

                            hero-block-align-ld-center
                            hero-block-align-tl-center
                            hero-block-align-tp-center
                            hero-block-align-pl-center
                            hero-block-align-pp-center

                            hero-head-size-ld-64
                            hero-head-size-tl-64
                            hero-head-size-tp-40
                            hero-head-size-pl-40
                            hero-head-size-pp-20

                            hero-text-display-ld-b
                            hero-text-display-tl-b
                            hero-text-display-tp-b
                            hero-text-display-pl-b
                            hero-text-display-pp-b

                            hero-image-m-display-ld-n
                            hero-image-m-display-tl-n
                            hero-image-m-display-tp-n
                            hero-image-m-display-pl-n
                            hero-image-m-display-pp-n

                            hero-image-type-ld-cover
                            hero-image-type-tl-cover
                            hero-image-type-tp-cover
                            hero-image-type-pl-cover
                            hero-image-type-pp-cover

                            hero-cover-bg-size-ld-contain
                            hero-cover-bg-size-tl-cover
                            hero-cover-bg-size-tp-cover
                            hero-cover-bg-size-pl-cover
                            hero-cover-bg-size-pp-cover" style="background:#e5e7f2;">

                    <div class="project-hero-carousel_cover hero-cover">
                        <div class="hero-slide-main_cover hero-figures-cover-1">
                            <div class="hero-slide-main_cover-in">
                                <div class="hero-slide-main_figure hero-figure-1 hero-figure-square hero-figure-size-0 hero-figure-color-1 hero-figure-rotate-1"><div></div></div>
                                <div class="hero-slide-main_figure hero-figure-2 hero-figure-square hero-figure-size-3 hero-figure-color-2 hero-figure-rotate-5"><div></div></div>
                                <div class="hero-slide-main_figure hero-figure-3 hero-figure-square hero-figure-size-1 hero-figure-color-3 hero-figure-rotate-3"><div></div></div>
                                <div class="hero-slide-main_figure hero-figure-4 hero-figure-square hero-figure-size-1 hero-figure-color-3 hero-figure-rotate-3"><div></div></div>
                                <div class="hero-slide-main_figure hero-figure-5 hero-figure-circle hero-figure-size-5 hero-figure-color-4 hero-figure-rotate-0"><div></div></div>
                                <div class="hero-slide-main_figure hero-figure-6 hero-figure-circle hero-figure-size-9 hero-figure-color-4 hero-figure-rotate-0"><div></div></div>
                                <div class="hero-slide-main_figure hero-figure-13 hero-figure-square hero-figure-size-4 hero-figure-color-5 hero-figure-rotate-2"><div></div></div>
                            </div>
                        </div>

                        <div class="hero-slide-main_cover hero-figures-cover-2">
                            <div class="hero-slide-main_cover-in">
                                <div class="hero-slide-main_figure hero-figure-7 hero-figure-square hero-figure-size-0 hero-figure-color-1 hero-figure-rotate-4"><div></div></div>
                                <div class="hero-slide-main_figure hero-figure-8 hero-figure-square hero-figure-size-5 hero-figure-color-1 hero-figure-rotate-4"><div></div></div>
                                <div class="hero-slide-main_figure hero-figure-9 hero-figure-square hero-figure-size-4 hero-figure-color-5 hero-figure-rotate-5"><div></div></div>
                                <div class="hero-slide-main_figure hero-figure-10 hero-figure-square hero-figure-size-05 hero-figure-color-3 hero-figure-rotate-1"><div></div></div>
                                <div class="hero-slide-main_figure hero-figure-11 hero-figure-circle hero-figure-size-4 hero-figure-color-4 hero-figure-rotate-0"><div></div></div>
                            </div>
                        </div>

                        <div class="hero-slide-main_cover hero-figures-cover-3">
                            <div class="hero-slide-main_cover-in">
                                <div class="hero-slide-main_figure hero-figure-12 hero-figure-square hero-figure-size-5 hero-figure-color-1 hero-figure-rotate-4"><div></div></div>
                                <div class="hero-slide-main_figure hero-figure-14 hero-figure-square hero-figure-size-5 hero-figure-color-7 hero-figure-rotate-5"><div></div></div>
                                <div class="hero-slide-main_figure hero-figure-15 hero-figure-square hero-figure-size-05 hero-figure-color-7 hero-figure-rotate-2"><div></div></div>
                                <div class="hero-slide-main_figure hero-figure-16 hero-figure-circle hero-figure-size-9 hero-figure-color-4 hero-figure-rotate-0"><div></div></div>
                                <div class="hero-slide-main_figure hero-figure-17 hero-figure-triangle hero-figure-size-7 hero-figure-color-8 hero-figure-rotate-5"><div></div></div>
                            </div>
                        </div>

                        <div class="hero-slide-main_cover hero-figures-cover-4">
                            <div class="hero-slide-main_cover-in">
                                <div class="hero-slide-main_figure hero-figure-18 hero-figure-circle hero-figure-size-1 hero-figure-color-4 hero-figure-rotate-0"><div></div></div>
                                <div class="hero-slide-main_figure hero-figure-19 hero-figure-square hero-figure-size-9 hero-figure-color-8 hero-figure-rotate-1"><div></div></div>
                                <div class="hero-slide-main_figure hero-figure-20 hero-figure-square hero-figure-size-4 hero-figure-color-2 hero-figure-rotate-5"><div></div></div>
                                <div class="hero-slide-main_figure hero-figure-21 hero-figure-square hero-figure-size-5 hero-figure-color-3 hero-figure-rotate-3"><div></div></div>
                                <div class="hero-slide-main_figure hero-figure-22 hero-figure-circle hero-figure-size-4 hero-figure-color-4 hero-figure-rotate-0"><div></div></div>
                                <div class="hero-slide-main_figure hero-figure-23 hero-figure-circle hero-figure-size-7 hero-figure-color-4 hero-figure-rotate-0"><div></div></div>
                                <div class="hero-slide-main_figure hero-figure-24 hero-figure-triangle hero-figure-size-8 hero-figure-color-1 hero-figure-rotate-2"><div></div></div>
                            </div>
                        </div>
                    </div>

                    <div class="project-hero-carousel_cont">
                        <div class="project-hero-carousel_wrap">
                            <div class="project-hero-carousel_block hero-block">
                                <div class="project-hero-carousel_head hero-head">
                                    {{ $t('welcome.main_slider.public_finance') }}
                                </div>

                                <div class="project-hero-carousel_text hero-text">
                                    <div class="hero-slide-main_top-text">
                                        {{ $t('welcome.main_slider.crowdfunding_in_russia') }}
                                    </div>

                                    <div class="hero-slide-main_text">
                                        {{ $t('welcome.main_slider.description') }}
                                    </div>
                                </div>

                                <div class="project-hero-carousel_action hero-action">
                                    <div class="hero-slide-main_stat">
                                        <div class="hero-slide-main_stat-i">
                                            <div class="hero-slide-main_stat-val">
                                                {{ humanNumber(stats.users) }}
                                            </div>
                                            <div class="hero-slide-main_stat-lbl">
                                                {{ $t('welcome.main_slider.already_supported') }}
                                            </div>
                                        </div>

                                        <div class="hero-slide-main_stat-i">
                                            <div class="hero-slide-main_stat-val">
                                                {{ humanNumber(stats.shares) }}
                                            </div>
                                            <div class="hero-slide-main_stat-lbl">
                                                {{ $t('welcome.main_slider.rare_rewards') }}
                                            </div>
                                        </div>

                                        <div class="hero-slide-main_stat-i">
                                            <div class="hero-slide-main_stat-val">
                                                {{ humanNumber(stats.activeCampaigns) }}
                                            </div>
                                            <div class="hero-slide-main_stat-lbl">
                                                {{ $t('welcome.main_slider.active_campaigns') }}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="hero-slide-main_btn">
                                        <a href="/search/projects" class="btn btn-nd-primary">
                                            {{ $t('welcome.main_slider.choose_your') }}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div v-for="b in banners" v-html="b.html" class="carousel-cell"></div>
        </div>

        <div class="project-hero-carousel_progress">
            <div class="project-hero-carousel_bar"></div>
        </div>
    </div>
</script>

<script id="planeta-welcome-template" type="text/x-jquery-template">
    <div id="welcome-carousel">
        <welcome-banner-carousel></welcome-banner-carousel>
    </div>

    <div class="wrap">
        <div class="js-banner-container-top projects-welcome-banner-top"></div>
    </div>
    <div class="js-projects-welcome-box-container"></div>
    <div class="welcome-rewards">
        <div class="wrap">
            <div class="welcome-rewards_head">
                <a href="/search/shares" class="welcome-rewards_all">
                    <spring:message code="planeta-welcome.jsp.propertie.36" text="default text"/>
                </a>
                <div class="welcome-rewards_title">
                    <spring:message code="planeta-welcome.jsp.planeta-rewards" text="default text"/>
                </div>
            </div>
            <div class="welcome-rewards_list">
                <rewards-list></rewards-list>
            </div>
        </div>
    </div>
    <div class="welcome-planeta-sections welcome-planeta-sections__list-5">
        <div class="wrap">
            <div class="welcome-planeta-sections_head">
                <spring:message code="planeta-welcome.jsp.planeta-projects" text="default text"/>
            </div>

            <div class="welcome-planeta-sections_list">
                <div class="welcome-planeta-sections_i">
                    <div class="welcome-planeta-sections_in">
                        <a href="https://${properties['school.application.host']}"
                           class="welcome-planeta-sections_link">
                            <span class="welcome-planeta-sections_ico">
                                <svg width="54" height="54" viewBox="0 0 32 32"><path d="M18.266 12.096a1.667 1.667 0 1 1 0 3.333 1.667 1.667 0 0 1 0-3.333m-5.158 1.633c-1.729.105-3.457.861-4.474 2.285-.138.195-.338.472-.474.933.285.035.687.112.923.152.622.105 1.352.185 2.047.596a19.143 19.143 0 0 0-.34 1.527.689.689 0 0 0-.009.119c.345.173.802.367 1.157.721.354.354.548.812.72 1.157.04 0 .08-.002.12-.009a19.143 19.143 0 0 0 1.526-.341c.412.696.491 1.425.596 2.048.04.235.118.637.153.922.46-.135.738-.335.932-.474 1.425-1.016 2.18-2.745 2.286-4.473 1.086-.714 2.177-1.48 2.871-2.545 1.317-2.022 1.425-4.425 1.425-6.915-2.49 0-4.893.109-6.914 1.425-1.066.694-1.831 1.786-2.545 2.872m12.7 12.076c-4.003 4.003-9.845 5.038-14.808 3.122-1.183-.531-4.242-2.467.291-7.001a.87.87 0 1 0-1.229-1.23c-4.437 4.437-6.387 1.6-6.966.368-1.95-4.977-.921-10.852 3.099-14.873C10.911 1.475 18.18.87 23.556 4.369a3.192 3.192 0 0 0 .743 3.331 3.195 3.195 0 0 0 3.332.744c3.499 5.375 2.894 12.644-1.823 17.361m3.484-18.714a3.195 3.195 0 0 0-.466-3.917 3.196 3.196 0 0 0-3.917-.467C18.688-1.47 10.184-.815 4.686 4.683c-6.248 6.248-6.248 16.382 0 22.63 6.248 6.249 16.383 6.249 22.631 0 5.498-5.498 6.153-14.001 1.975-20.222" fill="#F2AD0C"/></svg>
                            </span>
                            <span class="welcome-planeta-sections_name">
                                <spring:message code="planeta-welcome.jsp.planeta-school" text="default text"/>
                            </span>
                        </a>
                    </div>
                </div>

                <div class="welcome-planeta-sections_i">
                    <div class="welcome-planeta-sections_in">
                        <a href="https://${properties['charity.application.host']}"
                           class="welcome-planeta-sections_link">
                            <span class="welcome-planeta-sections_ico">
                                <svg width="54" height="54" viewBox="0 0 32 32"><path d="M17.129 13.138c-.626-.341-1.358-.445-2.298-.323a.128.128 0 0 1-.142-.109.127.127 0 0 1 .11-.142c.921-.12 1.66-.033 2.301.273a12.56 12.56 0 0 0-.413-2.139c-.001-.002-.004-.002-.005-.003-.564-.423-2.014-.38-2.03-.38a.128.128 0 0 1-.13-.123.126.126 0 0 1 .122-.13c.056-.002 1.226-.036 1.935.284a8.868 8.868 0 0 0-1.084-2.252.126.126 0 0 1 .033-.176.126.126 0 0 1 .176.033c.568.834.966 1.747 1.245 2.65.42-.763 1.275-1.151 1.316-1.169a.126.126 0 0 1 .167.064.126.126 0 0 1-.064.167c-.01.005-1.012.462-1.309 1.314.198.724.322 1.433.399 2.084.621-.737 1.574-1.269 1.619-1.294a.127.127 0 0 1 .122.222c-.011.006-1.138.635-1.703 1.437.145 1.557.023 2.696.02 2.719-.009.063-.06.148-.123.148l-.218-.011c-.069-.01-.096-.087-.086-.157.003-.024.186-1.3.04-2.987m8.677 12.667a13.804 13.804 0 0 1-5.163 3.264c-1.326.385-3.223.445-3.852-1.996a29.634 29.634 0 0 0-.211-2.063c.003-.301.017-.62.044-.962.085-.612.275-1.252.673-1.705.349-.397.797-.607 1.39-.684.325-.033.446.06.492.263.171.748.717 1.241 1.561 1.311 1.001.084 2.095-1.564 3.834-2.034-.918-1.172-2.432-1.695-3.434-1.778-.843-.07-1.804.475-1.919 1.203-.026.112-.072.405-.589.446-.01.001-.005.001.01.001-.866.07-1.547.334-2.042.898a2.996 2.996 0 0 0-.427.637c-.265-1.542-.088-2.021.303-2.74.57-1.047.882-2.011 1.028-2.874l.004-.013c.197-.908.489-.86 1.064-1.264 1.227-.862 2.019-3.049 1.281-4.756-.875-2.027-3.943-5.307-6.463-5.408 1.507 3.904-.314 4.894-.021 7.082.283 2.115 1.493 3.457 2.75 3.445.554-.012.836.162.811.834a5.124 5.124 0 0 1-.177.689c-.243.721-.763 1.369-.992 1.813-.187.36-.373.627-.516.927l-.041-.103c-.54-1.288-1.503-1.834-2.568-2.041l-.25-.047c-.228-.07-.263-.407-.267-.422-.098-.65-.376-.947-.983-1.08-.719-.157-2.42.642-2.696.999.957.162 1.541 1.504 2.261 1.662.606.132.917-.133 1.278-.683.079-.12.325-.109.409-.111l.162.031c1.969.382 2.412 3.111 2.482 3.656l.01.089.001.005c.029.229.07.482.126.767.07.357.122.702.161 1.034.464 5.492-2.316 5.466-4.043 4.946a13.807 13.807 0 0 1-5.092-3.238C.78 20.39.78 11.608 6.195 6.193 10.908 1.48 18.172.87 23.546 4.362a3.201 3.201 0 0 0 4.091 4.091c3.492 5.374 2.883 12.639-1.831 17.352m3.492-18.709a3.2 3.2 0 0 0-4.395-4.395C18.684-1.471 10.182-.811 4.686 4.685c-6.248 6.248-6.248 16.381 0 22.629 6.248 6.248 16.381 6.248 22.629 0 5.496-5.496 6.156-13.998 1.983-20.218" fill="#D60B52"/></svg>
                            </span>
                            <span class="welcome-planeta-sections_name">
                                <spring:message code="planeta-welcome.jsp.planeta-charity" text="default text"/>
                            </span>
                        </a>
                    </div>
                </div>

                <div class="welcome-planeta-sections_i">
                    <div class="welcome-planeta-sections_in">
                        <a href="https://${properties['shop.application.host']}"
                           class="welcome-planeta-sections_link">
                            <span class="welcome-planeta-sections_ico">
                                <svg width="54" height="54" viewBox="0 0 32 32"><path d="M15.184 19.655c.033-.033.084-.091.148-.183v3.809h-3.727a2.886 2.886 0 0 1-2.885-2.886v-3.828h3.859a1.59 1.59 0 0 0-.275.208 2.039 2.039 0 0 0 0 2.88c.385.385.896.596 1.44.596.544 0 1.055-.211 1.44-.596m1.808-3.088h6.289v3.828a2.886 2.886 0 0 1-2.885 2.886h-3.948v-5.686c.29.259.629.505 1.019.738a8.737 8.737 0 0 0 1.546.725l.351-1.06c-.504-.167-1.647-.678-2.372-1.431M11.605 8.72h3.727v5.37a6.624 6.624 0 0 0-1.019-.738 8.737 8.737 0 0 0-1.546-.725l-.352 1.06c.006.002.65.219 1.345.635.438.263.963.648 1.308 1.129H8.72v-3.846a2.885 2.885 0 0 1 2.885-2.885m11.676 2.885v3.846h-3.959a1.57 1.57 0 0 0 .274-.208c.385-.385.597-.896.597-1.44 0-.544-.212-1.055-.597-1.44a2.02 2.02 0 0 0-1.439-.596 2.02 2.02 0 0 0-1.44.596 1.863 1.863 0 0 0-.269.374V8.72h3.948a2.885 2.885 0 0 1 2.885 2.885m-4.474 2.848c-.178.178-.985.502-2.099.798.296-1.113.621-1.921.798-2.099a.918.918 0 0 1 1.301.001.91.91 0 0 1 .269.65.913.913 0 0 1-.269.65m-5.063 4.682a.92.92 0 0 1-.65-1.57c.178-.178.985-.502 2.099-.798-.297 1.114-.621 1.921-.799 2.098a.91.91 0 0 1-.65.27m12.062 6.671c-5.415 5.414-14.197 5.414-19.611 0C.78 20.391.78 11.609 6.195 6.194 10.908 1.481 18.172.871 23.547 4.363a3.199 3.199 0 0 0 4.09 4.091c3.492 5.374 2.883 12.639-1.831 17.352m3.492-18.709a3.2 3.2 0 0 0-4.395-4.395C18.684-1.471 10.182-.81 4.686 4.686c-6.248 6.248-6.248 16.381 0 22.628 6.248 6.248 16.381 6.248 22.629 0 5.496-5.496 6.156-13.997 1.983-20.217" fill="#0098D9"/></svg>
                            </span>
                            <span class="welcome-planeta-sections_name">
                                <spring:message code="planeta-welcome.jsp.planeta-shop" text="default text"/>
                            </span>
                        </a>
                    </div>
                </div>

                <div class="welcome-planeta-sections_i">
                    <div class="welcome-planeta-sections_in">
                        <a href="https://${properties['biblio.application.host']}"
                           class="welcome-planeta-sections_link">
                            <span class="welcome-planeta-sections_ico">
                                <svg width="54" height="54" viewBox="0 0 32 32"><path d="M24.351 8.142H10.079c-.724 0-1.256-.242-1.63-.736-.327-.435-.519-1.055-.548-1.754v-.11c0-.028 0-.053-.003-.082.003-.846.252-1.583.689-2.024.359-.362.86-.547 1.492-.547h14.269a.943.943 0 1 0 0-1.889H10.079c-1.139 0-2.118.381-2.833 1.105-.812.821-1.252 2.043-1.237 3.437 0 .047 0 .095.004.142v19.468c0 1.171.333 2.159.966 2.865.718.799 1.763 1.205 3.1 1.205h14.272a.943.943 0 0 0 .944-.944V9.086a.943.943 0 0 0-.944-.944zm-.944 19.188H10.079c-.78 0-1.35-.195-1.696-.579-.4-.444-.482-1.114-.482-1.599V9.436c.614.393 1.351.595 2.178.595h13.328V27.33zM10.02 4.62a.943.943 0 1 0 0 1.888h13.311a.942.942 0 0 0 .944-.944.943.943 0 0 0-.944-.944H10.02z" fill="#1A8CFF"/><path d="M18.589 18.278c-1.165-.598-2.537-.819-4.47-.819h-1.857v-3.982c0-.368.207-.459.783-.459h4.511l.431 1.322c.038.126.117.182.236.182h.954c.17 0 .249-.069.249-.207.038-1.231.053-2.153.053-2.773 0-.174-.104-.252-.314-.252h-7.268c-1.373 0-2.053.437-2.053 1.485v4.687H8.399s-.188.066-.188.189v.976s.047.085.157.126c.104.041 1.388.472 1.473.5v6.352c0 .026.003.051.006.076v6.229l1.165-1.164 1.228 1.164v-5.958h1.328c2.04 0 3.425-.151 4.171-.45 1.766-.702 2.641-2.024 2.641-3.948 0-1.599-.602-2.691-1.791-3.276zm-3.897 6.005h-2.43V19.14h2.392c2.077 0 3.189.853 3.189 2.474 0 1.69-1.178 2.669-3.151 2.669z"/></svg>
                            </span>
                            <span class="welcome-planeta-sections_name">
                                <spring:message code="planeta-welcome.jsp.planeta-bibliorodina" text="default text"/>
                            </span>
                        </a>
                    </div>
                </div>

                <div class="welcome-planeta-sections_i">
                    <div class="welcome-planeta-sections_in">
                        <a href="https://${properties['promo.application.host']}/campus" class="welcome-planeta-sections_link">
                        <span class="welcome-planeta-sections_ico">
                            <svg width="54" height="54" viewBox="0 0 32 32"><g fill="#163870"><path d="M29.86 5.564a3.323 3.323 0 1 1-6.646 0 3.323 3.323 0 0 1 6.646 0z"/><path d="M15.98 31.959C7.154 31.959 0 24.805 0 15.98 0 7.154 7.154 0 15.98 0c8.825 0 15.979 7.154 15.979 15.98 0 8.825-7.154 15.979-15.979 15.979zm0-2.038c7.699 0 13.941-6.242 13.941-13.941 0-7.7-6.242-13.942-13.941-13.942-7.7 0-13.942 6.242-13.942 13.942 0 7.699 6.242 13.941 13.942 13.941z"/><path d="M24.132 11.394a1.316 1.316 0 0 0-.652-.469c.021.265 0 .469-.061.632l-3.281 10.782a.76.76 0 0 1-.388.448 1.142 1.142 0 0 1-.611.163H9.07c-.876 0-1.406-.244-1.569-.754-.082-.204-.062-.346.02-.469.082-.102.224-.163.408-.163h9.477c.673 0 1.142-.122 1.407-.367.265-.244.509-.815.774-1.671l2.996-9.885c.163-.53.102-1.019-.203-1.427-.286-.408-.714-.611-1.244-.611H12.82c-.102 0-.285.04-.55.101l.02-.04a2.593 2.593 0 0 0-.509-.061.665.665 0 0 0-.387.122.888.888 0 0 0-.286.265 1.137 1.137 0 0 0-.204.346c-.061.143-.122.265-.183.388-.041.122-.102.244-.163.387a1.548 1.548 0 0 1-.183.326 1.462 1.462 0 0 1-.184.224c-.081.102-.143.184-.204.245-.061.081-.081.142-.102.204-.02.061 0 .163.021.285.02.122.04.224.04.285-.02.286-.122.632-.305 1.06-.163.428-.326.734-.469.917-.02.041-.102.123-.245.245a1.33 1.33 0 0 0-.244.326c-.021.041-.041.143 0 .306.02.163.041.285.02.346-.02.245-.122.571-.265.979-.163.407-.306.733-.469.998-.02.041-.081.123-.183.245a.838.838 0 0 0-.183.306c-.021.061-.021.163 0 .305.02.143.02.245 0 .326a5.67 5.67 0 0 1-.327.999 9.17 9.17 0 0 1-.489.999 1.637 1.637 0 0 1-.183.265 1.549 1.549 0 0 0-.184.265c-.04.081-.081.143-.081.224 0 .041 0 .122.041.204.02.102.04.183.04.244 0 .102-.02.245-.04.408-.021.163-.041.265-.041.285-.163.429-.163.897.02 1.386.204.571.571 1.04 1.08 1.427.53.387 1.06.591 1.631.591h10.089c.469 0 .917-.163 1.345-.468a2.28 2.28 0 0 0 .836-1.183l2.996-9.885c.163-.53.082-.999-.204-1.406zm-11.617.02l.224-.693a.364.364 0 0 1 .183-.245.49.49 0 0 1 .286-.101h6.644c.102 0 .184.04.224.101.041.062.041.143.021.245l-.224.693a.369.369 0 0 1-.184.245.486.486 0 0 1-.285.102h-6.645c-.102 0-.183-.041-.224-.102-.041-.062-.041-.143-.02-.245zm-.897 2.792l.224-.693a.363.363 0 0 1 .183-.244.49.49 0 0 1 .286-.102h6.644c.102 0 .184.041.225.102.04.061.04.142.02.244l-.224.693a.366.366 0 0 1-.184.245.486.486 0 0 1-.285.102h-6.645c-.101 0-.183-.041-.224-.102a.25.25 0 0 1-.02-.245z"/></g></svg>
                        </span>
                        <span class="welcome-planeta-sections_name">
                            <spring:message code="planeta-welcome.jsp.planeta-online-campus" text="default text"/>
                        </span>
                        </a>
                    </div>
                </div>

                <%--<div class="welcome-planeta-sections_i">
                    <div class="welcome-planeta-sections_in">
                        <a href="https://${properties['promo.application.host']}/techbattle"
                           class="welcome-planeta-sections_link">
                            <span class="welcome-planeta-sections_ico">
                                <img src="{{= workspace.staticNodesService.getResourceUrl('/images/welcome_new/logo-bt.png')}}"
                                     width="181" height="54">
                            </span>
                            <span class="welcome-planeta-sections_name">
                                <spring:message code="planeta-welcome.jsp.planeta-bt" text="default text"/>
                            </span>
                        </a>
                    </div>
                </div>--%>
            </div>
        </div>
    </div>

    <div class="wrap">
        <div class="js-projects-welcome-promo-container"></div>
        <div class="js-projects-welcome-banner-container"></div>
        <div class="js-projects-welcome-banner-container-new project-welcome-banner"></div>
        <div class="js-projects-welcome-types-container"></div>
    </div>


    <div class="welcome-project-bottom">
        <div class="wrap">
            <div class="welcome-quick-info">
                <div class="welcome-quick-info_list">
                    <div class="welcome-quick-info_i">
                        <div class="welcome-quick-info_name">
                            <spring:message code="planeta-welcome.jsp.what-is-crowdfunding" text="default text"/>
                        </div>
                        <div class="welcome-quick-info_text">
                            <spring:message code="planeta-welcome.jsp.what-is-crowdfunding_descr" text="default text"/>
                        </div>
                    </div>

                    <div class="welcome-quick-info_i">
                        <div class="welcome-quick-info_name">
                            <spring:message code="planeta-welcome.jsp.what-is-share" text="default text"/>
                        </div>
                        <div class="welcome-quick-info_text">
                            <spring:message code="planeta-welcome.jsp.what-is-share_descr" text="default text"/>
                        </div>
                    </div>

                    <div class="welcome-quick-info_i">
                        <div class="welcome-quick-info_name">
                            <spring:message code="planeta-welcome.jsp.about-planeta" text="default text"/>
                        </div>
                        <div class="welcome-quick-info_text">
                            <spring:message code="planeta-welcome.jsp.about-planeta_descr" text="default text"/>
                        </div>
                    </div>
                </div>
            </div>

            <div class="project-partners project-partners__partners js-projects-welcome-partners-container"></div>
            <div class="project-partners__curator js-projects-welcome-curators-container"></div>
        </div>
    </div>
</script>

<script id="rewards-lazy-load-list" type="text/x-template">
    <div>
        <div class="reward-card">
            <reward-card v-for="reward in rewards" :reward="reward" page="search"></reward-card>
        </div>
        <div class="list-preloader" v-if="showPreloader"></div>
        <div class="project-list-not-found" v-if="notFound">
            <div>{{ $t('share_search.not_found') }}</div>
            <div class="hl">{{ $t('share_search.change_criteria') }}</div>
        </div>
    </div>
</script>

<script id="reward-search-page" type="text/x-template">
    <div class="project-list-container">
        <search-query :placeholder="$t('share_search.query.placeholder')" :query="model.query" @query_changed="queryChanged"></search-query>

        <div class="wrap">
            <div class="wrap-row">
                <div class="m-reaward-filter-button d-tl-none col-12">
                    <button type="button" class="btn btn-nd-primary btn-block" @click="toggleFilter()">Фильтр</button>
                </div>

                <div class="m-col-ld-3 m-col-tl-4 col-12 m-col-ld-push-9 m-col-tl-push-8">
                    <div class="rewards-filter">
                        <div class="rewards-filter_block">
                            <div class="rewards-filter_i">
                                <div class="rewards-filter_head">
                                    <div class="rewards-filter_title">
                                        {{ $t('share_search.filters.campaigns') }}
                                    </div>
                                    <div class="rewards-filter_more" @click="deselectAll()">
                                        {{ $t('share_search.filters.clear') }}
                                    </div>
                                </div>

                                <div class="rewards-filter_body">
                                    <div class="rewards-filter-list">
                                        <div class="rewards-filter-list_i" v-for="cat in projectCategories" v-if="cat.visibleInSearch">
                                            <div class="form-ui form-ui-reward-filter">
                                                <input class="form-ui-control" :id="'project-type-' + cat.mnemonicName" type="checkbox" name="checkbox" :value="cat.id" v-model="model.campaignTagsIds">
                                                <label class="form-ui-label" :for="'project-type-' + cat.mnemonicName">
                                                    <span class="form-ui-del"></span>
                                                    <span class="form-ui-txt">{{ getCurrentLanguageCatName(cat) }}</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="rewards-filter_i">
                                <div class="rewards-filter_head">
                                    <div class="rewards-filter_title">
                                        {{ $t('share_search.filters.price') }}
                                    </div>
                                </div>

                                <div class="rewards-filter_body">
                                    <div class="rewards-filter_range">
                                        <div class="wrap-row">
                                            <div class="col-6">
                                                <vue-numeric currency="₽" :minus="false" separator="space" v-model="model.priceFrom" class="form-control"></vue-numeric>
                                            </div>
                                            <div class="col-6">
                                                <vue-numeric currency="₽" :minus="false" separator="space" v-model="model.priceTo" class="form-control"></vue-numeric>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="rewards-filter_action">

                            <div class="wrap-row">
                                <div class="col-6 d-tl-none">
                                    <button type="button" class="btn btn-nd-primary btn-block" @click="toggleFilter()">
                                        {{ $t('share_search.filters.ready') }}
                                    </button>
                                </div>

                                <div class="m-col-tl-12 col-6">
                                    <button type="button" class="btn btn-nd-default btn-block" @click="reset()">
                                        {{ $t('share_search.filters.reset') }}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-col-ld-9 m-col-tl-8 col-12 m-col-ld-pull-3 m-col-tl-pull-4">
                    <div>
                        <div class="project-list-filter">
                            <div class="project-list-filter_i">
                                <custom-select v-model="model.sortOrderList" :options="sortOrderOptions" :current="model.sortOrderList"></custom-select>
                            </div>

                        </div>

                        <div class="reward-list-block">
                            <rewards-lazy-load-list :model="model" :limit="12"></rewards-lazy-load-list>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</script>

<script id="bb-reward-search-page" type="text/x-jquery-template">
    <reward-search-page></reward-search-page>
</script>

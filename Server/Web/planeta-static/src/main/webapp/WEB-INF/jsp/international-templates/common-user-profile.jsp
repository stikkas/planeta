<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script id="profile-info-hover-template" type="text/x-jquery-template">
    {{if profileType}}
        <div class="avatar">
                <a href="{{= ProfileUtils.getAbsoluteUserLink(profileId, alias)}}">
                <img src="{{= ImageUtils.getUserAvatarUrl(smallImageUrl, ImageUtils.ALBUM_COVER, userGender)}}"
                     alt="{{= displayName}}"/>
                </a>
        </div>
        <div class="info">
            <a href="{{= ProfileUtils.getAbsoluteUserLink(profileId, alias)}}" class="name"> {{= StringUtils.cutString(displayName, 64)}}</a>
            {{if profileType == "USER"}}
            {{tmpl "#online-status-template"}}
            <p>
                {{if userBirthDate}}
                <spring:message code="age" text="default text"> </spring:message>: {{= DateUtils.getAge(userBirthDate)}} <br>
                {{/if}}
                {{if countryName}}
                <spring:message code="location2" text="default text"> </spring:message>: {{= countryName}}{{if cityName}}, {{= cityName}}{{/if}}
                {{/if}}
            </p>

            {{if summary}}
            <p>{{= StringUtils.cutString(StringUtils.stripHtmlTags(summary),160)}}</p>
            {{/if}}

            {{else profileType == "GROUP"}}

            <p>
                <spring:message code="type.of.community" text="default text"> </spring:message>: {{= ProfileUtils.getGroupCategoryName(groupCategory) }}<br/>
                <spring:message code="decl.member" text="default text"> </spring:message>
            </p>

            {{if summary}}
            <p>{{= StringUtils.cutString(summary,160)}}</p>
            {{/if}}
            {{/if}}
        </div>

        {{if useActions}}
        {{tmpl '#profile-info-hover-buttons-template'}}
        {{/if}}

        {{else}}
        <spring:message code="loading.dots" text="default text"> </spring:message>
    {{/if}}
</script>

<script id="admin-online-profile-info-template" type="text/x-jquery-template">
    <div class="avatar">
        <a href="{{= ProfileUtils.getAbsoluteUserLink(profileId, alias)}}">
            <img src="{{= ImageUtils.getUserAvatarUrl(imageUrl, ImageUtils.AVATAR, userGender)}}"
                 alt="{{= displayName}}"/>
        </a>
    </div>
    <div class="info">
        <a href="{{= ProfileUtils.getAbsoluteUserLink(profileId, alias)}}" class="name">
            {{= StringUtils.cutString(displayName, 64)}}</a>{{if userBirthDate}}, {{= DateUtils.getAge(userBirthDate)}}<br>{{/if}}

        {{if countryName}}
            {{= countryName}}{{if cityName}}, {{= cityName}}{{/if}}
        {{/if}}

        {{if summary}}
        <p>{{= StringUtils.cutString(StringUtils.stripHtmlTags(summary),160)}}</p>
        {{/if}}

    </div>
</script>

<script id="profile-info-hover-buttons-template" type="text/x-jquery-template">
    {{if $data.profileRelationStatus}}
        <div class="n-card_action">
            <div class="row-fluid">
                <div class="span12">
                    {{if PrivacyUtils.doISubscribeYou($data)}}
                        <span class="btn btn-lg btn-block n-card_subscr-btn"  data-event-click="onUnsubscribeClicked"><spring:message code="subscribed" text="default text"> </spring:message></span>
                    {{else}}
                        <span class="btn btn-lg btn-block" data-event-click="onSubscribeClicked"><spring:message code="subscribe" text="default text"> </spring:message></span>
                    {{/if}}
                </div>
            </div>
        </div>
    {{/if}}
</script>


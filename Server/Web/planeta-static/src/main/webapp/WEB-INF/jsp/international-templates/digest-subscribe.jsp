<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script id="digest-subscribe-form" type="text/x-jquery-template">
        <form class="f-digest">
            <div class="f-digest_lbl">
                <spring:message code="digest.subscribe" text="default text"/>
            </div>
            <div class="f-digest_form">
                <input type="text" class="f-digest_val form-control js-digest-subscribe-input" placeholder="<spring:message code="digest.email" text="default text"/>">
                <button type="button" class="f-digest_btn btn js-digest-subscribe-btn">
                    <svg width="18" height="12" viewBox="0 0 18 12"><path d="M12.268 5.384L17.749.236c.16.153.252.363.251.582v10.364c0 .206-.08.405-.225.556l-5.507-6.354zM.278.213A.851.851 0 0 1 .841 0h16.315a.853.853 0 0 1 .563.213L8.998 6.818.278.213zM.225 11.738A.8.8 0 0 1 0 11.182V.818C0 .599.09.389.251.236l5.481 5.148-5.507 6.354zm8.773-3.283l2.289-2.15 6.447 5.47a.85.85 0 0 1-.578.225H.841a.854.854 0 0 1-.579-.225l6.448-5.47 2.288 2.15z" fill="#FFF"></path></svg>
                </button>
            </div>
        </form>
</script>

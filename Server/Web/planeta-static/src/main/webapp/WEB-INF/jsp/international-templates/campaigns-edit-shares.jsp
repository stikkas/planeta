<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script id="campaign-edit-shares-content-template" type="text/x-jquery-template">
    <div class="project-create_cont">
        <div class="project-create_text">
            <div class="project-create_text_head">
                <spring:message code="campaigns-edit-shares.jsp.propertie.1" text="default text"> </spring:message>
            </div>

            <div class="project-create_text_text">
                <spring:message code="campaigns-edit-shares.jsp.propertie.2" text="default text"> </spring:message>
            </div>
        </div>

        {{if errors.shares}}
            <div id="errorRow" class="project-create_row error-row">
                <div class="project-create_col-lbl">
                    <div class="pc_col-lbl_title">
                        <spring:message code="campaigns-edit-shares.jsp.propertie.3" text="default text"> </spring:message>
                    </div>
                </div>
            </div>
        {{/if}}
    </div>
</script>

<script id="campaign-edit-share-modal-template" type="text/x-jquery-template">
    <div class="modal-header">
        <a class="close">×</a>

        <div class="modal-title">
            <spring:message code="campaigns-edit-shares.jsp.propertie.4" text="default text"> </spring:message>
        </div>
    </div>

    <div class="project-create_cont">
        <div class="project-create_list"></div>
    </div>
</script>

<script id="campaign-edit-share-added-template" type="text/x-jquery-template">
    <div class="pc_ars_msg">
        <spring:message code="campaigns-edit-shares.jsp.propertie.5" text="default text"> </spring:message>
    </div>
</script>

<script id="campaign-edit-share-add-template" type="text/x-jquery-template">
    <div class="project-create_col-val">
        {{if shareId}}
            <div class="project-create_col-val_cont">
                <div class="pull-right">
                    <button class="btn btn-primary" data-event-click="onShareSave">
                        <spring:message code="campaigns-edit-shares.jsp.propertie.6" text="default text"> </spring:message>
                    </button>
                </div>

                <span class="btn cancel">
                    <spring:message code="campaigns-edit-shares.jsp.propertie.7" text="default text"> </spring:message>
                </span>
            </div>
        {{else}}
            <div class="project-create_col-val_cont text-center">
                <span class="btn btn-primary btn-lg" data-event-click="onShareAdd">
                    <spring:message code="campaigns-edit-shares.jsp.propertie.8" text="default text"> </spring:message>
                </span>
            </div>
        {{/if}}
    </div>
</script>

<script id="campaign-edit-share-list-template" type="text/x-jquery-template">
    <div class="action-card-block action-card-edit"></div>
    {{if $data.shares.length < 2}}
    <div class="project-create_reward-error">
        <div class="project-create_reward-error_text">
            <spring:message code="campaigns-edit-shares.jsp.propertie.9" text="default text"> </spring:message>
        </div>
    </div>
    {{/if}}
</script>

<script id="interactive-campaign-edit-share-list-template" type="text/x-jquery-template">
    <div class="action-card-block action-card-edit"></div>
    {{if $data.shares.length < 2}}
    <div class="project-create_reward-error">
        <div class="project-create_reward-error_text">
            <spring:message code="campaigns-edit-shares.jsp.propertie.9" text="default text"> </spring:message>
        </div>
    </div>
    {{/if}}
</script>

<script id="campaign-edit-share-list-must-be-one-share-template" type="text/x-jquery-template">
    <div class="project-create_reward-error">
        <div class="project-create_reward-error_text">
            <spring:message code="campaigns-edit-shares.jsp.propertie.9" text="default text"> </spring:message>
        </div>
    </div>
</script>


<script id="campaign-edit-share-name-template" type="text/x-jquery-template">
    <div class="project-create_row{{if errors.name}} error{{/if}}">
        <div class="project-create_col-lbl">
            <div class="pc_col-lbl_title">
                <spring:message code="campaigns-edit-shares.jsp.propertie.10" text="default text"> </spring:message>

                {{if errors.name}}
                    <div class="pc_row_warning" data-tooltip="{{= errors.name}}">
                        <div class="s-pc-action-warning"></div>
                    </div>
                {{/if}}
            </div>
        </div>

        <div class="project-create_col-val">
            <input class="form-control prj-crt-input js-share-name-limit" data-limit="130" type="text"
                   placeholder="<spring:message code="campaigns-edit-shares.jsp.propertie.10" text="default text"/>" value="{{= name}}">
            <div class="rest-count"></div>
        </div>
    </div>
</script>

<script id="campaign-edit-share-type-template" type="text/x-jquery-template">
    <div class="project-create_row">
        <div class="project-create_col-lbl">
            <div class="pc_col-lbl_title">
                <spring:message code="campaigns-edit-shares.jsp.propertie.35" text="default text"> </spring:message>
            </div>
        </div>

        <div class="project-create_col-val">
            <select class="pln-select pln-select-xlg" name="shareStatus" data-planeta-ui="dropDownSelect">
                <option value="ACTIVE"><spring:message code="share-type-1" text="default text"> </spring:message></option>
                <option value="INVESTING"><spring:message code="share-type-2" text="default text"> </spring:message></option>
                <option value="INVESTING_WITHOUT_MODERATION"><spring:message code="share-type-3" text="default text"> </spring:message></option>
                <option value="INVESTING_ALL_ALLOWED"><spring:message code="share-type-4" text="default text"> </spring:message></option>
            </select>
        </div>
    </div>
</script>

<script id="campaign-edit-share-description-template" type="text/x-jquery-template">
    <div class="project-create_row{{if errors.description}} error{{/if}}">
        <div class="project-create_col-lbl">
            <div class="pc_col-lbl_title">
                <spring:message code="campaigns-edit-shares.jsp.propertie.11" text="default text"> </spring:message>

                {{if errors.description}}
                    <div class="pc_row_warning" data-tooltip="{{= errors.description}}">
                        <div class="s-pc-action-warning"></div>
                    </div>
                {{/if}}
            </div>
            <div class="pc_col-lbl_text">
                <spring:message code="campaigns-edit-shares.jsp.propertie.12" text="default text" />
            </div>
        </div>

        <div class="project-create_col-val project-create_ww-block project-create__ww-block-300">
            <div id="description-editor-stub" class="campaign-tinymce-loader">
                <div class="campaign-tinymce-loader-in"></div>
            </div>

            <div data-anchor="tiny-mce"></div>
        </div>
    </div>
</script>

<script id="campaign-edit-share-price-template" type="text/x-jquery-template">
    <div class="project-create_row{{if errors.price}} error{{/if}}">
        <div class="project-create_col-lbl">
            <div class="pc_col-lbl_title">
                <spring:message code="campaigns-edit-shares.jsp.propertie.13" text="default text" /> <span class="b-rub">Р</span>

                {{if errors.price}}
                    <div class="pc_row_warning" data-tooltip="{{= errors.price}}">
                        <div class="s-pc-action-warning"></div>
                    </div>
                {{/if}}
            </div>
        </div>

        <div class="project-create_col-val">
            <input class="form-control prj-crt-input" name="price" data-parse="number" type="text" placeholder="<spring:message code="campaigns-edit-shares.jsp.propertie.15" text="default text" />" value="{{= price}}">
        </div>
    </div>
</script>


<script id="campaign-edit-share-delivery-address-template" type="text/x-jquery-template">
    <div class="project-create_col-val">
        <div class="project-create_col-val_cont">
            <label>
                <input type="checkbox" {{if deliverToCustomer}}checked="checked"{{/if}}>
                <spring:message code="campaigns-edit-shares.jsp.propertie.16" text="default text"> </spring:message>
            </label>
        </div>
    </div>
</script>

<script id="campaign-edit-share-reward-instruction-template" type="text/x-jquery-template">
    <div class="project-create_row{{if errors.rewardInstruction}} error{{/if}}">
        <div class="project-create_col-lbl">
            <div class="pc_col-lbl_title">
                <spring:message code="campaigns-edit-shares.jsp.propertie.17" text="default text"/>

                {{if errors.rewardInstruction}}
                    <div class="pc_row_warning" data-tooltip="{{= errors.rewardInstruction}}">
                        <div class="s-pc-action-warning"></div>
                    </div>
                {{/if}}
            </div>
            <div class="pc_col-lbl_text">
                <spring:message code="campaigns-edit-shares.jsp.propertie.36" text="default text" />
            </div>
        </div>

        <div class="project-create_col-val">
            <textarea class="form-control prj-crt-input" data-limit="250" rows="12">{{= rewardInstruction}}</textarea>

            <div class="rest-count"></div>
        </div>
    </div>
</script>

<script id="campaign-edit-share-reward-instruction-row-template" type="text/x-jquery-template">
    <div class="project-create_col-lbl">
        <div class="pc_col-lbl_title">
            <spring:message code="campaigns-edit-shares.jsp.propertie.19" text="default text"> </spring:message>
        </div>
    </div>

    <div class="project-create_col-val">
            <textarea class="form-control prj-crt-input" rows="4" placeholder="">{{= rewardInstruction}}</textarea>
    </div>
</script>

<script id="campaign-edit-share-author-question-row-template" type="text/x-jquery-template">
    <div class="project-create_col-lbl">
        <div class="pc_col-lbl_title">
            <spring:message code="campaigns-edit-shares.jsp.propertie.20" text="default text"> </spring:message>
        </div>
    </div>

    <div class="project-create_col-val">
        <textarea id="question" class="form-control prj-crt-input" rows="4" placeholder="">{{= questionToBuyer}}</textarea>
    </div>
</script>

<script id="campaign-edit-share-author-question-template" type="text/x-jquery-template">
    <div class="project-create_col-val">
        <div class="project-create_col-val_cont">
            <label>
                <input type="checkbox" {{if questionToBuyer}}checked="checked"{{/if}}>
                <spring:message code="campaigns-edit-shares.jsp.propertie.21" text="default text"> </spring:message>
            </label>
            <div class="pc_row_help" data-tooltip="<spring:message code="campaigns-edit-shares.jsp.propertie.22" text="default text"> </spring:message>"></div>
        </div>
    </div>

    <div class="project-create_list" data-anchor="questionToBuyer" {{if questionToBuyer}}{{else}} style="display: none"{{/if}}></div>
</script>

<script id="campaign-edit-share-author-question-row-variants-template" type="text/x-jquery-template">
    <div class="prj-crt-select_wrap">
        <select class="pln-select pln-select-xlg js-selector" name="questionToBuyer" data-planeta-ui="dropDownSelect">
            <option><spring:message code="question-template-1" text="default text"> </spring:message></option>
            <option><spring:message code="question-template-2" text="default text"> </spring:message></option>
            <option><spring:message code="question-template-3" text="default text"> </spring:message></option>
            <option><spring:message code="question-template-4" text="default text"> </spring:message></option>
            <option><spring:message code="question-template-9" text="default text"> </spring:message></option>
        </select>
    </div>
</script>

<script id="campaign-edit-share-reward-instruction-row-variants-template" type="text/x-jquery-template">
    <div class="prj-crt-select_wrap">
        <select class="pln-select pln-select-xlg js-selector" name="rewardInstruction" data-planeta-ui="dropDownSelect">
            <option><spring:message code="question-template-5" text="default text"> </spring:message></option>
            <option><spring:message code="question-template-6" text="default text"> </spring:message></option>
            <option><spring:message code="question-template-7" text="default text"> </spring:message></option>
            <option><spring:message code="question-template-8" text="default text"> </spring:message></option>
        </select>
    </div>
</script>

<script id="campaign-edit-share-pickup-template" type="text/x-jquery-template">
    <div class="project-create_col-val">
        <div class="project-create_col-val_cont">
            <label>
                <input type="checkbox" {{if pickupByCustomer}}checked="checked"{{/if}}>
                <spring:message code="campaigns-edit-shares.jsp.propertie.23" text="default text"> </spring:message>
            </label>
        </div>
    </div>

    <div class="project-create_list" data-anchor="customerPickup" {{if pickupByCustomer}}{{else}}style="display: none"{{/if}}></div>
</script>

<script id="campaign-edit-share-pickup-city-template" type="text/x-jquery-template">
    <div class="project-create_col-lbl">
        <div class="pc_col-lbl_title">
            <spring:message code="campaigns-edit-shares.jsp.propertie.24" text="default text"> </spring:message>
            {{if errors.city}}
                <div class="pc_row_warning" data-tooltip="{{= errors.city}}">
                    <div class="s-pc-action-warning"></div>
                </div>
            {{/if}}
        </div>
    </div>

    <div class="project-create_col-val">
        <input class="form-control prj-crt-input" type="text" placeholder="<spring:message code="campaigns-edit-shares.jsp.propertie.25" text="default text"> </spring:message>"  value="{{= city}}">
    </div>
</script>

<script id="campaign-edit-share-pickup-address-template" type="text/x-jquery-template">
    <div class="project-create_col-lbl">
        <div class="pc_col-lbl_title">
            <spring:message code="campaigns-edit-shares.jsp.propertie.26" text="default text"> </spring:message>

            {{if errors.street}}
                <div class="pc_row_warning" data-tooltip="{{= errors.street}}">
                    <div class="s-pc-action-warning"></div>
                </div>
            {{/if}}
        </div>
    </div>

    <textarea class="form-control prj-crt-input" placeholder="<spring:message code="campaigns-edit-shares.jsp.propertie.27" text="default text"> </spring:message>">{{= street}}</textarea>

    <div class="project-create_col-val"></div>
</script>

<script id="campaign-edit-share-pickup-tel-template" type="text/x-jquery-template">
    <div class="project-create_col-lbl">
        <div class="pc_col-lbl_title">
            <spring:message code="campaigns-edit-shares.jsp.propertie.28" text="default text"> </spring:message>

            {{if errors.phone}}
                <div class="pc_row_warning" data-tooltip="{{= errors.phone}}">
                    <div class="s-pc-action-warning"></div>
                </div>
            {{/if}}
        </div>
    </div>

    <div class="project-create_col-val">
        <input class="form-control prj-crt-input" type="text" placeholder="+7 (495) 123-45-67" value="{{= phone}}">
    </div>
</script>


<script id="campaign-edit-share-amount-template" type="text/x-jquery-template">
    <div class="project-create_row{{if errors.amount}} error{{/if}}">
        <div class="project-create_col-lbl">
            <div class="pc_col-lbl_title">
                <spring:message code="campaigns-edit-shares.jsp.propertie.29" text="default text"> </spring:message>

                {{if errors.amount}}
                    <div class="pc_row_warning" data-tooltip="{{= errors.amount}}">
                        <div class="s-pc-action-warning"></div>
                    </div>
                {{/if}}
            </div>
        </div>

        <div class="project-create_col-val">
            <div class="project-create_col-val_col">
                <input class="form-control prj-crt-input" data-parse="number" type="text" placeholder="<spring:message code="campaigns-edit-shares.jsp.propertie.33" text="default text"> </spring:message>" value="{{= amount}}" {{if amount == 0}}disabled{{/if}}>
            </div>

            <div class="project-create_col-val_col">
                <div class="project-create_col-val_cont">
                    <label>
                        <input type="checkbox" {{if amount == 0}}checked="checked"{{/if}}>
                        <spring:message code="campaigns-edit-shares.jsp.propertie.30" text="default text"> </spring:message>
                    </label>
                </div>
            </div>
        </div>
    </div>
</script>

<script id="campaign-edit-share-estimated-delivery-time-template" type="text/x-jquery-template">
    <div class="project-create_row">
        <div class="project-create_col-lbl">
            <div class="pc_col-lbl_title">
                <spring:message code="campaigns-edit-shares.jsp.propertie.31" text="default text"> </spring:message>

                <div class="pc_row_help" data-tooltip="<spring:message code="campaigns-edit-shares.jsp.propertie.32" text="default text"> </spring:message>"></div>
            </div>
        </div>

        <div class="project-create_col-val">
            <input class="form-control prj-crt-input prj-crt-input__date" type="text" placeholder="31.12.2017" data-planeta-ui="dateSelect" data-field-name="estimatedDeliveryTime" data-ui-default-value="{{= currentDate}}" name="estimatedDeliveryTime" value="{{= estimatedDeliveryTime}}">
        </div>
    </div>
</script>

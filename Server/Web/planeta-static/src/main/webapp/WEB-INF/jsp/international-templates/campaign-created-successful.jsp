<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!--include css /css-generated/project.css-->
<script id="campaign-edit-created-successful-template" type="text/x-jquery-template">
    <div class="project-create-success">
        <div class="project-create-success_cont">
            <div class="project-create-success_ico">
                <svg width="60" height="60"><path fill="#20c924" d="M30.000,60.000 C13.432,60.000 0.000,46.569 0.000,30.000 C0.000,13.432 13.432,0.000 30.000,0.000 C46.569,0.000 60.000,13.432 60.000,30.000 C60.000,46.569 46.569,60.000 30.000,60.000 ZM30.000,5.625 C16.538,5.625 5.625,16.538 5.625,30.000 C5.625,43.462 16.538,54.375 30.000,54.375 C43.462,54.375 54.375,43.462 54.375,30.000 C54.375,16.538 43.462,5.625 30.000,5.625 ZM43.239,24.489 L28.239,39.489 L28.239,39.489 C27.730,39.998 27.027,40.313 26.250,40.313 C25.473,40.313 24.770,39.998 24.261,39.489 L24.261,39.489 L15.824,31.052 L15.824,31.052 C15.315,30.542 15.000,29.839 15.000,29.063 C15.000,27.509 16.259,26.250 17.813,26.250 C18.589,26.250 19.292,26.565 19.801,27.074 L19.801,27.074 L26.250,33.523 L39.261,20.512 L39.261,20.512 C39.770,20.003 40.473,19.688 41.250,19.688 C42.803,19.688 44.063,20.947 44.063,22.500 C44.063,23.277 43.748,23.980 43.239,24.489 L43.239,24.489 ZM43.125,19.687 C43.125,19.687 43.125,19.688 43.125,19.688 C43.125,19.688 43.125,19.688 43.125,19.689 L43.125,19.687 ZM19.688,43.125 C19.687,43.125 19.687,43.125 19.687,43.125 L19.688,43.125 C19.688,43.125 19.688,43.125 19.688,43.125 ZM40.313,43.125 C40.312,43.125 40.312,43.125 40.312,43.125 L40.313,43.125 C40.313,43.125 40.313,43.125 40.313,43.125 ZM43.125,40.314 L43.125,40.312 C43.125,40.312 43.125,40.312 43.125,40.313 C43.125,40.313 43.125,40.313 43.125,40.314 Z"/></svg>
            </div>

            <div class="project-create-success_head">
                {{if $data.campaign && $data.campaign.name}}
                Ваша заявка на&nbsp;создание проекта <a href="/campaigns/{{= objectId}}">{{= $data.campaign.name ? campaign.name : 'название проекта'}}</a> принята.
                {{else}}
                Ваша заявка на&nbsp;создание <a href="/campaigns/{{= objectId}}">проекта</a> принята.
                {{/if}}
            </div>

            <div class="project-create-success_text">
                Заявка будет рассмотрена в&nbsp;течение трех рабочих дней. В случае, если ваш проект успешно пройдет модерацию, с вами свяжется менеджер Planeta.ru по электронной почте: {{each contactList}} {{= $value.email}} {{/each}}
            </div>
        </div>

        <div class="project-create-success_polling js-feedback-form" style="display: none;">
            <form id="user-feedback-form" class="quality-polling js-feedback-message-container">
                <div class="quality-polling_head">
                    Оцените качество сервиса
                </div>
                <div class="quality-polling_tip">
                    1 — очень плохо, 5 — хорошо, 10 — отлично!
                </div>
                <div class="quality-polling_value">
                    <div class="quality-polling-score">
                        {{each(index, value) Array.apply(null, Array(10)).map(function (_, i) {return i;})}}
                        <div class="quality-polling-score_i">
                            <div class="form-ui form-ui-default">
                                <input class="form-ui-control" id="score-{{= index}}" type="radio" name="score" value="{{= index}}">
                                <label class="form-ui-label" for="score-{{= index}}">
                                                <span class="form-ui-txt">
                                                    <span class="form-ui-val">{{= index + 1}}</span>
                                                </span>
                                </label>
                            </div>
                        </div>
                        {{/each}}
                    </div>
                </div>
                <input type="hidden" name="orderId" value="0"/>
                <input type="hidden" name="pageType" value="CAMPAIGN_CREATED_SUCCESSFUL"/>
                <input type="hidden" name="campaignId" value="{{if $data.campaign}}{{= $data.campaign.campaignId}}{{else}}{{= objectId}}{{/if}}"/>

                <div class="quality-polling_action">
                    <div class="quality-polling_extra">
                        <div class="form-ui form-ui-default">
                            <input class="form-ui-control" id="extraTesting" type="checkbox" name="extraTesting" value="true">
                            <label class="form-ui-label" for="extraTesting">
                                <span class="form-ui-txt">я хочу принять участие в детальном опросе</span>
                            </label>
                        </div>
                    </div>
                    <div class="quality-polling_btn">
                        <button type="submit" class="btn btn-primary js-send-feedback-form">Отправить</button>
                    </div>
                </div>
            </form>
        </div>


        <div class="project-create-success_book">
            <div class="project-create-success_book-head">
                <a href="https://school.planeta.ru/#manual-pdf">Скачайте</a> наше практическое пособие!
            </div>

            <div class="project-create-success_book-img"></div>
        </div>
    </div>

</script>
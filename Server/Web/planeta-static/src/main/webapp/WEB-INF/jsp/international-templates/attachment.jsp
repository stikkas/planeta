<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script id="photo-attachment-multi-dialog-template" type="text/x-jquery-template">
    <div class="modal-header">
        <a href="javascript:void(0)" class="close">×</a>
        <div class="modal-title"><span id="title"><spring:message code="tinymce.add.photo" text="default text" /></span></div>
        <div class="modal-header-shadow"></div>
    </div>
	<div class="modal-body">
		<div class="modal-foto-load">
			<p class="tc"><spring:message code="tinymce.add.photo.desc" text="default text" /></p>

			<div class="modal-foto-load-select">
				<span class="link" data-event-click="showPhotoLink"><spring:message code="tinymce.add.photo.from" text="default text" /></span>
				<span class="upload" data-event-click="showPhotoUpload"><spring:message code="tinymce.add.photo.upload" text="default text" /></span>
			</div>
		</div>
	</div>
</script>

<script id="photo-attachment-link-dialog-template" type="text/x-jquery-template">
	<div class="modal-header">
        <a href="javascript:void(0)" class="close">×</a>
        <div class="modal-title"><spring:message code="tinymce.add.photo" text="default text" /></div>
        <div class="modal-header-shadow"></div>
    </div>
    <div class="modal-body">
        <div class="upload-pic clearfix">
            <div class="upload-pic-img">
                <img src='{{= workspace.staticNodesService.getResourceUrl("images/defaults/upload-pic.png")}}' alt="upload-pic.png">
            </div>
            <div class="upload-pic-option">
                <p class="tc"><spring:message code="tinymce.add.photo.link" text="default text" /></p>

                <div class="upload-path">
                    <input name="img_url" placeholder="http://www.example.com" id="img-url" value="{{= imageUrl}}">
                </div>
                <p></p>

                <p class="photo-name"></p>

                <div id="photo-select-container">
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="modal-footer-cont">
            <span id="spanButtonPlaceHolder"></span>
            <button type="submit" disabled="true" class="btn btn-primary"><spring:message code="tinymce.ok" text="default text" /></button>
            <button type="reset" class="btn"><spring:message code="tinymce.cancel" text="default text" /></button>
        </div>
    </div>
</script>

<script id="youtube-attachment-link-dialog-template" type="text/x-jquery-template">
	<div class="modal-header">
        <a href="javascript:void(0)" class="close">×</a>
        <div class="modal-title"><span><spring:message code="tinymce.add.video.from.youtube" text="default text" /></span></div>
        <div class="modal-header-shadow"></div>
    </div>
    <div class="modal-body">
        <div class="upload-pic clearfix">
            <div class="upload-pic-img">
                <img src='{{= workspace.staticNodesService.getResourceUrl("images/defaults/upload-pic.png")}}' alt="upload-pic.png">
            </div>
            <div class="upload-pic-option">
                <p class="tc"><spring:message code="tinymce.add.video.link" text="default text" /></p>

                <div class="upload-path">
                    <input name="img_url" placeholder="http://www.youtube.com/watch?v=ZTUVgYoeN_b" id="img-url" value="">
                </div>
                <p></p>

                <p class="video-name"></p>

                <p class="video-duration"></p>
                <!--<button type="submit" class="btn">Вставить</button> -->
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="modal-footer-cont">
            <span id="spanButtonPlaceHolder"></span>
            <button type="submit" disabled="true" class="btn btn-primary"><spring:message code="tinymce.ok" text="default text" /></button>
            <button type="reset" class="btn"><spring:message code="tinymce.cancel" text="default text" /></button>
        </div>
    </div>
</script>



<script id="attachment-link-template" type="text/x-jquery-template">
    <div class="modal-header">
        <a href="javascript:void(0)" class="close">×</a>
        <div class="modal-title"><spring:message code="tinymce.add.link" text="default text" /></div>
        <div class="modal-header-shadow"></div>
    </div>
    <div class="modal-body">
        <div class="att-modl-link">
            <div class="label-name">
                <label for="link-text"><spring:message code="tinymce.add.link.address" text="default text" /></label>
            </div>
            <div class="input input-field">
                <input name="text" placeholder="http://www.example.com" class="span7" id="link-text"/>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="modal-footer-cont">
            <button type="submit" disabled="disable" class="btn btn-primary"><spring:message code="tinymce.ok" text="default text" /></button>
            <button type="reset" class="btn"><spring:message code="tinymce.cancel" text="default text" /></button>
        </div>
    </div>
</script>

<!--currently unused-->
<script id="attachment-editor-link-template" type="text/x-jquery-template">
    <div class="modal-header">
        <a href="javascript:void(0)" class="close">×</a>
        <div class="modal-title"><span><spring:message code="tinymce.add.link" text="default text" /></span></div>
    </div>
    <div class="modal-body">
        <div class="att-modl-link">
            <div class="label-name">
                <label for="link-text"><spring:message code="tinymce.add.link.text" text="default text" /></label>
            </div>
            <div class="input">
                <input name="name-text" value="{{selection}}" class="span7" id="link-name"/>
            </div>
            <div class="label-name">
                <label for="link-text"><spring:message code="tinymce.add.link.address" text="default text" /></label>
            </div>
            <div class="input">
                <input name="text" placeholder="http://www.example.com" class="span7" id="link-text"/>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="modal-footer-cont">
            <button type="submit" disabled="disable" class="btn btn-primary"><spring:message code="tinymce.ok" text="default text" /></button>
            <button type="reset" class="btn"><spring:message code="tinymce.cancel" text="default text" /></button>
        </div>
    </div>
</script>

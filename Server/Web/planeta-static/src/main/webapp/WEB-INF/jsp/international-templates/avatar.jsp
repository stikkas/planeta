<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script id="avatar-crop-dialog-template" type="text/x-jquery-template">
    <div class="modal-header">
        <a href="javascript:void(0)" class="close">×</a>
        <div class="modal-title">{{if title}}{{= title}}{{else}}<spring:message code="avatar.edit" />{{/if}}</div>
        <div class="modal-header-shadow"></div>
    </div>
    <form>
        <div class="modal-body" style="overflow: visible">
            <div class="avatar-upload">
                <div class="avatar-upload-full">
                    <p><spring:message code="avatar.main.version" /></p>
                    <div class="avatar-upload-full-pic">
                        <img id="original-avatar" src="{{= ImageUtils.getUserAvatarUrl(imageUrl, ImageUtils.AVATAR, userGender)}}" alt="main avatar">
                    </div>
                    {{if webcam == undefined || webcam}}
                        <a href="javascript:void(0)" class="webcam">
                            <i class="icon-webcam icon-gray"></i>
                            <spring:message code="avatar.web.cam.shot" />
                        </a>
                    {{/if}}
                    <a href="javascript:void(0)" class="upload">
                        <i class="icon-photo icon-gray"></i>
                        <spring:message code="avatar.add.photo" />
                    </a>
                </div>
                <div class="avatar-upload-preview">
                    <p><spring:message code="avatar.small.version" /></p>
                    <div class="avatar-upload-preview-pic">
                       {{if imageUrl}}
                       <img id="preview-avatar" src="{{= ImageUtils.getUserAvatarUrl(imageUrl, ImageUtils.ORIGINAL, userGender)}}" alt="crop avatar">
                       {{else}}
                       <img id="preview-avatar" src="{{= ImageUtils.getUserAvatarUrl(smallImageUrl, ImageUtils.SMALL_AVATAR, userGender)}}" alt="crop avatar">
                       {{/if}}
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="modal-footer-cont">
                <button type="submit" class="btn btn-primary"><spring:message code="avatar.save.changes" /></button>
                <button type="reset" class="btn"><spring:message code="avatar.cancel" /></button>
            </div>
        </div>
    </form>
</script>

<script id="avatar-webcam-dialog-template" type="text/x-jquery-template">
    <div class="modal-header">
        <a href="javascript:void(0)" class="close">×</a>
        <div class="modal-title"><span id="title"><spring:message code="avatar.make.web.cam.shot" /></span></div>
        <div class="modal-header-shadow"></div>
    </div>
    <form>
        <div class="modal-body" style="overflow: visible">
            
        </div>
        <div class="modal-footer">
            <div class="modal-footer-cont">
                <button type="submit" class="btn btn-primary"><spring:message code="avatar.make.shot" /></button>
                <button type="reset" class="btn"><spring:message code="avatar.cancel" /></button>
            </div>
        </div>
    </form>
</script>

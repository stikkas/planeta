<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script id="campaign-edit-description-editor-template" type="text/x-jquery-template">
    <div class="project-create_col-lbl">
        <div class="pc_col-lbl_title">
            <spring:message code="campaign-edit-details.jsp.propertie.1" text="default text"> </spring:message>

            {{if errors.descriptionHtml}}
                <div class="pc_row_warning tooltip" data-tooltip="{{= errors.descriptionHtml}}">
                    <div class="s-pc-action-warning"></div>
                </div>
            {{/if}}
        </div>

        <div class="pc_col-lbl_text">
            <spring:message code="campaign-edit-details.jsp.propertie.2" text="default text"> </spring:message>
        </div>
    </div>

    <div class="project-create_col-val project-create_ww-block">
        <div id="description-editor-stub" class="campaign-tinymce-loader">
            <div class="campaign-tinymce-loader-in"></div>
        </div>

        <div data-anchor="tiny-mce"></div>
    </div>
</script>




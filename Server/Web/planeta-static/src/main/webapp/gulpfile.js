process.setMaxListeners(0);
// process.stdin.setMaxListeners(0);
// process.stdout.setMaxListeners(0);
// process.stderr.setMaxListeners(0);
require('events').EventEmitter.prototype._maxListeners = 100;

var gulp = require('gulp');
var duration = require('gulp-duration');
var debug = require ('gulp-debug');
var less = require('gulp-less');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var argv = require('yargs').argv;


var lessWatch = require('./gulp/gulp-less-watch');


gulp.task('css', function() {
    var lessPath = 'less/*.less';

    if (
        !!argv.project &&
        typeof argv.project == 'string' &&
        argv.project != ''
    ) {
        var projects = argv.project.split('/');
        lessPath = [];
        projects.forEach(function (n) {
            lessPath.push('less/' + n + '.less')
        });
    }

    gulp.src(lessPath)
        .pipe(lessWatch());
});


gulp.task('default', ['css']);

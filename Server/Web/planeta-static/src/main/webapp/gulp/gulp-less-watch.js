'use strict';
var gulp = require('gulp');
var debug = require('gulp-debug');
var watch = require('gulp-watch');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var less = require('gulp-less');
var insert = require('gulp-insert');

var runSequence = require('run-sequence');
var gutil = require('gulp-util');
var PluginError = gutil.PluginError;
var through = require('through2');
var lessc = require('less');
var path = require('path');

var PLUGIN_NAME = 'gulp-less-watch';


module.exports = function() {
    return through.obj(function(file, enc, cb) {
        if (file.isNull()) {
            return cb(null, file);
        }

        if (file.isStream()) {
            this.emit('error', new PluginError(PLUGIN_NAME, 'Streams not supported!'));
            return;
        }

        try {

            getLessFileImports(file, function (err, imports) {
                if (err) { cb(new PluginError(PLUGIN_NAME, err)); }

                var name = path.basename(file.path, '.less');

                gulp.task('styles:' + name, function(){
                    return gulp.src(file.path)
                        .pipe(debug({title: name}))
                        .pipe(sourcemaps.init())
                        .pipe(less().on('error', function(err){
                            console.log(err);
                            this.emit('end');
                        }))
                        .pipe(autoprefixer({
                            browsers: ['> 1%', 'last 3 versions']
                        }))
                        .pipe(insert.append('#_' + name + '_css_{display:none;}'))
                        .pipe(sourcemaps.write('.'))
                        .pipe(gulp.dest('css-generated'))
                        .pipe(debug({title: name}))
                });


                gulp.task('watch:' + name, function(){
                    return watch(imports, function () {
                        gulp.start('styles:' + name);
                    });
                });


                runSequence('styles:' + name, 'watch:' + name);
            })


        } catch (err) {
            this.emit('error', new PluginError(PLUGIN_NAME, err));
        }

        cb();
    });
};


function getLessFileImports(file, cb) {
    var imports = [];

    // Parse the filepath, using file path as `filename` option
    lessc.parse(file.contents.toString('utf8'), {
            filename: file.path
        },
        function(err, root, imports) {
            // Add a better error message / properties
            if (err) {
                err.lineNumber = err.line;
                err.fileName = err.filename;
                err.message = err.message + ' in file ' + err.fileName + ' line no. ' + err.lineNumber;
            }

            // Generate imports list from the files hash (sorted)
            var imports = Object.keys(imports.files).sort();
            var paths = [file.path];

            // add parent folder
            imports.forEach(function (item, i, arr) {
                if ( item.indexOf('icomoon') != -1
                    || item.indexOf('variables') != -1
                    || item.indexOf('fonts') != -1 ) {
                    paths.push(item);
                } else {
                    var lessPath = path.dirname(item) + '/**/*.less';
                    if ( paths.indexOf(lessPath) == -1 ) {
                        paths.push(lessPath);
                    }
                }
            });

            cb(err, paths);
        });
}

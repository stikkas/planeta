/*!
 * Start Bootstrap - SB Admin 2 v3.3.7+1 (http://startbootstrap.com/template-overviews/sb-admin-2)
 * Copyright 2013-2016 Start Bootstrap
 * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap/blob/gh-pages/LICENSE)
 */
$(function() {
    $('#side-menu').metisMenu();
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    var url = window.location.pathname;

    var element = $('.sidebar a[href="' + url + '"]');

    if (!element.hasClass('firs-level-link')) {
        element.addClass('active');
        element.closest('.first-level').addClass('active expanded');
        element.closest('.nav-second-level').addClass('in');
    } else {
        element.parent().addClass('active');
    }

    if($( window ).width() > 767) {
        if(localStorage.getItem('adminMenuCollapsed') === 'true') {
            $('.sidebar').toggleClass('collapsed');
            $('#page-wrapper').toggleClass('expanded');
            $('.js-toggle-menu').toggleClass('collapsed');
            $('.js-toggle-menu').attr('title', 'Развернуть');
            $('.sidebar .sidebar-menu-block > ul > li > ul.in').removeClass('in')
        }
    }


    $(document).on('click', '.js-toggle-menu', function (e) {
        var $elem = $(this);

        $('.sidebar').toggleClass('collapsed');
        $('#page-wrapper').toggleClass('expanded');
        $elem.toggleClass('collapsed');

        if ($elem.hasClass('collapsed')) {
            localStorage.setItem('adminMenuCollapsed', 'true');
            $elem.attr('title', 'Развернуть');
            $('.sidebar .sidebar-menu-block > ul > li > ul.in').removeClass('in')
        } else {
            localStorage.setItem('adminMenuCollapsed', 'false');
            $('.first-level.active .nav-second-level').addClass('in');
            $elem.attr('title', 'Свернуть');
        }
    });

    $(document).on('click', '.js-toggle-navbar', function () {
        $('.top-panel').toggleClass('expanded');
    });

    $(document).on('click', '.sidebar .firs-level-link', function () {
        var sidebar = $('.sidebar');
        if (sidebar.hasClass('collapsed')) {
            var $elem = $('.js-toggle-menu');

            sidebar.toggleClass('collapsed');
            $('#page-wrapper').toggleClass('expanded');
            $elem.toggleClass('collapsed');

            if ($elem.hasClass('collapsed')) {
                $elem.attr('title', 'Развернуть');
            } else {
                $elem.attr('title', 'Свернуть');
            }
            localStorage.setItem('adminMenuCollapsed', 'false');
        } else {
            $(this).closest('.first-level').toggleClass('expanded');
        }

        $('.sidebar .first-level').not('.active').removeClass('expanded');
    });

    $(window).bind("load resize", function() {
        var topOffset = 50;
        var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        var height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });
});

module.exports = function (grunt) {

    require('time-grunt')(grunt);

    // Load NPM Tasks
    require('jit-grunt')(grunt)({
        loadTasks: './grunt/'
    });


    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        less: {
            options: {
                sourceMap: true,
                sourceMapFileInline: true,
                outputSourceFiles: true
            },
            local: {
                files: [
                    {
                        expand: true,
                        cwd: 'custom-css',
                        src: '**/*.less',
                        dest: 'css-generated',
                        ext: '.css'
                    }
                ]
            }
        },



        bless: {
            css: {
                options: {
                    cacheBuster: false
                },
                files: [
                    {
                        expand: true,
                        src: 'css-generated/bootstrap.css',
                        dest: 'css-generated'
                    }
                ]
            }
        },


        postcss: {
            options: {
                map: {
                    inline: false
                },

                processors: [
                    require('autoprefixer-core')({browsers: '> 1%, last 3 versions, Firefox >= 16, Firefox ESR, Opera 12.1'})
                ]
            },
            local: {
                files: [{
                    expand: true,
                    flatten: true,
                    src: 'css-generated/*.css',
                    dest: 'css-generated'
                }]
            }
        },


        addCssFilePointer: {
            local: {
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: 'css-generated/*.css',
                        dest: 'css-generated'
                    }
                ]
            }
        },


        watch: {
            css: {
                files: ['custom-css/**/*.less', 'less/**/*.less'],
                tasks: ['styles']
            }
        }

    });




    grunt.registerTask('styles', ['less', 'postcss', 'addCssFilePointer']);

    grunt.registerTask('default', ['styles', 'watch']);

};

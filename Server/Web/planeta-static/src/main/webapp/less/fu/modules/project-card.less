//
// Planeta Project Card Block
//
// --------------------------------------------


@projectCardGutter: 20px;
@projectCardOffset: 14px;
@projectFontFamily: Arial, sans-serif;

.project-card-list{
    &:extend(.cf-mix all);
    margin: -@projectCardGutter 0 20px -@projectCardGutter;
    .list-loader{
        margin: 40px 0 0 @gridGutterWidth;
    }
    .hidden{
        display: none;
    }
}

.project-card-item{
    position: relative;
    float: left;
    box-sizing: content-box;
    width: 218px;
    margin: @projectCardGutter 0 0 @projectCardGutter;
    word-wrap: break-word;
    border: 1px solid #dfe1e1;
    border-top: 0;
    background: #fff;
}
.project-card-link{
    &:extend(.cf-mix all);
    display: block;
    min-height: 480px;
    color: @textColor;
    background: #fff;
    text-decoration: none;
    [class^="project-card-"] {
        display: block;
    }
    &:hover{
        text-decoration: none;
    }
}

.project-card-update{
    position: absolute;
    top: 0;
    left: 100%;
    .pcu-count{
        position: relative;
        top: -8px;
        left: -50%;
        padding: 1px 5px;
        font: 700 11px/14px @baseFontFamily;
        color: #fff;
        background: #e35050;
        border-radius: 3px;
        cursor: default;
    }
}

.project-card-cover-wrap{
    position: relative;
    height: 134px;
    margin: 0 -1px 15px;
    background: #1a1a1a;
}

.project-card-text{
    max-height: 24px * 3 + 8px + 21 * 6; // name lh * 3 + name margin + descr lh * 6
    overflow: hidden;
}

.project-card-name{
    max-height: 21px * 3;
    margin: 0 @projectCardOffset 12px;
    overflow: hidden;
    font: 700 15px/21px @ff-proxima-nova;
    color: #000;
}
.project-card-descr{
    margin: 0 @projectCardOffset;
    font-size: 12px;
    line-height: 21px;
    color: #393939;
}

.project-card-footer{
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
}

.project-card-info{
    &:extend(.cf-mix all);
    margin: 15px @projectCardOffset 8px;
}
.project-card-info-i{
    width: 50%;
    float: left;
}
.pci-label{
    display: block;
    font-size: 10px;
    line-height: 12px;
    color: #2d2e2e;
    text-transform: uppercase;
}
.pci-value{
    display: block;
    height: 24px;
    overflow: hidden;
    font: 700 18px/24px @baseFontFamily;
    color: @linkColor;
}
.project-card-info-i + .project-card-info-i{
    .pci-value{
        color: #acacac;
    }
}

.project-card-link-to-project{
    padding: 5px @projectCardOffset @projectCardOffset;
}

.project-card-finish{
    margin: 15px 0 0;
    font-size: 10px;
    line-height: 12px;
    text-align: center;
    text-transform: uppercase;
    color: #3f3f3f;
}
.project-card-finish-sum{
    text-align: center;
    margin: 0 0 8px;
}


.project-card-state{
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background: rgba(255,255,255,.7);
    &__supported{
        background: fade(#ffd925, 50);
        .project-card-state-lbl{
            color: #483a06;
            background: fade(#ffd925, 80);
        }
    }
}
.project-card-state-lbl{
    position: absolute;
    right: 0;
    bottom: 0;
    left: 0;
    padding: 6px 0 6px;
    background: fade(#999, 50%);
    font-size: 8px;
    font-weight: 700;
    text-transform: uppercase;
    text-align: center;
    letter-spacing: 1px;
    color: #fff;
}

.project-card-cover-finish{
    &:extend(.project-card-state);
    &.project-success{
        background: rgba(52,152,219,.5);
    }
}
.project-card-fail-label,
.project-card-success-label{
    &:extend(.project-card-state-lbl);
}
.project-card-success-label{
    background: fade(@linkColor, 80%);
}


.project-card-no-progress{
    position: absolute;
    top: 0;
    left: -1px;
    right: -1px;
    border-bottom: 1px dashed #95a5a6;
}
.project-card-progress{
    position: absolute;
    top: 0;
    left: -1px;
    right: -1px;
    background: #d7dadf;
    .pcp-bar{
        display: block;
        max-width: 100%;
        height: 2px;
        background: @linkColor;
    }

    &.over-progress{
        background: #a8d0ed;
    }

    &.finish-fail{
        .pcp-bar{
            background: #acacac;
        }
    }
}

.project-card-item-finish{
    .project-card-name,
    .project-card-descr{
        color: #808080;
    }
}

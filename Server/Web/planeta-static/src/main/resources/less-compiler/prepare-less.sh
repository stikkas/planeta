#!/bin/bash

if [ "x$GRUNT_PLEASE" != "x" ]; then
    exit 0;
fi

PATH=$PATH:$HOME/bin
shopt -s nullglob
for fullfile in src/main/webapp/custom-css/*.less;
do
        filename=$(basename "${fullfile}")
        basename="${filename%.*}"
        echo ${filename}
        mkdir -p src/main/webapp/css-generated
        lessc src/main/webapp/custom-css/${filename} -x | postcss -u autoprefixer --autoprefixer.browsers "> 1%, last 2 versions, Firefox >= 16, Firefox ESR, Opera 12.1" -o src/main/webapp/css-generated/${basename}.css
        blessc -f src/main/webapp/css-generated/${basename}.css
#        if [ "$?" != "0" ]; then
#            exit 1;
#        fi
        echo "#_${basename}_css_{display:none;}" >> src/main/webapp/css-generated/${basename}.css
done

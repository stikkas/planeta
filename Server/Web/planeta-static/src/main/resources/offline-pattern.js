// Version "${version}"

var CACHE = 'cache-and-update${version}';
var CACHE_DYNAMIC = 'cache-dynamic${version}';

var PLANETA_FILES = [
    "/",
    "/api/util/campaign/total-purchase-sum.json",
    "/api/util/manifest.json",
    "/api/welcome/dashboard-filtered-campaigns.json?limit=12&offset=0",
    "/api/welcome/dashboard-recommended-campaigns.json?limit=12&offset=0&status=RECOMMENDED",
    "/api/util/random-banner-json.json?bannerType=PROMO_BANNER"
];

var FALLBACK = "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" x=\"0\" y=\"0\" width=\"100%\" height=\"100%\" viewBox=\"-10 -10 220 220\">\n" +
    "\t<title>planeta.ru-no-content</title>\n" +
    "\t<style>\n" +
    "\t\t.a{fill:#3498db;}\n" +
    "\t\t.b{fill:none; stroke:#3498db; stroke-width:16;}\n" +
    "\t\t.c{fill:#fff;}\n" +
    "\t\t.d{fill:#000;}\n" +
    "\t</style>\n" +
    "\t<circle cx=\"165\" cy=\"35\" r=\"20\" class=\"moon a\"/>\n" +
    "\t<circle cx=\"100\" cy=\"100\" r=\"60\" class=\"a\" />\n" +
    "\t<circle cx=\"100\" cy=\"100\" r=\"92\" class=\"b\" />\n" +
    "\n" +
    "</svg>\n";

function fromCache(request) {
    return caches.open(CACHE).then(function (cache) {
        return cache.match(request).then(function (matching) {
            return matching || Promise.reject('request-not-in-cache');
        });
    });
}

function update(request, cache) {
    return fetch(request).then(function (response) {
        cache.put(request, response.clone());
        return response;
    })
}

function fromDynamicCache(request) {
    return caches.open(CACHE_DYNAMIC).then(function (cache) {
        return cache.match(request).then(function (matching) {
            if (matching) {
                update(request, cache);
                return matching;
            }
            return update(request, cache);
        });
    });
}

function fromNetwork(request) {
    return fetch(request).then(function (response) {
        return caches.open(CACHE_DYNAMIC).then(function (cache) {
            return cache.match(request).then(function (matching) {
                if (matching) {
                    cache.put(request, response.clone());
                }
                return response;
            });
        });
    }).catch(function(err) {
        return caches.open(CACHE_DYNAMIC).then(function (cache) {
            return cache.match(request).then(function (matching) {
                return matching || Promise.reject('request-not-in-cache');
            });
        });
    });
}

function useFallback(request) {
    if (request.url.endsWith(".js") || request.url.endsWith(".json")
        || (request.url.indexOf(".json?") > 0) || (request.url.indexOf(".js?") > 0)) {
        return Promise.reject('request-not-in-fallback');
    }
    return Promise.resolve(new Response(FALLBACK, { headers: {
        'Content-Type': 'image/svg+xml'
    }}));
}


function сacheOrNetwork(request) {
    return fromCache(evt.request).catch(function() {
        evt.respondWith(fromDynamicCache(request));
        evt.waitUntil(update(request));
    });
}

function precache() {

    return caches.open(CACHE_DYNAMIC).then(function (cache) {
        return cache.addAll(PLANETA_FILES).then(function () {
            return caches.open(CACHE).then(function (cache) {
                return cache.addAll([${content}]).then(function () {
                    return self.skipWaiting();
                });
            });
        });
    });

}

self.addEventListener('install', function (e) {
    e.waitUntil(precache());
});


self.addEventListener('activate', function(event) {
    var cacheWhitelist = [CACHE, CACHE_DYNAMIC];

    event.waitUntil(
        caches.keys().then(function(keyList) {
            return Promise.all(keyList.map(function(key) {
                if (cacheWhitelist.indexOf(key) === -1) {
                    return caches.delete(key);
                }
            }));
        })
    );
});

self.addEventListener('fetch', function(evt) {
    evt.respondWith(fromCache(evt.request).catch(function () {
        return fromNetwork(evt.request).catch(function () {
            return useFallback(evt.request);
        });
    }));
});

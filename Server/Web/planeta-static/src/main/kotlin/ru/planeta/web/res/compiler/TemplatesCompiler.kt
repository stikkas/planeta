package ru.planeta.web.res.compiler

import org.apache.commons.io.FileUtils
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

import java.io.File
import java.util.*
import org.apache.commons.io.FilenameUtils

/**
 * Class for compiling templates configuration map
 * and also transforms template html file into json or jsonp.
 */
@Service
class TemplatesCompiler : AbstractResourceCompiler<TemplatesCompilerParams>() {

    @Value("\${templates.directory:}")
    lateinit var templatesDirectory: String

    /**
     * Reads template file and wraps it in jsonp.
     *
     */
    @Synchronized
    override fun compile(baseDirectory: String, fileName: String, params: TemplatesCompilerParams, flushCache: Boolean): String {
        val file = File(baseDirectory, FilenameUtils.concat(templatesDirectory, fileName))
        return FileUtils.readFileToString(file, "utf-8")
    }

    override fun getFiles(baseDirectory: String, fileName: String, params: TemplatesCompilerParams): Collection<File> {
        val result = HashSet<File>(1)
        result.add(File(baseDirectory, FilenameUtils.concat(templatesDirectory, fileName)))
        return result
    }

}

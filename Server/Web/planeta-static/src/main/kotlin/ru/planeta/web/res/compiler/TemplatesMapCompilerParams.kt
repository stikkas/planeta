package ru.planeta.web.res.compiler

/**
 * @author pvyazankin
 * @since: 03.10.12 17:16
 */
class TemplatesMapCompilerParams(var isFlushCache: Boolean, var baseUrl: String?, val lang: String) : AbstractCompilerParams() {

    override fun toString(): String {
        return "TemplatesMapCompilerParams{" +
                "baseUrl='" + baseUrl + '\'' +
                ", lang=" + lang +
                '}'
    }
}

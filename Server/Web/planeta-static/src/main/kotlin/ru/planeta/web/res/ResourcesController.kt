package ru.planeta.web.res

import org.apache.commons.io.FilenameUtils
import org.apache.commons.io.IOUtils
import org.apache.commons.lang3.StringUtils
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.view.InternalResourceViewResolver
import ru.planeta.web.res.compiler.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.TimeUnit
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Controller that manages static resources that need to be transformed dynamically.
 */
@Controller
class ResourcesController(private val javascriptCompiler: JavascriptCompiler,
                          private val templatesCompiler: TemplatesCompiler,
                          private val templatesMapCompiler: TemplatesMapCompiler,
                          private val manifestCompiler: ManifestCompiler) {

    @Autowired
    @Qualifier("viewResolver")
    lateinit var internalResourceViewResolver: InternalResourceViewResolver


    @Value("\${static.cache.checkFileRenewed}")
    var checkFileRenewed: Boolean = false

    private val templatesInternationalDirectory = "international-templates/"
    private val cache = ConcurrentHashMap<String, CacheElement<String>>()

    private class CacheElement<out T> constructor(val lastModified: Long, val obj: T)

    /**
     * Universal method for handling any resource
     *
     *
     * Note: average invocation time (before file modification monitoring) 7.143 ms
     */
    private fun <T : AbstractCompilerParams> handleRes(
            baseDirectory: String,
            file: String,
            compiler: AbstractResourceCompiler<T>,
            params: T,
            request: HttpServletRequest,
            response: HttpServletResponse,
            contentType: String,
            flushCache: Boolean) {

        var cacheElement: CacheElement<String>? = null
        val key = baseDirectory + file + params.cacheDiscriminator
        val ifModifiedSince = parseIfModifiedSince(request)

        if (flushCache) {
            cache.remove(key)
        } else {
            cacheElement = cache[key]
        }

        val isFileRenewed = (checkFileRenewed
                && ifModifiedSince != null
                && compiler.isAnyFileModified(baseDirectory, file, params, ifModifiedSince.getTime()))

        if (cacheElement == null || isFileRenewed) {
            val calendar = Calendar.getInstance()
            calendar.set(Calendar.MILLISECOND, 0)  // cut milliseconds!     Date format from browser doesn't contain them.

            cacheElement = CacheElement(calendar.timeInMillis, compiler.compile(baseDirectory, file, params, flushCache))
            cache.put(key, cacheElement)
            writeOutput(response, contentType, cacheElement.lastModified, cacheElement.obj)
        } else if (ifModifiedSince != null && ifModifiedSince.getTime() >= cacheElement.lastModified) {
            writeNotModified(response)
        } else {
            writeOutput(response, contentType, cacheElement.lastModified, cacheElement.obj)
        }

    }

    /**
     * NOTE: **Be aware, that @RequestMapping value is relational
     * (we have mapping /res/, so the real path is /res/js/templates.js**
     *
     * @param request  HTTP request
     * @param response HTTP response
     * @throws IOException
     */
    @GetMapping("/{baseUrl}/js/templates.js")
    @ResponseBody
    fun handleVersionedTemplatesMap(@RequestParam(defaultValue = "false") flushCache: Boolean,
                                    @RequestParam(defaultValue = "ru") lang: String,
                                    @PathVariable(value = "baseUrl") baseUrl: String,
                                    request: HttpServletRequest, response: HttpServletResponse) {
        handleRes(
                FilenameUtils.concat(request.session.servletContext.getRealPath(""), baseUrl),
                request.getHeader("Host"),
                templatesMapCompiler,
                TemplatesMapCompilerParams(flushCache, baseUrl, lang),
                request,
                response,
                "text/javascript; charset=utf-8",
                flushCache
        )
    }

    @GetMapping("/{baseUrl}/templates.json")
    @ResponseBody
    fun handleVersionedTemplatesMapJson(
            @RequestParam(defaultValue = "ru") lang: String,
            @PathVariable(value = "baseUrl") baseUrl: String,
            request: HttpServletRequest, response: HttpServletResponse) {
        val resp = templatesMapCompiler.compileJson(
                FilenameUtils.concat(request.session.servletContext.getRealPath(""), baseUrl),
                request.getHeader("Host"), TemplatesMapCompilerParams(false, baseUrl, lang))
        writeOutput(response, "application/json; charset=utf-8", System.currentTimeMillis(), resp)
    }

    @GetMapping("/templates.json")
    @ResponseBody
    fun handleTemplatesMapJson(@RequestParam(defaultValue = "ru") lang: String,
                               request: HttpServletRequest, response: HttpServletResponse) {
        val resp = templatesMapCompiler.compileJson(request.session.servletContext.getRealPath(""),
                request.getHeader("Host"), TemplatesMapCompilerParams(false, null, lang))
        writeOutput(response, "application/json; charset=utf-8", System.currentTimeMillis(), resp)
    }


    /**
     * NOTE: **Be aware, that @RequestMapping value is relational
     * (we have mapping /res/, so the real path is /res/js/templates.js**
     *
     * @param request  HTTP request
     * @param response HTTP response
     * @throws IOException
     */
    @GetMapping("/js/templates.js")
    @ResponseBody
    fun handleTemplatesMap(@RequestParam(defaultValue = "false") flushCache: Boolean,
                           @RequestParam(defaultValue = "ru") lang: String,
                           request: HttpServletRequest, response: HttpServletResponse) {
        handleRes(
                request.session.servletContext.getRealPath(""),
                request.getHeader("Host"),
                templatesMapCompiler,
                TemplatesMapCompilerParams(flushCache, null, lang),
                request,
                response,
                "text/javascript; charset=utf-8",
                flushCache
        )
    }

    @GetMapping("/{baseUrl}/js/{fileName}.js")
    @ResponseBody
    fun handleVersionedJavascript(@PathVariable(value = "fileName") fileName: String,
                                  @RequestParam(defaultValue = "false") flushCache: Boolean,
                                  @RequestParam(defaultValue = "true") compress: Boolean,
                                  @RequestParam(defaultValue = "ru") lang: String,
                                  @PathVariable(value = "baseUrl") baseUrl: String,
                                  request: HttpServletRequest, response: HttpServletResponse) {

        handleRes(
                FilenameUtils.concat(request.session.servletContext.getRealPath(""), baseUrl),
                fileName,
                javascriptCompiler,
                JavaScriptCompilerParams(compress, baseUrl, lang),
                request,
                response,
                "text/javascript; charset=utf-8",
                flushCache
        )
    }


    @GetMapping("/js/offline.js")
    @ResponseBody
    fun handleManifest(@RequestParam(defaultValue = "false") flushCache: Boolean,
                       @RequestParam(defaultValue = "false") compress: Boolean,
                       @RequestParam(defaultValue = "ru") lang: String,
                       request: HttpServletRequest, response: HttpServletResponse) {
        handleRes(
                request.session.servletContext.getRealPath(""),
                request.getHeader("Host"),
                manifestCompiler,
                ManifestCompilerParams(flushCache, null, lang, compress),
                request,
                response,
                "text/javascript; charset=utf-8",
                flushCache
        )
    }

    @GetMapping("{baseUrl}/js/offline.js")
    @ResponseBody
    fun handleManifestVersioned(@RequestParam(defaultValue = "false") flushCache: Boolean,
                                @RequestParam(defaultValue = "true") compress: Boolean,
                                @RequestParam(defaultValue = "ru") lang: String,
                                @PathVariable(value = "baseUrl") baseUrl: String,
                                request: HttpServletRequest, response: HttpServletResponse) {
        handleRes(
                FilenameUtils.concat(request.session.servletContext.getRealPath(""), baseUrl),
                request.getHeader("Host"),
                manifestCompiler,
                ManifestCompilerParams(flushCache, baseUrl, lang, compress),
                request,
                response,
                "text/javascript; charset=utf-8",
                flushCache
        )
    }

    @GetMapping("/js/{fileName}.js")
    @ResponseBody
    fun handleJavascript(@PathVariable(value = "fileName") fileName: String,
                         @RequestParam(defaultValue = "false") flushCache: Boolean,
                         @RequestParam(defaultValue = "true") compress: Boolean,
                         @RequestParam(defaultValue = "ru") lang: String,
                         request: HttpServletRequest, response: HttpServletResponse) {

        handleRes(
                request.session.servletContext.getRealPath(""),
                fileName,
                javascriptCompiler,
                JavaScriptCompilerParams(compress, null, lang),
                request,
                response,
                "text/javascript; charset=utf-8",
                flushCache
        )
    }


    @GetMapping("/{baseUrl}/templateFileJsp.html")
    fun handleVersionedTemplateJsp(@RequestParam fileName: String,
                                   @PathVariable(value = "baseUrl") baseUrl: String,
                                   request: HttpServletRequest, response: HttpServletResponse): String {
        internalResourceViewResolver.setPrefix("/$baseUrl/WEB-INF/jsp/")
        return templatesInternationalDirectory + fileName
    }

    @GetMapping("/templateFileJsp.html")
    fun handleTemplateJsp(@RequestParam fileName: String,
                          request: HttpServletRequest, response: HttpServletResponse): String {
        return templatesInternationalDirectory + fileName
    }


    @GetMapping("/{baseUrl}/templateFile.json")
    @ResponseBody
    fun handleVersionedTemplate(@RequestParam fileName: String,
                                @RequestParam callback: String,
                                @RequestParam(defaultValue = "true") flushCache: Boolean,
                                @PathVariable(value = "baseUrl") baseUrl: String,
                                request: HttpServletRequest, response: HttpServletResponse) {

        handleRes(
                FilenameUtils.concat(request.session.servletContext.getRealPath(""), baseUrl),
                fileName,
                templatesCompiler,
                TemplatesCompilerParams(callback),
                request,
                response,
                "application/json; charset=utf-8",
                flushCache
        )
    }


    @GetMapping("/templateFile.json")
    @ResponseBody
    fun handleTemplate(@RequestParam fileName: String,
                       @RequestParam callback: String,
                       @RequestParam(defaultValue = "false") flushCache: Boolean,
                       request: HttpServletRequest, response: HttpServletResponse) {

        handleRes(
                request.session.servletContext.getRealPath(""),
                fileName,
                templatesCompiler,
                TemplatesCompilerParams(callback),
                request,
                response,
                "application/json; charset=utf-8",
                flushCache
        )
    }


    @GetMapping("/{baseUrl}/templateFile.html")
    fun handleVersionedTemplateHtml(@RequestParam fileName: String,
                                    @RequestParam(defaultValue = "true") flushCache: Boolean,
                                    @PathVariable(value = "baseUrl") baseUrl: String,
                                    request: HttpServletRequest, response: HttpServletResponse) {
        handleRes(
                FilenameUtils.concat(request.session.servletContext.getRealPath(""), baseUrl),
                fileName,
                templatesCompiler,
                TemplatesCompilerParams(""),
                request,
                response,
                HTML_CONTENT_TYPE,
                flushCache
        )
    }


    @GetMapping("/templateFile.html")
    fun handleTemplateHtml(@RequestParam fileName: String,
                           @RequestParam(defaultValue = "true") flushCache: Boolean,
                           request: HttpServletRequest, response: HttpServletResponse) {

        handleRes(
                request.session.servletContext.getRealPath(""),
                fileName,
                templatesCompiler,
                TemplatesCompilerParams(""),
                request,
                response,
                HTML_CONTENT_TYPE,
                flushCache
        )
    }

    @GetMapping("/flushCache")
    fun flushCache(response: HttpServletResponse) {
        cache.clear()
        response.status = HttpServletResponse.SC_OK
    }

    companion object {

        private const val IF_MODIFIED_SINCE = "If-Modified-Since"
        private const val LAST_MODIFIED = "Last-Modified"
        private const val EXPIRES = "Expires"
        private const val CACHE_CONTROL = "Cache-Control"

        private const val HTML_CONTENT_TYPE = "text/html; charset=utf-8"
        private const val TEXT_CONTENT_TYPE = "text/plain; charset=utf-8"

        private val log = Logger.getLogger(ResourcesController::class.java)

        /**
         * Parses last modified date
         *
         * @param request Incoming request
         * @return A date parsed from If-Modified-Since header
         */
        private fun parseIfModifiedSince(request: HttpServletRequest?): Date? {
            if (request == null) {
                return null
            }

            val lastModifiedHeader = request.getHeader(IF_MODIFIED_SINCE)
            if (StringUtils.isEmpty(lastModifiedHeader)) {
                return null
            }
            return try {
                val dateFormat = SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z", Locale.ENGLISH)
                dateFormat.parse(lastModifiedHeader)
            } catch (ex: Exception) {
                log.warn("Error while parsing If-Modified-Since date: \"" + lastModifiedHeader + "\"", ex)
                null
            }

        }

        private fun writeOutput(response: HttpServletResponse, contentType: String, lastModified: Long, data: String) {
            response.contentType = contentType
            response.status = HttpServletResponse.SC_OK
            response.setDateHeader(LAST_MODIFIED, lastModified)
            response.setHeader(CACHE_CONTROL, "public; max-age=31536000")
            response.setDateHeader(EXPIRES, System.currentTimeMillis() + TimeUnit.DAYS.toMillis(365))
            response.setHeader("Access-Control-Allow-Origin", "*")
            val outputStream = response.getOutputStream()
            IOUtils.write(data, outputStream, "utf-8")
            outputStream.flush()
        }

        private fun writeNotModified(response: HttpServletResponse) {
            response.status = HttpServletResponse.SC_NOT_MODIFIED
        }
    }

}





package ru.planeta.web.res

import org.apache.commons.io.FileUtils
import org.apache.commons.lang3.StringUtils
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.RequestMapping

import javax.annotation.PostConstruct
import javax.servlet.ServletContext
import java.io.File
import java.io.FileFilter
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.Arrays

/**
 * Special class just for starting static service.
 * It prepares static service for work in @PostConstruct method.
 */
@Service
@Lazy(false)
class StaticServiceStarter {

    /**
     * We need servlet context for one purpose only:
     * to get base directory
     */
    @Autowired(required = false)
    lateinit var servletContext: ServletContext

    /**
     * List of versions to use
     */
    @Value("\${static.versions:}")
    lateinit var staticVersions: Array<String>

    /**
     * Directory to look up for static versions
     */
    @Value("\${static.versions.dir:}")
    lateinit var staticVersionsBaseDir: String

    /**
     * Checks if we need to do preparations or not
     *
     * @return true if we need to prepare
     */
    private fun checkParameters(): Boolean {

        if (staticVersions == null || staticVersions.size == 0) {
            log.info("Static content is not versioned")
            return false
        }

        if (StringUtils.isEmpty(staticVersionsBaseDir)) {
            log.info("Static versions directory is not set")
            return false
        }

        if (!File(staticVersionsBaseDir).exists()) {
            log.info("Static versions directory does not exist")
            return false
        }

        return servletContext != null

    }

    @RequestMapping("/stub.html")
    fun stubForPostConstructToWork() {
    }

    @PostConstruct
    fun init() {
        if (!checkParameters()) {
            log.info("Preparation is not needed, exiting")
            return
        }

        val baseDirectory = servletContext.getRealPath("")
        val currentStaticVersion = staticVersions[staticVersions.size - 1]
        log.info("Current static version is " + currentStaticVersion)

        copyErrorPages(baseDirectory, ERROR_JAVA_PATH, ERROR_SERVER_PATH)

        copyCurrentVersionStaticContent(baseDirectory, staticVersionsBaseDir, currentStaticVersion)

        for (version in staticVersions) {
            copyVersionToWorkingDir(baseDirectory, staticVersionsBaseDir, version)
        }
    }

    /**
     * Copies current version static content to the static versions directory
     *
     * @param baseDirectory        Base directory of the application. Static content should be got from here.
     * @param staticDir            Directory where static content is stored
     * @param currentStaticVersion Current version of the static content
     */
    private fun copyCurrentVersionStaticContent(baseDirectory: String, staticDir: String?, currentStaticVersion: String) {
        val currentVersionDirectory = File(staticDir, currentStaticVersion)

        if (currentVersionDirectory.exists()) {
            log.info("Deleting previous current version directory " + currentVersionDirectory.absolutePath)
            FileUtils.deleteDirectory(currentVersionDirectory)
        }

        val baseDir = File(baseDirectory)

        FileUtils.copyDirectory(baseDir, currentVersionDirectory) { file ->
            !(file.parentFile == baseDir
                    && file.isDirectory
                    && Arrays.binarySearch(staticVersions, file.name) >= 0)
        }
    }

    companion object {
        private val log = Logger.getLogger(StaticServiceStarter::class.java)

        private const val WEB_INF_PATH = "WEB-INF"
        private const val META_INF_PATH = "META-INF"
        private const val ERROR_JAVA_PATH = "WEB-INF/classes/errorpages/"
        private const val ERROR_SERVER_PATH = "/var/www/errorpages/"

        @JvmStatic
        private fun copyErrorPages(baseDirectory: String, errorLocation: String, staticDir: String) {
            try {
                val dstDir = File(staticDir)
                val baseDir = File(baseDirectory, errorLocation)
                log.info("Copying version from java " + baseDir.absolutePath + " to errorpages: " + dstDir)
                FileUtils.copyDirectory(baseDir, dstDir)
            } catch (ex: Exception) {
                log.error(ex)
            }

        }

        /**
         * Copies version files to specific directory in web application dir.
         *
         * @param baseDirectory Web app base directory
         * @param staticDir     Static directory
         * @param version       Version to copy
         */
        @JvmStatic
        private fun copyVersionToWorkingDir(baseDirectory: String, staticDir: String?, version: String) {

            val versionDirectory = File(staticDir, version)

            if (!versionDirectory.exists()) {
                log.warn("Version directory does not exist: " + versionDirectory.absolutePath)
                return
            }

            val webappVersionDirectory = File(baseDirectory, version)

            if (webappVersionDirectory.exists()) {
                log.info("Deleting webapp version directory: " + webappVersionDirectory.absolutePath)
                FileUtils.deleteDirectory(webappVersionDirectory)
            }

            log.info("Copying version from " + versionDirectory.absolutePath + " to webapp: " + webappVersionDirectory)
            FileUtils.copyDirectory(versionDirectory, webappVersionDirectory)

            log.info("Removing META-INF directory from static content version")
            val metaInfDirectory = File(webappVersionDirectory, META_INF_PATH)
            FileUtils.deleteDirectory(metaInfDirectory)

            log.info("Version $version is ready")
        }

        // Jetty doesn't work with links
        @JvmStatic
        private fun createLinks(baseDirectory: String, staticDir: String, version: String) {

            val versionDirectory = Paths.get(staticDir, version)

            if (!Files.exists(versionDirectory)) {
                log.warn("Version directory does not exist: " + versionDirectory)
                return
            }

            val linkName = Paths.get(baseDirectory, version)

            log.info("create link on $versionDirectory to webapp: $linkName")
            Files.createSymbolicLink(linkName, versionDirectory)

            log.info("Version $version is ready")
        }

    }
}


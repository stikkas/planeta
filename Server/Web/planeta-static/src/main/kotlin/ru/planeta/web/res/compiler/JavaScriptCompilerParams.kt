package ru.planeta.web.res.compiler

/**
 * If compress=true -- compresses all files
 *
 * @author pvyazankin
 * @since: 03.10.12 17:04
 */
class JavaScriptCompilerParams
/**
 * If compress=true -- compresses all files
 */
(val isCompress: Boolean, val baseUrl: String?, val lang: String) : AbstractCompilerParams() {

    override fun toString(): String {
        return "JavaScriptCompilerParams{" +
                "compress=" + isCompress +
                ", baseUrl='" + baseUrl + '\'' +
                ", lang='" + lang + '\'' +
                '}'
    }

}

package ru.planeta.web.res.compiler

import com.google.javascript.jscomp.CompilationLevel
import com.google.javascript.jscomp.Compiler
import com.google.javascript.jscomp.CompilerOptions
import com.google.javascript.jscomp.JSSourceFile
import org.apache.commons.io.FileUtils
import org.apache.commons.io.IOUtils
import org.apache.commons.lang3.StringUtils
import org.apache.log4j.Logger
import org.springframework.stereotype.Service

import java.io.File
import java.io.FileNotFoundException
import java.io.FileReader
import java.io.IOException
import java.net.URISyntaxException
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * Compiles javascript files into on path
 */
@Service
class JavascriptCompiler : AbstractResourceCompiler<JavaScriptCompilerParams>() {

    private var failOnError = false

    private val backboneClass = TreeSet<String>(
            setOf("Backbone.Events",
                    "Backbone.Model",
                    "Backbone.Collection",
                    "Backbone.View",
                    "Backbone.Router"))

    private var javascriptPropertiesMap: MutableMap<String, Set<String>>? = null
    private var javascriptPropertiesLastModification: Long = 0

    // for tests
    internal fun setFailOnError(failOnError: Boolean) {
        this.failOnError = failOnError
    }

    /**
     * Compiles javascript into one big path
     *
     * @param baseDirectory Base directory
     * @param fileName      Name of the path (we search for it in javascript.properties path)
     * @param params        If true -- flushes cache for this fileName
     * @return Compiled javascript
     */
    @Synchronized
    override fun compile(baseDirectory: String, fileName: String, params: JavaScriptCompilerParams, flushCache: Boolean): String {
        val files = getFiles(baseDirectory, fileName, params)
        val compiled = compileFiles(baseDirectory, params, files)
        return compiled.toString()
    }

    private fun compileFiles(baseDirectory: String, params: JavaScriptCompilerParams, files: Collection<File>?): StringBuilder {
        val compiled = StringBuilder()
        if (!params.isCompress) {
            compiled.append("window.debugMode=true;\r\n")
        }

        files?.forEach { file ->

            if (!file.exists()) {
                throw IOException("File " + file.absolutePath + " does not exists")
            }

            var javascript = StringUtils.strip(FileUtils.readFileToString(file))

            if (!params.isCompress) {
                compiled
                        .append("/******** Start: ")
                        .append(file.name)
                        .append(" ******** size = ").append(javascript.length).append(' ')
                        .append("********/")
                        .append("\r\n")
                        .append("window.currentFileName = \"").append(file.name).append("\";\r\n")

            }
            if (!file.name.endsWith("min.js")) {
                if (params.isCompress) {
                    log.info("Compiling file " + file.name)
                    javascript = StringUtils.strip(nativeToAsciiReverse(compileFile(javascript)))
                    log.info("File " + file.name + " has been compiled, returning.")
                } else {
                    javascript = addBackboneClassName(removeTryCatch(javascript))
                }
            }

            if (StringUtils.isBlank(javascript)) {
                log.error("Syntax error in file " + file.name)
                if (failOnError) {
                    throw IOException("Syntax error in file " + file.absolutePath)
                }
                javascript = "alert('Syntax error in file " + File(baseDirectory).toURI().relativize(file.toURI()) + "');"
            }
            compiled.append(javascript)
            // NOTE: We need this to properly concatenate files
            if (!StringUtils.endsWith(javascript, ";")) {
                compiled.append(";")
            }

            if (!params.isCompress) {
                compiled.append("/******** End: ")
                compiled.append(file.name)
                compiled.append("********/")
                compiled.append("\r\n")
            }

        }
        return compiled
    }

    /**
     * Search in the txt string like "class.name = parent.class.name.extend({"
     * if parent.class.name is in the set backboneClass,
     * then add to the class.name field backboneClassName (i.e class.name = parent.class.name({class.name.backboneClassName="class.name")
     * and add the class.name in the set backboneClass
     *
     */
    private fun addBackboneClassName(txt: String): String {
        var copyFrom = 0
        var result: StringBuilder? = null
        val txtWithoutComments = replaceCommentsBySpace(txt)
        val matcher = BACKBONE_CLASS_DEFINITION_PATTERN.matcher(txtWithoutComments)
        while (matcher.find()) {
            if (result == null) {
                result = StringBuilder(txt.length + txt.length / 16)
            }
            val curClass = matcher.group(1)
            val parentClass = matcher.group(2)
            val isEmptyClass = matcher.group(3) != null

            if (backboneClass.contains(parentClass)) {
                if (!curClass.isEmpty()) {
                    backboneClass.add(curClass)
                    val copyTo = if (isEmptyClass) matcher.start(3) else matcher.end()
                    result.append(txt.substring(copyFrom, copyTo))
                    copyFrom = copyTo
                    result.append("backboneClassName:'").append(curClass).append("'")
                    if (!isEmptyClass) {
                        result.append(',')
                    }
                }
            }
        }
        if (result != null) {
            result.append(txt.substring(copyFrom))
            return result.toString()
        }
        return txt
    }

    private fun getJavascriptPropertiesMap(baseDir: String): Map<String, Set<String>> {
        val file = File(baseDir + "/" + JAVASCRIPT_PROPERTIES_FILE_NAME)
        var jsPM = javascriptPropertiesMap
        if (jsPM != null && file.lastModified() <= javascriptPropertiesLastModification) {
            return jsPM
        }
        javascriptPropertiesLastModification = file.lastModified()
        if (jsPM == null) {
            jsPM = HashMap()
            javascriptPropertiesMap = jsPM
        }
        val reader = FileReader(file)
        try {
            val properties = Properties()
            properties.load(reader)

            val enumeration = properties.propertyNames()
            while (enumeration.hasMoreElements()) {
                val propertyName = enumeration.nextElement() as String
                val set = properties.getProperty(propertyName)
                        .split(",".toRegex())
                        .dropLastWhile { it.isEmpty() }
                        .toTypedArray()
                        .mapTo(LinkedHashSet<String>()) { it.trim(' ') }
                jsPM.put(propertyName, set)
            }


        } catch (e: Exception) {
            log.error(e)
        } finally {
            IOUtils.closeQuietly(reader)
        }
        return jsPM
    }

    override fun getFiles(baseDirectory: String, fileName: String, params: JavaScriptCompilerParams): Collection<File>? {
        val jsMap = getJavascriptPropertiesMap(baseDirectory)
        return getFiles(baseDirectory, MESSAGE_PREFIX + fileName, jsMap, params.lang)
    }

    companion object {

        private val MESSAGE_PREFIX = "resources.js."
        private val DEFAULT_LANGUAGE = "ru"
        private val log = Logger.getLogger(JavascriptCompiler::class.java)

        /**
         * Obfuscates specified path
         *
         * @param sourceText String to compile
         * @return String
         */

        private fun compileFile(sourceText: String): String {
            val compiler = Compiler()
            val compilerOptions = CompilerOptions()
            compilerOptions.languageIn = CompilerOptions.LanguageMode.ECMASCRIPT5
            CompilationLevel.SIMPLE_OPTIMIZATIONS.setOptionsForCompilationLevel(compilerOptions)
            val jsSourceFile = JSSourceFile.fromCode("input.js", sourceText)
            val jsOutputFile = JSSourceFile.fromCode("output.js", "")
            compiler.compile(jsOutputFile, jsSourceFile, compilerOptions)
            return compiler.toSource()
        }

        private fun nativeToAsciiReverse(javascript: String): String {
            return ru.planeta.commons.lang.StringUtils.nativeToAsciiReverse(javascript)
        }

        private val BACKBONE_CLASS_DEFINITION_PATTERN = Pattern.compile("\\b([\\w.]+)\\s*=\\s*([\\w.]+)\\.extend\\s*\\(\\s*\\{(\\s*\\})?")

        private val commentOrQuotes = Pattern.compile("//|/\\*|'|\"")

        /**
         * Replace all comments by space
         *
         */
        private fun replaceCommentsBySpace(txt: String): String {
            var result: StringBuilder? = null
            var copyFrom = 0
            var pos = 0
            val matcher = commentOrQuotes.matcher(txt)
            while (matcher.find(pos)) {
                val foundStr = matcher.group()
                pos = matcher.start()
                if (foundStr == "\"" || foundStr == "'") {
                    pos = findEndQuote(txt, foundStr[0], pos + 1)
                    if (pos != txt.length) {
                        pos++
                    }
                } else {
                    if (result == null) {
                        result = StringBuilder(txt.length)
                    }

                    var end: Int
                    if (foundStr == "//") {
                        end = txt.indexOf('\n', pos)
                        end = if (end < 0) txt.length else end
                    } else {
                        end = txt.indexOf("*/", pos)
                        end = if (end < 0) txt.length else end + 2
                    }

                    result.append(txt.substring(copyFrom, pos))
                    copyFrom = pos
                    while (copyFrom < end) {
                        result.append(' ')
                        copyFrom++
                    }
                    pos = copyFrom
                }
            }
            if (result != null) {
                result.append(txt.substring(copyFrom))
                return result.toString()
            }
            return txt
        }

        private fun findEndQuote(txt: String, quote: Char, atPos: Int): Int {
            var pos = atPos
            while (pos < txt.length) {
                pos = txt.indexOf(quote, pos)
                if (pos < 0) {
                    return txt.length
                }
                if (pos == atPos || txt[pos - 1] != '\\') {
                    return pos
                }
                pos++
            }
            return pos
        }

        private val JAVASCRIPT_PROPERTIES_FILE_NAME = "javascript.properties"


        private fun getFiles(baseDirectory: String, propertyName: String, jsMap: Map<String, Set<String>>, lang: String): Collection<File>? {
            var result: Collection<File>? = null
            if (jsMap.containsKey(propertyName)) {
                result = LinkedHashSet()
                jsMap[propertyName]?.forEach {
                    if (it.startsWith("\${")) {
                        var localizeFiles = getFiles(baseDirectory, StringUtils.substringBetween(it, "\${", "}")
                                + (if (isItNotDefaultLang(lang)) "_" + lang else ""), jsMap, lang)
                        if (localizeFiles != null && !localizeFiles.isEmpty()) {
                            result.addAll(localizeFiles)
                        } else {
                            result.addAll(getFiles(baseDirectory, StringUtils.substringBetween(it, "\${", "}"), jsMap, lang) as Collection<File>)
                        }
                    } else {
                        result.add(File(baseDirectory, it))
                    }
                }
            }
            return result
        }

        private fun isItNotDefaultLang(lang: String): Boolean {
            return StringUtils.isNotEmpty(lang) && DEFAULT_LANGUAGE != lang
        }

        private fun removeTryCatch(tx: String): String {
            try {
                return RemoveTryCatch.remove(tx)
            } catch (e: RuntimeException) {
                log.error(e)
            }

            return tx
        }
    }
}
package ru.planeta.web.res.compiler

import org.apache.commons.io.FileUtils
import org.apache.commons.io.FilenameUtils
import org.apache.commons.io.IOUtils
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

import java.io.File
import java.io.IOException
import java.io.UnsupportedEncodingException
import java.net.URLEncoder

/**
 * Class for compiling templates configuration map
 */
@Service
class ManifestCompiler : AbstractResourceCompiler<ManifestCompilerParams>() {

    @Value("\${templates.directory:}")
    lateinit var templatesDirectory: String

    @Value("\${templates.international.directory:}")
    lateinit var templatesInternationalDirectory: String

    /**
     * Compiles templates map javascript object.
     *
     * @param baseDirectory Base web application directory
     * @param host          Web application host (got from request)
     * @return compiled templates map
     */
    @Synchronized
    override fun compile(baseDirectory: String, host: String, params: ManifestCompilerParams, flushCache: Boolean): String {

        val content = getStringFromFile(OFFLINE_FILE_PATTERN)
        val staticContent = getStringBuilderStatic(baseDirectory, host, params)
        var returnStr = content.replace("\${content}", staticContent)

        return if (params.baseUrl != null) {
            returnStr.replace("\${version}", params.baseUrl!!)
        } else {
            returnStr.replace("\${version}", "planetaOfflineStorage")
        }

    }

    private fun getStringBuilderStatic(baseDirectory: String, host: String, params: ManifestCompilerParams): String {
        val sb = StringBuilder()
        sb.append("\"")
        scanOneFile(host, params, "templates.json", sb)
        for (mainfile in mainFiles) {
            sb.append(PREFIX)
            scanOneFile(host, params, mainfile, sb)
        }

        for (cssFileToAdd in cssToOffline) {
            addHostWithVersion(host, params, sb)
            sb.append("/css-generated/")
            sb.append(cssFileToAdd)
        }

        for (file in pictures) {
            addHostWithVersion(host, params, sb)
            sb.append("/images/")
            sb.append(file)
        }

        for (file in fonts) {
            addHostWithVersion(host, params, sb)
            sb.append("/fonts/")
            sb.append(file)
        }


        val files = getFiles(baseDirectory, host, params)
        for (file in files) {
            sb.append(PREFIX)
            scanOneFile(host, params, file.name, sb)
        }
        sb.append("\"")
        return sb.toString()
    }

    override fun getFiles(baseDirectory: String, fileName: String, params: ManifestCompilerParams): Collection<File> {
        val files = FileUtils.listFiles(File(baseDirectory, templatesDirectory!!), null, true)
        files.addAll(FileUtils.listFiles(File(baseDirectory, templatesInternationalDirectory!!), null, true))
        return files
    }

    companion object {

        private val log = Logger.getLogger(ManifestCompiler::class.java)
        private const val OFFLINE_FILE_PATTERN = "offline-pattern.js"

        private val productionDefaults = ManifestCompilerParams(false, null, "ru", true)

        private val cssToOffline = arrayOf("common.css", "about.css", "project.css", "common-mobile.css")

        private val pictures = arrayOf("planeta-ico.svg")

        private val fonts = arrayOf("proximanova-reg-webfont.woff", "proximanova-bold-webfont.woff", "icomoon.ttf", "alsrubl-arial-regular.woff", "proxima_nova_semibold-webfont.woff")

        private val mainFiles = arrayOf("planeta.js", "header.js", "header-search.js", "welcome.js")
        private const val PREFIX = "\",\n\""


        private fun getStringFromFile(sourcePath: String): String =
                IOUtils.toString(ManifestCompiler::class.java.classLoader.getResourceAsStream(sourcePath))

        private fun addHostWithVersion(host: String, params: ManifestCompilerParams, sb: StringBuilder) {
            sb.append(PREFIX).append("//").append(host)
            if (params.baseUrl != null) {
                sb.append("/").append(params.baseUrl)
            }
        }

        private fun scanOneFile(host: String, params: ManifestCompilerParams, fileName: String, sb: StringBuilder) {
            try {
                sb.append("//").append(host).append("/res/")
                if (params.baseUrl != null) {
                    sb.append(params.baseUrl).append("/")
                }

                val useInternational = addName(fileName, params, sb)
                if (productionDefaults.isFlushCache != params.isFlushCache && fileName.endsWith(".js")) {
                    sb.append("&flushCache=").append(params.isFlushCache)
                }


                if (useInternational && productionDefaults.lang != params.lang) {
                    sb.append("&language=").append(params.lang)
                }
            } catch (e: IOException) {
                log.error("Error scanning file " + fileName, e)
                throw e
            }

        }

        private fun addName(fileName: String, params: ManifestCompilerParams, sb: StringBuilder): Boolean {
            val useInternational: Boolean
            if (fileName.endsWith(".js")) {
                sb.append("js/").append(fileName)
                if (params.isCompressJs != productionDefaults.isCompressJs) {
                    sb.append("?compress=").append(params.isCompressJs)
                }
                return false
            }
            if (fileName.endsWith(".json")) {
                sb.append(fileName)
                return false
            }
            var name = URLEncoder.encode(fileName, "utf-8")
            if (FilenameUtils.isExtension(name, "jsp")) {
                sb.append("templateFileJsp.html?fileName=")
                name = FilenameUtils.removeExtension(name)
                useInternational = true
            } else {
                sb.append("templateFile.html?fileName=")
                useInternational = false

            }
            sb.append(name)
            return useInternational
        }
    }

}

package ru.planeta.web.res.compiler

import org.apache.commons.io.FileUtils
import org.apache.commons.io.FilenameUtils
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

import java.io.File
import java.io.IOException
import java.net.URLEncoder
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * Class for compiling templates configuration map
 */
@Service
class TemplatesMapCompiler : AbstractResourceCompiler<TemplatesMapCompilerParams>() {

    @Value("\${templates.directory:}")
    lateinit var templatesDirectory: String

    @Value("\${templates.international.directory:}")
    lateinit var templatesInternationalDirectory: String


    /**
     * Compiles templates map javascript object.
     *
     *
     * @param baseDirectory Base web application directory
     * @param host          Web application host (got from request)
     * @return compiled templates map
     */
    @Synchronized
    override fun compile(baseDirectory: String, host: String, params: TemplatesMapCompilerParams, unused: Boolean): String {
        val sb = StringBuilder()
        sb.append("var FileTemplates = {")
        compileInner(baseDirectory, host, params, sb)
        return sb.append("};").toString()
    }

    @Synchronized
    fun compileJson(baseDirectory: String, host: String, params: TemplatesMapCompilerParams): String {
        val sb = StringBuilder()
        sb.append("{")
        compileInner(baseDirectory, host, params, sb)
        return sb.append("}").toString()
    }

    private fun compileInner(baseDirectory: String, host: String, params: TemplatesMapCompilerParams, sb: StringBuilder) {
        val files: Collection<File> = getFiles(baseDirectory, host, params)

        var prefix = ""
        for (file in files) {
            sb.append(prefix)
            prefix = ",\n"
            sb.append(scanOneFile(file, host, params))
        }
    }

    override fun getFiles(baseDirectory: String, unused: String, params: TemplatesMapCompilerParams): Collection<File> {
        val files = FileUtils.listFiles(File(baseDirectory, templatesDirectory!!), null, true)
        files.addAll(FileUtils.listFiles(File(baseDirectory, templatesInternationalDirectory!!), null, true))
        return files
    }

    private fun scanOneFile(file: File, host: String, params: TemplatesMapCompilerParams): String {
        try {
            val sb = StringBuilder()
            sb.append("\"//").append(host).append("/res/")
            if (params.baseUrl != null) {
                sb.append(params.baseUrl).append("/")
            }

            var name = URLEncoder.encode(file.name, "utf-8")
            var useInternational = false
            if (FilenameUtils.isExtension(name, "jsp")) {
                sb.append("templateFileJsp.html?fileName=")
                name = FilenameUtils.removeExtension(name)
                useInternational = true
            } else {
                sb.append("templateFile.html?fileName=")
            }
            sb.append(name)
            if (productionDefaults.isFlushCache != params.isFlushCache) {
                sb.append("&flushCache=").append(params.isFlushCache)
            }


            if (useInternational && productionDefaults.lang != params.lang) {
                sb.append("&language=").append(params.lang)
            }

            val text = FileUtils.readFileToString(file)
            val matcher = PATTERN.matcher(text)
            makeArrayOfTempateIds(matcher, sb)
            return sb.toString()
        } catch (e: IOException) {
            log.error("Error scanning file " + file.name, e)
            throw e
        }

    }

    companion object {

        private val PATTERN = Pattern.compile("<script.*?id=(\"|')(.*?)(\"|')", Pattern.CASE_INSENSITIVE or Pattern.DOTALL)
        private val log = Logger.getLogger(TemplatesMapCompiler::class.java)

        private val productionDefaults = TemplatesMapCompilerParams(false, null, "ru")

        private fun makeArrayOfTempateIds(matcher: Matcher, sb: StringBuilder) {
            sb.append("\":[\n")

            var i = 0
            while (matcher.find()) {
                if (i > 0) {
                    sb.append(",\n")
                }
                val id = matcher.group(2)
                sb.append("\"").append(id).append("\"")
                i++
            }
            sb.append("\n]")
        }
    }
}

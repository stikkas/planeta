package ru.planeta.web.res.compiler

/**
 * @author asavan
 * @since: 27.11.16 19:37
 */
class ManifestCompilerParams(var isFlushCache: Boolean, var baseUrl: String?, val lang: String, var isCompressJs: Boolean) : AbstractCompilerParams() {

    override fun toString(): String {
        return "ManifestCompilerParams{" +
                "baseUrl='" + baseUrl + '\'' +
                ", lang=" + lang +
                '}'
    }
}

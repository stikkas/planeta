package ru.planeta.web.res.compiler

import org.apache.commons.lang3.StringUtils

object RemoveTryCatch {

    //    static {
    //        map.put("base-router.js", new String[]{"doNavigate", "onNavigationStateChanged"});
    //        map.put("app-view.js", new String[]{"replaceViews"});
    //        map.put("backbone.js", new String[]{"renderView", "onAdd"});
    //        map.put("template-manager.js", new String[]{"fetchAndCompileTemplate", "applyTemplate"});
    //        StringBuilder sb = new StringBuilder();
    //        for (String fileName : map.keySet()) {
    //            sb.append(fileName).append(' ');
    //        }
    //        fileNames = sb.toString();
    //    }

    private const val REMOVE_TRY_CATCH_OPEN_COMMENT = "/*removed-try-catch* "
    private const val REMOVE_TRY_CATCH_CLOSE_COMMENT = " *removed-try-catch*/"
    private const val REMOVE_TRY_CATCH_COMMENT = "//removeTryCatch"


    fun remove(tx: String): String {
        var tx = tx
        while (tx.contains(REMOVE_TRY_CATCH_COMMENT)) {
            tx = removeTryCatch(tx)
        }
        return tx
    }


    private fun removeTryCatch(tx: String): String {
        val sb = StringBuilder(tx.length)
        val txWithoutComments = removeComments(tx)
        var trySt = tx.lastIndexOf(REMOVE_TRY_CATCH_COMMENT)
        val commentEn = trySt + REMOVE_TRY_CATCH_COMMENT.length
        if (prevWord(txWithoutComments, trySt) != "{") {
            throw RuntimeException("not fount 'try {' before //removeTryCatch")
        }
        val tryEn = tx.lastIndexOf("{", trySt)
        if (prevWord(txWithoutComments, tryEn - 1) != "try") {
            throw RuntimeException("not fount 'try {' before //removeTryCatch")
        }
        trySt = tx.lastIndexOf("try", trySt)

        val catchSt = findCloseBracket(txWithoutComments, '{', '}', tryEn)

        if (catchSt < 0) {
            throw RuntimeException("not found close } after 'try'")
        }

        val catchEn = nextChar(txWithoutComments, '{', catchSt)

        if (catchEn < 0) {
            throw RuntimeException("not found { after 'catch'")
        }
        if (!StringUtils.deleteWhitespace(txWithoutComments.substring(catchSt + 1, catchEn)).startsWith("catch(")) {
            throw RuntimeException("catch block must start with 'catch(', but found \n " + tx.substring(catchSt, catchEn + 1))
        }

        val finallySt = findCloseBracket(txWithoutComments, '{', '}', catchEn)
        if (finallySt < 0) {
            throw RuntimeException("not found close '}' after 'catch{' ")
        }

        sb.append(tx.substring(0, trySt))
                .append(REMOVE_TRY_CATCH_OPEN_COMMENT)
                .append(tx.substring(trySt, tryEn + 1))
                .append(REMOVE_TRY_CATCH_CLOSE_COMMENT)
                .append(tx.substring(commentEn, catchSt))
                .append(REMOVE_TRY_CATCH_OPEN_COMMENT)
        var en = 0
        if (nextWord(txWithoutComments, finallySt + 1) == "finally") {
            val finallyEn = findCloseBracket(txWithoutComments, '}', '{', finallySt)
            if (finallyEn < 0) {
                throw RuntimeException("not found '{' after '} finally' in \n" + tx)
            }
            if (StringUtils.deleteWhitespace(txWithoutComments.substring(finallySt + 1, finallyEn)) != "finally") {
                throw RuntimeException("finally block must contains 'finally', but found \n " + tx.substring(finallySt, finallyEn + 1) + "\n" + tx)
            }
            sb.append(txWithoutComments.substring(catchSt, finallyEn + 1))
            sb.append(REMOVE_TRY_CATCH_CLOSE_COMMENT)

            en = findCloseBracket(txWithoutComments, '{', '}', finallyEn)
            if (en < 0) {
                throw RuntimeException("not found close '}' after 'finally'\n" + tx)
            }
            sb.append(tx.substring(finallyEn + 1, en))
            sb.append(REMOVE_TRY_CATCH_OPEN_COMMENT)
                    .append("}")
                    .append(REMOVE_TRY_CATCH_CLOSE_COMMENT)

        } else {
            en = finallySt
            sb.append(tx.substring(catchSt, en + 1))
            sb.append(REMOVE_TRY_CATCH_CLOSE_COMMENT)
        }


        sb.append(tx.substring(en + 1, tx.length))
        return sb.toString()

    }

    private fun removeComments(s: String): String {
        return removeOneLineComments(removeMultiLineComments(s))
    }

    private fun removeOneLineComments(s: String): String {
        val sb = StringBuilder(s)
        var en = 0
        while (true) {
            val st = s.indexOf("//", en)
            if (st < 0) {
                break
            }
            en = s.indexOf('\n', st)
            if (en < 0) {
                en = s.length
            }
            for (i in st until en) {
                sb.setCharAt(i, ' ')
            }
        }
        return if (en == 0) {
            s
        } else sb.toString()
    }

    private fun removeMultiLineComments(s: String): String {
        val sb = StringBuilder(s)
        var en = 0
        while (true) {
            val st = s.indexOf("/*", en)
            if (st < 0) {
                break
            }
            en = s.indexOf("*/", st)
            if (en < 0) {
                en = s.length
            } else {
                en += 2
            }
            for (i in st until en) {
                sb.setCharAt(i, ' ')
            }
        }
        return if (en == 0) {
            s
        } else sb.toString()
    }

    private fun nextChar(s: String, c: Char, pos: Int): Int {
        var pos = pos
        val len = s.length
        while (pos < len && s[pos] != c) {
            pos++
        }
        return if (pos < len) pos else -1
    }

    private fun nextWord(s: String, pos: Int): String {
        var pos = pos
        val len = s.length
        while (pos < len) {
            if (" \t\r\n\b".indexOf(s[pos]) < 0) {
                break
            }
            pos++
        }
        if (pos >= len) {
            return ""
        }
        val st = pos
        while (pos < len) {
            if (" \t\r\n\b".indexOf(s[pos]) >= 0) {
                break
            }
            pos++
        }

        return s.substring(st, pos)
    }

    private fun prevWord(s: String, pos: Int): String {
        var pos = pos
        while (pos >= 0) {
            if (" \t\r\n\b".indexOf(s[pos]) < 0) {
                break
            }
            pos--
        }
        if (pos < 0) {
            return ""
        }
        val en = pos
        while (pos >= 0) {
            if (" \t\r\n\b".indexOf(s[pos]) >= 0) {
                break
            }
            pos--
        }

        return s.substring(pos + 1, en + 1)
    }

    private fun findCloseBracket(s: String, openBracket: Char, closeBracket: Char, pos: Int): Int {
        var pos = pos
        val len = s.length
        var lvl = 0
        while (pos < len) {
            val curChar = s[pos]
            if (curChar == openBracket) {
                lvl++
            } else if (curChar == closeBracket) {
                lvl--
                if (lvl < 1) {
                    break
                }
            }
            pos++
        }
        return if (pos < len) pos else -1
    }


}

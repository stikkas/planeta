package ru.planeta.web.res.compiler

/**
 * @author pvyazankin
 * @since: 03.10.12 17:13
 */
class TemplatesCompilerParams(var callback: String?) : AbstractCompilerParams() {

    override val cacheDiscriminator: String
        get() = ""

    override fun toString(): String {
        return "TemplatesCompilerParams{" +
                "callback='" + callback + '\'' +
                '}'
    }
}

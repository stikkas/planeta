package ru.planeta.web.res.compiler

import org.apache.commons.io.FileUtils

import java.io.File
import java.io.IOException

/**
 * @author pvyazankin
 * Date: 03.10.12 16:59
 */
abstract class AbstractResourceCompiler<in T : AbstractCompilerParams> {

    abstract fun compile(baseDirectory: String, fileName: String, params: T, flushCache: Boolean): String

    /**
     * Checks is one of required files was modyfied later than lastModified.
     */
    fun isAnyFileModified(baseDirectory: String, fileName: String, params: T, lastModified: Long): Boolean =
        getFiles(baseDirectory, fileName, params)?.any { FileUtils.isFileNewer(it, lastModified) } ?: false

    abstract fun getFiles(baseDirectory: String, fileName: String, params: T): Collection<File>?
}

package ru.planeta.web.res.compiler

/**
 * @author pvyazankin
 * @since: 03.10.12 17:01
 */
abstract class AbstractCompilerParams {

    /**
     * Gets discriminator for caching response
     * of the request with this request.
     */
    open val cacheDiscriminator: String
        get() = toString()

    /**
     * All parameters should be used in toString output due to cache needs
     */
    abstract override fun toString(): String
}

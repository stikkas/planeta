package ru.planeta.web.res

import org.junit.Ignore
import org.junit.Test
import java.io.File
import java.io.FileNotFoundException
import java.util.Scanner
import java.util.TreeSet
import java.util.regex.Matcher
import java.util.regex.Pattern

class AnalyzeCss {

    private val cssClasses = TreeSet<String>()
    private val cssIds = TreeSet<String>()

    @Ignore
    @Test
    fun test() {

        val sb = StringBuilder()
        sb.append(readFile("Server/Web/planeta-static/src/main/webapp/css-generated/bootstrap.css"))
                .append(" ").append(readFile("Server/Web/planeta-static/src/main/webapp/css-generated/bootstrap-blessed1.css"))
                .append(" ").append(readFile("Server/Web/planeta-static/src/main/webapp/css-generated/bootstrap-blessed2.css"))
        val css = sb.toString()
        val matcher = Pattern.compile("[\\.#][\\w-]+").matcher(css.replace("\\{[^\\}]+\\}".toRegex(), " "))
        while (matcher.find()) {
            if (matcher.group().startsWith("#")) {
                cssIds.add(matcher.group().substring(1))
            } else {
                cssClasses.add(matcher.group().substring(1))
            }
        }

        println("CssClasses " + cssClasses.size + " cssIds " + cssIds.size)
        val dir = File("Server/")
        analyze(dir)
        println(" ****************** Class ****************** ")
        println("CssClasses " + cssClasses.size + " cssIds " + cssIds.size)
        var totalBytes = 0
        for (clazz in cssClasses) {
            val countBytes = getCountBytes(sb, clazz)
            println(clazz + "\t" + countBytes)
            totalBytes += countBytes
        }

        println(" ****************** ID ****************** ")
        for (clazz in cssIds) {
            println(clazz)
        }

        println("totalBytes " + totalBytes)

    }

    private fun getCountBytes(sb: StringBuilder, clazz: String): Int {
        var clazz = clazz
        clazz = "." + clazz
        var countBytes = 0

        while (true) {
            var st = sb.indexOf(clazz)
            if (st < 0) {
                break
            }
            val en = sb.indexOf("}", st)

            st = Math.max(sb.lastIndexOf("}", st), sb.lastIndexOf("{", st))
            if (st < 0 || en < 0) {
                break
            }
            countBytes += en - st
            st++
            while (st < en && sb[st] != '{') {
                sb.setCharAt(st++, ' ')
            }

        }
        return countBytes

    }

    private fun analyze(dir: File) {
        for (file in dir.listFiles()!!) {
            if (file.isDirectory) {
                analyze(file)
            } else if (file.name.endsWith(".html")) {
                analyzeSimple(readFile(file))
            } else if (file.name.endsWith(".js")) {
                analyzeSimple(readFile(file))
            } else if (file.name.endsWith(".jsp")) {
                analyzeSimple(readFile(file))
            } else if (file.name.endsWith(".ftl")) {
                analyzeSimple(readFile(file))
            }
        }
    }

    private fun analyzeSimple(tx: String) {
        val matcher = Pattern.compile("[\\w-]+").matcher(tx)
        while (matcher.find()) {
            for (clazz in matcher.group().split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {
                cssClasses.remove(clazz)
                cssIds.remove(clazz)
            }
        }
    }

    private fun readFile(path: String): String {
        return readFile(File(path))
    }

    private fun readFile(file: File): String {
        try {
            val scanner = Scanner(file).useDelimiter("\\A")
            return if (scanner.hasNext()) {
                scanner.next()
            } else {
                ""
            }
        } catch (e: FileNotFoundException) {
            throw RuntimeException(e)
        }

    }

}

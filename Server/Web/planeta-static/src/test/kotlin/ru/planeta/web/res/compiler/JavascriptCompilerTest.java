package ru.planeta.web.res.compiler;

import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

/**
 * @author  ameshkov
 * @since  06.06.12 13:08
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/applicationContext-*.xml"})
//@ContextConfiguration(locations = {"classpath:spring/applicationContext-*.xml", "classpath*:/spring/applicationContext-dao.xml", "classpath*:/spring/applicationContext-geo.xml"})
public class JavascriptCompilerTest {

    private static final Logger log = Logger.getLogger(JavascriptCompilerTest.class);

    @Value("${test.js.basedir}")
    private String testJSBaseDir;

    @Autowired
    private JavascriptCompiler compiler;

    @Test
    public void testCompile() throws IOException, URISyntaxException {
        compiler.setFailOnError(true);
        String compiled = compiler.compile(testJSBaseDir, "planeta", new JavaScriptCompilerParams(false, null, null), false );
        assertNotNull(compiled);
        final Pattern OPEN_COMMAS = Pattern.compile("[\\w\\s]{1,50},\\s*(\\}|\\]|\\))");
        Matcher matcher = OPEN_COMMAS.matcher(compiled);
        if (matcher.find()) {
            log.info(matcher.group());
        }
        // assertFalse(matcher.find());
        compiled = compiler.compile(testJSBaseDir, "planeta", new JavaScriptCompilerParams(true, null, null), false);
        matcher = OPEN_COMMAS.matcher(compiled);
        boolean res = matcher.find();
        if (res) {
            log.info(matcher.group());
        }
        assertFalse(res);
        assertNotNull(compiled);
    }

    @Test
    @Ignore
    public void testCompileFile() throws IOException, URISyntaxException {
        compiler.setFailOnError(true);
        JavaScriptCompilerParams params = new JavaScriptCompilerParams(true, null, null);
        String compiled = compiler.compile("g:\\git1\\planeta\\Server\\Web\\planeta-static\\src\\main\\webapp\\", "search-shares-test", new JavaScriptCompilerParams(true, null, null), false );
        System.out.println(compiled);
    }
}

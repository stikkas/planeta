<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<head>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
    <%@ include file="/WEB-INF/jsp/includes/head.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/generated/main-page-meta-header.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/metas-welcome.jsp" %>

    <p:script src="school.js"/>

    <%@ include file="/WEB-INF/jsp/includes/school-registration-js.jsp" %>

    <script type="text/javascript">
        $(document).ready(function() {
            var tagSelector = new Seminars.Views.TagSelector({
                model: new BaseModel({
                    tagId: 0
                })
            });

            var seminarsCollectionView = new Seminars.Views.SeminarList({
                el: '.seminar-list',
                collection: new BaseCollection([], {
                    url: "/school/get-seminars-calendar.json",
                    data: {
                        tagId: tagSelector.model.get('tagId')
                    },
                    offset: 0,
                    limit: 100
                })
            });
            tagSelector.render();
            tagSelector.model.bind('change', seminarsCollectionView.seminarChanged, seminarsCollectionView);
            seminarsCollectionView.collection.load().done(function() {
                seminarsCollectionView.render();
            });
        });
    </script>
</head>

<body class="school-page">
<%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>

<div id="global-container">
    <div id="main-container" class="wrap-container">
        <div id="center-container">
            <%@ include file="/WEB-INF/jsp/includes/school-header.jsp" %>

            <div class="school-calendar">

                <div class="wrap">
                    <div class="col-12">

                        <div class="school-calendar_head school-page-head">
                            Календарь мероприятий
                        </div>


                        <table class="school-calendar_table table seminar-list">
                            <colgroup>
                                <col width="100%">
                                <col width="80">
                                <col width="150">
                                <col width="250">
                                <col width="150">
                            </colgroup>
                            <thead>
                            <tr>
                                <th>Название</th>
                                <th>Когда</th>
                                <th>Город</th>
                                <th colspan="2" class="seminar-tag-selector">
                                </th>
                            </tr>
                            </thead>
                        </table>


                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<%@ include file="/WEB-INF/jsp/includes/generated/irma/footer.jsp" %>
<div id="registration-modal-form"></div>
</body>
</html>
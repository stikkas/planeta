<c:if test="${seminar.participantsLimit != null}">
    <div class="school-countdown_places">
        <div class="school-countdown_head">
            Мест:
        </div>

        <div class="school-countdown_cont">
            <div class="school-countdown_val">
                <c:if test="${seminar.participantsLimit < 10}">
                    <span class="n-school-countdown-0"></span>
                </c:if>
                <c:forEach var="number" items="${arrayParticipantsLimit}">
                    <span class="n-school-countdown-${number}"></span>
                </c:forEach>
            </div>
        </div>
    </div>
</c:if>
<div class="school-footer">

    <div class="wrap">
        <div class="col-12">


            <div class="school-footer_copy">&copy; <a href="https://planeta.ru/">PLANETA.RU</a> 2018</div>

            <div class="school-footer_info">
                <div class="school-footer_info_i">
                    <span class="school-footer_info_ico"><span class="s-school-mail"></span></span>
                    <a class="school-footer_info_val" href="mailto:school@planeta.ru">school@planeta.ru</a>
                </div>

                <div class="school-footer_info_i">
                    <span class="school-footer_info_ico"><span class="s-school-phone"></span></span>
                    <span class="school-footer_info_val">+7 (495) 181-05-05</span>
                </div>
            </div>


        </div>
    </div>

</div>
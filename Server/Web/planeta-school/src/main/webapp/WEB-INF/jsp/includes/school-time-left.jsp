<div class="school-countdown_time">
    <div class="school-countdown_head">
        До старта осталось:
    </div>

    <c:if test="${timeLeftMills > 0}">
        <div class="school-countdown_cont">
            <div class="school-countdown_days">
                <c:if test="${daysLeft < 10}">
                    <span class="n-school-countdown-0"></span>
                </c:if>
                <c:forEach var="number" items="${arrayDaysLeft}">
                    <span class="n-school-countdown-${number}"></span>
                </c:forEach>
                <div class="school-countdown_lbl">
                    Дней
                </div>
            </div>
            <div class="school-countdown_hours">
                <div class="school-countdown_val">
                    <c:if test="${hoursLeft < 10}">
                        <span class="n-school-countdown-0"></span>
                    </c:if>
                    <c:forEach var="number" items="${arrayHoursLeft}">
                        <span class="n-school-countdown-${number}"></span>
                    </c:forEach>
                </div>
                <div class="school-countdown_lbl">
                    Часов
                </div>
            </div>
            <div class="school-countdown_minutes">
                <div class="school-countdown_val">
                    <c:if test="${minutesLeft < 10}">
                        <span class="n-school-countdown-0"></span>
                    </c:if>
                    <c:forEach var="number" items="${arrayMinutesLeft}">
                        <span class="n-school-countdown-${number}"></span>
                    </c:forEach>
                </div>
                <div class="school-countdown_lbl">
                    Минут
                </div>
            </div>
        </div>
    </c:if>



    <c:if test="${timeLeftMills <= 0}">
        <div class="school-countdown_cont">
            <div class="school-countdown_days">
                <div class="school-countdown_val">
                    <span class="n-school-countdown-0"></span>
                    <span class="n-school-countdown-0"></span>
                </div>
                <div class="school-countdown_lbl">
                    Дней
                </div>
            </div>
            <div class="school-countdown_hours">
                <div class="school-countdown_val">
                    <span class="n-school-countdown-0"></span>
                    <span class="n-school-countdown-0"></span>
                </div>
                <div class="school-countdown_lbl">
                    Часов
                </div>
            </div>
            <div class="school-countdown_minutes">
                <div class="school-countdown_val">
                    <span class="n-school-countdown-0"></span>
                    <span class="n-school-countdown-0"></span>
                </div>
                <div class="school-countdown_lbl">
                    Минут
                </div>
            </div>
        </div>
    </c:if>
</div>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<head>
    <%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/generated/main-page-meta-header.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/metas-startup.jsp" %>
    <c:set var="seminar" value="${seminarOnMainPage}" />

    <p:script src="school.js"/>
    <%@ include file="/WEB-INF/jsp/includes/school-registration-js.jsp" %>

    <style type="text/css">
        .inj-project-card-graduate{
            display: none;
        }

        .step-block {
            position: relative;
            padding-top: 74px;
        }

        .school-step_head {
            position: relative;
            margin: 0 0 64px;
        }

        .white {
            color: #fff;
        }

        .blue {
            color: #00B2E3;
        }

        .school-lessons_name {
            font-size: 20px;
            overflow: hidden;
            padding: 2px 0 0;
        }


        .school-lessons_name_white {
            font-size: 20px;
            overflow: hidden;
            padding: 2px 0 0;
            color: #fff;
        }

        .school-lessons_num {
            font-size: 14px;
            float: left;
            width: 27px;
            height: 27px;
            padding: 7px 0 0;
            margin: 0 15px 0 0;
            line-height: 1.2;
            font-weight: 700;
            text-align: center;
            background: #fff;
            border-radius: 50%;
        }

        .school-lessons_num_blue {
            font-size: 14px;
            float: left;
            width: 27px;
            height: 27px;
            padding: 7px 0 0;
            margin: 0 15px 0 0;
            line-height: 1.2;
            font-weight: 700;
            text-align: center;
            border-radius: 50%;
            color: #fff;
            background: #00B2E3;
        }

        .background-blue-gradient {
            background: linear-gradient(to top, #00B2E2 0%,#3498db 58%,#296dbc 100%);
        }

        .school-lead_cont {
            position: relative;
            width: 550px;
            font: 18px/1.24 'ProximaNova', Arial, sans-serif;
            color: #fff;
        }

        .object {
            position: absolute;
            right: 0;
            transition: 0.25s;
            -webkit-filter: drop-shadow(0 30px 7px rgba(0, 0, 0, 0.15));
        }

        .object:hover {
            -webkit-filter: drop-shadow(0 20px 4px rgba(0, 0, 0, 0.3));
            cursor: pointer
        }

        .step1 {
            top: 150px;
        }

        .step1:hover {
            top: 160px;
        }

        .step2 {
            top: -30px;
        }

        .step2:hover {
            top: -20px;
        }

        .step3 {
            top: 40px;
        }

        .step3:hover {
            top: 50px;
        }

        .step4 {
            top: -30px;
        }

        .step4:hover {
            top: -20px;
        }

        .school-lead_second {
            margin: 9px 0 17px;
            font-size: 18px;
            text-transform: uppercase;
        }

        .calendar {
            position: absolute;
            background: #fff;
            z-index: 1000;
            border-radius: 10px;
            left: 0;
            right: 0;
            margin-top: 30px;
        }

        .top-contacts {
            float: left;
            margin: 27px 0 0 65px;
            font: 17px/20px 'ProximaNova', Arial, sans-serif;
        }

        .school-header_info_val {
            display: inline-block;
            vertical-align: middle;
            color: #fff;
            text-transform: uppercase;
        }

        .school-header_info_val:hover {
            text-decoration: none;
        }

        .school-book_book {
            width: 777px;
            height: 501px;
            margin: 0 0 0 -56px;
            background: url(https://s2.planeta.ru/f/382/book.jpg) no-repeat;
        }

        .pointer {
            font-size: 50px;
            left: 485px;
            top: 210px;
            position: absolute;
            -webkit-animation: blink 1s linear infinite;
            animation: blink 1s linear infinite;
        }

        @-webkit-keyframes blink {
            50% { color: rgb(34, 34, 34); }
            51% { color: rgba(34, 34, 34, 0); }
            100% { color: rgba(34, 34, 34, 0); }
        }
        @keyframes blink {
            50% { color: rgb(34, 34, 34); }
            51% { color: rgba(34, 34, 34, 0); }
            100% { color: rgba(34, 34, 34, 0); }
        }

        .project-partners-list > a {
            display: inline-block;
            margin: 0 23px;
            vertical-align: middle !important;
        }
    </style>

    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            window.localStorage.setItem('cssPath', workspace.staticNodesService.getResourceUrl("/images/project/rainbow.mp4"));
            var topCampaigns = new (BaseCollection.extend({
                url: '/api/util/search-campaign-sponsor.json',
                data: {
                    sponsorAlias: 'GRADUATE'
                },
                limit: 10
            }))();

            moduleLoader.loadModule('welcome').done(function () {
                var viewTopCampaigns = new (Welcome.Views.TopCampaigns.extend({
                    el: '.project-card-list',
                    collection: topCampaigns,
                    afterRender: function () {
                        $("img", this.$el).each(function () {
                            $(this).attr("src", $(this).attr("data-original"));
                        });


                        var itemCount = $(".project-card-item", this.$el).length;
                        var slideStep = (itemCount < 8) ? (itemCount % 4) : 4;

                        var owl = this.$el.addClass('owl-carousel').owlCarousel({
                            lazyLoad: true,
                            autoplay: false,
                            autoplayTimeout: 5000,
                            autoplayHoverPause: true,
                            loop: true,
                            items: 4,
                            slideBy: slideStep,
                            nav: true,
                            dots: false,
                            margin: 10,
                            mouseDrag: false,
                            smartSpeed: 200,
                            navText: [
                                '<span class="s-icon s-icon-arrow-left">',
                                '<span class="s-icon s-icon-arrow-right">'
                            ]
                        });
                    }
                }))();

                topCampaigns.load().done(function () {
                    viewTopCampaigns.render();
                });
            });

            ymaps.ready(function () {
                var myMap = new ymaps.Map('map', {
                    center: [55.7805,37.6320],
                    zoom: 17,
                    controls: []
                });

                myMap.behaviors.disable('scrollZoom');

                myMap.controls.add('fullscreenControl');

                var myPlacemark = new ymaps.Placemark([55.7780,37.6316], {
                    balloonContentHeader: "Planeta.ru",
                    hintContent: "Planeta.ru"
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: 'https://s2.planeta.ru/f/383/marker.png',
                    iconImageSize: [70, 70],
                    iconImageOffset: [-15, -80]
                });
                myMap.geoObjects.add(myPlacemark);
            });
        });
    </script>
</head>

<body class="school-page school-page-main">

<div id="global-container">
<div id="main-container" class="wrap-container">
<div id="center-container">

    <div class="school-header">
        <div class="wrap">
            <div class="col-12">
                <a href="//${properties['school.application.host']}" class="school-logo"></a>

                <div class="top-contacts">
                    <div class="school-footer_info_i">
                        <span class="school-footer_info_ico"><span class="s-school-mail"></span></span>
                        <a class="school-header_info_val" href="mailto:school@planeta.ru">school@planeta.ru</a>
                    </div>

                    <div class="school-footer_info_i">
                        <span class="school-footer_info_ico"><span class="s-school-phone"></span></span>
                        <span class="school-header_info_val">+7 (495) 181-05-05</span>
                    </div>
                </div>

                <div class="school-header-social">
                    <a href="http://vk.com/planetaru" class="school-header-social_link" target="_blank" rel="nofollow noopener">
                        <span class="s-school-header-social-vk"></span>
                    </a>
                    <a href="https://www.facebook.com/planetaru" class="school-header-social_link" target="_blank" rel="nofollow noopener">
                        <span class="s-school-header-social-fb"></span>
                    </a>
                    <a href="http://instagram.com/planeta_ru" class="school-header-social_link" target="_blank" rel="nofollow noopener">
                        <span class="s-school-header-social-ig"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>

<div class="school-lead">
    <div class="wrap">
        <div class="col-12">

            <div class="school-lead_block">

                <div class="school-lead_img" style="left: 0;"></div>


                <div class="school-lead_cont">
                    <div class="school-lead_head">
                        ОБРАЗОВАТЕЛЬНЫЙ КВЕСТ PLANETA.RU
                    </div>

                    <div class="school-lead_second">
                        ДЛЯ СТАРТАПОВ И СОЦИАЛЬНЫХ ПРЕДПРИНИМАТЕЛЕЙ
                    </div>

                    <div class="school-lead_text">
                        Мы представляем абсолютно новый формат образовательного направления краудфандинговой платформы Planeta.ru! Вас ждут еженедельные брейнштормы, специальные семинары, регулярные мастер-классы, видеоуроки и практические задания в уникальной форме квестинаров от гуру краудфандинга - экспертов платформы Planeta.ru!
                        <br>
                        <br>
                        В режиме интенсивных игровых хакатонов и ежедневных онлайн-заданий вы узнаете, из чего состоит и как работает краудфандинг в России - не только с наших слов, но и на собственном опыте!
                    </div>
                </div>


                <div class="school-lead_next"></div>
                <script>
                    $(function () {
                        $('.school-lead_next').on('click', function () {
                            var top = $('.school-info').offset().top;
                            $('html, body').animate({
                                scrollTop: top
                            }, 300);
                        });
                    });
                </script>


                <div class="school-presentation">
                    <div class="school-presentation_ico"></div>
                    <div class="school-presentation_cont">
                        <div class="school-presentation_text">
                            Хотите попасть на первый хакатон для стартапов и социальных предпринимателей Planeta.ru?
                            <br>
                            Количество мест ограничено!
                            <br>
                            Регистрируйтесь здесь
                        </div>

                        <script type="text/javascript">
                            var handleTWFpostRepaint = function postRepaint(params) {
                                var self = this;
                                var $tpw = this.$$('#tpw_cont button');
                                this.$$('#tpw_cont').addClass('school-presentation_btn');
                                this.$$('#tpw_cont').css('text-align', 'center');
                                $tpw.removeClass('btn btn-large btn-info b-pro-widget__open-button');
                                $tpw.addClass('school-btn school-btn-primary school-btn-lg');
                                $('iframe').css('height', '86px');
                                this.$$('.open_widget').text('Хочу участвовать!');

                                if(!postRepaint.clickObject) {
                                    postRepaint.clickObject = $('.object').on('click', function() {
                                        self.$$('.open_widget').click();
                                    });
                                }

                                if(!postRepaint.clickLink) {
                                    postRepaint.clickLink = $('.js-register-button-time-pad').on('click', function () {
                                        self.$$('.open_widget').click();
                                    });
                                }
                            }
                        </script>

                        <script type="text/javascript"
                                defer="defer"
                                charset="UTF-8"
                                data-timepad-widget-v2="event_register"
                                src="https://timepad.ru/js/tpwf/loader/min/loader.js">
                            (function() {
                                return {
                                    loadCSS: [
                                        '//${hf:getStaticBaseUrl("")}/css-generated/school.css'
                                    ],
                                    event: {id: "350615"},
                                    bindEvents: {
                                        preRoute :"TWFpreRouteHandler",
                                        postRepaint: "handleTWFpostRepaint"
                                    },
                                    isInEventPage: true,
                                    hidePreloading: true,
                                    initialRoute: "button"
                                };
                            })();
                        </script>
                    </div>
                </div>

            </div>


        </div>
    </div>
</div>

<div class="school-book">
    <div class="school-book_wrap">
        <div class="wrap">
            <div class="school-book_block col-12" style="text-align: center;">
                <div class="school-book_img">
                    <div class="school-book_book"></div>
                </div>

                <div class="school-book_text">
                    <div class="school-book_head" style="margin: -80px 0 36px; font-size: 30px">
                        ДОСТУП В КВЕСТ <br>+<br> ПРАКТИЧЕСКОЕ ПОСОБИЕ ПО КРАУДФАНДИНГУ <br>+<br> ПЕРСОНАЛЬНЫЙ ПЛАН ДЕЙСТВИЙ
                    </div>

                    <div class="school-book_download">
                        <div class="header-no-user header-info-content">
                            Чтобы получить их <a href="javascript:void(0)" class="js-register-button-time-pad">зарегистрируйтесь</a> на хакатон
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="map" style="width:100%;height:900px">
    <div class="wrap calendar">
        <div class="col-12" style="text-align: center">
            <br><br>
            <div class="school-calendar_head school-page-head" style="line-height: 1.2;">
                НАЧАЛО КВЕСТА ДЛЯ СТАРТАПОВ И СОЦИАЛЬНЫХ ПРЕДПРИНИМАТЕЛЕЙ
            </div>
            <div class="school-countdown_time" style="margin-left: 0px; float: none">
                <div class="school-countdown_cont">
                    <div class="school-countdown_days">
                        <div class="school-countdown_val">
                            <span class="n-school-countdown-2"></span>
                            <span class="n-school-countdown-0"></span>
                        </div>
                    </div>
                    <div class="school-countdown_hours">
                        <div class="school-countdown_val">
                            <span class="n-school-countdown-0"></span>
                            <span class="n-school-countdown-7"></span>
                        </div>
                    </div>
                    <div class="school-countdown_minutes">
                        <div class="school-countdown_val">
                            <span class="n-school-countdown-2"></span>
                            <span class="n-school-countdown-0"></span>
                            <span class="n-school-countdown-1"></span>
                            <span class="n-school-countdown-6"></span>
                        </div>
                    </div>
                </div>
                <div class="school-countdown_cont" style="margin-top: 25px;">
                    <div class="school-countdown_days">
                        <div class="school-countdown_val">
                            <span class="n-school-countdown-1"></span>
                            <span class="n-school-countdown-9"></span>
                        </div>
                    </div>
                    <span class="pointer">:</span>
                    <div class="school-countdown_hours">
                        <div class="school-countdown_val">
                            <span class="n-school-countdown-0"></span>
                            <span class="n-school-countdown-0"></span>
                        </div>
                    </div>
                </div>
                <div class="school-countdown_head" style="margin-top: 16px; margin-bottom: 0; line-height: 1.5">
                    Вход только для участников<br />
                    <div class="school-info_action" >
                        <script type="text/javascript"
                                defer="defer"
                                charset="UTF-8"
                                data-timepad-widget-v2="event_register"
                                src="https://timepad.ru/js/tpwf/loader/min/loader.js">
                            (function() {
                                return {
                                    "loadCSS": [
                                        '//${hf:getStaticBaseUrl("")}/css-generated/school.css'
                                    ],
                                    "event": {"id":"350615"},
                                    "bindEvents": {
                                        "preRoute" :"TWFpreRouteHandler",
                                        "postRepaint": "handleTWFpostRepaint"
                                    },
                                    "isInEventPage": true,
                                    "hidePreloading": true,
                                    "initialRoute": "button"
                                };
                            })();
                        </script>
                    </div>
                    <br />
                    Москва, м. Проспект Мира<br />
                    ул. Проспект Мира, д. 19, стр. 3, <br/><br />
                </div>
            </div>
        </div>
    </div>
</div>

<div class="school-lessons background-blue-gradient">
    <div class="wrap">
        <div class="col-12 school-lessons_cont">
            <div class="school-step_head school-page-head white">
                Разработаем профи-краудфандинговый проект
            </div>
            <div class="wrap-row">
                <div class="col-6">
                    <div class="school-lessons_list">
                        <div class="school-lessons_i">
                            <div class="school-lessons_num">1</div>
                            <div class="school-lessons_name_white">Станем участников закрытой игры по краудфандингу для предпринимателей</div>
                        </div>
                        <div class="school-lessons_i">
                            <div class="school-lessons_num">2</div>
                            <div class="school-lessons_name_white">Сделаем из себя профессионального стартапера и краудфандера</div>
                        </div>
                        <div class="school-lessons_i">
                            <div class="school-lessons_num">3</div>
                            <div class="school-lessons_name_white">Доточим до идеала свою идею для проекта</div>
                        </div>
                        <div class="school-lessons_i">
                            <div class="school-lessons_num">4</div>
                            <div class="school-lessons_name_white">Выберем концепцию проекта из нескольких шаблонов</div>
                        </div>
                        <div class="school-lessons_i">
                            <div class="school-lessons_num">5</div>
                            <div class="school-lessons_name_white">Зафиксируем с нашей помощью план действий и бюджет</div>
                        </div>
                        <div class="school-lessons_i">
                            <div class="school-lessons_num">6</div>
                            <div class="school-lessons_name_white">Соберем из краудфандинг сообщества команду</div>
                        </div>
                        <div class="school-lessons_i">
                            <div class="school-lessons_num">7</div>
                            <div class="school-lessons_name_white">Оформим проект с помощью целого авторского отдела</div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <img class="object step1" src="https://s1.planeta.ru/f/37f/step1.png">
                </div>
            </div>
        </div>
    </div>
</div>


<div class="step-block" style="background: #fff">
    <div class="wrap">
        <div class="col-12 school-lessons_cont">
            <div class="school-step_head school-page-head blue">
                ОСВОИМ БАЗОВЫЙ УРОВЕНЬ
            </div>
            <div class="wrap-row" style="height: 300px;">
                <div class="col-6">
                    <div class="school-lessons_list">
                        <div class="school-lessons_i">
                            <div class="school-lessons_num_blue">8</div>
                            <div class="school-lessons_name">Начнем продвижения среди друзей</div>
                        </div>
                        <div class="school-lessons_i">
                            <div class="school-lessons_num_blue">9</div>
                            <div class="school-lessons_name">Найдём целевую аудиторию проекта</div>
                        </div>
                        <div class="school-lessons_i">
                            <div class="school-lessons_num_blue">10</div>
                            <div class="school-lessons_name">Научимся работать с контент-маркетингом</div>
                        </div>
                        <div class="school-lessons_i">
                            <div class="school-lessons_num_blue">11</div>
                            <div class="school-lessons_name">Используем видеоблог</div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <img class="object step2" src="https://s1.planeta.ru/f/37d/step2.png">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="school-lessons background-blue-gradient">
    <div class="wrap">
        <div class="col-12 school-lessons_cont">
            <div class="school-lessons_head school-page-head white">
                ПЕРЕЙДЁМ НА УЛУЧШЕННЫЙ<br> УРОВЕНЬ
            </div>
            <div class="wrap-row">
                <div class="col-6">
                    <div class="school-lessons_list">
                        <div class="school-lessons_i">
                            <div class="school-lessons_num">12</div>
                            <div class="school-lessons_name_white">Откроем секреты SMM для краудфандинга</div>
                        </div>
                        <div class="school-lessons_i">
                            <div class="school-lessons_num">13</div>
                            <div class="school-lessons_name_white">Используем профессиональный event-маркетинг</div>
                        </div>
                        <div class="school-lessons_i">
                            <div class="school-lessons_num">14</div>
                            <div class="school-lessons_name_white">Используем лайфхаки образовательного маркетинга</div>
                        </div>
                        <div class="school-lessons_i">
                            <div class="school-lessons_num">15</div>
                            <div class="school-lessons_name_white">Применим секреты PR для лояльности аудитории</div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <img class="object step3" src="https://s1.planeta.ru/f/380/step3.png">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="step-block" style="background: #fff">
    <div class="wrap">
        <div class="col-12 school-lessons_cont">
            <div class="school-step_head school-page-head blue">
                ПРОКАЧАЕМСЯ ДО <br>ПРОФЕССИОНАЛЬНОГО УРОВНЯ
            </div>
            <div class="wrap-row" style="height: 260px">
                <div class="col-6" >
                    <div class="school-lessons_list">
                        <div class="school-lessons_i">
                            <div class="school-lessons_num_blue">16</div>
                            <div class="school-lessons_name">Применим секреты PR для лояльности аудитории</div>
                        </div>
                        <div class="school-lessons_i">
                            <div class="school-lessons_num_blue">17</div>
                            <div class="school-lessons_name">Научимся работать с информационными партнерами</div>
                        </div>
                        <div class="school-lessons_i">
                            <div class="school-lessons_num_blue">18</div>
                            <div class="school-lessons_name">Попробуем спонсорский маркетинг с корпорациями</div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <img class="object step4" src="https://s1.planeta.ru/f/381/step4.png">
                </div>
            </div>
        </div>
    </div>
</div>


<div class="school-graduates">
    <div class="wrap">
        <div class="col-12">

            <div class="school-graduates_head school-page-head">
                Наши выпускники
            </div>

            <div class="school-graduates_list">
                <div class="project-card-list"></div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>

<div class="project-partners" data-backbone-class-name="view Welcome.Views.Partners view53 : model Welcome.Models.Partners c52">
    <div class="wrap">
        <div class="project-partners-list">
            <a href="https://te-st.ru/" target="_blank" rel="nofollow noopener" title="Теплица социальных технологий" style="margin-left: 0;"><img src="https://s1.planeta.ru/i/130ccd/1468595134624_renamed.jpg" alt="Теплица социальных технологий" width="127" height="90"></a>
            <a href="https://www.hse.ru/" target="_blank" rel="nofollow noopener" title="HSE"><img src="https://s1.planeta.ru/i/130ca1/1468593535310_renamed.gif" alt="HSE" width="170" height="40"></a>
            <a href="https://welcome.timepad.ru/" target="_blank" rel="nofollow noopener" title="TimePad"><img src="https://s1.planeta.ru/i/130cba/1468594721451_renamed.jpg" alt="TimePad" width="155" height="40"></a>
            <a href="http://tceh.com/" title="#tceh" target="_blank" rel="nofollow noopener"><img src="https://s1.planeta.ru/i/130cca/1468595048599_renamed.jpg" alt="#tceh" width="127" height="40"></a>
            <a href="http://gvalaunch.guru/" target="_blank" title="GVA" style="margin-right: 0;" rel="nofollow noopener"><img src="https://s1.planeta.ru/i/130cd8/1468595367487_renamed.jpg" alt="GVA" width="147" height="90"></a>
        </div>
    </div>
</div>

<%@ include file="/WEB-INF/jsp/includes/generated/irma/footer.jsp" %>
<div id="registration-modal-form"></div>
</body>
</html>
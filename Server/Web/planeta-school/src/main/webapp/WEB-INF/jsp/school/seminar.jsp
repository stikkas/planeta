<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<head>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
    <%@ include file="/WEB-INF/jsp/includes/head.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/generated/main-page-meta-header.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/metas-seminar.jsp" %>

    <p:script src="school.js"/>
    <%@ include file="/WEB-INF/jsp/includes/school-registration-js.jsp" %>
    <script type="text/javascript" src="https://${hf:getStaticBaseUrl("")}/js/utils/plugins/share-widget.js?compress=${properties['static.compress']}&flushCache=${properties['static.flushCache']}"></script>
</head>

<body class="school-page school-page-project">
<%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>


<script type="text/javascript">
    $(function() {
        var getDataForShare = {
            url : "${schoolHostUrl}/school/${seminar.seminarId}",
            title: '${hf:escapeEcmaScript(seminar.name)}',
            className: 'horizontal donate-sharing sharing-mini', counterEnabled: false, hidden: false,
            description: '${hf:escapeEcmaScript(seminar.shortDescription)}',
            imageUrl: 'https://${hf:getStaticBaseUrl("")}/images/school/school-sharing.jpg'
        };
        $('.school-info_share_val').share(getDataForShare);
    });
</script>

<div id="global-container">
    <div id="main-container" class="wrap-container">
        <div id="center-container">
            <%@ include file="/WEB-INF/jsp/includes/school-header.jsp" %>

            <div class="school-info" style="background-image: url(${seminar.backgroundImage})">
                <div class="school-info_wrap">
                    <div class="wrap">
                        <div class="school-info_block col-12">

                            <div class="school-info_head school-page-head">
                                ${seminar.name}
                            </div>

                            <div class="school-info_post-head">
                                <div class="school-info_post-head_ttl">&laquo;${seminar.tagName}&raquo;</div>
                            </div>

                            <div class="school-info_share">
                                <div class="school-info_share_cont">
                                    <div class="school-info_share_lbl">
                                        Поделись в соцсетях:
                                    </div>
                                    <div class="school-info_share_val"></div>
                                </div>
                            </div>

                            <div class="school-info_descr">
                                ${seminar.shortDescription}
                            </div>

                            <div class="school-sign-box">
                                <div class="school-sign-box_btn">
                                    <c:set var="now" value="<%=new java.util.Date()%>" />
                                    <span class="school-btn school-btn-primary school-btn-lg js-register-button <c:if test="${!(now.time < seminar.timeRegistrationEnd.time)}">disabled</c:if>">
                                        Записаться <span class="s-school-rocket"></span>
                                    </span>
                                </div>
                                <div class="school-sign-box_info">
                                    <div class="school-sign-box_date">Заявки принимаются до
                                        <fmt:formatDate pattern="dd / MM / yyyy" value="${seminar.timeRegistrationEnd}" />
                                    </div>
                                </div>
                            </div>


                            <div class="school-countdown">
                                <%@ include file="/WEB-INF/jsp/includes/school-participants-limit.jsp" %>

                                <%@ include file="/WEB-INF/jsp/includes/school-time-left.jsp" %>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="school-project-about">
                <div class="wrap">
                    <div class="col-8">

                        <div class="school-format_head school-page-head">
                            Что мы будем делать
                        </div>


                        <div class="school-project-about_text">
                            ${seminar.description}
                        </div>


                    </div>



                    <div class="col-4">

                        <div class="school-project-about_add">
                            <div class="school-project-about_add_text">
                                &laquo;Школа краудфандинга&raquo;&nbsp;&mdash; это образовательное направление Planeta.ru, краудфандинговой платформы #1 в&nbsp;России. Мы&nbsp;помогаем понять, из&nbsp;чего состоит и&nbsp;как работает краудфандинг в&nbsp;теории и&nbsp;на&nbsp;практике. Лекции, семинары, <nobr>мастер-классы</nobr>, вебинары и&nbsp;практические интенсивы. Все это &laquo;Школа краудфандинга&raquo; Planeta.ru.
                            </div>
                        </div>


                    </div>
                </div>
            </div>

            <div class="school-project-about school-project-about__yellow">
                <div class="school-project-about_hr"></div>
                <div class="wrap">
                    <div class="col-8">
                        <div class="school-project-about_head school-page-head">
                            Что я&nbsp;получу в&nbsp;итоге
                        </div>

                        <div class="school-project-about_text">
                            ${seminar.results}
                        </div>

                        <div class="school-sign-box school-sign-box__invert">
                            <span class="school-btn school-btn-default school-btn-lg js-register-button <c:if test="${!(now.time < seminar.timeRegistrationEnd.time)}">disabled</c:if>">
                                Записаться <span class="s-school-rocket"></span>
                            </span>
                            <div class="school-sign-box_info">
                                <div class="school-sign-box_date">Заявки принимаются до
                                    <fmt:formatDate pattern="dd / MM / yyyy" value="${seminar.timeRegistrationEnd}" />
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="col-4">
                        <div class="school-project-about_add">
                            <div class="school-project-about_add_head">
                                Формат и условия обучения
                            </div>
                            <div class="school-project-about_add_text">
                                ${seminar.conditions}
                            </div>
                        </div>
                    </div>



                </div>
            </div>




            




            <div class="school-speakers">
                <div class="wrap">
                    <div class="col-12">

                        <div class="school-speakers_head school-page-head">
                            Спикеры и эксперты
                        </div>


                        <div class="school-speakers_list">
                        <c:forEach var="speaker" items="${speakers}">
                            <div class="school-speakers_i">
                                <div class="school-speakers_ava">
                                    <img src="${hf:getThumbnailUrl(speaker.photoUrl, 'MEDIUM', 'PHOTO')}" width="140" height="140" alt="${speaker.name}">
                                </div>
                                <div class="school-speakers_cont">
                                    <div class="school-speakers_name">
                                        ${speaker.name}
                                    </div>
                                    <div class="school-speakers_descr">
                                        ${speaker.description}
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                        </div>

                    </div>
                </div>
            </div>




            <div class="school-partners">
                <div class="wrap">
                    <div class="col-12">

                        <div class="school-partners_head school-page-head">
                            Наши партнеры
                        </div>


                        <div class="school-partners_list">
                            <c:forEach var="partner" items="${partners}">
                            <a href="${partner.siteUrl}" class="school-partners_i" target="_blank" rel="nofollow noopener">
                                <img src="${hf:getThumbnailUrl(partner.logoUrl, 'MEDIUM', 'PHOTO')}">
                            </a>
                            </c:forEach>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%@ include file="/WEB-INF/jsp/includes/footer.jsp" %>
<div id="registration-modal-form"></div>
</body>
</html>
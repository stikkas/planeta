<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<head>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
    <%@ include file="/WEB-INF/jsp/includes/head.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/generated/main-page-meta-header.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/metas-welcome.jsp" %>
    <c:set var="seminar" value="${seminarOnMainPage}" />

    <p:script src="school.js"/>
    <%@ include file="/WEB-INF/jsp/includes/school-registration-js.jsp" %>


    <style>
        .inj-project-card-graduate{
            display: none;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            var tagSelector = new Seminars.Views.TagSelector({
                model: new BaseModel({
                    tagId: 0
                })
            });

            var seminarsCollectionView = new Seminars.Views.SeminarList({
                el: '.seminar-list',
                collection: new BaseCollection([], {
                    url: "/school/get-seminars-calendar.json",
                    data: {
                        tagId: tagSelector.model.get('tagId')
                    },
                    offset: 0,
                    limit: 10
                })
            });
            tagSelector.render();
            tagSelector.model.bind('change', seminarsCollectionView.seminarChanged, seminarsCollectionView);
            seminarsCollectionView.collection.load().done(function () {
                seminarsCollectionView.render();
            });

            var topCampaigns = new (BaseCollection.extend({
                url: '/api/util/search-campaign-sponsor.json',
                data: {
                    sponsorAlias: 'GRADUATE'
                },
                limit: 100
            }))();


            moduleLoader.loadModule('welcome').done(function () {


                var viewTopCampaigns = new (Welcome.Views.TopCampaigns.extend({
                    el: '.project-card-list',
                    collection: topCampaigns,
                    afterRender: function () {
                        $("img", this.$el).each(function () {
                            $(this).attr("src", $(this).attr("data-original"));
                        });


                        var itemCount = $(".project-card-item", this.$el).length;
                        var slideStep = (itemCount < 8) ? (itemCount % 4) : 4;

                        var owl = this.$el.addClass('owl-carousel').owlCarousel({
                            lazyLoad: true,
                            autoplay: false,
                            autoplayTimeout: 5000,
                            autoplayHoverPause: true,
                            loop: true,
                            items: 4,
                            slideBy: slideStep,
                            nav: true,
                            dots: false,
                            margin: 10,
                            mouseDrag: false,
                            smartSpeed: 200,
                            navText: [
                                '<span class="s-icon s-icon-arrow-left">',
                                '<span class="s-icon s-icon-arrow-right">'
                            ]
                        });
                    }
                }))();


                topCampaigns.load().done(function () {
                    viewTopCampaigns.render();
                });
            });
            $('.js-gtm-school-pdf_1, .js-gtm-school-pdf_2').click(function (e) {
                e.preventDefault();
                $.get("/api/profile/track-manual-pdf.json", {link: $(e.currentTarget).hasClass('js-gtm-school-pdf_1') ? 1 : 2});
                window.open("https://s2.planeta.ru/f/390/prakticheskoe_posobie_po_crowdfundingu_planeta.ru.pdf");
            });

        });
    </script>
</head>

<body class="school-page school-page-main">
    <%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>

    <div id="global-container">
        <div id="main-container" class="wrap-container">
            <div id="center-container">

                <div class="school-interactive">
                    <div class="school-interactive_head">
                        <div class="wrap">
                            <div class="col-12">
                                <div class="school-interactive_top-link">
                                    <a href="https://planeta.ru/faq/article/30!paragraph205">Подробнее о курсе</a>
                                </div>
                                <span class="school-logo"></span>
                                <svg width="222" height="51" viewBox="0 0 632 145"><path d="M132.3 32.3c3.4-5.6 2.7-12.9-2.1-17.7-4.8-4.8-12.1-5.5-17.7-2.1C84.4-6.4 46-3.4 21.2 21.4-7 49.6-7 95.4 21.2 123.6c28.2 28.2 74 28.2 102.2 0 24.8-24.8 27.7-63.2 8.9-91.3m-15.8 84.5c-18 18-44.1 22.7-66.4 14.3-5-2.1-20.2-10.8.9-31.8l-2.8-2.8-2.8-2.8c-20.3 20.3-29.1 6.8-31.6 1.4C5.2 72.7 9.9 46.3 28 28.2c21.3-21.3 54.1-24 78.4-8.3-1.8 5.1-.7 11 3.4 15.1s10 5.2 15.1 3.4c15.7 24.3 12.9 57.1-8.4 78.4M70.7 49.3c-4.8 3.1-8.3 8.1-11.5 13-7.8.5-15.6 3.9-20.2 10.3-.6.9-1.5 2.1-2.1 4.2 1.3.2 3.1.5 4.2.7 2.8.5 6.1.8 9.2 2.7-.6 2.1-1.2 5-1.5 6.9v.5c1.6.8 3.6 1.7 5.2 3.3 1.6 1.6 2.5 3.7 3.3 5.2h.5c1.9-.3 4.8-1 6.9-1.5 1.9 3.1 2.2 6.4 2.7 9.2.2 1.1.5 2.9.7 4.2 2.1-.6 3.3-1.5 4.2-2.1 6.4-4.6 9.8-12.4 10.3-20.2 4.9-3.2 9.8-6.7 13-11.5 5.9-9.1 6.4-20 6.4-31.2-11.4-.1-22.2.3-31.3 6.3m11.8 20.6c-4.2 0-7.5-3.4-7.5-7.5s3.4-7.5 7.5-7.5 7.5 3.4 7.5 7.5-3.4 7.5-7.5 7.5" fill="#1A8CFF"></path><path d="M175 23.5h8v29.8h8V23.5h8v29.8h8V23.5h8v37.2h-40V31m77.3 29.8l-14-18.6 14-18.6H243l-12 16v-16h-8v37.2h8V44.9l12 15.9h9.3zm34.8-18.6c0-13.6-7.2-19.9-17-19.9s-17 6.3-17 19.9c0 13.6 7.2 19.9 17 19.9s17-6.3 17-19.9m-26 0c0-9.5 3.7-12.4 9-12.4 5.4 0 9 3 9 12.4 0 9.5-3.7 12.4-9 12.4-5.4 0-9-3-9-12.4m60.4 18.6V23.5h-22.9L295.9 49c-.4 3.8-1 5-2.8 5-.5 0-2.1-.1-3.3-.3l-1.1 6.9c2.2.6 4.7.9 6.1.9 5.3 0 8.1-4 9-12.4l1.9-18.1h7.8v29.8h8zm40.9 0l-15.8-37.2h-5.3l-15.8 37.2h8.5l2.7-6.4h14.6l2.7 6.4h8.4zM348.2 47h-8.4l2-4.8 2.2-7.9 2.2 7.9 2 4.8zm-143.8 69l-14-18.6 14-18.6h-9.3l-12 16v-16h-8V116h8v-16l12 15.9h9.3zm12.9 0v-12.8h7.2c6.8 0 12.2-3.7 12.2-12.2s-5.6-12.3-12.2-12.3h-15.2V116h8zm0-20.3v-9.6h7.2c2.3 0 4.3.6 4.3 4.8s-2.1 4.7-4.3 4.7h-7.2zm57.5 20.3L259 78.7h-5.3L237.9 116h8.5l2.7-6.4h14.6l2.7 6.4h8.4zm-14.2-13.9h-8.4l2-4.8 2.2-7.9 2.2 7.9 2 4.8zm29.1 2.9c-1.4 3.2-2 4.3-4.5 4.3-1.3 0-3.8-.3-5.7-.9l-1.1 6.9c2.6 1 4.5 1.4 6.9 1.4 7.6 0 9.7-4.4 13.6-13.5l10.7-24.5H301l-7.6 17.7-8.9-17.7h-8.9l14.1 26.3zm54.3 18.9v-15.4h-4V78.7h-24l-2.4 23.2c-.5 3.8-1.5 6.6-4 6.6h-1.2v15.4h6.9v-8h21.8v8h6.9zm-12-37.7v22.3h-12.6c1.1-2 1.9-4.8 2.3-8.5l1.4-13.8h8.9zm37.5 29.8v-6.4h1.9c7.2 0 13.3-4.1 13.3-13.8 0-9.6-6.2-13.9-13.3-13.9h-1.9v-3.2h-8v3.2h-1.9c-7.1 0-13.3 4.3-13.3 13.9 0 9.6 5.9 13.8 13.3 13.8h1.9v6.4h8zm-8-13.9h-1.9c-2.7 0-5.3-.7-5.3-6.3s2.5-6.4 5.3-6.4h1.9v12.7zm8 0V89.4h1.9c2.8 0 5.3.8 5.3 6.4 0 5.6-2.6 6.3-5.3 6.3h-1.9zm52 13.9l-15.8-37.2h-5.3L384.6 116h8.5l2.7-6.4h14.6l2.7 6.4h8.4zm-14.2-13.9h-8.4l2-4.8 2.2-7.9 2.2 7.9 2 4.8zm47.5 13.9V78.7h-8v14.9h-13.4V78.7h-8V116h8v-14.9h13.4V116h8zm39.9 7.9v-15.4h-4V78.7h-24l-2.4 23.2c-.5 3.8-1.5 6.6-4 6.6h-1.2v15.4h6.9v-8h21.8v8h6.9zm-12-37.7v22.3h-12.6c1.1-2 1.9-4.8 2.3-8.5l1.4-13.8h8.9zm45.5 29.8V78.7h-7.4l-.5 3.6-13.4 17.7V78.7h-8V116h7.4l.5-3.5 13.4-17.8V116h8zm37.3 0V78.7h-8v14.9h-13.4V78.7h-8V116h8v-14.9h13.4V116h8zm16 0V86.2h14.6v-7.4h-22.6V116h8zm50.3 0L616 78.7h-5.3L594.9 116h8.5l2.7-6.4h14.6l2.7 6.4h8.4zm-14.3-13.9h-8.4l2-4.8 2.2-7.9 2.2 7.9 2 4.8z" fill="#FFFFFF"></path></svg>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="school-interactive_cont">
                        <div class="wrap">
                            <div class="col-12">

                                <div class="school-interactive_cont-in">
                                    <div class="school-interactive_title">
                                        <div class="school-interactive_title-text">
                                            Онлайн-курс<br><span class="laquo">«</span>Краудфандинговый<br>проект за 60 минут»                                    </div>
                                    </div>
                                    <div class="school-interactive_post-title">
                                        на <span class="school-interactive_pln-logo" alt="Planeta.ru"></span> с Егором Ельчиным
                                    </div>

                                    <div class="school-interactive_text">
                                        Некогда читать книги и&nbsp;смотреть вебинары о&nbsp;краудфандинге? Пора&nbsp;ускоряться! Вас ждут 12 интерактивных уроков «2 в&nbsp;1»&nbsp;— Егор&nbsp;Ельчин не&nbsp;только рассказывает о&nbsp;народном финансировании, но&nbsp;и&nbsp;одновременно помогает вам создавать проект на&nbsp;Planeta.ru.
                                        <br>
                                        <br>
                                        Всё это абсолютно бесплатно, экстремально быстро и&nbsp;практично: начинайте учиться прямо сейчас, и&nbsp;через час у&nbsp;вас будет краудфандинговая кампания, готовая к&nbsp;запуску!
                                    </div>

                                    <div class="school-interactive_next">
                                        <div class="school-interactive_next-btn">
                                            <a href="${mainAppUrl}/interactive" class="btn school-interactive-btn">Пройти курс</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <script>
                        $('.school-interactive-btn').on('click', function () {
                            var form = $(".school-interactive-request_form").serializeObject();
                            var errEl = $(".school-interactive-request_error");
                            errEl.html("");
                            $.post('/school/online-course-application-request.json', form).done(function (response) {
                                if (response) {
                                    if (response.success) {
                                        $('.school-interactive-request').addClass('success');
                                        setTimeout(function () {
                                            $('.school-interactive-request').removeClass('success');
                                        }, 4000);
                                    } else {
                                        var errors = "";
                                        if (response.fieldErrors) {
                                            _.each(response.fieldErrors, function (errorValue, errorKey) {
                                                if (errorValue === "Обязательное поле") {
                                                    errors = "Пожалуйста, заполните поля";
                                                    return;
                                                }
                                                if (errors.length !== 0) {
                                                    errors += ", ";
                                                }
                                                errors += errorValue;
                                            });
                                        } else if (response.errorMessage) {
                                            errors = response.errorMessage;
                                        }
                                        errEl.html(errors);
                                    }
                                }
                            });
                        });
                    </script>
                    <div class="school-interactive-sharing">
                        <div class="wrap">
                            <div class="col-12">
                                <div class="school-interactive-sharing_cont">
                                    <div class="school-interactive-sharing_text">
                                        Расскажите про онлайн-курс Школы краудфандинга в соцсетях:
                                    </div>
                                    <div class="school-interactive-sharing_body">

                                        <!--  sharing template  -->
                                        <div class="sharing-popup-social sharing-mini">
                                            <div class="sps-button">
                                                <div class="spsb-item" title="В контакте"><a href="http://vk.com/planetaru"><i class="bs-icons-vk"></i></a></div>
                                                <div class="spsb-item" title="Facebook"><a href="https://www.facebook.com/planetaru"><i class="bs-icons-fb"></i></a></div>
                                                <div class="spsb-item" title="Twitter"><a href="http://twitter.com/PlanetaPortal"><i class="bs-icons-tw"></i></a></div>
                                                <div class="spsb-item" title="SurfingBird"><a href="javascript:void(0)"><i class="bs-icons-sb"></i></a></div>
                                                <div class="spsb-item" title="Мой Мир"><a href="javascript:void(0)"><i class="bs-icons-mr"></i></a></div>
                                                <div class="spsb-item" title="Одноклассники"><a href="javascript:void(0)"><i class="bs-icons-ok"></i></a></div>
                                                <div class="spsb-item" title="Google+"><a href="https://plus.google.com/+PlanetaRuportal/posts"><i class="bs-icons-gp"></i></a></div>
                                                <div class="spsb-item" title="Telegram"><a href="javascript:void(0)"><i class="bs-icons-tg"></i></a></div>
                                            </div>
                                        </div>
                                        <!--  // sharing template  -->

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="school-book">
                    <div class="school-book_wrap">
                        <div class="wrap">
                            <div class="school-book_block col-12">

                                <div class="school-book_img">
                                    <div class="school-book_book"></div>
                                </div>

                                <div class="school-book_text" id="manual-pdf">
                                    <div class="school-book_head">
                                        <c:if test="${isAuthorized}"><a class="js-gtm-school-pdf_1" href="javascript:void(0)"></c:if>Скачайте<c:if test="${isAuthorized}"></a></c:if> наше практическое пособие
                                        </div>

                                        <div class="school-book_download">
                                        <c:choose>
                                            <c:when test="${!isAuthorized}">
                                                <div class="header-no-user header-info-content">
                                                    Для того чтобы скачать 
                                                    <a href="javascript:void(0)" class="js-registration-link registration-link h-no-user-link">зарегистрируйтесь</a> или 
                                                    <a href="javascript:void(0)" class="js-signup-link signup-link h-no-user-link">авторизуйтесь</a>
                                                </div>
                                            </c:when>
                                            <c:otherwise>
                                                <a class="school-book_download-link js-gtm-school-pdf_2" href="javascript:void(0)">
                                                    <span class="school-book_download-link-text">Скачать</span>
                                                    <br>
                                                    pdf, 53,1 мб
                                                </a>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <c:if test="${seminarOnMainPage != null}">
                    <div class="school-info" style="background-image: url(${seminarOnMainPage.backgroundImage})">
                        <div class="school-info_wrap">
                            <div class="wrap">
                                <div class="school-info_block col-12">

                                    <div class="school-info_head school-page-head">
                                        ${seminarOnMainPage.name}
                                    </div>

                                    <div class="school-info_post-head">
                                        Категория &laquo;${seminarOnMainPage.tagName}&raquo;
                                    </div>

                                    <div class="school-info_descr">
                                        ${seminarOnMainPage.shortDescription}
                                    </div>

                                    <div class="school-info_action">
                                        <c:choose>
                                            <c:when test = "${seminarTypeName == 'EXTERNAL'}">
                                                <a href="${seminarOnMainPage.url}" target="_blank">
                                                    <span class="school-btn school-btn-default school-btn-lg">Подробности <span class="s-school-descr"></span></span>
                                                </a>                            
                                            </c:when>
                                            <c:otherwise>                            
                                                <c:set var="now" value="<%=new java.util.Date()%>" />
                                                <span class="school-btn school-btn-primary school-btn-lg js-register-button <c:if test="${!(now.time < seminarOnMainPage.timeRegistrationEnd.time)}">disabled</c:if>">
                                                        Записаться <span class="s-school-rocket"></span>
                                                    </span>
                                                    <a href="${schoolHostUrl}/school/${seminarOnMainPage.seminarId}" target="_blank">
                                                    <span class="school-btn school-btn-default school-btn-lg">Подробности <span class="s-school-descr"></span></span>
                                                </a>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>

                                    <div class="school-countdown">
                                        <%@ include file="/WEB-INF/jsp/includes/school-participants-limit.jsp" %>

                                        <%@ include file="/WEB-INF/jsp/includes/school-time-left.jsp" %>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </c:if>





                <div class="school-calendar">

                    <div class="wrap">
                        <div class="col-12">

                            <div class="school-calendar_head school-page-head">
                                Календарь мероприятий
                            </div>


                            <table class="school-calendar_table table seminar-list">
                                <colgroup>
                                    <col width="100%">
                                    <col width="80">
                                    <col width="150">
                                    <col width="250">
                                    <col width="150">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th>Название</th>
                                        <th>Когда</th>
                                        <th>Город</th>
                                        <th colspan="2" class="seminar-tag-selector">
                                        </th>
                                    </tr>
                                </thead>
                            </table>

                            <div class="school-calendar_more">
                                <a href="/calendar"><span class="school-btn school-btn-default school-btn-lg">Показать ещё <span class="s-school-table"></span></span></a>
                            </div>
                        </div>
                    </div>
                </div>




                <div class="school-lessons">
                    <div class="wrap">
                        <div class="col-12 school-lessons_cont">

                            <div class="school-lessons_img"></div>

                            <div class="school-lessons_head school-page-head">
                                Уроки школы краудфандинга
                            </div>


                            <div class="wrap-row">
                                <div class="col-4">
                                    <div class="school-lessons_list">
                                        <div class="school-lessons_i">
                                            <div class="school-lessons_num">1</div>
                                            <div class="school-lessons_name"><a href="${tvHostUrl}/broadcast/367732">Вводное занятие</a></div>
                                        </div>
                                        <div class="school-lessons_i">
                                            <div class="school-lessons_num">2</div>
                                            <div class="school-lessons_name"><a href="${tvHostUrl}/broadcast/370296">Текстовое описание проекта</a></div>
                                        </div>
                                        <div class="school-lessons_i">
                                            <div class="school-lessons_num">3</div>
                                            <div class="school-lessons_name"><a href="${tvHostUrl}/broadcast/375931">Видеообращение</a></div>
                                        </div>
                                        <div class="school-lessons_i">
                                            <div class="school-lessons_num">4</div>
                                            <div class="school-lessons_name"><a href="${tvHostUrl}/broadcast/377870">Акции</a></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-3">
                                    <div class="school-lessons_list">
                                        <div class="school-lessons_i">
                                            <div class="school-lessons_num">5</div>
                                            <div class="school-lessons_name"><a href="${tvHostUrl}/broadcast/381404">Продвижение. Часть 1</a></div>
                                        </div>
                                        <div class="school-lessons_i">
                                            <div class="school-lessons_num">6</div>
                                            <div class="school-lessons_name"><a href="${tvHostUrl}/broadcast/385875">Продвижение. Часть 2</a></div>
                                        </div>
                                        <div class="school-lessons_i">
                                            <div class="school-lessons_num">7</div>
                                            <div class="school-lessons_name"><a href="${tvHostUrl}/broadcast/390157">Продвижение. Часть 3</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="school-lessons_more">
                                <a href="${tvHostUrl}/school" target="_blank">
                                    <span class="school-btn school-btn-default school-btn-lg">Архив занятий <span class="s-school-archive"></span></span>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>




                <div class="school-graduates">
                    <div class="wrap">
                        <div class="col-12">

                            <div class="school-graduates_head school-page-head">
                                Наши выпускники
                            </div>

                            <div class="school-graduates_list">
                                <div class="project-card-list"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%@ include file="/WEB-INF/jsp/includes/footer.jsp" %>
    <div id="registration-modal-form"></div>
</body>
</html>


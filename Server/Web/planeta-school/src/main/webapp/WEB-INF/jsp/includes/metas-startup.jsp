<!-- Sharing meta data: start -->
<meta property="og:site_name" content="school.planeta.ru/startup"/>

<c:choose>
    <c:when test="${not empty customMetaTag.image}">
        <meta property="og:image" content="${hf:getThumbnailUrl(customMetaTag.image, "ORIGINAL", "VIDEO")}" />
    </c:when>
    <c:otherwise>
        <meta property="og:image" content="https://${hf:getStaticBaseUrl("")}/images/school/school-sharing.jpg" />
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${not empty customMetaTag.ogTitle}">
        <meta property="og:title" content="${customMetaTag.ogTitle}" />
    </c:when>
    <c:otherwise>
        <meta property="og:title" content="Школа краудфандинга planeta.ru для стартапов" />
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${not empty customMetaTag.ogDescription}">
        <meta property="og:description" content="${customMetaTag.ogDescription}" />
    </c:when>
    <c:otherwise>
        <meta property="og:description" content="Школа краудфандинга — это образовательное направление Planeta.ru краудфандинговой платформы № 1 в России. Мы помогаем понять, из чего состоит и как работает краудфандинг в теории и на практике.
Лекции, семинары, мастер-классы, вебинары и практические интенсивы. Все это «Школа краудфандинга» Planeta.ru." />
    </c:otherwise>
</c:choose>
<!-- Sharing meta data: end -->
<c:choose>
    <c:when test="${not empty customMetaTag.title}">
        <title>${customMetaTag.title}</title>
    </c:when>
    <c:otherwise>
        <title>Школа краудфандинга planeta.ru для стартапов</title>
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${not empty customMetaTag.description}">
        <meta name="description" content="${customMetaTag.description}"/>
    </c:when>
    <c:otherwise>
        <meta name="description" content="Школа краудфандинга — это образовательное направление Planeta.ru краудфандинговой платформы № 1 в России. Мы помогаем понять, из чего состоит и как работает краудфандинг в теории и на практике.
Лекции, семинары, мастер-классы, вебинары и практические интенсивы. Все это «Школа краудфандинга» Planeta.ru."/>
    </c:otherwise>
</c:choose>

<c:if test="${not empty customMetaTag.keywords}">
    <meta name="keywords" content="${customMetaTag.keywords}"/>
</c:if>
<meta name="viewport" content="width=device-width">

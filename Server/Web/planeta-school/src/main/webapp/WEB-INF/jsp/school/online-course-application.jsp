<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<head>
    <%@ include file="/WEB-INF/jsp/includes/generated/main-page-meta-header.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/metas-online-course-application.jsp" %>

    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/interactive-landing.css" />
    <p:script src="school-interactive-landing.js"/>
    <script>
        $(function () {
            $('.js-send-online-course-application').click(function (e) {
                e.preventDefault();
                $('#online-course-application-form').removeClass('error');
                $('.request_error').hide();
                $('.request_error').html('');
                var form = $("#online-course-application-form").serializeObject();
                $.post('/school/online-course-application-request.json', form).done(function (response) {
                    if (response) {
                        if (response.success) {
                            $('.request_form').html('<div class="request_success">Ваше сообщение отправлено!</div>');
                        } else {
                            if (response.fieldErrors) {
                                $('#online-course-application-form').addClass('error');
                                _.each(response.fieldErrors, function (errorValue, errorKey) {
                                    var el = $('.js-form-field-' + errorKey);
                                    el.show();
                                    el.html(errorValue);
                                });
                            } else if (response.errorMessage) {
                                var el = $('.js-form-field-email');
                                el.show();
                                el.html(response.errorMessage);
                            }
                        }
                    }
                });
            });
        });
    </script>
</head>

<body>
<div class="lead wow fadeIn">
    <div class="lead_top">
        <div class="wrap">
            <div class="lead_logo">
                    <span class="school-logo logo">
                        <svg width="220" height="50" viewBox="0 0 632 145">
                            <path d="M132.3 32.3c3.4-5.6 2.7-12.9-2.1-17.7-4.8-4.8-12.1-5.5-17.7-2.1C84.4-6.4 46-3.4 21.2 21.4-7 49.6-7 95.4 21.2 123.6c28.2 28.2 74 28.2 102.2 0 24.8-24.8 27.7-63.2 8.9-91.3m-15.8 84.5c-18 18-44.1 22.7-66.4 14.3-5-2.1-20.2-10.8.9-31.8l-2.8-2.8-2.8-2.8c-20.3 20.3-29.1 6.8-31.6 1.4C5.2 72.7 9.9 46.3 28 28.2c21.3-21.3 54.1-24 78.4-8.3-1.8 5.1-.7 11 3.4 15.1s10 5.2 15.1 3.4c15.7 24.3 12.9 57.1-8.4 78.4M70.7 49.3c-4.8 3.1-8.3 8.1-11.5 13-7.8.5-15.6 3.9-20.2 10.3-.6.9-1.5 2.1-2.1 4.2 1.3.2 3.1.5 4.2.7 2.8.5 6.1.8 9.2 2.7-.6 2.1-1.2 5-1.5 6.9v.5c1.6.8 3.6 1.7 5.2 3.3 1.6 1.6 2.5 3.7 3.3 5.2h.5c1.9-.3 4.8-1 6.9-1.5 1.9 3.1 2.2 6.4 2.7 9.2.2 1.1.5 2.9.7 4.2 2.1-.6 3.3-1.5 4.2-2.1 6.4-4.6 9.8-12.4 10.3-20.2 4.9-3.2 9.8-6.7 13-11.5 5.9-9.1 6.4-20 6.4-31.2-11.4-.1-22.2.3-31.3 6.3m11.8 20.6c-4.2 0-7.5-3.4-7.5-7.5s3.4-7.5 7.5-7.5 7.5 3.4 7.5 7.5-3.4 7.5-7.5 7.5" fill="#1A8CFF"/>
                            <path d="M175 23.5h8v29.8h8V23.5h8v29.8h8V23.5h8v37.2h-40V31m77.3 29.8l-14-18.6 14-18.6H243l-12 16v-16h-8v37.2h8V44.9l12 15.9h9.3zm34.8-18.6c0-13.6-7.2-19.9-17-19.9s-17 6.3-17 19.9c0 13.6 7.2 19.9 17 19.9s17-6.3 17-19.9m-26 0c0-9.5 3.7-12.4 9-12.4 5.4 0 9 3 9 12.4 0 9.5-3.7 12.4-9 12.4-5.4 0-9-3-9-12.4m60.4 18.6V23.5h-22.9L295.9 49c-.4 3.8-1 5-2.8 5-.5 0-2.1-.1-3.3-.3l-1.1 6.9c2.2.6 4.7.9 6.1.9 5.3 0 8.1-4 9-12.4l1.9-18.1h7.8v29.8h8zm40.9 0l-15.8-37.2h-5.3l-15.8 37.2h8.5l2.7-6.4h14.6l2.7 6.4h8.4zM348.2 47h-8.4l2-4.8 2.2-7.9 2.2 7.9 2 4.8zm-143.8 69l-14-18.6 14-18.6h-9.3l-12 16v-16h-8V116h8v-16l12 15.9h9.3zm12.9 0v-12.8h7.2c6.8 0 12.2-3.7 12.2-12.2s-5.6-12.3-12.2-12.3h-15.2V116h8zm0-20.3v-9.6h7.2c2.3 0 4.3.6 4.3 4.8s-2.1 4.7-4.3 4.7h-7.2zm57.5 20.3L259 78.7h-5.3L237.9 116h8.5l2.7-6.4h14.6l2.7 6.4h8.4zm-14.2-13.9h-8.4l2-4.8 2.2-7.9 2.2 7.9 2 4.8zm29.1 2.9c-1.4 3.2-2 4.3-4.5 4.3-1.3 0-3.8-.3-5.7-.9l-1.1 6.9c2.6 1 4.5 1.4 6.9 1.4 7.6 0 9.7-4.4 13.6-13.5l10.7-24.5H301l-7.6 17.7-8.9-17.7h-8.9l14.1 26.3zm54.3 18.9v-15.4h-4V78.7h-24l-2.4 23.2c-.5 3.8-1.5 6.6-4 6.6h-1.2v15.4h6.9v-8h21.8v8h6.9zm-12-37.7v22.3h-12.6c1.1-2 1.9-4.8 2.3-8.5l1.4-13.8h8.9zm37.5 29.8v-6.4h1.9c7.2 0 13.3-4.1 13.3-13.8 0-9.6-6.2-13.9-13.3-13.9h-1.9v-3.2h-8v3.2h-1.9c-7.1 0-13.3 4.3-13.3 13.9 0 9.6 5.9 13.8 13.3 13.8h1.9v6.4h8zm-8-13.9h-1.9c-2.7 0-5.3-.7-5.3-6.3s2.5-6.4 5.3-6.4h1.9v12.7zm8 0V89.4h1.9c2.8 0 5.3.8 5.3 6.4 0 5.6-2.6 6.3-5.3 6.3h-1.9zm52 13.9l-15.8-37.2h-5.3L384.6 116h8.5l2.7-6.4h14.6l2.7 6.4h8.4zm-14.2-13.9h-8.4l2-4.8 2.2-7.9 2.2 7.9 2 4.8zm47.5 13.9V78.7h-8v14.9h-13.4V78.7h-8V116h8v-14.9h13.4V116h8zm39.9 7.9v-15.4h-4V78.7h-24l-2.4 23.2c-.5 3.8-1.5 6.6-4 6.6h-1.2v15.4h6.9v-8h21.8v8h6.9zm-12-37.7v22.3h-12.6c1.1-2 1.9-4.8 2.3-8.5l1.4-13.8h8.9zm45.5 29.8V78.7h-7.4l-.5 3.6-13.4 17.7V78.7h-8V116h7.4l.5-3.5 13.4-17.8V116h8zm37.3 0V78.7h-8v14.9h-13.4V78.7h-8V116h8v-14.9h13.4V116h8zm16 0V86.2h14.6v-7.4h-22.6V116h8zm50.3 0L616 78.7h-5.3L594.9 116h8.5l2.7-6.4h14.6l2.7 6.4h8.4zm-14.3-13.9h-8.4l2-4.8 2.2-7.9 2.2 7.9 2 4.8z" fill="#FFF"/>
                        </svg>
                    </span>
            </div>
            <div class="lead_action">
                <button class="intr-land-btn" onclick="javascript: $('html, body').animate({ scrollTop: $('.js-application-request').offset().top }, 'slow');">Записаться на курс</button>
            </div>
        </div>
    </div>

    <div class="wrap">
        <div class="lead_head">
            Создайте свой проект за&nbsp;<span class="text-hl">60&nbsp;минут</span>
        </div>

        <div class="lead_text">
            Нас часто спрашивают: сколько времени необходимо для создания <nobr>крауд-проекта</nobr>? На&nbsp;самом деле нет предела совершенству. Можно посвятить хоть всю жизнь доведению своей <nobr>крауд-кампании</nobr> до&nbsp;идеала, но&nbsp;благодаря нашему курсу вы&nbsp;сможете подготовить черновик проекта всего за&nbsp;60&nbsp;минут или даже быстрее.
        </div>
    </div>
</div>


<div class="what wow slideUp" data-wow-offset="150">
    <div class="what_wrap">
        <div class="wrap">

            <div class="what_block">

                <div class="what_list">
                    <div class="what_i">
                        <div class="what_title">
                            Почему онлайн-курс?
                        </div>
                        <div class="what_text">
                            Мы&nbsp;знаем, что желающих пройти обучение краудфандингу куда больше, чем мы&nbsp;можем охватить на&nbsp;лекциях и&nbsp;семинарах. Поэтому мы&nbsp;создали интерактивный <nobr>онлайн-курс</nobr>, который позволит вам учиться когда угодно и&nbsp;где угодно абсолютно бесплатно.
                        </div>
                    </div>

                    <div class="what_i">
                        <div class="what_title">
                            Что мы предлагаем
                        </div>
                        <div class="what_text">
                            Динамичный и&nbsp;удобный способ получить всю необходимую информацию о&nbsp;создании <nobr>крауд-проектов</nobr> и&nbsp;применить знания на&nbsp;практике.
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>



<div class="options">
    <div class="wrap">
        <div class="options_list wrap-row">
            <div class="options_i col-4 wow slideUp" data-wow-offset="150">
                <div class="options_i-cont">
                    <div class="options_i-cnt">
                        <div class="options_ico">
                            <svg width="86" height="77" viewBox="0 0 86 77">
                                <path d="M86 38.5a4.777 4.777 0 0 0-2.636-4.292l-6.404-3.222 6.404-3.22A4.777 4.777 0 0 0 86 23.472a4.778 4.778 0 0 0-2.636-4.292L47.214.995a9.425 9.425 0 0 0-8.43 0L2.637 19.181A4.776 4.776 0 0 0 0 23.473a4.777 4.777 0 0 0 2.636 4.292l6.403 3.221-6.404 3.222A4.777 4.777 0 0 0 0 38.5a4.777 4.777 0 0 0 2.636 4.292l6.403 3.222-6.402 3.22A4.775 4.775 0 0 0 0 53.525C0 55.355 1.01 57 2.636 57.82l36.15 18.186A9.385 9.385 0 0 0 43 77a9.392 9.392 0 0 0 4.215-.995l36.15-18.186A4.777 4.777 0 0 0 86 53.525a4.774 4.774 0 0 0-2.636-4.29l-6.404-3.221 6.404-3.222A4.778 4.778 0 0 0 86 38.5zM4.45 24.235A.79.79 0 0 1 4 23.5a.79.79 0 0 1 .45-.734L40.555 4.578A5.439 5.439 0 0 1 43 4c.84 0 1.68.193 2.444.578L81.55 22.766A.79.79 0 0 1 82 23.5a.79.79 0 0 1-.45.735L45.444 42.422a5.46 5.46 0 0 1-4.889 0L4.451 24.235zm77.1 28.36c.391.199.45.548.45.74a.799.799 0 0 1-.45.74L45.445 72.418a5.418 5.418 0 0 1-4.89 0L4.452 54.076A.8.8 0 0 1 4 53.334c0-.19.058-.54.45-.74L13.495 48 38.79 60.85A9.3 9.3 0 0 0 43 61.853a9.306 9.306 0 0 0 4.21-1.003L72.505 48l9.046 4.595zm0-13.52L45.445 57.418a5.418 5.418 0 0 1-4.889 0L4.451 39.076a.798.798 0 0 1-.451-.74c0-.192.058-.542.45-.741L13.495 33 38.79 45.85A9.3 9.3 0 0 0 43 46.853a9.302 9.302 0 0 0 4.21-1.003L72.505 33l9.044 4.594c.393.2.451.55.451.741 0 .191-.058.541-.45.74z" fill="#36F"/>
                                <path d="M86 38.52c0-1.83-1.01-3.476-2.636-4.296L76.96 31 47.215 46.042c-1.406.622-2.81.933-4.215.933-1.405 0-2.81-.311-4.215-.933L9.04 31l-6.404 3.225A4.782 4.782 0 0 0 0 38.52c0 1.83 1.01 3.476 2.636 4.296l6.403 3.225 29.746 14.962A9.378 9.378 0 0 0 43 62a9.384 9.384 0 0 0 4.215-.996l29.746-14.962 6.404-3.225A4.783 4.783 0 0 0 86 38.521zm-4.45.556L45.445 57.417a5.418 5.418 0 0 1-4.889 0L4.451 39.076a.798.798 0 0 1-.451-.74c0-.192.058-.542.45-.741L13.495 33 38.8 45.85a9.302 9.302 0 0 0 8.409 0L72.505 33l9.044 4.594c.393.2.451.55.451.741 0 .191-.058.541-.45.74z" fill="#1B2336"/>
                            </svg>
                        </div>
                        <div class="options_name">
                            12 уроков
                        </div>
                    </div>
                </div>
            </div>

            <div class="options_i col-4 wow slideUp" data-wow-offset="150" data-wow-delay="300ms">
                <div class="options_i-cont">
                    <div class="options_i-cnt">
                        <div class="options_ico">
                            <svg width="76" height="76" viewBox="0 0 76 76">
                                <path d="M38.507 0C17.047 0 0 17.047 0 38.507 0 58.953 17.047 76 38.507 76 58.953 76 76 58.953 76 38.507 76 17.047 58.953 0 38.507 0zM38 72C19.253 72 4 56.747 4 38S19.253 4 38 4s34 15.253 34 34-15.253 34-34 34z" fill="#36F"/>
                                <path d="M39 10c-.983 0-1.556.567-2 1v28H19c-.428.462-1 1.029-1 2 0 .433.572 2 1 2h21c.428 0 1-.567 1-1V11c0-.433-1.572-1-2-1z" fill="#1B2336"/>
                            </svg>
                        </div>
                        <div class="options_name">
                            60 минут
                        </div>
                    </div>
                </div>
            </div>

            <div class="options_i col-4 wow slideUp" data-wow-offset="150" data-wow-delay="600ms">
                <div class="options_i-cont">
                    <div class="options_i-cnt">
                        <div class="options_ico">
                            <svg width="78" height="77" viewBox="0 0 78 77">
                                <path d="M71.443 30.097a5.097 5.097 0 0 0 1.301-5.264 5.11 5.11 0 0 0-4.153-3.495l-17.07-2.475a1.028 1.028 0 0 1-.774-.562L43.113 2.862A5.114 5.114 0 0 0 38.5 0a5.114 5.114 0 0 0-4.613 2.862l-7.634 15.439a1.03 1.03 0 0 1-.775.562l-17.07 2.475a5.11 5.11 0 0 0-4.152 3.495 5.096 5.096 0 0 0 1.302 5.264l12.351 12.017c.243.236.353.576.296.909L15.29 59.991a5.099 5.099 0 0 0 2.047 5.023 5.121 5.121 0 0 0 5.418.39l15.267-8.01c.3-.158.658-.158.958 0l15.267 8.01c.76.4 1.582.596 2.4.596a5.13 5.13 0 0 0 3.018-.987 5.099 5.099 0 0 0 2.047-5.022l-2.916-16.968a1.027 1.027 0 0 1 .296-.91l12.352-12.016zM54.8 43.757l2.927 17.032c.098.57-.255.895-.41 1.008a1.009 1.009 0 0 1-1.089.079l-15.326-8.042a5.172 5.172 0 0 0-4.806 0L20.77 61.875c-.513.27-.932.035-1.088-.078a1.003 1.003 0 0 1-.41-1.008l2.926-17.032a5.146 5.146 0 0 0-1.485-4.562l-12.4-12.062a1.003 1.003 0 0 1-.26-1.056c.059-.184.26-.619.833-.702l17.135-2.485a5.163 5.163 0 0 0 3.889-2.82l7.663-15.495c.256-.519.733-.575.926-.575.193 0 .67.056.926.575L47.09 20.07a5.162 5.162 0 0 0 3.889 2.82l17.135 2.484c.573.083.774.518.834.702.06.183.153.653-.262 1.056L56.286 39.195a5.146 5.146 0 0 0-1.485 4.562z" fill="#36F"/>
                                <path d="M62.192 3.386a1.92 1.92 0 0 0-2.737.448l-2.08 2.955c-.637.903-.443 2.168.433 2.825.348.26.751.386 1.15.386.606 0 1.204-.29 1.587-.834l2.08-2.954c.637-.904.443-2.17-.433-2.826zM19.626 6.788l-2.08-2.954a1.921 1.921 0 0 0-2.738-.448c-.876.657-1.07 1.922-.434 2.825l2.08 2.955c.384.545.982.834 1.588.834.4 0 .802-.126 1.15-.387.876-.656 1.07-1.92.434-2.825zM7.894 46.35c-.367-1.026-1.58-1.587-2.711-1.254l-3.696 1.09C.357 46.52-.26 47.624.106 48.65.4 49.475 1.245 50 2.152 50c.22 0 .444-.031.665-.096l3.696-1.091c1.13-.333 1.748-1.436 1.381-2.462zM39 69c-1.105 0-2 .941-2 2.102v3.796c0 1.16.895 2.102 2 2.102s2-.941 2-2.102v-3.796C41 69.94 40.105 69 39 69zM76.513 46.187l-3.696-1.09c-1.13-.334-2.344.227-2.711 1.254-.367 1.026.251 2.129 1.381 2.462l3.696 1.09c.22.066.445.097.665.097.907 0 1.75-.525 2.046-1.351.367-1.026-.251-2.129-1.381-2.462z" fill="#1B2336"/>
                            </svg>
                        </div>
                        <div class="options_name">
                            Готовый проект
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="video">
    <div class="wrap">
        <div class="video_cont wrap-row">
            <div class="video_col col-6 col-push-6 wow slideUp" data-wow-offset="300" data-wow-delay="300ms">
                <div class="video_text">
                    Все необходимые базовые знания для запуска <span class="text-hl"><nobr>крауд-проекта</nobr></span> в&nbsp;удобной видеоформе
                </div>
            </div>
            <div class="video_col col-6 col-pull-6 wow slideUp" data-wow-offset="300">
                <div class="video_tablet">
                    <div class="video_cover"
                         style="background-image:url(//${hf:getStaticBaseUrl("")}/images/interactive-landing/video-cover.jpg);">
                        <span class="video-play" data-toggle="modal" data-target="#modal-video"><span class="video-play_ico"></span></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="advantage">
    <div class="wrap">

        <div class="advantage_head wow slideUp" data-wow-offset="50">
            Преимущества <nobr class="text-hl">онлайн-курса</nobr>
        </div>


        <div class="advantage_list">
            <div class="advantage_i wow slideUp" data-wow-offset="100">
                <div class="advantage_num">
                    1
                </div>
                <div class="advantage_name">
                    Просто
                </div>
                <div class="advantage_descr">
                    Простые и&nbsp;понятные шаги. Поэтапный <nobr>онлайн-курс</nobr> обеспечит полное понимание структуры проекта и&nbsp;позволит без труда приступить к&nbsp;его созданию.
                </div>
            </div>

            <div class="advantage_i wow slideUp" data-wow-offset="100">
                <div class="advantage_num">
                    2
                </div>
                <div class="advantage_name">
                    Практично
                </div>
                <div class="advantage_descr">
                    От&nbsp;теории к&nbsp;практике. Все полученные знания вы&nbsp;сможете тут&nbsp;же применить для создания собственной кампании.
                </div>
            </div>

            <div class="advantage_i wow slideUp" data-wow-offset="100">
                <div class="advantage_num">
                    3
                </div>
                <div class="advantage_name">
                    Понятно
                </div>
                <div class="advantage_descr">
                    Простой и&nbsp;доступный язык обучения в&nbsp;видеоформате. Мы&nbsp;подготовили максимально понятный и&nbsp;доступный курс, который не&nbsp;только полезно слушать, но&nbsp;и&nbsp;приятно смотреть.
                </div>
            </div>

            <div class="advantage_i wow slideUp" data-wow-offset="100">
                <div class="advantage_num">
                    4
                </div>
                <div class="advantage_name">
                    Удобно
                </div>
                <div class="advantage_descr">
                    Всегда есть возможность дополнить или переделать каждое задание. Возвращайтесь к&nbsp;предыдущим шагам в&nbsp;любое время, чтобы еще раз прослушать теорию и&nbsp;внести правки в&nbsp;свой проект.
                </div>
            </div>

            <div class="advantage_i wow slideUp" data-wow-offset="100">
                <div class="advantage_num">
                    5
                </div>
                <div class="advantage_name">
                    Наглядно
                </div>
                <div class="advantage_descr">
                    Каждый шаг мы&nbsp;иллюстрируем реальными историями успешного краудфандинга.
                </div>
            </div>

            <div class="advantage_i wow slideUp" data-wow-offset="100">
                <div class="advantage_num">
                    6
                </div>
                <div class="advantage_name">
                    Доступно
                </div>
                <div class="advantage_descr">
                    Теперь на&nbsp;занятия &laquo;Школы краудфандинга&raquo; можно попасть с&nbsp;помощью компьютера в&nbsp;любое удобное время.
                </div>
            </div>
        </div>

    </div>
</div>




<div class="request js-application-request">
    <div class="wrap">
        <div class="request_cont wow slideUp" data-wow-offset="100">
            <div class="request_head">
                Получи <span class="text-hl">доступ к&nbsp;курсу</span>
            </div>

            <div class="request_text">
                Хотите стать одним из&nbsp;первых счастливчиков, получивших доступ к&nbsp;<nobr>онлайн-курсу</nobr> &laquo;Школы краудфандинга&raquo;? Оставляйте заявку, и&nbsp;мы&nbsp;сообщим вам о&nbsp;старте программы!
            </div>


            <form id="online-course-application-form" class="request_form">
                <div class="request_fieldset">
                    <input name="name" type="text" class="form-control" placeholder="Имя">
                </div>
                <div class="request_error js-form-field-name" display="none;">
                </div>
                <div class="request_fieldset">
                    <input name="email" type="text" class="form-control" placeholder="Электронная почта">
                </div>
                <div class="request_error js-form-field-email hidden" display="none;">
                </div>
                <div class="request_action">
                    <button class="intr-land-btn js-send-online-course-application" onclick="javascript:void(0);">Отправить</button>
                </div>
            </form>
        </div>
    </div>
</div>




<div class="footer wow fadeIn" data-wow-offset="-200">
    <div class="footer_dev">
        Разработано в <a href="https://planeta.ru/" class="pln-logo logo"></a>
    </div>
</div>



<div class="modal-dialog hide" id="modal-video">
    <div class="modal modal-video">
        <div class="modal-close">
            <a class="close" data-dismiss="modal">×</a>
        </div>
        <div class="embed-video">
            <iframe width="560" height="315" data-src="https://www.youtube.com/embed/IdQrGkO6GM8?autoplay=1" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>
</div>




<script>
    $('#modal-video').on('shown.pln.modal', function () {
        var frame = $('#modal-video iframe');
        frame.attr('src', frame.data('src'));
    });
    $('#modal-video').on('hidden.pln.modal', function () {
        var frame = $('#modal-video iframe');
        frame.attr('src', '');
    });
</script>



<script>
    new WOW().init();
</script>
</body>
</html>
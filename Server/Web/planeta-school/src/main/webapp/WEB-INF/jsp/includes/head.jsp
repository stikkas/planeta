<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8"%>

<%@ include file="/WEB-INF/jsp/includes/common-css.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/planeta-js.jsp" %>

<%@include file="/WEB-INF/jsp/includes/generated/common-favicon.jsp" %>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<script>
    var workspace = {};
    workspace.staticNodesService = new StaticNodesService("${staticNode}", ${hf:toJson(staticNodes)}, '${hf:getStaticBaseUrl("")}');
</script>



<script type="text/javascript">
    $(document).ready(function () {
        if (workspace) {
            workspace.initRegisterDialogs = function (seminarId, formatSelection, seminarTypeName) {
                switch (seminarTypeName) {
                    case "SOLO":
                        new SeminarRegistration.Views.Solo({
                            model: new SeminarRegistration.Models.Main({
                                seminarId: seminarId,
                                formatSelection: formatSelection
                            }),
                            callback: {
                                send: function(model, view) {
                                    SeminarRegistration.Models
                                            .sync("${schoolHostUrl}/school/seminar-registration-send-solo.json", model)
                                            .done(function() {
                                                view.dispose();
                                                new SeminarRegistration.Views.Success({}).render();
                                            })
                                            .fail(function(errorMessage, fieldErrors){
                                                Form.highlightErrors(fieldErrors);
                                            });
                                }
                            }
                        }).render();
                        break;
                    case "SOLOSIMPLE":
                        new SeminarRegistration.Views.SoloSimple({
                            model: new SeminarRegistration.Models.Main({
                                seminarId: seminarId,
                                formatSelection: formatSelection
                            }),
                            callback: {
                                send: function(model, view) {
                                    SeminarRegistration.Models
                                            .sync("${schoolHostUrl}/school/seminar-registration-send-solosimple.json", model)
                                            .done(function() {
                                                view.dispose();
                                                new SeminarRegistration.Views.Success({}).render();
                                            })
                                            .fail(function (errorMessage, fieldErrors) {
                                                Form.highlightErrors(fieldErrors);
                                            });
                                }
                            }
                        }).render();
                        break;
                    case "PAIR":
                        new SeminarRegistration.Views.Pair({
                            model: new SeminarRegistration.Models.Main({
                                seminarId: seminarId,
                                formatSelection: formatSelection
                            }),
                            callback: {
                                send: function(model, view) {
                                    SeminarRegistration.Models
                                            .sync("${schoolHostUrl}/school/seminar-registration-send-pair.json", model)
                                            .done(function() {
                                                view.dispose();
                                                new SeminarRegistration.Views.Success({}).render();
                                            })
                                            .fail(function (errorMessage, fieldErrors) {
                                                Form.highlightErrors(fieldErrors);
                                            });
                                }
                            }
                        }).render();
                        break;
                    case "WEBINAR":
                        new SeminarRegistration.Views.Webinar({
                            model: new SeminarRegistration.Models.Main({
                                seminarId: seminarId,
                                formatSelection: formatSelection
                            }),
                            callback: {
                                send: function(model, view) {
                                    SeminarRegistration.Models
                                            .sync("${schoolHostUrl}/school/seminar-registration-send-webinar.json", model)
                                            .done(function() {
                                                view.dispose();
                                                new SeminarRegistration.Views.Success({}).render();
                                            })
                                            .fail(function (errorMessage, fieldErrors) {
                                                Form.highlightErrors(fieldErrors);
                                            });
                                }
                            }
                        }).render();
                        break;
                    case "COMPANY":
                        new SeminarRegistration.Views.Company({
                            model: new SeminarRegistration.Models.Main({
                                seminarId: seminarId,
                                formatSelection: formatSelection
                            }),
                            callback: {
                                send: function(model, view) {
                                    SeminarRegistration.Models
                                            .sync("${schoolHostUrl}/school/seminar-registration-send-company.json", model)
                                            .done(function() {
                                                view.dispose();
                                                new SeminarRegistration.Views.Success({}).render();
                                            })
                                            .fail(function (errorMessage, fieldErrors) {
                                                Form.highlightErrors(fieldErrors);
                                            });
                                }
                            }
                        }).render();
                        break;
                    default:
                        break;
                }
            }
        }
        var seminar = ${hf:toJson(seminar)};
        if (seminar){
            var seminarId = seminar ? seminar.seminarId : 0
            var seminarisAllowFormatSelection = seminar ? seminar.formatSelection : false;
            var seminarTypeName = seminar ? seminar.seminarType : '';
            $('.js-register-button').bind("click", _.bind(workspace.initRegisterDialogs, null, seminarId, seminarisAllowFormatSelection, seminarTypeName));
        }
    });
</script>
<div class="school-header">
    <div class="wrap">
        <div class="col-12">
            <a href="${schoolHostUrl}" class="school-logo"></a>

            <div class="school-menu">
                <div class="school-menu_i">
                    <a class="school-menu_link" href="${tvHostUrl}/school">Архив</a>
                </div>
                <div class="school-menu_i">
                    <a class="school-menu_link" href="${schoolHostUrl}/contacts">Контакты</a>
                </div>
            </div>

            <div class="school-header-social">
                <a href="http://vk.com/planetaru" class="school-header-social_link" target="_blank" rel="nofollow noopener">
                    <span class="s-school-header-social-vk"></span>
                </a>
                <a href="https://www.facebook.com/planetaru" class="school-header-social_link" target="_blank" rel="nofollow noopener">
                    <span class="s-school-header-social-fb"></span>
                </a>
                <a href="http://instagram.com/planeta_ru" class="school-header-social_link" target="_blank" rel="nofollow noopener">
                    <span class="s-school-header-social-ig"></span>
                </a>
            </div>
        </div>
    </div>
</div>
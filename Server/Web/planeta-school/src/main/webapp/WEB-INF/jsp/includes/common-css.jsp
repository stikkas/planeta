<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>

<%-- Dividing css in 2 files (because of IE bug) --%>
<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/common.css" />
<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/school.css"/>
<%--<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/tv.css" />--%>
<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css/jquery.fancybox.css"/>
<!--[if lte IE 9]>
<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css/placeholder_polyfill.css"/>
<![endif]-->
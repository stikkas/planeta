package ru.planeta.school.controllers

import ru.planeta.api.web.controllers.IAction

enum class Actions(prefix: ActionPrefixes, private val _text: String) : IAction {
    WELCOME(ActionPrefixes.WELCOME, "welcome"),
    SEMINAR(ActionPrefixes.SCHOOL, "seminar"),
    CALENDAR(ActionPrefixes.SCHOOL, "seminars-calendar"),
    CONTACTS(ActionPrefixes.STATIC, "contacts"),
    STARTUP(ActionPrefixes.WELCOME, "startup"),
    ONLINE_COURSE_APPLICATION(ActionPrefixes.SCHOOL, "online-course-application");

    private val text
        get(): String = prefix + _text

    private val prefix: String = prefix.toString()

    override val path: String
        get() = text

    override val actionName: String
        get() = _text

    override fun toString(): String = text
}


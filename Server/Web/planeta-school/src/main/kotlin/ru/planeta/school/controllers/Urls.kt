package ru.planeta.school.controllers

object Urls {
    const val ROOT = "/"

    const val SEMINAR = "/school/{seminarId}"
    const val CALENDAR = "/calendar"
    const val CONTACTS = "/contacts"
    const val STARTUP = "/startup"


    const val SEMINAR_CALENDAR_BY_TAG_ID = "/school/get-seminars-calendar.json"
    const val SEMINAR_TAGS_EXISTS_IN_SEMINARS = "/school/get-tags-exists-in-seminars.json"

    const val SEMINAR_REGISTRATION_SEND_SOLO = "/school/seminar-registration-send-solo.json"
    const val SEMINAR_REGISTRATION_SEND_SOLOSIMPLE = "/school/seminar-registration-send-solosimple.json"
    const val SEMINAR_REGISTRATION_SEND_PAIR = "/school/seminar-registration-send-pair.json"
    const val SEMINAR_REGISTRATION_SEND_WEBINAR = "/school/seminar-registration-send-webinar.json"
    const val SEMINAR_REGISTRATION_SEND_COMPANY = "/school/seminar-registration-send-company.json"
    const val TRACK_MANUAL_PDF = "/api/profile/track-manual-pdf.json"

    const val SCHOOL_ONLINE_COURSE_LANDING = "/online-course-application"
    const val SCHOOL_APPLICATION_TO_ACCESS_ONLINE_COURSE = "/school/online-course-application-request.json"

    const val SITE_MAP_INDEX = "/sitemapindex.xml"
}

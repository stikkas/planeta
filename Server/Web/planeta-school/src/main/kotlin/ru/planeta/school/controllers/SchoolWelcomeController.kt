package ru.planeta.school.controllers

import org.apache.commons.lang3.ArrayUtils
import org.springframework.context.MessageSource
import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.Utils.empty
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.helper.ProjectService
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.news.LoggerService
import ru.planeta.api.search.SearchService
import ru.planeta.api.service.common.PartnerService
import ru.planeta.api.service.common.SeminarService
import ru.planeta.api.service.configurations.ConfigurationService
import ru.planeta.api.service.notifications.NotificationService
import ru.planeta.api.service.profile.SeminarRegistrationService
import ru.planeta.api.service.registration.RegistrationService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.IAction
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.api.web.utils.JstlHelperFunctions
import ru.planeta.dao.commondb.BaseSpeakerService
import ru.planeta.dao.trashcan.SchoolOnLineApplicationsDAO
import ru.planeta.model.common.campaign.CampaignTag
import ru.planeta.model.common.school.*
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.enums.SeminarType
import ru.planeta.model.commondb.Partner
import ru.planeta.model.commondb.Speaker
import ru.planeta.model.news.ProfileNews
import ru.planeta.model.stat.RefererStatType
import ru.planeta.model.stat.RequestStat
import ru.planeta.model.trashcan.SchoolOnlineCourseApplication
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid

/**
 * School web app welcome controller
 */
@Controller
class SchoolWelcomeController(private val seminarService: SeminarService,
                              private val speakerService: BaseSpeakerService,
                              private val partnerService: PartnerService,
                              private val registrationService: RegistrationService,
                              private val seminarRegistrationService: SeminarRegistrationService,
                              private val notificationService: NotificationService,
                              private val schoolOnLineApplicationsDAO: SchoolOnLineApplicationsDAO,
                              private val configurationService: ConfigurationService,
                              private val searchService: SearchService,
                              private val messageSource: MessageSource,
                              private val profileNewsService: LoggerService,
                              private val projectService: ProjectService,
                              private val baseControllerService: BaseControllerService) {

    @GetMapping(Urls.SEMINAR_TAGS_EXISTS_IN_SEMINARS)
    @ResponseBody
    fun tagsExistsInSeminars(): List<CampaignTag> = seminarService.selectCampaignTagsExistsInSeminars()

    @GetMapping(Urls.ROOT)
    fun welcome(): ModelAndView {
        val seminars = seminarService.selectSeminarWithTagNameAndCityNameListIsShownOnMainPage(true, 0, 1)

        return if (!empty(seminars)) {
            val seminarOnTheMainPage: SeminarWithTagNameAndCityName = seminars[0]
            createModelAndViewWithDateNumberArrays(seminarOnTheMainPage, Actions.WELCOME, ProjectType.SCHOOL)
                    .addObject("seminarOnMainPage", seminarOnTheMainPage)
                    .addObject("seminarTypeName", seminarOnTheMainPage.seminarType?.name)
        } else createDefaultSchoolModelAndView(Actions.WELCOME, ProjectType.SCHOOL)
    }

    @GetMapping(Urls.SEMINAR)
    fun getSeminar(@ModelAttribute("seminar") seminar: SeminarWithTagNameAndCityName,
                   errors: BindingResult,
                   @PathVariable("seminarId") seminarId: Long): ModelAndView {
        var seminar = seminar

        if (!errors.hasErrors()) {
            if (seminarId > 0) {
                seminar = seminarService.selectSeminarWithTagNameAndCityName(seminarId)
                if (seminar == null) {
                    throw NotFoundException(Seminar::class.java, seminarId)
                }

                if (seminar.seminarType == SeminarType.EXTERNAL) {
                    throw PermissionException(MessageCode.PERMISSION_EXCEPTION)
                }
            } else {
                throw NotFoundException(Seminar::class.java, seminarId)
            }
        }

        val modelAndView = createModelAndViewWithDateNumberArrays(seminar, Actions.SEMINAR, ProjectType.SCHOOL)
                .addObject("seminar", seminar)
                .addObject("seminarTypeName", seminar.seminarType?.name)

        val speakers: List<Speaker> = if (seminar.speakersIds != null) {
            speakerService.selectSpeakerList(Arrays.asList(*ArrayUtils.toObject(seminar.speakersIds)))
        } else {
            emptyList()
        }
        modelAndView.addObject("speakers", speakers)

        val partners: List<Partner> = if (seminar.partnersIds != null) {
            partnerService.selectPartnerList(Arrays.asList(*ArrayUtils.toObject(seminar.partnersIds)))
        } else {
            emptyList()
        }
        modelAndView.addObject("partners", partners)

        return modelAndView
    }

    @GetMapping(Urls.STARTUP)
    fun seminar(): ModelAndView {
        val seminarId = configurationService.startupSeminarId.toLong()
        var startupSeminar: Seminar? = null
        if (seminarId > 0) {
            startupSeminar = seminarService.selectSeminar(seminarId)
        }

        val modelAndView = createDefaultSchoolModelAndView(Actions.STARTUP, ProjectType.SCHOOL)
        if (startupSeminar != null) {
            modelAndView.addObject("seminarOnMainPage", startupSeminar)
        }

        return modelAndView
    }


    @GetMapping(Urls.CALENDAR)
    fun calendar(): ModelAndView {
        val seminarSearchResult = searchService.searchForSeminarsByTagId(0L, Date(), 0, 10)
        val seminars = seminarSearchResult.searchResultRecords

        return createDefaultSchoolModelAndView(Actions.CALENDAR, ProjectType.SCHOOL)
                .addObject("seminars", seminars)
    }

    @GetMapping(Urls.SEMINAR_CALENDAR_BY_TAG_ID)
    @ResponseBody
    fun getSeminarsCalendarByTagId(@RequestParam(defaultValue = "0") tagId: Long,
                                   @RequestParam(defaultValue = "0") offset: Int,
                                   @RequestParam(defaultValue = "10") limit: Int): List<SeminarWithTagNameAndCityName> =
            searchService.searchForSeminarsByTagId(tagId, Date(), offset, limit).searchResultRecords

    @GetMapping(Urls.CONTACTS)
    fun contacts(): ModelAndView = createDefaultSchoolModelAndView(Actions.CONTACTS, ProjectType.SCHOOL)

    @PostMapping(Urls.SEMINAR_REGISTRATION_SEND_SOLO)
    @ResponseBody
    fun saveSeminarRegistrationSolo(@RequestBody @Valid seminarRegistration: SeminarRegistrationSolo,
                                    result: BindingResult,
                                    request: HttpServletRequest, response: HttpServletResponse): ActionStatus<SeminarRegistrationSolo> {
        if (result.hasErrors()) {
            return ActionStatus.createErrorStatus(result, messageSource)
        }
        val seminar = seminarService.selectSeminar(seminarRegistration.seminarId)
        if (isRegistrationEnd(seminar)) {
            return ActionStatus.createErrorStatus("field.required.seminar.registration.date.expired", messageSource)
        }

        val profileId = registrationService.autoRegisterIfNotExists(seminarRegistration.email, seminarRegistration.fio, getStatRequestSchool(request, response))
        seminarRegistration.profileId = profileId
        seminarRegistrationService.insertOrUpdateSeminarRegistration(seminarRegistration)
        notificationService.sendSchoolSoloRegistration(seminarRegistration.email, seminar)
        return ActionStatus.createSuccessStatus()
    }

    @PostMapping(Urls.SEMINAR_REGISTRATION_SEND_SOLOSIMPLE)
    @ResponseBody
    fun saveSeminarRegistrationSoloSimple(@RequestBody @Valid seminarRegistration: SeminarRegistrationSoloSimple,
                                          result: BindingResult,
                                          request: HttpServletRequest, response: HttpServletResponse): ActionStatus<SeminarRegistrationSoloSimple> {
        if (result.hasErrors()) {
            return ActionStatus.createErrorStatus(result, messageSource)
        }
        val seminar = seminarService.selectSeminar(seminarRegistration.seminarId)
        if (isRegistrationEnd(seminar)) {
            return ActionStatus.createErrorStatus("field.required.seminar.registration.date.expired", messageSource)
        }

        val profileId = registrationService.autoRegisterIfNotExists(seminarRegistration.email, seminarRegistration.fio, getStatRequestSchool(request, response))
        seminarRegistration.profileId = profileId
        seminarRegistrationService.insertOrUpdateSeminarRegistration(seminarRegistration)
        notificationService.sendSchoolSolosimpleRegistration(seminarRegistration, seminar)
        return ActionStatus.createSuccessStatus()
    }

    @PostMapping(Urls.SEMINAR_REGISTRATION_SEND_PAIR)
    @ResponseBody
    fun saveSeminarRegistrationPair(@RequestBody @Valid seminarRegistration: SeminarRegistrationPair,
                                    result: BindingResult,
                                    request: HttpServletRequest, response: HttpServletResponse): ActionStatus<SeminarRegistrationPair> {
        if (result.hasErrors()) {
            return ActionStatus.createErrorStatus(result, messageSource)
        }
        val seminar = seminarService.selectSeminar(seminarRegistration.seminarId)
        if (isRegistrationEnd(seminar)) {
            return ActionStatus.createErrorStatus("field.required.seminar.registration.date.expired", messageSource)
        }

        val profileId = registrationService.autoRegisterIfNotExists(seminarRegistration.email, seminarRegistration.fio, getStatRequestSchool(request, response))
        seminarRegistration.profileId = profileId

        val profileId2 = registrationService.autoRegisterIfNotExists(seminarRegistration.email2, seminarRegistration.fio2, getStatRequestSchool(request, response))
        seminarRegistration.profileId2 = profileId2
        seminarRegistrationService.insertOrUpdateSeminarRegistration(seminarRegistration)
        notificationService.sendSchoolPairRegistration(seminarRegistration.email, seminarRegistration.email2, seminar)
        return ActionStatus.createSuccessStatus()
    }

    @PostMapping(Urls.SEMINAR_REGISTRATION_SEND_WEBINAR)
    @ResponseBody
    fun saveSeminarRegistrationWebinar(@RequestBody @Valid seminarRegistration: SeminarRegistrationWebinar,
                                       result: BindingResult,
                                       request: HttpServletRequest, response: HttpServletResponse): ActionStatus<SeminarRegistrationWebinar> {
        if (result.hasErrors()) {
            return ActionStatus.createErrorStatus(result, messageSource)
        }
        val seminar = seminarService.selectSeminar(seminarRegistration.seminarId)
        if (isRegistrationEnd(seminar)) {
            return ActionStatus.createErrorStatus("field.required.seminar.registration.date.expired", messageSource)
        }

        val profileId = registrationService.autoRegisterIfNotExists(seminarRegistration.email, seminarRegistration.fio, getStatRequestSchool(request, response))
        seminarRegistration.profileId = profileId
        seminarRegistrationService.insertOrUpdateSeminarRegistration(seminarRegistration)
        notificationService.sendSchoolWebinarRegistration(seminarRegistration.email, seminar)
        return ActionStatus.createSuccessStatus()
    }

    @PostMapping(Urls.SEMINAR_REGISTRATION_SEND_COMPANY)
    @ResponseBody
    fun saveSeminarRegistrationCompany(@RequestBody @Valid seminarRegistration: SeminarRegistrationCompany,
                                       result: BindingResult,
                                       request: HttpServletRequest, response: HttpServletResponse): ActionStatus<SeminarRegistrationCompany> {
        if (result.hasErrors()) {
            return ActionStatus.createErrorStatus(result, messageSource)
        }
        val seminar = seminarService.selectSeminar(seminarRegistration.seminarId)
        if (isRegistrationEnd(seminar)) {
            return ActionStatus.createErrorStatus("field.required.seminar.registration.date.expired", messageSource)
        }

        val profileId = registrationService.autoRegisterIfNotExists(seminarRegistration.email, seminarRegistration.fio, getStatRequestSchool(request, response))
        seminarRegistration.profileId = profileId

        seminarRegistrationService.insertOrUpdateSeminarRegistration(seminarRegistration)

        notificationService.sendSchoolCompanyRegistration(seminarRegistration, seminar)
        return ActionStatus.createSuccessStatus()
    }

    private fun createDefaultSchoolModelAndView(action: IAction, pType: ProjectType): ModelAndView =
            baseControllerService.createDefaultModelAndView(action, pType)
                    .addObject("schoolHostUrl", projectService.getUrl(ProjectType.SCHOOL))
                    .addObject("tvHostUrl", projectService.getUrl(ProjectType.TV))

    private fun createModelAndViewWithDateNumberArrays(seminar: SeminarWithTagNameAndCityName, action: IAction, projectType: ProjectType): ModelAndView {
        val modelAndView = createDefaultSchoolModelAndView(action, projectType)
        if (seminar.participantsLimit != null) {
            modelAndView.addObject("arrayParticipantsLimit", seminar.participantsLimit.toString().toCharArray())
        }

        val timeLeftCalendar = Calendar.getInstance()
        val now = Date()
        val timeLeftMills = seminar.timeStart.time - now.time
        modelAndView.addObject("timeLeftMills", timeLeftMills)

        if (timeLeftMills > 0) {
            timeLeftCalendar.time = Date(timeLeftMills)

            val daysLeft = JstlHelperFunctions.getDateDifferenceInDays(now, seminar.timeStart)
            modelAndView.addObject("daysLeft", daysLeft)
            modelAndView.addObject("arrayDaysLeft", daysLeft.toString().toCharArray())

            val hoursLeft = timeLeftCalendar.get(Calendar.HOUR_OF_DAY)
            modelAndView.addObject("hoursLeft", hoursLeft)
            modelAndView.addObject("arrayHoursLeft", hoursLeft.toString().toCharArray())

            val minutesLeft = timeLeftCalendar.get(Calendar.MINUTE)
            modelAndView.addObject("minutesLeft", minutesLeft)
            modelAndView.addObject("arrayMinutesLeft", minutesLeft.toString().toCharArray())
        }

        return modelAndView
    }

    @GetMapping(Urls.TRACK_MANUAL_PDF)
    @ResponseBody
    fun saveSeminarRegistrationCompany(@RequestParam("link") link: Long): ActionStatus<SeminarRegistrationCompany> {
        profileNewsService.addProfileNews(ProfileNews.Type.SCHOOL_DOWNLOAD_PDF, myProfileId(), link)
        return ActionStatus.createSuccessStatus()
    }

    @GetMapping(Urls.SCHOOL_ONLINE_COURSE_LANDING)
    fun openOnlineCourseApplication(): ModelAndView = baseControllerService.createDefaultModelAndView(Actions.ONLINE_COURSE_APPLICATION, ProjectType.SCHOOL)

    @PostMapping(Urls.SCHOOL_APPLICATION_TO_ACCESS_ONLINE_COURSE)
    @ResponseBody
    fun addOnlineCourseApplication(@Valid onlineCourseApplication: SchoolOnlineCourseApplication,
                                   errors: BindingResult): ActionStatus<*> {
        if (errors.hasErrors()) {
            return baseControllerService.createErrorStatus<Any>(errors)
        }
        val selectedOnlineCourseApplication = schoolOnLineApplicationsDAO.select(onlineCourseApplication.email)
        if (selectedOnlineCourseApplication == null) {
            schoolOnLineApplicationsDAO.insert(onlineCourseApplication)
        }
        notificationService.sendSchoolOnlineCourse(onlineCourseApplication)
        return ActionStatus.createSuccessStatus<Any>()
    }

    private fun isRegistrationEnd(seminar: Seminar): Boolean = seminar.timeRegistrationEnd.before(Date())


    private fun getStatRequestSchool(request: HttpServletRequest, response: HttpServletResponse): RequestStat =
            baseControllerService.getRequestStat(request, response, RefererStatType.REGISTRATION, ProjectType.SCHOOL)
}

package ru.planeta.school.controllers

enum class ActionPrefixes(private val text: String) {
    WELCOME("welcome/"),
    SCHOOL("school/"),
    STATIC("static/");

    override fun toString(): String = text
}





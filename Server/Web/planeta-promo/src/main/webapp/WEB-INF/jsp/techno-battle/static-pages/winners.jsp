<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>

<%@ include file="/WEB-INF/jsp/techno-battle/includes/head.jsp" %>

<body>
<p:script src="technobattle-news.js"></p:script>
<script>
    $(document).ready(function() {
        $('.js-view-video').click(function (e) {
            e.preventDefault();
            var $el = $(e.currentTarget);
            var iframeUrl = $el.data('video');
            if (iframeUrl) {
                var videoModel = new BaseModel({iframeurl: iframeUrl});
                Modal.showDialogByViewClass(Technobattle.Views.VideoDialog, videoModel, null, null, null, null, true);
            }
        });
    });
</script>

<%@ include file="/WEB-INF/jsp/techno-battle/includes/sidebar.jsp" %>
<div class="wrapper">
    <div class="main">
        <%@ include file="/WEB-INF/jsp/techno-battle/includes/topbar.jsp" %>

        <div class="container">
            <div class="china-videos">
                <div class="china-videos_text">
                    <div class="china-videos_text-in">
                        <div class="china-videos_head">
                            <span class="china-videos_head-hl-1">Русские</span>
                            <span class="china-videos_head-hl-2">мейкеры</span>
                            в&nbsp;китае
                        </div>

                        <div class="china-videos_descr">
                            Два финалиста «Битвы технологий» в рамках специальной номинации, учрежденной при поддержке генерального партнера шоу, компании РВК, отправились на стажировку в крупнейший фаблаб Евразии, расположенный в Шэньчжэне (Китай).
                            <br>
                            <br>
                            Встреча в СИДА и SZOIL. Обзор запуска производственных процессов в Шеньчжене. Знакомство с промышленным дизайном. Обсуждение вопроса запуска проектов представителей Заказчика в г.Шеньчжень. Выстраивание работы с партнерами и контрагентами.
                            <br>
                            <br>
                            Еще больше подробностей в разделе <a href="/techbattle/news">новости</a>.
                        </div>
                    </div>
                </div>


                <div class="china-videos_cont">
                    <div class="china-videos_list">
                        <c:if test="${not empty videos}">
                            <c:forEach var="video" items="${videos}" varStatus="loop">
                                <div class="china-videos_i js-view-video" data-video="${video.iframeurl}">
                                    <div class="china-videos_cover" style="background-image:url(<c:if test="${loop.index == 0}">${hf:getThumbnailUrl(video.image,"ORIGINAL","VIDEO")}</c:if><c:if test="${loop.index > 0}">${hf:getThumbnailUrl(video.image,"USER_AVATAR","VIDEO")}</c:if>);">
                                        <span class="video-play"><span class="video-play_ico"></span></span>
                                    </div>
                                </div>
                            </c:forEach>
                        </c:if>
                    </div>

                    <div class="china-videos_more">
                        <a href="/techbattle/video" class="tech-btn tech-btn-primary">Больше видео</a>
                    </div>
                </div>
            </div>



            <div class="tech-winners-prizes">
                <div class="tech-winners-prizes_head">
                    Каждый <span class="tech-winners-prizes_head-hl-1">победитель</span> <span class="tech-winners-prizes_head-hl-2">получит</span>:
                </div>

                <div class="tech-winners-prizes_list">
                    <div class="tech-winners-prizes_i">
                        <div class="tech-winners-prizes_ico">
                            <img src="//${hf:getStaticBaseUrl("")}/images/tech/prizes/winners-prizes-rub.png">
                        </div>
                        <div class="tech-winners-prizes_name">
                            Стартовый взнос в свой <nobr>крауд-проект</nobr> в размере <nobr>100 000</nobr>&nbsp;рублей
                        </div>
                    </div>

                    <div class="tech-winners-prizes_i">
                        <div class="tech-winners-prizes_ico">
                            <img src="//${hf:getStaticBaseUrl("")}/images/tech/prizes/winners-prizes-pln.png">
                        </div>
                        <div class="tech-winners-prizes_name">
                            Специальный пакет продвижения проекта от&nbsp;Planeta.ru
                        </div>
                    </div>

                    <div class="tech-winners-prizes_i">
                        <div class="tech-winners-prizes_ico">
                            <img src="//${hf:getStaticBaseUrl("")}/images/tech/prizes/winners-prizes-kurator.png">
                        </div>
                        <div class="tech-winners-prizes_name">
                            Персонального <nobr>куратора-трекера</nobr> на весь период <nobr>крауд-кампании</nobr>
                        </div>
                    </div>

                    <div class="tech-winners-prizes_i">
                        <div class="tech-winners-prizes_ico">
                            <img src="//${hf:getStaticBaseUrl("")}/images/tech/prizes/winners-prizes-mentor.png">
                        </div>
                        <div class="tech-winners-prizes_name">
                            Селебрити-ментора, который станет амбассадором проекта
                        </div>
                    </div>

                    <div class="tech-winners-prizes_i">
                        <div class="tech-winners-prizes_ico">
                            <img src="//${hf:getStaticBaseUrl("")}/images/tech/prizes/winners-prizes-eva.png">
                        </div>
                        <div class="tech-winners-prizes_name">
                            Встречу с инвесторами и рекомендации по подготовке инвестиционных материалов
                        </div>
                    </div>
                </div>
            </div>



            <div class="tech-winner">
                <div class="tech-winner_text">
                    <div class="tech-winner_text-in">
                        <div class="tech-winner_head">
                            Победители. Выбор экспертного совета.
                        </div>

                        <div class="tech-winner_date">
                            <span class="tech-winner_date-hl">1 марта</span> 2017 года
                        </div>

                        <div class="tech-winner_descr">
                            Кто из участников «Битвы технологий» лучше всех готов к гонке коллективного финансирования? <a href="/techbattle/experts">Эксперты</a> сделали выбор, встречайте победителей!
                        </div>
                    </div>
                </div>


                <div class="tech-winner_cont">

                    <div class="tech-projects">

                        <svg xmlns="http://www.w3.org/2000/svg" style="display: none">
                            <defs><g id="like"><path d="M10.062.75c-1.052 0-1.895.385-2.638 1.144l-.001.002h-.001L7 2.439l-.423-.511-.001-.001C5.832 1.168 4.989.75 3.937.75c-1.052 0-2.04.418-2.784 1.177A4.032 4.032 0 0 0 0 4.768c0 1.072.409 2.081 1.152 2.84l5.226 5.378a.866.866 0 0 0 1.243 0l5.226-5.378A4.038 4.038 0 0 0 14 4.768c0-1.073-.41-2.082-1.154-2.841A3.872 3.872 0 0 0 10.062.75"/></g></defs>
                        </svg>

                        <div class="tech-projects_list">
                            <div class="tech-projects_i">
                                <a href="/techbattle/project/6" class="tech-projects_link">
                                    <span class="tech-projects_winner">
                                        <span class="tech-projects_winner-ico"></span>
                                        <span class="tech-projects_winner-cont">
                                            <span class="tech-projects_winner-head">
                                                Победитель
                                            </span>
                                            <span class="tech-projects_winner-text">
                                                Выбор экспертного совета
                                            </span>
                                        </span>
                                    </span>

                                    <span class="tech-projects_cover">
                                        <img src="https://s3.planeta.ru/i/17305f/product_cover_new.jpg">
                                        <span class="tech-projects_hover">
                                            <span class="tech-projects_hover-cont">Подробнее</span>
                                        </span>
                                    </span>

                                    <span class="tech-projects_cont">
                                        <span class="tech-projects_name">
                                            Комплект для создания робототехники LIVETRONIC
                                        </span>
                                        <span class="tech-projects_likes-block">
                                            <span class="tech-projects_likes">
                                                <span class="tech-projects_likes-ico">
                                                    <svg viewBox="0 0 14 14"><use xlink:href="#like"></use></svg>
                                                </span>
                                                <span class="tech-projects_likes-val">
                                                    522
                                                </span>
                                            </span>
                                        </span>

                                        <span class="tech-projects_more-cont">
                                            <span class="tech-projects_more">
                                                о проекте
                                            </span>
                                        </span>
                                    </span>
                                </a>
                            </div>
                            <div class="tech-projects_i">
                                <a href="/techbattle/project/3" class="tech-projects_link">
                                    <span class="tech-projects_winner">
                                        <span class="tech-projects_winner-ico"></span>
                                        <span class="tech-projects_winner-cont">
                                            <span class="tech-projects_winner-head">
                                                Победитель
                                            </span>
                                            <span class="tech-projects_winner-text">
                                                Выбор экспертного совета
                                            </span>
                                        </span>
                                    </span>

                                    <span class="tech-projects_cover">
                                        <img src="https://s3.planeta.ru/i/173065/product_cover_new.jpg">
                                        <span class="tech-projects_hover">
                                            <span class="tech-projects_hover-cont">Подробнее</span>
                                        </span>
                                    </span>

                                    <span class="tech-projects_cont">
                                        <span class="tech-projects_name">
                                            ЭЛЕКТРИЧЕСКАЯ ПРИСТАВКА ДЛЯ ИНВАЛИДНОГО КРЕСЛА UNA
                                        </span>
                                        <span class="tech-projects_likes-block">
                                            <span class="tech-projects_likes">
                                                <span class="tech-projects_likes-ico">
                                                    <svg viewBox="0 0 14 14"><use xlink:href="#like"></use></svg>
                                                </span>
                                                <span class="tech-projects_likes-val">
                                                    451
                                                </span>
                                            </span>
                                        </span>

                                        <span class="tech-projects_more-cont">
                                            <span class="tech-projects_more">
                                                о проекте
                                            </span>
                                        </span>
                                    </span>
                                </a>
                            </div>
                            <div class="tech-projects_i">
                                <a href="/techbattle/project/8" class="tech-projects_link">
                                    <span class="tech-projects_winner">
                                        <span class="tech-projects_winner-ico"></span>
                                        <span class="tech-projects_winner-cont">
                                            <span class="tech-projects_winner-head">
                                                Победитель
                                            </span>
                                            <span class="tech-projects_winner-text">
                                                Выбор экспертного совета
                                            </span>
                                        </span>
                                    </span>

                                    <span class="tech-projects_cover">
                                        <img src="https://s3.planeta.ru/i/178eea/product_cover_new.jpg">
                                        <span class="tech-projects_hover">
                                            <span class="tech-projects_hover-cont">Подробнее</span>
                                        </span>
                                    </span>

                                    <span class="tech-projects_cont">
                                        <span class="tech-projects_name">
                                            АВТОМАТИЧЕСКАЯ КОРМУШКА ДЛЯ ЖИВОТНЫХ ANIMO
                                        </span>
                                        <span class="tech-projects_likes-block">
                                            <span class="tech-projects_likes">
                                                <span class="tech-projects_likes-ico">
                                                    <svg viewBox="0 0 14 14"><use xlink:href="#like"></use></svg>
                                                </span>
                                                <span class="tech-projects_likes-val">
                                                    376
                                                </span>
                                            </span>
                                        </span>

                                        <span class="tech-projects_more-cont">
                                            <span class="tech-projects_more">
                                                о проекте
                                            </span>
                                        </span>
                                    </span>
                                </a>
                            </div>
                            <div class="tech-projects_i">
                                <a href="/techbattle/project/9" class="tech-projects_link">
                                    <span class="tech-projects_winner">
                                        <span class="tech-projects_winner-ico"></span>
                                        <span class="tech-projects_winner-cont">
                                            <span class="tech-projects_winner-head">
                                                Победитель
                                            </span>
                                            <span class="tech-projects_winner-text">
                                                Выбор экспертного совета
                                            </span>
                                        </span>
                                    </span>

                                    <span class="tech-projects_cover">
                                        <img src="https://s3.planeta.ru/i/178b70/product_cover_new.jpg">
                                        <span class="tech-projects_hover">
                                            <span class="tech-projects_hover-cont">Подробнее</span>
                                        </span>
                                    </span>

                                    <span class="tech-projects_cont">
                                        <span class="tech-projects_name">
                                            ДЕТСКАЯ ИГРУШКА «MISHKA»
                                        </span>
                                        <span class="tech-projects_likes-block">
                                            <span class="tech-projects_likes">
                                                <span class="tech-projects_likes-ico">
                                                    <svg viewBox="0 0 14 14"><use xlink:href="#like"></use></svg>
                                                </span>
                                                <span class="tech-projects_likes-val">
                                                    41
                                                </span>
                                            </span>
                                        </span>

                                        <span class="tech-projects_more-cont">
                                            <span class="tech-projects_more">
                                                о проекте
                                            </span>
                                        </span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="tech-winner-peoples">
                <div class="tech-winner-peoples_text">
                    <div class="tech-winner-peoples_text-in">
                        <div class="tech-winner-peoples_head">
                            Победитель <span class="tech-winner-peoples_head-hl-1">народного</span> <span class="tech-winner-peoples_head-hl-2">голосования</span>
                        </div>
                        <div class="tech-winner-peoples_descr">
                            Весь февраль на странице реалити-шоу шло голосование за самые перспективные разработки этого года. Самым интересным гаджетом по мнению зрителей «Битвы технологий» стал тайм-трекер <a href="/techbattle/project/10">TimeFlip</a>.
                            <br>
                            <br>
                            Это значит, что разработчики устройства первыми вошли в число победителей реалити-шоу. Поздравляем команду!
                        </div>
                    </div>
                </div>

                <div class="tech-winner-peoples_cont">
                    <div class="tech-projects">

                        <svg xmlns="http://www.w3.org/2000/svg" style="display: none">
                            <defs>
                                <g id="like">
                                    <path d="M10.062.75c-1.052 0-1.895.385-2.638 1.144l-.001.002h-.001L7 2.439l-.423-.511-.001-.001C5.832 1.168 4.989.75 3.937.75c-1.052 0-2.04.418-2.784 1.177A4.032 4.032 0 0 0 0 4.768c0 1.072.409 2.081 1.152 2.84l5.226 5.378a.866.866 0 0 0 1.243 0l5.226-5.378A4.038 4.038 0 0 0 14 4.768c0-1.073-.41-2.082-1.154-2.841A3.872 3.872 0 0 0 10.062.75"/>
                                </g>
                            </defs>
                        </svg>

                        <div class="tech-projects_list">
                            <div class="tech-projects_i">
                                <a href="/techbattle/project/10" class="tech-projects_link  tech-projects-winner">
                                    <span class="tech-projects_winner">
                                        <span class="tech-projects_winner-ico"></span>
                                        <span class="tech-projects_winner-cont">
                                            <span class="tech-projects_winner-head">
                                                Победитель
                                            </span>
                                            <span class="tech-projects_winner-text">
                                                Народного голосования
                                            </span>
                                        </span>
                                    </span>

                                    <span class="tech-projects_cover">
                                        <img src="https://s3.planeta.ru/i/178ce1/product_cover_new.jpg">
                                        <span class="tech-projects_hover">
                                            <span class="tech-projects_hover-cont">Подробнее</span>
                                        </span>
                                    </span>
                                    <span class="tech-projects_cont">
                                        <span class="tech-projects_name">
                                            TimeFlip: гаджет для контроля вашего  времени
                                        </span>
                                        <span class="tech-projects_likes-block">
                                            <span class="tech-projects_likes">
                                                <span class="tech-projects_likes-ico">
                                                    <svg viewBox="0 0 14 14"><use xlink:href="#like"></use></svg>
                                                </span>
                                                <span class="tech-projects_likes-val">
                                                    999
                                                </span>
                                            </span>
                                        </span>
                                        <span class="tech-projects_more-cont">
                                            <span class="tech-projects_more">
                                                о проекте
                                            </span>
                                        </span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%@ include file="/WEB-INF/jsp/techno-battle/includes/footer.jsp" %>

</div>

</body>
</html>
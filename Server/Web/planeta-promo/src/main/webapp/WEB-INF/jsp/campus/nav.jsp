<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="campus-nav">
    <div class="wrap">
        <div class="col-12">
            <div class="campus-nav_list">
                <div class="campus-nav_i">
                    <a class="campus-nav_link" href="#campus-lead">О проекте</a>
                </div>
                <div class="campus-nav_i">
                    <a class="campus-nav_link" href="/campus/#campus-members">Участники</a>
                </div>
                <div class="campus-nav_i">
                    <a class="campus-nav_link" href="/campus/#campus-projects">Проекты</a>
                </div>
                <div class="campus-nav_i">
                    <a class="campus-nav_link" href="/campus/#campus-faq">FAQ</a>
                </div>
            </div>
        </div>
    </div>
</div>


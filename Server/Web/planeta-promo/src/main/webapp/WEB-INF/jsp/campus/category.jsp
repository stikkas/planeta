<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>

<head>
    <%@ include file="/WEB-INF/jsp/campus/common-css.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp" %>
    <%@ include file="/WEB-INF/jsp/campus/category-js.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/generated/screen-width.jsp" %>

    <%@include file="/WEB-INF/jsp/includes/generated/common-favicon.jsp" %>

    <meta content="width=device-width" name="viewport">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@ include file="/WEB-INF/jsp/includes/generated/main-page-meta-header.jsp" %>
    <%@ include file="/WEB-INF/jsp/campus/metas-welcome.jsp" %>
</head>
<body class="campus-page grid-1200">
    <%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>

    <div id="global-container">
        <div class="wrap-container" id="main-container">
            <div id="center-container">
                <div class="campus-header">
                    <a class="campus-logo-small" href="/campus"></a>
                </div>
                <div class="js-banner-container-top"></div>
                <%@ include file="/WEB-INF/jsp/campus/nav.jsp" %>
                <%@ include file="/WEB-INF/jsp/campus/lead_small.jsp" %>
                <div class="wrap">
                    <div class="col-12">
                        <div class="campus-name">${campus.name}</div>
                        <div class="campus-create">
                            <a class="btn btn-campus-primary" href="//${properties['application.host']}/funding-rules?${campus.tagName}&UNIVERSITY">Создать проект от ${campus.shortName}</a>
                        </div>
                    </div>
                </div>
                <div class="campus-projects">
                    <div class="wrap">
                        <div class="col-12">
                            <div class="campus-projects-head">
                                <div class="campus-projects-head_title">Проекты</div>
                                <div class="campus-projects-type">
                                    <div class="campus-projects-type_i">
                                        <a href="javascript:void(0);" data-status="ACTIVE" class="campus-projects-type_link">Актуальные проекты</a>
                                    </div>
                                    <div class="campus-projects-type_i">
                                        <a href="javascript:void(0);" data-status="CLOSE_TO_FINISH" class="campus-projects-type_link">Близки к завершению</a>
                                    </div>
                                    <div class="campus-projects-type_i active">
                                        <a href="javascript:void(0);" class="campus-projects-type_link">Все</a>
                                    </div>
                                </div>
                            </div>
                            <div class="campus-projects-list">
                                <div class="project-card-list js-campus-campaigns">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%@ include file="/WEB-INF/jsp/includes/generated/irma/footer.jsp" %>
</body>
</html>







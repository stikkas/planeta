<div class="what" id="what">
    <div class="wrap wow slideUp" data-wow-offset="250">
        <div class="col-12">
            <div class="what_head">
                Что дает
                <br>
                «Битва технологий»
            </div>

            <div class="what_list">
                <div class="what_i">
                    <div class="what_name">
                        Теория и <span class="what_name-hl">практика</span>
                    </div>
                    <div class="what_descr">
                        Навыки краудфандинга и&nbsp;общения с&nbsp;инвесторами и&nbsp;целевой аудиторией
                    </div>
                </div>

                <div class="what_i">
                    <div class="what_name">
                        Собственный <span class="what_name-hl">проект</span>
                    </div>
                    <div class="what_descr">
                        Упакованный <nobr>крауд-проект</nobr>, одобренный экспертами
                    </div>
                </div>

                <div class="what_i">
                    <div class="what_name">
                        Реклама и <span class="what_name-hl">PR</span>
                    </div>
                    <div class="what_descr">
                        Готовый план продвижения всех этапов <nobr>крауд-кампании</nobr>
                    </div>
                </div>

                <div class="what_i">
                    <div class="what_name">
                        Известность <span class="what_name-hl">проекта</span>
                    </div>
                    <div class="what_descr">
                        Узнаваемость вашего продукта за&nbsp;счет СМИ и&nbsp;партнеров проекта
                    </div>
                </div>

                <div class="what_i">
                    <div class="what_name">
                        Быстрый <span class="what_name-hl">старт</span>
                    </div>
                    <div class="what_descr">
                        Первоначальный денежный взнос в&nbsp;<nobr>крауд-кампании</nobr> победителей <b>&laquo;Битвы технологий&raquo;</b>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>

    <%@ include file="/WEB-INF/jsp/techno-battle/includes/project-metas.jsp" %>

    <%@include file="/WEB-INF/jsp/includes/generated/common-favicon.jsp" %>

    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <%@ include file="/WEB-INF/jsp/techno-battle/common-css.jsp" %>
    <%@ include file="/WEB-INF/jsp/techno-battle/tech-css.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/generated/stat-counters-head.jsp" %>
    <%@ include file="/WEB-INF/jsp/techno-battle/planeta-js.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/generated/screen-width.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/generated/back-to-top.jsp" %>
</head>
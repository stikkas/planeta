<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>

<%@ include file="/WEB-INF/jsp/techno-battle/includes/project-head.jsp" %>



<script>
    $(function() {
        var techProjectsTabsModel = new Tech.Models.TechProjectTabs({
            profileId: ${profileId},
            projectId: ${projectId},
            tab: '${tab}'
        });

        var techProjectsTabsView = new Tech.Views.TechProjectTabs({
            model: techProjectsTabsModel,
            el: "#js-project-tab"
        });
        techProjectsTabsView.model.set('tab', '${tab}');
        techProjectsTabsView.render();

        if ('${tab}' == "about") {
            techProjectsTabsView.initDescription();
        }

        if ('${tab}' == "news") {
            techProjectsTabsView.initNews();
        }

        if ('${tab}' == "comments") {
            techProjectsTabsView.initComments();
        }

        $('.js-${tab}').show();

        Backbone.history.start({pushState: true, silent: true});

        <c:if test="${not empty post}">
        $("#tech-news-post-popup").css("display", "");
        var postModel = new News.Models.Post(${hf:toJson(post)});
        postModel.set({authorProfileId: ${profileId}});
        var view = new Tech.Views.ModalPost({
            el: '#tech-news-post-popup',
            model: postModel
        });
        view.render();
        </c:if>
    });
</script>
<body>
<p:script src="technobattle-project.js"></p:script>

<div id="tech-news-post-popup" class="modal modal-feed-post" style="display: none;"></div>

<%@ include file="/WEB-INF/jsp/techno-battle/includes/sidebar.jsp" %>
<div class="wrapper">
    <div class="main">
        <%@ include file="/WEB-INF/jsp/techno-battle/includes/topbar.jsp" %>
        <div class="container">
            <div class="wrap">
            <c:if test="${isAdmin}">
                <div class="project-name">
                    <div class="project-name_title"></div>
                    <div class="project-name_add">
                        <div class="project-name_vote">
                            <a class="tech-btn tech-btn-primary" href="${newsAddUrl}" target="_blank">
                                Добавить новость
                            </a>
                        </div>
                    </div>
                </div>
            </c:if>

                <div class="project-name">
                    <div class="project-name_title">
                        ${project.title}
                    </div>

                    <div class="project-name_add">
                        <div class="project-name_likes">
                            <div class="project-name_likes-ico">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 14">
                                    <path d="M10.062.75c-1.052 0-1.895.385-2.638 1.144l-.001.002h-.001L7 2.439l-.423-.511-.001-.001C5.832 1.168 4.989.75 3.937.75c-1.052 0-2.04.418-2.784 1.177A4.032 4.032 0 0 0 0 4.768c0 1.072.409 2.081 1.152 2.84l5.226 5.378a.866.866 0 0 0 1.243 0l5.226-5.378A4.038 4.038 0 0 0 14 4.768c0-1.073-.41-2.082-1.154-2.841A3.872 3.872 0 0 0 10.062.75"/>
                                </svg>
                            </div>
                            <div class="project-name_likes-val">
                                ${votesCount}
                            </div>
                        </div>

                    </div>
                </div>


                <div class="project-info">
                    <div class="project-info_cover"<c:if test="${not empty project.imageUrl}"> style="background-image:url(${hf:getThumbnailUrl(project.imageUrl, "HUGE", "PHOTO")});"</c:if>>
                        <c:if test="${not empty project.videoSrc}">
                            <iframe src="${project.videoSrc}" width="590" height="322" frameborder="0" allowfullscreen></iframe>
                        </c:if>
                    </div>

                    <div class="project-info_meta">
                        <div class="project-info_i">
                            <div class="project-info_lbl">
                                Целевая аудитория
                            </div>
                            <div class="project-info_val">
                                ${project.consumers}
                            </div>
                        </div>
                        <div class="project-info_i city">
                            <div class="project-info_lbl">
                                Город
                            </div>
                            <div class="project-info_val">
                                Москва
                            </div>
                        </div>
                        <div class="project-info_i">
                            <c:if test="${not empty project.campaignAlias}">
                                <a href="https://${properties['application.host']}/campaigns/${project.campaignAlias}" class="tech-btn tech-btn-primary btn-block">Поддержать проект</a>
                            </c:if>
                            <c:if test="${empty project.campaignAlias and not empty project.campaignId and project.campaignId > 0}">
                                <a href="https://${properties['application.host']}/campaigns/${project.campaignId}" class="tech-btn tech-btn-primary btn-block">Поддержать проект</a>
                            </c:if>
                        </div>
                    </div>
                </div>

                <div id="js-project-tab">
                </div>


                <div class="project-content">
                    <div class="project-content_inner">
                        <div class="project-description js-about js-project-tab-content" style="display: none;">
                            <div class="js-description-inner"></div>
                        </div>
                        <div class="project-news js-news js-project-tab-content" style="display: none;">
                            <div class="js-news-inner"></div>
                        </div>
                        <div class="project-comments js-comments js-project-tab-content" style="display: none;">
                            <div class="js-comments-inner"></div>
                        </div>
                    </div>
                    <!-- /description tab content -->
                </div>
            </div>


        </div>
    </div>
    <%@ include file="/WEB-INF/jsp/techno-battle/includes/footer.jsp" %>
</div>

</body>
</html>
<div class="cost" id="cost">
    <div class="wrap wow slideUp" data-wow-offset="250">
        <div class="col-12">
            <div class="cost_block">
                <div class="wrap-row">
                    <div class="cost_block-i col-6">
                        <div class="cost_head">Оплата обучения</div>

                        <div class="cost_lead">
                            Обучение доступно только <span class="cost_lead-hl">для&nbsp;10&nbsp;проектов</span>.
                            <br>
                            <br>
                            Проект может быть представлен командой <span class="cost_lead-hl"><nobr>от&nbsp;1 до&nbsp;4 человек</nobr></span>.
                            <br>
                            <br>
                            Подробную информацию о стоимости участия в "Битве технологий" и порядке оплаты вы сможете найти в
                            <a href="http://files.planeta.ru/technobattle/rules.pdf">правилах</a> программы
                        </div>

                        <div class="cost_date">
                            Заявки принимаются <span class="cost_date-hl">до 20 декабря</span> 2016 года
                        </div>
                    </div>

                    <div class="cost_block-i col-6">
                        <div class="cost_text">
                            <div class="cost_text-head">Она включает в себя:</div>

                            <div class="cost_text-text">
                                <ul>
                                    <li>участие в&nbsp;образовательной программе (теоретический и&nbsp;практический курсы)</li>
                                    <li>помощь в&nbsp;подготовке и&nbsp;съёмке видеообращения</li>
                                    <li>услуги редактора и&nbsp;корректура текста</li>
                                    <li>помощь дизайнера при оформлении проекта (вёрстка и&nbsp;дизайн элементов для текстового описания)</li>
                                    <li>финансовая и&nbsp;юридическая экспертиза</li>
                                    <li>логистический консалтинг для проработки отправки вознаграждений после завершения <nobr>крауд-кампании</nobr></li>
                                    <li>возможность вести блог проекта на&nbsp;одном из&nbsp;крупнейших техноресурсов страны</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="cost_action">
                <a href="https://vk.com/techbattle" class="tech-request-btn js-tech-request-3" target="_blank" rel="nofollow noopener">Подписаться</a>
            </div>
        </div>
    </div>
</div>
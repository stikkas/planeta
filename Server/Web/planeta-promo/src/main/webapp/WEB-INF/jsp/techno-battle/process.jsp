<div class="process" id="process">
    <div class="wrap">
        <div class="col-12">
            <div class="process_block wow slideUp100" data-wow-offset="0">
                <div class="process_banner">
                    <a href="https://vk.com/techbattle" target="_blank" rel="nofollow noopener">
                        <img src="//${hf:getStaticBaseUrl("")}/images/promo/technobattle/tech-battle.jpg">
                    </a>
                </div>

                <div class="process_cont">
                    <div class="process_head">
                        Процесс обучения
                    </div>

                    <div class="process_list">
                        <div class="process_i wow slideLeft" data-wow-offset="150">
                            <div class="process_data">
                                <div class="process_count">
                                    1-й <span class="process_count-bl">шаг</span>
                                </div>
                                <div class="process_ico">
                                    <div class="process_back-rect wow rectBackOffsetX" data-wow-offset="150" data-wow-delay="1000ms"></div>
                                    <span class="s-icons-people"></span>
                                </div>
                            </div>

                            <div class="process_text">
                                <div class="process_text-in">
                                    <div class="process_name">
                                        Отбор hardware-проектов
                                    </div>
                                    <div class="process_descr">
                                        После получения заявки вас пригласят на&nbsp;отборочный митап, где отберут 10 участников.
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="process_i wow slideLeft" data-wow-offset="150">
                            <div class="process_data">
                                <div class="process_count">
                                    2-й <span class="process_count-bl">шаг</span>
                                </div>
                                <div class="process_ico">
                                    <div class="process_back-rect wow rectBackOffsetX" data-wow-offset="150" data-wow-delay="1000ms"></div>
                                    <span class="s-icons-edit"></span>
                                </div>
                            </div>

                            <div class="process_text">
                                <div class="process_text-in">
                                    <div class="process_name">
                                        Образовательный курс
                                    </div>
                                    <div class="process_descr">
                                        Вы&nbsp;проработаете вместе с&nbsp;экспертами все этапы краудфандинга и&nbsp;будете вести блог о&nbsp;проекте на&nbsp;одном из&nbsp;крупных техноресурсов РФ.
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="process_i wow slideLeft" data-wow-offset="150">
                            <div class="process_data">
                                <div class="process_count">
                                    3-й <span class="process_count-bl">шаг</span>
                                </div>
                                <div class="process_ico">
                                    <div class="process_back-rect wow rectBackOffsetX" data-wow-offset="150" data-wow-delay="1000ms"></div>
                                    <span class="s-icons-bag"></span>
                                </div>
                            </div>

                            <div class="process_text">
                                <div class="process_text-in">
                                    <div class="process_name">
                                        Защита проекта
                                    </div>
                                    <div class="process_descr">
                                        Вы&nbsp;будете защищать свой проект перед экспертами отрасли и&nbsp;широкой аудиторией. Их&nbsp;голоса дадут вам шанс на&nbsp;победу в&nbsp;<b>&laquo;Битве технологий&raquo;</b>.
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="process_i wow slideLeft" data-wow-offset="150">
                            <div class="process_data">
                                <div class="process_count">
                                    4-й <span class="process_count-bl">шаг</span>
                                </div>
                                <div class="process_ico">
                                    <div class="process_back-rect wow rectBackOffsetX" data-wow-offset="150" data-wow-delay="1000ms"></div>
                                    <span class="s-icons-prize"></span>
                                </div>
                            </div>

                            <div class="process_text">
                                <div class="process_text-in">
                                    <div class="process_name">
                                        Краудфандинг
                                    </div>
                                    <div class="process_descr">
                                        Вы&nbsp;получите готовый к&nbsp;запуску <nobr>крауд-проект</nobr> и&nbsp;навыки его продвижения. Победители, отобранные жюри, получат дополнительные бонусы для своей кампании.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
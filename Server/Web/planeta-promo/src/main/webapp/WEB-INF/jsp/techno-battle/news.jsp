<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>

<%@ include file="/WEB-INF/jsp/techno-battle/includes/head.jsp" %>

<body>
<p:script src="promo.js"></p:script>
<p:script src="technobattle-news.js"></p:script>
<p:script src="technobattle-contest.js"></p:script>

<script id="promo-news-item-template" type="text/x-jquery-template">
    <span class="news-list_link">
        <span class="news-list_cover">
            <img src="{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.PROMO_NEWS, ImageType.PHOTO, 380, 250, true, false)}}">
        </span>

        <span class="news-list_cont">
            <span class="news-list_name">
                {{= title}}
            </span>

            <span class="news-list_date">
                {{= DateUtils.formatDateSimple(timeAdded)}}
            </span>

            <span class="news-list_text">
                {{html headingText}}
            </span>
        </span>
    </span>
</script>

<script id="promo-news-pager-template" type="text/x-jquery-template">
    <div class="news-list-more">
        <span class="tech-btn tech-btn-primary btn-block">Показать еще новости</span>
    </div>
</script>

<script>
    $(document).ready(function() {
        Backbone.history.start({pushState: true, silent: true});

        var promoNews = new Promo.Models.NewsPostsList({
            limit: 6
        });

        promoNews.load().done(function () {
            var viewPromoNews = new Promo.Views.News({
                collection: promoNews,
                el: '.js-promo-news-list'
            });
            viewPromoNews.render();
        });

        <c:if test="${not empty post}">
        $("#tech-news-post-popup").css("display", "");
        var postModel = new News.Models.Post(${hf:toJson(post)});
        postModel.set({authorProfileId: ${profileId}});

        var view = new Tech.Views.ModalPost({
            el: '#tech-news-post-popup',
            model: postModel
        });
        view.render();
        </c:if>
    });
</script>

<div id="tech-news-post-popup" class="modal modal-feed-post" style="display: none;"></div>

<%@ include file="/WEB-INF/jsp/techno-battle/includes/sidebar.jsp" %>
<div class="wrapper">
    <div class="main">
        <%@ include file="/WEB-INF/jsp/techno-battle/includes/topbar.jsp" %>
        <div class="container">
            <div class="wrap">

                <div class="news-list-rich">
                    <div class="news-list-carousel">
                        <c:forEach var="newsItem" items="${topNews}">
                        <div class="news-list-carousel_i">
                            <a href="https://${properties["application.host"]}/${newsItem.profileId}/news/${newsItem.postId}" class="news-list-carousel_link" style="background-image:url(${hf:getProxyThumbnailUrl(newsItem.imageUrl, "ORIGINAL", "PHOTO", 960, 420, true, false)});">
                                <span class="news-list-carousel_cont">
                                    <span class="news-list-carousel_name">
                                        ${newsItem.title}
                                    </span>
                                    <span class="news-list-carousel_date">
                                        ${hf:dateFormatByTemplate(newsItem.timeAdded, 'dd MMMM yyyy в HH:mm')}
                                    </span>
                                </span>
                            </a>
                        </div>
                        </c:forEach>
                    </div>

                    <div class="news-list-titles">
                        <c:forEach var="newsItem" items="${topNews}">
                        <div class="news-list-titles_i">
                            <div class="news-list-titles_link">
                                ${newsItem.title}
                            </div>
                        </div>
                        </c:forEach>
                    </div>
                </div>
                <script>
                    $(function () {
                        var newsItems = $('.news-list-carousel_i');
                        var titleItems = $('.news-list-titles_i');

                        newsItems.each(function (i, k) {
                            $(k).attr('data-id', i);
                        });

                        titleItems.each(function (i, k) {
                            $(k).attr('data-id', i);
                        });
                        titleItems.eq(0).addClass('active');


                        var news = $('.news-list-carousel').addClass('owl-carousel').owlCarousel({
                            autoplay: false,
                            loop: false,
                            items: 1,
                            slideBy: 1,
                            nav: true,
                            dots: false,
                            margin: 20,
                            mouseDrag: false,
                            smartSpeed: 600
                        });



                        titleItems.on('click', function () {
                            var item = $(this);
                            var id = item.data('id');

                            if ( item.hasClass('active') ) return;

                            news.trigger('to.owl.carousel', [id, 400]);
                        });


                        news.on('changed.owl.carousel', function() {
                            setTimeout(function () {
                                var item = $('.owl-item.active:eq(0) .news-list-carousel_i', news);
                                var id = item.data('id');

                                $('.news-list-titles_i.active').removeClass('active');
                                $('.news-list-titles_i').eq(id).addClass('active');
                            });
                        });
                    });
                </script>


                <div class="js-promo-news-list news-list">
                </div>


            </div>
        </div>
    </div>
    <%@ include file="/WEB-INF/jsp/techno-battle/includes/footer.jsp" %>
</div>

</body>
</html>
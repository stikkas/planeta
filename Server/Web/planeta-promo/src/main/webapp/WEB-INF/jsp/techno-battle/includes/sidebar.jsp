<c:if test="${!isAuthorized}">
    <%@ include file="/WEB-INF/jsp/includes/generated/header-info-auth-forms.jsp" %>
</c:if>
<div class="sidebar">

    <div class="header">
        <a href="/techbattle/" class="logo"></a>
            <span class="nav-toggle">
                <span class="nav-toggle_ico"></span>
            </span>
    </div>

    <div class="sidebar_cont">
        <div class="sidebar_cont-in">

            <%@ include file="/WEB-INF/jsp/techno-battle/includes/navbar.jsp" %>

            <div class="sidebar-rules">
                <a href="http://files.planeta.ru/technobattle/rules.pdf">Правила проведения конкурса</a>
            </div>

            <a href="https://vk.com/techbattle" target="_blank" rel="nofollow noopener" class="vk-news-link">
                <img src="//${hf:getStaticBaseUrl("")}/images/tech/vk-news.png" width="260" height="54">
            </a>
        </div>
    </div>
</div>

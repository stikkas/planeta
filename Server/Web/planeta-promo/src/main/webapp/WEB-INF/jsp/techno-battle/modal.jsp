<div class="modal-dialog hide" id="modal-tech-form">
    <div class="modal modal-tech-request modal-tech-form">
        <span class="modal-close" data-dismiss="modal">
            <span class="s-icon s-icon-close"></span>
        </span>

        <div class="tech-request">
            <form id="registartion-form" method="post">
                <div class="tech-request_head">
                    Заявка на участие
                </div>

                <div class="tech-request_group-list">
                    <div class="tech-request_group">
                        <div class="tech-request_fieldset">
                            <div class="tech-request_group-head">
                                Личные данные участника
                            </div>
                        </div>

                        <div class="wrap-row">
                            <div class="tech-request_fieldset js-float-label col-12">
                                <label class="float-label" for="name">Фамилия, имя и отчество</label>
                                <input class="form-control" type="text" id="name" name="name">
                            </div>

                            <div class="tech-request_fieldset js-float-label col-6">
                                <label class="float-label" for="phone">Мобильный телефон</label>
                                <input class="form-control" type="tel" id="phone" name="phone">
                            </div>

                            <div class="tech-request_fieldset js-float-label col-6">
                                <label class="float-label" for="email">Электронная почта</label>
                                <input class="form-control" type="text" id="email" name="email">
                            </div>

                            <div class="tech-request_fieldset js-float-label col-12">
                                <label class="float-label" for="city">Ваш город</label>
                                <input class="form-control" type="text" id="city" name="city">
                            </div>
                        </div>
                    </div>

                    <div class="tech-request_group">
                        <div class="tech-request_fieldset">
                            <div class="tech-request_group-head">
                                Данные о проекте
                            </div>
                        </div>

                        <div class="wrap-row">
                            <div class="tech-request_fieldset tech-request_fieldset__wt js-float-label col-12">
                                <div class="tech-request_fieldset-text">
                                    <p>Описание должно быть составлено по формату:</p>

                                    <ul>
                                        <li>Расскажите о&nbsp;разработанном вами продукте и&nbsp;его назначении</li>
                                        <li>В&nbsp;чем заключается инновационность или конкурентное преимущество разработанного вами продукта?</li>
                                        <li>На&nbsp;каком этапе находится разработка рабочего прототипа продукта?</li>
                                    </ul>
                                </div>
                                <div class="tech-request_fieldset-val">
                                    <label class="float-label" for="description">Кратко о проекте</label>
                                    <textarea rows="15" class="form-control" id="description" name="description"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tech-request_action">
                    <div class="tech-request_action-term">
                        <div class="form-ui form-ui-tech-modal">
                            <input class="form-ui-control" id="confirmed" type="checkbox" name="confirmed" checked>
                            <label class="form-ui-label" for="confirmed">
                                <span class="form-ui-txt js-checkbox">
                                    Отправляя заявку, вы подтверждаете своё согласие с условиями участия в спецпроекте <b>«Битва технологий»</b>. Условия участия изложены в <a href="http://files.planeta.ru/technobattle/rules.pdf">правилах</a>.
                                </span>
                            </label>
                        </div>
                    </div>

                    <div class="tech-request_action-btn">
                        <button class="tech-btn tech-btn-primary js-send-registration" type="button">Отправить</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal-dialog hide" id="modal-tech-form-success">
    <div class="modal modal-tech-request modal-tech-form-success">
        <span class="modal-close" data-dismiss="modal">
            <span class="s-icon s-icon-close"></span>
        </span>

        <div class="tech-request-success">
            <div class="tech-request-success_head">
                УРА!
            </div>
            <div class="tech-request-success_text">
                Спасибо, ваша заявка принята! Мы свяжемся с вами после подведения итогов отборочного тура.
            </div>
        </div>
    </div>
</div>
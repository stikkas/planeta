<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" %>

<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/planeta-js-init.jsp" %>
<p:script src="campus.js" />

<script type="text/javascript">
    (function() {
    var c = workspaceInitParameters.configuration;
    var staticNodesService = new StaticNodesService(c.staticNode, c.staticNodes, c.resourcesHost,
        c.jsBaseUrl, c.flushCache, c.compress);
    TemplateManager.init(workspaceInitParameters.currentLanguage, staticNodesService);

    $(document).ready(function () {
        workspace = Planeta.init({
            myProfile: workspaceInitParameters.myProfile,
            appModel: {
                constructor: AppModel,
                profileConstructor: BaseProfileModel,
                profile: workspaceInitParameters.profile
            },
            configuration: workspaceInitParameters.configuration,
            staticNodesService: staticNodesService,
            routing: {
                router: SimpleRouter
            },
            currentLanguage: workspaceInitParameters.currentLanguage
        });

        LazyHeader.init({
            auth: headerInitParameters.auth
        });

        if (window.location.hostname.indexOf('localhost') >= 0) {
            var $links = $('a');
            for (var i = 0; i < $links.length; i++) {
                if ($links[i].href.indexOf('https') >= 0) {
                    $links[i].href = $links[i].href.replace(/https/g, 'http');
                }
            }
        }

        new Promo.Views.CampaignListContainer({
            el: '.campus-projects',
            template: '#campus-project-container-template',
            model: new Promo.Models.Filter({
                activeFilter: 0,
                category: 'UNIVERSITY',
                limit: 50,
                filters: [],
                carousel: {
                    itemsInRow: 4
                },
                isEmpty: true
            })
        }).render();


        $('[id^="campus_"].campus-members-list_i').click(function (e) {
            var id = $(e.currentTarget).prop('id').split('_').pop();
            toggleCampus(id);
            toggleTops(id);
        });
        function toggleCampus(tagId) {
            $('[id^="campus_"].campus-members-list_i').each(function () {
                var self = $(this);
                if (self.prop('id').split('_').pop() === tagId) {
                    self.addClass('active');
                } else {
                    self.removeClass('active');
                }
            });
        }
        function toggleTops(tagId) {
            topCampusCollection.url = '/campus/top/' + tagId;
            topCampusCollection.load().success(function () {
                if (!topCampusView) {
                    topCampusView = new TopCampusView();
                    topCampusView.render();
                }
            });
        }
        //---------------- Views and Models -----------------------------------------
        var topCampusCollection = new BaseCollection();
        var TopCampusView = BaseListView.extend({
            itemViewType: Welcome.Views.TopCampaign,
            el: '.js-top-campus-campaigns',
            collection: topCampusCollection
        }); 
        var topCampusView;
        //-------------------------------------------------------------------------
        var firstCampus = $('[id^="campus_"].campus-members-list_i').first().addClass('active');
        toggleTops(firstCampus.prop('id').split('_').pop());

    });
    })();
</script>

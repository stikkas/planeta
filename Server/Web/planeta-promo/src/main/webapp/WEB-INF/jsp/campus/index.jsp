<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>

<head>
    <%@ include file="/WEB-INF/jsp/campus/common-css.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp" %>
    <%@ include file="/WEB-INF/jsp/campus/index-js.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/generated/screen-width.jsp" %>

    <%@include file="/WEB-INF/jsp/includes/generated/common-favicon.jsp" %>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@ include file="/WEB-INF/jsp/includes/generated/main-page-meta-header.jsp" %>
    <%@ include file="/WEB-INF/jsp/campus/metas-welcome.jsp" %>
</head>
<body class="campus-page grid-1200">
    <%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>

    <div id="global-container">
        <div class="wrap-container" id="main-container">
            <div id="center-container">
                <div class="campus-header">
                    <span class="campus-logo"></span>
                </div>
                <%@ include file="/WEB-INF/jsp/campus/nav.jsp" %>
                <%@ include file="/WEB-INF/jsp/campus/lead.jsp" %>
                <%@ include file="/WEB-INF/jsp/campus/idea.jsp" %>
                <div class="campus-members" id="campus-members">
                    <div class="wrap">
                        <div class="col-6">
                            <div class="campus-members_cont">
                                <div class="campus-members_head">Участники</div>
                                <div class="campus-members-list">
                                    <c:forEach items="${campuses}" var="campus">
                                        <div class="campus-members-list_i" id="campus_${campus.tagId}">
                                            <div class="campus-members-list_cover" style="background-image:url(${campus.imageUrl});"></div>
                                                <a class="campus-members-list_all" href="/campus/category/${campus.id}">
                                                <c:choose>
                                                    <c:when test="${campus.count gt 0}">Показать все ${campus.count} <spring:message code="decl.projects" arguments="${hf:plurals(campus.count)}"/></c:when>
                                                    <c:otherwise>Перейти на страницу кампуса</c:otherwise>
                                                </c:choose>
                                                </a>
                                            <div class="campus-members-list_name">${campus.name}</div>
                                        </div>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="campus-members_projects campus-projects-list">
                                <div class="project-card-list js-top-campus-campaigns"> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="campus-projects" id="campus-projects"> </div>
                <%@ include file="/WEB-INF/jsp/campus/faq.jsp" %>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $('.campus-nav_list').singlePageNav({
                currentClass: 'active',
                updateHash: true
            });
        });
    </script>

    <%@ include file="/WEB-INF/jsp/includes/generated/irma/footer.jsp" %>
</body>
</html>

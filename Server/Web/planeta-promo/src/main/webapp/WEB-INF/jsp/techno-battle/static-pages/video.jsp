<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>

<%@ include file="/WEB-INF/jsp/techno-battle/includes/head.jsp" %>

<body>
<p:script src="technobattle-news.js"></p:script>

<script>
    $(document).ready(function() {
        $('.js-view-video').click(function (e) {
            e.preventDefault();
            var $el = $(e.currentTarget);
            var iframeUrl = $el.data('video');
            if (iframeUrl) {
                var videoModel = new BaseModel({iframeurl: iframeUrl});
                Modal.showDialogByViewClass(Technobattle.Views.VideoDialog, videoModel, null, null, null, null, true);
            }
            if ($el.attr('id')) {
                window.history.pushState(null, null , window.location.origin + window.location.pathname + "?videoId=" + $el.attr('id'));
            } else {
                window.history.pushState(null, null , window.location.origin + window.location.pathname);
            }
        });

        var items = $('.news-tabs_i');

        items.on('click', function(){
            var item = $(this);
            if ( item.hasClass('active') ) return;

            var prevActive = items.filter('.active').removeClass('active');
            var prevActiveHref = prevActive.data('href');
            $(prevActiveHref).removeClass('active');

            item.addClass('active');
            var itemHref = item.data('href');
            $(itemHref).addClass('active');
        });

        <c:if test="${videoId != null}">
        $('#${videoId}').click();
        </c:if>
    });


</script>

<%@ include file="/WEB-INF/jsp/techno-battle/includes/sidebar.jsp" %>
<div class="wrapper">
    <div class="main">
        <%@ include file="/WEB-INF/jsp/techno-battle/includes/topbar.jsp" %>
        <div class="container">



            <div class="video-page">
                <div class="videos active" id="videos">
                    <div class="news-head">
                        Видеоблог
                    </div>

                    <c:if test="${not empty topVideo}">
                    <div class="news news__l">
                        <div <c:if test='${topVideo.videoId != null}'> id="${topVideo.videoId}" </c:if> class="news_i js-view-video" data-toggle="modal1" data-target="#modal-video1" data-video="${topVideo.iframeurl}">
                            <div class="news_cover"
                                 style="background-image:url(${hf:getThumbnailUrl(topVideo.image, "ORIGINAL", "VIDEO")});">
                                <div class="video-play"><span class="video-play_ico"></span></div>
                            </div>
                            <div class="news_cont">
                                <div class="news_name">
                                    ${topVideo.title}
                                </div>
                                <div class="news_date">
                                    ${topVideo.date}
                                </div>
                            </div>
                        </div>
                    </div>
                    </c:if>

                    <div class="news-post-text">
                        Новые видеоотчёты о&nbsp;том, что произошло на&nbsp;<nobr class="news-post-text_bt">&laquo;Битве технологий&raquo;</nobr>,&nbsp;&mdash; каждый четверг!
                        <br>
                        <span class="news-post-text_hl">Следите за&nbsp;новостями!</span>
                    </div>



                    <c:if test="${not empty videos}">
                    <div class="news-wrapper">
                        <div class="news news__list">
                        <c:forEach var="video" items="${videos}">
                            <div <c:if test='${video.videoId != null}'> id="${video.videoId}" </c:if> class="news_i js-view-video" data-video="${video.iframeurl}">
                                <div class="news_cover"
                                     style="background-image:url(${hf:getThumbnailUrl(video.image,"USER_AVATAR","VIDEO")});">
                                    <div class="video-play"><span class="video-play_ico"></span></div>
                                </div>
                                <div class="news_cont">
                                    <div class="news_name">
                                            ${video.title}
                                    </div>
                                    <div class="news_date">
                                            ${video.date}
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                        </div>
                    </div>
                    </c:if>



                </div>
            </div>


        </div>
    </div>
    <%@ include file="/WEB-INF/jsp/techno-battle/includes/footer.jsp" %>
</div>

</body>
</html>
<!-- Sharing meta data: start -->
<meta property="og:site_name" content="promo.planeta.ru/techbattle"/>

<c:choose>
    <c:when test="${not empty customMetaTag.image}">
        <meta property="og:image" content="${hf:getThumbnailUrl(customMetaTag.image, "ORIGINAL", "VIDEO")}" />
    </c:when>
    <c:otherwise>
        <c:choose>
            <c:when test="${not empty project.sharingImageUrl}">
                <meta property="og:image" content="${project.sharingImageUrl}" />
            </c:when>
            <c:otherwise>
                <meta property="og:image" content="https://s3.planeta.ru/i/172e30/1485509863671_renamed.jpg" />
            </c:otherwise>
        </c:choose>
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${not empty customMetaTag.ogTitle}">
        <meta property="og:title" content="${customMetaTag.ogTitle}" />
    </c:when>
    <c:otherwise>
        <meta property="og:title" content="${project.sharingTitle}" />
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${not empty customMetaTag.ogDescription}">
        <meta property="og:description" content="${customMetaTag.ogDescription}" />
    </c:when>
    <c:otherwise>
        <meta property="og:description" content="«Битва технологий» - первое реалити-шоу Рунета, посвященное создателям нового перспективного hardware. Отдай свой голос и помоги участникам воплотить их разработки в жизнь!" />
    </c:otherwise>
</c:choose>
<!-- Sharing meta data: end -->
<c:choose>
    <c:when test="${not empty customMetaTag.title}">
        <title>${customMetaTag.title}</title>
    </c:when>
    <c:otherwise>
        <title>${project.title}</title>
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${not empty customMetaTag.description}">
        <meta name="description" content="${customMetaTag.description}"/>
    </c:when>
    <c:otherwise>
        <meta name="description" content="«Битва технологий» - первое реалити-шоу Рунета, посвященное создателям нового перспективного hardware. Отдай свой голос и помоги участникам воплотить их разработки в жизнь!"/>
    </c:otherwise>
</c:choose>

<c:if test="${not empty customMetaTag.keywords}">
    <meta name="keywords" content="${customMetaTag.keywords}"/>
</c:if>
<meta name="viewport" content="width=device-width">

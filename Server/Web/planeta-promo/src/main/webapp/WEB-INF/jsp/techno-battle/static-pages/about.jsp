<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>

<%@ include file="/WEB-INF/jsp/techno-battle/includes/head.jsp" %>

<body>
<%@ include file="/WEB-INF/jsp/techno-battle/includes/sidebar.jsp" %>
<div class="wrapper">
    <div class="main">
        <%@ include file="/WEB-INF/jsp/techno-battle/includes/topbar.jsp" %>

    <div class="container">
        <div class="about-page">
            <div class="about-lead">
                <div class="about-lead_head">
                    &laquo;Битва технологий&raquo;&nbsp;&mdash; первое технологическое <nobr>реалити-шоу</nobr> Рунета для <span class="about-lead_head-create">создателей</span> <nobr class="about-lead_head-hardware">hardware-стартапов</nobr>.
                </div>

                <div class="about-lead_text">
                    В&nbsp;рамках спецпроекта, организованного платформой <a href="https://planeta.ru">Planeta.ru</a>, <a href="http://eva.fund/">EVA Invest</a> и&nbsp;<a href="http://www.rvc.ru/">РВК</a>, разработчики технологических новинок обучаются искусству создания эффективных краудфандинговых кампаний, общаются со&nbsp;своей аудиторией и&nbsp;находят средства для развития собственных проектов.
                    <br>
                    <br>
                    <span class="about-lead_text-hl">Основными задачами &laquo;Битвы технологий&raquo;</span> являются привлечение широкого внимания к&nbsp;отечественным технологиям, поддержка российских техностартапов и&nbsp;популяризация краудфандинга.
                </div>
            </div>




            <div class="about-qualifying">
                <div class="about-qualifying_wrap">
                    <div class="about-qualifying_cont">
                        <div class="about-round-cmn">
                            <div class="about-round-cmn_name">
                                Отборочный этап
                            </div>

                            <div class="about-round-cmn_start">
                                Ноябрь –
                            </div>
                            <div class="about-round-cmn_end">
                                декабрь 2016 года
                            </div>
                        </div>


                        <div class="about-qualifying_descr">
                            Краудфандинговая платформа <a href="https://planeta.ru">Planeta.ru</a>, <a href="http://eva.fund/">EVA Invest</a> и&nbsp;<a href="http://www.rvc.ru/">РВК</a> начали поиск участников <nobr>реалити-шоу</nobr> &laquo;Битва технологий&raquo;.
                            <br>
                            <br>
                            Отбор проводился среди проектных команд, работающих на&nbsp;базе творческих центров FabLab, ЦМИТ и&nbsp;других площадок, а&nbsp;также среди независимых разработчиков устройств.
                        </div>
                    </div>


                    <div class="about-qualifying_img">
                        <div class="about-qualifying_img-block">
                            <div class="about-qualifying_img-wrap">
                                <div class="about-qualifying_img-cont">
                                    <div class="about-qualifying_img-top">
                                        <img src="//${hf:getStaticBaseUrl("")}/images/tech/about-qualifying-1.jpg">
                                    </div>
                                    <div class="about-qualifying_img-mdl-1">
                                        <img src="//${hf:getStaticBaseUrl("")}/images/tech/about-qualifying-2.jpg">
                                    </div>
                                    <div class="about-qualifying_img-mdl-2">
                                        <img src="//${hf:getStaticBaseUrl("")}/images/tech/about-qualifying-3.jpg">
                                    </div>
                                    <div class="about-qualifying_img-btm">
                                        <img src="//${hf:getStaticBaseUrl("")}/images/tech/about-qualifying-4.jpg">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            <div class="about-presentation">
                <div class="about-presentation_block">
                    <div class="about-presentation_cont">
                        <div class="about-round-cmn">
                            <div class="about-round-cmn_start">
                                21 декабря
                            </div>
                            <div class="about-round-cmn_end">
                                2016 года
                            </div>
                        </div>

                        <div class="about-presentation_descr">
                            Претенденты собрались в&nbsp;креативном коворкинге Deworkacy, чтобы представить организаторам <nobr>реалити-шоу</nobr> свои технические проекты. По&nbsp;итогам этой презентации для участия в&nbsp;&laquo;Битве технологий&raquo; было отобрано 10 команд.
                        </div>
                    </div>
                </div>
            </div>



            <div class="about-education">
                <div class="about-education_wrap">
                    <div class="about-education_cont">
                        <div class="about-round-cmn">
                            <div class="about-round-cmn_name">
                                Образовательный этап
                            </div>

                            <div class="about-round-cmn_start">
                                1 февраля –
                            </div>
                            <div class="about-round-cmn_end">
                                1 марта 2017 года
                            </div>
                        </div>
                    </div>

                    <div class="about-education_descr">
                        В&nbsp;течение всего февраля 2017&nbsp;г. участники &laquo;Битвы технологий&raquo; под руководством экспертов &laquo;<a href="https://school.planeta.ru">Школы краудфандинга Planeta.ru</a>&raquo;, профессиональных режиссеров, редакторов и&nbsp;дизайнеров обучаются тонкостям создания эффективных краудфандинговых кампаний.
                        <br>
                        <br>
                        В&nbsp;ходе образовательного курса разработчики тщательно прорабатывают элементы своих будущих <nobr>крауд-проектов</nobr>: создают информативное описание, записывают видеообращение, продумывают интересные вознаграждения, формируют бюджет.
                        <br>
                        <br>
                        По&nbsp;итогам этой работы эксперты &laquo;Битвы технологий&raquo; и&nbsp;зрительское голосование определяют участников следующего этапа <nobr>реалити-шоу</nobr>.
                    </div>
                </div>
            </div>




            <div class="about-finish-block">
                <div class="about-finish-block_i">

                    <div class="about-finish">
                        <div class="about-finish_info"
                             style="background-image:url(//${hf:getStaticBaseUrl("")}/images/tech/about-crowd-bg.jpg);">
                            <div class="about-round-cmn">
                                <div class="about-round-cmn_name">
                                    Стадия краудфандинга
                                </div>

                                <div class="about-round-cmn_start">
                                    Март –
                                </div>
                                <div class="about-round-cmn_end">
                                    апрель 2017 года
                                </div>
                            </div>
                        </div>

                        <div class="about-finish_descr">
                            В&nbsp;марте 2017 года команды запускают созданные в&nbsp;ходе обучения краудфандинговые кампании.
                            <br>
                            <br>
                            Пятерка участников, отобранных экспертами и&nbsp;зрителями <nobr>реалити-шоу</nobr>, получает стартовый взнос в&nbsp;свои проекты и&nbsp;поддержку в&nbsp;продвижении со&nbsp;стороны <nobr>селебрити-блогеров</nobr> и&nbsp;<nobr>менторов-экспертов</nobr>.
                            <br>
                            <br>
                            На&nbsp;этом этапе команды прилагают все усилия, чтобы привлечь поддержку своих разработок со&nbsp;стороны пользователей.
                        </div>
                    </div>

                </div>



                <div class="about-finish-block_i">

                    <div class="about-finish">
                        <div class="about-finish_info"
                             style="background-image:url(//${hf:getStaticBaseUrl("")}/images/tech/about-finish-bg.jpg);">
                            <div class="about-round-cmn">
                                <div class="about-round-cmn_name">
                                    Подведение итогов
                                </div>

                                <div class="about-round-cmn_start">
                                    Апрель
                                </div>
                                <div class="about-round-cmn_end">
                                    2017 года
                                </div>
                            </div>
                        </div>

                        <div class="about-finish_descr">
                            Создатели стартапов, сумевшие в&nbsp;полном объеме привлечь финансирование для своих краудфандинговых кампаний, получают средства на&nbsp;развитие своих разработок.
                            <br>
                            <br>
                            Кроме того, на&nbsp;закрытую встречу с&nbsp;инвесторами приглашаются три победителя &laquo;Битвы технологий&raquo;: участник, собравший больше всего средств, команда, в&nbsp;чьем проекте был самый большой пересбор по&nbsp;отношению к&nbsp;финансовой цели, и&nbsp;разработчики, чью кампанию поддержало больше всего спонсоров.
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>
    <%@ include file="/WEB-INF/jsp/techno-battle/includes/footer.jsp" %>

</div>

</body>
</html>
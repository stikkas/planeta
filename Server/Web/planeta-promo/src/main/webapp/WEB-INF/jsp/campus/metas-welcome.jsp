<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!-- Sharing meta data: start -->
<meta property="og:site_name" content="promo.planeta.ru/campus"/>

<meta property="og:image" content="https://${hf:getStaticBaseUrl("")}/images/campus/imgpsh_fullsize.jpg"/>
<meta property="og:title" content="«Онлайн-Кампус» | Planeta.ru" />
<meta property="og:description" content="интернет-площадка, где студенты, выпускники и сотрудники университетов объединяются, чтобы воплотить в жизнь проекты, связанные с их учебными заведениями" />
<!-- Sharing meta data: end -->
<title>Онлайн-Кампус</title>
<meta name="description" content="интернет-площадка, где студенты, выпускники и сотрудники университетов объединяются, чтобы воплотить в жизнь проекты, связанные с их учебными заведениями"/>
<c:if test="${not empty customMetaTag.keywords}">
    <meta name="keywords" content="${customMetaTag.keywords}"/>
</c:if>
<meta name="viewport" content="width=device-width">


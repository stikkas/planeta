<div class="program" id="program">
    <div class="wrap wow slideUp" data-wow-offset="250">
        <div class="col-12">
            <div class="program_head">
                Программа курса
            </div>

            <div class="program_list">
                <div class="program_i wow zoomInUp" data-wow-offset="250">
                    <div class="program_date">
                        <div class="program_num">
                            <div class="program_num-val">1</div>
                        </div>
                        <div class="program_day">февраля<br>среда</div>
                    </div>

                    <div class="program_cont">
                        <div class="program_text">
                            <div class="program_name">Старт &laquo;Битвы технологий&raquo;</div>
                            <div class="program_descr">Лекция &laquo;Краудфандинг как эффективный инструмент финансирования и&nbsp;продвижения технологических проектов&raquo;.</div>
                        </div>
                    </div>
                </div>

                <div class="program_i wow zoomInUp" data-wow-offset="250">
                    <div class="program_date">
                        <div class="program_num">
                            <div class="program_num-val">13</div>
                        </div>
                        <div class="program_day">февраля<br>понедельник</div>
                    </div>

                    <div class="program_cont">
                        <div class="program_text">
                            <div class="program_name">Лекция &laquo;Бонусы и&nbsp;вознаграждения. Чего хотят спонсоры&raquo;</div>
                            <div class="program_descr">Публичная защита бюджетирования проекта.</div>
                        </div>
                    </div>
                </div>

                <div class="program_i wow zoomInUp" data-wow-offset="250">
                    <div class="program_date">
                        <div class="program_num">
                            <div class="program_num-val">15</div>
                        </div>
                        <div class="program_day">февраля<br>среда</div>
                    </div>

                    <div class="program_cont">
                        <div class="program_text">
                            <div class="program_name">Лекция &laquo;Продвижение и&nbsp;PR&nbsp;<nobr>крауд-проекта</nobr>. Когда и&nbsp;с&nbsp;чего начинать&raquo;</div>
                            <div class="program_descr">Публичная защита списка вознаграждений.</div>
                        </div>
                    </div>
                </div>

                <div class="program_i wow zoomInUp" data-wow-offset="250">
                    <div class="program_date">
                        <div class="program_num">
                            <div class="program_num-val">17</div>
                        </div>
                        <div class="program_day">февраля<br>пятница</div>
                    </div>

                    <div class="program_cont">
                        <div class="program_text">
                            <div class="program_name">Лекция &laquo;Технотренды будущего&raquo;</div>
                        </div>
                    </div>
                </div>

                <div class="program_i wow zoomInUp" data-wow-offset="250">
                    <div class="program_date">
                        <div class="program_num">
                            <div class="program_num-val">20</div>
                        </div>
                        <div class="program_day">февраля<br>понедельник</div>
                    </div>

                    <div class="program_cont">
                        <div class="program_text">
                            <div class="program_name">Лекция &laquo;Как сформулировать предложение инвестору&raquo;</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="send-program wow slideUp200" data-wow-offset="150">
                <div class="send-program_lbl">
                    Получить полную программу на e-mail
                </div>
                <form class="send-program_val" action="#">
                    <input type="text" class="form-control" name="email" id="letterEmail">
                    <button class="tech-btn tech-btn-primary js-send-letter">Отправить</button>
                </form>
            </div>
        </div>
    </div>
</div>
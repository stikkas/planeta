<div class="footer wow fadeIn" data-wow-delay="1300ms">
    <div class="wrap">
        <div class="col-12">
            <div class="footer_info">
                <a href="mailto:school@planeta.ru">SCHOOL@PLANETA.RU</a>
                <br>
                +7 (495) 181-05-05
            </div>

            <div class="footer_develop">
                <div class="footer_develop-head">
                    Разработано в
                </div>
                <div class="footer_develop-logo">
                    <a href="https://planeta.ru/" class="pln-logo"></a>
                </div>
            </div>

            <div class="footer_sharing">
                <div class="footer_sharing-head">
                    Поделиться:
                </div>
                <div class="footer_sharing-cont"></div>
            </div>
        </div>
    </div>
</div>
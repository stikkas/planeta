<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>

<%@ include file="/WEB-INF/jsp/techno-battle/includes/head.jsp" %>


<body>

<p:script src="promo.js"></p:script>

<script>
    $(document).ready(function() {
        console.log(workspace.appModel.get('myProfile').get('isAuthor'));
        Banner.init('${mainAppUrl}', workspace.appModel.get('myProfile').get('isAuthor'));

        var topCampaigns = new Promo.Models.CampaignList({
            data: {
                categories: "TECHBATTLE"
            },
            limit: 10
        });

        topCampaigns.load().done(function () {
            var viewTopCampaigns = new Welcome.Views.TopCampaigns({
                collection: topCampaigns,
                el: '.js-project-list'
            });
            viewTopCampaigns.render();
        });
    });
</script>

<%@ include file="/WEB-INF/jsp/techno-battle/includes/sidebar.jsp" %>
<div class="wrapper">
    <div class="main">
        <%@ include file="/WEB-INF/jsp/techno-battle/includes/topbar.jsp" %>
        <div class="container">
            <div class="js-banner-container-top"></div>

            <div class="pln-projects">
                <div class="pln-projects_head">
                    Крауд-кампании участников «Битвы технологий»
                </div>
                <div class="js-project-list project-card-list"></div>
            </div>

            <div class="tech-projects">

                <div class="tech-projects_head">
                    Участиники «Битвы технологий»
                </div>

                <svg xmlns="http://www.w3.org/2000/svg" style="display: none">
                    <defs>
                        <g id="like">
                            <path d="M10.062.75c-1.052 0-1.895.385-2.638 1.144l-.001.002h-.001L7 2.439l-.423-.511-.001-.001C5.832 1.168 4.989.75 3.937.75c-1.052 0-2.04.418-2.784 1.177A4.032 4.032 0 0 0 0 4.768c0 1.072.409 2.081 1.152 2.84l5.226 5.378a.866.866 0 0 0 1.243 0l5.226-5.378A4.038 4.038 0 0 0 14 4.768c0-1.073-.41-2.082-1.154-2.841A3.872 3.872 0 0 0 10.062.75"></path>
                        </g>
                        <defs>
                        </defs>
                    </defs>
                </svg>

                <div class="tech-projects_list">
                    <c:forEach var="project" items="${projects}">
                        <div class="tech-projects_i">
                            <a href="/techbattle/project/${project.projectId}" class="tech-projects_link <c:if test="${project.status == \"POPULAR_VOTE_WINNER\"}"> tech-projects-winner</c:if>">
                                <c:if test="${project.status == \"EXPERT_COUNCIL_WINNER\" || project.status == \"POPULAR_VOTE_WINNER\"}">
                                <span class="tech-projects_winner">
                                    <span class="tech-projects_winner-ico"></span>
                                    <span class="tech-projects_winner-cont">
                                        <span class="tech-projects_winner-head">
                                            Победитель
                                        </span>
                                        <span class="tech-projects_winner-text">
                                            <c:if test="${project.status == \"EXPERT_COUNCIL_WINNER\"}">Выбор экспертного совета</c:if>
                                            <c:if test="${project.status == \"POPULAR_VOTE_WINNER\"}">Народного голосования</c:if>
                                        </span>
                                    </span>
                                </span>
                                </c:if>
                                <span class="tech-projects_cover">
                                    <img src="${hf:getThumbnailUrl(project.smallImageUrl, "PRODUCT_COVER_NEW", "PRODUCT")}">
                                </span>
                                <span class="tech-projects_cont">
                                    <span class="tech-projects_name">${project.title}</span>
                                    <span class="tech-projects_likes-block">
                                        <span class="tech-projects_likes">
                                            <span class="tech-projects_likes-ico">
                                                <svg viewBox="0 0 14 14">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#like"></use>
                                                </svg>
                                            </span>
                                            <span class="tech-projects_likes-val">
                                                ${project.votesCount}
                                            </span>
                                        </span>
                                    </span>
                                    <span class="tech-projects_more-cont">
                                        <span class="tech-projects_more">
                                            о проекте
                                        </span>
                                    </span>
                                </span>
                            </a>
                        </div>
                    </c:forEach>
                </div>
            </div>

        </div>
    </div>
    <%@ include file="/WEB-INF/jsp/techno-battle/includes/footer.jsp" %>
</div>

</body>
</html>
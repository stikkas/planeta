<div class="nav-wrap">
    <div class="nav">
        <div class="wrap">
            <div class="col-12">
                <div class="nav_cont">
                    <div class="nav_list">
                        <div class="nav_i" style="display: none;">
                            <a href="#organizer" class="nav_link">Организаторы</a>
                        </div>
                        <div class="nav_i">
                            <a href="#process" class="nav_link">Обучение</a>
                        </div>
                        <div class="nav_i">
                            <a href="#what" class="nav_link">Что дает БТ</a>
                        </div>
                        <div class="nav_i">
                            <a href="#prizes" class="nav_link">Призы победителям</a>
                        </div>
                        <div class="nav_i">
                            <a href="#program" class="nav_link">Программа курса</a>
                        </div>
                        <div class="nav_i">
                            <a href="#cost" class="nav_link">Оплата</a>
                        </div>
                        <div class="nav_i">
                            <a href="#faq" class="nav_link">FAQ</a>
                        </div>
                    </div>

                    <div class="nav_action">
                        <a href="https://vk.com/techbattle" class="tech-request-btn js-tech-request-2" target="_blank" rel="nofollow noopener">Подписаться</a>
                        <%--<button class="tech-request-btn js-tech-request-2" data-tech-request="js-tech-request-2" data-toggle="modal" data-target="#modal-tech-form">Подать заявку</button>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
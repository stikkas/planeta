<div class="prizes">
    <div class="wrap">
        <div class="col-12">
            <div class="prizes_block wow slideUp200" id="prizes">
                <div class="prizes_cont">
                    <div class="prizes_head">
                        Призы победителям
                    </div>

                    <div class="prizes_list">
                        <div class="prizes_i wow slideUp" data-wow-offset="200">
                            <div class="prizes_ico">
                                <div class="prizes_back-rect wow rectBackOffsetXDesc" data-wow-offset="200" data-wow-delay="1000ms"></div>
                                <span class="s-icons-oculus"></span>
                            </div>
                            <div class="prizes_name">
                                Амбассадор продукта
                            </div>
                            <div class="prizes_descr">
                                Ваш проект будет тестировать, курировать и&nbsp;освещать известный блогер
                            </div>
                        </div>

                        <div class="prizes_i wow slideUp" data-wow-offset="200">
                            <div class="prizes_ico">
                                <div class="prizes_back-rect wow rectBackOffsetXDesc" data-wow-offset="200" data-wow-delay="1000ms"></div>
                                <span class="s-icons-man"></span>
                            </div>
                            <div class="prizes_name">
                                Персональный трекер
                            </div>
                            <div class="prizes_descr">
                                Ваш проект будет курировать персональный консультант от&nbsp;Planeta.ru
                            </div>
                        </div>

                        <div class="prizes_i wow slideUp" data-wow-offset="200">
                            <div class="prizes_ico">
                                <div class="prizes_back-rect wow rectBackOffsetXDesc" data-wow-offset="200" data-wow-delay="1000ms"></div>
                                <span class="s-icons-horn"></span>
                            </div>
                            <div class="prizes_name">
                                Инфоподдержка
                            </div>
                            <div class="prizes_descr">
                                Публикации о&nbsp;вашем проекте на&nbsp;крупнейших техноресурсах страны
                            </div>
                        </div>

                        <div class="prizes_i wow slideUp" data-wow-offset="200">
                            <div class="prizes_ico">
                                <div class="prizes_back-rect wow rectBackOffsetXDesc" data-wow-offset="200" data-wow-delay="1000ms"></div>
                                <span class="s-icons-planeta"></span>
                            </div>
                            <div class="prizes_name">
                                PR-поддержка Planeta.ru
                            </div>
                            <div class="prizes_descr">
                                Публикации о&nbsp;вашем проекте по&nbsp;всем каналам Planeta.ru
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>

<%@ include file="/WEB-INF/jsp/techno-battle/includes/head.jsp" %>

<body>
<%@ include file="/WEB-INF/jsp/techno-battle/includes/sidebar.jsp" %>
<div class="wrapper">
    <div class="main">
    <%@ include file="/WEB-INF/jsp/techno-battle/includes/topbar.jsp" %>

    <div class="container">
        <div class="partners">

            <div class="partners-block">
                <div class="partners-head">
                    Генеральный партнер
                </div>

                <div class="partners-list partners-list__gen">
                    <div class="partners-list_i">
                        <div class="partners-list_i-cont">
                            <div class="partners-list_ava">
                                <a href="http://www.rvc.ru/" class="partners-list_link" target="_blank">
                                    <img class="partners-list_img" src="//${hf:getStaticBaseUrl("")}/images/tech/partners/rvc.png" alt="rvc" title="rvc">
                                </a>
                            </div>
                            <div class="partners-list_cont">
                                <div class="partners-list_text">
                                    Российская венчурная компания обеспечивает стартовые взносы в&nbsp;<nobr>крауд-проекты</nobr> пяти победителей &laquo;Битвы технологий&raquo;, а&nbsp;также утверждает специальную номинацию, в&nbsp;рамках которой двое участников получат возможность доработать свой продукт на&nbsp;базе фаблаба в&nbsp;Китае.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <c:if test="${not empty specialPartners}">
            <div class="partners-block">
                <div class="partners-head">
                    Специальные партнеры
                </div>


                <div class="partners-list partners-list__spec">
                    <c:forEach var="partner" items="${specialPartners}">
                    <div class="partners-list_i">
                    <div class="partners-list_i-cont">
                        <div class="partners-list_ava">
                            <a href="${partner.url}" class="partners-list_link" target="_blank">
                                <img class="partners-list_img" src="${partner.image}">
                            </a>
                        </div>
                        <div class="partners-list_cont">
                            <div class="partners-list_text">
                                ${partner.text}
                            </div>
                        </div>
                    </div>
                    </div>
                    </c:forEach>
                </div>
            </div>
            </c:if>


            <c:if test="${not empty infoPartners}">
            <div class="partners-block">
                <div class="partners-head">
                    Информационные партнеры
                </div>


                <div class="partners-list">
                    <c:forEach var="partner" items="${infoPartners}">
                    <div class="partners-list_i">
                        <a href="${partner.url}" class="partners-list_link" target="_blank">
                            <img class="partners-list_img" src="${partner.image}">
                        </a>
                    </div>
                    </c:forEach>
                </div>
            </div>
            </c:if>


        </div>



    </div>
    </div>
    <%@ include file="/WEB-INF/jsp/techno-battle/includes/footer.jsp" %>
</div>

</body>
</html>
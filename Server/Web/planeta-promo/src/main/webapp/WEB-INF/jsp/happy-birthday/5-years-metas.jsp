<!-- Sharing meta data: start -->
<meta property="og:site_name" content="promo.planeta.ru/chronicles"/>

<c:choose>
    <c:when test="${not empty customMetaTag.image}">
        <meta property="og:image" content="${hf:getThumbnailUrl(customMetaTag.image, "ORIGINAL", "VIDEO")}" />
    </c:when>
    <c:otherwise>
        <meta property="og:image" content="https://s3.planeta.ru/i/1a1110/1496679959206_renamed.jpg" />
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${not empty customMetaTag.ogTitle}">
        <meta property="og:title" content="${customMetaTag.ogTitle}" />
    </c:when>
    <c:otherwise>
        <meta property="og:title" content="День рождения крауд-платформы «Планета» | История краудфандинга в России"/>
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${not empty customMetaTag.ogDescription}">
        <meta property="og:description" content="${customMetaTag.ogDescription}" />
    </c:when>
    <c:otherwise>
        <meta property="og:description" content="7 июня не просто День российского краудфандинга – это день рождения платформы Planeta.ru. Вспомнить «как это было», кликайте по ссылке. Итак, мы взлетаем!"/>
    </c:otherwise>
</c:choose>
<!-- Sharing meta data: end -->
<c:choose>
    <c:when test="${not empty customMetaTag.title}">
        <title>${customMetaTag.title}</title>
    </c:when>
    <c:otherwise>
        <title>Хроники Planeta.ru | Наша история краудфандинга</title>
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${not empty customMetaTag.description}">
        <meta name="description" content="${customMetaTag.description}"/>
    </c:when>
    <c:otherwise>
        <meta name="description" content="Planeta.ru с самого начала была больше, чем просто сайт или удобный сервис — это новая философия, форма отношений между авторами проектов и их аудиторией."/>
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${not empty customMetaTag.keywords}">
        <meta name="keywords" content="${customMetaTag.keywords}"/>
    </c:when>
    <c:otherwise>
        <meta name="keywords" content="день российского краудфандинга, краудфандинг в россии, краудфандинг, история краудфандинга, день рождения планеты, планета ру"/>
    </c:otherwise>
</c:choose>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" %>

<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/planeta-js-init.jsp" %>
<p:script src="campus.js" />
<script type="text/javascript">
    (function() {
        var c = workspaceInitParameters.configuration;
        var staticNodesService = new StaticNodesService(c.staticNode, c.staticNodes, c.resourcesHost,
            c.jsBaseUrl, c.flushCache, c.compress);
        TemplateManager.init(workspaceInitParameters.currentLanguage, staticNodesService);
    $(document).ready(function () {
        workspace = Planeta.init({
            myProfile: workspaceInitParameters.myProfile,
            appModel: {
                constructor: AppModel,
                profileConstructor: BaseProfileModel,
                profile: workspaceInitParameters.profile
            },
            configuration: workspaceInitParameters.configuration,
            staticNodesService: staticNodesService,
            routing: {
                router: SimpleRouter
            },
            currentLanguage: workspaceInitParameters.currentLanguage
        });

        LazyHeader.init({
            auth: headerInitParameters.auth
        });

        if (window.location.hostname.indexOf('localhost') >= 0) {
            var $links = $('a');
            for (var i = 0; i < $links.length; i++) {
                if ($links[i].href.indexOf('https') >= 0) {
                    $links[i].href = $links[i].href.replace(/https/g, 'http');
                }
            }
        }
        var campusCollection = new BaseCollection();
        var TopCampusView = BaseListView.extend({
            itemViewType: Welcome.Views.TopCampaign,
            el: '.js-campus-campaigns',
            collection: campusCollection
        });
        campusCollection.url = '/campus/category/campaigns/${campus.tagId}';
        campusCollection.load().success(function () {
            new TopCampusView().render();
        });
        $('.campus-projects-type_i > a').click(function (e) {
            e.preventDefault();
            var me = $(this),
                    status = me.attr('data-status');
            if (status) {
                campusCollection.url = '/campus/category/campaigns/${campus.tagId}?status=' + status;
            } else {
                campusCollection.url = '/campus/category/campaigns/${campus.tagId}';
            }
            campusCollection.load();
            $('.campus-projects-type_i.active').each(function(i, el) {
                $(el).removeClass('active');
            });
            me.parent().addClass('active');
        });

        Banner.init('${mainAppUrl}', workspace.appModel.get('myProfile').get('isAuthor'));
    });
    })();

</script>

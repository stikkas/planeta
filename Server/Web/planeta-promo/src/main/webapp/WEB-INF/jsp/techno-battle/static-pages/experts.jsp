<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>

<%@ include file="/WEB-INF/jsp/techno-battle/includes/head.jsp" %>

<body>
<%@ include file="/WEB-INF/jsp/techno-battle/includes/sidebar.jsp" %>
<div class="wrapper">
    <div class="main">
        <%@ include file="/WEB-INF/jsp/techno-battle/includes/topbar.jsp" %>
        <div class="container">
            <div class="experts">
                <c:if test="${not empty experts}">
                <c:forEach var="expert" items="${experts}">
                    <div class="experts_i">
                        <div class="experts_i-cont">
                            <div class="experts_ava">
                                <div class="experts_ava-in">
                                    <img src="${hf:getThumbnailUrl(expert.image, "USER_AVATAR", "PHOTO")}" alt="${expert.name} ${expert.surname}">
                                </div>
                            </div>

                            <div class="experts_cont">
                                <div class="experts_cont-in">
                                    <div class="experts_name">
                                        <span class="experts_name-f">${expert.name}</span>
                                        <span class="experts_name-l">${expert.surname}</span>
                                    </div>
                                    <div class="experts_prof">${expert.description}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach>
                </c:if>
            </div>
        </div>


    </div>
    <%@ include file="/WEB-INF/jsp/techno-battle/includes/footer.jsp" %>
</div>

</body>
</html>
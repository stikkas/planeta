<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<head>

    <%@include file="5-years-metas.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-head.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/generated/common-favicon.jsp" %>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="//${hf:getStaticBaseUrl("")}/css-generated/planeta-4-years.css" type="text/css" rel="stylesheet">

    <script>
        var isBreak = false;
        var videoSrc = "https://files.planeta.ru/mailer/earth-15.mp4";
    </script>
    <p:script src="happy-birthday-5-years.js" />
    <script>
        $(function () {
            var pluralRules = [
                function (n) { return ((n % 10 === 1) && (n % 100 !== 11)); },
                function (n) { return ((n % 10) >= 2 && (n % 10) <= 4 && ((n % 10) % 1) === 0) && ((n % 100) < 12 || (n % 100) > 14); },
                function (n) { return ((n % 10) === 0 || ((n % 10) >= 5 && (n % 10) <= 9 && ((n % 10) % 1) === 0) || ((n % 100) >= 11 && (n % 100) <= 14 && ((n % 100) % 1) === 0)); },
                function (n) { return true; }
            ];

            var plural = function (word, num) {
                var forms = word.split('_'),
                    minCount = Math.min(pluralRules.length, forms.length),
                    i = -1;

                while (++i < minCount) {
                    if (pluralRules[i](num)) {
                        return forms[i];
                    }
                }
                return forms[minCount - 1];
            };

            var relativeTimeWithPlural = function (number, withoutSuffix, key) {
                var format = {
                    'mm': 'минута_минуты_минут_минуты',
                    'hh': 'час_часа_часов_часа',
                    'dd': 'день_дня_дней_дня',
                    'MM': 'месяц_месяца_месяцев_месяца',
                    'yy': 'год_года_лет_года'
                };
                if (key === 'm') {
                    return withoutSuffix ? 'минута' : 'минуту';
                }
                if (key === 'mm' && pluralRules[0](+number) && +number > 0) {
                    return number + ' ' + (withoutSuffix ? 'минута' : 'минуту');
                }
                else {
                    return number + ' ' + plural(format[key], +number);
                }
            };

            var startDate = moment([2012, 5, 7]);
            var nowDate = moment();

            var years = nowDate.diff(startDate, "years");
            startDate.add(years, 'years');

            var months = nowDate.diff(startDate, "months");
            startDate.add(months, 'months');

            var days = nowDate.diff(startDate, "days");

            if ( years > 0 ) {
                $('.header_counter-year').html(relativeTimeWithPlural(years, true, 'yy')).show();
            }
            if ( months > 0 ) {
                var monthText = monthText = relativeTimeWithPlural(months, true, 'MM');
                if (days == 0) {
                    $('.header_counter-month').html('и   ' + monthText);
                } else {
                    $('.header_counter-month').html(monthText).show();
                }
            }
            if ( days > 0 ) {
                var daysString = relativeTimeWithPlural(days, true, 'dd');
                if (years > 0 && months > 0) {
                    $('.header_counter-day').html('и   ' + daysString).show();
                } else {
                    $('.header_counter-day').html(daysString).show();
                }
            }
            if ( months == 0 && days > 0 ) {
                $('.header_counter-month').html('и').show();
            }

            var shareData = {
                counterEnabled: false,
                hidden: false,
                parseMetaTags: true,
                url: window.location.href,
                className: 'sharing-popup-social sharing-mini'
            };

            $('.share_cont').share(shareData);

        });
    </script>

</head>
<body>


<div class="video-back">
    <video class="video-back_video active" preload="auto"></video>
</div>


<div class="page-scroll">
    <div><div class="page-scroll_mouse"></div></div>
    <div><div class="page-scroll_point animBounce"></div></div>
    <div class="page-scroll_text">вниз</div>
</div>





<div class="year-nav">
    <a href="#page-first" class="year-nav_link year-nav_start" id="year-nav-0"><span>старт</span></a>
    <a href="#page-1" class="year-nav_link" id="year-nav-1"><span>2012</span></a>
    <a href="#page-2" class="year-nav_link" id="year-nav-2"><span>2013</span></a>
    <a href="#page-3" class="year-nav_link" id="year-nav-3"><span>2014</span></a>
    <a href="#page-4" class="year-nav_link" id="year-nav-4"><span>2015</span></a>
    <a href="#page-5" class="year-nav_link" id="year-nav-5"><span>2016</span></a>
    <a href="#page-6" class="year-nav_link" id="year-nav-6"><span>2017</span></a>
</div>



<div id="wrap-0" class="year-wrap">
    <div class="page static" id="page-first">
        <div class="page-cnt">
            <div class="page-cont">
                <div class="page-wrap wrap">

                    <div class="header">
                        <div class="wrap">
                            <div class="header_head">
                                Хроники <a href="https://planeta.ru/" class="logo"></a> planeta.ru
                            </div>
                            <div class="header_counter-lbl">
                                Развиваем краудфандинг в россии
                            </div>
                            <div class="header_counter">
                                <span class="header_counter-year hide"></span> <span class="header_counter-month hide"></span> <span class="header_counter-day hide"></span>
                            </div>
                            <div class="header_text">
                                <p>7 июня&nbsp;&mdash; День российского краудфандинга не&nbsp;просто так, а&nbsp;в&nbsp;честь дня рождения краудфандинговой платформы &#8470;&nbsp;1 в&nbsp;России!</p>
                                <p>С&nbsp;самого начала Planeta.ru была больше, чем просто сайт или удобный сервис&nbsp;&mdash; это&nbsp;новая философия, форма отношений между авторами проектов и&nbsp;их&nbsp;аудиторией.</p>
                            </div>
                            <div class="header_action">
                                <span class="header-btn">Смотреть наши хроники</span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>




<div id="wrap-1" class="year-wrap">
    <div class="year" id="year-1">
        <div class="year-cnt">
            <div class="year-cont">
                2012
            </div>
        </div>
    </div>



    <div class="page" id="page-1">
        <div class="page-cnt">
            <div class="page-cont">
                <div class="page-wrap wrap">

                    <div class="timeline timeline__sm">
                        <div class="timeline_i">
                            <div class="timeline_date">
                                10 января 2012
                            </div>
                            <div class="timeline_event">
                                <div class="timeline_title">
                                    <nobr>Бета-тест</nobr>: <a href="https://planeta.ru/campaigns/19" target="_blank">первый проект на&nbsp;Planeta.ru</a>&nbsp;&mdash; новый альбом группы <nobr>&laquo;Би-2&raquo;</nobr>
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date">
                                4 марта 2012
                            </div>
                            <div class="timeline_event">
                                <div class="timeline_title">
                                    1&nbsp;000&nbsp;000&nbsp;рублей&nbsp;&mdash; первая круглая цифра краудфандинговой платформы Planeta.ru
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date">
                                30 мая 2012
                            </div>
                            <div class="timeline_event">
                                <div class="timeline_title">
                                    В&nbsp;нас верят: первые 10 проектов запущены на&nbsp;Planeta.ru!
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date">
                                7 июня 2012
                            </div>
                            <div class="timeline_event">
                                <div class="timeline_project-wrap">
                                    <div class="timeline_project-ico">
                                        <div class="project-cosmonaut"></div>
                                    </div>
                                    <div class="timeline_project">
                                        <div class="timeline_project-box">
                                            Поехали! Официальный запуск Planeta.ru
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date">
                                27 октября 2012
                            </div>
                            <div class="timeline_event">
                                <div class="timeline_title">
                                    Первая <nobr>онлайн-трансляция</nobr>: концерт <nobr>&laquo;Би-2&raquo;</nobr> в&nbsp;<nobr>Crocus City Hall</nobr>
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date">
                                19 ноября 2012
                            </div>
                            <div class="timeline_event">
                                <div class="timeline_title">
                                    Авторы <nobr>крауд-проектов</nobr> на&nbsp;Planeta.ru собрали 5&nbsp;млн.&nbsp;рублей
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



<div id="wrap-2" class="year-wrap">
    <div class="year" id="year-2">
        <div class="year-cnt">
            <div class="year-cont">
                2013
            </div>
        </div>
    </div>



    <div class="page" id="page-2">
        <div class="page-cnt">
            <div class="page-cont">
                <div class="page-wrap wrap">

                    <div class="timeline">
                        <div class="timeline_i">
                            <div class="timeline_date">
                                18 марта 2013
                            </div>
                            <div class="timeline_event">
                                <div class="timeline_title">
                                    Авторы <nobr>крауд-проектов</nobr> на&nbsp;Planeta.ru собрали 10&nbsp;млн.&nbsp;рублей
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date">
                                31 августа 2013
                            </div>
                            <div class="timeline_event timeline_event__md">
                                <div class="timeline_project-wrap">
                                    <div class="timeline_project-ico">
                                        <div class="project-planet-2"></div>
                                    </div>
                                    <div class="timeline_project">
                                        <div class="timeline_project-box">
                                            Мультфильм Гарри Бардина &laquo;Три мелодии&raquo; <a href="https://planeta.ru/campaigns/995" target="_blank">собрал 2&nbsp;млн. 251 тыс. 681&nbsp;рубль</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date">
                                30 октября 2013
                            </div>
                            <div class="timeline_event">
                                <div class="timeline_title">
                                    500 проектов запущено на&nbsp;Planeta.ru
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



<div id="wrap-3" class="year-wrap">
    <div class="year" id="year-3">
        <div class="year-cnt">
            <div class="year-cont">
                2014
            </div>
        </div>
    </div>



    <div class="page" id="page-3">
        <div class="page-cnt">
            <div class="page-cont">
                <div class="page-wrap wrap">

                    <div class="timeline">
                        <div class="timeline_i">
                            <div class="timeline_date">
                                3 апреля 2014
                            </div>
                            <div class="timeline_event timeline_event__md">
                                <div class="timeline_project-wrap">
                                    <div class="timeline_project-ico">
                                        <div class="project-planet-1"></div>
                                    </div>
                                    <div class="timeline_project">
                                        <div class="timeline_project-box">
                                            Телеспектакль &laquo;Петрушка&raquo; Виктора Шендеровича и&nbsp;Владимира Мирзоева <a href="https://planeta.ru/campaigns/2088" target="_blank">собрал 5&nbsp;млн. 865 тыс. 800&nbsp;рублей</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date">
                                13 мая 2014
                            </div>
                            <div class="timeline_event">
                                <div class="timeline_title">
                                    1&nbsp;000 проектов запущено на&nbsp;Planeta.ru
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date">
                                7 июля 2014
                            </div>
                            <div class="timeline_event">
                                <div class="timeline_title">
                                    Авторы <nobr>крауд-проектов</nobr> собрали на&nbsp;Planeta.ru 100&nbsp;000&nbsp;000&nbsp;рублей!
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date">
                                8 сентября 2014
                            </div>
                            <div class="timeline_event timeline_event__md">
                                <div class="timeline_project-wrap">
                                    <div class="timeline_project-ico">
                                        <div class="project-planet-5"></div>
                                    </div>
                                    <div class="timeline_project">
                                        <div class="timeline_project-box">
                                            Запуск <a href="https://shop.planeta.ru/" target="_blank">магазина <nobr>крауд-товаров</nobr> Planeta.ru</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date">
                                25 ноября 2014
                            </div>
                            <div class="timeline_event">
                                <div class="timeline_title">
                                    Премию Рунета в&nbsp;номинации &laquo;Экономика, бизнес и&nbsp;инвестиции&raquo; получила Planeta.ru!
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



<div id="wrap-4" class="year-wrap">
    <div class="year" id="year-4">
        <div class="year-cnt">
            <div class="year-cont">
                2015
            </div>
        </div>
    </div>



    <div class="page" id="page-4">
        <div class="page-cnt">
            <div class="page-cont">
                <div class="page-wrap wrap">

                    <div class="timeline">
                        <div class="timeline_i">
                            <div class="timeline_date">
                                1 февраля 2015
                            </div>
                            <div class="timeline_event">
                                <div class="timeline_title">
                                    Бренд Lipton и&nbsp;Planeta.ru запускают <a href="http://goodstarter.liptontea.ru/" target="_blank">Goodstarter</a>&nbsp;&mdash; конкурс для социальных предпринимателей
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date">
                                7 апреля 2015
                            </div>
                            <div class="timeline_event timeline_event__md">
                                <div class="timeline_project-wrap">
                                    <div class="timeline_project-ico">
                                        <div class="project-planet-4"></div>
                                    </div>
                                    <div class="timeline_project">
                                        <div class="timeline_project-box">
                                            Первое занятие в&nbsp;<a href="https://school.planeta.ru/" target="_blank">&laquo;Школе краудфандинга&raquo;</a> Planeta.ru
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date">
                                7 июня 2015
                            </div>
                            <div class="timeline_event">
                                <div class="timeline_title">
                                    День рождения &laquo;Планеты&raquo; назван Днём российского краудфандинга
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date">
                                5 октября 2015
                            </div>
                            <div class="timeline_event">
                                <div class="timeline_title">
                                    Авторы <nobr>крауд-проектов</nobr> собрали на&nbsp;Planeta.ru 300&nbsp;млн.&nbsp;рублей!
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date">
                                2 декабря 2015
                            </div>
                            <div class="timeline_event">
                                <div class="timeline_title">
                                    4&nbsp;000 проектов запущено на&nbsp;Planeta.ru
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date">
                                22 декабря 2015
                            </div>
                            <div class="timeline_event timeline_event__md">
                                <div class="timeline_project-wrap">
                                    <div class="timeline_project-ico">
                                        <div class="project-planet-6"></div>
                                    </div>
                                    <div class="timeline_project">
                                        <div class="timeline_project-box">
                                            &laquo;Аквариум&raquo; и&nbsp;БГ: новые песни группы <a href="https://planeta.ru/campaigns/aquarium" target="_blank">собрали 7&nbsp;млн. 303 тыс. 803&nbsp;рубля</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



<div id="wrap-5" class="year-wrap">
    <div class="year" id="year-5">
        <div class="year-cnt">
            <div class="year-cont">
                2016
            </div>
        </div>
    </div>



    <div class="page" id="page-5">
        <div class="page-cnt">
            <div class="page-cont">
                <div class="page-wrap wrap">

                    <div class="timeline timeline__sm">
                        <div class="timeline_i">
                            <div class="timeline_date">
                                16 марта 2016
                            </div>
                            <div class="timeline_event timeline_event__md">
                                <div class="timeline_project-wrap">
                                    <div class="timeline_project-ico">
                                        <div class="project-planet-1"></div>
                                    </div>
                                    <div class="timeline_project">
                                        <div class="timeline_project-box">
                                            &laquo;Первая Всероссийская Школа краудфандинга&raquo;: стартовал образовательный проект Planeta.ru в&nbsp;5 регионах России
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date">
                                17 сентября 2016
                            </div>
                            <div class="timeline_event">
                                <div class="timeline_title">
                                    Авторы <nobr>крауд-проектов</nobr> собрали на&nbsp;Planeta.ru 500&nbsp;млн.&nbsp;рублей!
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date">
                                18 сентября 2016
                            </div>
                            <div class="timeline_event timeline_event__md">
                                <div class="timeline_project-wrap">
                                    <div class="timeline_project-ico">
                                        <div class="project-planet-2"></div>
                                    </div>
                                    <div class="timeline_project">
                                        <div class="timeline_project-box">
                                            Новый рекорд российского краудфандинга: группа &laquo;Алиса&raquo; <a href="https://planeta.ru/campaigns/kinchev" target="_blank">собирает 11&nbsp;млн. 333 тыс. 777&nbsp;рублей</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date">
                                4 октября 2016
                            </div>
                            <div class="timeline_event">
                                <div class="timeline_title">
                                    <nobr>Премьер-министр</nobr> РФ&nbsp;Дмитрий Медведев <a href="https://planeta.ru/planeta/news!post67012" target="_blank">поддержал</a> наш новый проект&nbsp;&mdash; &laquo;БиблиоРодина&raquo;!
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date">
                                20 октября 2016
                            </div>
                            <div class="timeline_event">
                                <div class="timeline_title">
                                    <a href="https://charity.planeta.ru/" target="_blank">&laquo;Планета добрых людей&raquo;</a>: запущен раздел для благотворительных проектов и&nbsp;социального предпринимательства
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date">
                                27 декабря 2016
                            </div>
                            <div class="timeline_event">
                                <div class="timeline_title">
                                    7&nbsp;000 проектов запущено на&nbsp;Planeta.ru
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



<div id="wrap-6" class="year-wrap">
    <div class="year" id="year-6">
        <div class="year-cnt">
            <div class="year-cont">
                2017
            </div>
        </div>
    </div>



    <div class="page" id="page-6">
        <div class="page-cnt">
            <div class="page-cont">
                <div class="page-wrap wrap">

                    <div class="timeline timeline__sm">
                        <div class="timeline_i">
                            <div class="timeline_date">
                                1 февраля 2017
                            </div>
                            <div class="timeline_event timeline_event__md">
                                <div class="timeline_project-wrap">
                                    <div class="timeline_project-ico">
                                        <div class="project-planet-1"></div>
                                    </div>
                                    <div class="timeline_project">
                                        <div class="timeline_project-box">
                                            <nobr>Реалити-шоу</nobr> <a href="https://promo.planeta.ru/techbattle" target="_blank">&laquo;Битва технологий&raquo;</a>: Planeta.ru запустила первое в&nbsp;Рунете шоу для железных стартапов
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date">
                                8 марта 2017
                            </div>
                            <div class="timeline_event">
                                <div class="timeline_title">
                                    Авторы <nobr>крауд-проектов</nobr> собрали на&nbsp;Planeta.ru 600&nbsp;млн.&nbsp;рублей!
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date">
                                2 апреля 2017
                            </div>
                            <div class="timeline_event timeline_event__md">
                                <div class="timeline_project-wrap">
                                    <div class="timeline_project-ico">
                                        <div class="project-planet-2"></div>
                                    </div>
                                    <div class="timeline_project">
                                        <div class="timeline_project-box">
                                            &laquo;Ночные снайперы&raquo; <a href="https://planeta.ru/campaigns/arbenina" target="_blank">собрали</a> на&nbsp;строительство студии 5&nbsp;млн. 707 тыс. 577&nbsp;рублей
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date">
                                20 апреля 2017
                            </div>
                            <div class="timeline_event">
                                <div class="timeline_title">
                                    8&nbsp;000 проектов запущено на&nbsp;Planeta.ru
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date">
                                24 апреля 2017
                            </div>
                            <div class="timeline_event">
                                <div class="timeline_title">
                                    Planeta.ru и&nbsp;РГСУ: соглашение о&nbsp;создании курса по&nbsp;краудфандингу с&nbsp;выдачей дипломов государственного образца
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date">
                                27 апреля 2017
                            </div>
                            <div class="timeline_event">
                                <div class="timeline_title">
                                    Проекты Центра помощи животным &laquo;Ника&raquo; за&nbsp;3 года на&nbsp;Planeta.ru <a href="https://planeta.ru/221926" target="_blank">собрали 13&nbsp;млн. 945 тыс. 347&nbsp;рублей</a>
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date">
                                28 апреля 2017
                            </div>
                            <div class="timeline_event">
                                <div class="timeline_title">
                                    780 проектов одновременно собирают средства на&nbsp;Planeta.ru&nbsp;&mdash; переименовываем отдел по&nbsp;работе с&nbsp;авторами в&nbsp;&laquo;горячий цех&raquo;!
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date">
                                3 мая 2017
                            </div>
                            <div class="timeline_event timeline_event__md">
                                <div class="timeline_project-wrap">
                                    <div class="timeline_project-ico">
                                        <div class="project-planet-2"></div>
                                    </div>
                                    <div class="timeline_project">
                                        <div class="timeline_project-box">
                                            Первый <nobr>крауд-проект</nobr> &laquo;Битвы технологий&raquo;&nbsp;&mdash; электроприставка UNA&nbsp;&mdash; <a href="https://promo.planeta.ru/techbattle/participants" target="_blank">собрал миллион рублей</a>!
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



<div id="wrap-last" class="year-wrap">
    <div class="page static" id="page-last">
        <div class="page-cnt">
            <div class="page-cont">
                <div class="page-wrap wrap">

                    <div class="timeline timeline__final">
                        <div class="timeline_i">
                            <div class="timeline_date"></div>
                            <div class="timeline_event">
                                <div class="timeline_final">
                                    То ли еще будет!
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date"></div>
                            <div class="timeline_event">
                                <div class="timeline_share">
                                    <div class="timeline_share-head">
                                        Поделиться в социальных сетях
                                    </div>

                                    <div class="share_cont">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="timeline_i">
                            <div class="timeline_date"></div>
                            <div class="timeline_event">
                                <div class="read-blog">
                                    <p>Историю создания Planeta.ru от&nbsp;первого лица читайте в&nbsp;разделе <a href="https://planeta.ru/about">&laquo;О&nbsp;нас&raquo;</a></p>
                                </div>
                            </div>
                        </div>
                    </div>




                    <div class="planeta-menu">
                        <div class="planeta-menu_list wrap">
                            <div class="planeta-menu_i">
                                <a href="https://planeta.ru/welcome/about.html#about">О Planeta.ru</a>
                            </div>
                            <div class="planeta-menu_i">
                                <a href="https://planeta.ru/search/projects">Проекты</a>
                            </div>
                            <div class="planeta-menu_i">
                                <a href="https://planeta.ru/welcome/bonuses.html">VIP Клуб</a>
                            </div>
                            <div class="planeta-menu_i">
                                <a href="https://school.planeta.ru/">Школа</a>
                            </div>
                            <div class="planeta-menu_i">
                                <a href="https://shop.planeta.ru/">Магазин</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


</body>
</html>

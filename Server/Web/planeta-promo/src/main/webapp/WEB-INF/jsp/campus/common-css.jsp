<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>

<link rel="stylesheet" type="text/css" href="//${hf:getStaticBaseUrl("")}/css-generated/common.css">
<link rel="stylesheet" type="text/css" href="//${hf:getStaticBaseUrl("")}/css-generated/campus.css">
<link rel="stylesheet" type="text/css" href="//${hf:getStaticBaseUrl("")}/css-generated/pln-stats.css">
<!--[if lte IE 9]>
<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css/placeholder_polyfill.css"/>
<![endif]-->
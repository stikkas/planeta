<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div id="campus-lead" class="campus-lead campus-lead__small">
    <div class="wrap">
        <div class="col-12">
            <div class="campus-lead_head">
                Что такое <nobr>онлайн-кампус</nobr>?
            </div>
            <div class="campus-lead_text">
                <p><nobr>Онлайн-кампус</nobr>&nbsp;&mdash; это <nobr>интернет-площадка</nobr>, где студенты, выпускники и&nbsp;сотрудники университетов объединяются, чтобы воплотить в&nbsp;жизнь проекты, связанные с&nbsp;их&nbsp;учебными заведениями.</p>
                <p>Здесь молодые ученые могут получить средства на&nbsp;свои исследования, учащиеся обустраивают университетские кафе и&nbsp;зоны отдыха, а&nbsp;преподаватели организуют семинары, но&nbsp;самое главное&nbsp;&mdash; все они находят здесь своих единомышленников.</p>
            </div>
        </div>
    </div>
</div>

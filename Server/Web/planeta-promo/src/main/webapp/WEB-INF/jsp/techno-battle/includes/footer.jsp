<div class="footer">
    <div class="container">
        <div class="f-sharing">
            <div class="f-sharing_head">
                Поделиться:
            </div>
            <div class="f-sharing_cont js-share-container">
            </div>
        </div>



        <div class="f-subscribe">
            <div class="f-subscribe_head">
                Подпишитесь на новости «Битвы технологий»
            </div>
            <form class="f-subscribe_cont" id="formEmail">
                <div class="f-subscribe_msg msg-error">
                    Некорректный адрес
                </div>
                <div class="f-subscribe_msg msg-success">
                    Подиска оформлена
                </div>
                <input id="letterEmail" type="text" class="f-subscribe_input" placeholder="Ваш e-mail">
                <button class="f-subscribe_btn js-send-letter"></button>
            </form>
        </div>

        <div class="f-common">
            <div class="f-copy">
                <b>БИТВА ТЕХНОЛОГИЙ</b> © 2017
            </div>


            <div class="f-develop">
                <div class="f-develop_head">
                    Разработано в
                </div>
                <div class="f-develop_cont">
                    <a href="https://planeta.ru/" class="pln-logo"></a>
                </div>
            </div>
        </div>
    </div>
</div>


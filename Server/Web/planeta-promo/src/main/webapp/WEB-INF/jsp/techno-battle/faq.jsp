<div class="faq" id="faq">
    <div class="wrap wow slideUp" data-wow-offset="250">
        <div class="col-12">
            <div class="faq_block">
                <div class="faq_head">FAQ</div>

                <div class="faq_list">
                    <div class="faq_i">
                        <div class="faq_q">
                            Что это?
                        </div>
                        <div class="faq_a">
                            <p><b>&laquo;Битва технологий&raquo;</b>&nbsp;&mdash; это первое технологическое <nobr>реалити-шоу</nobr> в&nbsp;Рунете для стартапов в&nbsp;сфере hardware. Организаторы: Planeta.ru, краудфандинговая платформа &#8470;&nbsp;1 в&nbsp;России, и&nbsp;EVA Invest&nbsp;&mdash; инвестиционная площадка для <nobr>hardware-стартапов</nobr>.</p>
                            <p>Десять лучших проектов подготовят свои краудфандинговые кампании, а&nbsp;все желающие смогут понаблюдать за&nbsp;этим и&nbsp;выбрать лучших. Это не&nbsp;просто учеба&nbsp;&mdash; это настоящее шоу с&nbsp;<nobr>онлайн-трансляцией</nobr> и&nbsp;народным голосованием!</p>
                        </div>
                    </div>

                    <div class="faq_i">
                        <div class="faq_q">
                            Когда и на каких условиях?
                        </div>
                        <div class="faq_a">
                            <p><b>&laquo;Битва технологий&raquo;</b> пройдет в&nbsp;<nobr>офлайн-формате</nobr> (Москва) в&nbsp;феврале 2017&nbsp;г. Для участия оставьте свою заявку на&nbsp;этой странице. Допускаются команды от&nbsp;1 до&nbsp;4 человек (число людей, работающих над проектом, не&nbsp;ограничено). Отбор будет проводиться до&nbsp;20 декабря 2016 года.</p>
                            <p><b>Обязательным условием участия является наличие рабочего прототипа <nobr>hardware-продукта</nobr>. Учтите, что участие в&nbsp;&laquo;Битве технологий&raquo; платное.</b></p>
                        </div>
                    </div>

                    <div class="faq_i">
                        <div class="faq_q">
                            Что я получу?
                        </div>
                        <div class="faq_a">
                            <p>Все десять команд по&nbsp;итогам получат готовый <nobr>крауд-проект</nobr>, созданный при участии опытных режиссеров, операторов, дизайнеров, копирайтеров и&nbsp;экспертов краудфандинга.</p>
                            <p><b>В&nbsp;конце курса эксперты &laquo;Битвы технологий&raquo; и&nbsp;народное голосование выберут пять лучших <nobr>крауд-проектов</nobr>, которые разделят призовой фонд и&nbsp;получат специальную <nobr>PR-поддержку</nobr> со&nbsp;стороны Planeta.ru, EVA Invest, информационных партнеров и&nbsp;<nobr>celebrity-ментора</nobr>.</b></p>
                        </div>
                    </div>

                    <div class="faq_i">
                        <div class="faq_q">
                            Как это будет происходить?
                        </div>
                        <div class="faq_a">
                            <p>В&nbsp;течение февраля участники <b>&laquo;Битвы технологий&raquo;</b> под руководством экспертов Planeta,ru и&nbsp;авторов успешных проектов будут изучать тонкости создания <nobr>крауд-кампаний</nobr>, снимать видео, писать тексты, готовить вознаграждения и&nbsp;учиться продвигать свои идеи.</p>
                            <p><b>Командам предстоит защитить результаты своей работы не&nbsp;только перед экспертами профильных индустрий, но&nbsp;и&nbsp;перед <nobr>интернет-аудиторией</nobr>.</b></p>
                        </div>
                    </div>
                </div>

                <script>
                    $('.faq_i').each(function() {
                        var item = this;
                        var q = $('.faq_q', item);
                        var a = $('.faq_a', item);

                        q.on('click', function() {
                            q.toggleClass('expanded');
                            a.slideToggle(300);
                        });
                    });
                </script>
            </div>
        </div>
    </div>
</div>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="campus-idea">
    <div class="wrap">
        <div class="col-12">
            <div class="campus-idea_list">
                <div class="campus-idea_i">
                    <div class="campus-idea_i-cont">
                        В&nbsp;вашем общежитии нужен <nobr>WI-FI</nobr>, а&nbsp;научной лаборатории не&nbsp;хватает оборудования?
                    </div>
                </div>
                <div class="campus-idea_i">
                    <div class="campus-idea_i-cont">
                        Вы&nbsp;хотите устроить интересное мероприятие для студентов или сотрудников института?
                    </div>
                </div>
                <div class="campus-idea_i">
                    <div class="campus-idea_i-cont">
                        У&nbsp;вас появилась идея, которая сделает учебный процесс проще и&nbsp;эффективней?
                    </div>
                </div>
            </div>
            <div class="campus-idea_text">
                Предложите ее&nbsp;жителям «<span class="campus-idea_logo"><span class="campus-idea_logo-hl">Онлайн</span> кампуса</span>», и&nbsp;они помогут вам сделать ее&nbsp;реальностью! Ваше образование находится в&nbsp;ваших руках: измените его к&nbsp;лучшему при поддержке единомышленников!
            </div>
            <div class="campus-idea_action">
                <a class="btn btn-campus-primary" href="https://${properties['application.host']}/funding-rules?UNIVERSITY">Создать проект</a>
            </div>
        </div>
    </div>
</div>

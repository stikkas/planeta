<div class="top-bar">
    <div class="container">
        <div class="top-bar_logos">
            <div class="top-bar_logos-i">
                <a href="https://planeta.ru/" class="header-pln-logo pln-logo"></a>
            </div>
            <div class="top-bar_logos-i">
                <a href="http://www.rvc.ru/" class="header-rvc-logo"></a>
            </div>
            <div class="top-bar_logos-i">
                <a href="http://eva.fund/" class="header-eva-logo"></a>
            </div>
        </div>

        <div class="top-bar_lead">
            Первое технологическое <nobr>реалити-шоу</nobr> в&nbsp;рунете для <nobr>hardware-стартапов</nobr>
        </div>
    </div>
</div>

package ru.planeta.promo.controllers.social.sharingcount

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.commons.io.IOUtils
import org.apache.commons.lang3.math.NumberUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.helper.ProjectService
import ru.planeta.commons.web.WebUtils
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.promo.VoteType
import ru.planeta.promo.controllers.Urls

import java.io.IOException
import java.net.URL
import java.util.Date
import java.util.TreeMap
import java.util.concurrent.ConcurrentHashMap

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 17.11.2016
 * Time: 16:55
 */
@Controller
class SharingCountController(private val projectService: ProjectService) {

    private val cache = ConcurrentHashMap<ShareObject, ShareCount>()


    private fun getProjectUrl(projectId: Long): String = projectService.getUrl(ProjectType.PROMO, "techbattle/project/" + projectId)

    private fun getExternalCountUrl(projectId: Long, voteType: VoteType?): String = getExternalCountUrl(voteType, getProjectUrl(projectId))

    private fun getCount(projectId: Long, voteType: VoteType?): Int =
            parseResp(IOUtils.toString(URL(getExternalCountUrl(projectId, voteType))), voteType)

    private fun getCount(shareCount: ShareObject): Int {
        val fromCache = cache[shareCount]
        if (fromCache != null) {
            if (!expired(fromCache.lastTimeChecked)) {
                return fromCache.count
            }
        }

        val count = getCount(shareCount.projectId, shareCount.voteType)
        val shareObject = ShareCount(count, Date())
        cache.put(shareCount, shareObject)
        return count
    }

    private fun expired(lastTimeChecked: Date?): Boolean =
            if (lastTimeChecked == null) false
            else Date().time - lastTimeChecked.time > period

    @GetMapping(Urls.SHARING_COUNT)
    @ResponseBody
    fun getCachedCount(@RequestParam projectId: Long,
                       @RequestParam voteType: VoteType): ActionStatus<Int> =
            ActionStatus.createSuccessStatus(getCount(ShareObject(projectId, voteType)))

    companion object {
        private const val vkCountUrl = "https://vk.com/share.php?act=count&index=1&url="
        private const val fbCountUrl = "https://graph.facebook.com/?id="

        private const val period: Long = 1000

        private val countMap = object : TreeMap<VoteType, String>() {
            init {
                put(VoteType.VK, vkCountUrl)
                put(VoteType.FB, fbCountUrl)
            }
        }

        private fun getExternalCountUrl(voteType: VoteType?, url: String): String {
            return countMap[voteType] + WebUtils.encodeUrl(url)
        }

        private fun parseResp(resp: String, voteType: VoteType?): Int =
                when (voteType) {
                    VoteType.FB -> parseFb(resp)
                    VoteType.VK -> parseVk(resp)
                    null -> 0
                }

        private fun parseFb(resp: String): Int {
            val mapper = ObjectMapper()
            val userData = mapper.readValue(resp, Map::class.java)
            val count = userData["shares"]
            return if (count != null && count is String) {
                NumberUtils.toInt(count as String?)
            } else 0

        }

        private fun parseVk(resp: String): Int {
            // VK.Share.count(1, 17157);
            val begin = resp.indexOf(' ')
            val end = resp.indexOf(')')
            return NumberUtils.toInt(resp.substring(begin + 1, end))
        }
    }
}

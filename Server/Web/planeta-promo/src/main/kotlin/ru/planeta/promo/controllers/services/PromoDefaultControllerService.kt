package ru.planeta.promo.controllers.services

import org.springframework.stereotype.Service
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.model.enums.ProjectType
import ru.planeta.promo.controllers.Actions


/**
 * User: michail
 * Date: 17.11.2016
 * Time: 17:01
 */

@Service
class PromoDefaultControllerService(val baseControllerService: BaseControllerService) {
    fun createDefaultPromoModelAndView(action: Actions): ModelAndView =
            baseControllerService.createDefaultModelAndView(action, ProjectType.PROMO)
}

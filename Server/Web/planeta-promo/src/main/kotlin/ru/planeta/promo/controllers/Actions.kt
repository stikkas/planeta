package ru.planeta.promo.controllers

import ru.planeta.api.web.controllers.IAction

enum class Actions(private val text: String) : IAction {
    TECHNO_BATTLE_PROJECT_PAGE("techno-battle/project"),
    TECHNO_BATTLE_PARTICIPANTS("techno-battle/contest"),
    TECHNO_BATTLE_ABOUT("techno-battle/static-pages/about"),
    TECHNO_BATTLE_EXPERTS("techno-battle/static-pages/experts"),
    TECHNO_BATTLE_NEWS("techno-battle/news"),
    TECHNO_BATTLE_PARTNERS("techno-battle/static-pages/partners"),
    TECHNO_BATTLE_WINNERS("techno-battle/static-pages/winners"),
    TECHNO_BATTLE_VIDEO("techno-battle/static-pages/video"),

    CAMPUS_CATEGORY("campus/category"),
    HAPPY_BIRTHDAY_5_YEARS("happy-birthday/5-years"),
    CAMPUS("campus/index");

    override val path: String
        get() = text

    override val actionName: String
        get() = text

    override fun toString(): String = text
}





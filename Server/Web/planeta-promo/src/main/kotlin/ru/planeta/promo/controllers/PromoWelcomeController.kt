package ru.planeta.promo.controllers

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.web.utils.ProperRedirectView

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 22.11.2016
 * Time: 12:00
 */

@Controller
class PromoWelcomeController {
    @GetMapping(Urls.ROOT)
    fun welcome(): ModelAndView = ModelAndView(ProperRedirectView(Urls.Technobattle.CONTEST))
}

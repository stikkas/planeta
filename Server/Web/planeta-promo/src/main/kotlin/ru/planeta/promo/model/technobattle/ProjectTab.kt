package ru.planeta.promo.model.technobattle

enum class ProjectTab {
    ABOUT, NEWS, COMMENTS;

    companion object {
        operator fun contains(tab: String): Boolean {
            return ProjectTab.values().any { it.name == tab }
        }
    }
}

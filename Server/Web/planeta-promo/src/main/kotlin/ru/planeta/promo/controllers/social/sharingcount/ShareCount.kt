package ru.planeta.promo.controllers.social.sharingcount

import java.util.Date

/**
 * Created by asavan on 21.01.2017.
 */
class ShareCount(var count: Int, var lastTimeChecked: Date?)

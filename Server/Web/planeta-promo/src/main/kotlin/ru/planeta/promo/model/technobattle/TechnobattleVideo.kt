package ru.planeta.promo.model.technobattle

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 19.01.2017
 * Time: 13:10
 */
class TechnobattleVideo {
    var image: String? = null
    var iframeurl: String? = null
    var title: String? = null
    var date: String? = null
    var videoId: String? = null
}

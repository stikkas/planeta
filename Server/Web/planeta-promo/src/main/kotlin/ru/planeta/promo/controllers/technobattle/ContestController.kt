package ru.planeta.promo.controllers.technobattle

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.model.ConfigurationType
import ru.planeta.api.news.ProfileNewsService
import ru.planeta.api.service.configurations.ConfigurationService
import ru.planeta.api.service.promo.TechnobattleProjectService
import ru.planeta.dao.profiledb.PostDAO
import ru.planeta.model.profile.Post
import ru.planeta.promo.controllers.Actions
import ru.planeta.promo.controllers.Urls
import ru.planeta.promo.controllers.services.PromoDefaultControllerService
import ru.planeta.promo.model.technobattle.TechnobattleExpert
import ru.planeta.promo.model.technobattle.TechnobattlePartner
import ru.planeta.promo.model.technobattle.TechnobattleVideo
import java.util.*

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 17.01.2017
 * Time: 12:32
 */
@Controller
class ContestController(private val technobattleProjectService: TechnobattleProjectService,
                        private val newsService: ProfileNewsService,
                        private val postDAO: PostDAO,
                        private val configurationService: ConfigurationService,
                        private val promoDefaultControllerService: PromoDefaultControllerService) {

    @GetMapping(Urls.Technobattle.CONTEST)
    fun showDefaultContestPage(@RequestParam(required = false) videoId: String?): ModelAndView = showNewsContestPage()

    @GetMapping(Urls.Technobattle.WINNERS)
    fun showWinnersContestPage(): ModelAndView {
        val videos = configurationService.getJsonArrayConfig(TechnobattleVideo::class.java, ConfigurationType.TECHBATTLE_WINNERS_VIDEOS_LIST)
        return promoDefaultControllerService.createDefaultPromoModelAndView(Actions.TECHNO_BATTLE_WINNERS)
                .addObject("videos", videos)
                .addObject("step", "WINNERS")
    }

    @GetMapping(Urls.Technobattle.PARTICIPANTS)
    fun showParticipantsContestPage(): ModelAndView {
        val projects = technobattleProjectService.getProjects(0, 10)
        return promoDefaultControllerService.createDefaultPromoModelAndView(Actions.TECHNO_BATTLE_PARTICIPANTS)
                .addObject("projects", projects)
                .addObject("step", "PARTICIPANTS")
    }

    @GetMapping(Urls.Technobattle.ABOUT)
    fun showAboutContestPage(): ModelAndView =
            promoDefaultControllerService.createDefaultPromoModelAndView(Actions.TECHNO_BATTLE_ABOUT).addObject("step", "ABOUT")

    @GetMapping(Urls.Technobattle.EXPERTS)
    fun showExpertsContestPage(): ModelAndView {
        val mav = promoDefaultControllerService.createDefaultPromoModelAndView(Actions.TECHNO_BATTLE_EXPERTS)
        val experts = configurationService.getJsonArrayConfig(TechnobattleExpert::class.java, ConfigurationType.TECHBATTLE_EXPERTS_LIST)
        if (experts != null && !experts.isEmpty()) {
            mav.addObject("experts", experts)
        }
        return mav.addObject("step", "EXPERTS")
    }

    @GetMapping(Urls.Technobattle.VIDEO)
    fun showVideoContestPage(@RequestParam(required = false) videoId: String?): ModelAndView {
        val mav = promoDefaultControllerService.createDefaultPromoModelAndView(Actions.TECHNO_BATTLE_VIDEO)

        var videos: List<TechnobattleVideo>? = configurationService.getJsonArrayConfig(TechnobattleVideo::class.java, ConfigurationType.TECHBATTLE_VIDEOS_LIST)
        if (videos != null && !videos.isEmpty()) {
            val topVideo = videos[0]
            videos = videos.subList(1, videos.size)
            mav.addObject("topVideo", topVideo)
            mav.addObject("videos", videos)
        }
        mav.addObject("videoId", videoId)
        return mav.addObject("step", "VIDEO")
    }

    @GetMapping(Urls.Technobattle.NEWS)
    fun showNewsContestPage(): ModelAndView {
        val mav = promoDefaultControllerService.createDefaultPromoModelAndView(Actions.TECHNO_BATTLE_NEWS)
        val newsSliderIds = configurationService.promoNewsSliderIds
        val posts = postDAO.selectPosts(newsSliderIds)
        mav.addObject("topNews", posts)
        return mav.addObject("step", "NEWS")
    }

    @GetMapping(Urls.Technobattle.NEWS_SECTION__NEWS_POST)
    fun showNewsContestPage(@PathVariable("postId") postId: Long): ModelAndView {
        val post = newsService.getProfileNewsPost(postId)
        return showNewsContestPage().addObject("post", post)
                .addObject("profileId", post.profileId)
    }

    @GetMapping(Urls.Technobattle.POSTS_BY_PROFILE_ID_POST)
    @ResponseBody
    fun showNewsContestPage(@RequestParam(defaultValue = "0") offset: Int,
                            @RequestParam(defaultValue = "10") limit: Int): ActionStatus<List<Post>> {
        val newsProfileId = configurationService.promoNewsSourceProfileId
        val posts = postDAO.selectPromoPostsByProfileId(newsProfileId, offset, limit)
        return ActionStatus.createSuccessStatus(posts)
    }

    @GetMapping(Urls.Technobattle.PARTNERS)
    fun showPartnersContestPage(): ModelAndView {
        val mav = promoDefaultControllerService.createDefaultPromoModelAndView(Actions.TECHNO_BATTLE_PARTNERS)
        val partners = configurationService.getJsonArrayConfig(TechnobattlePartner::class.java, ConfigurationType.TECHBATTLE_PARTNERS_LIST)
        val specialPartners = ArrayList<TechnobattlePartner>()
        val infoPartners = ArrayList<TechnobattlePartner>()
        for (partner in partners) {
            when (partner.type) {
                TechnobattlePartner.Type.INFO -> infoPartners.add(partner)
                TechnobattlePartner.Type.SPECIAL -> specialPartners.add(partner)
            }
        }

        return mav.addObject("specialPartners", specialPartners)
                .addObject("infoPartners", infoPartners)
                .addObject("step", "PARTNERS")
    }

}

package ru.planeta.promo.model.technobattle

import ru.planeta.model.promo.TechnobattleRegistration

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 17.11.2016
 * Time: 18:06
 */
class TechnobattleUserDTO : TechnobattleRegistration() {
    var isConfirmed: Boolean = false      // only for registration. need on validator only
}

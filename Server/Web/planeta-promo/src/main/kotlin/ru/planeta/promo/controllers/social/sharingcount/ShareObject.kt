package ru.planeta.promo.controllers.social.sharingcount

import ru.planeta.model.promo.VoteType

import java.util.Date

/**
 * Created by asavan on 21.01.2017.
 */
data class ShareObject(var projectId: Long, var voteType: VoteType?)


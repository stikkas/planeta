package ru.planeta.promo.controllers.technobattle

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.helper.ProjectService
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.news.ProfileNewsService
import ru.planeta.api.service.promo.TechnobattleProjectService
import ru.planeta.api.service.promo.VoteService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.promo.TechnobattleProject
import ru.planeta.promo.controllers.Actions
import ru.planeta.promo.controllers.Urls
import ru.planeta.promo.controllers.services.PromoDefaultControllerService
import ru.planeta.promo.model.technobattle.ProjectTab

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 11.01.2017
 * Time: 15:41
 */
@Controller
class ProjectController(private val technobattleProjectService: TechnobattleProjectService,
                        private val voteService: VoteService,
                        private val newsService: ProfileNewsService,
                        private val promoDefaultControllerService: PromoDefaultControllerService,
                        private val permissionService: PermissionService,
                        private val projectService: ProjectService) {

    @GetMapping(Urls.Technobattle.PROJECT_PAGE)
    fun getProjectPage(@PathVariable("projectId") projectId: Long): ModelAndView = getProjectPageTab(projectId, "about")

    @GetMapping(Urls.Technobattle.PROJECT_PAGE_NEWS_POST)
    fun getProjectNewPost(@PathVariable("projectId") projectId: Long,
                          @PathVariable("postId") postId: Long): ModelAndView {
        val post = newsService.getProfileNewsPost(postId)
        return getProjectPageTab(projectId, "news").addObject("post", post)
    }

    @GetMapping(Urls.Technobattle.PROJECT_PAGE_TAB)
    fun getProjectPageTab(@PathVariable("projectId") projectId: Long,
                          @PathVariable("tab") tab: String): ModelAndView {
        if (!ProjectTab.contains(tab.toUpperCase())) {
            throw NotFoundException("Tab '$tab' for TechProject ID: $projectId")
        }

        val technobattleProject = technobattleProjectService.getProject(projectId) ?: throw NotFoundException("Profile for TechProject ID: " + projectId)

        return promoDefaultControllerService.createDefaultPromoModelAndView(Actions.TECHNO_BATTLE_PROJECT_PAGE)
                .addObject("projectId", projectId)
                .addObject("profileId", technobattleProject.profileId)
                .addObject("votesCount", voteService.getVotesCountForProject(projectId))
                .addObject("tab", tab)
                .addObject("project", technobattleProject)
                .addObject("isAdmin", permissionService.isAdmin(myProfileId(), technobattleProject.profileId))
                .addObject("newsAddUrl", projectService.getUrl(ProjectType.MAIN, "" + technobattleProject.profileId + "/news"))
    }

    @GetMapping(ru.planeta.api.web.controllers.Urls.PROFILE_TECH_PROJECT)
    @ResponseBody
    fun getTechProject(@RequestParam projectId: Long): ActionStatus<TechnobattleProject> =
            ActionStatus.createSuccessStatus(technobattleProjectService.getProject(projectId))
}

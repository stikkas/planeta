package ru.planeta.promo.controllers.technobattle

import org.springframework.context.MessageSource
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.model.promo.TechnobattleRegistration
import ru.planeta.promo.controllers.Urls
import ru.planeta.promo.service.TechnobattleUserService

/**
 * User: michail
 * Date: 17.11.2016
 * Time: 16:55
 */

@RestController
class TechnobattleController(private val technobattleUserService: TechnobattleUserService,
                             private val messageSource: MessageSource) {

    @PostMapping(Urls.Technobattle.SUBSCRIBE_EMAIL)
    fun subscribeEmail(@RequestParam email: String): ActionStatus<*> {
        if (!ValidateUtils.isValidEmail(email)) {
            return ActionStatus.createErrorStatus<Any>("wrong.email", messageSource)
        }

        technobattleUserService.addRegistration(TechnobattleRegistration(email, TechnobattleRegistration.Type.SUBSCRIPTION))
        return ActionStatus.createSuccessStatus<Any>()
    }


    @PostMapping(Urls.Technobattle.SEND_LETTER)
    fun sendLetter(@RequestParam email: String): ActionStatus<*> {
        if (!ValidateUtils.isValidEmail(email)) {
            return ActionStatus.createErrorStatus<Any>("wrong.email", messageSource)
        }

        technobattleUserService.addRegistrationWithProgrammLetter(TechnobattleRegistration(email, TechnobattleRegistration.Type.LETTER))
        return ActionStatus.createSuccessStatus<Any>()
    }

}

package ru.planeta.promo.controllers

object Urls {

    const val ROOT = "/"

    private const val TECHNOBATTLE_PREFIX = "/techbattle"
    private const val CAMPUS_PREFIX = "/campus"

    const val SHARING_COUNT = "/api/util/sharing-count.json"

    interface Technobattle {
        companion object {
            const val PROJECT_PAGE = TECHNOBATTLE_PREFIX + "/project/{projectId}"
            const val PROJECT_PAGE_TAB = TECHNOBATTLE_PREFIX + "/project/{projectId}/{tab}"
            const val PROJECT_PAGE_NEWS_POST = TECHNOBATTLE_PREFIX + "/project/{projectId}/news!post{postId}"
            const val POSTS_BY_PROFILE_ID_POST = TECHNOBATTLE_PREFIX + "/promo-news.json"
            const val SEND_LETTER = TECHNOBATTLE_PREFIX + "/send-letter.json"
            const val SUBSCRIBE_EMAIL = TECHNOBATTLE_PREFIX + "/subscribe-email.json"
            const val CONTEST = TECHNOBATTLE_PREFIX
            const val ABOUT = TECHNOBATTLE_PREFIX + "/about"
            const val EXPERTS = TECHNOBATTLE_PREFIX + "/experts"
            const val NEWS = TECHNOBATTLE_PREFIX + "/news"
            const val NEWS_SECTION__NEWS_POST = TECHNOBATTLE_PREFIX + "!post{postId}"
            const val PARTNERS = TECHNOBATTLE_PREFIX + "/partners"
            const val PARTICIPANTS = TECHNOBATTLE_PREFIX + "/participants"
            const val WINNERS = TECHNOBATTLE_PREFIX + "/winners"
            const val VIDEO = TECHNOBATTLE_PREFIX + "/video"
        }
    }

    interface Campus {
        companion object {
            const val WELCOME = CAMPUS_PREFIX
            const val CATEGORY = CAMPUS_PREFIX + "/category/{campusId}"
            const val CATEGORY_CAMAPIGNS = CAMPUS_PREFIX + "/category/campaigns/{tagId}"
            const val TOP_CAMPUS_CAMAPIGNS = CAMPUS_PREFIX + "/top/{tagId}"
        }
    }

    interface HappyBirthday {
        companion object {
            const val FIVE_YEARS = "/chronicles"
        }
    }

}

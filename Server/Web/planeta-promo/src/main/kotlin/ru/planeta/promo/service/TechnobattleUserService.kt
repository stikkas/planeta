package ru.planeta.promo.service

import ru.planeta.model.promo.TechnobattleRegistration

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 18.01.2017
 * Time: 13:13
 */
interface TechnobattleUserService {
    fun addRegistration(registration: TechnobattleRegistration)

    fun addRegistrationAndSendPromoCode(registration: TechnobattleRegistration)

    fun addRegistrationWithProgrammLetter(registration: TechnobattleRegistration)
}

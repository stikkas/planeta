package ru.planeta.promo.model.technobattle

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 18.01.2017
 * Time: 20:40
 */
class TechnobattleExpert {
    var name: String? = null
    var surname: String? = null
    var image: String? = null
    var description: String? = null
}

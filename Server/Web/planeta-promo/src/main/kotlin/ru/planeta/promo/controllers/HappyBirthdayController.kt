package ru.planeta.promo.controllers

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.servlet.ModelAndView
import ru.planeta.promo.controllers.services.PromoDefaultControllerService

@Controller
class HappyBirthdayController(private val promoDefaultControllerService: PromoDefaultControllerService) {

    @GetMapping(Urls.HappyBirthday.FIVE_YEARS)
    fun welcome(): ModelAndView = promoDefaultControllerService.createDefaultPromoModelAndView(Actions.HAPPY_BIRTHDAY_5_YEARS)
}

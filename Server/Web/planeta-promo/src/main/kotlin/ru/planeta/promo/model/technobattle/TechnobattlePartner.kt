package ru.planeta.promo.model.technobattle

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 19.01.2017
 * Time: 18:38
 */
class TechnobattlePartner {

    var type: Type? = null
    var image: String? = null
    var url: String? = null
    var text: String? = null

    enum class Type {
        SPECIAL, INFO
    }
}

package ru.planeta.promo.validation

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.promo.model.technobattle.TechnobattleUserDTO

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 17.11.2016
 * Time: 18:21
 */
@Component
class TechnobattleRegistrationValidator : Validator {
    override fun supports(clazz: Class<*>): Boolean {
        return clazz.isAssignableFrom(TechnobattleUserDTO::class.java)
    }

    override fun validate(o: Any, errors: Errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "field.required")
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "field.required")
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", "field.required")
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "city", "field.required")
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description", "field.required")

        ValidateUtils.rejectIfContainsNotValidString(errors, "name", "wrong.chars")
        ValidateUtils.rejectIfContainsNotValidString(errors, "phone", "wrong.chars")
        ValidateUtils.rejectIfContainsNotValidString(errors, "city", "wrong.chars")
        ValidateUtils.rejectIfContainsNotValidString(errors, "description", "wrong.chars")

        val registration = o as TechnobattleUserDTO
        if (registration.email != null) {
            if (!ValidateUtils.isValidEmail(registration.email)) {
                errors.rejectValue("email", "wrong.email")
            }
        }

        if (!registration.isConfirmed) {
            errors.rejectValue("confirmed", "field.required")
        }
    }
}

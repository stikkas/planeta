package ru.planeta.promo.controllers

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.search.CampaignStatusFilter
import ru.planeta.api.search.SearchCampaigns
import ru.planeta.api.search.SearchService
import ru.planeta.api.search.SortOrder
import ru.planeta.dao.promo.CampusDAO
import ru.planeta.model.common.campaign.Campaign
import ru.planeta.model.common.campaign.CampaignTag
import ru.planeta.model.common.campaign.enums.CampaignStatus
import ru.planeta.promo.controllers.services.PromoDefaultControllerService
import java.util.*

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 17.11.2016
 * Time: 16:55
 */
@Controller
class CampusController(private val campusDAO: CampusDAO,
                       private val searchService: SearchService,
                       private val promoDefaultControllerService: PromoDefaultControllerService) {

    @GetMapping(Urls.Campus.WELCOME)
    fun welcome(): ModelAndView = promoDefaultControllerService
            .createDefaultPromoModelAndView(Actions.CAMPUS)
            .addObject("campuses", campusDAO.selectAll())

    @GetMapping(Urls.Campus.TOP_CAMPUS_CAMAPIGNS)
    @ResponseBody
    fun topCampaigns(@PathVariable("tagId") tagId: Int): List<Campaign> {
        val searchCampaigns = SearchCampaigns(listOf(SortOrder.SORT_BY_SUM), null,
                listOf(CampaignTag(tagId)),
                EnumSet.of(CampaignStatus.ACTIVE, CampaignStatus.FINISHED), 0, 2)
        return searchService.getTopFilteredCampaignsSearchResult(searchCampaigns).searchResultRecords
    }

    @GetMapping(Urls.Campus.CATEGORY)
    fun category(@PathVariable("campusId") campusId: Long): ModelAndView =
            promoDefaultControllerService
                    .createDefaultPromoModelAndView(Actions.CAMPUS_CATEGORY)
                    .addObject("campus", campusDAO.select(campusId))

    @GetMapping(Urls.Campus.CATEGORY_CAMAPIGNS)
    @ResponseBody
    fun campusCampaigns(@PathVariable("tagId") tagId: Int,
                        @RequestParam(required = false) status: CampaignStatusFilter?): List<Campaign> {
        val searchService = searchService
        val searchCampaigns = SearchCampaigns(listOf(SortOrder.SORT_BY_SUM), null,
                listOf(CampaignTag(tagId)), null, 0, 50)
        if (status == null) {
            searchCampaigns.campaignStatus = EnumSet.of(CampaignStatus.ACTIVE, CampaignStatus.FINISHED)
        } else {
            searchCampaigns.status = status
        }
        return searchService.getTopFilteredCampaignsSearchResult(searchCampaigns).searchResultRecords
    }

}


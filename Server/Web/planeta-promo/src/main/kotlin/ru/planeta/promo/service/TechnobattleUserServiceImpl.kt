package ru.planeta.promo.service

import org.apache.log4j.Logger
import org.springframework.stereotype.Service
import ru.planeta.api.service.notifications.NotificationService
import ru.planeta.api.service.promo.TechnobattleRegistrationService
import ru.planeta.dao.trashcan.LitresPromoCodeDAO
import ru.planeta.model.promo.TechnobattleRegistration
import ru.planeta.model.trashcan.LitresPromoCode
import java.util.*

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 18.01.2017
 * Time: 13:14
 */

@Service
class TechnobattleUserServiceImpl(private val notificationService: NotificationService,
                                  private val technobattleRegistrationService: TechnobattleRegistrationService,
                                  private val litresPromoCodeDAO: LitresPromoCodeDAO) : TechnobattleUserService {


    override fun addRegistration(registration: TechnobattleRegistration) {
        technobattleRegistrationService.insertRegistrationRequest(registration)
        notificationService.sendTechnobattleRegistration(registration)
    }

    override fun addRegistrationAndSendPromoCode(registration: TechnobattleRegistration) {
        try {
            val code = litresPromoCodeDAO.selectLastNonUsed()
            code.email = registration.email
            code.voteType = registration.voteType
            code.timeAdded = Date()
            code.status = LitresPromoCode.USED
            litresPromoCodeDAO.usePromoCode(code)
            notificationService.sendPromoCodeLitresLetter(registration.email, code.secretCode)
        } catch (e: Exception) {
            log.warn(e)
        }
    }

    override fun addRegistrationWithProgrammLetter(registration: TechnobattleRegistration) {
        addRegistration(registration)
        notificationService.sendTechnobattleProgrammLetter(registration.email)
    }

    companion object {
        private val log = Logger.getLogger(TechnobattleUserServiceImpl::class.java)
    }

}

#!/bin/sh

OUTPUT=${1:-create.sql}

rm -f ${OUTPUT}

cat maildb.sql >> ${OUTPUT}

for d in Create/Tables Create/Functions Create/Sequences; do
    echo ${d} 1>&2
    pushd ${d} > /dev/null

    for i in *.sql; do
        echo "---------- Start of file: $i -----------"
        cat $i
        echo
        echo "---------- End of file: $i -----------"
    done

    popd > /dev/null
done >> ${OUTPUT}


exit

set output=%1

rem if (%1) == () (
	echo Setting output to create.sql
	set output=create.sql
rem )

del %output%

echo Loading tables
call ech-scripts.bat Create\Tables >> %output%
call ech-scripts.bat Create\Functions >> %output%
call ech-scripts.bat Create\Sequences >> %output%

if errorlevel 1 echo build-create-script was unsuccessful.

echo build-create-script.bat ended work
CREATE TABLE maildb.enum_values (
  enum_value_id INTEGER NOT NULL, 
  attribute_id INTEGER NOT NULL, 
  value TEXT, 
  text_value TEXT, 
  CONSTRAINT enum_values_pkey PRIMARY KEY(enum_value_id)
) WITHOUT OIDS;

COMMENT ON COLUMN maildb.enum_values.enum_value_id
IS 'Multivalue identifier';

COMMENT ON COLUMN maildb.enum_values.attribute_id
IS 'Multivalue attribute identifier';

COMMENT ON COLUMN maildb.enum_values.value
IS 'Multivalue attribute value';

COMMENT ON COLUMN maildb.enum_values.text_value
IS 'MVA value''s text representation';
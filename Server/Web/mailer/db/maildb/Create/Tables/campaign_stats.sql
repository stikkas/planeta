CREATE TABLE maildb.campaign_stats (
  campaign_id BIGINT NOT NULL,
  domain_name VARCHAR(128) DEFAULT '',
  addresses BIGINT,
  sent BIGINT,
  opened BIGINT,
  failed BIGINT,
  unsubscribed BIGINT,
  abuse BIGINT,
  CONSTRAINT campaign_stats_pkey PRIMARY KEY(campaign_id, domain_name)
) WITHOUT OIDS;

COMMENT ON COLUMN maildb.campaign_stats.campaign_id
IS 'Statistics campaign identifier';

COMMENT ON COLUMN maildb.campaign_stats.domain_name
IS 'Statistics email domain name identifier';

COMMENT ON COLUMN maildb.campaign_stats.addresses
IS 'Statistics addresses count';

COMMENT ON COLUMN maildb.campaign_stats.sent
IS 'Statistics sent messages count';

COMMENT ON COLUMN maildb.campaign_stats.opened
IS 'Statistics opened messages count';

COMMENT ON COLUMN maildb.campaign_stats.failed
IS 'Statistics failed messages count';

COMMENT ON COLUMN maildb.campaign_stats.unsubscribed
IS 'Statistics unsubscribed messages count';

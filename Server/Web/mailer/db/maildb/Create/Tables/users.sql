CREATE TABLE maildb.users (
  user_id BIGINT NOT NULL,
  email TEXT,
  display_name TEXT,
  statistics TEXT,
  unsubscribed BOOLEAN DEFAULT false NOT NULL,
  hash TEXT,
  CONSTRAINT users_pkey PRIMARY KEY(user_id)
) WITHOUT OIDS;

COMMENT ON COLUMN maildb.users.user_id
IS 'Statistics user identifier';

COMMENT ON COLUMN maildb.users.email
IS 'Statistics user email';

COMMENT ON COLUMN maildb.users.display_name
IS 'Cached user display name';

COMMENT ON COLUMN maildb.users.statistics
IS 'Some statistics';

COMMENT ON COLUMN maildb.users.unsubscribed
IS 'Unsubscribe flag';

COMMENT ON COLUMN maildb.users.hash
IS 'Unsubscribe hash';
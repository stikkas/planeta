CREATE TABLE maildb.loaded_values (
  email          TEXT NOT NULL,
  filter_list_id BIGINT,
  loaded_params_string TEXT
) WITHOUT OIDS;
CREATE TABLE maildb.filter_lists (
  filter_list_id BIGINT NOT NULL, 
  name TEXT, 
  data BYTEA, 
  CONSTRAINT saved_filters_pkey PRIMARY KEY(filter_list_id)
) WITHOUT OIDS;

COMMENT ON COLUMN maildb.filter_lists.filter_list_id
IS 'Saved filter id';

COMMENT ON COLUMN maildb.filter_lists.name
IS 'Saved filter''s name';

COMMENT ON COLUMN maildb.filter_lists.data
IS 'Saved filter''s data';
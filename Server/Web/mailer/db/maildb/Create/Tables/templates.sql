CREATE TABLE maildb.templates (
  template_id BIGINT, 
  name TEXT,
  text TEXT, 
  PRIMARY KEY(template_id)
) WITHOUT OIDS;

COMMENT ON COLUMN maildb.templates.template_id
IS 'Template Identifier';

COMMENT ON COLUMN maildb.templates.name
IS 'Template''s name';

COMMENT ON COLUMN maildb.templates.text
IS 'Template as text';
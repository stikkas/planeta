CREATE TABLE maildb.campaigns (
  campaign_id BIGINT NOT NULL,
  name TEXT,
  template_id BIGINT,
  filter_list_id BIGINT,
  text TEXT,
  message_from TEXT,
  subject TEXT,
  date_confirmed TIMESTAMP WITHOUT TIME ZONE,
  CONSTRAINT campaigns_pkey PRIMARY KEY(campaign_id)
) WITHOUT OIDS;

COMMENT ON COLUMN maildb.campaigns.campaign_id
IS 'Mail campaign''s identifier';

COMMENT ON COLUMN maildb.campaigns.name
IS 'Mail campaign''s name';

COMMENT ON COLUMN maildb.campaigns.template_id
IS 'Mail campaign''s template';

COMMENT ON COLUMN maildb.campaigns.filter_list_id
IS 'Mail campaign''s filter list';

COMMENT ON COLUMN maildb.campaigns.text
IS 'Mail campaign''s text to send';

COMMENT ON COLUMN maildb.campaigns.message_from
IS 'Mail campaign''s From field';

COMMENT ON COLUMN maildb.campaigns.subject
IS 'Mail campaign''s Subject field';

COMMENT ON COLUMN maildb.campaigns.date_confirmed
IS 'Mail campaign''s confirm date';
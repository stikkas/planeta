CREATE TABLE maildb.attributes (
  attribute_id INTEGER NOT NULL, 
  name TEXT, 
  type INTEGER, 
  text_name TEXT, 
  CONSTRAINT attributes_pkey PRIMARY KEY(attribute_id)
) WITHOUT OIDS;

COMMENT ON COLUMN maildb.attributes.attribute_id
IS 'Attribute identifier';

COMMENT ON COLUMN maildb.attributes.name
IS 'Attribute name';

COMMENT ON COLUMN maildb.attributes.type
IS 'Attribute type';

COMMENT ON COLUMN maildb.attributes.text_name
IS 'Attribute display name';

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1001, E'email', 2, E'Электронная почта');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1002, E'display_name', 2, E'Имя пользователя');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1003, E'last_active', 3, E'Время последней активности');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1004, E'country', 4, E'Страна');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1005, E'city', 4, E'Город');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1006, E'user_birth_date', 3, E'Дата рождения');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1007, E'balance', 1, E'Баланс');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1008, E'groups', 5, E'Сообщества пользователя');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1009, E'share_last_order', 3, E'Дата последней покупки акции');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1010, E'share_max_price', 1, E'Максимальная сумма покупки акции');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1011, E'share_count', 1, E'Количество покупок акций');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1012, E'campaign_category_types', 5, E'Категории проектов');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1013, E'campaign_ids', 5, E'Проекты');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1014, E'share_ids', 5, E'Купленные акции');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1015, E'days_registered', 1, E'Количество дней с момента регистрации');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1016, E'registration_source', 4, E'Источник регистрации');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1017, E'region', 4, E'Регион');
CREATE TABLE maildb.messages (
  mailer_message_id BIGINT NOT NULL,
  message_id TEXT,
  user_id BIGINT,
  campaign_id BIGINT,
  date_sent TIMESTAMP WITHOUT TIME ZONE,
  is_open BOOLEAN,
  is_failed BOOLEAN,
  is_unsubscribed BOOLEAN DEFAULT false,
  is_abuse BOOLEAN DEFAULT false,
  CONSTRAINT messages_pkey PRIMARY KEY(mailer_message_id)
) WITHOUT OIDS;

CREATE UNIQUE INDEX messages_idx ON maildb.messages (user_id, campaign_id);

COMMENT ON COLUMN maildb.messages.mailer_message_id
IS 'Mailer message identifier';

COMMENT ON COLUMN maildb.messages.message_id
IS 'Sent mail identifier';

COMMENT ON COLUMN maildb.messages.user_id
IS 'Statistics list user identifier';

COMMENT ON COLUMN maildb.messages.campaign_id
IS 'Campaign_identifier';

COMMENT ON COLUMN maildb.messages.date_sent
IS 'When a message sent';

COMMENT ON COLUMN maildb.messages.is_open
IS 'Is message opened by user';

COMMENT ON COLUMN maildb.messages.is_failed
IS 'Is message deliver failed';

COMMENT ON COLUMN maildb.messages.is_unsubscribed
IS 'Is user unsubscribed from link in this message';
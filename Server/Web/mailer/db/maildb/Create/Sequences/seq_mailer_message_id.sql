CREATE SEQUENCE maildb.seq_mailer_message_id
INCREMENT 1 MINVALUE 1
START 1;
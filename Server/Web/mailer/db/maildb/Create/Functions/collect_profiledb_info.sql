CREATE OR REPLACE FUNCTION maildb.collect_profiledb_info (
  out profile_id bigint,
  out city_id bigint,
  out region_id bigint,
  out country_id bigint,
  out user_birth_date timestamp,
  out display_name varchar
)
RETURNS SETOF record AS
$body$
CLUSTER 'planeta';
RUN ON ALL;
SELECT t1.profile_id,
	   t1.city_id,
	   COALESCE(t5.region_id,0) AS region_id,
	   t1.country_id,
       t1.user_birth_date,
       t1.display_name
  FROM profiledb.profiles t1
  JOIN profiledb.users t4
    ON t4.profile_id = t1.profile_id
  LEFT JOIN profiledb.city t5
    ON t1.city_id = t5.city_id;
$body$
LANGUAGE 'plproxy'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;

CREATE OR REPLACE FUNCTION maildb.collect_user_relations (
  out user_id bigint,
  out group_id bigint
)
RETURNS SETOF record AS
$body$
CLUSTER 'planeta';
RUN ON ALL;
SELECT profile.profile_id as user_id, grp.profile_id as group_id
      FROM profiledb.profiles profile
      JOIN profiledb.profile_relations relation ON profile.subject_profile_id = relation.profile_id AND (relation.status & 16) > 0
      JOIN profiledb.profiles grp ON relation.profile_id = grp.profile_id and grp.profile_type_id = 2
     WHERE profile.profile_type_id = 1
     ORDER BY profile.profile_id;
$body$
LANGUAGE 'plproxy'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;

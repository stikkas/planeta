CREATE OR REPLACE FUNCTION maildb.get_countries (
  out country_id integer,
  out country_name_ru text
)
RETURNS SETOF record AS
$body$
CLUSTER 'planeta';
SELECT country_id, country_name_ru from profiledb.country;
$body$
LANGUAGE 'plproxy'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;

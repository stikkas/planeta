CREATE OR REPLACE FUNCTION maildb.get_groups (
  out profile_id integer,
  out display_name text
)
RETURNS SETOF record AS
$body$
CLUSTER 'planeta';
SELECT profile_id, display_name
FROM profiledb.profiles
WHERE profile_type_id = 2
ORDER BY profile_id;
$body$
LANGUAGE 'plproxy'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;

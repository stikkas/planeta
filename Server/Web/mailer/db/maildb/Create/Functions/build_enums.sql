CREATE OR REPLACE FUNCTION maildb.build_enums ()
RETURNS void AS
$body$
  DECLARE
    attr RECORD;
    rec RECORD;
  BEGIN
    DELETE FROM maildb.enum_values;
    ALTER SEQUENCE maildb.seq_enum_value_id RESTART WITH 1;
    FOR attr IN (
      SELECT *
      FROM maildb.attributes
    ) LOOP
    IF attr.name = 'city' THEN
	  FOR rec IN (
		SELECT *
		FROM maildb.get_cities()
	  ) LOOP
	  INSERT INTO maildb.enum_values (
		enum_value_id,
		attribute_id,
		"value",
		text_value
	  ) VALUES (
		nextval('maildb.seq_enum_value_id'),
		attr.attribute_id,
		text(rec.city_id),
		rec.city_name_ru
	  );
      END LOOP;
	ELSIF attr.name = 'region' THEN
	  FOR rec IN (
		SELECT *
		FROM maildb.get_regions()
	  ) LOOP
	  INSERT INTO maildb.enum_values (
		enum_value_id,
		attribute_id,
		"value",
		text_value
	  ) VALUES (
		nextval('maildb.seq_enum_value_id'),
		attr.attribute_id,
		text(rec.region_id),
		rec.region_name_ru
	  );
      END LOOP;
    ELSIF attr.name = 'country' THEN
      FOR rec IN (
        SELECT *
        FROM maildb.get_countries()
      ) LOOP
      INSERT INTO maildb.enum_values (
        enum_value_id,
        attribute_id,
        "value",
        text_value
      ) VALUES (
        nextval('maildb.seq_enum_value_id'),
        attr.attribute_id,
        text(rec.country_id),
        rec.country_name_ru
      );
      END LOOP;
    ELSIF attr.name = 'campaign_ids' THEN
      FOR rec IN (
        SELECT t1.campaign_id,
          concat(case when t2.display_name != '' then t2.display_name else '<Без названия>' end, ' / ', case when t1.name != '' then t1.name else '<Без названия>' end) as name
        FROM commondb.campaigns t1
          LEFT JOIN maildb.get_groups() t2
            ON t1.profile_id = t2.profile_id
      ) LOOP
      INSERT INTO maildb.enum_values (
        enum_value_id,
        attribute_id,
        "value",
        text_value
      ) VALUES (
        nextval('maildb.seq_enum_value_id'),
        attr.attribute_id,
        text(rec.campaign_id),
        rec.name
      );
      END LOOP;
    ELSIF attr.name = 'share_ids' THEN
      FOR rec IN (
        SELECT t1.share_id,
          concat(case when t3.display_name != '' then t3.display_name else '<Без названия>' end, ' / ', case when t2.name != '' then t2.name else '<Без названия>' end, ' / ', t1.name) as name
        FROM commondb.shares t1
          LEFT JOIN commondb.campaigns t2
            ON t1.campaign_id = t2.campaign_id
          LEFT JOIN maildb.get_groups() t3
            ON t1.profile_id = t3.profile_id
      ) LOOP
      INSERT INTO maildb.enum_values (
        enum_value_id,
        attribute_id,
        "value",
        text_value
      ) VALUES (
        nextval('maildb.seq_enum_value_id'),
        attr.attribute_id,
        text(rec.share_id),
        rec.name
      );
      END LOOP;
    ELSIF attr.name = 'campaign_category_types' THEN
      FOR rec IN (
        SELECT *
        FROM commondb.campaign_tags ct
      ) LOOP
      INSERT INTO maildb.enum_values (
        enum_value_id,
        attribute_id,
        "value",
        text_value
      ) VALUES (
        nextval('maildb.seq_enum_value_id'),
        attr.attribute_id,
        text(rec.tag_id),
        concat(rec.tag_name)
      );
      END LOOP;
    ELSIF attr.name = 'groups' THEN
      FOR rec IN (
        SELECT *
        FROM maildb.get_groups()
      ) LOOP
      INSERT INTO maildb.enum_values (
        enum_value_id,
        attribute_id,
        "value",
        text_value
      ) VALUES (
        nextval('maildb.seq_enum_value_id'),
        attr.attribute_id,
        text(rec.profile_id),
        concat(rec.display_name)
      );
      END LOOP;
    ELSIF attr.name = 'registration_source' THEN
      INSERT INTO maildb.enum_values (
        enum_value_id,
        attribute_id,
        "value",
        text_value
      ) VALUES (
                 nextval('maildb.seq_enum_value_id'),
                 attr.attribute_id,
                 '1',
                 'Вконтакте'
               ), (
                 nextval('maildb.seq_enum_value_id'),
                 attr.attribute_id,
                 '2',
                 'Поиск'
               ), (
                 nextval('maildb.seq_enum_value_id'),
                 attr.attribute_id,
                 '3',
                 'Facebook'
               ), (
                 nextval('maildb.seq_enum_value_id'),
                 attr.attribute_id,
                 '4',
                 'Другой сайт'
               ), (
                 nextval('maildb.seq_enum_value_id'),
                 attr.attribute_id,
                 '5',
                 'Прямые'
               ), (
                 nextval('maildb.seq_enum_value_id'),
                 attr.attribute_id,
                 '6',
                 'Офферная реклама VK'
               ), (
                 nextval('maildb.seq_enum_value_id'),
                 attr.attribute_id,
                 '7',
                 'MixUni'
               );
    END IF;
    END LOOP;
  END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
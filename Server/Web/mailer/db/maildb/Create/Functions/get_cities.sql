CREATE OR REPLACE FUNCTION maildb.get_cities (
  out city_id text,
  out city_name_ru text
)
RETURNS SETOF record AS
$body$
CLUSTER 'planeta';
SELECT t1.city_id, concat(country_name_ru, ', ', region_name_ru, ', ', city_name_ru) as city_name_ru
FROM profiledb.city t1
LEFT JOIN profiledb.region t2
ON t1.region_id = t2.region_id
LEFT JOIN profiledb.country t3
ON t1.country_id = t3.country_id;
$body$
LANGUAGE 'plproxy'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;

CREATE OR REPLACE FUNCTION maildb.sources (
  out profile_id bigint,
  out registration_source text
)
RETURNS SETOF record AS
$body$
select profile_id,
(case
when enter_url ~ 'utm_source=mixuni' then '7'
when referrer_url ~ 'vk.com' and enter_url !~ 'offers_adv' then '1'
when referrer_url ~ 'vkontakte.ru' and enter_url !~ 'offers_adv' then '1'
when referrer_url ~ 'vk.com' and enter_url ~ 'offers_adv' then '6'
when referrer_url ~ 'vkontakte.ru' and enter_url ~ 'offers_adv' then '6'
when referrer_url ~ 'google.com' then '2'
when referrer_url ~ 'yandex.com' then '2'
when referrer_url ~ 'go.mail.ru' then '2'
when referrer_url ~ 'mail.ru' then '2'
when referrer_url ~ 'facebook.com' then '3'
when referrer_url = '' then '4'
when referrer_url = NULL then '4'
when referrer_url ~ 'planeta.ru' then '3'
else '5' end) as registration_source
from commondb.user_sources
$body$
LANGUAGE 'sql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;

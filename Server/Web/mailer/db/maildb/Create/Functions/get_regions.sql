CREATE OR REPLACE FUNCTION maildb.get_regions (
  out region_id text,
  out region_name_ru text
)
RETURNS SETOF record AS
$body$
CLUSTER 'planeta';
SELECT t1.region_id, concat(t2.country_name_ru, ', ', t1.region_name_ru) as region_name_ru
  FROM profiledb.region t1
  JOIN profiledb.country t2
    ON t1.country_id = t2.country_id;
$body$
LANGUAGE 'plproxy'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;
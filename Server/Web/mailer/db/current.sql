DELETE FROM maildb.attributes;

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1001, E'email', 2, E'Электронная почта');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1002, E'display_name', 2, E'Имя пользователя');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1003, E'last_active', 3, E'Время последней активности');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1004, E'country', 4, E'Страна');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1005, E'city', 4, E'Город');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1006, E'user_birth_date', 3, E'Дата рождения');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1007, E'balance', 1, E'Баланс');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1008, E'groups', 5, E'Сообщества пользователя');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1009, E'share_last_order', 3, E'Дата последней покупки акции');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1010, E'share_max_price', 1, E'Максимальная сумма покупки акции');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1011, E'share_count', 1, E'Количество покупок акций');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1012, E'campaign_category_types', 5, E'Категории проектов');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1013, E'campaign_ids', 5, E'Проекты');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1014, E'share_ids', 5, E'Купленные акции');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1015, E'days_registered', 1, E'Количество дней с момента регистрации');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1016, E'registration_source', 4, E'Источник регистрации');

INSERT INTO maildb.attributes ("attribute_id", "name", "type", "text_name")
VALUES (1017, E'region', 4, E'Регион');

DROP FUNCTION maildb.collect_profiledb_info();

CREATE OR REPLACE FUNCTION maildb.collect_profiledb_info (
  out profile_id bigint,
  out city_id bigint,
  out region_id bigint,
  out country_id bigint,
  out user_birth_date timestamp,
  out display_name varchar
)
RETURNS SETOF record AS
$body$
CLUSTER 'planeta';
RUN ON ALL;
SELECT t1.profile_id,
	   t1.city_id,
	   COALESCE(t5.region_id,0) AS region_id,
	   t1.country_id,
       t1.user_birth_date,
       t1.display_name
  FROM profiledb.profiles t1
  JOIN profiledb.users t4
    ON t4.profile_id = t1.profile_id
  LEFT JOIN profiledb.city t5
    ON t1.city_id = t5.city_id;
$body$
LANGUAGE 'plproxy'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;

DROP TABLE maildb."values";

CREATE TABLE maildb."values" (
  email TEXT NOT NULL, 
  display_name TEXT, 
  last_active TIMESTAMP(0) WITHOUT TIME ZONE, 
  country TEXT, 
  region TEXT,
  city TEXT, 
  user_birth_date TIMESTAMP(0) WITHOUT TIME ZONE, 
  balance NUMERIC, 
  groups TEXT[], 
  share_last_order TIMESTAMP WITHOUT TIME ZONE, 
  share_max_price NUMERIC, 
  share_count BIGINT, 
  campaign_category_types TEXT[], 
  campaign_ids TEXT[], 
  share_ids TEXT[], 
  days_registered BIGINT, 
  registration_source TEXT, 
  CONSTRAINT values_pkey PRIMARY KEY(email)
) WITHOUT OIDS;


CREATE OR REPLACE FUNCTION maildb.collect_info (
)
RETURNS void AS
$body$
  DECLARE
    rec RECORD;
    rec2 RECORD;
  BEGIN
    FOR rec IN (
      SELECT t1.profile_id,
			 t1.city_id,
			 t1.region_id,
			 t1.country_id,
			 t1.user_birth_date,
			 t1.display_name,
			 t2.email,
			 t4.balance,
			 t3.email IS NOT NULL AS current_value_exists,
			 date_part('days', now() - t2.time_added) as days_registered
        FROM maildb.collect_profiledb_info() AS t1
   LEFT JOIN commondb.users_private_info AS t2
          ON t2.user_id = t1.profile_id
   LEFT JOIN maildb.values AS t3
          ON t3.email = t2.email
   LEFT JOIN commondb.profile_balances AS t4
          ON t4.profile_id = t1.profile_id
       WHERE t2.email IS NOT NULL
         AND t2.email != ''
    ) LOOP
		IF (rec.current_value_exists = FALSE) THEN
		  INSERT INTO maildb.values (
			email,
			display_name,
			last_active,
			country,
			region,
			city,
			user_birth_date,
			balance,
			days_registered
		  ) VALUES (
			rec.email,
			rec.display_name,
			now(),
			rec.country_id,
			rec.region_id,
			rec.city_id,
			rec.user_birth_date,
			rec.balance,
			rec.days_registered
		  );
		ELSE
		  UPDATE maildb."values"
		  SET display_name = rec.display_name,
			last_active = now(),
			country = rec.country_id,
			city = rec.city_id,
			region = rec.region_id,
			user_birth_date = rec.user_birth_date,
			balance = rec.balance,
			groups = NULL,
			share_last_order = NULL,
			share_max_price = NULL,
			share_count = NULL,
			campaign_category_types = NULL,
			campaign_ids = NULL,
			share_ids = NULL,
			days_registered = rec.days_registered
		  WHERE email = rec.email;
		END IF;

		UPDATE maildb."values" t1
		   SET share_last_order = t6.share_last_order,
			   share_max_price = t6.share_max_price,
			   share_count = t6.share_count
		  FROM (SELECT t3.buyer_id,
					   max(t3.time_added) as share_last_order,
					   max(t4.price) as share_max_price,
					   count(t4.price) as share_count
				  FROM commondb.orders t3
			 LEFT JOIN commondb.order_objects t4
					ON t3.order_id = t4.order_id
				 WHERE t3.buyer_id = rec.profile_id
				   AND t4.order_object_type = 1
				   AND t3.payment_status = 1
			  GROUP BY buyer_id) AS t6
		 WHERE t1.email = rec.email;

		UPDATE maildb."values" t1
		SET share_ids = ARRAY(
			SELECT t5.share_id
			  FROM commondb.orders t3
		 LEFT JOIN commondb.order_objects t4
				ON t3.order_id = t4.order_id
			  JOIN commondb.shares t5
				ON t5.share_id = t4.object_id
			 WHERE t3.buyer_id = rec.profile_id
			   AND t3.payment_status = 1)
		WHERE t1.email = rec.email;

		UPDATE maildb."values" t1
		   SET campaign_category_types = ARRAY(
			SELECT t6.category_type
			  FROM commondb.shares t5
			  JOIN commondb.campaigns t6
				ON t6.campaign_id = t5.campaign_id
			 WHERE text(t5.share_id) = ANY(t1.share_ids)),
		  campaign_ids = ARRAY(
			SELECT t6.campaign_id
			  FROM commondb.shares t5
			  JOIN commondb.campaigns t6
				ON t6.campaign_id = t5.campaign_id
			 WHERE text(t5.share_id) = ANY(t1.share_ids))
		 WHERE t1.email = rec.email;
    END LOOP;

    FOR rec IN (SELECT t1.user_id,
					   array_agg(t1.group_id) as groups,
					   t2.email
				  FROM maildb.collect_user_relations() t1
			 LEFT JOIN commondb.users_private_info AS t2
				    ON t2.user_id = t1.user_id
			  GROUP BY t1.user_id, t2.email
    ) LOOP

		UPDATE maildb."values" t1
   		   SET groups = rec.groups
		 WHERE t1.email = rec.email;

    END LOOP;

    UPDATE maildb."values" t1
       SET registration_source = 5;
    
	FOR rec IN (SELECT t1.profile_id, t1.registration_source, t2.email
				  FROM maildb.sources() t1
			 LEFT JOIN commondb.users_private_info AS t2
				    ON t2.user_id = t1.profile_id
    ) LOOP

		UPDATE maildb."values" t1
		   SET registration_source = rec.registration_source
		 WHERE t1.email = rec.email;

    END LOOP;
  END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

CREATE OR REPLACE FUNCTION maildb.get_regions (
  out region_id text,
  out region_name_ru text
)
RETURNS SETOF record AS
$body$
CLUSTER 'planeta';
SELECT t1.region_id, concat(t2.country_name_ru, ', ', t1.region_name_ru) as region_name_ru
  FROM profiledb.region t1
  JOIN profiledb.country t2
    ON t1.country_id = t2.country_id;
$body$
LANGUAGE 'plproxy'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;

CREATE OR REPLACE FUNCTION maildb.build_enums ()
RETURNS void AS
$body$
  DECLARE
    attr RECORD;
    rec RECORD;
  BEGIN
    DELETE FROM maildb.enum_values;
    ALTER SEQUENCE maildb.seq_enum_value_id RESTART WITH 1;
    FOR attr IN (
      SELECT *
      FROM maildb.attributes
    ) LOOP
    IF attr.name = 'city' THEN
	  FOR rec IN (
		SELECT *
		FROM maildb.get_cities()
	  ) LOOP
	  INSERT INTO maildb.enum_values (
		enum_value_id,
		attribute_id,
		"value",
		text_value
	  ) VALUES (
		nextval('maildb.seq_enum_value_id'),
		attr.attribute_id,
		text(rec.city_id),
		rec.city_name_ru
	  );
      END LOOP;
	ELSIF attr.name = 'region' THEN
	  FOR rec IN (
		SELECT *
		FROM maildb.get_regions()
	  ) LOOP
	  INSERT INTO maildb.enum_values (
		enum_value_id,
		attribute_id,
		"value",
		text_value
	  ) VALUES (
		nextval('maildb.seq_enum_value_id'),
		attr.attribute_id,
		text(rec.region_id),
		rec.region_name_ru
	  );
      END LOOP;
    ELSIF attr.name = 'country' THEN
      FOR rec IN (
        SELECT *
        FROM maildb.get_countries()
      ) LOOP
      INSERT INTO maildb.enum_values (
        enum_value_id,
        attribute_id,
        "value",
        text_value
      ) VALUES (
        nextval('maildb.seq_enum_value_id'),
        attr.attribute_id,
        text(rec.country_id),
        rec.country_name_ru
      );
      END LOOP;
    ELSIF attr.name = 'campaign_ids' THEN
      FOR rec IN (
        SELECT t1.campaign_id,
          concat(case when t2.display_name != '' then t2.display_name else '<Без названия>' end, ' / ', case when t1.name != '' then t1.name else '<Без названия>' end) as name
        FROM commondb.campaigns t1
          LEFT JOIN maildb.get_groups() t2
            ON t1.profile_id = t2.profile_id
      ) LOOP
      INSERT INTO maildb.enum_values (
        enum_value_id,
        attribute_id,
        "value",
        text_value
      ) VALUES (
        nextval('maildb.seq_enum_value_id'),
        attr.attribute_id,
        text(rec.campaign_id),
        rec.name
      );
      END LOOP;
    ELSIF attr.name = 'share_ids' THEN
      FOR rec IN (
        SELECT t1.share_id,
          concat(case when t3.display_name != '' then t3.display_name else '<Без названия>' end, ' / ', case when t2.name != '' then t2.name else '<Без названия>' end, ' / ', t1.name) as name
        FROM commondb.shares t1
          LEFT JOIN commondb.campaigns t2
            ON t1.campaign_id = t2.campaign_id
          LEFT JOIN maildb.get_groups() t3
            ON t1.profile_id = t3.profile_id
      ) LOOP
      INSERT INTO maildb.enum_values (
        enum_value_id,
        attribute_id,
        "value",
        text_value
      ) VALUES (
        nextval('maildb.seq_enum_value_id'),
        attr.attribute_id,
        text(rec.share_id),
        rec.name
      );
      END LOOP;
    ELSIF attr.name = 'campaign_category_types' THEN
      FOR rec IN (
        SELECT *
        FROM maildb.campaign_category_types
      ) LOOP
      INSERT INTO maildb.enum_values (
        enum_value_id,
        attribute_id,
        "value",
        text_value
      ) VALUES (
        nextval('maildb.seq_enum_value_id'),
        attr.attribute_id,
        text(rec.value),
        concat(rec.name)
      );
      END LOOP;
    ELSIF attr.name = 'groups' THEN
      FOR rec IN (
        SELECT *
        FROM maildb.get_groups()
      ) LOOP
      INSERT INTO maildb.enum_values (
        enum_value_id,
        attribute_id,
        "value",
        text_value
      ) VALUES (
        nextval('maildb.seq_enum_value_id'),
        attr.attribute_id,
        text(rec.profile_id),
        concat(rec.display_name)
      );
      END LOOP;
    ELSIF attr.name = 'registration_source' THEN
      INSERT INTO maildb.enum_values (
        enum_value_id,
        attribute_id,
        "value",
        text_value
      ) VALUES (
                 nextval('maildb.seq_enum_value_id'),
                 attr.attribute_id,
                 '1',
                 'Вконтакте'
               ), (
                 nextval('maildb.seq_enum_value_id'),
                 attr.attribute_id,
                 '2',
                 'Поиск'
               ), (
                 nextval('maildb.seq_enum_value_id'),
                 attr.attribute_id,
                 '3',
                 'Facebook'
               ), (
                 nextval('maildb.seq_enum_value_id'),
                 attr.attribute_id,
                 '4',
                 'Другой сайт'
               ), (
                 nextval('maildb.seq_enum_value_id'),
                 attr.attribute_id,
                 '5',
                 'Прямые'
               ), (
                 nextval('maildb.seq_enum_value_id'),
                 attr.attribute_id,
                 '6',
                 'Офферная реклама VK'
               ), (
                 nextval('maildb.seq_enum_value_id'),
                 attr.attribute_id,
                 '7',
                 'MixUni'
               );
    END IF;
    END LOOP;
  END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

---------- Start of file: collect_user_relations.sql -----------
CREATE OR REPLACE FUNCTION maildb.collect_user_relations (
  out user_id bigint,
  out group_id bigint
)
RETURNS SETOF record AS
$body$
CLUSTER 'planeta';
RUN ON ALL;
SELECT
t1.profile_id AS user_id,
t6.profile_id AS group_id
FROM profiledb.profiles t1
LEFT JOIN profiledb.profile_relations t5
ON t1.profile_id = t5.subject_profile_id AND (t5.status & 16) > 0
JOIN profiledb.profiles t6
ON t5.profile_id = t6.profile_id
WHERE t1.profile_type_id = 1  AND (t6.profile_type_id IS NULL OR t6.profile_type_id = 2)
ORDER BY t1.profile_id;
$body$
LANGUAGE 'plproxy'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;

---------- End of file: collect_user_relations.sql -----------

CREATE TABLE maildb.loaded_values (
  email          TEXT NOT NULL,
  filter_list_id BIGINT,
  loaded_params_string TEXT
) WITHOUT OIDS;

SELECT maildb.build_enums();
SELECT maildb.collect_info();

SELECT grant_to_all_tables('maildb', 'SELECT', 'planeta');
SELECT grant_to_all_tables('maildb', 'INSERT', 'planeta');
SELECT grant_to_all_tables('maildb', 'UPDATE', 'planeta');
SELECT grant_to_all_tables('maildb', 'DELETE', 'planeta');
SELECT grant_to_all_functions('maildb', 'EXECUTE', 'planeta');


INSERT INTO maildb.campaign_category_types (value, name, short_name)
VALUES
  (35, E'Спорт', E'SPORT');

INSERT INTO commondb.campaign_category_types (campaign_category_type, category_value, category_name)
    VALUES (35, E'SPORT', E'Спорт');

UPDATE maildb.values
    SET share_count = 0
WHERE share_count IS NULL;

ALTER TABLE maildb.values ALTER COLUMN share_count SET DEFAULT 0;
ALTER TABLE maildb.values ALTER COLUMN share_count SET NOT NULL;
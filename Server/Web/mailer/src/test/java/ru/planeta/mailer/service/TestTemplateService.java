package ru.planeta.mailer.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.model.generated.Template;
import ru.planeta.mailer.test.GenericSpringTest;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class TestTemplateService extends GenericSpringTest {

    @Autowired
    TemplateService templateService;

    @Test
    public void filterServiceTest() throws NotFoundException {

        Template template = new Template();

        template.setName("cool template");
        template.setText("<b>test</b>");

        templateService.saveTemplate(template);
        assertNotNull(template.getTemplateId());

        List templates = templateService.getTemplates();
        long size = templates.size();
        assertTrue(templates.size() > 0);

        templateService.saveTemplate(template);

        templateService.deleteTemplate(template.getTemplateId());

        templates = templateService.getTemplates();
        assertEquals(templates.size(), size - 1);
    }
}

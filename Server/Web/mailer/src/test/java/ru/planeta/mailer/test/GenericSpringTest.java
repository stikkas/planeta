package ru.planeta.mailer.test;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import java.io.File;
import java.net.URISyntaxException;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 25.07.12
 */
@ContextConfiguration(locations = {"/spring/applicationContext-*.xml", "classpath*:/spring/applicationContext-dao.xml", "classpath*:/spring/applicationContext-aop.xml", "classpath*:/spring/applicationContext-geo.xml"})
public abstract class GenericSpringTest extends AbstractTransactionalJUnit4SpringContextTests {

	public static File getFileFromClasspath(String path) throws URISyntaxException {
		return new File(ClassLoader.getSystemClassLoader().getResource(path).toURI());
	}
}

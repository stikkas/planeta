package ru.planeta.mailer.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.model.custom.Value;
import ru.planeta.mailer.model.generated.User;
import ru.planeta.mailer.test.GenericSpringTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TestUserService extends GenericSpringTest {

    @Autowired
    private MailerUserService mailerUserService;

    @Test
    public void filterServiceTest() throws NotFoundException {
        Value value = new Value();
        value.setEmail("test@mail.ru");
        value.setDisplayName("tester");

        User user1 = mailerUserService.getUserByValue(value);
        assertNotNull(user1.getUserId());

        user1.setDisplayName("tester2");
        mailerUserService.saveUser(user1);

        User user2 = mailerUserService.getUserByValue(value);
        assertNotNull(user2.getUserId());
        assertEquals(user1.getUserId(), user2.getUserId());
        assertEquals(user1.getEmail(), user2.getEmail());
        assertEquals(user1.getDisplayName(), user2.getDisplayName());

        User user3 = mailerUserService.getUserByEmail(value.getEmail());
        assertEquals(user3.getUserId(), user1.getUserId());
    }
}

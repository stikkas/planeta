package ru.planeta.mailer.service;

import org.apache.commons.io.FileUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.model.custom.AbstractValue;
import ru.planeta.mailer.model.custom.Filter;
import ru.planeta.mailer.model.custom.FilterList;
import ru.planeta.mailer.model.custom.LoadedValue;
import ru.planeta.mailer.model.enums.Predicate;
import ru.planeta.mailer.model.generated.Attribute;
import ru.planeta.mailer.test.GenericSpringTest;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class TestFilterService extends GenericSpringTest {

    @Autowired
    FilterService filterService;

	private static final int SEND_LIMIT = 5000;
	private static final int MAX_ITERATIONS = 1000;

    @Test
    public void filterServiceTest() throws NotFoundException {
        List<Attribute> attributes = filterService.getAttributes();

        if (attributes == null || attributes.size() == 0) {
            return;
        }

        FilterList filterList = new FilterList();
        List<Filter> filters = new ArrayList<Filter>();
        Filter filter = new Filter();

        filter.setFilteredAttribute(attributes.get(0));
        filter.setPredicate(Predicate.EQUALS);
        filter.setObject("test");
        filters.add(filter);
        filterList.setFilters(filters);

        filterService.saveList(filterList);
        assertNotNull(filterList.getFilterListId());

        FilterList newList = filterService.getFilterListById(filterList.getFilterListId());
        assertNotNull(newList.getFilters());
        assertEquals(filterList.getFilters().size(), newList.getFilters().size());
    }

	@Test
	public void testFilterListFromFile() throws URISyntaxException, IOException, NotFoundException, InterruptedException {
		File dstFile = null;

		try {
			File testFile = getFileFromClasspath("shares.csv");
			dstFile = new File("testfile.csv");
			FileUtils.copyFile(testFile, dstFile);

			List<String[]> inputList = filterService.parseDataFromCSVFile(dstFile);
			assertNotNull(inputList);
			assertEquals(4, inputList.size());

			String[] row = inputList.get(2);
			assertEquals("testuseremail3@test.ee", row[0]);
			assertEquals("param31", row[1]);
			assertEquals("param32", row[2]);
			assertEquals("param33", row[3]);

			FilterList filterList = filterService.createListFromCSVFile(dstFile);
			assertNotNull(filterList);
			assertTrue(filterList.getFilterListId() > 0);

			filterList = filterService.getFilterListById(filterList.getFilterListId());
			assertNotNull(filterList);
			assertTrue(filterList.getFilterListId() > 0);

			Thread.sleep(5000);

			List<LoadedValue> values = (List<LoadedValue>) filterService.getValues(filterList, 0, 100);
			assertNotNull(values);
			assertEquals(inputList.size(), values.size());
			assertEquals(inputList.get(0)[0], values.get(0).getEmail());
			assertEquals(inputList.get(0)[0], values.get(0).getParams()[0]);
			assertEquals(inputList.get(0)[1], values.get(0).getParams()[1]);
			assertEquals(inputList.get(0)[2], values.get(0).getParams()[2]);
			assertEquals(inputList.get(0)[3], values.get(0).getParams()[3]);

			long count = filterService.countValues(filterList, null, null, null);
			assertEquals(inputList.size(), count);

		} finally {
			FileUtils.deleteQuietly(dstFile);
		}
	}

	@Test
	@Ignore
	public void testMailerAll() throws NotFoundException {
		FilterList filterList = filterService.getFilterListById(619);
		long offset = 0;
		long count = 0;
		for (int i = 0; i < MAX_ITERATIONS; i++) {
			List<? extends AbstractValue> emailValues = filterService.getValues(filterList, offset, SEND_LIMIT);
			count += emailValues.size();
			if (emailValues.size() < SEND_LIMIT) {
				break;
			}

			offset = offset + SEND_LIMIT;
		}
		System.out.println(count);
	}
}

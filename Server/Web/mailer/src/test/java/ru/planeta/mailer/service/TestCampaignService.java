package ru.planeta.mailer.service;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.model.custom.Filter;
import ru.planeta.mailer.model.custom.FilterList;
import ru.planeta.mailer.model.generated.Campaign;
import ru.planeta.mailer.model.generated.Template;
import ru.planeta.mailer.test.GenericSpringTest;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class TestCampaignService extends GenericSpringTest {

    @Autowired
    TemplateService templateService;

    @Autowired
    FilterService filterService;

    @Autowired
    MailerCampaignService mailerCampaignService;

    @Test
    public void filterServiceTest() throws NotFoundException {

        Template template = new Template();

        template.setName("cool template");
        template.setText("<b>test</b>");

        templateService.saveTemplate(template);
        assertNotNull(template.getTemplateId());

        FilterList filterList = new FilterList();
        List<Filter> filters = new ArrayList<Filter>();
        Filter filter = new Filter();

        filterList.setFilters(filters);

        filterService.saveList(filterList);
        assertNotNull(filterList.getFilterListId());

        Campaign campaign = new Campaign();
        campaign.setName("test");
        campaign.setFilterListId(filterList.getFilterListId());
        campaign.setTemplateId(template.getTemplateId());
        campaign.setText(template.getText());

        mailerCampaignService.saveCampaign(campaign);
        assertNotNull(campaign.getCampaignId());

        List<Campaign> campaigns = mailerCampaignService.getCampaigns();

        boolean found = false;
        for (Campaign c : campaigns) {
            if (campaign.getCampaignId().equals(c.getCampaignId())) {
                found = true;
                break;
            }
        }

        assertTrue(found);

        mailerCampaignService.deleteCampaign(campaign.getCampaignId());
        filterService.deleteList(filterList.getFilterListId());
        templateService.deleteTemplate(template.getTemplateId());
    }

    @Ignore
    @Test
    public void getWithTimeToSendTest() {
        GregorianCalendar calendarFrom = new GregorianCalendar();
        GregorianCalendar calendarTo = new GregorianCalendar();
        calendarFrom.set(2016, Calendar.JANUARY, 1);
        calendarTo.set(2017, Calendar.JANUARY, 1);
        List<Campaign> campaigns = mailerCampaignService.getCampaignsWithTimeToSend(calendarFrom.getTime(), calendarTo.getTime(), 0, 0);
    }
}

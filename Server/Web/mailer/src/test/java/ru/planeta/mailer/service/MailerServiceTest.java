package ru.planeta.mailer.service;

import org.apache.commons.lang3.StringEscapeUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.mailer.dao.generated.CampaignMapper;
import ru.planeta.mailer.model.generated.Campaign;
import ru.planeta.mailer.test.GenericSpringTest;

import static org.junit.Assert.*;

/**
 * Created by asavan on 19.06.2017.
 */
public class MailerServiceTest extends GenericSpringTest {
    @Autowired
    private CampaignMapper campaignMapper;
    @Autowired
    private MailerService mailerService;
    @Test
    public void previewMessageHtml() throws Exception {

        Campaign campaign = campaignMapper.selectByPrimaryKey(2257L);
        String html = mailerService.previewMessageHtml(campaign);

        System.out.println(html);
    }


    @Test
    public void hrefUnescape() {
        String hrefHtml = "https://biblio.planeta.ru/books?bookId=79&amp;utm_source=newsletter&amp;utm_medium=email&amp;utm_campaign=20170518_weekly_russiandevil&amp;utm_content=item4";
        String hrefExpected = "https://biblio.planeta.ru/books?bookId=79&utm_source=newsletter&utm_medium=email&utm_campaign=20170518_weekly_russiandevil&utm_content=item4";
        String href = StringEscapeUtils.unescapeHtml4(hrefHtml);
        assertEquals(hrefExpected, href);
    }
}
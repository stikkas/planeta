package ru.planeta.mailer.service.mail;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.model.custom.FilterList;
import ru.planeta.mailer.model.generated.Campaign;
import ru.planeta.mailer.model.generated.Template;
import ru.planeta.mailer.service.FilterService;
import ru.planeta.mailer.service.MailerCampaignService;
import ru.planeta.mailer.service.MailerService;
import ru.planeta.mailer.service.TemplateService;
import ru.planeta.mailer.test.GenericSpringTest;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class TestMailerService extends GenericSpringTest {

    private static final Logger LOG = Logger.getLogger(TestMailerService.class);

    @Autowired
    TemplateService templateService;

    @Autowired
    FilterService filterService;

    @Autowired
    MailerCampaignService mailerCampaignService;

    @Autowired
    MailerService mailerService;

    @Test
	@Ignore
    public void MailerServiceWithFileFilterTest() throws NotFoundException, InterruptedException, URISyntaxException, IOException {

		File dstFile = null;

		try {
			File testFile = getFileFromClasspath("shares.csv");
			dstFile = new File("testfile.csv");
			FileUtils.copyFile(testFile, dstFile);

			FilterList filterList = filterService.createListFromCSVFile(dstFile);
			assertNotNull(filterList);
			assertTrue(filterList.getFilterListId() > 0);

			Template template = createTemplate();

			Campaign campaign = createCampaign(template, filterList);
			mailerCampaignService.saveCampaign(campaign);
			assertTrue(campaign.getCampaignId() > 0);

			filterList = filterService.getFilterListById(campaign.getFilterListId());
			assertNotNull(filterList);
			assertTrue(filterList.getFilterListId() > 0);

			mailerCampaignService.sendCampaign(campaign);

			Thread.sleep(100000);

		} finally {
			FileUtils.deleteQuietly(dstFile);
		}

    }


	@Test
	@Ignore
	public void filterServiceTest() throws NotFoundException, InterruptedException {

		Template template = createTemplate();

		FilterList filterList = filterService.getFilterListById(384);
		Campaign campaign = createCampaign(template, filterList);

		mailerCampaignService.saveCampaign(campaign);
		assertNotNull(campaign.getCampaignId());

		Campaign campaign2 = new Campaign();
		campaign2.setName("test2");
		campaign2.setFilterListId(filterList.getFilterListId());
		campaign2.setTemplateId(template.getTemplateId());
		campaign2.setText(template.getText());

		mailerCampaignService.saveCampaign(campaign2);
		assertNotNull(campaign.getCampaignId());

		mailerCampaignService.sendCampaign(campaign);
		mailerCampaignService.sendCampaign(campaign2);

		Thread.sleep(100000);
	}

	private Template createTemplate() {
		Template template = new Template();

		template.setName("cool template");
		template.setText("<b>test</b>");

		templateService.saveTemplate(template);
		assertNotNull(template.getTemplateId());
		return template;
	}

	private Campaign createCampaign(Template template, FilterList filterList) {
		Campaign campaign = new Campaign();
		campaign.setName("test");
		campaign.setFilterListId(filterList.getFilterListId());
		campaign.setTemplateId(template.getTemplateId());
		campaign.setText(template.getText());
        campaign.setSendToAll(false);
		return campaign;
	}
}

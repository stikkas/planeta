package ru.planeta.mailer.service.stats;

import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.model.custom.FilterList;
import ru.planeta.mailer.model.generated.Campaign;
import ru.planeta.mailer.model.generated.Template;
import ru.planeta.mailer.service.FilterService;
import ru.planeta.mailer.service.MailerCampaignService;
import ru.planeta.mailer.service.TemplateService;
import ru.planeta.mailer.test.GenericSpringTest;

import java.util.Date;

import static org.junit.Assert.assertNotNull;

/**
 * @author: ds.kolyshev
 * Date: 08.07.13
 */
public class TestMessageStatsService extends GenericSpringTest {

	private static final Logger log = Logger.getLogger(TestMessageStatsService.class);

	@Autowired
	private MessageStatsService messageStatsService;

	@Autowired
	private TemplateService templateService;

	@Autowired
	private FilterService filterService;

	@Autowired
	private MailerCampaignService mailerCampaignService;

	@Test
	@Ignore
	public void testStatsService() throws NotFoundException {


		FilterList filterList = filterService.getFilterListById(384);
		assertNotNull(filterList);

		Template template = createTemplate();
		Campaign campaign = createCampaign(template, filterList);
		mailerCampaignService.saveCampaign(campaign);
		assertNotNull(campaign.getCampaignId());

		Date now = new Date();

		messageStatsService.updateCampaignStats(campaign, true);

		log.debug("Test time: " + ((new Date()).getTime() - now.getTime()));
	}

	private Template createTemplate() {
		Template template = new Template();

		template.setName("cool template");
		template.setText("<b>test</b>");

		templateService.saveTemplate(template);
		assertNotNull(template.getTemplateId());
		return template;
	}

	private Campaign createCampaign(Template template, FilterList filterList) {
		Campaign campaign = new Campaign();
		campaign.setName("test");
		campaign.setFilterListId(filterList.getFilterListId());
		campaign.setTemplateId(template.getTemplateId());
		campaign.setText(template.getText());
		return campaign;
    }

}

package ru.planeta.mailer.web.controllers;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.test.GenericSpringTest;

/**
 * User: a.savanovich
 * Date: 09.01.14
 * Time: 18:16
 */
@Ignore
public class TestCampaignController extends GenericSpringTest {
    @Autowired
    private CampaignController campaignController;
    @Test
    public void test() throws NotFoundException {
        Model model = new ExtendedModelMap();
        campaignController.stats(model, 925L);
    }
}

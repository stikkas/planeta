package ru.planeta.mailer.service;

import org.junit.Ignore;
import org.junit.Test;

import java.util.concurrent.*;

/**
 * User: a.savanovich
 * Date: 23.10.13
 * Time: 12:41
 */
@Ignore
public class TestExecutionPool {
    private static final int MAX_THREADS = 10;

    @Test
    public void testExecutors() {
        for (int i = 0; i < 6; ++i) {
            testExecution(i);
        }
    }

    public void testExecution(int j) {
        long startTime = System.currentTimeMillis();
        ExecutorService executor = (j < 3) ? getExecutorService() : getExecutorService2();
        //ExecutorService ex = Executors.newFixedThreadPool(3);
        System.out.println("Main thread " + Thread.currentThread().getId());
        for (int i = 0; i < 20; ++i) {
            Runnable task = new PrintMessage(i);
            executor.execute(task);
            // System.out.println("Wake up");
        }
        executor.shutdown();
        executor.shutdown();
        executor.shutdown();
        try {
            executor.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long endTime = System.currentTimeMillis();
        System.out.println(endTime - startTime);
    }



    private ExecutorService getExecutorService() {
        return new ThreadPoolExecutor(MAX_THREADS, MAX_THREADS,
                0L, TimeUnit.MILLISECONDS,
                new SynchronousQueue<Runnable>(),
                new BlockingStrategy());
    }

    private ExecutorService getExecutorService2() {
        return new ThreadPoolExecutor(MAX_THREADS, MAX_THREADS,
            0L, TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<Runnable>(3),
            new ThreadPoolExecutor.CallerRunsPolicy());
    }

    private static class BlockingStrategy implements RejectedExecutionHandler {

        @Override
        public void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor) {
            if (!threadPoolExecutor.isShutdown()) {
                try {
                    threadPoolExecutor.getQueue().put(runnable);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static class PrintMessage implements Runnable {
        final private int i;

        PrintMessage(int i) {
            this.i = i;
        }

        @Override
        public void run() {
            // System.out.print(i + " ");
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.print(i + " " + Thread.currentThread().getId() + "  . ");
        }
    }
}

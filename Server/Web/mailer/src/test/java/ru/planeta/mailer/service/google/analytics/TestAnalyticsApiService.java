package ru.planeta.mailer.service.google.analytics;

import com.google.api.services.analytics.model.GaData;
import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.google.analytics.AnalyticsApiService;
import ru.planeta.mailer.google.analytics.GoogleAnalyticsQueryJson;
import ru.planeta.mailer.google.analytics.GoogleAnalyticsQueryParams;
import ru.planeta.mailer.model.generated.Campaign;
import ru.planeta.mailer.service.MailerCampaignService;
import ru.planeta.mailer.service.MailerUserService;

import java.util.List;

/**
 * User: s.makarov
 * Date: 30.07.14
 * Time: 13:36
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:spring/applicationContext-*.xml"})
public class TestAnalyticsApiService {

    private static final Logger LOG = Logger.getLogger(TestAnalyticsApiService.class);

    @Autowired
    AnalyticsApiService analyticsApiService;
    @Autowired
    MailerCampaignService mailerCampaignService;
    @Autowired
    MailerUserService mailerUserService;

    @Test
    @Ignore
    public void testAnalyticsQueryJsonHandMade() throws NotFoundException {
        GoogleAnalyticsQueryJson analyticsQuery = new GoogleAnalyticsQueryJson();
        GoogleAnalyticsQueryParams params = new GoogleAnalyticsQueryParams();
        params.setDimensions("ga:source,ga:campaign");
        params.setMetrics("ga:sessions");
        params.setFilters("ga:source=~newsletter_200514");
        params.setStartDate("2014-07-14");
        params.setEndDate("2014-07-28");

        analyticsQuery.setParams(params);

        GaData gaData = analyticsApiService.executeQuery(analyticsQuery);
        System.out.println(gaData);
    }

    @Test
    @Ignore
    public void testAnalyticsQueryJson() throws NotFoundException {
        long campaignId = 537;
        Campaign campaign = mailerCampaignService.getCampaignById(campaignId);

        if (campaign == null) {
            throw new NotFoundException(Campaign.class, campaignId);
        }

        GoogleAnalyticsQueryJson analyticsQuery = mailerCampaignService.getGoogleAnalyticsQueryJson(campaign);
        analyticsQuery.getParams().setMaxResults(3);
        List<String> usersWhoClickedLinksFromEmail = mailerCampaignService.getActiveUsersEmails(analyticsQuery);

        for(String email : usersWhoClickedLinksFromEmail) {
            LOG.debug(email);
        }

    }
}

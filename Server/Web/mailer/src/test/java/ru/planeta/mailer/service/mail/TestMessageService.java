package ru.planeta.mailer.service.mail;

import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.model.mail.MailerMessage;
import ru.planeta.mailer.test.GenericSpringTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class TestMessageService extends GenericSpringTest {

    private static final Logger log = Logger.getLogger(TestMessageService.class);

    @Autowired
    private MessageService messageService;

    @Test
    public void messageServiceTest() throws NotFoundException {

        MailerMessage message = new MailerMessage("test", new ArrayList<String>(){{add("test");}}, "test", "test", null, null, null, null, false);

        messageService.saveMessage(message);
        assertNotNull(message.getMailerMessageId());

        message.setOpen(true);
        message.setFailed(true);
        message.setUnsubscribed(true);
        messageService.saveMessage(message);

        MailerMessage newMessage = messageService.getMessageById(message.getMailerMessageId());
        assertEquals(message.getUserId(), newMessage.getUserId());
        assertNull(newMessage.getFrom());
        assertTrue(newMessage.isOpen());
        assertTrue(newMessage.isFailed());
        assertTrue(newMessage.isUnsubscribed());

        messageService.deleteMessage(message.getMailerMessageId());

    }

    @Ignore
    @Test
    public void campaignMessages() throws NotFoundException {
        List<MailerMessage> messages = messageService.getMessagesByCampaignId(47L, 0, 0, null, null, null, null, null);

        log.info(messages);
    }
}

package ru.planeta.mailer.service;

import com.google.api.services.analytics.model.GaData;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.planeta.mailer.dao.generated.CampaignMapper;
import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.google.analytics.AnalyticsApiService;
import ru.planeta.mailer.google.analytics.GoogleAnalyticsQueryJson;
import ru.planeta.mailer.google.analytics.GoogleAnalyticsQueryParams;
import ru.planeta.mailer.model.generated.Campaign;
import ru.planeta.mailer.model.generated.CampaignExample;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 24.07.12
 */
@Service
public class MailerCampaignServiceImpl implements MailerCampaignService {

    @Autowired
    private MailerService mailerService;

    @Autowired
    private CampaignMapper campaignMapper;

    @Autowired
    private AnalyticsApiService analyticsApiService;

    @Autowired
    private MailerUserService mailerUserService;

    private CampaignMapper getCampaignMapper() {
        return campaignMapper;
    }

    @Value("${from.names}")
    private String[] fromNames;

    @Override
    public List<Campaign> getCampaigns() {
        CampaignExample example = new CampaignExample();
        example.setOrderByClause("date_confirmed DESC");
        return getCampaignMapper().selectByExample(example);
    }

    @Override
    public List<Campaign> getCampaigns(String campaignName, Date dateFrom, Date dateTo, int offset, int limit, Boolean draftCampaignChecked) {
        CampaignExample example = getCampaignInner(campaignName, dateFrom, dateTo, draftCampaignChecked);
        return getCampaignMapper().selectByExampleWithRowbounds(example, getRowBounds(offset, limit));
    }

    @Override
    public List<Campaign> getCampaignsWithTimeToSend(Date dateFrom, Date dateTo, int offset, int limit) {
        CampaignExample example = new CampaignExample();
        CampaignExample.Criteria criteria = example.createCriteria();
        criteria.andTimeToSendBetween(dateFrom, dateTo);
        criteria.andDateConfirmedIsNotNull();
        return getCampaignMapper().selectByExample(example);
    }

    @Override
    public int getCampaignsCount(String campaignName, Date dateFrom, Date dateTo, int offset, int limit, Boolean draftCampaignChecked) {
        CampaignExample example = getCampaignInner(campaignName, dateFrom, dateTo, draftCampaignChecked);
        return getCampaignMapper().countByExample(example);
    }

    private CampaignExample getCampaignInner(String campaignName, Date dateFrom, Date dateTo, Boolean draftCampaignChecked) {
        CampaignExample example = new CampaignExample();
        CampaignExample.Criteria criteria = example.createCriteria();
        criteria.andNameLikeInsensitive("%" + campaignName + "%");

        if (draftCampaignChecked) {
            criteria.andDateConfirmedIsNull();
            example.setOrderByClause("campaign_id DESC");
        } else {
            criteria.andDateConfirmedIsNotNull();
            if (dateFrom != null) {
                criteria.andDateConfirmedBetween(dateFrom, dateTo);
            }
            example.setOrderByClause("date_confirmed DESC");
        }
        return example;
    }

    private RowBounds getRowBounds(int offset, int limit) {
        return new RowBounds(offset, limit <= 0 ? RowBounds.NO_ROW_LIMIT : limit);
    }

    @Override
    public Campaign getCampaignById(long id) throws NotFoundException {
        Campaign campaign = getCampaignMapper().selectByPrimaryKey(id);
        if (campaign == null) {
            throw new NotFoundException("Campaign", id);
        }
        return campaign;
    }

    @Override
    public void saveCampaign(Campaign campaign) {
        if (campaign.getCampaignId() == null) {
            getCampaignMapper().insert(campaign);
        } else {
            Campaign oldCampaign = getCampaignMapper().selectByPrimaryKey(campaign.getCampaignId());
            if (campaign.getDateConfirmed() == null && oldCampaign != null) {
                campaign.setDateConfirmed(oldCampaign.getDateConfirmed());
            }
            getCampaignMapper().updateByPrimaryKey(campaign);
        }
    }

    @Override
    public void deleteCampaign(long id) throws NotFoundException {
        getCampaignById(id);
        getCampaignMapper().deleteByPrimaryKey(id);
    }

    @Override
    public void sendCampaign(Campaign campaign) throws NotFoundException {

        mailerService.sendCampaign(campaign);

        campaign.setDateConfirmed(new Date());
        campaign.setDateSent(new Date());
        saveCampaign(campaign);
    }

	@Override
	public void stopCampaign(Campaign campaign) {
		mailerService.stopCampaign(campaign);
	}

	@Override
    public String[] getFromNames() {
        return fromNames;
    }

    @Override
    public GoogleAnalyticsQueryJson getGoogleAnalyticsQueryJson(Campaign campaign) {
        GoogleAnalyticsQueryJson analyticsQuery = new GoogleAnalyticsQueryJson();
        GoogleAnalyticsQueryParams params = new GoogleAnalyticsQueryParams();
        params.setDimensions("ga:campaign,ga:landingPagePath");
        params.setMetrics("ga:sessions,ga:transactions");
        params.setFilters("ga:source=~newsletter_" + new SimpleDateFormat("ddMMyyyy").format(campaign.getDateConfirmed()) + "_" + campaign.getCampaignId());
        params.setSort("-ga:transactions");
        params.setStartDate(new SimpleDateFormat("yyyy-MM-dd").format(campaign.getDateConfirmed()));
        params.setEndDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));

        analyticsQuery.setParams(params);
        return analyticsQuery;
    }

    @Override
    public GoogleAnalyticsQueryJson getGoogleAnalyticsQueryJsonWithoutUserID(Campaign campaign) {
        GoogleAnalyticsQueryJson analyticsQuery = new GoogleAnalyticsQueryJson();
        GoogleAnalyticsQueryParams params = new GoogleAnalyticsQueryParams();
        params.setDimensions("ga:landingPagePath");
        params.setMetrics("ga:sessions,ga:transactions");
        params.setFilters("ga:source=~newsletter_" + new SimpleDateFormat("ddMMyyyy").format(campaign.getDateConfirmed()) + "_" + campaign.getCampaignId());
        params.setSort("-ga:transactions");
        params.setStartDate(new SimpleDateFormat("yyyy-MM-dd").format(campaign.getDateConfirmed()));
        params.setEndDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));

        analyticsQuery.setParams(params);
        return analyticsQuery;
    }

    @Override
    public GoogleAnalyticsQueryJson getGoogleAnalyticsQueryJsonOfNewslettersByDateRange(Date dateFrom, Date dateTo) {
        GoogleAnalyticsQueryJson analyticsQuery = new GoogleAnalyticsQueryJson();
        GoogleAnalyticsQueryParams params = new GoogleAnalyticsQueryParams();
        params.setDimensions("ga:landingPagePath");
        params.setMetrics("ga:sessions,ga:transactions");
        params.setFilters("ga:source=~newsletter");
        params.setSort("-ga:transactions");
        params.setStartDate(new SimpleDateFormat("yyyy-MM-dd").format(dateFrom != null ? dateFrom : new Date()));
        params.setEndDate(new SimpleDateFormat("yyyy-MM-dd").format(dateTo != null ? dateTo : new Date()));

        analyticsQuery.setParams(params);
        return analyticsQuery;
    }

    @Override
    public List<String> getActiveUsersEmails(GoogleAnalyticsQueryJson analyticsQuery) {
        GaData gaData;
        HashSet<Long> userIds = new HashSet<>();
        int startIndex = analyticsQuery.getParams().getStartIndex();
        do {
            gaData = analyticsApiService.executeQuery(analyticsQuery);
            userIds.addAll(mailerUserService.getUserIds(gaData));

            startIndex += gaData.getRows().size();
            analyticsQuery.getParams().setStartIndex(startIndex);
        } while (gaData.getNextLink() != null);
        return mailerUserService.getEmailLists(userIds);
    }
}

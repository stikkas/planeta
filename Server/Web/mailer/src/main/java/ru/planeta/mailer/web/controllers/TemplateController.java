package ru.planeta.mailer.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.model.generated.Template;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 20.07.12
 */
@Controller
public class TemplateController extends BaseController {

    @RequestMapping(value = Urls.TEMPLATE_INDEX, method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("templates", getTemplateService().getTemplates());
        return Actions.TEMPLATES.getText();
    }

    @RequestMapping(value = Urls.TEMPLATE_WITH_ID_EDIT, method = RequestMethod.GET)
    public String template(Model model,
                           @PathVariable("id") Long templateId) throws NotFoundException {
        return template(model, templateId, null);
    }

    @RequestMapping(value = Urls.TEMPLATE_EDIT, method = RequestMethod.GET)
    public String template(Model model,
                           @RequestParam(value = "id", required = false) Long templateId,
                           @ModelAttribute Template template) throws NotFoundException {
        if (templateId != null) {
            template = getTemplateService().getTemplateById(templateId);
            model.addAttribute("template", template);
        }
        return Actions.TEMPLATE_EDIT.getText();
    }

    @RequestMapping(value = {Urls.TEMPLATE_WITH_ID_EDIT, Urls.TEMPLATE_EDIT}, method = RequestMethod.POST)
    public String templatePost(Model model,
                               @ModelAttribute @Valid Template template,
                               BindingResult templateErrors) {
        if (templateErrors.hasErrors()) {
            return Actions.TEMPLATE_EDIT.getText();
        }
        getTemplateService().saveTemplate(template);
        return Urls.redirect(Urls.TEMPLATE_WITH_ID_EDIT, "id", template.getTemplateId());
    }

    @RequestMapping(value = Urls.TEMPLATE_WITH_ID_DELETE, method = RequestMethod.GET)
    public String deleteTemplate(Model model,
                                 @PathVariable("id") Long templateId) throws NotFoundException {
        Template template = getTemplateService().getTemplateById(templateId);
        model.addAttribute("template", template);
        return Actions.TEMPLATE_DELETE.getText();
    }

    @RequestMapping(value = Urls.TEMPLATE_WITH_ID_DELETE, method = RequestMethod.POST)
    public String deleteTemplatePost(Model model,
                                     @PathVariable("id") Long templateId) throws NotFoundException {
        getTemplateService().deleteTemplate(templateId);
        return Urls.redirect(Urls.TEMPLATE_INDEX);
    }

    @RequestMapping(value = Urls.TEMPLATE_TEXT, method = RequestMethod.GET)
    public void templateText(@RequestParam("id") Long id, HttpServletResponse response) throws IOException, NotFoundException {
        response.setContentType("text/html;charset=utf-8");

        response.getWriter().print(getTemplateService().getTemplateById(id).getText());
    }

}

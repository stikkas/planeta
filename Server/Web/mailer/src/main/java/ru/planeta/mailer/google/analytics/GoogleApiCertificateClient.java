package ru.planeta.mailer.google.analytics;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collection;

/**
 * OAuth 2.0 authentication for server using x509 certificates
 * @see <a href="https://developers.google.com/accounts/docs/OAuth2ServiceAccount">google guide</a> .
 * @author s.kalmykov
 */
public class GoogleApiCertificateClient {

    private static HttpTransport httpTransport;
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

    /** Global instance of the HTTP transport. */
    public final static synchronized HttpTransport getHttpTransportInstance() throws GeneralSecurityException, IOException {
        if(httpTransport == null) {
            httpTransport = GoogleNetHttpTransport.newTrustedTransport();
        }
        return httpTransport;
    }

    /** Global instance of the JSON factory. */
    public final static JsonFactory getJsonFactoryInstance() {
        return JSON_FACTORY;
    }

    /** Key from https://cloud.google.com/console, generate new key in certificate tab. */
    private File privateKey;
    /** Email generated after enabling certificate oauth in https://cloud.google.com/console. */
    private String serviceAccountEmail;
    /** Predefined api-specific constants. */
    private Collection<String> accountScopes;

    public GoogleApiCertificateClient(File privateKey, String serviceAccountEmail, Collection<String> accountScopes) {
        this.privateKey = privateKey;
        this.serviceAccountEmail = serviceAccountEmail;
        this.accountScopes = accountScopes;
    }


    public final GoogleCredential getGoogleCredential() throws GeneralSecurityException, IOException {
        return new GoogleCredential.Builder().setTransport(httpTransport)
            .setJsonFactory(JSON_FACTORY)
            .setServiceAccountId(serviceAccountEmail)
            .setServiceAccountScopes(accountScopes)
            .setServiceAccountPrivateKeyFromP12File(privateKey)
            .build();
    }

}

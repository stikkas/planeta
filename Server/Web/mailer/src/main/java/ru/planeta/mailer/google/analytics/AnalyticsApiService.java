package ru.planeta.mailer.google.analytics;

import com.google.api.services.analytics.model.GaData;

public interface AnalyticsApiService {
    GaData executeQuery(GoogleAnalyticsQueryJson queryJson);
}

package ru.planeta.mailer.dao.stats;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import ru.planeta.mailer.model.stats.CampaignStats;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 10.08.12
 */
@Repository
public interface CampaignStatsMapper {

    //int deleteByPrimaryKey(Long campaignStatsId);

    int insert(CampaignStats record);

    CampaignStats selectByPrimaryKey(@Param("campaignId") Long campaignId, @Param("domainName") String domainName);

    CampaignStats selectTotalByDateInterval(@Param("domainName") String domainName, @Param("dateFrom") Date dateFrom, @Param("dateTo") Date dateTo);

    int updateByPrimaryKey(CampaignStats record);

    void updateCampaignStatsForDomainNameList(@Param("campaignId") Long campaignId, @Param("domainNameList") List<String> domainNameList, @Param("resend") boolean resend);
}

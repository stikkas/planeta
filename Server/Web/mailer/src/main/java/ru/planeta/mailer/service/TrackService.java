package ru.planeta.mailer.service;

import org.apache.commons.codec.DecoderException;
import ru.planeta.mailer.exceptions.NotFoundException;

/**
 *
 * Created by asavan on 19.06.2017.
 */
public interface TrackService {
    /**
     * Track the message was opened by user
     * @param code
     */
    void trackOpen(String code) throws NotFoundException, DecoderException;

    void trackUnsubscribed(String code) throws NotFoundException, DecoderException;

}

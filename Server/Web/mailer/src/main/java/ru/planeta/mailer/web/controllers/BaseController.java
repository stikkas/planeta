package ru.planeta.mailer.web.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.planeta.mailer.service.*;
import ru.planeta.mailer.service.mail.MessageService;
import ru.planeta.mailer.service.stats.MessageStatsService;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 24.07.12
 */
@Component
public abstract class BaseController {

    @Autowired
    private FilterService filterService;

    @Autowired
    private TemplateService templateService;

    @Autowired
    private MailerCampaignService mailerCampaignService;

    @Autowired
    private MailerUserService mailerUserService;

    @Autowired
    private MailerService mailerService;

    @Autowired
    private MessageService messageService;

    @Autowired
    private MessageStatsService messageStatsService;

    private Logger logger = Logger.getLogger(getClass());

    protected FilterService getFilterService() {
        return filterService;
    }

    protected TemplateService getTemplateService() {
        return templateService;
    }

    protected MailerCampaignService getMailerCampaignService() {
        return mailerCampaignService;
    }

    protected MailerUserService getMailerUserService() {
        return mailerUserService;
    }

    protected MailerService getMailerService() {
        return mailerService;
    }

    protected MessageService getMessageService() {
        return messageService;
    }

    public MessageStatsService getMessageStatsService() {
        return messageStatsService;
    }

    protected Logger getLogger() {
        return logger;
    }
}

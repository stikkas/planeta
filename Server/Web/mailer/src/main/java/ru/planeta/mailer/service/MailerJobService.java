package ru.planeta.mailer.service;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.model.enums.CampaignState;
import ru.planeta.mailer.model.generated.Campaign;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 06.07.16
 * Time: 17:20
 */
@Service
public class MailerJobService {
    private final MailerCampaignService mailerCampaignService;
    private final MailerService mailerService;

    private static final Logger logger = Logger.getLogger(MailerJobService.class);
    final static private int CHECK_DELAY = 1000 * 60 * 5;

    @Autowired
    public MailerJobService(MailerCampaignService mailerCampaignService, MailerService mailerService) {
        this.mailerCampaignService = mailerCampaignService;
        this.mailerService = mailerService;
    }

    @Scheduled(fixedDelay = CHECK_DELAY)
    public synchronized void checkForDeferredMail() throws NotFoundException {
        logger.debug("checkForDeferredMail: started");

        final Date now = new Date();
        // TODO offset limit
        List<Campaign> campaigns = mailerCampaignService.getCampaignsWithTimeToSend(DateUtils.addHours(now, -1), now, 0, 0);
        for (Campaign campaign : campaigns) {
            if (campaign.getTimeToSend() != null) {
                CampaignState campaignState = mailerService.getCampaignState(campaign);
                logger.debug("checkForDeferredMail: found campaign " + campaign.getCampaignId() +
                        " with timeToSend = " + campaign.getTimeToSend() +
                        " and state = " + campaignState);
                if (campaignState == CampaignState.NOT_RUNNING) {
                    mailerCampaignService.sendCampaign(campaign);
                }
            }
        }
    }
}

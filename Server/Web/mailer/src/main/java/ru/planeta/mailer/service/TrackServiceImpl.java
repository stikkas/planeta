package ru.planeta.mailer.service;

import org.apache.commons.codec.DecoderException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.model.generated.Campaign;
import ru.planeta.mailer.model.generated.User;
import ru.planeta.mailer.service.mail.MessageService;

/**
 *
 * Created by asavan on 19.06.2017.
 */
@Service
public class TrackServiceImpl extends BaseService implements TrackService {
    private final MailerUserService mailerUserService;
    private final MailerCampaignService mailerCampaignService;
    private final MessageService messageService;

    @Autowired
    public TrackServiceImpl(MailerUserService mailerUserService, MailerCampaignService mailerCampaignService, MessageService messageService) {
        this.mailerUserService = mailerUserService;
        this.mailerCampaignService = mailerCampaignService;
        this.messageService = messageService;
    }

    @Override
    public void trackUnsubscribed(String code) throws NotFoundException, DecoderException {
        User user = mailerUserService.getUserByCode(code);
        Campaign campaign = mailerCampaignService.getCampaignById(mailerUserService.getCampaignIdByCode(code));
        messageService.setMessageUnsubscribed(user, campaign);
    }

    @Override
    public void trackOpen(String code) throws NotFoundException, DecoderException {
        User user = mailerUserService.getUserByCode(code);
        Campaign campaign = mailerCampaignService.getCampaignById(mailerUserService.getCampaignIdByCode(code));
        messageService.setMessageOpen(user, campaign);
    }

}

package ru.planeta.mailer.model.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.mailer.model.generated.Template;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 19.07.12
 */
@Component
public class TemplateValidator implements Validator {

    public boolean supports(Class<?> type) {

        return type.isAssignableFrom(Template.class);
    }

    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "name", "name.empty.Template");
        ValidationUtils.rejectIfEmpty(errors, "text", "text.empty.Template");

        if ((! ((String)errors.getFieldValue("text")).contains("{{unsubscribe}}")) &&
                (! ((String)errors.getFieldValue("text")).contains("{{unsubscribeLink}}"))) {
            errors.rejectValue("text", "text.dont.have.unsubscribe");
        }
    }
}

package ru.planeta.mailer.service.mail;

import com.sun.mail.smtp.SMTPTransport;

import javax.mail.*;
import java.util.Date;

/**
* Created with IntelliJ IDEA.
* User: sfionov
* Date: 26.11.12
*/
public class MailTransport extends SMTPTransport {
    private Date lastUsed;

    public MailTransport(Session session) {
        super(session, new URLName("smtp", null, -1, null, null, null));
        this.updateLastUsed();
    }

    public MailTransport(Session session, String host, int port, String user, String password) {
        super(session, new URLName("smtp", host, port, null, user, password));
        this.updateLastUsed();
    }

    public synchronized Session getSession() {
        return session;
    }

    public synchronized Date getLastUsed() {
        return lastUsed;
    }

    public synchronized void updateLastUsed() {
        this.lastUsed = new Date();
    }

    public synchronized void sendMessage(Message message, Address[] addresses) throws MessagingException, SendFailedException {
        super.sendMessage(message, addresses);
        this.updateLastUsed();
    }
}

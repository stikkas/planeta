package ru.planeta.mailer.model.stats;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 01.08.12
 */
public class MailerDaemonInfo {
    /**
     * List of message recipients
     */
    List<String> emails;

    /**
     * Error code
     */
    int errorCode;

    /**
     * Message identifier
     */
    String messageId;

    /**
     * Is abuse report
     */
    boolean abuse;

    /**
     * Unsubscribe
     * If original message ID has been shrinked from bounce message, campaign id and user key
     * may be extracted from the unsubscribe link.
     */
    String unsubscribeLink;

    public List<String> getEmails() {
        return emails;
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getUnsubscribeLink() {
        return unsubscribeLink;
    }

    public void setUnsubscribeLink(String unsubscribeLink) {
        this.unsubscribeLink = unsubscribeLink;
    }

    public boolean isAbuse() {
        return abuse;
    }

    public void setAbuse(boolean abuse) {
        this.abuse = abuse;
    }
}

package ru.planeta.mailer.model.typeHandler;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;
import ru.planeta.mailer.model.enums.AttributeType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@MappedTypes({AttributeType.class})
public class AttributeTypeHandler implements TypeHandler<AttributeType> {
    private static Map<Integer, AttributeType> map = new HashMap<Integer, AttributeType>();

    static {
        for (AttributeType attributeType : AttributeType.values()) {
            map.put(attributeType.getCode(), attributeType);
        }
    }

    @Override
    public void setParameter(PreparedStatement ps, int i, AttributeType parameter, JdbcType jdbcType) throws SQLException {
        ps.setInt(i, parameter.getCode());
    }

    @Override
    public AttributeType getResult(ResultSet rs, String columnName) throws SQLException {
        return rs.getObject(columnName) == null ? null : map.get(rs.getInt(columnName));
    }

    @Override
    public AttributeType getResult(ResultSet rs, int columnIndex) throws SQLException {
        return rs.getObject(columnIndex) == null ? null : map.get(rs.getInt(columnIndex));
    }

    @Override
    public AttributeType getResult(CallableStatement cs, int columnIndex) throws SQLException {
        return cs.getObject(columnIndex) == null ? null : map.get(cs.getInt(columnIndex));
    }

}

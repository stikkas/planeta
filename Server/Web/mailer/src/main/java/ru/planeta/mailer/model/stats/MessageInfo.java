package ru.planeta.mailer.model.stats;

import ru.planeta.mailer.model.generated.Campaign;
import ru.planeta.mailer.model.generated.User;
import ru.planeta.mailer.model.mail.MailerMessage;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 08.08.12
 */
public class MessageInfo {
    private MailerMessage mailerMessage;

    private User user;

    private Campaign campaign;

    public MailerMessage getMailerMessage() {
        return mailerMessage;
    }

    public void setMailerMessage(MailerMessage mailerMessage) {
        this.mailerMessage = mailerMessage;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Campaign getCampaign() {
        return campaign;
    }

    public void setCampaign(Campaign campaign) {
        this.campaign = campaign;
    }
}

package ru.planeta.mailer.service;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.model.generated.Template;
import ru.planeta.mailer.model.generated.TemplateExample;

import javax.servlet.ServletContext;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 23.07.12
 */
@Service
public class TemplateServiceImpl extends BaseService implements TemplateService {

    private static final Logger log = Logger.getLogger(TemplateServiceImpl.class);
    @Value("${template.css.uri}")
    private String templateCssURI;
    @Autowired(required = false)
    private ServletContext servletContext;

    private String getRealPath() {
        if (servletContext == null) {
            return "Server/Web/planeta-static/src/main/webapp";
        }
        return servletContext.getRealPath("");
    }

    @Override
    public List<Template> getTemplates() {
        TemplateExample example = new TemplateExample();
        example.setOrderByClause("name ASC");
        return getTemplateMapper().selectByExample(example);
    }

    @Override
    public List<Template> getTemplates(String query, int offset, int limit) {
        TemplateExample example = new TemplateExample();
        if (StringUtils.isNotBlank(query)) {
            query = StringUtils.normalizeSpace(query).replace(' ','%');
            example.or().andNameLikeInsensitive("%" + query + "%");
        }

        example.setOrderByClause("name ASC");
        return getTemplateMapper().selectByExampleWithRowbounds(example, new RowBounds(offset, limit));
    }

    @Override
    public Template getTemplateById(long templateId) throws NotFoundException {
        Template template = getTemplateMapper().selectByPrimaryKey(templateId);
        if (template == null) {
            throw new NotFoundException("Template", templateId);
        }
        return template;
    }

    @Override
    public void saveTemplate(Template template) {
        if (template.getTemplateId() == null) {
            getTemplateMapper().insert(template);
        } else {
            getTemplateMapper().updateByPrimaryKey(template);
        }
    }

    @Override
    public void deleteTemplate(long templateId) throws NotFoundException {
        Template template = getTemplateMapper().selectByPrimaryKey(templateId);
        if (template == null) {
            throw new NotFoundException("Template", templateId);
        }
        getTemplateMapper().deleteByPrimaryKey(templateId);
    }

    @Override
    public String renderMessageHtml(String content, ModelMap model) {
        Pattern pattern = Pattern.compile(Pattern.quote("{{") + "([^}]+)" + Pattern.quote("}}"));
        Matcher matcher = pattern.matcher(content);
        Set<String> attributes = new HashSet<String>();
        while (matcher.find()) {
            attributes.add(matcher.group(1));
        }
        for (String attribute : attributes) {
            if (model.containsKey(attribute)) {
                content = content.replace("{{" + attribute + "}}", String.valueOf(model.get(attribute)));
            }
        }
        content += "\n<img src=\"" + model.get("trackLink") + "\" width=\"1\" height=\"1\" />";
        return buildHtml(content, new File(getRealPath(), templateCssURI));
    }

    /**
     * Apply CSS to Document object model
     */
    private static void applyCSS(Document document, File file) {
        try {
            String css = FileUtils.readFileToString(file);
            // Skip comments
            css = Pattern.compile("\\s*/\\*.*?\\*/", Pattern.DOTALL).matcher(css).replaceAll("");
            // Add custom styles (don't override styles specified in the redactor)
            Pattern cssRule = Pattern.compile("([^{}]+)\\{([^{}]+)\\}", Pattern.DOTALL);
            Matcher matcher = cssRule.matcher(css);

            while (matcher.find()) {
                String[] selectors = matcher.group(1).replaceAll("[\r\n]","").split(",");
                List<String> selList = new ArrayList<String>();
                for (String selector : selectors) {
                    if (selector.contains(":")) continue;
                    selList.add(selector);
                }
                if (selList.isEmpty()) continue;
                String selector = StringUtils.join(selList.toArray(), ",");
                String style = matcher.group(2).replaceAll("[\r\n]+", "").replaceAll("\\s+"," ").trim();
                Elements elements = document.select(selector);
                for (Element element : elements) {
                    String currentStyle = element.attr("style").trim();
                    if (!StringUtils.isEmpty(currentStyle) && !StringUtils.endsWith(currentStyle, ";")) {
                        currentStyle += ";";
                    }
                    currentStyle = currentStyle + style;
                    element.attr("style", currentStyle);
                }
            }
        } catch (IOException e) {
            log.warn(e);
        }
    }

    private static void changeLinks(Document document) {
        Elements elements = document.select("a");
        for (Element element : elements) {
            element.attr("target", "_blank");
            String oldHref = element.attr("href");
            element.attr("href", StringEscapeUtils.unescapeHtml4(oldHref));
        }
    }


    private static String buildHtml(String content, File file) {
        Document doc = new Document("mail-message");
        Element htmlEl = doc.appendElement("html");
        Element headEl = htmlEl.appendElement("head");
        headEl.appendElement("title");
        Element bodyEl = htmlEl.appendElement("body");
        bodyEl.append(content);
        applyCSS(doc, file);
        changeLinks(doc);

        return "<!DOCTYPE html>\r\n" + doc.outerHtml();
    }
}

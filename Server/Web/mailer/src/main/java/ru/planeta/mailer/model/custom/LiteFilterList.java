package ru.planeta.mailer.model.custom;

import ru.planeta.mailer.model.enums.FilterType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kostiagn on 29.09.2015.
 */
public class LiteFilterList {
    private Long filterListId;
    private String name;

    public LiteFilterList(FilterList filterList) {
        filterListId = filterList.getFilterListId();
        name = filterList.getName();
    }

    public Long getFilterListId() {
        return filterListId;
    }

    public void setFilterListId(Long filterListId) {
        this.filterListId = filterListId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static List<LiteFilterList> convert(List<FilterList> list) {
        List<LiteFilterList> result = new ArrayList<>();
        for (FilterList filterList : list) {
            result.add(new LiteFilterList(filterList));
        }
        return result;
    }
}

package ru.planeta.mailer.model.custom;

/**
 * Base class for email-values
 *
 * @author ds.kolyshev
 * Date: 25.06.13
 */
public abstract class AbstractValue {

	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDisplayName() {
		return email;
	}

}

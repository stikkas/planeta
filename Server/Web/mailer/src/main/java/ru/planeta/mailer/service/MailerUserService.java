package ru.planeta.mailer.service;

import com.google.api.services.analytics.model.GaData;
import org.apache.commons.codec.DecoderException;
import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.model.custom.AbstractValue;
import ru.planeta.mailer.model.generated.User;

import javax.annotation.Nonnull;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 26.07.12
 */
public interface MailerUserService {

    /**
     * Gets or creates user by email, storing displayName
     * @param value
     * @return
     */
    User getUserByValue(AbstractValue value);

    /**
     * Gets or creates user by email
     * @param email
     * @return
     */
    @Nonnull
    User getUserByEmail(String email) throws NotFoundException;

    User getUserByEmailWithoutException(String email);



    /**
     * Gets user by identifier
     * @param userId
     * @return
     * @throws NotFoundException
     */
    User getUserById(Long userId) throws NotFoundException;

    /**
     * Gets user by unsibscribe code
     * @param code
     * @return
     * @throws NotFoundException
     */
    User getUserByCode(String code) throws NotFoundException;

    /**
     * Gets campaignId from code
     * @param code
     * @return
     * @throws DecoderException
     */
    long getCampaignIdByCode(String code) throws DecoderException;

    /**
     * Generates unsubscribe code for user
     * @param email
     * @param campaignId
     * @return
     */
    String getUnsubscribeCode(String email, long campaignId) throws NotFoundException;

    /**
     * Saves user
     * @param user
     */
    void saveUser(User user);

    List<User> getUserListByIds(List<Long> ids);

    List<String> getEmailLists(Set<Long> userIds);

    HashSet<Long> getUserIds(GaData gaData);

    List<User> getUsers(String userName, int offset, int limit);

    int getUsersCount(String userName);
}

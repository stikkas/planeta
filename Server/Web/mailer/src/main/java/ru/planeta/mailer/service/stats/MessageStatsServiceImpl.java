package ru.planeta.mailer.service.stats;

import com.google.api.services.analytics.model.GaData;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.google.analytics.AnalyticsApiService;
import ru.planeta.mailer.google.analytics.GoogleAnalyticsQueryJson;
import ru.planeta.mailer.model.generated.Campaign;
import ru.planeta.mailer.model.generated.User;
import ru.planeta.mailer.model.mail.MailerMessage;
import ru.planeta.mailer.model.stats.CampaignStats;
import ru.planeta.mailer.model.stats.MessageInfo;
import ru.planeta.mailer.service.BaseService;
import ru.planeta.mailer.service.FilterService;
import ru.planeta.mailer.service.MailerCampaignService;
import ru.planeta.mailer.service.MailerUserService;
import ru.planeta.mailer.service.mail.MessageService;
import ru.planeta.mailer.web.model.enums.MailDomain;
import ru.planeta.reports.ReportType;
import ru.planeta.reports.SimpleReport;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 07.08.12
 */
@Service
public class MessageStatsServiceImpl extends BaseService implements MessageStatsService {

    private static final Logger log = Logger.getLogger(MessageStatsServiceImpl.class);

    @Autowired
    private MailerUserService mailerUserService;

    @Autowired
    private MailerCampaignService mailerCampaignService;

    @Autowired
    private MessageService messageService;

    @Autowired
    private FilterService filterService;

    @Autowired
    private AnalyticsApiService analyticsApiService;

    @Autowired
    private MessageSource messageSource;

    @Override
    public List<MessageInfo> getCampaignMessages(Campaign campaign, long offset, long limit, String searchString, Boolean open, Boolean failed, Boolean unsubscribed, Boolean abuse) throws NotFoundException {
        List<MessageInfo> messageInfos = new ArrayList<MessageInfo>();
        List<MailerMessage> messages = messageService.getMessagesByCampaignId(campaign.getCampaignId(), offset, limit, searchString, open, failed, unsubscribed, abuse);
        if (messages == null || messages.isEmpty()) {
            return messageInfos;
        }

        List<Long> userIdList = new ArrayList<>(messages.size());
        for (MailerMessage message : messages) {
            userIdList.add(message.getUserId());
        }
        final List<User> userList = mailerUserService.getUserListByIds(userIdList);

        for (MailerMessage message : messages) {
            MessageInfo messageInfo = new MessageInfo();
            messageInfo.setMailerMessage(message);
            for (User user : userList) {
                if (user.getUserId().equals(message.getUserId())) {
                    messageInfo.setUser(user);
                    break;
                }
            }
            messageInfo.setCampaign(campaign);
            messageInfos.add(messageInfo);
        }
        return messageInfos;
    }

    @Override
    public long countCampaignMessages(Campaign campaign) throws NotFoundException {
        return messageService.countMessagesByCampaignId(campaign.getCampaignId(), null, null, null, null, null);
    }

    @Override
    public long countUserMessages(User user) throws NotFoundException {
        return messageService.countMessagesByUserId(user.getUserId());
    }

    @Override
    public List getUserMessages(User user, long offset, long limit, String searchString) throws NotFoundException {
        List<MessageInfo> messageInfos = new ArrayList<MessageInfo>();
        List<MailerMessage> messages = messageService.getMessagesByUserId(user.getUserId(), offset, limit, searchString);

        if (messages == null) return null;

        for (MailerMessage message : messages) {
            MessageInfo messageInfo = new MessageInfo();
            messageInfo.setMailerMessage(message);
            messageInfo.setUser(user);
            try {
                Campaign campaign = mailerCampaignService.getCampaignById(message.getCampaignId());
                messageInfo.setCampaign(campaign);
            } catch (Exception ignored){}
            messageInfos.add(messageInfo);
        }
        return messageInfos;
    }

    @Override
    public void updateCampaignStats(Campaign campaign) throws NotFoundException {
        updateCampaignStats(campaign, false);
    }

    @Override
    public void updateCampaignStats(Campaign campaign, boolean resend) throws NotFoundException {
        if (campaign != null) {
            getCampaignStatsMapper().updateCampaignStatsForDomainNameList(campaign.getCampaignId(), MailDomain.getCodeList(), resend);
        } else {
            log.debug("Start updateTotalCampaignsStatsForDomainName");
			updateTotalCampaignsStatsForDomainName("");

			for (MailDomain domain : MailDomain.values()) {
                log.debug("Start updateTotalCampaignsStatsForDomainName " + domain.name());
				updateTotalCampaignsStatsForDomainName(domain.getCode());
			}
            log.debug("finish updateTotalCampaignsStatsForDomainName");
        }
    }
    
    @Override
    public void increaseCompaignAdresses(Campaign campaign, long delta) {
        if (campaign != null && delta > 0) {
            CampaignStats campaignStats = getCampaignStatsMapper().selectByPrimaryKey(campaign.getCampaignId(), "");
            campaignStats.setAddresses(campaignStats.getAddresses() + delta);
            getCampaignStatsMapper().updateByPrimaryKey(campaignStats);
        }
    }

	/**
	 * Updates stats for all campaigns for specified domain name
	 * @param domainName "" for all domains
	 */
	private void updateTotalCampaignsStatsForDomainName(String domainName) {
        log.debug("Start prepareCampaignStats " + domainName);
		CampaignStats stats = prepareCampaignStats(0L, domainName);

        log.debug("Start setAddresses " + domainName);
		stats.setAddresses(filterService.countValues(null, domainName, null, null));
        log.debug("Start setSent " + domainName);
		stats.setSent(messageService.countMessages(null, null, null, null, domainName, null, null));
        log.debug("Start setOpened " + domainName);
		stats.setOpened(messageService.countMessages(true, null, null, null, domainName, null, null));
        log.debug("Start setFailed " + domainName);
		stats.setFailed(messageService.countMessages(null, true, null, null, domainName, null, null));
        log.debug("Start setUnsubscribed " + domainName);
		stats.setUnsubscribed(messageService.countMessages(null, null, true, null, domainName, null, null));
        log.debug("Start setAbuse " + domainName);
		stats.setAbuse(messageService.countMessages(null, null, null, true, domainName, null, null));

        log.debug("Start updateByPrimaryKey " + domainName);
		getCampaignStatsMapper().updateByPrimaryKey(stats);
        log.debug("finish updateTotalCampaignsStatsForDomainName " + domainName);

    }

	/**
	 * Inserts new campaign stats if it doesn't exists
	 */
	private CampaignStats prepareCampaignStats(Long campaignId, String domainName) {
		CampaignStats stats = getCampaignStatsMapper().selectByPrimaryKey(campaignId, domainName);
		if (stats == null) {
			stats = new CampaignStats();
			stats.setCampaignId(campaignId);
			stats.setDomainName(domainName);
			getCampaignStatsMapper().insert(stats);
		}
		return stats;
	}

	@Override
    public void updateTotalCampaignStats() throws NotFoundException {
        updateCampaignStats(null);
    }

    @Override
    public CampaignStats getCampaignStats(Campaign campaign, String domainName) throws NotFoundException {
        CampaignStats stats = getCampaignStatsMapper().selectByPrimaryKey(campaign.getCampaignId(), domainName);
        if (stats == null) {
            throw new NotFoundException("campaign stats", campaign.getCampaignId());
        }
        return stats;
    }

    @Override
    public CampaignStats getTotalCampaignStats(String domainName) throws NotFoundException {
        CampaignStats stats = getCampaignStatsMapper().selectByPrimaryKey(0L, domainName);
        if (stats == null) {
            throw new NotFoundException("Cannot find total campaign stats");
        }
        return stats;
    }

	@Override
	public CampaignStats getTotalCampaignStats(String domainName, Date fromDate, Date toDate) throws NotFoundException {
		CampaignStats stats = getCampaignStatsMapper().selectTotalByDateInterval(domainName, fromDate, toDate);
		if (stats == null) {
		    stats = new CampaignStats();    // no stats for this interval
        }
		stats.setCampaignId(0L);
		stats.setDomainName(domainName);
		return stats;
	}

	@Override
	public Map<String, CampaignStats> getCampaignDomainStats(Campaign campaign) throws NotFoundException {
		Map<String, CampaignStats> map = new HashMap<String, CampaignStats>();
		for (MailDomain domain : MailDomain.values()) {
			map.put(domain.name(), getCampaignStats(campaign, domain.getCode()));
		}

		return map;
	}

	@Override
	public Map<String, CampaignStats> getTotalCampaignDomainStats() throws NotFoundException {
		Map<String, CampaignStats> map = new HashMap<String, CampaignStats>();
		for (MailDomain domain : MailDomain.values()) {
			map.put(domain.name(), getTotalCampaignStats(domain.getCode()));
		}

		return map;
	}

	@Override
	public Map<String, CampaignStats> getTotalCampaignDomainStats(Date fromDate, Date toDate) throws NotFoundException {
		Map<String, CampaignStats> map = new HashMap<String, CampaignStats>();
		for (MailDomain domain : MailDomain.values()) {
			map.put(domain.name(), getTotalCampaignStats(domain.getCode(), fromDate, toDate));
		}

		return map;
	}

    @Override
    public SimpleReport getCampaignTotalStatsReport(Date dateFrom, Date dateTo, ReportType reportType) throws NotFoundException {
        CampaignStats totalCampaignStats;
        Map<String, CampaignStats> totalCampaignDomainStats;

        String reportName = "TotalCampaignsReport"
                + (dateFrom != null ? "_from_" + new SimpleDateFormat("ddMMyyyy").format(dateFrom) : "")
                + (dateTo != null ? "_to_" + new SimpleDateFormat("ddMMyyyy").format(dateTo) : "");
        SimpleReport report = reportType.createReport(reportName + "_" + reportType.name());

        totalCampaignStats = getTotalCampaignStats("", dateFrom, dateTo);
        totalCampaignDomainStats = getTotalCampaignDomainStats(dateFrom, dateTo);

        if (dateFrom!= null) {
            GoogleAnalyticsQueryJson analyticsQuery = mailerCampaignService.getGoogleAnalyticsQueryJsonOfNewslettersByDateRange(dateFrom, dateTo);
            GaData gaData = analyticsApiService.executeQuery(analyticsQuery);
            if (gaData != null) {
                String sessions = gaData.getTotalsForAllResults().get("ga:sessions");
                String transactions = gaData.getTotalsForAllResults().get("ga:transactions");
                Boolean containsSampledData = gaData.getContainsSampledData();
                if (containsSampledData) {
                    report.addRow("ATTENTION: This result from Google Analytics is based on sampled data.");
                    report.addRow("https://developers.google.com/analytics/resources/concepts/gaConceptsSampling");
                    report.skipRow();
                }
                report.addRow("Google Analytics");
                report.addRow(messageSource.getMessage("redirect.to.site.transactions", null, Locale.getDefault()).split(","));
                report.addRow();
                try {
                    report.addCell(Long.valueOf(sessions));
                    report.addCell(Long.valueOf(transactions));
                } catch(Exception ex) {
                    report.addCell(sessions);
                    report.addCell(transactions);
                }
            }
            report.skipRow();
        }
        report.addRow(messageSource.getMessage("for.all.mailings", null, Locale.getDefault()));
        addStatsToReport(totalCampaignStats, report);

        report.skipRow();
        Iterator it = totalCampaignDomainStats.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry)it.next();
            report.skipRow();
            report.addRow(pairs.getKey());
            addStatsToReport((CampaignStats)pairs.getValue(), report);
            it.remove(); // avoids a ConcurrentModificationException
        }

        return report;

    }

    @Override
    public SimpleReport getCampaignStatsReport(long campaignId, ReportType reportType) throws NotFoundException {
        Campaign campaign = mailerCampaignService.getCampaignById(campaignId);

        if (campaign == null) {
            throw new NotFoundException(Campaign.class, campaignId);
        }

        if (campaign.getDateConfirmed() != null) {
            CampaignStats campaignStats = getCampaignStats(campaign, "");

            SimpleReport report = reportType.createReport("Report_campaign" + campaignId + "_" + reportType.name());
            report.addRow("Google Analytics");
            report.addRow(messageSource.getMessage("redirect.to.site.transactions", null, Locale.getDefault()).split(","));
            report.addRow();
            if (campaign.getDateConfirmed() != null) {
                GoogleAnalyticsQueryJson analyticsQuery = mailerCampaignService.getGoogleAnalyticsQueryJsonWithoutUserID(campaign);
                if (analyticsQuery == null) {
                    report.skipCell();
                } else {
                    GaData gaData = analyticsApiService.executeQuery(analyticsQuery);
                    if (gaData == null || gaData.getTotalsForAllResults() == null) {
                        report.skipCell(2);
                    } else {
                        String sessions = gaData.getTotalsForAllResults().get("ga:sessions");
                        String transactions = gaData.getTotalsForAllResults().get("ga:transactions");
                        try {
                            report.addCell(Long.valueOf(sessions));
                            report.addCell(Long.valueOf(transactions));
                        } catch (Exception ex) {
                            report.addCell(sessions);
                            report.addCell(transactions);
                        }
                    }
                }
            } else {
                report.addCell("");
            }

            report.skipRow();
            report.addRow(messageSource.getMessage("for.this.mailing", null, Locale.getDefault()));
            addStatsToReport(campaignStats, report);

            Map<String, CampaignStats> domainStats = getCampaignDomainStats(campaign);
            report.skipRow();

            Iterator it = domainStats.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry)it.next();
                report.skipRow();
                report.addRow(pairs.getKey());
                addStatsToReport((CampaignStats)pairs.getValue(), report);
                it.remove(); // avoids a ConcurrentModificationException
            }

            return report;
        } else {
            return null;
        }
    }

    private void addStatsToReport(CampaignStats campaignStats, SimpleReport report) {
        report.addRow(messageSource.getMessage("status.of.mailing", null, Locale.getDefault()).split(","));
        report.addRow();
        report.addCell(campaignStats.getSent());
        report.addCell(campaignStats.getSent() - campaignStats.getFailed());
        if (campaignStats.getSent() != 0) {
            report.addCell((campaignStats.getSent().doubleValue() - campaignStats.getFailed()) * 100 /campaignStats.getSent());
        } else {
            report.addCell(0);
        }
        report.addCell(campaignStats.getOpened());
        report.addCell(campaignStats.getAbuse());
        if (campaignStats.getSent() != 0) {
            report.addCell(campaignStats.getAbuse().doubleValue() * 100 /campaignStats.getSent());
        } else {
            report.addCell(0);
        }
        report.addCell(campaignStats.getFailed());
    }
}

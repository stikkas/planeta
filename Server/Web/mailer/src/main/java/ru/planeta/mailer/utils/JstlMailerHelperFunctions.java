package ru.planeta.mailer.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.planeta.mailer.model.generated.Attribute;
import ru.planeta.mailer.model.generated.EnumValue;
import ru.planeta.mailer.model.enums.AttributeType;
import ru.planeta.mailer.service.FilterService;

import java.util.List;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 17.07.12
 */
@Component
public class JstlMailerHelperFunctions {

    private static FilterService filterService;

    private static MessageSource messageSource;

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        JstlMailerHelperFunctions.messageSource = messageSource;
    }

    @Autowired
    public void setFilterService(FilterService filterService) {
        JstlMailerHelperFunctions.filterService = filterService;
    }

    public static List<EnumValue> getEnumValues(Attribute attribute) {
        List<EnumValue> enumValues = filterService.getEnumValues(attribute);
        return enumValues;
    }

    public static String getMessage(String name, AttributeType attributeType) {
        String messageName = name + (attributeType == AttributeType.DATE ? ".date" : "");
        return messageSource.getMessage(messageName, null, Locale.getDefault());
    }
}

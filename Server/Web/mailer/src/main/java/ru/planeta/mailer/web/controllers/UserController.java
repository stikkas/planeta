package ru.planeta.mailer.web.controllers;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.model.generated.User;
import ru.planeta.mailer.model.stats.MessageInfo;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 08.08.12
 */
@Controller
public class UserController extends BaseController {

    @RequestMapping(value = Urls.USER_WITH_ID, method = RequestMethod.GET)
    public String getUser(Model model,
                          @PathVariable("id") long userId,
                          @RequestParam(value = "offset", required = false, defaultValue = "0") long offset,
                          @RequestParam(value = "limit", required = false, defaultValue = "10") long limit,
                          @RequestParam(value = "query", required = false) String searchString) throws NotFoundException {

        User user = getMailerUserService().getUserById(userId);

        model.addAttribute("user", user);

        List<MessageInfo> messages = null;
        long messageCount;
        if (StringUtils.isEmpty(searchString)) {
            messages = getMessageStatsService().getUserMessages(user, offset, limit, searchString);
            messageCount = getMessageStatsService().countUserMessages(user);
            model.addAttribute("messagesContinued", false);
        } else {
            messages = getMessageStatsService().getUserMessages(user, offset, limit, searchString);
            messageCount = offset + messages.size();
            if (messages.size() > limit) {
                messages.remove((int) (limit - 1));
            }
            model.addAttribute("messagesContinued", true);
        }

        model.addAttribute("messages", messages);
        model.addAttribute("messageCount", messageCount);
        model.addAttribute("offset", offset);
        model.addAttribute("limit", limit);
        model.addAttribute("searchString", searchString);

        return Actions.USER_VIEW.getText();
    }

    @RequestMapping(value = Urls.USER_WITH_ID, method = RequestMethod.POST)
    public String postUser(Model model,
                           @PathVariable("id") long userId,
                           @RequestParam("unsubscribed") boolean unsubscribed) throws NotFoundException {

        User user = getMailerUserService().getUserById(userId);

        user.setUnsubscribed(unsubscribed);

        getMailerUserService().saveUser(user);

        return Urls.redirect(Urls.USER_WITH_ID, "id", userId);
    }

    @RequestMapping(value = Urls.USER_CHANGE_SUBSCRIBE_WITH_ID, method = RequestMethod.POST)
    public String postUserUnsubscribe(Model model,
                                      @PathVariable("id") long userId,
                                      @RequestParam("unsubscribed") boolean unsubscribed,
                                      @RequestParam(value = "offset", required = false, defaultValue = "0") int offset,
                                      @RequestParam(value = "limit", required = false, defaultValue = "20") int limit,
                                      @RequestParam(value = "searchString", required = false, defaultValue = "") String searchString) throws NotFoundException {

        User user = getMailerUserService().getUserById(userId);
        user.setUnsubscribed(unsubscribed);
        getMailerUserService().saveUser(user);
        UriComponents uriComponents =
                UriComponentsBuilder.newInstance()
                        .path(Urls.LIST_USERS)
                        .queryParam("offset", offset)
                        .queryParam("limit", limit)
                        .queryParam("searchString", searchString)
                        .build()
                        .encode();
        return Urls.redirect(uriComponents.toUriString());
    }
}

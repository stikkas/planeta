package ru.planeta.mailer.service.stats;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;
import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.model.generated.User;
import ru.planeta.mailer.model.mail.MailerMessage;
import ru.planeta.mailer.model.stats.MailerDaemonInfo;
import ru.planeta.mailer.service.BaseService;
import ru.planeta.mailer.service.MailerUserService;
import ru.planeta.mailer.service.mail.MessageService;

import javax.annotation.PostConstruct;
import javax.mail.*;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FlagTerm;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.security.Security;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 01.08.12
 */
@Service
public class BounceServiceImpl extends BaseService implements BounceService {

    @Value("${imap.user}")
    private String imapUser;

    @Value("${imap.password}")
    private String imapPassword;

    @Value("${imap.check.period}")
    private long imapCheckPeriodMs;

    @Value("${imap.url}")
    private java.net.URI imapUrl;

    @Value("${imap.enable}")
    private boolean enabled;

    @Autowired
    private MailerUserService mailerUserService;

    @Autowired
    private MessageService messageService;

    private ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @PostConstruct
    public void init() {
        if (!enabled) {
            return;
        }

        executorService.scheduleWithFixedDelay(new Runnable(){
            @Override public void run() {
                try {
                    checkMail();
                } catch (Exception e) {
                    getLogger().error("Error checking mail", e);
                }
            }
        }, 0, imapCheckPeriodMs, TimeUnit.MILLISECONDS);
    }

    @Override
    public void checkMail() throws MessagingException, IOException {
        Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());

        Properties props = new Properties();
        props.put("mail.store.protocol", imapUrl.getScheme());
        Session session = Session.getInstance(props);

        Store store = session.getStore(imapUrl.getScheme());
        try {
            store.connect(imapUrl.getHost(), imapUrl.getPort(), imapUser, imapPassword);

            String folderName = imapUrl.getPath();
            folderName = StringUtils.removeStart(folderName, "/");
            if (StringUtils.isEmpty(folderName)) {
                folderName = "INBOX";
            }

            Folder inbox = store.getFolder(folderName);
            inbox.open(Folder.READ_WRITE);
            Message[] messages = inbox.search(new FlagTerm(new Flags(Flags.Flag.SEEN), false));

            for (Message message : messages) {
                try {
                    // Fetch entire message since IMAPMessage partial fetch does not work with GMail.
                    Message entireMessage = new MimeMessage((MimeMessage)message);
                    processMessage(entireMessage);
                    message.setFlag(Flags.Flag.SEEN, true);
                } catch (Exception e) {
                    getLogger().error("error processing message.", e);
                }
            }
            inbox.close(false);
        } finally {
            store.close();
        }
    }

    private void processMessage(Message message) throws Exception {
        getLogger().info(message.getSubject());

        if (!ArrayUtils.isEmpty(message.getFrom())
                && message.getFrom()[0].toString().toLowerCase().contains("mailer")
                || message.getSubject().toLowerCase().contains("deliver")
                || message.getSubject().toLowerCase().contains("abuse report")
                || message.getSubject().toLowerCase().contains("feedback loop")) {
            processMailerDaemon(message);
            return;
        }

        Object messageContent = message.getContent();
        if (messageContent instanceof MimeMultipart) {
            MimeMultipart multipart = (MimeMultipart)messageContent;
            for (int index = 0; index < multipart.getCount(); index++) {
                BodyPart part = multipart.getBodyPart(index);
                getLogger().info(part.getContentType() + ": " + part.getFileName());
                if (part.getContentType().contains("/") && part.getContentType().toLowerCase().split("/")[1].contains("zip")) {
                    processDMARC(part);
                }
            }
        }
    }

    private void processMailerDaemon(Message message) throws IOException, MessagingException, NotFoundException {
        // Get error info
        MailerDaemonInfo info = MailerDaemonMessageParser.getMailerDaemonInfo(message);

        // Mark emails as failed or abuse, and unsubscribe user
        if (!CollectionUtils.isEmpty(info.getEmails())) {
            for (String email : info.getEmails()) {
                try {
                    User user = mailerUserService.getUserByEmailWithoutException(email);

                    String messageId = info.getMessageId();
                    if (StringUtils.isEmpty(messageId)) {
                        getLogger().error("Error while processing mailer-daemon message: empty messageId");
                        return;
                    }

                    getLogger().info("Processing mailer-daemon message, messageId " + messageId);
                    MailerMessage mailerMessage = messageService.getMessageByMessageId(messageId);
                    if (!info.isAbuse()) {
                        mailerMessage.setFailed(true);
                    } else {
                        mailerMessage.setAbuse(true);
                    }
                    messageService.saveMessage(mailerMessage);


                    if (info.getErrorCode() >= 500) {
                        if (user == null) {
                            user = mailerUserService.getUserById(mailerMessage.getUserId());
                        }
                        user.setUnsubscribed(true);
                        mailerUserService.saveUser(user);
                    }
                } catch (Exception e) {
                    getLogger().error("Error while processing mailer-daemon message", e);
                }
            }
        }
    }

    private void processDMARC(BodyPart part) throws IOException, ParserConfigurationException, SAXException, TransformerException, MessagingException {
        DmarcParser.getFailedRecords(part);
        // TODO: collect this info
    }
}

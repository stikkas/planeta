package ru.planeta.mailer.utils;

import org.apache.taglibs.standard.lang.support.ExpressionEvaluatorManager;
import org.apache.taglibs.standard.tag.common.core.NullAttributeException;
import org.springframework.util.StringUtils;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.jstl.core.ConditionalTagSupport;

/**
 *
 * User: Serge Blagodatskih<stikkas17@gmail.com><br>
 * Date: 10.08.16<br>
 * Time: 09:30
 */
public class IfTag extends ConditionalTagSupport {

    public IfTag() {
        super();
        init();
    }

    @Override
    public void release() {
        super.release();
        init();
    }

    @Override
    protected boolean condition() throws JspTagException {
        if (StringUtils.isEmpty(test)) {
            return false;
        }
        try {
            Object r = ExpressionEvaluatorManager.evaluate(
                    "test", test, Boolean.class, this, pageContext);
            if (r == null) {
                throw new NullAttributeException("if", "test");
            } else {
                return (Boolean) r;
            }
        } catch (JspException ex) {
            throw new JspTagException(ex.toString(), ex);
        }
    }

    private String test;

    public void setTest(String test) {
        this.test = test;
    }

    private void init() {
        test = null;
    }
}

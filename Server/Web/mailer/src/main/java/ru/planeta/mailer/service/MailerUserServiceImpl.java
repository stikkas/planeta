package ru.planeta.mailer.service;

import com.google.api.services.analytics.model.GaData;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Service;
import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.model.custom.AbstractValue;
import ru.planeta.mailer.model.generated.User;
import ru.planeta.mailer.model.generated.UserExample;

import javax.annotation.Nonnull;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 26.07.12
 */
@Service
public class MailerUserServiceImpl extends BaseService implements MailerUserService {

    private static final String SALT = "/eY0oolif";

    private static final int HASH_HEX_LENGTH = 40;

    private static final Hex hex = new Hex();

    @Override
    public User getUserByValue(AbstractValue value) {
        User user = getUserByEmailWithoutException(value.getEmail());
        if (user == null) {
            user = new User();
            user.setEmail(value.getEmail());
            user.setDisplayName(value.getDisplayName());
            user.setHash(generateHash(value.getEmail()));
            saveUser(user);
        }

        return user;
    }

    @Nonnull
    @Override
    public User getUserByEmail(String email) throws NotFoundException {
        User user = getUserByEmailWithoutException(email);
        if (user == null) {
            throw new NotFoundException("User not found");
        }
        return user;
    }

    @Override
    public User getUserByEmailWithoutException(String email) {
        UserExample example = new UserExample();
        example.createCriteria().andEmailEqualTo(email);
        List<User> users = getUserMapper().selectByExample(example);
        if (users.isEmpty()) {
            return null;
        }
        return users.get(0);
    }

    @Override
    public User getUserById(Long userId) throws NotFoundException {
        User user = getUserMapper().selectByPrimaryKey(userId);
        if (user == null) {
            throw new NotFoundException("User", userId);
        }
        return user;
    }

    @Override
    public List<User> getUserListByIds(List<Long> ids) {
        UserExample userExample = new UserExample();
        userExample.or().andUserIdIn(ids);

        return getUserMapper().selectByExample(userExample);
    }

    @Override
    public User getUserByCode(String code) throws NotFoundException {
        UserExample example = new UserExample();
        example.createCriteria().andHashEqualTo(getHashFromCode(code));
        List<User> users = getUserMapper().selectByExample(example);
        if (users.isEmpty()) {
            throw new NotFoundException("User not found");
        }
        return users.get(0);
    }

    @Override
    public void saveUser(User user) {
        if (user.getUserId() == null) {
            getUserMapper().insert(user);
        } else {
            getUserMapper().updateByPrimaryKey(user);
        }
    }

    private static String generateHash(String email) {
        return new String(hex.encode(DigestUtils.sha(email + SALT)), StandardCharsets.UTF_8);
    }

    private static String getHashFromCode(String code) throws NotFoundException {
        if (code.length() < HASH_HEX_LENGTH) {
            throw new NotFoundException("code not found");
        }
        return code.substring(0, HASH_HEX_LENGTH);
    }

    @Override
    public long getCampaignIdByCode(String code) throws DecoderException {
        return Long.parseLong(code.substring(HASH_HEX_LENGTH), 16);
    }

    @Override
    public String getUnsubscribeCode(String email, long campaignId) throws NotFoundException {
        return getUserByEmail(email).getHash() + Long.toHexString(campaignId);
    }

    @Override
    public List<String> getEmailLists(Set<Long> userIds) {
        List<Long> userIdsList = new ArrayList<Long>(userIds);
        List<String> resultEmails = new ArrayList<String>();
        if (userIdsList.size() > 0) {
            for (User user : getUserListByIds(userIdsList)) {
                resultEmails.add(user.getEmail());
            }
        }
        return resultEmails;
    }

    @Override
    public HashSet<Long> getUserIds(GaData gaData) {
        HashSet<Long> users = new HashSet<Long>();

        if (gaData.getTotalResults() <= 0) {
            return users;
        }

        int userIdIndex = -1;
        int i = 0;
        for (GaData.ColumnHeaders header : gaData.getColumnHeaders()) {
            if (header.getName().equals("ga:campaign")) {
                userIdIndex = i;
            }
            i++;
        }
        if (userIdIndex < 0) {
            return users;
        }

        for (List<String> row : gaData.getRows()) {
            try {
                users.add(Long.parseLong(row.get(userIdIndex)));
            } catch (Exception ex) {
                getLogger().error("[Mailer] Failed to convert String to Long!", ex);
            }
        }

        return users;
    }

    @Override
    public List<User> getUsers(String userName, int offset, int limit) {
        UserExample example = new UserExample();
        example.createCriteria().andEmailLikeInsensitive("%" + userName + "%");
        example.setOrderByClause("user_id DESC");
        return getUserMapper().selectByExampleWithRowbounds(example, getRowBounds(offset, limit));
    }

    @Override
    public int getUsersCount(String userName) {
        UserExample example = new UserExample();
        example.createCriteria().andEmailLikeInsensitive("%" + userName + "%");
        return getUserMapper().countByExample(example);
    }

}

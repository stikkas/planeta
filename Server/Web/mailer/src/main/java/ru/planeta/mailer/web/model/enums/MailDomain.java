package ru.planeta.mailer.web.model.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * Mail domains enum
 *
 * @author: ds.kolyshev
 * Date: 05.03.13
 */
public enum MailDomain {
    MAIL_RU("mail.ru"),
    BK_RU("bk.ru"),
    LIST_RU("list.ru"),
    INBOX_RU("inbox.ru"),
    YANDEX_RU("yandex.ru"),
    GMAIL_COM("gmail.com"),
    YAHOO_COM("yahoo.com"),
    HOTMAIL_COM("hotmail.com"),
    LIVE_RU("live.ru");

    private final String code;

    MailDomain(String code) {
        this.code = code;
    }

    public String getCode() {
        return "@" + code;
    }


    public static List<String> getCodeList() {
        List<String> codeList = new ArrayList<>();
        for (MailDomain mailDomain : values()) {
            codeList.add(mailDomain.getCode());
        }
        return codeList;
    }
}

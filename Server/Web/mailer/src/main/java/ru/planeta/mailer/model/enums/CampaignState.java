package ru.planeta.mailer.model.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: s.fionov
 * Date: 10.07.12
 */
public enum CampaignState {
    NOT_RUNNING(0), RUNNING(1), FAILED(2), FINISHED(3), IN_QUEUE(4), PAUSED(5);

    private int code;

    private CampaignState(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }

    private static final Map<Integer, CampaignState> lookup = new HashMap<Integer, CampaignState>();

    static {
        for (CampaignState s : values()) {
            lookup.put(s.getCode(), s);
        }
    }

    public static CampaignState getByValue(int code) {
        return lookup.get(code);
    }
}

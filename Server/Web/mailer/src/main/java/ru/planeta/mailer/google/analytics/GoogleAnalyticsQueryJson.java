package ru.planeta.mailer.google.analytics;

/**
 * User: s.makarov
 * Date: 28.07.14
 * Time: 18:55
 */
public class GoogleAnalyticsQueryJson {
    private String jsonrpc = "2.0";
    private String id = "gapiRpc";
    private String method = "analytics.data.ga.get";
    private GoogleAnalyticsQueryParams params;
    private String apiVersion = "v3";

    public String getJsonrpc() {
        return jsonrpc;
    }

    public void setJsonrpc(String jsonrpc) {
        this.jsonrpc = jsonrpc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public GoogleAnalyticsQueryParams getParams() {
        return params;
    }

    public void setParams(GoogleAnalyticsQueryParams params) {
        this.params = params;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }
}

package ru.planeta.mailer.model.stats;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 08.08.12
 */
public class CampaignStats {
    private Long campaignId;
    private String domainName = "";
    private Long addresses  = 0L;
    private Long sent = 0L;
    private Long opened = 0L;
    private Long failed = 0L;
    private Long unsubscribed = 0L;
    private Long abuse = 0L;

    public Long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Long campaignId) {
        this.campaignId = campaignId;
    }

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public Long getAddresses() {
        return addresses;
    }

    public void setAddresses(Long addresses) {
        this.addresses = addresses;
    }

    public Long getSent() {
        return sent;
    }

    public void setSent(Long sent) {
        this.sent = sent;
    }

    public Long getFailed() {
        return failed;
    }

    public void setFailed(Long failed) {
        this.failed = failed;
    }

    public Long getUnsubscribed() {
        return unsubscribed;
    }

    public void setUnsubscribed(Long unsubscribed) {
        this.unsubscribed = unsubscribed;
    }

    public Long getOpened() {
        return opened;
    }

    public void setOpened(Long opened) {
        this.opened = opened;
    }

    public Long getUnopened() {
        return sent - opened;
    }

    public Long getAbuse() {
        return abuse;
    }

    public void setAbuse(Long abuse) {
        this.abuse = abuse;
    }
}

package ru.planeta.mailer.model.enums;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 16.07.12
 */
public enum Predicate {
    LESS(0, "predicate.less", AttributeType.LONG, AttributeType.DATE),
    GREATER(1, "predicate.greater", AttributeType.LONG, AttributeType.DATE),
    EQUALS(2, "predicate.equals", AttributeType.LONG, AttributeType.STRING, AttributeType.ENUM),
    NOT_EQUALS(3, "predicate.not.equals", AttributeType.LONG, AttributeType.STRING, AttributeType.ENUM),
    CONTAINS(4, "predicate.contains", AttributeType.STRING, AttributeType.MULTI),
    NOT_CONTAINS(5, "predicate.not.contains", AttributeType.STRING, AttributeType.MULTI);

    private int code;
    private List<AttributeType> attributeTypes = new ArrayList<AttributeType>();
    private String text;

    private Predicate(int code, String text, AttributeType ... types) {
        this.code = code;
        this.text = text;
        Collections.addAll(attributeTypes, types);
    }

    public int getCode() {
        return code;
    }

    public List<AttributeType> getAttributeTypes() {
        return attributeTypes;
    }

    private static final Map<Integer, Predicate> lookup = new HashMap<Integer, Predicate>();

    private static final Map<AttributeType, List<Predicate>> lookupByType = new HashMap<AttributeType, List<Predicate>>();

    static {
        for (Predicate s : values()) {
            lookup.put(s.getCode(), s);
            for (AttributeType t : s.getAttributeTypes()) {
                List<Predicate> list = lookupByType.get(t);
                if (list == null) {
                    list = new ArrayList<Predicate>();
                }
                list.add(s);
                lookupByType.put(t, list);
            }
        }
    }

    public static Predicate getByValue(int code) {
        return lookup.get(code);
    }

    public static List<Predicate> getByAttributeType(AttributeType type) {
        return lookupByType.get(type);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}

package ru.planeta.mailer.dao.mail;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import ru.planeta.mailer.model.mail.MailerMessage;

import java.util.Date;
import java.util.List;

@Repository
public interface MailerMessageMapper {

    int deleteByPrimaryKey(Long mailerMessageId);

    int insert(MailerMessage record);

    List<MailerMessage> selectByUserId(@Param("userId") Long userId,
                                       @Param("offset") long offset,
                                       @Param("limit") long limit,
                                       @Param("searchString") String searchString);

    List<MailerMessage> selectByCampaignId(@Param("campaignId") Long campaignId,
                                           @Param("offset") long offset,
                                           @Param("limit") long limit,
                                           @Param("searchString") String searchString,
                                           @Param("open") Boolean isOpen,
                                           @Param("failed") Boolean isFailed,
                                           @Param("unsubscribed") Boolean isUnsubscribed,
                                           @Param("abuse") Boolean isAbuse);

    MailerMessage selectByUserIdAndCampaignId(@Param("userId") Long userId,
                                              @Param("campaignId") Long campaignId);

    long countByCampaignId(@Param("campaignId") Long campaignId,
						   @Param("domainName") String domainName,
                           @Param("open") Boolean isOpen,
                           @Param("failed") Boolean isFailed,
                           @Param("unsubscribed") Boolean isUnsubscribed,
                           @Param("abuse") Boolean isAbuse);

    /* myBatis denied method overloading since 3.1.0 */
    long countByParameters(@Param("domainName") String domainName,
						   @Param("open") Boolean isOpen,
						   @Param("failed") Boolean isFailed,
						   @Param("unsubscribed") Boolean isUnsubscribed,
						   @Param("abuse") Boolean isAbuse,
						   @Param("fromDate") Date fromDate,
						   @Param("toDate") Date toDate);

    long countByUserId(Long userId);

    MailerMessage selectByPrimaryKey(Long mailerMessageId);

    MailerMessage selectByMessageId(String messageId);

    int updateByPrimaryKey(MailerMessage record);

}
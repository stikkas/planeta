package ru.planeta.mailer.service.mail;

import com.sun.mail.smtp.SMTPMessage;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.model.generated.User;
import ru.planeta.mailer.model.mail.MailerMessage;
import ru.planeta.mailer.service.MailerUserService;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.annotation.PostConstruct;
import javax.mail.*;
import javax.mail.internet.*;
import java.io.File;
import java.security.Security;
import java.util.*;

/**
 * @author ds.kolyshev
 * Date: 12.01.12
 */
@Service
public class MailerMailServiceImpl implements MailerMailService {

    private static Logger log = Logger.getLogger(MailerMailServiceImpl.class);
    private String host;
    private int port;
    private String user;
    private String password;

    private static final int NUM_TRANSPORTS = 30;
    private static final int NUM_SSL_TRANSPORTS = 1;
    private static final int CONNECTION_TIMEOUT = 60000;
    private static final int DATA_TIMEOUT = 60000;
    private static final int CHECK_INTERVAL_MS = 60000 * 5;
    private static final int IDLE_INTERVAL_MS = CHECK_INTERVAL_MS * 2;

    private Session session;
    private Session sslSession;

    /** Lock for protecting transports */
    private final Object transportsSyncLock = new Object();
    private List<MailTransport> transports = new ArrayList<>();
    private List<MailTransport> sslTransports = new ArrayList<>();
    private int lastTransportIndex;

    private final MessageService messageService;
    private final MailerUserService mailerUserService;

    @Autowired
    public MailerMailServiceImpl(MessageService messageService, MailerUserService mailerUserService) {
        this.messageService = messageService;
        this.mailerUserService = mailerUserService;
    }

    @Value("${mail.host}")
    public void setHost(String host) {
        this.host = host;
    }

    @Value("${mail.port}")
    public void setPort(int port) {
        this.port = port;
    }

    @Value("${mail.user}")
    public void setUser(String user) {
        this.user = user;
    }

    @Value("${mail.password}")
    public void setPassword(String password) {
        this.password = password;
    }

    private Session createSession(boolean ssl) {

        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.connectiontimeout", CONNECTION_TIMEOUT);
        props.put("mail.smtp.timeout", DATA_TIMEOUT);
        Session session = Session.getInstance(props);

        if (isAuthenticated()) {
            props.put("mail.smtp.user", user);
            props.put("mail.smtp.auth", "true");

            if (ssl) {
                Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
                props.put("mail.smtp.socketFactory.port", port);
                props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
                props.put("mail.smtp.socketFactory.fallback", "false");
                props.put("mail.smtp.ssl", "true");
            }


            Authenticator authenticator = new SMTPAuthenticator(user, password);
            session = Session.getDefaultInstance(props, authenticator);
        }

        return session;
    }

    @PostConstruct
    public void init() {
        // create normal session
        this.session = createSession(false);
        // create SSL session
        this.sslSession = createSession(true);
    }

    private void closeIdleTransports(List<MailTransport> list) {
        long criticalTime = new Date().getTime() - IDLE_INTERVAL_MS;
        synchronized (transportsSyncLock) {
            int count = 0;
            for (MailTransport transport : list) {
                if (transport != null && transport.getLastUsed().getTime() < criticalTime) {
                    try {
                        transport.close();
                        ++count;
                    } catch (MessagingException ignored) {
                    }
                }
            }
            log.debug("Closed " + count);
        }
    }

    @Scheduled(fixedDelay = CHECK_INTERVAL_MS)
    protected void closeIdleTransports() {
        closeIdleTransports(transports);
        closeIdleTransports(sslTransports);
        log.debug("cleanup");
    }

    private static Logger getLogger() {
        return log;
    }

    private boolean isAuthenticated() {
        return !StringUtils.isEmpty(user) && !StringUtils.isEmpty(password);
    }

    private static MimeMessage composeMessage(MailerMessage mailerMessage, Session session) throws MessagingException {
        MimeMessage message = new MailMimeMessage(session);

        InternetAddress fromAddress = new InternetAddress(mailerMessage.getFrom());

        if (mailerMessage.getUnsubscribeLink() != null) {
            message.addHeader("X-List-Unsubscribe", "<" + mailerMessage.getUnsubscribeLink() + ">");
        }
        if (mailerMessage.getCampaignId() != null) {
            message.addHeader("X-Mailru-Msgtype", "planeta-" + mailerMessage.getCampaignId().toString());
        }

        message.addHeader("Precedence", "bulk");

        MimeMultipart multipart = createMimeMultipart(mailerMessage.getContentHtml(), mailerMessage.getMultipartFiles());

        message.setFrom(fromAddress);
        message.setSubject(mailerMessage.getSubject());
        message.setContent(multipart);
        message.saveChanges();

        SMTPMessage smtpMessage = new SMTPMessage(message);

        for (String to : mailerMessage.getTo()) {
            InternetAddress toAddress = new InternetAddress(to);
            if (mailerMessage.getCampaignId() != null) {
                smtpMessage.setEnvelopeFrom("bounce-" + mailerMessage.getCampaignId().toString() + "+" + toAddress.getAddress().replace("@", "="));
            }
            smtpMessage.addRecipient(Message.RecipientType.TO, toAddress);
        }

        return smtpMessage;
    }

    private MailTransport getTransport(MailerMessage mailerMessage) {
        // get transport-states list
        List<MailTransport> list;
        Session session;
        int maxStates;
        if (isAuthenticated() && mailerMessage.isSendViaSmtps()) {
            list = sslTransports;
            session = this.sslSession;
            maxStates = NUM_SSL_TRANSPORTS;
        } else {
            list = transports;
            session = this.session;
            maxStates = NUM_TRANSPORTS;
        }
        // get random transport or create new
        MailTransport transport;
        if (list.size() < maxStates) {
            transport = new MailTransport(session, host, port, user, password);
            synchronized (transportsSyncLock) {
                list.add(transport);
            }
        } else {
            synchronized (transportsSyncLock) {
                transport = list.get(lastTransportIndex % maxStates);
                lastTransportIndex = (lastTransportIndex + 1) % (Integer.MAX_VALUE - 1);
            }
        }
        transport.updateLastUsed();
        return transport;
    }

    @Override
    public void send(MailerMessage mailerMessage) throws MessagingException, NotFoundException {

        MailTransport transport = getTransport(mailerMessage);

        MimeMessage message = composeMessage(mailerMessage, transport.getSession());

        log.debug("Mime message prepared");

        mailerMessage.setMessageId(message.getMessageID());
        messageService.saveMessage(mailerMessage);

        log.debug("sending mail message... " + transport.toString() + " " + mailerMessage.getTo().toString());
        boolean tryToSend = true;
        while (tryToSend) {
            tryToSend = false;
            try {
                mailerMessage.setFailed(false);
                if (!transport.isConnected()) {
                    transport.connect();
                    log.debug("Transport reconnect");
                }

                transport.sendMessage(message, message.getAllRecipients());
            }
            catch (MessagingException e) {
                getLogger().error("message send transport error", e);
                if (e instanceof SendFailedException)  {
                    SendFailedException ex = (SendFailedException) e;
                    if (ex.getInvalidAddresses().length > 0) {
                        unsubscribeInvalidAddresses(((SendFailedException) e).getInvalidAddresses());
                    }
                    if (ex.getValidUnsentAddresses().length > 0) {
                        mailerMessage.incAttempt();
                        tryToSend = mailerMessage.getAttempt() <= 3;
                    }
                }
                mailerMessage.setFailed(true);
                transport.close();
            }
        }

        mailerMessage.setDateSent(new Date());
        messageService.saveMessage(mailerMessage);

        log.debug("Mail message sent");
    }

    private static MimeMultipart createMimeMultipart(String contentHtml, Map<String, File> multipartFiles) throws MessagingException {
        // Create a related multi-part to combine the parts
        MimeMultipart multipart = new MimeMultipart("related");
        // Create your new message part
        multipart.addBodyPart(createContentBodyPart(contentHtml));
        if (multipartFiles != null) {
            for (Map.Entry<String, File> entry : multipartFiles.entrySet()) {
                multipart.addBodyPart(createFileBodyPart(entry.getKey(), entry.getValue()));
            }
        }
        return multipart;
    }

    private static BodyPart createContentBodyPart(String contentHtml) throws MessagingException {
        // Create your new message part
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(contentHtml, "text/html; charset=\"utf-8\"");
        return messageBodyPart;
    }

    private static BodyPart createFileBodyPart(String contentId, File file) throws MessagingException {

        BodyPart bodyPart = new MimeBodyPart();
        DataSource fds = new FileDataSource(file);
        bodyPart.setDataHandler(new DataHandler(fds));
        bodyPart.setDisposition(Part.INLINE);
        bodyPart.setHeader("Content-ID", contentId);
        bodyPart.setFileName(file.getName());
        return bodyPart;
    }

    private static class SMTPAuthenticator extends Authenticator {

        private final String username;
        private final String password;

        SMTPAuthenticator(String username, String password) {
            this.username = username;
            this.password = password;
        }

        @Override
        protected PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(username, password);
        }
    }
    private void unsubscribeInvalidAddresses(Address[] invalidAddresses) throws AddressException, NotFoundException {
        if (invalidAddresses.length > 0) {
            for (Address adr : invalidAddresses) {
                User user = mailerUserService.getUserByEmail(adr.toString());
                user.setUnsubscribed(true);
                mailerUserService.saveUser(user);
            }
        }
    }

}

package ru.planeta.mailer.model.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.mailer.model.generated.Campaign;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 19.07.12
 */
@Component
public class CampaignValidator implements Validator {

    public boolean supports(Class<?> type) {

        return type.isAssignableFrom(Campaign.class);
    }

    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "name", "name.empty.Campaign");
        ValidationUtils.rejectIfEmpty(errors, "filterListId", "list.empty.Campaign");
        ValidationUtils.rejectIfEmpty(errors, "text", "text.empty.Campaign");
        ValidationUtils.rejectIfEmpty(errors, "messageFrom", "from.empty.Campaign");
        ValidationUtils.rejectIfEmpty(errors, "subject", "subject.empty.Campaign");

        try {
            InternetAddress address = new InternetAddress((String)errors.getFieldValue("messageFrom"));
            address.validate();
        } catch (AddressException e) {
            errors.rejectValue("messageFrom", "from.invalid.Campaign");
        }

        if ((! ((String)errors.getFieldValue("text")).contains("{{unsubscribe}}")) &&
                (! ((String)errors.getFieldValue("text")).contains("{{unsubscribeLink}}"))) {
            errors.rejectValue("text", "text.dont.have.unsubscribe");
        }
    }
}

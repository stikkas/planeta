package ru.planeta.mailer.dao.custom;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import ru.planeta.mailer.model.custom.ValueExtended;

import java.util.List;

@Repository
public interface MailUserMapper {
    List<ValueExtended> select(@Param("offset") int offset, @Param("limit") int limit);
}

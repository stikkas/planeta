package ru.planeta.mailer.service.mail;

import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.model.mail.MailerMessage;

import javax.mail.MessagingException;

/**
 * @author ds.kolyshev
 * Date: 12.01.12
 */
public interface MailerMailService {
    void send(MailerMessage mailerMessage) throws MessagingException, NotFoundException;
}

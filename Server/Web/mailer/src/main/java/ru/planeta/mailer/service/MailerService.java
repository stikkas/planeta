package ru.planeta.mailer.service;

import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.model.generated.Campaign;
import ru.planeta.mailer.model.generated.Template;
import ru.planeta.mailer.model.generated.User;
import ru.planeta.mailer.model.enums.CampaignState;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 12.07.12
 */
public interface MailerService {
    /**
     * Returns a message preview
     * @param campaign
     * @return
     */
    String previewMessageHtml(Campaign campaign);

    String previewMessageHtml(Template template);

    String getMessageHtml(Campaign campaign, User user) throws NotFoundException;

    /**
     * Sends the campaign
     * @param campaign
     */
    void sendCampaign(Campaign campaign) throws NotFoundException;

	/**
	 * Stops campaign
	 *
	 * @param campaign
	 */
	void stopCampaign(Campaign campaign);

    /**
     * Get campaign state
     * @param campaign
     * @return
     */
    CampaignState getCampaignState(Campaign campaign);

	/**
	 * Gets map of states for campaigns list
	 * @param campaigns
	 * @return
	 */
	Map<Long, CampaignState> getCampaignsStates(List<Campaign> campaigns);

    /**
     * Set campaign state
     * @param campaign
     * @param campaignState
     */
    void setCampaignState(Campaign campaign, CampaignState campaignState);
}

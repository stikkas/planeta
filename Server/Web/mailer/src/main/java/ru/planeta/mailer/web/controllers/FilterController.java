package ru.planeta.mailer.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.model.custom.LiteFilterList;
import ru.planeta.mailer.model.generated.Attribute;
import ru.planeta.mailer.model.custom.Filter;
import ru.planeta.mailer.model.custom.FilterList;
import ru.planeta.mailer.model.generated.EnumValue;
import ru.planeta.mailer.model.generated.Template;

import javax.validation.Valid;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 13.07.12
 */
@Controller
public class FilterController extends BaseController {

    @RequestMapping(value = Urls.LIST_INDEX, method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("lists", getFilterService().getFilterLists());
        return Actions.LISTS.getText();
    }

    @RequestMapping(value = Urls.LIST_WITH_ID_EDIT, method = RequestMethod.GET)
    public String listParam(Model model,
                            @PathVariable(value = "id") Long id) throws NotFoundException {
        FilterList filterList = getFilterService().getFilterListById(id);
        model.addAttribute("filterList", filterList);

        return list(model, filterList, null);
    }

    @RequestMapping(value = Urls.LIST_EDIT, method = RequestMethod.GET)
    public String list(Model model,
                       @ModelAttribute FilterList filterList,
                       BindingResult filterListErrors) throws NotFoundException {

        List<Attribute> attributes = getFilterService().getAttributes();

        model.addAttribute("filterList", filterList);
        model.addAttribute("attributes", attributes);
        model.addAttribute("emailsCount", getFilterService().countValues(filterList, null, null, null));
        model.addAttribute("emails", getFilterService().getValues(filterList, 0, 100));

        return Actions.LIST_EDIT.getText();
    }

    @RequestMapping(value = Urls.LIST_EMAILS, method = RequestMethod.POST)
    public String emails(Model model,
                         @ModelAttribute FilterList filterList,
                         BindingResult filterListResult) {

        getFilterService().prepareFilterList(filterList);

        model.addAttribute("filterList", filterList);
        model.addAttribute("emails", getFilterService().getValues(filterList, 0, 100));
        model.addAttribute("emailsCount", getFilterService().countValues(filterList, null, null, null));

        return Actions.LIST_EMAILS.getText();
    }

    @RequestMapping(value = {Urls.LIST_EDIT, Urls.LIST_WITH_ID_EDIT}, method = RequestMethod.POST)
    public String listSave(Model model,
                           @ModelAttribute @Valid FilterList filterList,
                           BindingResult filterListResult) throws NotFoundException {

        getFilterService().prepareFilterList(filterList);

        if (!filterListResult.hasErrors()) {
            getFilterService().saveList(filterList);
            return Urls.redirect(Urls.LIST_WITH_ID_EDIT, "id", filterList.getFilterListId());
        }

        return list(model, filterList, filterListResult);
    }

    @RequestMapping(value = Urls.LIST_ATTRIBUTE_INFO, method = RequestMethod.GET)
    public String getAttributeInfo(Model model,
                                   @RequestParam(value = "attrId", required = false) Long attributeId,
                                   @RequestParam(value = "index", required = true) Long index) throws NotFoundException {
        List<Attribute> attributes = getFilterService().getAttributes();
        Filter filter = new Filter();

        if (attributeId != null) {
            filter.setFilteredAttribute(getFilterService().getAttributeById(attributeId));
        }

        model.addAttribute("index", index);
        model.addAttribute("filter", filter);
        model.addAttribute("attributes", attributes);

        return Actions.LIST_ATTRIBUTE_INFO.getText();
    }

    @RequestMapping(value = Urls.LIST_WITH_ID_DELETE, method = RequestMethod.GET)
    public String deleteList(Model model,
                             @PathVariable("id") Long id,
                             @ModelAttribute FilterList filterList) throws NotFoundException {

        filterList = getFilterService().getFilterListById(id);
        model.addAttribute("filterList", filterList);
        return Actions.LIST_DELETE.getText();
    }

    @RequestMapping(value = Urls.LIST_WITH_ID_DELETE, method = RequestMethod.POST)
    public String deleteListPost(@PathVariable("id") Long id) throws NotFoundException {
        getFilterService().deleteList(id);
        return Urls.redirect(Urls.LIST_INDEX);
    }

    @RequestMapping(value = Urls.GET_ENUM_LIST, method = RequestMethod.GET)
    @ResponseBody
    public List<EnumValue> getEnumList(Attribute attribute,
                                       @RequestParam(value = "offset", required = false, defaultValue = "0") int offset,
                                       @RequestParam(value = "limit", required = false, defaultValue = "20") int limit,
                                       @RequestParam(value = "query", required = false, defaultValue = "") String query
    ) throws NotFoundException {

        return getFilterService().getEnumValues(attribute, query, offset, limit);
    }

    @RequestMapping(value = Urls.GET_FILTER_LIST, method = RequestMethod.GET)
    @ResponseBody
    public List<LiteFilterList> getFilterList(@RequestParam(value = "offset", required = false, defaultValue = "0") int offset,
                                              @RequestParam(value = "limit", required = false, defaultValue = "20") int limit,
                                              @RequestParam(value = "query", required = false, defaultValue = "") String query
    ) throws NotFoundException {
        final List<FilterList> filterLists = getFilterService().getFilterLists(query, offset, limit);
        return LiteFilterList.convert(filterLists);
    }

    @RequestMapping(value = Urls.GET_TEMPLATE_LIST, method = RequestMethod.GET)
    @ResponseBody
    public List<Template> getTemplateList(@RequestParam(value = "offset", required = false, defaultValue = "0") int offset,
                                          @RequestParam(value = "limit", required = false, defaultValue = "20") int limit,
                                          @RequestParam(value = "query", required = false, defaultValue = "") String query
    ) throws NotFoundException {
        return getTemplateService().getTemplates(query, offset, limit);
    }

}

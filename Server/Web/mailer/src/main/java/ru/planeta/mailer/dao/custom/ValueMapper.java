package ru.planeta.mailer.dao.custom;

import org.springframework.stereotype.Repository;
import ru.planeta.mailer.model.custom.Value;
import ru.planeta.mailer.model.custom.ValueExtended;

import java.util.List;
import java.util.Map;

@Repository
public interface ValueMapper {
    /**
     * Select Values by specified clause
     * @param params(whereClause)
     * @param params(offset)
     * @param params(limit)
     * @return
     */
    List<Value> selectByWhereClause(Map params);

    void insertValue(ValueExtended valueExtended);

    long countByWhereClause(Map params);
}
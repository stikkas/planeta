package ru.planeta.mailer.service;

import com.csvreader.CsvReader;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.model.generated.Attribute;
import ru.planeta.mailer.model.generated.AttributeExample;
import ru.planeta.mailer.model.generated.EnumValue;
import ru.planeta.mailer.model.generated.EnumValueExample;
import ru.planeta.mailer.model.custom.*;
import ru.planeta.mailer.model.enums.AttributeType;
import ru.planeta.mailer.model.enums.FilterType;
import ru.planeta.mailer.model.enums.Predicate;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by IntelliJ IDEA.
 * User: s.fionov
 * Date: 10.07.12
 */
@Service
public class FilterServiceImpl extends BaseService implements FilterService {

    private int stringParamNumber = 1;
    private static final Logger log = Logger.getLogger(FilterServiceImpl.class);

    private ExecutorService executorService = Executors.newFixedThreadPool(2);

    @Override
    public List<Attribute> getAttributes() {
        AttributeExample attributeExample = new AttributeExample();
        attributeExample.setOrderByClause("attribute_id");
        return getAttributeMapper().selectByExample(attributeExample);
    }

    @Override
    public List<EnumValue> getEnumValues(Attribute attribute) {
        return getEnumValues(attribute, null, 0, 0);
    }

    @Override
    public List<EnumValue> getEnumValues(Attribute attribute, String query, int offset, int limit) {

        EnumValueExample enumValueExample = new EnumValueExample();
        if (StringUtils.equals(attribute.getName(), "region")) {
            enumValueExample.setOrderByClause("text_value");
        }
        EnumValueExample.Criteria criteria = enumValueExample.or();
        criteria.andAttributeIdEqualTo(attribute.getAttributeId());
        if (StringUtils.isNotBlank(query)) {
            query = StringUtils.normalizeSpace(query).replace(' ', '%');
            criteria.andTextValueLikeInsensitive("%" + query + "%");
        }

        List<EnumValue> enumValues = getEnumValueMapper().selectByExampleWithRowbounds(enumValueExample, getRowBounds(offset, limit));
        if (enumValues != null && AttributeType.MULTI.equals(attribute.getType())) {
            Collections.sort(enumValues, new Comparator<EnumValue>() {
                @Override
                public int compare(EnumValue o1, EnumValue o2) {
                    return o1.getTextValue().compareToIgnoreCase(o2.getTextValue());
                }
            });
        }
        return enumValues;
    }

    private String buildWhereClause(List<String> whereClauses, FilterType filterType) {
        if (whereClauses == null || whereClauses.isEmpty()) {
            // not use empty string here
            return null;
        }
        StringBuilder whereBuilder = new StringBuilder();
        boolean first = true;
        for (String clause : whereClauses) {
            if (first) {
                first = false;
                whereBuilder.append("WHERE ");
            } else {
                whereBuilder.append(" ").append(filterType.name()).append(" ");
            }
            whereBuilder.append(clause);
        }
        return whereBuilder.toString();
    }

    private String newParam(Map<String, Object> params, Object param) {
        String paramName = "param" + stringParamNumber;
        ++stringParamNumber;
        params.put(paramName, param);
        return "#{" + paramName + "}";
    }

    private static Object getObjectByAttributeType(Filter filter) throws ParseException {
        Object param;
        if (filter.getFilteredAttribute().getType() == AttributeType.DATE) {
            param = new SimpleDateFormat("d-M-y").parse(filter.getObject());
        } else if (filter.getFilteredAttribute().getType() == AttributeType.LONG) {
            param = Long.parseLong(filter.getObject());
        } else {
            param = filter.getObject();
        }
        return param;
    }

    private static String getFilterWhereClause(Filter filter, String sqlParameter) throws NotFoundException {
        if (filter.getPredicate() == Predicate.GREATER) {
            return filter.getFilteredAttribute().getName() + " >= " + sqlParameter;
        }

        if (filter.getPredicate() == Predicate.LESS) {
            return filter.getFilteredAttribute().getName() + " <= " + sqlParameter;
        }

        if (filter.getPredicate() == Predicate.EQUALS) {
            return filter.getFilteredAttribute().getName() + " = " + sqlParameter;
        }

        if (filter.getPredicate() == Predicate.NOT_EQUALS) {
            return filter.getFilteredAttribute().getName() + " != " + sqlParameter;
        }

        if (filter.getPredicate() == Predicate.CONTAINS) {
            if (filter.getFilteredAttribute().getType() == AttributeType.STRING) {
                return filter.getFilteredAttribute().getName() + " LIKE '%' || " + sqlParameter + " || '%'";
            } else if (filter.getFilteredAttribute().getType() == AttributeType.ENUM) {
                return filter.getFilteredAttribute().getName() + " = " + sqlParameter;
            } else if (filter.getFilteredAttribute().getType() == AttributeType.MULTI) {
                return sqlParameter + " = ANY(" + filter.getFilteredAttribute().getName() + ")";
            }
        }

        if (filter.getPredicate() == Predicate.NOT_CONTAINS) {
            if (filter.getFilteredAttribute().getType() == AttributeType.STRING) {
                return filter.getFilteredAttribute().getName() + " NOT LIKE '%' || " + sqlParameter + " || '%'";
            } else if (filter.getFilteredAttribute().getType() == AttributeType.ENUM) {
                return filter.getFilteredAttribute().getName() + " != " + sqlParameter;
            } else if (filter.getFilteredAttribute().getType() == AttributeType.MULTI) {
                return "NOT (" + sqlParameter + " = ANY(" + filter.getFilteredAttribute().getName() + "))";
            }
        }

        throw new NotFoundException("Unknown predicate in filter");
    }

    private Map<String, Object> getListClauses(FilterList filterList) {
        Map<String, Object> params = new HashMap<>();
        List<String> whereClauses = new ArrayList<>();
        if (filterList != null) {
            if (filterList.getFilters() != null) {
                fillFilters(filterList.getFilters());
                for (Filter filter : filterList.getFilters()) {
                    if (filter != null
                            && filter.getFilteredAttribute() != null
                            && filter.getObject() != null) {

                        try {
                            Object param = getObjectByAttributeType(filter);
                            whereClauses.add(getFilterWhereClause(filter, newParam(params, param)));
                        } catch (Exception e) {
                            log.error("Error while processing filter, skipping", e);
                        }
                    }
                }
            }
            params.put("whereClause", buildWhereClause(whereClauses, filterList.getFilterType()));
        }
        return params;
    }

    @Override
    public List<? extends AbstractValue> getValues(FilterList filterList, long offset, long limit) {
        if (FilterType.FILE.equals(filterList.getFilterType())) {
            return getFileValues(filterList, offset, limit);
        }

        Map<String, Object> params = getListClauses(filterList);
        params.put("offset", offset);
        params.put("limit", limit);
        return getValueMapper().selectByWhereClause(params);
    }

    @Override
    public LoadedValue getFirstFileValue(FilterList filterList) {
        if (filterList != null && FilterType.FILE.equals(filterList.getFilterType())) {
            List<LoadedValue> values = getFileValues(filterList, 0, 1);
            if (values == null || values.isEmpty()) {
                return null;
            }
            return values.get(0);
        } else {
            return null;
        }
    }

    private List<LoadedValue> getFileValues(FilterList filterList, long offset, long limit) {
        Map<String, Object> params = new HashMap<>();
        params.put("filterListId", filterList.getFilterListId());
        params.put("offset", offset);
        params.put("limit", limit);
        return getLoadedValueMapper().selectByFilterListId(params);
    }

    @Override
    public long countValues(FilterList filterList, String domainName, Date fromDate, Date toDate) {
        if (filterList != null && FilterType.FILE.equals(filterList.getFilterType())) {
            Map<String, Object> params = new HashMap<>();
            params.put("filterListId", filterList.getFilterListId());
            params.put("domainName", domainName);
            return getLoadedValueMapper().countByFilterListId(params);
        }

        Map<String, Object> params;
        if (filterList != null) {
            params = getListClauses(filterList);
        } else {
            params = new HashMap<>();
        }

        params.put("domainName", domainName);
        params.put("fromDate", fromDate);
        params.put("toDate", toDate);

        return getValueMapper().countByWhereClause(params);
    }

    @Override
    public List<String[]> parseDataFromCSVFile(File file) throws IOException {

        List<String[]> result = new ArrayList<>();
        CsvReader reader = new CsvReader(new FileReader(file), ';');
        while (reader.readRecord()) {
            result.add(reader.getValues());
        }
        reader.close();

        return result;
    }

    @Override
    public FilterList createListFromCSVFile(File file) throws IOException {
        FilterList filterList = new FilterList();
        filterList.setFilterType(FilterType.FILE);
        filterList.setName("LOADING:" + file.getName());
        getFilterListMapper().insert(filterList);

        final List<String[]> loadedValues = parseDataFromCSVFile(file);
        final String fileName = file.getName();
        final long filterListId = filterList.getFilterListId();

        executorService.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    insertLoadedValues(loadedValues, fileName, filterListId);
                } catch (NotFoundException e) {
                    log.error("Error insert", e);
                }
            }
        });

        return filterList;
    }

    private void insertLoadedValues(List<String[]> loadedValues, String fileName, long filterListId) throws NotFoundException {
        for (String[] string : loadedValues) {
            LoadedValue value = new LoadedValue();
            value.setFilterListId(filterListId);
            value.setEmail(string[0]);
            value.setLoadedParamsString(StringUtils.join(string, ";"));

            getLoadedValueMapper().insert(value);
        }

        FilterList filterList = getFilterListSafe(filterListId);
        filterList.setName(fileName);
        getFilterListMapper().updateByPrimaryKey(filterList);
    }

    /**
     * Fill filter after restore from database or from webpage
     */
    private void fillFilter(Filter filter) {
        if (filter.getFilteredAttribute() != null) {
            filter.setFilteredAttribute(getAttributeMapper().selectByPrimaryKey(filter.getFilteredAttribute().getAttributeId()));
        }

        if (filter.getFilteredAttribute() != null) {
            if (filter.getFilteredAttribute().getType() == AttributeType.DATE && filter.getObject() == null) {
                filter.setObject(new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
            }
        }
    }

    /**
     * Fill filter attributes after restore from database or from webpage
     */
    private void fillFilters(List<Filter> filters) {
        if (filters == null) {
            return;
        }
        for (Filter filter : filters) {
            fillFilter(filter);
        }
    }

    @Override
    public FilterList getFilterListById(long id) throws NotFoundException {
        FilterList filterList = getFilterListSafe(id);
        prepareFilterList(filterList);
        return filterList;
    }

    @Override
    public FilterList getFilterListByIdUnsafe(long id) {
        FilterList filterList = getFilterListMapper().selectByPrimaryKey(id);
        if (filterList == null) {
            return null;
        }
        prepareFilterList(filterList);
        return filterList;
    }

    private FilterList getFilterListSafe(long id) throws NotFoundException {
        FilterList filterList = getFilterListMapper().selectByPrimaryKey(id);
        if (filterList == null) {
            throw new NotFoundException(FilterList.class, id);
        }
        return filterList;
    }

    @Override
    public void saveList(FilterList filterList) {
        if (filterList.getFilterListId() == null || filterList.getFilterListId() == 0) {
            getFilterListMapper().insert(filterList);
        } else {
            getFilterListMapper().updateByPrimaryKeyWithBLOBs(filterList);
        }
    }

    @Override
    public List<FilterList> getFilterLists() {
        return getFilterListMapper().selectList(null, 0, 0);
    }

    @Override
    public List<FilterList> getFilterLists(String query, int offset, int limit) {
        if (StringUtils.isNotBlank(query)) {
            query = StringUtils.normalizeSpace(query).replace(' ', '%');
        }
        return getFilterListMapper().selectList(query, offset, limit);
    }

    @Override
    public void deleteList(long id) throws NotFoundException {
        // check
        getFilterListSafe(id);
        getFilterListMapper().deleteByPrimaryKey(id);
    }

    @Override
    public List<String> getEmails(List<Value> values) {
        List<String> emails = new ArrayList<>();
        for (Value value : values) {
            emails.add(value.getEmail());
        }
        return emails;
    }

    @Override
    public void prepareFilterList(FilterList filterList) {
        List<Filter> filters = new ArrayList<>();
        if (filterList.getFilters() != null) {
            for (Filter filter : filterList.getFilters()) {
                if (filter.getFilteredAttribute() != null) {
                    filters.add(filter);
                }
            }
            fillFilters(filterList.getFilters());
        }
        filterList.setFilters(filters);
    }

    @Override
    public Attribute getAttributeById(long attributeId) throws NotFoundException {
        Attribute attribute = getAttributeMapper().selectByPrimaryKey((int) attributeId);
        if (attribute == null) {
            throw new NotFoundException("Attribute", attributeId);
        }
        return attribute;
    }
}

package ru.planeta.mailer.model.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 19.07.12
 */
public class ModelValidator implements Validator {

    @Autowired
    Collection<Validator> validators;

    Map<Class, Validator> validatorsMap = new HashMap<Class, Validator>();

    @Override
    public boolean supports(Class<?> clazz) {
        if (validatorsMap.containsKey(clazz)) {
            return true;
        }
        for (Validator validator : validators) {
            if (validator.supports(clazz)) {
                validatorsMap.put(clazz, validator);
                return true;
            }
        }
        return false;
    }

    @Override
    public void validate(Object target, Errors errors) {
        if (validatorsMap.containsKey(target.getClass())) {
            validatorsMap.get(target.getClass()).validate(target, errors);
            return;
        }
        for (Validator validator : validators) {
            if (validator.supports(target.getClass())) {
                validatorsMap.put(target.getClass(), validator);
                validator.validate(target, errors);
                return;
            }
        }
    }
}

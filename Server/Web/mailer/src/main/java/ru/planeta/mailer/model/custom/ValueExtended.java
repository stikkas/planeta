package ru.planeta.mailer.model.custom;

import java.math.BigDecimal;
import java.util.Date;

public class ValueExtended extends Value {
    private Date lastActive;
    private long countryId;
    private long regionId;
    private long cityId;
    private Date userBirthDate;
    private BigDecimal balance;
    private long daysRegistered;
    private String registrationSource;

    public Date getLastActive() {
        return lastActive;
    }

    public void setLastActive(Date lastActive) {
        this.lastActive = lastActive;
    }

    public long getCountryId() {
        return countryId;
    }

    public void setCountryId(long countryId) {
        this.countryId = countryId;
    }

    public long getRegionId() {
        return regionId;
    }

    public void setRegionId(long regionId) {
        this.regionId = regionId;
    }

    public long getCityId() {
        return cityId;
    }

    public void setCityId(long cityId) {
        this.cityId = cityId;
    }

    public Date getUserBirthDate() {
        return userBirthDate;
    }

    public void setUserBirthDate(Date userBirthDate) {
        this.userBirthDate = userBirthDate;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public long getDaysRegistered() {
        return daysRegistered;
    }

    public void setDaysRegistered(long daysRegistered) {
        this.daysRegistered = daysRegistered;
    }

    public String getRegistrationSource() {
        return registrationSource;
    }

    public void setRegistrationSource(String registrationSource) {
        this.registrationSource = registrationSource;
    }
}
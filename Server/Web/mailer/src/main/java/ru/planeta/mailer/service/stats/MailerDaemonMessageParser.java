package ru.planeta.mailer.service.stats;

import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import ru.planeta.mailer.model.stats.MailerDaemonInfo;

import javax.mail.BodyPart;
import javax.mail.Header;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MimeMultipart;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 01.08.12
 */
public class MailerDaemonMessageParser {
    private static void getElementText(Node el, StringBuilder sb) {
        for (Node child : el.childNodes()) {
            if (child instanceof TextNode) {
                TextNode textNode = (TextNode) child;
                sb.append(textNode.getWholeText());
            } else if (child instanceof Element) {
                if (((Element) child).tagName().equals("br") || ((Element) child).tagName().equals("p") || ((Element) child).tagName().equals("div")) {
                    sb.append("\n");
                }
                getElementText(child, sb);
            }
        }
    }

    private static BufferedReader getContentReader(BodyPart part) throws IOException, MessagingException {
        if (part.getContentType().toLowerCase().contains("text/html")) {
            Document doc = Jsoup.parse(part.getInputStream(), null, "htmlpart");
            // Trick for better formatting
            doc.body().wrap("<pre></pre>");
            StringBuilder sb = new StringBuilder();
            getElementText(doc, sb);
            // Converting nbsp entities
            String text = sb.toString().replaceAll("\u00A0", " ");
            return new BufferedReader(new StringReader(text));
        } else {
            return new BufferedReader(new InputStreamReader(part.getInputStream(), StandardCharsets.UTF_8));
        }
    }

    private static void parseStream(BufferedReader reader, Collection<String> emailsCollection, MailerDaemonInfo mailerDaemonInfo) throws IOException, MessagingException {
        Set<String> emails = new HashSet<String>();
        String line;
        while ((line = reader.readLine()) != null) {
            if (line.contains("Final-Recipient")
                    || line.contains("Original-Recipient")
                    || line.contains("To:")) {
                RFC822Header header = new RFC822Header(line);
                try {
                    InternetAddress addr = new InternetAddress(header.getValue());
                    emails.add(addr.getAddress());
                } catch (AddressException ignored) {}
            } else if (line.contains("Diagnostic-Code")) {
                RFC822Header header = new RFC822Header(line);
                Scanner scanner = new Scanner(header.getValue());
                if (scanner.hasNextInt()) {
                    mailerDaemonInfo.setErrorCode(scanner.nextInt());
                }
            } else if (line.contains("Status: 5.4.4")) {
                mailerDaemonInfo.setErrorCode(544);
            }
        }
        emailsCollection.addAll(emails);
    }

    private static void basicMailerDaemonInfo(Message message, MailerDaemonInfo mailerDaemonInfo) throws MessagingException, IOException {

        Set<String> emails = new HashSet<String>();

        // try to obtain from header
        String[] failedRecipientsHeader = message.getHeader("X-Failed-Recipients");
        if (failedRecipientsHeader != null) {
            for (String failedRecipient : failedRecipientsHeader) {
                try {
                    InternetAddress addr = new InternetAddress(failedRecipient);
                    emails.add(addr.getAddress());
                } catch (Exception ignored) {}
            }
        }

        // If message is not multipart, parse it as string
        Object messageContent = message.getContent();
        if (!(messageContent instanceof MimeMultipart)) {
            parseStream(new BufferedReader(new StringReader(messageContent.toString())), emails, mailerDaemonInfo);
            mailerDaemonInfo.setEmails(Arrays.asList(emails.toArray(new String[emails.size()])));
            return;
        }

        MimeMultipart multipart = (MimeMultipart)message.getContent();
        Set<Integer> parts = new HashSet<Integer>();

        parts.add(0);
        for (int index = 1; index < multipart.getCount(); index++) {
            BodyPart part = multipart.getBodyPart(index);
            if (part.getContentType().toLowerCase().contains("html")
                    || part.getContentType().toLowerCase().contains("delivery-status")
                    || part.getContentType().toLowerCase().contains("message/rfc822")) {
                parts.add(index);
            } else if (part.getContentType().toLowerCase().contains("message/feedback-report")) {
                mailerDaemonInfo.setErrorCode(554);
                mailerDaemonInfo.setAbuse(true);
            }
        }
        for (int index : parts) {
            BodyPart part = multipart.getBodyPart(index);
            BufferedReader reader = getContentReader(part);

            // Parse recipients in error message
            parseStream(reader, emails, mailerDaemonInfo);
        }
        mailerDaemonInfo.setEmails(Arrays.asList(emails.toArray(new String[emails.size()])));
    }

    private static void extendedMailerDaemonInfo(MimeMultipart multipart, MailerDaemonInfo mailerDaemonInfo) throws MessagingException, IOException {
        Set<Integer> parts = new HashSet<Integer>();

        for (int index = 1; index < multipart.getCount(); index++) {
            BodyPart part = multipart.getBodyPart(index);
            if (part.getContentType().toLowerCase().contains("rfc822")) {
                parts.add(index);
            }
        }

        for (int index : parts) {
            BodyPart part = multipart.getBodyPart(index);
            BufferedReader reader = new BufferedReader(new InputStreamReader(part.getInputStream(), StandardCharsets.UTF_8));
            try {
                String line;
                while ((line = reader.readLine()) != null) {
                    if (line.contains("Message-ID")) {
                        RFC822Header header = new RFC822Header(line);
                        mailerDaemonInfo.setMessageId(header.getValue());
                    }
                }
            } finally {
                IOUtils.closeQuietly(reader);
            }
        }
    }

    public static MailerDaemonInfo getMailerDaemonInfo(Message message) throws IOException, MessagingException {

        MailerDaemonInfo mailerDaemonInfo = new MailerDaemonInfo();

        Object messageContent = message.getContent();

        if (messageContent instanceof MimeMultipart) {
            MimeMultipart multipart = (MimeMultipart)messageContent;

            basicMailerDaemonInfo(message, mailerDaemonInfo);
            extendedMailerDaemonInfo(multipart, mailerDaemonInfo);
        }

        return mailerDaemonInfo;
    }

    private static class RFC822Header {
        private String name;
        private String type;
        private String value;

        public RFC822Header(String line) throws UnsupportedEncodingException, MessagingException {
            InternetHeaders headers = new InternetHeaders(new ByteArrayInputStream(line.getBytes("UTF-8")));
            Enumeration<Header> allHeaders = headers.getAllHeaders();
            while (allHeaders.hasMoreElements()) {
                Header header = allHeaders.nextElement();
                this.name = header.getName();
                String[] values = headers.getHeader(this.name);
                if (values.length > 0) {
                    for (String value : values) {
                        Pattern pattern = Pattern.compile("([a-zA-Z0-9]+);(.*)");
                        Matcher matcher = pattern.matcher(value);
                        if (matcher.find()) {
                            this.type = matcher.group(1);
                            this.value = matcher.group(2).trim();
                        } else {
                            this.value = value;
                        }
                    }
                }
            }
        }

        public String getName() {
            return name;
        }

        public String getType() {
            return type;
        }

        public String getValue() {
            return value;
        }
    }
}

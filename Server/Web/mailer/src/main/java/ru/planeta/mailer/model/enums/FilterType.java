package ru.planeta.mailer.model.enums;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 17.07.12
 */
public enum FilterType {
    AND, OR, FILE
}

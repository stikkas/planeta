package ru.planeta.mailer.dao.custom;

import org.springframework.stereotype.Repository;
import ru.planeta.mailer.model.custom.LoadedValue;

import java.util.List;
import java.util.Map;

/**
 * @author ds.kolyshev
 * Date: 25.06.13
 */
@Repository
public interface LoadedValueMapper {
	int insert(LoadedValue record);

	List<LoadedValue> selectByFilterListId(Map params);

	long countByFilterListId(Map params);
}

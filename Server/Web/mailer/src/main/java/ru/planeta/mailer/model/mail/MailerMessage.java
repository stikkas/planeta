package ru.planeta.mailer.model.mail;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MailerMessage {

    private Long mailerMessageId;

    private String messageId;

    private Long userId;

    private Long campaignId;

    private Date dateSent;

    private String from;

    private List<String> to;

    private String subject;

    private String contentHtml;

    private Map<String, File> multipartFiles = new HashMap<String, File>();

    private boolean sendViaSmtps;

    private String unsubscribeLink;

    private boolean open;

    private boolean failed;

    private boolean unsubscribed;

    private boolean abuse;

    private int attempt;

    public MailerMessage(String from, List<String> to, String subject, String contentHtml, Map<String, File> multipartFiles, String unsubscribeLink, Long campaignId, Long userId, boolean sendViaSmtps) {
        this.from = from;
        this.to = to;
        this.subject = subject;
        this.contentHtml = contentHtml;
        this.multipartFiles = multipartFiles;
        this.unsubscribeLink = unsubscribeLink;
        this.sendViaSmtps = sendViaSmtps;
        this.campaignId = campaignId;
        this.userId = userId;
    }

    public MailerMessage() {
    }

    public Long getMailerMessageId() {
        return mailerMessageId;
    }

    public void setMailerMessageId(Long mailerMessageId) {
        this.mailerMessageId = mailerMessageId;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Long campaignId) {
        this.campaignId = campaignId;
    }

    public Date getDateSent() {
        return dateSent;
    }

    public void setDateSent(Date dateSent) {
        this.dateSent = dateSent;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public List<String> getTo() {
        return to;
    }

    public void setTo(List<String> to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContentHtml() {
        return contentHtml;
    }

    public void setContentHtml(String contentHtml) {
        this.contentHtml = contentHtml;
    }

    public Map<String, File> getMultipartFiles() {
        return multipartFiles;
    }

    public void setMultipartFiles(Map<String, File> multipartFiles) {
        this.multipartFiles = multipartFiles;
    }

    public boolean isSendViaSmtps() {
        return sendViaSmtps;
    }

    public void setSendViaSmtps(boolean sendViaSmtps) {
        this.sendViaSmtps = sendViaSmtps;
    }

    public String getUnsubscribeLink() {
        return unsubscribeLink;
    }

    public void setUnsubscribeLink(String unsubscribeLink) {
        this.unsubscribeLink = unsubscribeLink;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public boolean isFailed() {
        return failed;
    }

    public void setFailed(boolean failed) {
        this.failed = failed;
    }

    public boolean getFailed() {
        return failed;
    }

    public boolean isUnsubscribed() {
        return unsubscribed;
    }

    public void setUnsubscribed(boolean unsubscribed) {
        this.unsubscribed = unsubscribed;
    }

    public boolean isAbuse() {
        return abuse;
    }

    public void setAbuse(boolean abuse) {
        this.abuse = abuse;
    }

    public int getAttempt() {
        return attempt;
    }

    public void setAttempt(int attempt) {
        this.attempt = attempt;
    }

    public void incAttempt() {
        this.attempt = this.attempt + 1;
    }
}
package ru.planeta.mailer.web.controllers;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 16.07.12
 */
public class Urls {
    public static final String INDEX_HTML = "/index.html";
    public static final String INDEX = "/";

    public static final String LIVE_CHECK = "/live-check";

    public static final String CAMPAIGN_EDIT = "/campaign";
    public static final String CAMPAIGN_WITH_ID_EDIT = "/campaign/{id}";
    public static final String CAMPAIGN_WITH_ID_CONFIRM = "/campaign/{id}/confirm";
    public static final String CAMPAIGN_WITH_ID_STOP = "/campaign/{id}/stop";
    public static final String CAMPAIGN_WITH_ID_RESULT = "/campaign/{id}/result";
    public static final String CAMPAIGN_WITH_ID_STATS = "/campaign/{id}/stats";
    public static final String CAMPAIGN_WITH_ID_FILE_OF_STATS = "/campaign/file-of-stats";
    public static final String CAMPAIGN_WITH_ID_FILE_OF_ACTIVE_CLICKERS = "/campaign/file-of-active-clickers";
    public static final String CAMPAIGN_WITH_ID_STATS_MESSAGES = "/campaign/{id}/stats/messages";
    public static final String CAMPAIGN_WITH_ID_VIEW = "/campaign/{id}/view";
    public static final String CAMPAIGN_WITH_ID_DELETE = "/campaign/{id}/delete";
    public static final String CAMPAIGNS_TOTAL_STATS = "/campaigns/stats";
    public static final String CAMPAIGNS_UPDATE_TOTAL_STATS = "/campaigns/updatestats";
    public static final String CAMPAIGNS_FILE_OF_TOTAL_STATS = "/campaign/file-of-total-stats";

    public static final String LIST_INDEX = "/lists";
    public static final String LIST_EDIT = "/list";
    public static final String LIST_WITH_ID_EDIT = "/list/{id}";
    public static final String LIST_WITH_ID_DELETE = "/list/{id}/delete";
    public static final String LIST_ATTRIBUTE_INFO = "/attr.html";
    public static final String LIST_EMAILS = "/emails.html";
    public static final String LIST_USERS = "/users";

    public static final String TEMPLATE_INDEX = "/templates";
    public static final String TEMPLATE_EDIT = "/template";
    public static final String TEMPLATE_WITH_ID_EDIT = "/template/{id}";
    public static final String TEMPLATE_WITH_ID_DELETE = "/template/{id}/delete";
    public static final String TEMPLATE_TEXT = "/template-text.html";

    public static final String USER_WITH_ID = "/user/{id}";
    public static final String USER_CHANGE_SUBSCRIBE_WITH_ID = "/user-change-subscribe/{id}";

    public static final String UNSUBSCRIBE_WITH_CODE = "/unsubscribe/{code}";
    public static final String TRACK_OPEN_WITH_CODE = "/track/{code}.png";

    public static final String VIEW_WITH_CODE = "/view-message/{code}";
    public static final String VIEW_WITH_CAMPAIGN_ID = "/view-message/campaign/{id}";
    public static final String VIEW_WITH_TEMPLATE_ID = "/view-message/template/{id}";

    public static final String NOT_FOUND = "/errors/not-found.html";
    public static final String EXCEPTION = "/errors/exception.html";

    public static final String UPLOAD_IMAGE = "/upload-image.json";
    public static final String DOWNLOAD_IMAGE = "/image";
    public static final String DOWNLOAD_IMAGE_WITH_ID = DOWNLOAD_IMAGE + "/**";

	public static final String CREATE_LIST_FROM_CSV = "/createFromCSV.html";


    public static final String GET_ENUM_LIST = "/get-enum-list.json";
    public static final String GET_FILTER_LIST = "/get-filter-list.json";
    public static final String GET_TEMPLATE_LIST = "/get-template-list.json";

    public static String param(String url, Object... arguments) {
        int pairCount = arguments.length / 2;
        String newUrl = url;
        for (int i = 0; i < pairCount; i++) {
            newUrl = newUrl.replace("{" + arguments[2 * i] + "}", arguments[2 * i + 1].toString());
        }
        return newUrl;
    }

    public static String redirect(String url, Object... arguments) {
        return "redirect:" + param(url, arguments);
    }
}

package ru.planeta.mailer.google.analytics;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * User: s.makarov
 * Date: 28.07.14
 * Time: 19:53
 */
public class GoogleAnalyticsQueryParams {
    private String ids;
    private String dimensions;
    private String metrics;
    private String filters;
    private String sort;
    @JsonProperty("start-date")
    private String startDate;
    @JsonProperty("end-date")
    private String endDate;
    @JsonProperty("start-index")
    private int startIndex = 1;
    @JsonProperty("max-results")
    private int maxResults = 10;

    public GoogleAnalyticsQueryParams () {
        this.dimensions = "";
        this.metrics = "";
        this.filters = "";
    }

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public String getDimensions() {
        return dimensions;
    }

    public void setDimensions(String dimensions) {
        this.dimensions = dimensions;
    }

    public String getMetrics() {
        return metrics;
    }

    public void setMetrics(String metrics) {
        this.metrics = metrics;
    }

    public String getFilters() {
        return filters;
    }

    public void setFilters(String filters) {
        this.filters = filters;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getMaxResults() {
        return maxResults;
    }

    public void setMaxResults(int maxResults) {
        this.maxResults = maxResults;
    }
}

package ru.planeta.mailer.model.custom;

/**
 *
 * @author ds.kolyshev
 * Date: 25.06.13
 */
public class LoadedValue extends AbstractValue {
	private long filterListId;
	private String loadedParamsString;

	public long getFilterListId() {
		return filterListId;
	}

	public void setFilterListId(long filterListId) {
		this.filterListId = filterListId;
	}

	public String getLoadedParamsString() {
		return loadedParamsString;
	}

	public void setLoadedParamsString(String loadedParams) {
		this.loadedParamsString = loadedParams;
	}

	public String[] getParams() {
		return loadedParamsString.split(";");
	}

	@Override
	public String getDisplayName() {
		return getEmail();
	}

}

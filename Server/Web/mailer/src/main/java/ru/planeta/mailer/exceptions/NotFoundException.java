package ru.planeta.mailer.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This exception is thrown when requested resource is not found
 *
 * @author ameshkov
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Resource is not found")
public class NotFoundException extends Exception {

    public static final String NOT_FOUND_MSG_TEMPL = "%s with id %d not found!";

    public static String getNotFoundMessage(String objectName, long objectId) {
        return String.format(NOT_FOUND_MSG_TEMPL, objectName, objectId);
    }

    public NotFoundException() {
    }

    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(String objectName, long objectId) {
        super(getNotFoundMessage(objectName, objectId));
    }

    public NotFoundException(Class<?> objectClass, long objectId) {
        super(getNotFoundMessage(objectClass.getSimpleName(), objectId));
    }

}

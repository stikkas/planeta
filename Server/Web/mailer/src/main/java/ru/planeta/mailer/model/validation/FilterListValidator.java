package ru.planeta.mailer.model.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.planeta.mailer.model.custom.FilterList;


/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 19.07.12
 */
@Component
public class FilterListValidator implements Validator {

    public boolean supports(Class<?> type) {

        return type.isAssignableFrom(FilterList.class);
    }

    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "name", "name.empty.FilterList");
    }
}

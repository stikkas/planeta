package ru.planeta.mailer.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.planeta.mailer.dao.custom.MailUserMapper;
import ru.planeta.mailer.dao.custom.ValueMapper;
import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.model.custom.ValueExtended;

import java.util.List;


@Service
public class MaildbEmailsFillingJobService {

    private static final long MAILDB_EMAILS_FILLING_DELAY = 1000 * 60 * 60; // 60 min

    private static final Logger log = Logger.getLogger(MaildbEmailsFillingJobService.class);

    @Autowired
    private MailUserMapper mailUserMapper;
    @Autowired
    private MailerUserService mailerUserService;
    @Autowired
    private ValueMapper valueMapper;

    @Scheduled(fixedDelay = MAILDB_EMAILS_FILLING_DELAY)
    public synchronized void checkForDeferredMail() throws NotFoundException {
        log.info("MailDB filling VALUES and USERS is started");
        List<ValueExtended> valueExtendedList = mailUserMapper.select(0, 500);

        for (ValueExtended valueExtended : valueExtendedList) {
            log.info("MailDB insertValue " + valueExtended.getEmail());
            valueMapper.insertValue(valueExtended);
            log.info("MailDB insert or update User " + valueExtended.getEmail());
            mailerUserService.getUserByValue(valueExtended);
        }
        log.info("MailDB filling VALUES and USERS is finished");
    }
}
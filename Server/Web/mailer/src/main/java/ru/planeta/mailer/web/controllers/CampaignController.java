package ru.planeta.mailer.web.controllers;

import com.google.api.services.analytics.model.GaData;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.google.analytics.AnalyticsApiService;
import ru.planeta.mailer.google.analytics.GoogleAnalyticsQueryJson;
import ru.planeta.mailer.google.analytics.GoogleAnalyticsQueryParams;
import ru.planeta.mailer.model.custom.FilterList;
import ru.planeta.mailer.model.enums.CampaignState;
import ru.planeta.mailer.model.generated.Campaign;
import ru.planeta.mailer.model.generated.User;
import ru.planeta.mailer.model.stats.CampaignStats;
import ru.planeta.mailer.model.stats.MessageInfo;
import ru.planeta.reports.ReportType;
import ru.planeta.reports.SimpleReport;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 24.07.12
 */
@Controller
public class CampaignController extends BaseController {

    private final AnalyticsApiService analyticsApiService;

    private static final Logger log = Logger.getLogger(CampaignController.class);

    @Autowired
    public CampaignController(AnalyticsApiService analyticsApiService) {
        this.analyticsApiService = analyticsApiService;
    }


    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    @RequestMapping(value = Urls.INDEX, method = RequestMethod.GET)
    public String index(@RequestParam(value = "offset", required = false, defaultValue = "0") int offset,
            @RequestParam(value = "limit", required = false, defaultValue = "20") int limit,
            @RequestParam(value = "searchString", required = false, defaultValue = "") String searchString,
            @RequestParam(value = "dateFrom", required = false) String dateFromString,
            @RequestParam(value = "dateTo", required = false) String dateToString,
            @RequestParam(value = "draftCampaignChecked", required = false, defaultValue = "false") Boolean draftCampaignChecked,
            Model model) throws SQLException {
        Date dateFrom = null;
        Date dateTo = null;
        if (dateFromString != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(Long.parseLong(dateFromString));
            dateFrom = calendar.getTime();
            calendar.setTimeInMillis(Long.parseLong(dateToString));
            dateTo = calendar.getTime();
        }

        List<Campaign> campaigns = getMailerCampaignService().getCampaigns(searchString, dateFrom, dateTo, offset, limit, draftCampaignChecked);
        int campaignsCount = getMailerCampaignService().getCampaignsCount(searchString, dateFrom, dateTo, offset, limit, draftCampaignChecked);

        Boolean intervalChecked = false;
        if (dateFrom != null) {
            intervalChecked = true;
            model.addAttribute("dateFrom", dateFrom.getTime());
            model.addAttribute("dateTo", dateTo.getTime());
        }

        model.addAttribute("intervalChecked", intervalChecked);
        model.addAttribute("campaigns", campaigns);
        model.addAttribute("offset", offset);
        model.addAttribute("limit", limit);
        model.addAttribute("count", campaignsCount);
        model.addAttribute("searchString", searchString);
        model.addAttribute("campaignStates", getMailerService().getCampaignsStates(campaigns));
        model.addAttribute("draftCampaignChecked", draftCampaignChecked);

        return Actions.INDEX.getText();
    }

    @RequestMapping(value = Urls.LIST_USERS, method = RequestMethod.GET)
    public String users(@RequestParam(value = "offset", defaultValue = "0") int offset,
            @RequestParam(value = "limit", defaultValue = "20") int limit,
            @RequestParam(value = "searchString", defaultValue = "") String searchString,
            Model model) {
        List<User> users = getMailerUserService().getUsers(searchString, offset, limit);
        int usersCount = getMailerUserService().getUsersCount(searchString);

        model.addAttribute("users", users);
        model.addAttribute("offset", offset);
        model.addAttribute("limit", limit);
        model.addAttribute("count", usersCount);
        model.addAttribute("searchString", searchString);

        return Actions.LIST_USERS.getText();
    }

    @RequestMapping(value = Urls.CAMPAIGN_WITH_ID_EDIT, method = RequestMethod.GET)
    public String campaignId(Model model,
            @PathVariable("id") Long id,
            @RequestParam(value = "force", required = false, defaultValue = "false") boolean force) throws NotFoundException {

        return campaign(model, id, null, null, force);
    }

    @RequestMapping(value = Urls.CAMPAIGN_EDIT, method = RequestMethod.GET)
    public String campaign(Model model,
            Long campaignId,
            @ModelAttribute Campaign campaign,
            BindingResult campaignErrors,
            @RequestParam(value = "force", required = false, defaultValue = "false") boolean force) throws NotFoundException {

        if ((campaign == null || campaign.getCampaignId() == null) && campaignId != null) {
            campaign = getMailerCampaignService().getCampaignById(campaignId);
            model.addAttribute("campaign", campaign);
        }

        if (!force && campaign != null && campaign.getDateConfirmed() != null) {
            return Urls.redirect(Urls.CAMPAIGN_WITH_ID_VIEW, "id", campaign.getCampaignId());
        }

        if (campaign.getFilterListId() != null) {
            final FilterList selectedFilterList = getFilterService().getFilterListByIdUnsafe(campaign.getFilterListId());
            if (selectedFilterList != null) {
                model.addAttribute("selectedFilterList", selectedFilterList);
            }
        }
        if (campaign.getTemplateId() != null) {
            model.addAttribute("selectedTemplate", getTemplateService().getTemplateById(campaign.getTemplateId()));
        }
        model.addAttribute("fromNames", getMailerCampaignService().getFromNames());

        return Actions.CAMPAIGN_EDIT.getText();
    }

    @RequestMapping(value = {Urls.CAMPAIGN_WITH_ID_EDIT, Urls.CAMPAIGN_EDIT}, method = RequestMethod.POST)
    public String campaignPost(Model model,
            @RequestParam(value = "send", required = false) String send,
            @ModelAttribute @Valid Campaign campaign,
            BindingResult campaignErrors) throws NotFoundException {
        if (campaignErrors.hasErrors()) {
            return campaign(model, null, campaign, campaignErrors, true);
        }

        getMailerCampaignService().saveCampaign(campaign);

        if (send != null) {
            return Urls.redirect(Urls.CAMPAIGN_WITH_ID_CONFIRM, "id", campaign.getCampaignId());
        }

        return Urls.redirect(Urls.CAMPAIGN_WITH_ID_EDIT, "id", campaign.getCampaignId());
    }

    @RequestMapping(value = Urls.CAMPAIGN_WITH_ID_CONFIRM, method = RequestMethod.GET)
    public String confirmCampaign(Model model,
            @PathVariable("id") Long campaignId) throws NotFoundException {

        Campaign campaign = getMailerCampaignService().getCampaignById(campaignId);

        if (campaign.getDateConfirmed() == null) {
            return viewCampaign(model, campaignId);
        } else {
            return Urls.redirect(Urls.CAMPAIGN_WITH_ID_VIEW, "id", campaignId);
        }
    }

    @RequestMapping(value = Urls.CAMPAIGN_WITH_ID_CONFIRM, method = RequestMethod.POST)
    public String confirmCampaignPost(Model model,
            @PathVariable("id") Long campaignId,
            @RequestParam(value = "force", required = false, defaultValue = "false") boolean force) throws NotFoundException {

        Campaign campaign = getMailerCampaignService().getCampaignById(campaignId);

        if (campaign.getTimeToSend() != null) {
            model.addAttribute("isDeferred", true);
            campaign.setDateConfirmed(new Date());
            getMailerCampaignService().saveCampaign(campaign);
            return viewCampaign(model, campaignId);
        } else {
            model.addAttribute("isDeferred", false);
        }

        if (force || campaign.getDateConfirmed() == null) {
            campaign.setDateConfirmed(new Date());
            getMailerCampaignService().sendCampaign(campaign);
        } else {
            model.addAttribute("isAlreadySent", true);
            return viewCampaign(model, campaignId);
        }

        return Urls.redirect(Urls.CAMPAIGN_WITH_ID_RESULT, "id", campaign.getCampaignId());
    }

    @RequestMapping(value = Urls.CAMPAIGN_WITH_ID_STOP, method = RequestMethod.POST)
    public String stopCampaignPost(Model model,
            @PathVariable("id") Long campaignId) throws NotFoundException {

        Campaign campaign = getMailerCampaignService().getCampaignById(campaignId);
        if (campaign == null) {
            throw new NotFoundException("Mailer campaign", campaignId);
        }

        CampaignState state = getMailerService().getCampaignState(campaign);
        if (!CampaignState.RUNNING.equals(state) && !CampaignState.IN_QUEUE.equals(state)) {
            return viewCampaign(model, campaignId);
        }

        getMailerCampaignService().stopCampaign(campaign);

        return Urls.redirect(Urls.CAMPAIGN_WITH_ID_VIEW, "id", campaign.getCampaignId());
    }

    @RequestMapping(value = Urls.CAMPAIGN_WITH_ID_RESULT, method = RequestMethod.GET)
    public String confirmed(Model model,
            @PathVariable("id") Long campaignId) throws NotFoundException {

        Campaign campaign = getMailerCampaignService().getCampaignById(campaignId);

        if (campaign.getDateConfirmed() != null) {
            model.addAttribute("campaign", campaign);

            return Actions.CAMPAIGN_SENT.getText();
        }

        return Urls.redirect(Urls.CAMPAIGN_WITH_ID_CONFIRM, "id", campaign.getCampaignId());
    }

    @RequestMapping(value = Urls.CAMPAIGN_WITH_ID_VIEW, method = RequestMethod.GET)
    public String viewCampaign(Model model,
            @PathVariable("id") Long campaignId) throws NotFoundException {

        Campaign campaign = getMailerCampaignService().getCampaignById(campaignId);

        if (campaign.getDateConfirmed() == null) {
            model.addAttribute("isNeedConfirm", true);
        } else {
            model.addAttribute("isNeedConfirm", false);
        }

        model.addAttribute("campaign", campaign);

        CampaignState state = getMailerService().getCampaignState(campaign);
        model.addAttribute("state", state);

        if (campaign.getDateConfirmed() != null
                && state != CampaignState.RUNNING
                && state != CampaignState.IN_QUEUE) {
            List<MessageInfo> messages = getMessageStatsService().getCampaignMessages(campaign, 0, 20, null, null, null, null, null);
            model.addAttribute("messages", messages);
            try {
                model.addAttribute("count", getMessageStatsService().getCampaignStats(campaign, "").getAddresses());
            } catch (NotFoundException ignored) {
            }
            model.addAttribute("isAllRecipients", false);
        } else {
            FilterList filterList = getFilterService().getFilterListById(campaign.getFilterListId());

            if (filterList == null) {
                return Urls.redirect(Urls.CAMPAIGN_WITH_ID_EDIT, "id", campaignId);
            }
            model.addAttribute("addresses", getFilterService().getValues(filterList, 0, 20));
            model.addAttribute("count", getFilterService().countValues(filterList, null, null, null));
            if (filterList.getFilters() == null || filterList.getFilters().isEmpty()) {
                model.addAttribute("isAllRecipients", true);
            } else {
                model.addAttribute("isAllRecipients", false);
            }
        }

        return Actions.CAMPAIGN_VIEW.getText();
    }

    @RequestMapping(value = Urls.CAMPAIGN_WITH_ID_STATS, method = RequestMethod.GET)
    public String stats(Model model,
            @PathVariable("id") Long campaignId) throws NotFoundException {

        Campaign campaign = getMailerCampaignService().getCampaignById(campaignId);

        if (campaign.getDateConfirmed() == null) {
            return Urls.redirect(Urls.CAMPAIGN_WITH_ID_EDIT, "id", campaignId);
        }

        model.addAttribute("campaign", campaign);
        getMessageStatsService().updateCampaignStats(campaign);
        model.addAttribute("stats", getMessageStatsService().getCampaignStats(campaign, ""));
        model.addAttribute("domainStats", getMessageStatsService().getCampaignDomainStats(campaign));

        GoogleAnalyticsQueryJson analyticsQuery = getMailerCampaignService().getGoogleAnalyticsQueryJsonWithoutUserID(campaign);

        GaData gaData = analyticsApiService.executeQuery(analyticsQuery);
        model.addAttribute("gaData", gaData);

        if (gaData != null) {
            model.addAttribute("usersWhoClickedLinksFromEmail", gaData.getTotalResults());
        }
        return Actions.CAMPAIGN_STATS.getText();
    }

    @RequestMapping(value = Urls.CAMPAIGN_WITH_ID_FILE_OF_ACTIVE_CLICKERS, method = RequestMethod.POST)
    @ResponseBody
    public void getCampaignActiveMailClickersReport(
            @RequestParam(value = "campaignId") long campaignId,
            @RequestParam(value = "reportType", defaultValue = "CSV") ReportType reportType,
            HttpServletResponse response) throws NotFoundException, IOException {

        Campaign campaign = getMailerCampaignService().getCampaignById(campaignId);

        if (campaign == null) {
            throw new NotFoundException(Campaign.class, campaignId);
        }

        if (campaign.getDateConfirmed() != null) {
            GoogleAnalyticsQueryJson analyticsQuery = getMailerCampaignService().getGoogleAnalyticsQueryJson(campaign);
            GoogleAnalyticsQueryParams googleAnalyticsQueryParams = analyticsQuery.getParams();
            googleAnalyticsQueryParams.setMaxResults(1000);
            analyticsQuery.setParams(googleAnalyticsQueryParams);
            List<String> usersWhoClickedLinksFromEmail = getMailerCampaignService().getActiveUsersEmails(analyticsQuery);

            SimpleReport report = reportType.createReport("Emails" + campaignId + "_" + reportType.name());

            for (String email : usersWhoClickedLinksFromEmail) {
                report.addRow(email);
            }
            report.addToResponse(response);
        }
    }

    @RequestMapping(value = Urls.CAMPAIGN_WITH_ID_FILE_OF_STATS, method = RequestMethod.POST)
    @ResponseBody
    public void getCampaignStatsReport(
            @RequestParam(value = "campaignId") long campaignId,
            @RequestParam(value = "reportType", defaultValue = "CSV") ReportType reportType,
            HttpServletResponse response) throws NotFoundException, IOException {

        SimpleReport report = getMessageStatsService().getCampaignStatsReport(campaignId, reportType);
        if (report != null) {
            report.addToResponse(response);
        }
    }

    @RequestMapping(value = Urls.CAMPAIGNS_FILE_OF_TOTAL_STATS, method = RequestMethod.POST)
    @ResponseBody
    public void getCampaignTotalStatsReport(
            @RequestParam(value = "fromDate", required = false) Long dateFrom,
            @RequestParam(value = "toDate", required = false) Long dateTo,
            @RequestParam(value = "reportType", defaultValue = "CSV") ReportType reportType,
            HttpServletResponse response) throws NotFoundException, IOException {

        SimpleReport report = getMessageStatsService().getCampaignTotalStatsReport(dateFrom != null ? new Date(dateFrom) : null, dateTo != null ? new Date(dateTo) : null, reportType);
        if (report != null) {
            report.addToResponse(response);
        }
    }

    @RequestMapping(value = Urls.CAMPAIGNS_TOTAL_STATS, method = RequestMethod.GET)
    public String stats(Model model,
            @RequestParam(value = "dateFrom", required = false) Long dateFrom,
            @RequestParam(value = "dateTo", required = false) Long dateTo) throws NotFoundException {

        CampaignStats totalCampaignStats;
        Map<String, CampaignStats> totalCampaignDomainStats;

        Date fromDate = dateFrom != null ? new Date(dateFrom) : null;
        Date toDate = dateTo != null ? new Date(dateTo) : null;
        log.debug("start collecting total stats");
        totalCampaignStats = getMessageStatsService().getTotalCampaignStats("", fromDate, toDate);
        totalCampaignDomainStats = getMessageStatsService().getTotalCampaignDomainStats(fromDate, toDate);
        log.debug("finish collecting total stats");

        if (fromDate != null) {
            GoogleAnalyticsQueryJson analyticsQuery = getMailerCampaignService().getGoogleAnalyticsQueryJsonOfNewslettersByDateRange(fromDate, toDate);
            log.debug("afrer getGoogleAnalyticsQueryJsonOfNewslettersByDateRange specific date stats");
            GaData gaData = analyticsApiService.executeQuery(analyticsQuery);
            log.debug("afrer executeQuery specific date stats");

            model.addAttribute("gaData", gaData);
            if (gaData != null) {
                model.addAttribute("containsSampledData", gaData.getContainsSampledData());
            }
        }
        model.addAttribute("stats", totalCampaignStats);
        model.addAttribute("domainStats", totalCampaignDomainStats);
        model.addAttribute("fromDate", dateFrom);
        model.addAttribute("toDate", dateTo);

        return Actions.CAMPAIGNS_TOTAL_STATS.getText();
    }


    @RequestMapping(value = Urls.CAMPAIGNS_UPDATE_TOTAL_STATS, method = RequestMethod.GET)
    public String updateTotalStats() throws NotFoundException {
        getMessageStatsService().updateTotalCampaignStats();
        return Actions.CAMPAIGNS_TOTAL_STATS.getText();
    }

    @RequestMapping(value = Urls.CAMPAIGN_WITH_ID_STATS_MESSAGES, method = RequestMethod.GET)
    public String statsMessages(Model model,
            @PathVariable("id") Long campaignId,
            @RequestParam(value = "offset", required = false, defaultValue = "0") long offset,
            @RequestParam(value = "limit", required = false, defaultValue = "20") long limit,
            @RequestParam(value = "query", required = false) String searchString,
            @RequestParam(value = "open", required = false) Boolean open,
            @RequestParam(value = "failed", required = false) Boolean failed,
            @RequestParam(value = "unsubscribed", required = false) Boolean unsubscribed,
            @RequestParam(value = "abuse", required = false) Boolean abuse) throws NotFoundException {

        Campaign campaign = getMailerCampaignService().getCampaignById(campaignId);
        final List<MessageInfo> messages;
        long messageCount;

        if (StringUtils.isEmpty(searchString)
                && open == null && failed == null && unsubscribed == null && abuse == null) {
            messages = getMessageStatsService().getCampaignMessages(campaign, offset, limit, null, null, null, null, null);
            messageCount = getMessageStatsService().countCampaignMessages(campaign);
            model.addAttribute("messagesContinued", false);
        } else {
            // Don't count all records when searchString supplied
            messages = getMessageStatsService().getCampaignMessages(campaign, offset, limit + 1, searchString, open, failed, unsubscribed, abuse);
            messageCount = offset + messages.size();
            if (messages.size() > limit) {
                messages.remove((int) (limit - 1));
            }
            model.addAttribute("messagesContinued", true);
        }

        model.addAttribute("campaign", campaign);
        model.addAttribute("messages", messages);
        model.addAttribute("messageCount", messageCount);
        model.addAttribute("offset", offset);
        model.addAttribute("limit", limit);
        model.addAttribute("searchString", searchString);
        model.addAttribute("open", open);
        model.addAttribute("failed", failed);
        model.addAttribute("unsubscribed", unsubscribed);
        model.addAttribute("abuse", abuse);
        model.addAttribute("opts", (open != null) || (failed != null) || (unsubscribed != null) || (abuse != null));

        return Actions.CAMPAIGN_MESSAGES.getText();
    }

    @RequestMapping(value = Urls.CAMPAIGN_WITH_ID_DELETE, method = RequestMethod.GET)
    public String deleteList(Model model,
            @PathVariable("id") Long campaignId) throws NotFoundException {

        Campaign campaign = getMailerCampaignService().getCampaignById(campaignId);

        if (campaign.getDateConfirmed() != null) {
            return Urls.redirect(Urls.CAMPAIGN_WITH_ID_VIEW, "id", campaignId);
        }

        model.addAttribute("campaign", campaign);
        return Actions.CAMPAIGN_DELETE.getText();
    }

    @RequestMapping(value = Urls.CAMPAIGN_WITH_ID_DELETE, method = RequestMethod.POST)
    public String deleteListPost(@PathVariable("id") Long id) throws NotFoundException {
        Campaign campaign = getMailerCampaignService().getCampaignById(id);

        if (campaign.getDateConfirmed() == null) {
            getMailerCampaignService().deleteCampaign(id);
        }

        return Urls.redirect(Urls.INDEX);
    }
}

package ru.planeta.mailer.service.mail;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 27.07.12
 */
public class MailMimeMessage extends MimeMessage {

    private static Random generator = new Random();

    public MailMimeMessage(Session session) {
        super(session);
    }

    protected void updateMessageID() throws MessagingException {
        setHeader("Message-ID", "<" + (new Date()).getTime() + "." + generator.nextInt(Integer.MAX_VALUE) + "@mailer.planeta.ru>");
    }
}
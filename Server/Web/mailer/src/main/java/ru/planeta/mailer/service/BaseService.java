package ru.planeta.mailer.service;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Component;

import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 27.07.12
 */
@Component
public abstract class BaseService extends BaseMapperService {

    private Logger log = Logger.getLogger(this.getClass());

    public Logger getLogger() {
        return log;
    }

    protected RowBounds getRowBounds(int offset, int limit) {
        return new RowBounds(offset, limit <= 0 ? RowBounds.NO_ROW_LIMIT : limit);
    }
}

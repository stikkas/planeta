package ru.planeta.mailer.service.mail;

import org.apache.commons.codec.DecoderException;
import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.model.generated.Campaign;
import ru.planeta.mailer.model.generated.User;
import ru.planeta.mailer.model.mail.MailerMessage;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 30.07.12
 */
public interface MessageService {

    MailerMessage getMessageById(Long id) throws NotFoundException;

    List<MailerMessage> getMessagesByUserId(Long userId, long offset, long limit, String searchString);

    List<MailerMessage> getMessagesByCampaignId(Long campaignId, long offset, long limit, String searchString, Boolean open, Boolean failed, Boolean unsubscribed, Boolean abuse);

    MailerMessage getMessageByUserIdAndCampaignId(Long userId, Long campaignId);

    long countMessagesByCampaignId(Long campaignId, Boolean isOpen, Boolean isFailed, Boolean isUnsubscribed, Boolean isAbuse, String domainName);

    long countMessages(Boolean isOpen, Boolean isFailed, Boolean isUnsubscribed, Boolean isAbuse, String domainName, Date fromDate, Date toDate);

    long countMessagesByUserId(Long userId);

    MailerMessage getMessageByMessageId(String messageId) throws NotFoundException;

    void saveMessage(MailerMessage message);

    void deleteMessage(Long id) throws NotFoundException;

    void setMessageOpen(User user, Campaign campaign) throws NotFoundException, DecoderException;

    void setMessageUnsubscribed(User user, Campaign campaign) throws NotFoundException, DecoderException;
}

package ru.planeta.mailer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.planeta.mailer.dao.generated.*;
import ru.planeta.mailer.dao.custom.FilterListMapper;
import ru.planeta.mailer.dao.custom.LoadedValueMapper;
import ru.planeta.mailer.dao.custom.ValueMapper;
import ru.planeta.mailer.dao.stats.CampaignStatsMapper;

/**
 * This abstract class contains all autowired SQL mappers
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 27.07.12
 */
@Component
public abstract class BaseMapperService {

    @Autowired
    private AttributeMapper attributeMapper;

    @Autowired
    private EnumValueMapper enumValueMapper;

    @Autowired
    private ValueMapper valueMapper;

    @Autowired
    private FilterListMapper filterListMapper;

    @Autowired
    private TemplateMapper templateMapper;

    @Autowired
    private UserMapper userMapper;



    @Autowired
    private CampaignStatsMapper campaignStatsMapper;

	@Autowired
	private LoadedValueMapper loadedValueMapper;

    protected AttributeMapper getAttributeMapper() {
        return attributeMapper;
    }

    protected EnumValueMapper getEnumValueMapper() {
        return enumValueMapper;
    }

    protected ValueMapper getValueMapper() {
        return valueMapper;
    }

    protected FilterListMapper getFilterListMapper() {
        return filterListMapper;
    }

    protected TemplateMapper getTemplateMapper() {
        return templateMapper;
    }

    protected UserMapper getUserMapper() {
        return userMapper;
    }

    protected CampaignStatsMapper getCampaignStatsMapper() {
        return campaignStatsMapper;
    }

	protected LoadedValueMapper getLoadedValueMapper() {
		return loadedValueMapper;
	}
}

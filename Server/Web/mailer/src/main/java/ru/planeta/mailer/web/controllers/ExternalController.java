package ru.planeta.mailer.web.controllers;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.HandlerMapping;
import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.model.custom.FilterList;
import ru.planeta.mailer.model.generated.Campaign;
import ru.planeta.mailer.model.generated.Template;
import ru.planeta.mailer.model.generated.User;
import ru.planeta.mailer.service.MailerUserService;
import ru.planeta.mailer.service.TrackService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 26.07.12
 */
@Controller
public class ExternalController extends BaseController {

    private final MailerUserService mailerUserService;
    private final TrackService trackService;

    @Value("${mail.admin}")
    private String admin;

    @Value("${mail.fallback.url}")
    private String fallbackUrl;

    @Value("${mail.images.upload.path}")
    private String uploadImagesPath;

    @Value("${mail.files.upload.path}")
    private String uploadFilesPath;

    @Value("${mail.images.download.url}")
    private String downloadUrl;

    @Autowired
    public ExternalController(MailerUserService mailerUserService, TrackService trackService) {
        this.mailerUserService = mailerUserService;
        this.trackService = trackService;
    }

    @RequestMapping(value = Urls.UNSUBSCRIBE_WITH_CODE, method = RequestMethod.GET)
    public String unsubscribe(Model model,
                              @PathVariable("code") String code) throws DecoderException, NotFoundException {

        // TODO: encode a message id, and find email by it

        User user = getMailerUserService().getUserByCode(code);

        try {
            trackService.trackOpen(code);
        } catch (Exception e) {
            getLogger().error("error tracking message open", e);
        }

        if (!user.isUnsubscribed()) {
            model.addAttribute("user", user);
            model.addAttribute("campaignId", mailerUserService.getCampaignIdByCode(code));
            model.addAttribute("fallbackUrl", fallbackUrl);

            return Actions.UNSUBSCRIBE.getText();
        } else {
            return Actions.UNSUBSCRIBE_SUCCESS.getText();
        }
    }

    @RequestMapping(value = Urls.UNSUBSCRIBE_WITH_CODE, method = RequestMethod.POST)
    public String unsubscribePost(Model model,
                                  @PathVariable("code") String code) throws NotFoundException {

        User user = getMailerUserService().getUserByCode(code);

        user.setUnsubscribed(true);

        getMailerUserService().saveUser(user);

        try {
            trackService.trackUnsubscribed(code);
        } catch (Exception e) {
            getLogger().error("error tracking message unsubscribed", e);
        }

        return Urls.redirect(Urls.UNSUBSCRIBE_WITH_CODE, "code", code);
    }


    @RequestMapping(value = Urls.TRACK_OPEN_WITH_CODE, method = RequestMethod.GET)
    public void trackOpen(Model model,
                          @PathVariable("code") String code,
                          HttpServletResponse response) throws NotFoundException, DecoderException, IOException {

        trackService.trackOpen(code);
        response.setContentType("image/png");
        response.setContentLength(0);
        response.setStatus(HttpServletResponse.SC_OK);
    }

    @RequestMapping(value = Urls.VIEW_WITH_CAMPAIGN_ID, method = RequestMethod.GET)
    public void viewCampaignMessage(@PathVariable("id") Long campaignId,
                                    HttpServletResponse response) throws IOException, NotFoundException {

        Campaign campaign = getMailerCampaignService().getCampaignById(campaignId);

        response.setContentType("text/html;charset=utf-8");

        String html = getMailerService().previewMessageHtml(campaign);

        response.getWriter().write(html);
    }

    @RequestMapping(value = Urls.VIEW_WITH_TEMPLATE_ID, method = RequestMethod.GET)
    public void viewTemplateMessage(@PathVariable("id") Long templateId,
                                    HttpServletResponse response) throws IOException, NotFoundException {

        Template template = getTemplateService().getTemplateById(templateId);

        response.setContentType("text/html;charset=utf-8");

        String html = getMailerService().previewMessageHtml(template);

        response.getWriter().write(html);
    }

    @RequestMapping(value = Urls.VIEW_WITH_CODE, method = RequestMethod.GET)
    public void viewMessage(@PathVariable("code") String code,
                            HttpServletResponse response) throws IOException, NotFoundException, DecoderException {

        User user = getMailerUserService().getUserByCode(code);
        Campaign campaign = getMailerCampaignService().getCampaignById(getMailerUserService().getCampaignIdByCode(code));

        response.setContentType("text/html;charset=utf-8");

        String html = getMailerService().getMessageHtml(campaign, user);

        response.getWriter().write(html);
    }

    @RequestMapping(value = Urls.UPLOAD_IMAGE, method = RequestMethod.POST)
    @ResponseBody
    public Map uploadImage(@RequestParam("file") MultipartFile file) throws IOException {
        InputStream fis = null;
        OutputStream fos = null;
        try {

            fis = file.getInputStream();
            String fileName = DigestUtils.md5Hex(fis) + "." + StringUtils.substringAfterLast(file.getOriginalFilename(), ".");
            String fileDir = fileName.substring(0, 1) + "/" + fileName.substring(1, 2) + "/" + fileName.substring(2, 3);

            File directory = new File(uploadImagesPath + "/" + fileDir);
            FileUtils.forceMkdir(directory);


            File dest = new File(directory.getAbsolutePath() + "/" + fileName);

            if (!dest.exists()) {
                fos = new FileOutputStream(dest);
                IOUtils.copyLarge(fis, fos);
            }

            Map<String, String> map = new LinkedHashMap<>();
            map.put("filelink", StringUtils.removeEnd(downloadUrl, "/") + "/" + fileDir + "/" + fileName);

            return map;
        } finally {
            IOUtils.closeQuietly(fis);
            IOUtils.closeQuietly(fos);
        }
    }

    @RequestMapping(value = Urls.DOWNLOAD_IMAGE_WITH_ID, method = RequestMethod.GET)
    public void getImage(HttpServletRequest request, HttpServletResponse response) throws Exception {
        FileInputStream fis = null;
        try {
            String uri = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
            String path = StringUtils.removeStart(uri, Urls.DOWNLOAD_IMAGE);
            response.addHeader("Content-Disposition", "inline; filename=\"" + new File(path).getName() + "\"");

            fis = new FileInputStream(new File(uploadImagesPath + "/" + path));
            IOUtils.copyLarge(fis, response.getOutputStream());
            response.setStatus(HttpServletResponse.SC_OK);
        } catch (FileNotFoundException e) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        } catch (Exception e) {
            getLogger().error("error processing request", e);
            throw e;
        } finally {
            IOUtils.closeQuietly(fis);
        }
    }

    @RequestMapping(value = Urls.CREATE_LIST_FROM_CSV, method = RequestMethod.POST)
    public void createListFromCSV(@RequestParam("file") MultipartFile file,
                                  HttpServletResponse response) throws IOException {

        InputStream fis = null;
        OutputStream fos = null;
        File dest = null;
        try {
            fis = file.getInputStream();
            String fileName = file.getOriginalFilename();
            String fileDir = Long.toString(System.currentTimeMillis());

            File directory = new File(uploadFilesPath + "/" + fileDir);
            FileUtils.forceMkdir(directory);

            dest = new File(directory.getAbsolutePath() + "/" + fileName);

            if (!dest.exists()) {
                fos = new FileOutputStream(dest);
                IOUtils.copyLarge(fis, fos);
            }

            FilterList filterList = getFilterService().createListFromCSVFile(dest);
            response.sendRedirect(Urls.LIST_EDIT + "/" + filterList.getFilterListId());

        } finally {
            IOUtils.closeQuietly(fis);
            IOUtils.closeQuietly(fos);

            if (dest != null && dest.exists()) {
                FileUtils.deleteQuietly(dest);
                FileUtils.deleteDirectory(dest.getParentFile());
            }

        }

    }

    @RequestMapping(value = Urls.NOT_FOUND, method = {RequestMethod.GET, RequestMethod.HEAD, RequestMethod.POST})
    public String notFound(Model model) {
        model.addAttribute("admin", admin);
        return Actions.NOT_FOUND.getText();
    }

    @RequestMapping(value = Urls.EXCEPTION, method = {RequestMethod.GET, RequestMethod.HEAD, RequestMethod.POST})
    public String error(Model model) {
        model.addAttribute("admin", admin);
        return Actions.EXCEPTION.getText();
    }
}

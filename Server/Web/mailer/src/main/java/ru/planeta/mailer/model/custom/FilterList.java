package ru.planeta.mailer.model.custom;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.planeta.mailer.model.enums.AttributeType;
import ru.planeta.mailer.model.enums.FilterType;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 17.07.12
 */
@JsonIgnoreProperties(value = {"filterListId", "name", "data"})
public class FilterList {

    private Long filterListId;

    private List<Filter> filters = new ArrayList<Filter>();

    private FilterType filterType = FilterType.AND;

    private String name;

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public List<Filter> getFilters() {
        return filters;
    }

    public void setFilters(List<Filter> filters) {
        this.filters = filters;
    }

    public FilterType getFilterType() {
        return filterType;
    }

    public void setFilterType(FilterType filterType) {
        this.filterType = filterType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getFilterListId() {
        return filterListId;
    }

    public void setFilterListId(Long filterListId) {
        this.filterListId = filterListId;
    }

    public byte[] getData() throws IOException {
        return objectMapper.writeValueAsBytes(this);
    }

    public void setData(byte[] data) throws IOException {
        String s = new String(data);

        for (AttributeType attributeType: AttributeType.values()){
            s = s.replaceAll("\"typeCode\":"+ attributeType.getCode(),"\"type\":\"" + attributeType.name() + "\"");
        }

        objectMapper.readerForUpdating(this).readValue(s.getBytes());
    }
}

package ru.planeta.mailer.web.controllers;

public enum Actions {
    INDEX("index"),

    CAMPAIGN_EDIT("edit-campaign"),
    CAMPAIGN_SENT("sent-campaign"),
    CAMPAIGN_VIEW("view-campaign"),
    CAMPAIGN_STATS("campaign-stats"),
    CAMPAIGN_MESSAGES("campaign-messages"),
    CAMPAIGN_DELETE("delete-campaign"),
    CAMPAIGNS_TOTAL_STATS("campaign-total-stats"),

    LISTS("lists-list"),
    LIST_EDIT("edit-list"),
    LIST_EMAILS("emails"),
    LIST_ATTRIBUTE_INFO("attr"),
    LIST_DELETE("delete-list"),
    LIST_USERS("user-list"),

    TEMPLATES("templates-list"),
    TEMPLATE_EDIT("edit-template"),
    TEMPLATE_DELETE("delete-template"),

    UNSUBSCRIBE("unsubscribe"),
    UNSUBSCRIBE_SUCCESS("unsubscribe-success"),

    USER_VIEW("view-user"),

    NOT_FOUND("404"),
    EXCEPTION("500");

    private String text;

    private Actions(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return getText();
    }
}





package ru.planeta.mailer.google.analytics;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.services.analytics.Analytics;
import com.google.api.services.analytics.AnalyticsScopes;
import com.google.api.services.analytics.model.GaData;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;

@Service
public class AnalyticsApiServiceImpl implements AnalyticsApiService {

    /**
     * The namespaced view (profile) ID of the view (profile) from which to request data.
     * Use the view (profile) selector above to find this value.
     * example
     * ga:1174 where 1174 is your view (profile) ID.
     */
    @Value("${mailer.statistic.analytics.profileids}")
    public String analyticsProfileIds;

    private static final Logger LOGGER = Logger.getLogger(AnalyticsApiService.class);

    /**
     * E-mail address of the service account.
     */
    private static final String SERVICE_ACCOUNT_EMAIL = "405061959586-90tbs3iml8321p587o78u7lvkmc2haon@developer.gserviceaccount.com";

    private static final File PRIVATE_KEY = new File(AnalyticsApiService.class.getResource("/google/80b1bd57328d869f2e92d97bf6ce649df5c0b14e-privatekey.p12").getFile());

    private static final String APPLICATION_NAME = "Planeta-WidgetsReferrer/1.0";

    private GoogleApiCertificateClient certificateClient = new GoogleApiCertificateClient(
        PRIVATE_KEY,
        SERVICE_ACCOUNT_EMAIL,
        Collections.singleton(AnalyticsScopes.ANALYTICS_READONLY));

    private Analytics analytics;

    @Override
    public GaData executeQuery(GoogleAnalyticsQueryJson queryJson) {
        try {
            if (queryJson.getParams().getIds() == null) {
                if (this.analyticsProfileIds != null) {
                    queryJson.getParams().setIds(this.analyticsProfileIds);
                } else {
                    LOGGER.error("[Mailer] Google Analytics profile ids is not set");
                    return null;
                }
            }
            Analytics analytics = getAnalytics();
            return executeDataQuery(analytics, queryJson);
        } catch (GoogleJsonResponseException e) {
            LOGGER.error("There was a service error: " + e.getDetails().getCode() + " : "
                + e.getDetails().getMessage());
        } catch (GeneralSecurityException e) {
            LOGGER.error(e);
        } catch (IOException e) {
            LOGGER.error(e);
        }
        return null;
    }

    private static GaData executeDataQuery(Analytics analytics, GoogleAnalyticsQueryJson queryJson) throws IOException {
        return analytics.data().ga().get(queryJson.getParams().getIds(), // Table Id. ga: + profile id.
                queryJson.getParams().getStartDate(),
                queryJson.getParams().getEndDate(),
                queryJson.getParams().getMetrics())
                .setStartIndex(queryJson.getParams().getStartIndex())
                .setDimensions(queryJson.getParams().getDimensions())
                .setSort(queryJson.getParams().getSort())
                .setFilters(queryJson.getParams().getFilters())
                .setMaxResults(queryJson.getParams().getMaxResults())
                .execute();
    }

    /**
     * Performs all necessary setup steps for running requests against the API.
     * Set up and return Google Analytics API client.
     *
     * @return An initialized Analytics service object.
     * @throws Exception if an issue occurs with OAuth2Native authorize.
     */
    private synchronized Analytics getAnalytics() throws GeneralSecurityException, IOException {
        if (analytics == null) {
            analytics = new Analytics.Builder(GoogleApiCertificateClient.getHttpTransportInstance(),
                GoogleApiCertificateClient.getJsonFactoryInstance(),
                certificateClient.getGoogleCredential())
                .setApplicationName(APPLICATION_NAME)
                .build();
        }
        return analytics;
    }

}

package ru.planeta.mailer.service.stats;

import javax.mail.MessagingException;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 01.08.12
 */
public interface BounceService {
    /**
     * Check mail and analyze bounce messages
     * @throws javax.mail.MessagingException
     */
    void checkMail() throws MessagingException, IOException;

}

package ru.planeta.mailer.service.stats;

import com.sun.org.apache.xpath.internal.XPathAPI;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import ru.planeta.mailer.model.stats.DmarcRecord;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 03.08.12
 */
public class DmarcParser {

    private static final Logger log = Logger.getLogger(DmarcParser.class);

    public static List<DmarcRecord> getFailedRecords(BodyPart part) throws IOException, SAXException, ParserConfigurationException, TransformerException, MessagingException {
        ZipInputStream zis = null;
        try {
            List<DmarcRecord> records = new ArrayList<>();
            zis = new ZipInputStream(part.getInputStream());
            ZipEntry zipEntry;
            while ((zipEntry = zis.getNextEntry()) != null) {
                StringWriter text = new StringWriter();
                IOUtils.copy(zis, text);
                zis.closeEntry();

                DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                org.w3c.dom.Document doc = documentBuilder.parse(IOUtils.toInputStream(text.toString()));
                NodeList list = XPathAPI.selectNodeList(doc, "//row[count(./..//result[text()!=\"pass\"])>0]");
                for (int index = 0; index < list.getLength(); index++) {
                    Node node = list.item(index);
                    String ip = XPathAPI.eval(node, "source_ip/text()").str();
                    if (ip != null) {
                        int count = Integer.parseInt(XPathAPI.eval(doc, "count/text()").str());
                        log.info("Failed IP: " + ip + ", count=" + count);
                        records.add(new DmarcRecord(ip, count));
                    }
                }
            }
            return records;
        } finally {
            IOUtils.closeQuietly(zis);
        }
    }
}

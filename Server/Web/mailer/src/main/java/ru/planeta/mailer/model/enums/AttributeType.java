package ru.planeta.mailer.model.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: s.fionov
 * Date: 10.07.12
 */
public enum AttributeType {
    LONG(1), STRING(2), DATE(3), ENUM(4), MULTI(5);

    private int code;

    private AttributeType(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    private static final Map<Integer, AttributeType> lookup = new HashMap<Integer, AttributeType>();

    static {
        for (AttributeType s : values()) {
            lookup.put(s.getCode(), s);
        }
    }

    public static AttributeType getByValue(int code) {
        return lookup.get(code);
    }
}

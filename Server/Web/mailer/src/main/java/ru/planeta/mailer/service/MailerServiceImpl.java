package ru.planeta.mailer.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.model.custom.AbstractValue;
import ru.planeta.mailer.model.custom.FilterList;
import ru.planeta.mailer.model.custom.LoadedValue;
import ru.planeta.mailer.model.enums.CampaignState;
import ru.planeta.mailer.model.enums.FilterType;
import ru.planeta.mailer.model.generated.Campaign;
import ru.planeta.mailer.model.generated.Template;
import ru.planeta.mailer.model.generated.User;
import ru.planeta.mailer.model.mail.MailerMessage;
import ru.planeta.mailer.service.mail.MailerMailService;
import ru.planeta.mailer.service.mail.MessageService;
import ru.planeta.mailer.service.stats.MessageStatsService;

import javax.mail.MessagingException;
import java.util.*;
import java.util.concurrent.*;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 12.07.12
 */
@Service
public class MailerServiceImpl implements MailerService {

    private static final Logger log = Logger.getLogger(MailerServiceImpl.class);

    @Autowired
    public MailerServiceImpl(MailerUserService mailerUserService, FilterService filterService, TemplateService templateService, MailerMailService mailerMailService, MessageService messageService, MessageStatsService messageStatsService, MessageSource messageSource) {
        this.mailerUserService = mailerUserService;
        this.filterService = filterService;
        this.templateService = templateService;
        this.mailerMailService = mailerMailService;
        this.messageService = messageService;
        this.messageStatsService = messageStatsService;
        this.messageSource = messageSource;
    }

    public static Logger getLogger() {
        return log;
    }

    private final MailerUserService mailerUserService;
    private final FilterService filterService;
    private final TemplateService templateService;
    private final MailerMailService mailerMailService;
    private final MessageService messageService;
    private final MessageStatsService messageStatsService;
    private final MessageSource messageSource;

    private static final int SEND_LIMIT = 5000;
    private static final int MAX_ITERATIONS = 1000;
    private static final int MAX_THREADS = 30;

    private ExecutorService executorService = Executors.newSingleThreadExecutor();
    private volatile Map<Long, CampaignState> campaignStates = new HashMap<>();

    @Value("${mail.unsubscribe.url}")
    private String unsubscribeUrl;

    @Value("${mail.track.url}")
    private String trackUrl;


    @Override
    public String previewMessageHtml(Campaign campaign) {
		ModelMap model = getPreviewModelMap();

		try {
			FilterList filterList = filterService.getFilterListById(campaign.getFilterListId());
            LoadedValue values = filterService.getFirstFileValue(filterList);
            if (values != null ) {
                String[] params = values.getParams();
                for (int i = 0, paramsLength = params.length; i < paramsLength; i++) {
                    model.addAttribute(Integer.toString(i), params[i]);
                }
            }
		} catch (Exception e) {
			getLogger().error("Exception preparing model for message preview:" , e);
		}

		return templateService.renderMessageHtml(campaign.getText(), model);
	}

    @Override
    public String previewMessageHtml(Template template) {
		ModelMap model = getPreviewModelMap();
		return templateService.renderMessageHtml(template.getText(), model);
	}

	private ModelMap getPreviewModelMap() {
		ModelMap model = new ModelMap();

		model.addAttribute("displayName", "%username%");
        model.addAttribute("email", "%email%");
		model.addAttribute("name", "%username%");
		model.addAttribute("unsubscribeLink", "#unsubscribeLink");
		model.addAttribute("unsubscribe", messageSource.getMessage("message.mailer.unsubscribe", new Object[]{"#unsubscribeHTMLLink"}, Locale.getDefault()));
		model.addAttribute("trackLink", "");
		return model;
	}

	@Override
    public String getMessageHtml(Campaign campaign, User user) throws NotFoundException {
		ModelMap model = getCommonModelMap(campaign, user);

        return templateService.renderMessageHtml(campaign.getText(), model);
    }

	private String getMessageHtml(Campaign campaign, User user, LoadedValue value) throws NotFoundException {
		ModelMap model = getCommonModelMap(campaign, user);
		String[] params = value.getParams();
		for (int i = 0, paramsLength = params.length; i < paramsLength; i++) {
			model.addAttribute(Integer.toString(i), params[i]);
		}

		return templateService.renderMessageHtml(campaign.getText(), model);
	}

	private ModelMap getCommonModelMap(Campaign campaign, User user) throws NotFoundException {
		ModelMap model = new ModelMap();

		model.addAttribute("displayName", user.getDisplayName());
		model.addAttribute("name", user.getDisplayName());
        model.addAttribute("email", user.getEmail());

		String unsubscribeLink = getLink(unsubscribeUrl, campaign, user.getEmail());
		model.addAttribute("unsubscribeLink", unsubscribeLink);
		model.addAttribute("unsubscribe", messageSource.getMessage("message.mailer.unsubscribe", new Object[]{unsubscribeLink}, Locale.getDefault()));
		model.addAttribute("trackLink", getLink(trackUrl, campaign, user.getEmail()) + ".png");
		return model;
	}

    private void sendMessage(Campaign campaign, User user, String messageHtml) throws NotFoundException, MessagingException {
        String userEmail = user.getEmail();
        String unsubscribeLink = getLink(unsubscribeUrl, campaign, userEmail);
        if (userEmail.contains("@")) {
            MailerMessage mailerMessage = new MailerMessage(campaign.getMessageFrom(), Collections.singletonList(userEmail), campaign.getSubject(),
                    messageHtml, null, unsubscribeLink, campaign.getCampaignId(), user.getUserId(), false);
            mailerMailService.send(mailerMessage);

        } else {
            log.error("Wrong email " + userEmail);
        }
    }

    @Override
    public synchronized void sendCampaign(Campaign campaign) throws NotFoundException {
        if (getCampaignState(campaign) != CampaignState.RUNNING) {
            setCampaignState(campaign, CampaignState.IN_QUEUE);
            executorService.execute(new SendTask(campaign));
        }
    }

	@Override
	public synchronized void stopCampaign(Campaign campaign) {
		CampaignState campaignState = getCampaignState(campaign);
		if (CampaignState.RUNNING.equals(campaignState)
				|| CampaignState.IN_QUEUE.equals(campaignState)) {
			setCampaignState(campaign, CampaignState.PAUSED);
		}
	}

	@Override
    public CampaignState getCampaignState(Campaign campaign) {
        CampaignState campaignState = campaignStates.get(campaign.getCampaignId());
        if (campaignState == null) {
            campaignState = CampaignState.NOT_RUNNING;
            campaignStates.put(campaign.getCampaignId(), campaignState);
        }
        return campaignState;
    }

	@Override
	public Map<Long, CampaignState> getCampaignsStates(List<Campaign> campaigns) {
		Map<Long, CampaignState> states = new HashMap<>();
		for (Campaign campaign : campaigns) {
			states.put(campaign.getCampaignId(), getCampaignState(campaign));
		}

		return states;
	}

	@Override
    public void setCampaignState(Campaign campaign, CampaignState campaignState) {
        campaignStates.put(campaign.getCampaignId(), campaignState);
    }

    private String getLink(String url, Campaign campaign, String email) throws NotFoundException {
        StringBuilder sb = new StringBuilder();
        sb.append(url);

        if (!url.contains("?") && !url.matches(".*/$")) {
            sb.append("/");
        }

        sb.append(mailerUserService.getUnsubscribeCode(email, campaign.getCampaignId()));

        return sb.toString();
    }

    private void sendToOneEmail(AbstractValue value, FilterType filterType, Campaign campaign) {
        try {

            User user = mailerUserService.getUserByValue(value);
            if ( !campaign.getSendToAll() && user.isUnsubscribed()) {
                getLogger().debug("User is unsubscribed - continue");
                return;
            }

            MailerMessage message = messageService.getMessageByUserIdAndCampaignId(user.getUserId(), campaign.getCampaignId());
            if (message != null) {
                if (message.isFailed() || message.getDateSent() == null) {
                    getLogger().debug("Message was failed or date sent is null. Resending..");
                    messageService.deleteMessage(message.getMailerMessageId());
                } else {
                    return;
                }
            }

            try {
                final String messageHtml;
                if (FilterType.FILE.equals(filterType)) {
                    messageHtml = getMessageHtml(campaign, user, (LoadedValue) value);
                } else {
                    messageHtml = getMessageHtml(campaign, user);
                }
                getLogger().debug("Message html prepared");

                sendMessage(campaign, user, messageHtml);
            } catch (MessagingException e) {
                getLogger().error("Can not send mail", e);
            }
        } catch (Exception ex) {
            getLogger().error("Can not send mail error", ex);
        }
    }

    private class SendTask implements Runnable {

        private Campaign campaign;

        SendTask(Campaign campaign) {
            this.campaign = campaign;
        }

        @Override
        public void run() {
            if (getCampaignState(campaign) == CampaignState.RUNNING) {
                return;
            }
            setCampaignState(campaign, CampaignState.RUNNING);
            ExecutorService executor = getExecutorServiceForSendOneEmailTask();
            try {
                messageStatsService.updateCampaignStats(campaign, true);
				getLogger().debug("Campaign stats updated");

                final FilterList filterList = filterService.getFilterListById(campaign.getFilterListId());
				getLogger().debug("Filter list selected");

                long offset = 0;

                for (int i = 0; i < MAX_ITERATIONS; i++) {
                    try {
                        if (i % 10 == 0) {
                            messageStatsService.updateCampaignStats(campaign, true);
                        }

                        List<? extends AbstractValue> emailValues = filterService.getValues(filterList, offset, SEND_LIMIT);
                        getLogger().debug("Email list selected");
                        messageStatsService.increaseCompaignAdresses(campaign, emailValues.size());


                        for (AbstractValue value : emailValues) {

                            if (CampaignState.PAUSED.equals(getCampaignState(campaign))) {
                                getLogger().debug("Campaign is paused - return");
                                return;
                            }
                            Runnable emailSendTask = new SendOneEmail(value, filterList.getFilterType(), campaign);
                            executor.execute(emailSendTask);
                        }

                        if (emailValues.size() < SEND_LIMIT) {
                            break;
                        }
                    } catch (Exception ex) {
                        getLogger().error("error while sending bucket", ex);
                    }

                    offset = offset + SEND_LIMIT;
                }

                setCampaignState(campaign, CampaignState.FINISHED);
                executor.shutdown();
                try {
                    executor.awaitTermination(1, TimeUnit.HOURS);
                } catch (InterruptedException e) {
                    getLogger().warn("error while waiting ", e);
                }
            } catch (NotFoundException e) {
                getLogger().error("error while sending mail", e);
                setCampaignState(campaign, CampaignState.FAILED);
            } finally {
                executor.shutdown();
            }
        }


    }

    private static ExecutorService getExecutorServiceForSendOneEmailTask() {
        return new ThreadPoolExecutor(MailerServiceImpl.MAX_THREADS, MailerServiceImpl.MAX_THREADS,
                0L, TimeUnit.MILLISECONDS,
                new SynchronousQueue<Runnable>(),
                new BlockingStrategy());
    }

    private static class BlockingStrategy implements RejectedExecutionHandler {

        /**
         * Creates a <tt>BlockingStrategy</tt>.
         */
        BlockingStrategy() {}

        @Override
        public void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor) {
            if (!threadPoolExecutor.isShutdown()) {
                try {
                    threadPoolExecutor.getQueue().put(runnable);
                } catch (InterruptedException e) {
                    getLogger().error(e);
                }
            }
        }
    }

    private class SendOneEmail implements Runnable {
        final private AbstractValue value;
        final private FilterType filterType;
        final Campaign campaign;

        SendOneEmail(AbstractValue value, FilterType filterType, Campaign campaign) {
            this.value = value;
            this.filterType = filterType;
            this.campaign = campaign;
        }

        @Override
        public void run() {
            sendToOneEmail(value, filterType, campaign);
        }
    }


}

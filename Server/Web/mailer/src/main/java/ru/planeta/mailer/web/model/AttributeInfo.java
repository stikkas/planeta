package ru.planeta.mailer.web.model;

import ru.planeta.mailer.model.generated.Attribute;
import ru.planeta.mailer.model.generated.EnumValue;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 16.07.12
 */
public class AttributeInfo {
    private Attribute attribute;
    private List<EnumValue> enumValues;

    public Attribute getAttribute() {
        return attribute;
    }

    public void setAttribute(Attribute attribute) {
        this.attribute = attribute;
    }

    public List<EnumValue> getEnumValues() {
        return enumValues;
    }

    public void setEnumValues(List<EnumValue> enumValues) {
        this.enumValues = enumValues;
    }
}

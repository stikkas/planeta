package ru.planeta.mailer.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 13.07.12
 */
@Controller
public class IndexController extends BaseController {

    @RequestMapping(value = Urls.INDEX_HTML, method = RequestMethod.GET)
    public String redirect() {
        return Urls.redirect(Urls.INDEX);
    }

    @RequestMapping(value = Urls.LIVE_CHECK, method = RequestMethod.GET)
    @ResponseBody
    public String liveCheck() {
        return "{OK: 200}";
    }
}

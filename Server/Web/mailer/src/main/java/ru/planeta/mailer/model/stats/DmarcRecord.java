package ru.planeta.mailer.model.stats;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 03.08.12
 */
public class DmarcRecord {
    private String sourceIp;
    private int count;
    private String dkimStatus;
    private String spfStatus;

    public DmarcRecord(String sourceIp, int count) {
        this.sourceIp = sourceIp;
        this.count = count;
    }

    public String getSourceIp() {
        return sourceIp;
    }

    public void setSourceIp(String sourceIp) {
        this.sourceIp = sourceIp;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getSpfStatus() {
        return spfStatus;
    }

    public void setSpfStatus(String spfStatus) {
        this.spfStatus = spfStatus;
    }

    public String getDkimStatus() {
        return dkimStatus;
    }

    public void setDkimStatus(String dkimStatus) {
        this.dkimStatus = dkimStatus;
    }
}
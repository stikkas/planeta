package ru.planeta.mailer.service;

import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.model.generated.Attribute;
import ru.planeta.mailer.model.generated.EnumValue;
import ru.planeta.mailer.model.custom.AbstractValue;
import ru.planeta.mailer.model.custom.FilterList;
import ru.planeta.mailer.model.custom.LoadedValue;
import ru.planeta.mailer.model.custom.Value;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: s.fionov
 * Date: 09.07.12
 */
public interface FilterService {

    /**
     * Get a full list of attributes
     *
     * @return
     */
    List<Attribute> getAttributes();

    /**
     * Get values of enumerated attribute
     *
     * @param attribute
     * @return
     */
    List<EnumValue> getEnumValues(Attribute attribute);

    List<EnumValue> getEnumValues(Attribute attribute, String query, int offset, int limit);

    /**
     * Get subscribers of a selected list
     *
     * @param filterList
     * @param offset
     * @param limit
     * @return
     */
    List<? extends AbstractValue> getValues(FilterList filterList, long offset, long limit);

    // only if filterList.type == FILE
    LoadedValue getFirstFileValue(FilterList filterList);

    /**
     * Get a filter list by identifier
     *
     * @param id
     * @return
     * @throws NotFoundException
     */
    FilterList getFilterListById(long id) throws NotFoundException;

    FilterList getFilterListByIdUnsafe(long id) ;

    /**
     * Saves a filter list
     *
     * @param filterList
     */
    void saveList(FilterList filterList);

    /**
     * Get a list of filter lists
     *
     * @return
     */
    List<FilterList> getFilterLists();

    List<FilterList> getFilterLists(String query, int offset, int limit);

    /**
     * Delete a filter list
     *
     * @param filterListId
     * @throws NotFoundException
     */
    void deleteList(long filterListId) throws NotFoundException;

    /**
     * Extract emails from subscribers list
     *
     * @param values
     * @return
     */
    List<String> getEmails(List<Value> values);

    /**
     * Prepares filter list for controller
     *
     * @param filterList
     */
    void prepareFilterList(FilterList filterList);

    /**
     * Returns attribute by identifier
     *
     * @param attributeId
     * @return
     */
    Attribute getAttributeById(long attributeId) throws NotFoundException;

    long countValues(FilterList filterList, String domainName, Date fromDate, Date toDate);

    /**
     * Parses data from csv-file
     *
     * @param file
     * @return
     */
    List<String[]> parseDataFromCSVFile(File file) throws IOException;

    /**
     * Creates list from csv-file
     *
     * @param file
     * @return
     */
    FilterList createListFromCSVFile(File file) throws IOException;
}
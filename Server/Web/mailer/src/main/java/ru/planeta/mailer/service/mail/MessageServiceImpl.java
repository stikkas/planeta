package ru.planeta.mailer.service.mail;

import org.apache.commons.codec.DecoderException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.planeta.mailer.dao.mail.MailerMessageMapper;
import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.model.generated.Campaign;
import ru.planeta.mailer.model.generated.User;
import ru.planeta.mailer.model.mail.MailerMessage;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 30.07.12
 */
@Service
public class MessageServiceImpl implements MessageService {

    private final MailerMessageMapper mailerMessageMapper;

    @Autowired
    public MessageServiceImpl(MailerMessageMapper mailerMessageMapper) {
        this.mailerMessageMapper = mailerMessageMapper;
    }

    @Override
    public MailerMessage getMessageById(Long id) throws NotFoundException {
        MailerMessage message = mailerMessageMapper.selectByPrimaryKey(id);
        if (message == null) {
            throw new NotFoundException("Mailer message", id);
        }
        return message;
    }

    @Override
    public List<MailerMessage> getMessagesByUserId(Long userId, long offset, long limit, String searchString) {
		return mailerMessageMapper.selectByUserId(userId, offset, limit, searchString);
    }

    @Override
    public List<MailerMessage> getMessagesByCampaignId(Long campaignId, long offset, long limit, String searchString, Boolean open, Boolean failed, Boolean unsubscribed, Boolean abuse) {
		return mailerMessageMapper.selectByCampaignId(campaignId, offset, limit, searchString, open, failed, unsubscribed, abuse);
    }

    @Override
    public MailerMessage getMessageByUserIdAndCampaignId(Long userId, Long campaignId) {
        return mailerMessageMapper.selectByUserIdAndCampaignId(userId, campaignId);
    }

	@Override
    public long countMessagesByCampaignId(Long campaignId, Boolean isOpen, Boolean isFailed, Boolean isUnsubscribed, Boolean isAbuse, String domainName) {
        return mailerMessageMapper.countByCampaignId(campaignId, domainName, isOpen, isFailed, isUnsubscribed, isAbuse);
    }

    @Override
    public long countMessages(Boolean isOpen, Boolean isFailed, Boolean isUnsubscribed, Boolean isAbuse, String domainName, Date fromDate, Date toDate) {
        return mailerMessageMapper.countByParameters(domainName, isOpen, isFailed, isUnsubscribed, isAbuse, fromDate, toDate);
    }

    @Override
    public long countMessagesByUserId(Long userId) {
        return mailerMessageMapper.countByUserId(userId);
    }

    @Override
    public MailerMessage getMessageByMessageId(String messageId) throws NotFoundException {
        MailerMessage message = mailerMessageMapper.selectByMessageId(messageId.trim());
        if (message == null) {
            throw new NotFoundException("Mailer message with ID=" + messageId + " not found");
        }
        return message;
    }

    @Override
    public void saveMessage(MailerMessage message) {
        if (message.getMailerMessageId() == null) {
            mailerMessageMapper.insert(message);
        } else {
            mailerMessageMapper.updateByPrimaryKey(message);
        }
    }

    @Override
    public void deleteMessage(Long id) throws NotFoundException {
        MailerMessage message = mailerMessageMapper.selectByPrimaryKey(id);
        if (message == null) {
            throw new NotFoundException("Mailer message", id);
        }
        mailerMessageMapper.deleteByPrimaryKey(message.getMailerMessageId());
    }

    @Override
    public void setMessageOpen(User user, Campaign campaign) throws NotFoundException, DecoderException {
        MailerMessage mailerMessage = getMessageByUserIdAndCampaignId(user.getUserId(), campaign.getCampaignId());
        if (mailerMessage != null) {
            mailerMessage.setOpen(true);
            saveMessage(mailerMessage);
        }
    }

    @Override
    public void setMessageUnsubscribed(User user, Campaign campaign) throws NotFoundException, DecoderException {
        MailerMessage mailerMessage = getMessageByUserIdAndCampaignId(user.getUserId(), campaign.getCampaignId());
        if (mailerMessage != null) {
            mailerMessage.setUnsubscribed(true);
            saveMessage(mailerMessage);
        }
    }
}

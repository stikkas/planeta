package ru.planeta.mailer.service;

import org.springframework.ui.ModelMap;
import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.model.generated.Template;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 23.07.12
 */
public interface TemplateService {

    /**
     * Get a list of all templates
     * @return
     */
    List<Template> getTemplates();

    List<Template> getTemplates(String query, int offset, int limit);

    /**
     * Get template by an identifier
     * @param templateId
     * @return
     * @throws NotFoundException
     */
    Template getTemplateById(long templateId) throws NotFoundException;

    /**
     * Save template
     * @param template
     */
    void saveTemplate(Template template);

    /**
     * Delete template
     * @param templateId
     * @throws NotFoundException
     */
    void deleteTemplate(long templateId) throws NotFoundException;

    /**
     * Renders template to message
     * @param content
     * @param model
     * @return
     */
    String renderMessageHtml(String content, ModelMap model);
}

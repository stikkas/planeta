package ru.planeta.mailer.model.custom;

import ru.planeta.mailer.model.generated.Attribute;
import ru.planeta.mailer.model.enums.Predicate;

/**
 * Created by IntelliJ IDEA.
 * User: s.fionov
 * Date: 10.07.12
 */
public class Filter {
    private Attribute filteredAttribute;

    private Predicate predicate;
    private String object;

    public Attribute getFilteredAttribute() {
        return filteredAttribute;
    }

    public void setFilteredAttribute(Attribute filteredAttribute) {
        this.filteredAttribute = filteredAttribute;
    }

    public Predicate getPredicate() {
        return predicate;
    }

    public void setPredicate(Predicate predicate) {
        this.predicate = predicate;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }
}

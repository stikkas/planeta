package ru.planeta.mailer.model.custom;

public class Value extends AbstractValue {

    private String displayName;

	@Override
    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
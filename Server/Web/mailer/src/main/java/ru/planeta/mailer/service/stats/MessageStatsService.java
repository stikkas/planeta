package ru.planeta.mailer.service.stats;

import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.model.generated.Campaign;
import ru.planeta.mailer.model.generated.User;
import ru.planeta.mailer.model.stats.CampaignStats;
import ru.planeta.mailer.model.stats.MessageInfo;
import ru.planeta.reports.ReportType;
import ru.planeta.reports.SimpleReport;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 07.08.12
 */
public interface MessageStatsService {

    List getUserMessages(User user, long offset, long limit, String searchString) throws NotFoundException;

    List<MessageInfo> getCampaignMessages(Campaign campaign, long offset, long limit, String searchString, Boolean open, Boolean failed, Boolean unsubscribed, Boolean abuse) throws NotFoundException;

    long countCampaignMessages(Campaign campaign) throws NotFoundException;

    long countUserMessages(User user) throws NotFoundException;

    /**
     * Update campaign stats
     * @param campaign
     * @return
     * @throws NotFoundException
     */
    void updateCampaignStats(Campaign campaign) throws NotFoundException;

    void updateCampaignStats(Campaign campaign, boolean resend) throws NotFoundException;

    void updateTotalCampaignStats() throws NotFoundException;

    CampaignStats getCampaignStats(Campaign campaign, String domainName) throws NotFoundException;

    CampaignStats getTotalCampaignStats(String domainName) throws NotFoundException;

	/**
	 * Directly counts messages in db. Maybe really slow.
	 *
	 * @param domainName
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	CampaignStats getTotalCampaignStats(String domainName, Date fromDate, Date toDate) throws NotFoundException;

	/**
	 * Selects stats for campaign and domains in MailDomain enum
	 * @param campaign
	 * @return
	 */
	Map<String, CampaignStats> getCampaignDomainStats(Campaign campaign) throws NotFoundException;

	/**
	 * Selects total stats for domains in MailDomain enum
	 * @return
	 */
	Map<String, CampaignStats> getTotalCampaignDomainStats() throws NotFoundException;

	/**
	 * Selects total stats for domains map by time interval
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	Map<String, CampaignStats> getTotalCampaignDomainStats(Date fromDate, Date toDate) throws NotFoundException;

    SimpleReport getCampaignStatsReport(long campaignId, ReportType reportType) throws NotFoundException;

    SimpleReport getCampaignTotalStatsReport(Date dateFrom, Date dateTo, ReportType reportType) throws NotFoundException;

    void increaseCompaignAdresses(Campaign campaign, long delta);
}

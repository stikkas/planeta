package ru.planeta.mailer.dao.custom;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import ru.planeta.mailer.model.custom.FilterList;

import java.util.List;

@Repository
public interface FilterListMapper {

    int deleteByPrimaryKey(Long filterListId);

    int insert(FilterList record);

    List<FilterList> selectListWithBLOBs();

    List<FilterList> selectList(@Param("query") String query, @Param("offset") int offset, @Param("limit") int limit);

    FilterList selectByPrimaryKey(Long filterListId);

    int updateByPrimaryKeyWithBLOBs(FilterList record);

    int updateByPrimaryKey(FilterList record);
}
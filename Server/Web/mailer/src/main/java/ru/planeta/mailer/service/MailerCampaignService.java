package ru.planeta.mailer.service;

import ru.planeta.mailer.exceptions.NotFoundException;
import ru.planeta.mailer.google.analytics.GoogleAnalyticsQueryJson;
import ru.planeta.mailer.model.generated.Campaign;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 24.07.12
 */
public interface MailerCampaignService {

    /**
     * Get a full list of campaigns
     * @return
     */
    List<Campaign> getCampaigns();

    /**
     * Get campaign by identifier
     * @param id
     * @return
     * @throws NotFoundException
     */
    Campaign getCampaignById(long id) throws NotFoundException;

    /**
     * Save campaign
     * @param campaign
     */
    void saveCampaign(Campaign campaign);

    /**
     * Delete campaign
     * @param id
     * @throws NotFoundException
     */
    void deleteCampaign(long id) throws NotFoundException;

    /**
     * Send campaign to subscribers
     * @param campaign
     */
    void sendCampaign(Campaign campaign) throws NotFoundException;

	/**
	 * Stops running campaign
	 * @param campaign
	 */
	void stopCampaign(Campaign campaign);

    /**
     * Get a list of possible from names
     * @return
     */
    String[] getFromNames();

    GoogleAnalyticsQueryJson getGoogleAnalyticsQueryJson(Campaign campaign);

    GoogleAnalyticsQueryJson getGoogleAnalyticsQueryJsonWithoutUserID(Campaign campaign);

    List<String> getActiveUsersEmails(GoogleAnalyticsQueryJson analyticsQuery);

    List<Campaign> getCampaigns(String campaignName, Date dateFrom, Date dateTo, int offset, int limit, Boolean draftCampaignChecked);

    List<Campaign> getCampaignsWithTimeToSend(Date dateFrom, Date dateTo, int offset, int limit);

    int getCampaignsCount(String campaignName, Date dateFrom, Date dateTo, int offset, int limit, Boolean draftCampaignChecked);

    GoogleAnalyticsQueryJson getGoogleAnalyticsQueryJsonOfNewslettersByDateRange(Date dateFrom, Date dateTo);
}

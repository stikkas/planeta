<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/includes/taglibs.jsp" %>

<c:choose>
    <c:when test="${fn:contains(requestScope['javax.servlet.forward.servlet_path'],'/list')}">
        <c:set var="section" value="list" scope="request" />
    </c:when>
    <c:when test="${fn:contains(requestScope['javax.servlet.forward.servlet_path'],'/template')}">
        <c:set var="section" value="template" scope="request" />
    </c:when>
    <c:when test="${fn:contains(requestScope['javax.servlet.forward.servlet_path'],'/users')}">
        <c:set var="section" value="users" scope="request" />
    </c:when>
    <c:when test="${fn:contains(requestScope['javax.servlet.forward.servlet_path'],'/campaigns/stats')}">
        <c:set var="section" value="campaigns" scope="request" />
    </c:when>
    <c:otherwise>
        <c:set var="section" value="index" scope="request" />
    </c:otherwise>
</c:choose>

<link rel="stylesheet" type="text/css" href="/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="/css/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="/css/redactor.css" />
<link rel="stylesheet" type="text/css" href="/css/mailer.css?_rnd=${pageContext.session.id}" />
<link rel="stylesheet" type="text/css" href="/css/datepicker/base.css"/>
<link rel="stylesheet" type="text/css" href="/css/datepicker/clean.css"/>
<link rel="stylesheet" type="text/css" href="/css/bootstrap-switch.css"/>
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
<script type="text/javascript" src="/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/js/jquery.ui.datepicker-ru.js"></script>
<script type="text/javascript" src="/js/underscore.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-modal.js"></script>
<script type="text/javascript" src="/js/redactor-ru.js"></script>
<script type="text/javascript" src="/js/redactor.js"></script>
<script type="text/javascript" src="/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/js/mailer.js?_rnd=${pageContext.session.id}"></script>
<script type="text/javascript" src="/js/chart-utils.js?_rnd=${pageContext.session.id}"></script>
<script type="text/javascript" src="/js/datepicker.js"></script>
<script type="text/javascript" src="/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/js/jquery-ui-timepicker-addon-ru.js"></script>
<script type="text/javascript" src="/js/bootstrap-switch.js"></script>

<script type="text/javascript" src="/js/select2.full.min.js"></script>
<link rel="stylesheet" type="text/css" href="/css/select2.min.css"/>

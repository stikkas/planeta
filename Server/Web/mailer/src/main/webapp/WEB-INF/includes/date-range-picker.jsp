<script>
    $(document).ready(function() {

        var calendar = $('#datepicker-calendar');

        $('#todayButton').click(function() {
            var today = new Date();
            calendar.DatePickerSetDate([today, today], true);
            switchButtonAndSubmit($(this), [calendar.DatePickerGetDate()[0][0], calendar.DatePickerGetDate()[0][1]]);
        });

        $('#yesterdayButton').click(function() {
            var today = new Date();
            var yesterday = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1)
            calendar.DatePickerSetDate([yesterday, yesterday], true);
            switchButtonAndSubmit($(this), [calendar.DatePickerGetDate()[0][0], calendar.DatePickerGetDate()[0][1]]);
        });

        $('#7dayButton').click(function() {
            var today = new Date();
            var c7day = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7)
            calendar.DatePickerSetDate([c7day, today], true);
            switchButtonAndSubmit($(this), [calendar.DatePickerGetDate()[0][0], calendar.DatePickerGetDate()[0][1]]);
        });

        $('#30dayButton').click(function() {
            var today = new Date();
            var c30day = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 30)
            calendar.DatePickerSetDate([c30day, today], true);
            switchButtonAndSubmit($(this), [calendar.DatePickerGetDate()[0][0], calendar.DatePickerGetDate()[0][1]]);
        });

        $('#monthButton').click(function() {
            var date = new Date();
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            calendar.DatePickerSetDate([firstDay, lastDay], true);
            switchButtonAndSubmit($(this), [calendar.DatePickerGetDate()[0][0], calendar.DatePickerGetDate()[0][1]]);
        });

        $('#lastMonthButton').click(function() {
            var date = new Date();
            var firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth(), 0);
            calendar.DatePickerSetDate([firstDay, lastDay], true);
            switchButtonAndSubmit($(this), [calendar.DatePickerGetDate()[0][0], calendar.DatePickerGetDate()[0][1]]);
        });

        $('#allTimeButton').click(function() {
            var date = new Date();
            var firstDay = new Date(1900, 1, 1);
            var lastDay = new Date(date.getFullYear() + 100, date.getMonth(), 0);
            calendar.DatePickerSetDate([firstDay, lastDay], true);
            switchButtonAndSubmit($(this), [calendar.DatePickerGetDate()[0][0], calendar.DatePickerGetDate()[0][1]]);
        });

        $('#date-range-field').change(function() {
            $('.btn').removeClass('active');
            updateHiddenDates();
        });

    });

    function switchButtonAndSubmit(buttonPressed, datesS) {
        var dates = {
            0: new Date(datesS[0]),
            1: new Date(datesS[1])
        };
        updateHiddenDatesAndSubmit();
    }

    function updateHiddenDates() {
        var datePickerDate = $('#datepicker-calendar').DatePickerGetDate();
        var datesS = [datePickerDate[0][0], datePickerDate[0][1]];
        var dates = {
            0: new Date(datesS[0]),
            1: new Date(datesS[1])
        };

        $('input[name="dateFrom"]').val(dates[0].getTime());
        $('input[name="dateTo"]').val(dates[1].getTime());
    }

    function updateHiddenDatesAndSubmit() {
        updateHiddenDates();
        $('#stat-params').submit();
    }

    window.onload = function onWindowLoad() {
        var dates = [
            new Date(<c:out value="${dateFrom}"/>),
            new Date(<c:out value="${dateTo}"/>)
        ];

        // set previous date
        $('#datepicker-calendar').DatePickerSetDate(dates, true);
        updateHiddenDates();
        dates = [
            new Date(<c:out value="${dateFrom}"/>),
            new Date(<c:out value="${dateTo}"/>)
        ];
        $('#date-range-field').val(
                dates[0].getDate() + ' ' + dates[0].getMonthName(true) + ', ' + dates[0].getFullYear() + ' - ' +
                        dates[1].getDate() + ' ' + dates[1].getMonthName(true) + ', ' + dates[1].getFullYear()
        );

    }

</script>



<div class="input-append" id="date-range">
<div id="datepicker-calendar" style="display: none;">
<div class="datepicker" id="datepicker_850"
     style="display: block; position: relative; width: 0px; height: 0px;">
<div class="datepickerBorderT"></div>
<div class="datepickerBorderB"></div>
<div class="datepickerBorderL"></div>
<div class="datepickerBorderR"></div>
<div class="datepickerBorderTL"></div>
<div class="datepickerBorderTR"></div>
<div class="datepickerBorderBL"></div>
<div class="datepickerBorderBR"></div>
<div class="datepickerContainer" style="width: 0; height: 0;">
<table cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="datepickerBlock">
    <table cellspacing="0" cellpadding="0"
           class="datepickerViewDays datepickerFirstView">
        <thead>
        <tr>
            <th colspan="7"><a class="datepickerGoPrev" href="#"><span>◀</span></a><a
                    class="datepickerMonth" href="#"><span>January, 2013</span></a><a
                    class="datepickerGoNext" href="#"><span>▶</span></a></th>
        </tr>
        <tr class="datepickerDoW">
            <th><span>S</span></th>
            <th><span>M</span></th>
            <th><span>T</span></th>
            <th><span>W</span></th>
            <th><span>T</span></th>
            <th><span>F</span></th>
            <th><span>S</span></th>
        </tr>
        </thead>
        <tbody class="datepickerMonths">
        <tr>
            <td colspan="2"><a href="#"><span>Jan</span></a></td>
            <td colspan="2"><a href="#"><span>Feb</span></a></td>
            <td colspan="2"><a href="#"><span>Mar</span></a></td>
            <td colspan="1"><a href="#"><span>Apr</span></a></td>
        </tr>
        <tr>
            <td colspan="2"><a href="#"><span>May</span></a></td>
            <td colspan="2"><a href="#"><span>Jun</span></a></td>
            <td colspan="2"><a href="#"><span>Jul</span></a></td>
            <td colspan="1"><a href="#"><span>Aug</span></a></td>
        </tr>
        <tr>
            <td colspan="2"><a href="#"><span>Sep</span></a></td>
            <td colspan="2"><a href="#"><span>Oct</span></a></td>
            <td colspan="2"><a href="#"><span>Nov</span></a></td>
            <td colspan="1"><a href="#"><span>Dec</span></a></td>
        </tr>
        </tbody>
        <tbody class="datepickerDays">
        <tr>
            <td class="datepickerNotInMonth datepickerDisabled datepickerSunday"><a
                    href="#"><span>30</span></a></td>
            <td class="datepickerNotInMonth datepickerDisabled"><a
                    href="#"><span>31</span></a></td>
            <td class=""><a href="#"><span>1</span></a></td>
            <td class=""><a href="#"><span>2</span></a></td>
            <td class=""><a href="#"><span>3</span></a></td>
            <td class=""><a href="#"><span>4</span></a></td>
            <td class="datepickerSaturday"><a href="#"><span>5</span></a></td>
        </tr>
        <tr>
            <td class="datepickerSunday"><a href="#"><span>6</span></a></td>
            <td class=""><a href="#"><span>7</span></a></td>
            <td class=""><a href="#"><span>8</span></a></td>
            <td class=""><a href="#"><span>9</span></a></td>
            <td class=""><a href="#"><span>10</span></a></td>
            <td class=""><a href="#"><span>11</span></a></td>
            <td class="datepickerSaturday"><a href="#"><span>12</span></a></td>
        </tr>
        <tr>
            <td class="datepickerSunday"><a href="#"><span>13</span></a></td>
            <td class=""><a href="#"><span>14</span></a></td>
            <td class=""><a href="#"><span>15</span></a></td>
            <td class=""><a href="#"><span>16</span></a></td>
            <td class=""><a href="#"><span>17</span></a></td>
            <td class=""><a href="#"><span>18</span></a></td>
            <td class="datepickerSaturday"><a href="#"><span>19</span></a></td>
        </tr>
        <tr>
            <td class="datepickerSunday"><a href="#"><span>20</span></a></td>
            <td class=""><a href="#"><span>21</span></a></td>
            <td class=""><a href="#"><span>22</span></a></td>
            <td class=""><a href="#"><span>23</span></a></td>
            <td class=""><a href="#"><span>24</span></a></td>
            <td class=""><a href="#"><span>25</span></a></td>
            <td class="datepickerSaturday"><a href="#"><span>26</span></a></td>
        </tr>
        <tr>
            <td class="datepickerSunday"><a href="#"><span>27</span></a></td>
            <td class=""><a href="#"><span>28</span></a></td>
            <td class=""><a href="#"><span>29</span></a></td>
            <td class=""><a href="#"><span>30</span></a></td>
            <td class=""><a href="#"><span>31</span></a></td>
            <td class="datepickerNotInMonth datepickerDisabled"><a
                    href="#"><span>1</span></a></td>
            <td class="datepickerNotInMonth datepickerDisabled datepickerSaturday"><a
                    href="#"><span>2</span></a></td>
        </tr>
        <tr>
            <td class="datepickerNotInMonth datepickerDisabled datepickerSunday"><a
                    href="#"><span>3</span></a></td>
            <td class="datepickerNotInMonth datepickerDisabled"><a
                    href="#"><span>4</span></a></td>
            <td class="datepickerNotInMonth datepickerDisabled"><a
                    href="#"><span>5</span></a></td>
            <td class="datepickerNotInMonth datepickerDisabled"><a
                    href="#"><span>6</span></a></td>
            <td class="datepickerNotInMonth datepickerDisabled"><a
                    href="#"><span>7</span></a></td>
            <td class="datepickerNotInMonth datepickerDisabled"><a
                    href="#"><span>8</span></a></td>
            <td class="datepickerNotInMonth datepickerDisabled datepickerSaturday"><a
                    href="#"><span>9</span></a></td>
        </tr>
        </tbody>
        <tbody class="datepickerYears">
        <tr>
            <td colspan="2"><a href="#"><span>2007</span></a></td>
            <td colspan="2"><a href="#"><span>2008</span></a></td>
            <td colspan="2"><a href="#"><span>2009</span></a></td>
            <td colspan="1"><a href="#"><span>2010</span></a></td>
        </tr>
        <tr>
            <td colspan="2"><a href="#"><span>2011</span></a></td>
            <td colspan="2"><a href="#"><span>2012</span></a></td>
            <td colspan="2"><a href="#"><span>2013</span></a></td>
            <td colspan="1"><a href="#"><span>2014</span></a></td>
        </tr>
        <tr>
            <td colspan="2"><a href="#"><span>2015</span></a></td>
            <td colspan="2"><a href="#"><span>2016</span></a></td>
            <td colspan="2"><a href="#"><span>2017</span></a></td>
            <td colspan="1"><a href="#"><span>2018</span></a></td>
        </tr>
        </tbody>
    </table>
</td>
<td class="datepickerSpace">
    <div></div>
</td>
<td class="datepickerBlock">
    <table cellspacing="0" cellpadding="0" class="datepickerViewDays">
        <thead>
        <tr>
            <th colspan="7"><a class="datepickerGoPrev" href="#"><span>◀</span></a><a
                    class="datepickerMonth" href="#"><span>February, 2013</span></a><a
                    class="datepickerGoNext" href="#"><span>▶</span></a></th>
        </tr>
        <tr class="datepickerDoW">
            <th><span>S</span></th>
            <th><span>M</span></th>
            <th><span>T</span></th>
            <th><span>W</span></th>
            <th><span>T</span></th>
            <th><span>F</span></th>
            <th><span>S</span></th>
        </tr>
        </thead>
        <tbody class="datepickerMonths">
        <tr>
            <td colspan="2"><a href="#"><span>Jan</span></a></td>
            <td colspan="2"><a href="#"><span>Feb</span></a></td>
            <td colspan="2"><a href="#"><span>Mar</span></a></td>
            <td colspan="1"><a href="#"><span>Apr</span></a></td>
        </tr>
        <tr>
            <td colspan="2"><a href="#"><span>May</span></a></td>
            <td colspan="2"><a href="#"><span>Jun</span></a></td>
            <td colspan="2"><a href="#"><span>Jul</span></a></td>
            <td colspan="1"><a href="#"><span>Aug</span></a></td>
        </tr>
        <tr>
            <td colspan="2"><a href="#"><span>Sep</span></a></td>
            <td colspan="2"><a href="#"><span>Oct</span></a></td>
            <td colspan="2"><a href="#"><span>Nov</span></a></td>
            <td colspan="1"><a href="#"><span>Dec</span></a></td>
        </tr>
        </tbody>
        <tbody class="datepickerDays">
        <tr>
            <td class="datepickerNotInMonth datepickerDisabled datepickerSunday"><a
                    href="#"><span>27</span></a></td>
            <td class="datepickerNotInMonth datepickerDisabled"><a
                    href="#"><span>28</span></a></td>
            <td class="datepickerNotInMonth datepickerDisabled"><a
                    href="#"><span>29</span></a></td>
            <td class="datepickerNotInMonth datepickerDisabled"><a
                    href="#"><span>30</span></a></td>
            <td class="datepickerNotInMonth datepickerDisabled"><a
                    href="#"><span>31</span></a></td>
            <td class=""><a href="#"><span>1</span></a></td>
            <td class="datepickerSaturday"><a href="#"><span>2</span></a></td>
        </tr>
        <tr>
            <td class="datepickerSunday"><a href="#"><span>3</span></a></td>
            <td class=""><a href="#"><span>4</span></a></td>
            <td class=""><a href="#"><span>5</span></a></td>
            <td class=""><a href="#"><span>6</span></a></td>
            <td class=""><a href="#"><span>7</span></a></td>
            <td class=""><a href="#"><span>8</span></a></td>
            <td class="datepickerSaturday"><a href="#"><span>9</span></a></td>
        </tr>
        <tr>
            <td class="datepickerSunday"><a href="#"><span>10</span></a></td>
            <td class=""><a href="#"><span>11</span></a></td>
            <td class=""><a href="#"><span>12</span></a></td>
            <td class=""><a href="#"><span>13</span></a></td>
            <td class=""><a href="#"><span>14</span></a></td>
            <td class=""><a href="#"><span>15</span></a></td>
            <td class="datepickerSaturday"><a href="#"><span>16</span></a></td>
        </tr>
        <tr>
            <td class="datepickerSunday"><a href="#"><span>17</span></a></td>
            <td class=""><a href="#"><span>18</span></a></td>
            <td class=""><a href="#"><span>19</span></a></td>
            <td class=""><a href="#"><span>20</span></a></td>
            <td class=""><a href="#"><span>21</span></a></td>
            <td class=""><a href="#"><span>22</span></a></td>
            <td class="datepickerSaturday"><a href="#"><span>23</span></a></td>
        </tr>
        <tr>
            <td class="datepickerSunday"><a href="#"><span>24</span></a></td>
            <td class=""><a href="#"><span>25</span></a></td>
            <td class=""><a href="#"><span>26</span></a></td>
            <td class=""><a href="#"><span>27</span></a></td>
            <td class=""><a href="#"><span>28</span></a></td>
            <td class="datepickerNotInMonth datepickerDisabled"><a
                    href="#"><span>1</span></a></td>
            <td class="datepickerNotInMonth datepickerDisabled datepickerSaturday"><a
                    href="#"><span>2</span></a></td>
        </tr>
        <tr>
            <td class="datepickerNotInMonth datepickerDisabled datepickerSunday"><a
                    href="#"><span>3</span></a></td>
            <td class="datepickerNotInMonth datepickerDisabled"><a
                    href="#"><span>4</span></a></td>
            <td class="datepickerNotInMonth datepickerDisabled"><a
                    href="#"><span>5</span></a></td>
            <td class="datepickerNotInMonth datepickerDisabled"><a
                    href="#"><span>6</span></a></td>
            <td class="datepickerNotInMonth datepickerDisabled"><a
                    href="#"><span>7</span></a></td>
            <td class="datepickerNotInMonth datepickerDisabled"><a
                    href="#"><span>8</span></a></td>
            <td class="datepickerNotInMonth datepickerDisabled datepickerSaturday"><a
                    href="#"><span>9</span></a></td>
        </tr>
        </tbody>
        <tbody class="datepickerYears">
        <tr>
            <td colspan="2"><a href="#"><span>2007</span></a></td>
            <td colspan="2"><a href="#"><span>2008</span></a></td>
            <td colspan="2"><a href="#"><span>2009</span></a></td>
            <td colspan="1"><a href="#"><span>2010</span></a></td>
        </tr>
        <tr>
            <td colspan="2"><a href="#"><span>2011</span></a></td>
            <td colspan="2"><a href="#"><span>2012</span></a></td>
            <td colspan="2"><a href="#"><span>2013</span></a></td>
            <td colspan="1"><a href="#"><span>2014</span></a></td>
        </tr>
        <tr>
            <td colspan="2"><a href="#"><span>2015</span></a></td>
            <td colspan="2"><a href="#"><span>2016</span></a></td>
            <td colspan="2"><a href="#"><span>2017</span></a></td>
            <td colspan="1"><a href="#"><span>2018</span></a></td>
        </tr>
        </tbody>
    </table>
</td>
<td class="datepickerSpace">
    <div></div>
</td>
<td class="datepickerBlock">
    <table cellspacing="0" cellpadding="0"
           class="datepickerViewDays datepickerLastView">
        <thead>
        <tr>
            <th colspan="7"><a class="datepickerGoPrev" href="#"><span>◀</span></a><a
                    class="datepickerMonth" href="#"><span>March, 2013</span></a><a
                    class="datepickerGoNext" href="#"><span>▶</span></a></th>
        </tr>
        <tr class="datepickerDoW">
            <th><span>S</span></th>
            <th><span>M</span></th>
            <th><span>T</span></th>
            <th><span>W</span></th>
            <th><span>T</span></th>
            <th><span>F</span></th>
            <th><span>S</span></th>
        </tr>
        </thead>
        <tbody class="datepickerMonths">
        <tr>
            <td colspan="2"><a href="#"><span>Jan</span></a></td>
            <td colspan="2"><a href="#"><span>Feb</span></a></td>
            <td colspan="2"><a href="#"><span>Mar</span></a></td>
            <td colspan="1"><a href="#"><span>Apr</span></a></td>
        </tr>
        <tr>
            <td colspan="2"><a href="#"><span>May</span></a></td>
            <td colspan="2"><a href="#"><span>Jun</span></a></td>
            <td colspan="2"><a href="#"><span>Jul</span></a></td>
            <td colspan="1"><a href="#"><span>Aug</span></a></td>
        </tr>
        <tr>
            <td colspan="2"><a href="#"><span>Sep</span></a></td>
            <td colspan="2"><a href="#"><span>Oct</span></a></td>
            <td colspan="2"><a href="#"><span>Nov</span></a></td>
            <td colspan="1"><a href="#"><span>Dec</span></a></td>
        </tr>
        </tbody>
        <tbody class="datepickerDays">
        <tr>
            <td class="datepickerNotInMonth datepickerDisabled datepickerSunday"><a
                    href="#"><span>24</span></a></td>
            <td class="datepickerNotInMonth datepickerDisabled"><a
                    href="#"><span>25</span></a></td>
            <td class="datepickerNotInMonth datepickerDisabled"><a
                    href="#"><span>26</span></a></td>
            <td class="datepickerNotInMonth datepickerDisabled"><a
                    href="#"><span>27</span></a></td>
            <td class="datepickerNotInMonth datepickerDisabled"><a
                    href="#"><span>28</span></a></td>
            <td class=""><a href="#"><span>1</span></a></td>
            <td class="datepickerSaturday"><a href="#"><span>2</span></a></td>
        </tr>
        <tr>
            <td class="datepickerSunday"><a href="#"><span>3</span></a></td>
            <td class=""><a href="#"><span>4</span></a></td>
            <td class=""><a href="#"><span>5</span></a></td>
            <td class=""><a href="#"><span>6</span></a></td>
            <td class=""><a href="#"><span>7</span></a></td>
            <td class=""><a href="#"><span>8</span></a></td>
            <td class="datepickerSaturday"><a href="#"><span>9</span></a></td>
        </tr>
        <tr>
            <td class="datepickerSunday"><a href="#"><span>10</span></a></td>
            <td class=""><a href="#"><span>11</span></a></td>
            <td class=""><a href="#"><span>12</span></a></td>
            <td class=""><a href="#"><span>13</span></a></td>
            <td class=""><a href="#"><span>14</span></a></td>
            <td class="datepickerSelected"><a href="#"><span>15</span></a></td>
            <td class="datepickerSaturday datepickerSelected"><a
                    href="#"><span>16</span></a></td>
        </tr>
        <tr>
            <td class="datepickerSunday datepickerSelected"><a href="#"><span>17</span></a>
            </td>
            <td class="datepickerSelected"><a href="#"><span>18</span></a></td>
            <td class="datepickerSelected"><a href="#"><span>19</span></a></td>
            <td class="datepickerSelected"><a href="#"><span>20</span></a></td>
            <td class="datepickerSelected"><a href="#"><span>21</span></a></td>
            <td class="datepickerSelected"><a href="#"><span>22</span></a></td>
            <td class="datepickerSaturday datepickerSelected"><a
                    href="#"><span>23</span></a></td>
        </tr>
        <tr>
            <td class="datepickerSunday datepickerSelected"><a href="#"><span>24</span></a>
            </td>
            <td class="datepickerSelected"><a href="#"><span>25</span></a></td>
            <td class="datepickerSelected"><a href="#"><span>26</span></a></td>
            <td class="datepickerSelected"><a href="#"><span>27</span></a></td>
            <td class="datepickerSelected"><a href="#"><span>28</span></a></td>
            <td class="datepickerToday datepickerSelected"><a
                    href="#"><span>29</span></a></td>
            <td class="datepickerFuture datepickerSaturday"><a href="#"><span>30</span></a>
            </td>
        </tr>
        <tr>
            <td class="datepickerFuture datepickerSunday"><a
                    href="#"><span>31</span></a></td>
            <td class="datepickerFuture datepickerNotInMonth datepickerDisabled"><a
                    href="#"><span>1</span></a></td>
            <td class="datepickerFuture datepickerNotInMonth datepickerDisabled"><a
                    href="#"><span>2</span></a></td>
            <td class="datepickerFuture datepickerNotInMonth datepickerDisabled"><a
                    href="#"><span>3</span></a></td>
            <td class="datepickerFuture datepickerNotInMonth datepickerDisabled"><a
                    href="#"><span>4</span></a></td>
            <td class="datepickerFuture datepickerNotInMonth datepickerDisabled"><a
                    href="#"><span>5</span></a></td>
            <td class="datepickerFuture datepickerNotInMonth datepickerDisabled datepickerSaturday">
                <a href="#"><span>6</span></a></td>
        </tr>
        </tbody>
        <tbody class="datepickerYears">
        <tr>
            <td colspan="2"><a href="#"><span>2007</span></a></td>
            <td colspan="2"><a href="#"><span>2008</span></a></td>
            <td colspan="2"><a href="#"><span>2009</span></a></td>
            <td colspan="1"><a href="#"><span>2010</span></a></td>
        </tr>
        <tr>
            <td colspan="2"><a href="#"><span>2011</span></a></td>
            <td colspan="2"><a href="#"><span>2012</span></a></td>
            <td colspan="2"><a href="#"><span>2013</span></a></td>
            <td colspan="1"><a href="#"><span>2014</span></a></td>
        </tr>
        <tr>
            <td colspan="2"><a href="#"><span>2015</span></a></td>
            <td colspan="2"><a href="#"><span>2016</span></a></td>
            <td colspan="2"><a href="#"><span>2017</span></a></td>
            <td colspan="1"><a href="#"><span>2018</span></a></td>
        </tr>
        </tbody>
    </table>
</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
<input class="span4" type="text" id="date-range-field" name="date-range-field">
<input type="hidden" id="dateFrom" name="dateFrom">
<input type="hidden" id="dateTo" name="dateTo">
            <span class="add-on btn" id="date-range-add-on"><i class="icon-calendar"></i> <span
                    class="caret"></span></span>
</div>

<style type="text/css">
    #date-range {
        position: relative;
    }

    #datepicker-calendar {
        position: absolute;
        z-index: 3;
        top: 100%;
        left: 0;
        overflow: hidden;
        width: 497px;
        height: 153px;
        background-color: #f7f7f7;
        border: 1px solid #cccccc;
        border-radius: 0 5px 5px 5px;
        display: none;
        padding: 10px 0 0 10px;
    }

    #datepicker-calendar div.datepicker {
        background-color: transparent;
        border: none;
        border-radius: 0;
        padding: 0;
    }
</style>

<script>
    $(function () {
        var to = new Date();
        var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 14);

        $('#datepicker-calendar').DatePicker({
            date: [from, to],
            current: new Date(to.getFullYear(), to.getMonth() - 1, 1),
            inline: true,
            mode: 'range',
            calendars: 3,
            starts: 1,
            onChange: function (dates, el) {
                $('#date-range-field').val(
                        dates[0].getDate() + ' ' + dates[0].getMonthName(true) + ', ' + dates[0].getFullYear() + ' - ' +
                        dates[1].getDate() + ' ' + dates[1].getMonthName(true) + ', ' + dates[1].getFullYear()
                );
                $('#date-range-field').trigger('change');
            }
        });

        $('#date-range-field').val(from.getDate() + ' ' + from.getMonthName(true) + ', ' + from.getFullYear() + ' - ' +
                to.getDate() + ' ' + to.getMonthName(true) + ', ' + to.getFullYear());

        // datepicker show
        $('#date-range-field').bind('focusin', function () {
            $('#datepicker-calendar').show();
        });

        // datepicker toggle
        $('#date-range-add-on').bind('click', function () {
            if ($(this).hasClass('disabled')) {
                return;
            }
            $('#datepicker-calendar').toggle();
            if ($('#datepicker-calendar').css('display') == 'none') {
                $('#stat-params').submit();
            }
        });

        // datepicker hide
        $(document).click(function (e) {
            if (isEventOut($('#date-range > *'), e)) {
                var prevState = $('#datepicker-calendar').css('display');
                $('#datepicker-calendar').hide();
                if (prevState != 'none') {
                    $('#stat-params').submit();
                }
            }
        });

        function isEventOut(blocks, e) {
            var r = true;
            $(blocks).each(function () {
                if (!r) return;
                if ($(e.target).closest('HTML', $(this).get(0)).length == 0) {
                    r = false;
                    return;
                }
                if ($(e.target).get(0) == $(this).get(0)) {
                    r = false;
                    return;
                }
            });
            return r;
        }
    });
</script>
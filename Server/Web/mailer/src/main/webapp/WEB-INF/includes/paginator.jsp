<script type="text/javascript">
    function addParamToUrl (url, param, value) {
        var hashPos = url.indexOf("#");
        if (hashPos < 0) {
            hashPos = url.length;
        }

        var questPos = url.indexOf('?');
        if (questPos < 0) {
            questPos = hashPos;

        }
        var params = url.substring(questPos + 1, hashPos);
        if (params) {
            params = ("&" + params).replace(new RegExp("&" + param + "=[^&]*"), "");
        }
        return url.substring(0, questPos) + "?" + param + "=" + value + params + url.substring(hashPos);
    }

    function gotoPage(offset, limit){
        var newUrl = addParamToUrl(document.location.href, "offset", offset);
        document.location.href = addParamToUrl(newUrl, "limit", limit);
    }
</script>
<div class="row ">
    <div class="span12">
        <c:set var="page" value="${(offset div limit) + 1}"/>
        <c:set var="pageCount" value="${(count - 1) div limit + 1}"/>
        <c:set var="pagePrev" value="${page - 2}"/>
        <div class="pagination">
            <ul>
                <c:if test="${page > 1}">
                    <li>
                        <a href="javascript:gotoPage(${offset - limit}, ${limit});">Prev</a>
                    </li>
                </c:if>
                <c:if test="${page - 2 > 1}">
                    <li>
                        <a href="javascript:gotoPage(0, ${limit})">1</a>
                    </li>
                    <li><a>...</a></li>
                </c:if>
                <c:if test="${page - 2 >= 1}">
                    <li>
                        <a href="javascript:gotoPage(${offset - 2 * limit}, ${limit});"><fmt:formatNumber
                                maxFractionDigits="0" value="${page - 2}"/></a></li>
                </c:if>
                <c:if test="${page - 1 >= 1}">
                    <li>
                        <a href="javascript:gotoPage(${offset - 1 * limit}, ${limit});"><fmt:formatNumber
                                maxFractionDigits="0" value="${page - 1}"/></a></li>
                </c:if>
                <c:if test="${page < pageCount}">
                    <li class="active"><a
                            href="javascript:gotoPage(${offset}, ${limit});"><fmt:formatNumber
                            maxFractionDigits="0" value="${page}"/></a></li>
                </c:if>
                <c:if test="${page + 1 < pageCount}">
                    <li>
                        <a href="javascript:gotoPage(${offset + 1 * limit}, ${limit});"><fmt:formatNumber
                                maxFractionDigits="0" value="${page + 1}"/></a></li>
                </c:if>
                <c:if test="${page + 2 < pageCount}">
                    <li>
                        <a href="javascript:gotoPage(${offset + 2 * limit}, ${limit});"><fmt:formatNumber
                                maxFractionDigits="0" value="${page + 2}"/></a></li>
                </c:if>
                <c:if test="${pageCount - page > 1}">
                    <li>
                        <a href="javascript:gotoPage(${offset + 1 * limit}, ${limit});">Next</a>
                    </li>
                </c:if>
            </ul>
        </div>
    </div>
</div>

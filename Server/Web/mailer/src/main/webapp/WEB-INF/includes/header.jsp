<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/includes/taglibs.jsp" %>
<div class="navbar container">
    <div class="navbar-inner">
        <div class="container">
            <a class="brand">Рассылки Планеты</a>
            <ul class="nav">
                <li<c:if test="${section == 'index'}"> class="active"</c:if>><a href="/">Главная</a></li>
                <li<c:if test="${section == 'list'}"> class="active"</c:if>><a href="/lists">Списки</a></li>
                <li<c:if test="${section == 'users'}"> class="active"</c:if>><a href="/users">Адресаты</a></li>
                <li<c:if test="${section == 'template'}"> class="active"</c:if>><a href="/templates">Шаблоны</a></li>
                <li<c:if test="${section == 'campaigns'}"> class="active"</c:if>><a href="/campaigns/stats">Статистика</a></li>
            </ul>
        </div>
    </div>
</div>

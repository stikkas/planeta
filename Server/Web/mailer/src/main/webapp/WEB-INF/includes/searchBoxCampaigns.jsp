<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<form method="GET" action="/" class="clearfix">
        <div class="well">
            <legend>Поиск рассылки</legend>
            <div class="row-fluid">
                <div class="span10">
                    <input class="input-block-level" type="text" placeholder="введите любое слово для поиска по названию рассылки" name="searchString" value="${searchString}">
                    <input type="hidden" name="offset" value="0">
                    <input type="hidden" name="limit" value="20">
                </div>
                <div class="btn-group span2">
                    <button class="btn btn-primary btn-primary btn-block" type="submit" style="width: 100%;">
                        <i class="icon-search icon-white"></i>
                        Поиск
                    </button>
                </div>
            </div>

            <form class="form-inline">
                <div class="control-group">
                    <label class="checkbox inline">
                        <input value="true" type="checkbox" id="intervalCheckBox">За&nbsp;промежуток&nbsp;(по&nbsp;дате&nbsp;подтверждения)
                    </label>
                </div>

            <%@include file="date-range-picker.jsp"%>
            <br/>
            <div class="row-fluid">
                <input id="draftCampaignChecked" type="checkbox" name="draftCampaignChecked"
                       data-on-text="черновики"
                       data-off-text="рассылки"
                       data-label-text="Тип"
                       data-on-color="primary"
                       data-off-color="success"
                >
            </div>
            </form>
        </div>
</form>

<script type="text/javascript">

//    $('input[type=checkbox]').checkbox({wrapperClassName:''});

    $(document).ready(function(){

        $("#intervalCheckBox").prop("checked",${intervalChecked});
        $("#draftCampaignChecked").prop("checked",${draftCampaignChecked});
        $("[name='draftCampaignChecked']").bootstrapSwitch();
        $("#intervalCheckBox").change(intervalCheckboxChecked);
        intervalCheckboxChecked();
        if ($("#dateFromMs").val() != ''){
            $('#dateFrom').val(formatDateTime($("#dateFromMs").val(),'d.m.Y'));
        }

        if ($("#dateToMs").val() != ''){
            $('#dateTo').val(formatDateTime($("#dateToMs").val(),'d.m.Y'));
        }

        $("#dateFrom").datepicker({altField:$("#dateFromMs"),altFormat:'@',maxDate:new Date()});
        $("#dateTo").datepicker({altField:$("#dateToMs"),altFormat:'@',maxDate:new Date()});
    })

    function intervalCheckboxChecked(){
        var b = !$("#intervalCheckBox").prop("checked");
        $('#dateFrom').prop('disabled', b);
        $('#dateTo').prop('disabled', b);
        $('#date-range-field').prop('disabled', b);
        if (b) {
            $('#date-range-add-on').addClass('disabled');
        } else {
            $('#date-range-add-on').removeClass('disabled');
        }
    }

    function formatDateTime(time, format){
        if (time == null) {
            return "";
        }
        try {
            format = format ? format : 'd.m.y H:i';
            var dateItem = getDate(time);
            return dateItem.format(format);
        } catch (e) {
            return "";
        }
    }

    function getDate (time) {
        return new Date(parseInt(time));
    }
</script>
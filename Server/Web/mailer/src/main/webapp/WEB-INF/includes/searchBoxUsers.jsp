<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<form method="GET" action="/users" class="clearfix">
        <div class="well">
            <legend>Поиск пользователя</legend>
            <div class="row-fluid">
                <div class="span10">
                    <input class="input-block-level" type="text" placeholder="полное или частичное название email" name="searchString" value="${searchString}">
                    <input type="hidden" name="offset" value="0">
                    <input type="hidden" name="limit" value="20">
                </div>
                <div class="btn-group span2">
                    <button class="btn btn-primary btn-primary btn-block" type="submit" style="width: 100%;">
                        <i class="icon-search icon-white"></i>
                        Поиск
                    </button>
                </div>
            </div>
        </div>
</form>
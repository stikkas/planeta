<%@ tag language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ attribute name="offset" required="true" type="java.lang.Long" description="Current offset" %>
<%@ attribute name="limit" required="false" type="java.lang.Long" description="Page size" %>
<%@ attribute name="count" required="false" type="java.lang.Long" description="Records count" %>
<%@ attribute name="continued" required="false" type="java.lang.Boolean" description="Record continued" %>

<c:if test="${limit > 0 and (count > limit or offset > 0)}">
    <fmt:parseNumber var="page" type="number" integerOnly="true" value="${(offset div limit) + 1}"/>
    <fmt:parseNumber var="pageCount" type="number" integerOnly="true" value="${(count + limit - 1) div limit}"/>

    <div class="pagination">
        <ul>
            <my:pagelink disabled="${page == 1}" page="${page - 1}" limit="${limit}" active="false">&laquo;</my:pagelink>
            <c:if test="${page >= 3}">
                <my:pagelink page="1" limit="${limit}" active="false" disabled="false">1</my:pagelink>
            </c:if>
            <c:if test="${page >= 4}">
                <my:pagelink disabled="${page > 4}" page="2" limit="${limit}" active="false">
                    <c:choose><c:when test="${page <= 4}">2</c:when><c:otherwise>&hellip;</c:otherwise></c:choose>
                </my:pagelink>
            </c:if>
            <c:if test="${page >= 2}">
                <my:pagelink page="${page-1}" limit="${limit}" active="false" disabled="false">${page - 1}</my:pagelink>
            </c:if>
            <my:pagelink active="${(page - 1) * limit == offset}" page="${page}" limit="${limit}" disabled="false">${page}</my:pagelink>
            <c:if test="${pageCount - page + 1 >= 2}">
                <my:pagelink page="${page + 1}" limit="${limit}" disabled="false" active="false">${page + 1}</my:pagelink>
            </c:if>
            <c:if test="${pageCount - page + 1 >= 4}">
                <my:pagelink disabled="${pageCount - page + 1 > 4}" page="${pageCount - 1}" limit="${limit}" active="false">
                    <c:choose><c:when test="${pageCount - page + 1 <= 4}">${pageCount - 1}</c:when><c:otherwise>&hellip;</c:otherwise></c:choose>
                </my:pagelink>
            </c:if>
            <c:if test="${pageCount - page + 1 >= 3}">
                <my:pagelink page="${pageCount}" limit="${limit}" active="false" disabled="false">${pageCount}</my:pagelink>
            </c:if>
            <c:if test="${continued and page < pageCount}">
                <my:pagelink disabled="true" page="${pageCount}" limit="${limit}" active="false">&hellip;</my:pagelink>
            </c:if>
            <my:pagelink disabled="${pageCount - page + 1 == 1}" page="${page + 1}" limit="${limit}" active="false">&raquo;</my:pagelink>
        </ul>
    </div>
</c:if>


<%@ tag language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="page" required="true" type="java.lang.Long" description="Page number" %>
<%@ attribute name="limit" required="true" type="java.lang.Long" description="Page size" %>
<%@ attribute name="disabled" required="false" type="java.lang.Boolean" description="Page disabled" %>
<%@ attribute name="active" required="false" type="java.lang.Boolean" description="Page active" %>

<li class="<c:if test="${disabled}">disabled</c:if> <c:if test="${active}">active</c:if>">
    <a<c:if test="${! disabled}"> href="<c:url value="">
        <c:param name="offset" value="${(page - 1) * limit}"/>
        <c:param name="limit" value="${limit}"/>
        <c:if test="${requestScope.searchString != null and requestScope.searchString != ''}">
            <c:param name="query" value="${requestScope.searchString}"/>
        </c:if>
        <c:if test="${requestScope.open != null}">
            <c:param name="open" value="${requestScope.open}"/>
        </c:if>
        <c:if test="${requestScope.failed != null}">
            <c:param name="failed" value="${requestScope.failed}"/>
        </c:if>
        <c:if test="${requestScope.unsubscribed != null}">
            <c:param name="unsubscribed" value="${requestScope.unsubscribed}"/>
        </c:if>
    </c:url>"</c:if>><jsp:doBody/></a>
</li>



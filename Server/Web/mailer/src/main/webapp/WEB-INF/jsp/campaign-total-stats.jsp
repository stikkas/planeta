<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/includes/taglibs.jsp" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/includes.jsp"%>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <title>Статистика</title>

    <script type="text/javascript">
        var getReport = function(url, reportType) {
            if (reportType) {
                var form = document.getElementById('getfile');
                var fromDate = document.getElementById('dateFromHidden').value;
                var toDate = document.getElementById('dateToHidden').value;
                try {
                    form.setAttribute('action', url +  '?fromDate=' + fromDate + '&toDate=' + toDate + '&reportType=' + reportType);
                    form.submit();
                } catch (ex) {
                    console.log(ex);
                }
            }
        };

        var onChoiceTotalStatsReport = function(reportType) {
            getReport('/campaign/file-of-total-stats', reportType);
        };
    </script>
</head>
<body>
<jsp:include page="/WEB-INF/includes/header.jsp" />

<div class="container">
    <h3>
        Статистика
    </h3>

	<form method="get" class="form-horizontal">
		<div class="btn-toolbar">
			<input id="dateFrom" placeholder="Начальная дата" type="text" class="datepicker"/>
            <input id="dateTo" placeholder="Конечная дата" type="text" class="datepicker"/>
            <input name="dateFrom" id="dateFromHidden" type="hidden" value="${fromDate}"/>
            <input name="dateTo" id="dateToHidden" type="hidden" value="${toDate}"/>
            <button type="submit" class="btn date-filter-btn" value="week">Неделя</button>
            <button type="submit" class="btn date-filter-btn" value="month">Месяц</button>
            <button type="submit" class="btn date-filter-btn" value="all">Всё время</button>
            <button type="submit" class="btn btn-primary">Показать</button>
            <div class="btn-group pull-right">
                <button class="btn btn-expand dropdown-toggle" data-toggle="dropdown">
                    Скачать отчет
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="javascript:onChoiceTotalStatsReport('EXCEL')" class="download-icon-item">Excel</a></li>
                    <li><a href="javascript:onChoiceTotalStatsReport('CSV')" class="download-icon-item">CSV</a></li>
                </ul>
            </div>
            <a href="/campaigns/updatestats" class="btn">Обновить</a>
        </div>
	</form>
    <form method="post" id="getfile"></form>
    <div class="row">
        <div style="margin: 30px 80px;">
        <c:if test="${gaData != null}">
            <c:if test="${containsSampledData}">
            <div class="alert alert-info">
                <h4>Attention!</h4>
                This result from Google Analytics is based on <a href="https://developers.google.com/analytics/resources/concepts/gaConceptsSampling" target="_blank">sampled</a> data.<br>
            </div>
            </c:if>
            <table class="table table-striped table-bordered table-condensed" style="width: auto">
                <thead>
                <c:forEach items="${gaData.totalsForAllResults}" var="gaTotals">
                    <th class="column1">
                        <c:choose>
                            <c:when test="${gaTotals.key == 'ga:source'}">
                                Источник
                            </c:when>
                            <c:when test="${gaTotals.key == 'ga:campaign'}">
                                Email ID
                            </c:when>
                            <c:when test="${gaTotals.key == 'ga:landingPagePath'}">
                                Целевая страница
                            </c:when>
                            <c:when test="${gaTotals.key == 'ga:sessions'}">
                                Сессии
                            </c:when>
                            <c:when test="${gaTotals.key == 'ga:transactions'}">
                                Транзакции
                            </c:when>
                            <c:otherwise>
                                ${gaTotals.key}
                            </c:otherwise>
                        </c:choose>
                    </th>
                </c:forEach>
                </thead>
                <tbody>
                <c:forEach items="${gaData.totalsForAllResults}" var="gaTotals">
                    <th class="column1">${gaTotals.value}</th>
                </c:forEach>
                </tbody>
            </table>
        </c:if>
        </div>
    </div>

    <div class="row">
        <div class="stat-chart stats span9"></div>
	    <div class="stat-chart-MAIL_RU stats span9"></div>
	    <div class="stat-chart-BK_RU stats span9"></div>
	    <div class="stat-chart-LIST_RU stats span9"></div>
	    <div class="stat-chart-INBOX_RU stats span9"></div>
	    <div class="stat-chart-YANDEX_RU stats span9"></div>
	    <div class="stat-chart-GMAIL_COM stats span9"></div>
	    <div class="stat-chart-YAHOO_COM stats span9"></div>
	    <div class="stat-chart-HOTMAIL_COM stats span9"></div>
	    <div class="stat-chart-LIVE_RU stats span9"></div>
    </div>
</div>
<script type="text/javascript">

	(function () {

		var now = new Date();
		$('#dateFrom').datepicker();
		$('#dateTo').datepicker();

		<c:if test="${fromDate != null}">
			$('#dateFrom').datepicker('setDate', ${fromDate});
		</c:if>
		<c:if test="${toDate != null}">
			$('#dateTo').datepicker('setDate', ${toDate});
		</c:if>

		$('#dateFrom').change(function () {
			var time = $(this).datepicker('getDate') == null ? null : $(this).datepicker('getDate').getTime();
			$('input:hidden[name="dateFrom"]').val(time);
		});
		$('#dateTo').change(function () {
			var time = $(this).datepicker('getDate') == null ? null : $(this).datepicker('getDate').getTime();
			$('input:hidden[name="dateTo"]').val(time);
		});

		$('.date-filter-btn').click(function(e) {
			e.preventDefault();

			var dateFrom = new Date();
			var dateTo = new Date();
			switch($(e.target).val()) {
				case 'week':
					dateFrom.setDate(now.getDate() - 6);
					dateTo.setDate(now.getDate());
					break;
				case 'month':
					dateFrom.setDate(now.getDate() - 29);
					dateTo.setDate(now.getDate());
					break;
				default:
					dateFrom = null;
					dateTo = null;
					break;
			}

			$('#dateFrom').datepicker('setDate', dateFrom);
			$('#dateTo').datepicker('setDate', dateTo);
			$('input:hidden[name="dateFrom"]').val(dateFrom == null ? null : dateFrom.getTime());
			$('input:hidden[name="dateTo"]').val(dateTo == null ? null : dateTo.getTime());
		})

	})();

    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(function() {

	    var statsDataRows = {
		    title: 'По всем рассылкам',
		    el: '.stat-chart',
		    rows: [
			    ['Отправлено', ${stats.sent div stats.addresses}, ${stats.sent}],
			    ['Открыто', ${stats.opened div stats.sent}, ${stats.opened}],
			    ['Не открыто', ${stats.unopened div stats.sent}, ${stats.unopened}],
			    ['Не доставлено', ${stats.failed div stats.sent}, ${stats.failed}],
			    ['Отписалось', ${stats.unsubscribed div stats.sent}, ${stats.unsubscribed}],
			    ['Жалобы', ${stats.abuse div stats.sent}, ${stats.abuse}]
		    ]
	    };

	    ChartUtils.addChart(statsDataRows);

	    <c:forEach var="entry" items="${domainStats}">
		    var statsDataRows${entry.key} = {
			    title: '${entry.key}',
			    el: '.stat-chart-${entry.key}',
			    rows: [
				    ['Отправлено', ${entry.value.sent div stats.sent}, ${entry.value.sent}],
				    ['Открыто', ${entry.value.sent > 0 ? (entry.value.opened div entry.value.sent) : 0}, ${entry.value.opened}],
				    ['Не открыто', ${entry.value.sent > 0 ? (entry.value.unopened div entry.value.sent) : 0}, ${entry.value.unopened}],
				    ['Не доставлено', ${entry.value.sent > 0 ? (entry.value.failed div entry.value.sent) : 0}, ${entry.value.failed}],
				    ['Отписалось', ${entry.value.sent > 0 ? (entry.value.unsubscribed div entry.value.sent) : 0}, ${entry.value.unsubscribed}],
				    ['Жалобы', ${entry.value.sent > 0 ? (entry.value.abuse div entry.value.sent) : 0}, ${entry.value.abuse}]
			    ]
		    };

		    ChartUtils.addChart(statsDataRows${entry.key});
	    </c:forEach>
    });

</script>
<jsp:include page="/WEB-INF/includes/footer.jsp" />
</body>
</html>
<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/includes/taglibs.jsp" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/includes.jsp"%>
    <title>Списки рассылок</title>
</head>
<body>
<jsp:include page="/WEB-INF/includes/header.jsp" />

<div class="container">
    <h3>Списки рассылки</h3>
    <div class="btn-toolbar">
        <a class="btn btn-success" href="/list"><i class="icon-plus icon-white"></i> Создать</a>
	    <br/>
	    <br/>
	    <form class="form-horizontal" action="/createFromCSV.html" method="POST" enctype="multipart/form-data">
		    <input name="file" type="file"/>
		    <input type="submit" value="Загрузить" class="btn btn-success"/>
	    </form>
    </div>
    <table class="table table-striped">
        <thead>
        <tr>
            <th class="span1">№</th>
            <th>Название списка</th>
            <th>Действия</th>
        </tr>
        </thead>
        <c:forEach var="list" items="${lists}" varStatus="listStatus">
            <tr>
                <td>${listStatus.index + 1}</td>
                <td><a href="/list/${list.filterListId}">${list.name}</a></td>
                <td><a href="/list/${list.filterListId}/delete">Удалить</a></td>
            </tr>
        </c:forEach>
    </table>
</div>

<jsp:include page="/WEB-INF/includes/footer.jsp" />
</body>
</html>
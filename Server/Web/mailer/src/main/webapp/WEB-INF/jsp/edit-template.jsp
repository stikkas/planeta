<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/includes/taglibs.jsp" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/includes.jsp"%>
    <title>Шаблон</title>
</head>
<body>
<jsp:include page="/WEB-INF/includes/header.jsp" />

<div class="container">
    <h3>
        <c:choose>
            <c:when test="${template.templateId != null}">
                Редактирование шаблона
            </c:when>
            <c:otherwise>
                Создание шаблона
            </c:otherwise>
        </c:choose>
    </h3>
    <div class="btn-toolbar"></div>
    <form:form modelAttribute="template" style="height: 300px;" class="form-horizontal">
        <form:input path="templateId" type="hidden" />
        <fieldset>
            <div class="control-group<form:errors path="name"> error</form:errors>">
                <label class="control-label">Название шаблона</label>
                <div class="controls">
                    <form:input path="name" type="text" />
                    <form:errors path="name"><span class="help-inline"><form:errors path="name" /></span></form:errors>
                </div>
            </div>

                <form:textarea path="text" name="text" id="text" rows="10" cols="80"></form:textarea>
                    <script>
                        CKEDITOR.replace( 'text', {
                            language: 'ru'
                        } );
                        CKEDITOR.config.extraPlugins = 'name,unsubscribe,unsubscribeLink,email';
                    </script>
                <form:errors path="text"><span class="help-inline" style="color:#b94a48"><form:errors path="text" /></span></form:errors>
            <div class="form-actions">
                <button type="submit" class="btn btn-primary">Сохранить</button>
                <button type="reset" class="btn">Сбросить</button>
                <a <c:choose>
                    <c:when test="${template.templateId != null}">href="/view-message/template/${template.templateId}" class="btn pull-right"</c:when>
                    <c:otherwise>class="disabled btn pull-right"</c:otherwise>
                </c:choose> >Просмотр</a>
            </div>
        </fieldset>
    </form:form>
</div>
<jsp:include page="/WEB-INF/includes/footer.jsp" />
</body>
</html>
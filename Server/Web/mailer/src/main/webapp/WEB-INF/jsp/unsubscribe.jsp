<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/includes/taglibs.jsp" %>
<html>
<head>
    <style>
        <%@include file="/css/bootstrap.css" %>
        <%@include file="/css/mailer.css" %>
    </style>
    <title>Рассылки Планеты</title>
</head>
<body>
<div class="navbar container">
    <div class="navbar-inner">
        <div class="container">
            <a class="brand">Рассылки Планеты</a>
        </div>
    </div>
</div>
<form class="form-horizontal" method="post" action="#">
    <div class="modal modal-fixed" id="deleteModal">
        <div class="modal-header">
            <div class="modal-title">Отписаться от рассылок:</div>
        </div>
        <div class="modal-body">
            Вы действительно хотите отписаться от рассылок &laquo;Планеты.ру&raquo;?
        </div>
        <div class="modal-footer">
            <div class="modal-footer-cont">
                <a href="${fallbackUrl}" class="btn" data-dismiss="modal">Отмена</a>
                <button type="submit" name="save" class="btn btn-primary">Отписаться</button>
            </div>
        </div>
    </div>
</form>
</body>
</html>

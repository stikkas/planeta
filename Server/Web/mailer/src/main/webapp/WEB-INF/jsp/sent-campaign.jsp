<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/includes/taglibs.jsp" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/includes.jsp"%>
    <title>Рассылка завершена</title>
</head>
<body>
<jsp:include page="/WEB-INF/includes/header.jsp" />

<div class="container">
    <h3>
        Рассылка успешно завершена
    </h3>
    <div class="btn-toolbar"></div>

    <p>Вы можете
        <a href="/campaign/${campaign.campaignId}/view">посмотреть данные о рассылке</a>
        или
        <a href="/campaign/${campaign.campaignId}/stats">её статистику</a>.
    </p>

    Спасибо за работу с нашим сервисом рассылок.
</div>

<jsp:include page="/WEB-INF/includes/footer.jsp" />
</body>
</html>
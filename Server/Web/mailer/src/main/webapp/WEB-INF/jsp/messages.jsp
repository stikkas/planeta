<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@include file="/WEB-INF/includes/taglibs.jsp" %>
<table class="table table-striped">
    <thead>
    <tr>
        <th>№</th>
        <c:if test="${!hideUser}">
            <th>Кому</th>
        </c:if>
        <c:if test="${!hideCampaign}">
            <th>Название рассылки</th>
        </c:if>
        <th>Открыто</th>
        <th>Не доставлено</th>
        <th>Отказ от рассылки</th>
        <th>Жалоба</th>
    </tr>
    </thead>
    <c:forEach var="message" items="${messages}" varStatus="messageStatus">
        <tr>
            <td>${messageStatus.index + offset + 1}</td>
            <c:if test="${!hideUser}">
                <td><a href="/user/${message.user.userId}"><c:out value="${message.user.displayName} <${message.user.email}>"/></a></td>
            </c:if>
            <c:if test="${!hideCampaign}">
                <c:choose>
                    <c:when test="${message.campaign != null}">
                        <td><a href="/campaign/${message.campaign.campaignId}/view"><c:out value="${message.campaign.name}"/></a></td>
                    </c:when>
                    <c:otherwise>
                        <td>Удалена</td>
                    </c:otherwise>
                </c:choose>
            </c:if>

            <td><c:choose><c:when test="${message.mailerMessage.open}"><i class="icon-ok"></i></c:when><c:otherwise>-</c:otherwise></c:choose></td>
            <td><c:choose><c:when test="${message.mailerMessage.failed}"><i class="icon-remove"></i></c:when><c:otherwise>-</c:otherwise></c:choose></td>
            <td><c:choose><c:when test="${message.mailerMessage.unsubscribed}"><i class="icon-remove"></i></c:when><c:otherwise>-</c:otherwise></c:choose></td>
            <td><c:choose><c:when test="${message.mailerMessage.abuse}"><i class="icon-exclamation-sign"></i></c:when><c:otherwise>-</c:otherwise></c:choose></td>
        </tr>
    </c:forEach>
</table>
<my:pagination offset="${offset}" limit="${limit}" count="${messageCount}" continued="${messagesContinued}"/>

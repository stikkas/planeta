<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/includes/taglibs.jsp" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/includes.jsp"%>
    <title>Рассылки Планеты</title>
</head>
<body>
<jsp:include page="/WEB-INF/includes/header.jsp" />

<div class="container">
    <h3>Новая рассылка</h3>
    <div class="btn-toolbar">
        <a href="/campaign" class="btn btn-primary btn-large">Создать новую рассылку</a>
    </div>
    <hr/>
    <h3>Предыдущие рассылки</h3>
    <p>Вы можете посмотреть уже законченные рассылки или отредактировать черновик.</p>

    <div class="row ">
        <div class="span12">
            <%@ include file="/WEB-INF/includes/searchBoxCampaigns.jsp" %>
        </div>
    </div>

    <table class="table table-striped">
        <thead>
        <tr>
            <th class="span1">№</th>
            <th>Название рассылки</th>
            <th>Подверждена</th>
            <th>Действия</th>
        </tr>
        </thead>
        <c:forEach var="campaign" items="${campaigns}" varStatus="campaignStatus">
            <tr>
                <td>${campaign.campaignId}</td>
                <td><a href="/campaign/${campaign.campaignId}"><c:out value="${campaign.name}"/></a>
	                <c:set var="state" value="${campaignStates[campaign.campaignId]}" />
			        <c:if test="${state != 'NOT_RUNNING'}">
				        <c:choose>
					        <c:when test="${state == 'FINISHED'}">
						        <sup><span class="label label-mini">завершена</span></sup>
					        </c:when>
					        <c:when test="${state == 'FAILED'}">
						        <sup><span class="label label-mini">ошибка</span></sup>
					        </c:when>
					        <c:when test="${state == 'IN_QUEUE'}">
						        <sup><span class="label label-mini">в очереди</span></sup>
					        </c:when>
					        <c:when test="${state == 'RUNNING'}">
						        <sup><span class="label label-mini">отправляется</span></sup>
					        </c:when>
					        <c:when test="${state == 'PAUSED'}">
						        <sup><span class="label label-mini">остановлена</span></sup>
					        </c:when>
				        </c:choose>
			        </c:if>
                    <c:if test="${campaign.dateConfirmed == null}">
                        <sup><span class="label label-mini">черновик</span></sup>
                    </c:if>
                    <c:if test="${campaign.timeToSend != null}">
                        <sup>
                            <span class="label label-mini label-warning">отложенный запуск
                                <fmt:formatDate value="${campaign.timeToSend}" pattern="dd.MM.yyyy HH:mm" />
                            </span>
                        </sup>
                    </c:if>
                </td>
                <td>${campaign.dateConfirmed}</td>
                <td>
                    <c:choose>
                        <c:when test="${campaign.dateConfirmed != null}">
                            <a href="/campaign/${campaign.campaignId}/stats">Статистика</a>
                        </c:when>
                        <c:otherwise>
                            <a href="/campaign/${campaign.campaignId}/delete">Удалить</a>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
        </c:forEach>
    </table>
    <%@ include file="/WEB-INF/includes/paginator.jsp" %>
</div>

<jsp:include page="/WEB-INF/includes/footer.jsp" />
</body>
</html>
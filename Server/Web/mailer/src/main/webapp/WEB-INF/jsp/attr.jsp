<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/includes/taglibs.jsp" %>

<select name="filters[${index}].filteredAttribute.attributeId" value="${filter.filteredAttribute.attributeId}" onchange="Mailer.fetchFilterOptions(this.value, ${index}, this)">
    <option value="">Выберите атрибут</option>
    <c:forEach var="attribute" items="${attributes}">
        <option value="${attribute.attributeId}" <c:if test="${filter.filteredAttribute.attributeId eq attribute.attributeId}">selected="true"</c:if>>${attribute.textName}</option>
    </c:forEach>
</select>
<select class="input-medium" name="filters[${index}].predicate" value="${filter.predicate}">
    <c:forEach var="predicate" items="${mf:getPredicateByAttributeType(filter.filteredAttribute.type)}">
        <option value="${predicate}" <c:if test="${filter.predicate eq predicate}">selected="true"</c:if>>${mf:getMessage(predicate.text, filter.filteredAttribute.type)}</option>
    </c:forEach>
</select>
<c:choose>
    <c:when test="${filter.filteredAttribute.type == 'MULTI' || filter.filteredAttribute.type == 'ENUM'}">
        <%--<select class="span5" name="filters[${index}].object">--%>
            <%--<c:forEach var="enumValue" items="${mf:getEnumValues(filter.filteredAttribute)}">--%>
                <%--<option value="${enumValue.value}"<c:if test="${enumValue.value eq filter.object}">selected="true"</c:if>>${enumValue.textValue} </option>--%>
            <%--</c:forEach>--%>
        <%--</select>--%>
        <select class="span5" name="filters[${index}].object" id="filter${index}">
        </select>
        <script>
            $('#filter${index}').select2({
                placeholder: 'Начните печатать',
                ajax: {
                    url: "/get-enum-list.json",
                    dataType: 'json',
                    delay: 250,
                    data: function (params, page) {
                        return {
                            offset: (page || 0) * 20,
                            limit: 20,
                            attributeId: ${filter.filteredAttribute.attributeId},
                            type: "${filter.filteredAttribute.type}",
                            query: params.term
                        };
                    },
                    processResults: function (response) {
                        var data = [];
                        _.each(response, function (item) {
                            data.push({id: item.value, text: item.textValue});
                        });
                        return {
                            results: data
                        };
                    },
                    cache: true
                }
            });
        </script>
    </c:when>
    <c:when test="${filter.filteredAttribute.type == 'DATE'}">
        <input class="span5" type="text" name="filters[${index}].object" value="${filter.object}" class="datepicker" />
    </c:when>
    <c:otherwise>
        <input class="span5" type="text" name="filters[${index}].object" value="${filter.object}" />
    </c:otherwise>
</c:choose>
<a href="javascript:void(0)" onclick="Mailer.hideAndRemove($(this).closest('div.control-group'))"><i class="icon-remove"></i></a>
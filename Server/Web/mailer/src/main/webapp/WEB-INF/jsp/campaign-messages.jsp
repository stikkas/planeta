<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/includes/taglibs.jsp" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/includes.jsp"%>
    <title>Просмотр списка отправленных</title>
</head>
<body>
<jsp:include page="/WEB-INF/includes/header.jsp" />

<div class="container">
    <a href="/campaign/${campaign.campaignId}/stats">&larr; Вернуться к общей статистике</a>
    <h3>Список отправленных писем</h3>
    <form method="get" class="form-horizontal">
        <div class="btn-toolbar">
            <input type="text" name="query" value="${searchString}" class="span8"/>
            <button type="submit" class="btn btn-primary">Поиск</button>
        </div>
        <span class="search-toggle btn<c:if test="${opts}"> hide</c:if>" onclick="showOptions();">Дополнительные параметры</span>
        <div class="row">
            <div class="search-options span8<c:if test="${!opts}"> hide</c:if>">
                <div class="thumbnail">
                    <p><a class="close" href="#" onclick="hideOptions(); return false;">&times;</a></p>
                    <div class="row">
                        <div class="span7">
                            <div class="control-group">
                                <div class="control-label">Сообщение открыто</div>
                                <div class="controls">
                                    <select name="open">
                                        <option<c:if test="${open == null}"> selected</c:if> value="">Не важно</option>
                                        <option<c:if test="${open == true}"> selected</c:if> value="true">Да</option>
                                        <option<c:if test="${open == false}"> selected</c:if> value="false">Нет</option>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="control-label">Не доставлено</div>
                                <div class="controls">
                                    <select name="failed">
                                        <option<c:if test="${failed == null}"> selected</c:if> value="">Не важно</option>
                                        <option<c:if test="${failed == true}"> selected</c:if> value="true">Да</option>
                                        <option<c:if test="${failed == false}"> selected</c:if> value="false">Нет</option>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="control-label">Отказ от рассылки</div>
                                <div class="controls">
                                    <select name="unsubscribed">
                                        <option<c:if test="${unsubscribed == null}"> selected</c:if> value="">Не важно</option>
                                        <option<c:if test="${unsubscribed == true}"> selected</c:if> value="true">Да</option>
                                        <option<c:if test="${unsubscribed == false}"> selected</c:if> value="false">Нет</option>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="control-label">Получена жалоба</div>
                                <div class="controls">
                                    <select name="abuse">
                                        <option<c:if test="${abuse == null}"> selected</c:if> value="">Не важно</option>
                                        <option<c:if test="${abuse == true}"> selected</c:if> value="true">Да</option>
                                        <option<c:if test="${abuse == false}"> selected</c:if> value="false">Нет</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <c:set var="hideCampaign" value="true" scope="request"/>
    <jsp:include page="messages.jsp"/>
</div>

<script>
    $(function() {
        $('select[name=open],select[name=failed],select[name=unsubscribed],select[name=abuse]').btnSelect();
    });

    function showOptions() {
        $('.search-toggle').hide();
        $('.search-options').slideDown('fast');
        $('.search-options').find('select').removeAttr('disabled');
    };

    function hideOptions() {
        $('.search-options').slideUp('fast', function(){
            $('.search-toggle').show();
            $('.search-options').find('select').attr('disabled', 'disabled');
        });
    }
</script>


<jsp:include page="/WEB-INF/includes/footer.jsp" />
</body>
</html>
<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/includes/taglibs.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/includes.jsp"%>
    <title>Рассылка</title>
</head>
<body>
<jsp:include page="/WEB-INF/includes/header.jsp" />

<div class="container">
    <p:if test="${isDeferred}">
        <div class="modal modal-fixed" id="deleteModal">
            <div class="modal-header">
                <div class="modal-title">Рассылка запланирована на <fmt:formatDate value="${campaign.timeToSend}" pattern="dd.MM.yyyy HH:mm" /></div>
            </div>
            <div class="modal-body">
                Рассылка будет отправлена <fmt:formatDate value="${campaign.timeToSend}" pattern="dd.MM.yyyy HH:mm" />.
                Если вы хотите запустить рассылку прямо сейчас, удалите дату в <a href="/campaign/${campaign.campaignId}">редакторе рассылки</a>.
            </div>
        </div>
    </p:if>

    <p:if test="${isAlreadySent}">
        <form class="form-horizontal" action="/campaign/${campaign.campaignId}/confirm" method="post">
            <div class="modal modal-fixed" id="deleteModal">
                <div class="modal-header">
                    <div class="modal-title">Рассылка уже завершена</div>
                </div>
                <div class="modal-body">
                    Рассылка уже завершена, но может быть отправлена повторно. В этом случае письма будут доставлены только пользователям, которые ещё не получали эту рассылку.
                </div>
                <div class="modal-footer">
                    <div class="modal-footer-cont">
                        <a class="btn" href="javascript:history.back();">Отмена</a>
                        <button type="submit" name="force" value="true" class="btn btn-danger">Отправить</button>
                    </div>
                </div>
            </div>
        </form>
    </p:if>
    <h3>
        <c:choose>
            <c:when test="${isNeedConfirm}">
                Подтверждение отправки рассылки
            </c:when>
            <c:otherwise>
                Просмотр рассылки
            </c:otherwise>
        </c:choose>
    </h3>
    <div class="btn-toolbar"></div>
    <div>
        <table class="table table-striped">
            <tbody>
            <tr>
                <td class="span3">Название рассылки</td>
                <td>${campaign.name}</td>
            </tr>
            <tr>
                <td>Адресаты рассылки</td>
                <td>
                    <p>
                        <c:choose>
                            <c:when test="${messages != null}">
                                <c:set var="selectedCount" value="${fn:length(messages)}"/>
                                <c:forEach var="message" items="${messages}" varStatus="mStatus">
                                    <c:if test="${mStatus.index > 0}">, </c:if>
                                    <c:out value="${message.user.displayName} <${message.user.email}>" />
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                                <c:set var="selectedCount" value="${fn:length(addresses)}"/>
                                <c:forEach var="address" items="${addresses}" varStatus="aStatus">
                                    <c:if test="${aStatus.index > 0}">, </c:if>
                                    <c:out value='${not empty address.displayName ? address.displayName : "Загружено из фаила"} <${address.email}>' />
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                        <c:if test="${selectedCount < count}"> и ещё ${count - selectedCount} адресатов.</c:if>
                    </p>
                    <c:if test="${isAllRecipients}">
                        <div class="alert alert-danger">
                            <strong>Внимание!</strong> Вы отправляете рассылку всем пользователям.
                        </div>
                    </c:if>
                </td>
            </tr>
            <c:if test="${state != 'NOT_RUNNING'}">
                <tr>
                    <td>Статус рассылки</td>
                    <td>
                        <c:choose>
                            <c:when test="${state == 'FINISHED'}">Завершена</c:when>
                            <c:when test="${state == 'FAILED'}">Ошибка</c:when>
                            <c:when test="${state == 'IN_QUEUE'}">В очереди отправки</c:when>
                            <c:when test="${state == 'RUNNING'}">
                                <form class="form-horizontal">
                                    Отправляется
                                </form>
                            </c:when>
                            <c:when test="${state == 'PAUSED'}">Остановлена</c:when>
                        </c:choose>
                    </td>
                </tr>
            </c:if>
            <c:if test="${campaign.timeToSend != null}">
                <tr>
                    <td>Отложенный запуск</td>
                    <td>${campaign.timeToSend}</td>
                </tr>
            </c:if>
            <c:if test="${campaign.dateConfirmed != null}">
                <tr>
                    <td>Отправлена</td>
                    <td>${campaign.dateConfirmed}</td>
                </tr>
            </c:if>
            </tbody>
        </table>
        <c:if test="${campaign.sendToAll}">
            <p style="color: #aa0000;font-size: 16px;">Рассылка для ВСЕХ (даже отписаных) юзеров!</p>
        </c:if>
        <p>Рассылаемое письмо выглядит так:</p>
        <div class="breadcrumb msg-header">
            <div class="btn-toolbar">
                <div class="row">
                    <div class="span2"><b>Отправитель:</b></div>
                    <div class="span8"><c:out value="${campaign.messageFrom}"/></div>
                </div>
            </div>
            <div class="btn-toolbar">
                <div class="row">
                    <div class="span2"><b>Тема сообщения:</b></div>
                    <div class="span8"><c:out value="${campaign.subject}"/></div>
                </div>
            </div>
        </div>
        <div class="msg">
            <div class="msg-modal hide">&nbsp;</div>
            <iframe src="/view-message/campaign/${campaign.campaignId}" class="msg-iframe" frameborder="0"></iframe>
            <div class="msg-resizer redactor_resizer">&mdash;</div>
        </div>
	    <div class="form-actions btn-toolbar">
		    <form action="/campaign/${campaign.campaignId}/confirm" method="post">
			    <input type="hidden" name="campaignId" value="${campaign.campaignId}"/>
			    <c:choose>
				    <c:when test="${isNeedConfirm}">
                        <button type="submit" name="confirmed" class="btn btn-primary btn-large">Подтвердить и отправить <fmt:formatDate value="${campaign.timeToSend}" pattern="dd.MM.yyyy HH:mm" />
                        </button>
                        <span class="big-font">или</span>
                        <a href="/campaign/${campaign.campaignId}" class="btn btn-primary">Внести изменения</a>
				    </c:when>
				    <c:otherwise>
					    <a href="/campaign/${campaign.campaignId}/stats" class="btn btn-primary">Перейти к
						    статистике</a>
					    <a href="/campaign/${campaign.campaignId}?force=true" class="btn">Редактировать</a>
					    <c:if test="${state != 'RUNNING' and state != 'IN_QUEUE'}">
						    <button type="submit" name="confirmed" class="btn">Отправить ещё раз</button>
					    </c:if>
				    </c:otherwise>
			    </c:choose>
			    <a href="/view-message/campaign/${campaign.campaignId}" class="btn pull-right">Просмотр письма</a>

		    </form>
            <c:if test="${state == 'RUNNING' || state == 'IN_QUEUE'}">
		    <form action="/campaign/${campaign.campaignId}/stop" method="post" style="display:inline;">
			    <button type="submit" name="stop" class="btn"
			            onclick="return confirm('Вы действительно хотите остановить рассылку?')">
				    Остановить
			    </button>
		    </form>
            </c:if>
	    </div>

    </div>
</div>

<script type="text/javascript">
    (function(){
        var resizer = $(".msg-resizer");
        var msg = $(resizer).closest(".msg");
        var msgModal = msg.find(".msg-modal");
        var iframe = msg.find("iframe");
        $(resizer).mousedown(function(event) {
            event.preventDefault();
            var y = event.pageY;
            var height = iframe.height();
            var move = _.throttle(function(event) {
                $(iframe).height(height + event.pageY - y);
                $(msgModal).height(height + event.pageY - y);
            }, 40);
            var up = function(event) {
                $(msgModal).hide();
                $(document).unbind('mousemove', move);
                $(document).unbind('mouseup', up);
            }
            $(msgModal).show();
            $(document).mousemove(move);
            $(document).mouseup(up);
        })
    })();
</script>

<jsp:include page="/WEB-INF/includes/footer.jsp" />
</body>
</html>
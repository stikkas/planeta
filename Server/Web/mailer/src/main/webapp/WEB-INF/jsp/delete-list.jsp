<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/includes/taglibs.jsp" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/includes.jsp"%>
    <title>Удаление списка рассылки</title>
</head>
<body>
<jsp:include page="/WEB-INF/includes/header.jsp" />

<form class="form-horizontal" method="post" action="/list/${filterList.filterListId}/delete">
    <div class="modal modal-fixed" id="deleteModal">
        <div class="modal-header">
            <div class="modal-title">Удаление списка рассылки</div>
        </div>
        <div class="modal-body">
            Вы действительно хотите удалить список рассылки &laquo;${filterList.name}&raquo;?
        </div>
        <div class="modal-footer">
            <div class="modal-footer-cont">
                <a href="${header.referer}#" class="btn" data-dismiss="modal" <c:if test="${header.referer == null}">onclick="history.back(); event.preventDefault();"</c:if>>Отмена</a>
                <button type="submit" name="save" class="btn btn-primary">Удалить</button>
            </div>
        </div>
    </div>
</form>

<jsp:include page="/WEB-INF/includes/footer.jsp" />
</body>
</html>

<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/includes/taglibs.jsp" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/includes.jsp"%>
    <title>Рассылки Планеты</title>
    <script>
        function addSpaces(n){
            var rx=  /(\d+)(\d{3})/;
            return String(n).replace(/^\d+/, function(w){
                while(rx.test(w)){
                    w= w.replace(rx, '$1 $2');
                }
                return w;
            });
        }
    </script>
</head>
<body>
<jsp:include page="/WEB-INF/includes/header.jsp" />

<div class="container">
    <h3>Пользователи</h3>
    <p>Вы можете посмотреть список пользователей. А также подписать или отписать пользователя от рассылки.</p>

    <div class="row ">
        <div class="span12">
            <%@ include file="/WEB-INF/includes/searchBoxUsers.jsp" %>
        </div>
    </div>

    <div class="row">
        <div class="span12">
            Найдено: <span id="usersCount" class="badge">${count}</span>
        </div>
        <script>
            $("#usersCount").each(function () {
                var $this = $(this);
                $this.text(addSpaces($this.text()));
            });
        </script>
    </div>
    <br>
    <div class="row">
        <div class="span12">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th class="span1">№</th>
                    <th>Email</th>
                    <th>Статус</th>
                    <th>Статистика</th>
                </tr>
                </thead>
                <c:forEach var="user" items="${users}">
                    <tr>
                        <td>${user.userId}</td>
                        <td>${user.email}</td>
                        <td>
                            <c:if test="${!user.unsubscribed}">
                                <div class="btn-group">
                                    <a class="btn btn-success dropdown-toggle" data-toggle="dropdown" href="#">
                                        Подписан
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                        <form method="POST" action="/user-change-subscribe/${user.userId}" class="clearfix">
                                            <input type="hidden" name="offset" value="${offset}" />
                                            <input type="hidden" name="limit" value="${limit}" />
                                            <input type="hidden" name="searchString" value="${searchString}" />
                                            <input type="hidden" name="unsubscribed" value="true" />
                                            <button class="btn btn-primary btn-danger btn-block" type="submit" style="width: 100%;">
                                                Отписать
                                            </button>
                                        </form>
                                        </li>
                                    </ul>
                                </div>
                            </c:if>
                            <c:if test="${user.unsubscribed}">
                                <div class="btn-group">
                                    <a class="btn btn-danger dropdown-toggle" data-toggle="dropdown" href="#">
                                        Отписан
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <form method="POST" action="/user-change-subscribe/${user.userId}" class="clearfix">
                                                <input type="hidden" name="offset" value="${offset}" />
                                                <input type="hidden" name="limit" value="${limit}" />
                                                <input type="hidden" name="searchString" value="${searchString}" />
                                                <input type="hidden" name="unsubscribed" value="false" />
                                                <button class="btn btn-primary btn-success btn-block" type="submit" style="width: 100%;">
                                                    Подписать
                                                </button>
                                            </form>
                                        </li>
                                    </ul>
                                </div>
                            </c:if>
                        </td>
                        <td>
                            <a href="/user/${user.userId}" target="_blank">Посмотреть</a>
                        </td>
                    </tr>
                </c:forEach>
            </table>
            <%@ include file="/WEB-INF/includes/paginator.jsp" %>
        </div>
    </div>
</div>

<jsp:include page="/WEB-INF/includes/footer.jsp" />
</body>
</html>
<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/includes/taglibs.jsp" %>
<html>
<head>
    <style>
        <%@include file="/css/bootstrap.css" %>
        <%@include file="/css/mailer.css" %>
    </style>
    <title>Рассылки Планеты</title>
</head>
<body>
<div class="navbar container">
    <div class="navbar-inner">
        <div class="container">
            <a class="brand">Рассылки Планеты</a>
        </div>
    </div>
</div>
<div class="container">
    <h3>
        Вы отписались от рассылок &laquo;Планеты.ру&raquo;
    </h3>
    <div class="btn-toolbar"></div>

    Спасибо за работу с нашим сервисом рассылок.
</div>
</body>
</html>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/includes/taglibs.jsp" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/includes.jsp"%>

    <title>Рассылка</title>
    <script>
        $(function () {
            $("#timeToSend").datetimepicker();
            if (${campaign.timeToSend != null}) {
                $("#timeToSend").datetimepicker('setDate', new Date(${campaign.timeToSend.time}));
            }
        });
    </script>
</head>
<body>
<jsp:include page="/WEB-INF/includes/header.jsp" />

<div class="container">
    <h3>
        <c:choose>
            <c:when test="${campaign.campaignId != null}">
                Редактирование рассылки
            </c:when>
            <c:otherwise>
                Создание рассылки
            </c:otherwise>
        </c:choose>
    </h3>
    <div class="btn-toolbar"></div>
    <div>
        <form:form id="campaign-form" modelAttribute="campaign" style="height: 300px;" class="form-horizontal">
            <form:input path="campaignId" type="hidden" />
            <fieldset>
                <div class="control-group<form:errors path="name"> error</form:errors>">
                    <label class="control-label">Название рассылки</label>
                    <div class="controls">
                        <form:input path="name" type="text" />
                        <form:errors path="name"><span class="help-inline"><form:errors path="name" /></span></form:errors>
                    </div>
                </div>
                <div class="control-group<form:errors path="messageFrom"> error</form:errors>">
                    <label class="control-label">Отправитель</label>
                    <div class="controls">
                        <form:select path="messageFrom" class="span5">
                            <c:forEach var="fromName" items="${fromNames}">
                                <form:option value="${fromName}"><c:out value="${fromName}" /></form:option>
                            </c:forEach>
                        </form:select>
                        <form:errors path="messageFrom"><span class="help-inline"><form:errors path="messageFrom" /></span></form:errors>
                    </div>
                </div>
                <div class="control-group<form:errors path="subject"> error</form:errors>">
                    <label class="control-label">Тема</label>
                    <div class="controls">
                        <form:input path="subject" type="text" class="span10"/>
                        <form:errors path="subject"><span class="help-inline"><form:errors path="subject" /></span></form:errors>
                    </div>
                </div>
                <div class="control-group<form:errors path="filterListId"> error</form:errors>">
                    <label class="control-label">Список рассылки</label>
                    <div class="controls">

                        <select id="filterListId" name="filterListId" class="span5">
                            <c:if test="${not empty selectedFilterList}">
                                <option value="${selectedFilterList.filterListId}" selected="selected">${selectedFilterList.name}</option>
                            </c:if>
                        </select>
                        <form:errors path="filterListId"><span class="help-inline"><form:errors path="filterListId" /></span></form:errors>
                    </div>
                </div>
                <div class="control-group<form:errors path="timeToSend"> error</form:errors>">
                <label class="control-label">Дата рассылки</label>
                    <div class="controls">
                        <fmt:formatDate value="${campaign.timeToSend}" var="timeToSendString" pattern="dd.MM.yyyy HH:mm" />
                        <form:input id="timeToSend" path="timeToSend" type="text" value="${timeToSendString}" />
                        <form:errors path="timeToSend" cssClass="error"/>
                    </div>
                    <div class="controls">
                        <span>в формате 20.02.2015 14:56 (оставьте поле пустым, если не нужен отложенный запуск)</span>
                    </div>
                </div>

                <div class="control-group<form:errors path="templateId"> error</form:errors>">
                    <label class="control-label">Шаблон</label>
                    <div class="controls">
                        <select id="templateId" name="templateId" class="span5" >
                        <c:if test="${not empty selectedTemplate}">
                            <option value="${selectedTemplate.templateId}" selected="selected">${selectedTemplate.name}</option>
                        </c:if>
                        </select>
                        <a class="btn btn-success" onclick="Mailer.fetchTemplate();" href="javascript:void(0);">Взять за основу</a>
                        <form:errors path="templateId"><span class="help-inline"><form:errors path="templateId" /></span></form:errors>
                    </div>
                </div>
                <span class="control-group<form:errors path="text"> error</form:errors>">
                    <form:textarea path="text" style="height: 300px" class="span12"/>
                    <form:errors path="text"><span class="help-inline"><form:errors path="text" /></span></form:errors>
                </span>

                <div class="checkbox">
                    <label>
                        <form:checkbox path="sendToAll" id="sendToAll" name="sendToAll"/> Отправить рассылку даже отписанным пользователям
                    </label>
                </div>

                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                    <button type="submit" class="btn btn-primary" name="send">Отправить</button>
                    <a <c:choose>
                        <c:when test="${campaign.campaignId != null}">href="/view-message/campaign/${campaign.campaignId}" class="btn pull-right"</c:when>
                        <c:otherwise>class="disabled btn pull-right"</c:otherwise>
                    </c:choose> >Просмотр</a>
                </div>
            </fieldset>
        </form:form>
    </div>
</div>

<jsp:include page="/WEB-INF/includes/footer.jsp" />

<script>
    $('#filterListId').select2({
        placeholder: 'Начните печатать',
        ajax: {
            url: "/get-filter-list.json",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    offset: (params.page || 0) * 20,
                    limit: 20,
                    query: params.term
                };
            },
            processResults: function (response) {
                var data = [];
                _.each(response, function (item) {
                    data.push({id: item.filterListId, text: item.name});
                });
                return {
                    results: data
                };
            },
            cache: true
        },
        minimumInputLength: 1

    });




    $('#templateId').select2({
        placeholder: 'Начните печатать',
        ajax: {
            url: "/get-template-list.json",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    offset: (params.page || 0) * 20,
                    limit: 20,
                    query: params.term
                };
            },
            processResults: function (response) {
                var data = [];
                _.each(response, function (item) {
                    data.push({id: item.templateId, text: item.name});
                });
                return {
                    results: data
                };
            },
            cache: true
        },
        minimumInputLength: 1

    });
</script>
</body>
</html>


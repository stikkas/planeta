<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/includes/taglibs.jsp" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/includes.jsp"%>
    <title>Сведения о пользователе</title>
</head>
<body>
<jsp:include page="/WEB-INF/includes/header.jsp" />

<div class="container">
    <h3>Просмотр сведений о пользователе</h3>
    <div class="btn-toolbar"></div>
    <div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Имя пользователя</td>
                <td><c:out value="${user.displayName}"/></td>
            </tr>
            <tr>
                <td>Электронная почта</td>
                <td>${user.email}</td>
            </tr>
            <tr>
                <td>Статус</td>
                <td>
                    <form id="changeStatus" class="form-inline" action="/user/${user.userId}" method="post">
                        <input type="hidden" name="unsubscribed" value="${! user.unsubscribed}"/>
                        <div class="btn-group">
                            <button class="btn dropdown-toggle" data-toggle="dropdown" type="submit">
                                <c:choose>
                                    <c:when test="${user.unsubscribed}">
                                        <span class="text-status error">Отписан</span>
                                    </c:when>
                                    <c:otherwise>
                                        <span class="text-status success">Подписан</span>
                                    </c:otherwise>
                                </c:choose>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="javascript:void(0);" onclick="$(changeStatus).submit();">
                                        <c:choose>
                                            <c:when test="${user.unsubscribed}">
                                                <span class="text-status success">Подписать</span>
                                            </c:when>
                                            <c:otherwise>
                                                <span class="text-status error">Отписать</span>
                                            </c:otherwise>
                                        </c:choose>
                                    </a>
                                </li>

                            </ul>
                        </div>
                    </form>
                </td>
            </tr>
            </tbody>
        </table>
        <h4>Список писем пользователя</h4>
        <div class="btn-toolbar">
            <form method="get" class="form-horizontal">
                <input type="text" name="query" value="${searchString}" class="span8"/>
                <button type="submit" class="btn">Поиск</button>
            </form>
        </div>
        <c:set var="hideUser" value="true" scope="request"/>
        <jsp:include page="messages.jsp"/>
    </div>
</div>

<jsp:include page="/WEB-INF/includes/footer.jsp" />
</body>
</html>
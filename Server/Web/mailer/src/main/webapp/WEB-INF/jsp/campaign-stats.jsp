<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/includes/taglibs.jsp" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/includes.jsp"%>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <title>Статистика рассылки</title>
    <script type="text/javascript">
        var getReport = function(url, reportType) {
            if (reportType) {
                var form = document.getElementById('getfile');
                try {
                    form.setAttribute('action', url +  '?campaignId=${campaign.campaignId}&reportType=' + reportType);
                    form.submit();
                } catch (ex) {
                    console.log(ex);
                }
            }
        };

        var onChoiceEmailsReport = function(reportType) {
            getReport('/campaign/file-of-active-clickers', reportType);
        };

        var onChoiceStatsReport = function(reportType) {
            getReport('/campaign/file-of-stats', reportType);
        };
    </script>
</head>
<body>
<jsp:include page="/WEB-INF/includes/header.jsp" />

<div class="container">
    <a href="/campaign/${campaign.campaignId}/view">&larr; Вернуться к просмотру рассылки</a>
    <h3>
        Статистика рассылки &laquo;<c:out value="${campaign.name}"/>&raquo;
    </h3>
    <div class="btn-toolbar">
    </div>
    <div class="row">
        <div class="column span3">
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <div class="well stats-caption">
                <div class="caption">
                    Количество адресатов рассылки:
                    <h1>${stats.addresses - stats.unsubscribed}</h1>
                </div>
            </div>
            <p><a href="/campaign/${campaign.campaignId}/view" class="btn btn-expand">Информация о рассылке</a></p>
            <p><a href="/campaign/${campaign.campaignId}/stats/messages" class="btn btn-expand">Отправленные сообщения</a></p>

            <div class="btn-group">
                <button class="btn btn-expand dropdown-toggle" data-toggle="dropdown" style="width: 100%;">
                    Скачать отчет
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="javascript:onChoiceStatsReport('EXCEL')" class="download-icon-item">Excel</a></li>
                    <li><a href="javascript:onChoiceStatsReport('CSV')" class="download-icon-item">CSV</a></li>
                </ul>
            </div>

        </div>
	    <div class="span9">
            <div style="margin: 30px 60px;">
                <c:if test="${usersWhoClickedLinksFromEmail > 0}">
                <div>
                    <div class="btn-group">
                        <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                            Скачать список активных email
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:onChoiceEmailsReport('EXCEL')" class="download-icon-item">Excel</a></li>
                            <li><a href="javascript:onChoiceEmailsReport('CSV')" class="download-icon-item">CSV</a></li>
                        </ul>
                    </div>
                </div>
                <br>
                <table class="table table-striped table-bordered table-condensed" style="width: auto">
                    <thead>
                    <c:forEach items="${gaData.totalsForAllResults}" var="gaTotals">
                        <th class="column1">
                        <c:choose>
                            <c:when test="${gaTotals.key == 'ga:source'}">
                                Источник
                            </c:when>
                            <c:when test="${gaTotals.key == 'ga:campaign'}">
                                Email ID
                            </c:when>
                            <c:when test="${gaTotals.key == 'ga:landingPagePath'}">
                                Целевая страница
                            </c:when>
                            <c:when test="${gaTotals.key == 'ga:sessions'}">
                                Сессии
                            </c:when>
                            <c:when test="${gaTotals.key == 'ga:transactions'}">
                                Транзакции
                            </c:when>
                            <c:otherwise>
                                ${gaTotals.key}
                            </c:otherwise>
                        </c:choose>
                        </th>
                    </c:forEach>
                    </thead>
                    <tbody>
                    <c:forEach items="${gaData.totalsForAllResults}" var="gaTotals">
                        <th class="column1">${gaTotals.value}</th>
                    </c:forEach>
                    </tbody>
                </table>
                <p><span class="label label-info">ТОП 10 ссылок по количеству транзакций</span></p>
                <table class="table table-striped table-bordered table-condensed" style="width: auto">
                    <thead>
                    <c:forEach items="${gaData.columnHeaders}" var="gaHeader">
                        <th class="column1">
                            <c:choose>
                                <c:when test="${gaHeader.name == 'ga:source'}">
                                    Источник
                                </c:when>
                                <c:when test="${gaHeader.name == 'ga:campaign'}">
                                    Email ID
                                </c:when>
                                <c:when test="${gaHeader.name == 'ga:landingPagePath'}">
                                    Целевая страница
                                </c:when>
                                <c:when test="${gaHeader.name == 'ga:sessions'}">
                                    Сессии
                                </c:when>
                                <c:when test="${gaHeader.name == 'ga:transactions'}">
                                    Транзакции
                                </c:when>
                                <c:otherwise>
                                    ${gaHeader.name}
                                </c:otherwise>
                            </c:choose>
                        </th>
                    </c:forEach>
                    </thead>
                    <tbody>
                    <c:forEach items="${gaData.rows}" var="gaRow">
                        <tr>
                            <c:forEach items="${gaRow}" var="gaCell">
                                <td class="column1">${gaCell}</td>
                            </c:forEach>
                        </tr>
                    </c:forEach>
                    </thead>
                    </tbody>
                </table>
                </c:if>
            </div>
            <form method="post" id="getfile"></form>
		    <div class="stat-chart stats"></div>
		    <div class="stat-chart-MAIL_RU stats"></div>
		    <div class="stat-chart-BK_RU stats"></div>
		    <div class="stat-chart-LIST_RU stats"></div>
		    <div class="stat-chart-INBOX_RU stats"></div>
		    <div class="stat-chart-YANDEX_RU stats"></div>
		    <div class="stat-chart-GMAIL_COM stats"></div>
		    <div class="stat-chart-YAHOO_COM stats"></div>
		    <div class="stat-chart-HOTMAIL_COM stats"></div>
		    <div class="stat-chart-LIVE_RU stats"></div>
	    </div>
    </div>
</div>
<script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(function() {

	    var statsDataRows = {
		    title: 'По данной рассылке',
		    el: '.stat-chart',
		    rows: [
			    ['Отправлено', ${stats.sent div stats.addresses}, ${stats.sent}],
			    ['Открыто', ${stats.opened div stats.sent}, ${stats.opened}],
			    ['Не открыто', ${stats.unopened div stats.sent}, ${stats.unopened}],
			    ['Не доставлено', ${stats.failed div stats.sent}, ${stats.failed}],
			    ['Отписаны', ${stats.unsubscribed div stats.sent}, ${stats.unsubscribed}],
			    ['Жалобы', ${stats.abuse div stats.sent}, ${stats.abuse}]
		    ]
	    };

	    ChartUtils.addChart(statsDataRows);

	    <c:forEach var="entry" items="${domainStats}">
		    var statsDataRows = {
			    title: '${entry.key}',
			    el: '.stat-chart-${entry.key}',
			    rows: [
				    ['Отправлено', ${entry.value.sent div stats.sent}, ${entry.value.sent}],
				    ['Открыто', ${entry.value.sent > 0 ? (entry.value.opened div entry.value.sent) : 0}, ${entry.value.opened}],
				    ['Не открыто', ${entry.value.sent > 0 ? (entry.value.unopened div entry.value.sent) : 0}, ${entry.value.unopened}],
				    ['Не доставлено', ${entry.value.sent > 0 ? (entry.value.failed div entry.value.sent) : 0}, ${entry.value.failed}],
				    ['Отписаны', ${entry.value.sent > 0 ? (entry.value.unsubscribed div entry.value.sent) : 0}, ${entry.value.unsubscribed}],
				    ['Жалобы', ${entry.value.sent > 0 ? (entry.value.abuse div entry.value.sent) : 0}, ${entry.value.abuse}]
			    ]
		    };

		    ChartUtils.addChart(statsDataRows);
	    </c:forEach>
    });

</script>
<jsp:include page="/WEB-INF/includes/footer.jsp" />
</body>
</html>
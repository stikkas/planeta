<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/includes/taglibs.jsp" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/includes.jsp"%>
    <title>Шаблоны</title>
</head>
<body>
<jsp:include page="/WEB-INF/includes/header.jsp" />

<div class="container">
    <h3>Шаблоны</h3>
    <div class="btn-toolbar">
        <a class="btn btn-success" href="/template"><i class="icon-plus icon-white"></i> Создать</a>
    </div>
    <table class="table table-striped">
        <thead>
        <tr>
            <th class="span1">№</th>
            <th>Название шаблона</th>
            <th>Действия</th>
        </tr>
        </thead>
        <c:forEach var="template" items="${templates}" varStatus="templateStatus">
            <tr>
                <td>${templateStatus.index + 1}</td>
                <td><a href="/template/${template.templateId}">${template.name}</a></td>
                <td><a href="/template/${template.templateId}/delete">Удалить</a></td>
            </tr>
        </c:forEach>
    </table>
</div>

<jsp:include page="/WEB-INF/includes/footer.jsp" />
</body>
</html>
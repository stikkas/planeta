<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/includes/taglibs.jsp" %>
<html>
<head>
    <%@include file="/WEB-INF/includes/includes.jsp"%>
    <title>Список рассылки</title>
</head>
<body>
<jsp:include page="/WEB-INF/includes/header.jsp" />

<form class="form-horizontal" onsubmit="Mailer.saveFilterForm(); return false;">
    <div class="modal hide" id="filterModal">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <div class="modal-title">Сохранение списка</div>
        </div>
        <div class="modal-body">
            <fieldset>
                <div class="control-group">
                    <label class="control-label">Название</label>
                    <div class="controls">
                        <input name="nameNew" id="nameNew" type="text" placeholder="Введите название списка" />
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="modal-footer">
            <div class="modal-footer-cont">
                <a href="#" class="btn" data-dismiss="modal">Отмена</a>
                <button type="submit" name="save" class="btn btn-primary" onclick="$(this).attr('disabled', 'true'); Mailer.saveFilterForm(); return false;">Сохранить</button>
            </div>
        </div>
    </div>
</form>

<div class="container">
    <form:form modelAttribute="filterList" class="form-horizontal">
        <form:input path="filterListId" type="hidden" />
        <form:input path="name" type="hidden" />
        <fieldset>
            <div class="control-group">
                <label class="control-label">Тип запроса</label>
                <div class="controls" title="${index}">
                    <form:select path="filterType">
                        <form:option value="AND">AND</form:option>
                        <form:option value="OR">OR</form:option>
                        <form:option value="FILE">FILE</form:option>
                    </form:select>
                </div>
            </div>
	        <c:if test="${filterList.filterType != \"FILE\"}">
	            <div class="control-group">
	                <div class="control-label"></div>
	                <div class="controls">
	                    <button onclick="Mailer.addFilter(); event.preventDefault(); return false;" class="btn btn-success">
	                        <i class="icon-plus icon-white"></i> Добавить фильтр
	                    </button>
	                </div>
	            </div>
	        </c:if>
            <c:forEach var="filter" items="${filterList.filters}" varStatus="fStatus">
                <c:set var="index" value="${fStatus.index}"/>
                <div class="control-group">
                    <div class="control-label"></div>
                    <div class="controls" index="${index}">
                        <%@ include file="attr.jsp" %>
                    </div>
                </div>
            </c:forEach>
            <form:errors path="name">
                <div class="alert alert-error">
                    Ошибка: <form:errors path="name"/>
                </div>
            </form:errors>
            <div class="form-actions btn-toolbar">
                    <div class="btn-group">
                        <button type="submit" class="btn btn-primary" name="buildList" onclick="Mailer.getEmails(); event.preventDefault();">
                            <i class="icon-search icon-white"></i> Посмотреть список
                        </button>
                    </div>
                    <div class="btn-group">
                        <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" type="submit">
                            <i class="icon-star icon-white"></i> Сохранить
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="javascript:void(0);" onclick="Mailer.showModal();">Сохранить как...</a>
                            </li>
                            <c:if test="${filterList.filterListId != null}">
                                <li>
                                    <a href="javascript:void(0);" onclick="$('#save').click();">Сохранить как &laquo;${filterList.name}&raquo;</a>
                                </li>
                            </c:if>
                        </ul>
                    </div>
                <button type="submit" name="save" id="save" class="hide"></button>
            </div>
        </fieldset>
    </form:form>

    <div id="emails">
        <jsp:include page="emails.jsp" />
    </div>
</div>

<jsp:include page="/WEB-INF/includes/footer.jsp" />
</body>
</html>
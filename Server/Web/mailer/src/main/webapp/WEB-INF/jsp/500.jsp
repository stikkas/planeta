<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <style>
        <%@include file="/css/bootstrap.css" %>
        <%@include file="/css/mailer.css" %>
    </style>
    <title>Произошла ошибка</title>
</head>
<body>
<div class="navbar container">
    <div class="navbar-inner">
        <div class="container">
            <a class="brand">Рассылки Планеты</a>
        </div>
    </div>
</div>
<div class="container">
    <div class="btn-toolbar">
    </div>
    <div class="alert alert-error">
        К сожалению, произошла ошибка. Если вы попали на эту страницу по ссылке, <a href="mailto:${admin}">сообщите нам</a>.
    </div>
</div>
</body>
</html>


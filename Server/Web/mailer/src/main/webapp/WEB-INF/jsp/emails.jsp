<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/includes/taglibs.jsp" %>

<p>
    Выбрано ${emailsCount} адресатов:
</p>
<c:choose>
	<c:when test="${filterList.filterType == \"FILE\"}">
		<table class="table table-striped">
			<c:forEach var="email" items="${emails}">
				<tr>
					<td>${email.email}</td>
					<c:forEach var="p" items="${email.params}" varStatus="s">
						<c:if test="${!s.first}">
							<td>${p}</td>
						</c:if>
					</c:forEach>
				</tr>
			</c:forEach>
		</table>
	</c:when>
	<c:otherwise>
		<table class="table table-striped">
			<thead>
			<tr>
				<th>E-mail</th>
				<th>Имя пользователя</th>
			</tr>
			</thead>
			<c:forEach var="email" items="${emails}">
				<tr>
					<td>${email.email}</td>
					<td>${email.displayName}</td>
				</tr>
			</c:forEach>
		</table>
	</c:otherwise>
</c:choose>
<script>
    $('#filterType').btnSelect();
    $('.datepicker').datepicker();
</script>

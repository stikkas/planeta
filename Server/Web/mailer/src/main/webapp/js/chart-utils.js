ChartUtils = {
	getTooltip: function (col) {
		return function (table, row) {
			return table.getValue(row, 0) + '\n' + table.getColumnLabel(col) + ': ' + table.getValue(row, col) + ' (' + (table.getValue(row, col - 1) * 100).toFixed(2) + '%)';
		};
	},

	getPercent: function (col) {
		return function (table, row) {
			var value = table.getValue(row, col);
			return value ? Math.max(value, 0.02) : 0;
		};
	},

	addChart: function (data) {
		var options = {
			title: data.title != null ? data.title : 'Статистика по данной рассылке',
			vAxis: {
				format: '#%',
				viewWindowMode: 'explicit',
				viewWindow: {
					max: 1.1,
					min: 0
				}
			},
			colors: ['#0063a6']
		};

		var dataTable = new google.visualization.DataTable();
		dataTable.addColumn('string');
		dataTable.addColumn('number');
		dataTable.addColumn('number', data.title);
		dataTable.addRows(data.rows);

		var dataView = new google.visualization.DataView(dataTable);
		dataView.setColumns([0,
			{calc: ChartUtils.getPercent(1), type: 'number', label: data.title}, {calc: ChartUtils.getTooltip(2), type: 'string', role: 'tooltip'}
		]);

		var chart = new google.visualization.ColumnChart($(data.el).get(0));
		chart.draw(dataView, options);
	}
};

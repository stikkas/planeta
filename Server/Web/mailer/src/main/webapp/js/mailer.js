/**
 * Created with IntelliJ IDEA.
 * User: s.fionov
 * Date: 17.07.12
 */

$.fn.extend({
    render: function(data) {
        return $(Hogan.compile($(this).text()).render(data));
    },

    btnSelect:function () {
        return $(this).each(function () {
            if ($(this).hasClass('ui-planeta')) {
                return;
            }

            var elements = [];
            var select = this;
            var btnGroup = $('<div class="btn-group"></div>');

            $(this).children().each(function () {
                var span = $('<div class="btn"></div>');
                $(span).html($(this).html());

                if ($(this).attr('selected')) {
                    $(span).addClass('active');
                }

                elements.push({
                    option:this,
                    span:span
                });
            });

            $.each(elements, function (index, value) {
                $(btnGroup).append(value.span);
                $(value.span).click(function (e) {
                    e.preventDefault();
                    if ($(value.span).hasClass('active')) return;

                    // FF hack
                    select.selectedIndex = -1;

                    $.each(elements, function (index, el) {
                        $(el.span).removeClass('active');
                        $(el.option).removeAttr('selected');
                    });
                    $(value.span).addClass('active');
                    $(value.option).attr('selected', true);
                    $(select).change();
                });
            });

            $(select).after(btnGroup);
            $(this).addClass('ui-planeta').hide();
        });
    }
});

Mailer = {
    fetchFilterOptions: function(attribute, index, element) {
        $.ajax({
            url: '/attr.html',
            data: {
                attrId: attribute,
                index: index
            },
            success: function(a,b,c) {
                var controls = $(element).closest('div.control-group').find('div.controls');
                controls.html(a);
                controls.find('.datepicker').datepicker(Mailer.datepickerOptions);
            }
        });
    },


    hideAndRemove: function(element) {
        $(element).hide('fast', function() {
            $(element).remove();
        });
    },

    addFilter: function() {
        var lastIndex = new Number($('#filterList').find('div.controls:last').attr('index') || 0);
        var control = $("<div></div>")
            .addClass("control-group")
            .append($("<div></div>").addClass("control-label"))
            .append($("<div></div>").addClass("controls").attr("index", lastIndex + 1));
        $('#filterList').find('div.form-actions').before(control);
        Mailer.fetchFilterOptions(undefined, lastIndex + 1, control);
    },

    showModal: function() {
        $('#filterModal').modal();
    },

    saveFilterForm: function() {
        //$('#filterListId').val('');
        $('#name').val($('#nameNew').val());
        $('#save').click();
    },

    showForm: function(template, data) {
        if ($('body').data('modal') == null) {
            var container = $('<div id="modal-template"></div>');
            $('body').append(container);
            $('body').data('modal', container);
        }
        var modal = $(template).render(data);
        $('body').data('modal').html(modal);
        $('body').data('modal').find('div.modal').modal();
    },

    showDeleteForm: function(id, name) {
        Mailer.showForm('#delete-filter-template', {
            id: id,
            name: name
        });
    },

    getEmails: function() {
        $('#emails').html('<img src="/img/ajax.gif"/>');
        $('body').css('overflow-y', 'scroll');
        $.ajax({
            url: '/emails.html',
            type: 'post',
            data: $('#filterList').serialize(),
            success: function(response) {
                $('#emails').html(response);
            }
        });
    },

    fetchTemplate: function() {
        var id = $('#templateId').val();
        $.ajax({
            url: '/template-text.html',
            type: 'get',
            data: {
                id: id
            },
            success: function(response) {
                var redactor = $('#text').data('redactor');
                if (redactor) {
                    redactor.setCode(response);
                } else {
                    $('#text').val(response);
                }
            }
        });
    }
};

RedactorUtils = {};
RedactorUtils.insertName = function(redactor) {
    redactor.insertHtml("{{name}}");
};
RedactorUtils.insertUnsubscribe = function(redactor) {
    redactor.insertHtml("{{unsubscribe}}");
};
RedactorUtils.insertUnsubscribeUrl = function(redactor) {
    redactor.insertHtml("{{unsubscribeLink}}");
};
RedactorUtils.insertEmail = function(redactor) {
    redactor.insertHtml("{{email}}");
};
RedactorUtils.redactorOptions = {
    focus: true,
    lang: 'ru',
    imageUpload: '/upload-image.json',
    convertDivs: false,
    buttonsAdd: ['|', 'name', 'unsubscribe', 'unsubscribe_url', 'email'],
    buttonsCustom: {
        'name': {
            title: "Вставить имя",
            callback: RedactorUtils.insertName
        },
        'unsubscribe': {
            title: "Вставить информацию для отписывания",
            callback: RedactorUtils.insertUnsubscribe
        },
        'unsubscribe_url': {
            title: "Вставить URL для отписывания",
            callback: RedactorUtils.insertUnsubscribeUrl
        },
        'email': {
            title: "Вставить e-mail получателя",
            callback: RedactorUtils.insertEmail
        }
    }
};


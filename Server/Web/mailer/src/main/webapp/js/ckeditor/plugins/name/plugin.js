CKEDITOR.plugins.add('name', {
    init: function (editor) {
        editor.addCommand('insertName', {
            exec: function (editor) {
                editor.insertHtml('{{name}}');
            }
        });
        editor.ui.addButton('Name', {
            label: 'Вставить имя',
            command: 'insertName',
            toolbar: 'insert,100'
        });
    }
});


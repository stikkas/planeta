CKEDITOR.plugins.add('unsubscribe', {
    init: function (editor) {
        editor.addCommand('insertUnsubscribe', {
            exec: function (editor) {
                editor.insertHtml('{{unsubscribe}}');
            }
        });
        editor.ui.addButton('unsubscribe', {
            label: 'Вставить информацию для отписывания',
            command: 'insertUnsubscribe',
            toolbar: 'insert,200'
        });
    }
});
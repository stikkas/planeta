/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    config.height = 400;
    config.language = 'ru';
    config.enterMode = CKEDITOR.ENTER_DIV;
    config.fillEmptyBlocks = false;
    config.allowedContent = true;
    config.extraPlugins = 'lineheight,dragresize,email,name,unsubscribe,unsubscribeLink';
    config.toolbar = [
        ['Bold','Italic','Underline','Strike','-','Subscript','Superscript','-','RemoveFormat'],
        ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
        ['Cut','Copy','PasteText','-', 'SpellChecker', 'Scayt'],
        ['Maximize','-','Preview'],
        ['Source'],
        '/',
        ['NumberedList','BulletedList','-','Outdent','Indent'],
        ['Link','Unlink','Anchor'],
        ['Image','Table'],
        ['FontSize','lineheight','TextColor','BGColor'],
        ['email', 'Name', 'unsubscribe', 'unsubscribeLink']
    ];
};

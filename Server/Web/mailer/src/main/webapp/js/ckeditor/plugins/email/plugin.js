CKEDITOR.plugins.add('email', {
    init: function (editor) {
        editor.addCommand('insertEmail', {
            exec: function (editor) {
                editor.insertHtml('{{email}}');
            }
        });
        editor.ui.addButton('email', {
            label: 'Вставить email',
            command: 'insertEmail',
            toolbar: 'insert,400'
        });
    }
});
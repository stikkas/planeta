CKEDITOR.plugins.add('unsubscribeLink', {
    init: function (editor) {
        editor.addCommand('insertUnsubscribeLink', {
            exec: function (editor) {
                editor.insertHtml('{{unsubscribeLink}}');
            }
        });
        editor.ui.addButton('unsubscribeLink', {
            label: 'Вставить URL для отписывания',
            command: 'insertUnsubscribeLink',
            toolbar: 'insert,300'
        });
    }
});
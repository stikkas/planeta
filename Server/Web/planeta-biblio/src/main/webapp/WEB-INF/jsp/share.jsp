<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.rapid-framework.org.cn/rapid" prefix="layout" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${param['no-counters'] != true && header['User-Agent'] != 'planeta-snapshot-bot'}">
    <layout:override name="js-block">
        <script type="text/javascript">
            $(function() {
                setTimeout(function() {
                    window.location.href = 'https://' + workspace.serviceUrls.libraryUrl
                }, 200);
            })
        </script>
    </layout:override>
</c:if>

<%@ include file="/WEB-INF/jsp/base.jsp" %>
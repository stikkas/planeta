<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<%@ taglib uri="http://www.rapid-framework.org.cn/rapid" prefix="layout" %> 

<html class="no-js" lang="ru">
    <head>
        <%@include file = "/WEB-INF/jsp/includes/meta-tags.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width"/>
        <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>

        <%@include file = "/WEB-INF/jsp/includes/common-css.jsp" %>
        <layout:block name="css-block">  
        </layout:block>
        <%@include file="/WEB-INF/jsp/includes/planeta-js.jsp" %>
        <%@include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>        
    </head>
    <body class="biblio-page grid-1170">
        <%@include file="/WEB-INF/jsp/includes/generated/irma/header.jsp"%>
        <%@include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>

        <div id="global-container">
            <div id="main-container" class="wrap-container">
                <div id="center-container">
                    <layout:block name="center-container"> 
                    </layout:block>
                </div>
            </div>
        </div>
        <%@include file="/WEB-INF/jsp/includes/generated/irma/footer.jsp"%>
        <layout:block name="modal-block">  
        </layout:block>
        <layout:block name="js-block"> 
        </layout:block>
    </body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/js/share/book.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/js/share/bin.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/js/share/modal.jsp" %>
<script id="biblio-wizard_cont" type="text/x-jquery-template">
<div class="book-card"></div>
</script>

<script id="biblio-wizard-main_tag" type="text/x-jquery-template">
<span class="biblio-wizard-tabs_link">{{= name}}</span>
</script>

<script id="biblio-wizard-secondary_tag" type="text/x-jquery-template">
<span class="biblio-wizard-tabs_drop-link">{{= name}}</span>
</script>

<script id="wizard_add_info-template" type="text/x-jquery-template">
    <span {{if count < 1}}style="display:none"{{/if}}>{{if count > 1}}Все {{= count}} в{{/if}}{{if count < 2}}В{{/if}}ыбранн{{= StringUtils.plurals(count, ['ых', 'ое', 'ых'])}} издани{{= StringUtils.plurals(count, ['й', 'е', 'я'])}} отправ{{= StringUtils.plurals(count, ['я', 'и', 'я'])}}тся в библиотеки, которые вы укажите на следующем шаге.</span>
</script>

<script id="books-filter-template" type="text/x-jquery-template">
<div class="biblio-wizard-filter_search">
    <input name="query" type="text" placeholder="Поиск по названию издания" class="form-control" value="{{= query}}">
    <span class="s-icon s-icon-search"></span>
</div>
<div class="biblio-wizard-filter_tabs">
    <div class="biblio-wizard-tabs">
        <div id="wizard-main_tags" style="display: inline"></div>
    </div>
</div>
</script>
<p:script src="biblio-books.js"></p:script>
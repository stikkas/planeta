<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.rapid-framework.org.cn/rapid" prefix="layout" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<layout:override name="step">
    <c:set var="step" value="first"/>
    <c:set var="step_title" value="1. Выбор изданий"/>
    <c:set var="next_step" value="library"/>
    <c:set var="prev_step" value=""/>
    <c:set var="next_step_btn_label" value="Перейти к выбору библиотеки"/>
    <c:set var="wizard_text" value="Выберите издание и количество подписок. В стоимость каждой подписки включена доставка в библиотеку и то количество номеров, которое указано в описании издания."/>
</layout:override>

<layout:override name="biblio-wizard-filter">
</layout:override>

<layout:override name="biblio-wizard_cont-part">
    <div id="biblio-found-books"></div>
</layout:override>

<layout:override name="modal-block">  
    <div id="modal-book-card" class="modal-dialog in" style="display: none"></div>
    <div class="modal-backdrop modal-backdrop-biblio in" style="display: none"></div>
</layout:override> 

<layout:override name="js-block">  
    <%@ include file="/WEB-INF/jsp/includes/js/books-js.jsp" %> 
</layout:override> 

<%@ include file="/WEB-INF/jsp/library.jsp" %> 
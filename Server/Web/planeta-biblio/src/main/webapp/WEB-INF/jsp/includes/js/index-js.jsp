<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/js/share/book.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/js/share/modal.jsp" %>
<script id="biblio-stats_cont" type="text/x-jquery-template">
    <div class="biblio-stats_cont">
        <div class="biblio-stats_list">
            <div class="biblio-stats_i">
                <div class="biblio-stats_i-cont">
                    <div class="biblio-stats_val">{{= StringUtils.humanNumber(books)}}</div>
                    <div class="biblio-stats_lbl">
                        Издани{{= StringUtils.plurals(books, ['й', 'е', 'я'])}} <br> в&nbsp;проекте
                    </div>
                </div>
            </div>
            <div class="biblio-stats_i">
                <div class="biblio-stats_i-cont">
                    <div class="biblio-stats_val">{{= StringUtils.humanNumber(libraries)}}</div>
                    <div class="biblio-stats_lbl">
                        Библиотек{{= StringUtils.plurals(libraries, ['', 'а', 'и'])}} <br> в&nbsp;проекте
                    </div>
                </div>
            </div>
            <div class="biblio-stats_i">
                <div class="biblio-stats_i-cont">
                    <div class="biblio-stats_val">{{= StringUtils.humanNumber(signedIssues)}}</div>
                    <div class="biblio-stats_lbl">
                        {{= StringUtils.plurals(signedIssues, ['Подписок', 'Подписка',
                        'Подписки'])}} <br> оформлен{{= StringUtils.plurals(signedIssues, ['о', 'а', 'ы'])}}
                    </div>
                </div>
            </div>
            <div class="biblio-stats_i">
                <div class="biblio-stats_i-cont">
                    <div class="biblio-stats_val">{{= StringUtils.humanNumber(totalSum)}} <span class="b-rub">Р</span></div>
                    <div class="biblio-stats_lbl">
                        Собрано <br> на&nbsp;подписку
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>
<script id="book-card_special" type="text/x-jquery-template">
    <div class="book-card_special">
    <div class="book-card_special_count">{{= count}}</div>
    <div class="book-card_special_lbl"> периодических изданий </div>
    <div class="book-card_special_action js-to-books">
    <span class="book-card_special_btn">Выбрать издания</span>
    </div>
    </div>
</script>
<script id="biblio-main-list_preview__books" type="text/x-jquery-template">
    <div class="biblio-main-list_books">
    <div class="book-card"></div>
    </div>
</script>

<script id="biblio-library-request-template" type="text/x-jquery-template">
    <div class="modal-biblio-title">
        Заявка на участие в проекте
    </div>
    <form class="biblio-request">
        <div class="biblio-request_field">
            <div class="wrap-row">
                <div class="col-4"><div class="biblio-request_label">Название организации</div></div>
                <div class="col-8">
                    <textarea class="form-control control-lg js-form" name="name">{{= name}}</textarea>
                    <div id="nameError" class="js-error" style="display: none; color: red;"></div>
                </div>
            </div>
        </div>
        <div class="biblio-request_field">
            <div class="wrap-row">
                <div class="col-4"><div class="biblio-request_label">Тип организации</div></div>
                <div class="col-8">
                    <select class="form-control control-lg pln-select js-form" name="libraryType" data-planeta-ui="dropDownSelect">
                        <option value="PUBLIC">Публичные библиотеки</option>
                        <option value="SCHOLASTIC">Школьные библиотеки</option>
                        <option value="SCIENTIFIC">Научные библиотеки</option>
                        <option value="DOMESTIC">Детские и семейные библиотеки</option>
                        <option value="SPECIAL">Специализированные библиотеки</option>
                    </select>
                    <div id="libraryTypeError" class="js-error" style="display: none; color: red;"></div>
                </div>
            </div>
        </div>
        <div class="biblio-request_field">
            <div class="wrap-row">
                <div class="col-4"><div class="biblio-request_label">Почтовый адрес</div> </div>
                <div class="col-8">
                    <textarea class="form-control control-lg js-form" name="address">{{= address}}</textarea>
                    <div id="addressError" class="js-error" style="display: none; color: red;"></div>
                </div>
            </div>
        </div>
        <div class="biblio-request_field">
            <div class="wrap-row">
                <div class="col-4"><div class="biblio-request_label">Индекс</div> </div>
                <div class="col-8">
                    <input type="text" class="form-control control-lg js-form" name="postIndex" value="{{= postIndex}}">
                    <div id="postIndexError" class="js-error" style="display: none; color: red;"></div>
                </div>
            </div>
        </div>
        <div class="biblio-request_field">
            <div class="wrap-row">
                <div class="col-4"><div class="biblio-request_label">Контактное лицо</div></div>
                <div class="col-8">
                    <input type="text" class="form-control control-lg js-form" name="contractor" value="{{= contractor}}">
                    <div id="contractorError" class="js-error" style="display: none; color: red;"></div>
                </div>
            </div>
        </div>
        <div class="biblio-request_field">
            <div class="wrap-row">
                <div class="col-4"> <div class="biblio-request_label">E-mail организации</div></div>
                <div class="col-6">
                    <input type="text" class="form-control control-lg js-form" name="email" value="{{= email}}">
                    <div id="emailError" class="js-error" style="display: none; color: red;"></div>
                </div>
            </div>
        </div>
        <div class="biblio-request_field">
            <div class="wrap-row">
                <div class="col-4"> <div class="biblio-request_label">Сайт организации</div></div>
                <div class="col-8">
                    <input type="text" class="form-control control-lg js-form" name="site" value="{{= site}}">
                    <div id="siteError" class="js-error" style="display: none; color: red;"></div>
                </div>
            </div>
        </div>
        <div class="biblio-request_field">
            <div class="wrap-row">
                <div class="col-4 col-offset-4">
                    <div class="biblio-request_action">
                        <button class="btn btn-primary btn-lg btn-block">Отправить</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</script>

<script id="biblio-book-request-template" type="text/x-jquery-template">
    <div class="modal-biblio-title">
    Заявка на участие в проекте
    </div>
    <form class="biblio-request">
    <div class="biblio-request_field">
    <div class="wrap-row">
    <div class="col-4"><div class="biblio-request_label">Название издания</div></div>
    <div class="col-8">
    <textarea class="form-control control-lg js-form" name="title">{{= title}}</textarea>
    <div id="titleError" class="js-error" style="display: none; color: red;"></div>
    </div>
    </div>
    </div>
    <div class="biblio-request_field">
    <div class="wrap-row">
    <div class="col-4"><div class="biblio-request_label">Контактное лицо</div></div>
    <div class="col-8">
    <input type="text" class="form-control control-lg js-form" name="contact" value="{{= contact}}">
    <div id="contactError" class="js-error" style="display: none; color: red;"></div>
    </div>
    </div>
    </div>
    <div class="biblio-request_field">
    <div class="wrap-row">
    <div class="col-4"><div class="biblio-request_label">Сайт издания</div></div>
    <div class="col-8">
    <input type="text" class="form-control control-lg js-form" name="site" value="{{= site}}">
    <div id="siteError" class="js-error" style="display: none; color: red;"></div>
    </div>
    </div>
    </div>
    <div class="biblio-request_field">
    <div class="wrap-row">
    <div class="col-4"> <div class="biblio-request_label">E-mail для связи</div></div>
    <div class="col-8">
    <input type="text" class="form-control control-lg js-form" name="email" value="{{= email}}">
    <div id="emailError" class="js-error" style="display: none; color: red;"></div>
    </div>
    </div>
    </div>
    <div class="biblio-request_field">
    <div class="wrap-row">
    <div class="col-4 col-offset-4">
    <div class="biblio-request_action">
    <button class="btn btn-primary btn-lg btn-block">Отправить</button>
    </div>
    </div>
    </div>
    </div>
    </form>
</script>

<script id="biblio-request-form-template" type="text/x-jquery-template">
    <div class="m-project-feedback-title">
    Выберите тип организации
    </div>

    <div class="m-project-feedback-questions clearfix">
    <div class="mp-feedback-questions">
    <div class="mpf-questions-inner">
    <div class="mpf-questions-inner-item">Вы представляете журнал или другое издание и хотели бы участвовать в проекте или оказать поддержку. Свяжитесь с нами, чтобы узнать о различных условиях сотрудничества.</div>
    <div class="biblio-request_action biblio-request_action_choose biblio-request_action_choose_left">
    <button class="btn btn-primary btn-lg btn-block js-open-request-form" data-org-type="BOOK">Издание</button>
    </div>
    </div>
    </div>

    <div class="mp-feedback-questions">
    <div class="mpf-questions-inner mp-fi-right">
    <div class="mpf-questions-inner-item">Вы представляете библиотеку — публичную, школьную, научную, специализированную — и хотите, чтобы ее адрес появился в базе проекта.</div>
    <div class="biblio-request_action biblio-request_action_choose biblio-request_action_choose_right">
    <button class="btn btn-primary btn-lg btn-block js-open-request-form" data-org-type="LIBRARY">Библиотека</button>
    </div>
    </div>
    </div>
    </div>
</script>

<script id="request-success-template" type="text/x-jquery-template">
    <div class="biblio-request_field">
    <div class="wrap-row">
    <div class="col-12"><div class="biblio-request_success">Спасибо, ваша заявка получена!</div> </div>
    </div>
    </div>
    <div class="biblio-request_field">
    <div class="wrap-row">
    <div class="col-4 col-offset-4">
    <div class="biblio-request_action">
    <button class="btn btn-primary btn-lg btn-block">Закрыть</button>
    </div>
    </div>
    </div>
    </div>
</script>

<script id="partner-template" type="text/x-jquery-template">
    <a href="{{= originalUrl}}" target="_blank"><img title="{{= name}}" alt="{{= name}}" src="{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.SMALL)}}"></a>
</script>

<script id="biblio-main-list_i" type="text/x-jquery-template">
    <div class="biblio-main-list_descr">
    <div class="biblio-main-list_cont">
    <div class="biblio-main-list_cont-in">
    <div class="biblio-main-list_head">{{= title}}</div>
    <div class="biblio-main-list_text">{{= StringUtils.htmlDecode(annonce)}}</div>
    {{if textLink}}
    <div class="biblio-main-list_more"><a href="{{= textLink}}" target="_blank" rel="nofollow noopener">{{= textTitle}}</a></div>
    {{/if}}
    </div>
    </div>
    </div>
    <div class="biblio-main-list_preview">
    <div style="background-image:url({{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.ORIGINAL)}});" class="biblio-main-list_img"></div>
    {{if videoLink}}
    <div class="biblio-main-play">
    <div class="biblio-main-play_cont">
    <div class="biblio-main-play_link">
    <div class="biblio-main-play_ico"></div>
    <div class="biblio-main-play_text">{{= videoTitle}}</div>
    </div>
    </div>
    </div>
    <div class="biblio-main-play embed-video" style="display:none;">
    <iframe data-src="{{= videoLink}}" frameborder="0"></iframe>
    </div>
    {{/if}}
    </div>
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAcNhAbtfdB6fkVVs7BoaIoW5HEPjAn4Mw&v=3.31"></script>
<p:script src="biblio-index.js"></p:script>
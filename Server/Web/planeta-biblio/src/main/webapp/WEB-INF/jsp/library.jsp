<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.rapid-framework.org.cn/rapid" prefix="layout" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<layout:block name="step">
    <c:set var="step" value="second"/>
    <c:set var="step_title" value="2. Выбор библиотек"/>
    <c:set var="next_step" value="payment"/>
    <c:set var="prev_step" value="books"/>
    <c:set var="next_step_btn_label" value="Перейти к оплате"/>
    <c:set var="wizard_text" value="Все выбранные издания будут отправлены в каждую указанную вами библиотеку. При выборе пункта «Любая библиотека» выбор библиотеки будет осуществлен из общего списка случайным образом."/>
</layout:block>


<layout:override name="center-container">
    <script id="biblio-wizard-main_tag" type="text/x-jquery-template">
        <span class="biblio-wizard-tabs_link">{{= name}}</span>
    </script>

    <style>
        a.disabled {
            pointer-events: none;
        }
    </style>

    <div class="wrap">
        <div class="biblio-wizard-head">
            <layout:block name="biblio-wizard-head">
                <a href="/books" class="biblio-wizard-head_i <c:if test="${step eq 'first'}">active</c:if>">
                    <div class="biblio-wizard-head_ico">
                        <span class="s-wizard-books"></span>
                    </div>
                    <div class="biblio-wizard-head_name">
                        Выбор изданий
                    </div>
                </a>

                <a href="/library" class="js-second biblio-wizard-head_i <c:if test="${step eq 'second'}"> active</c:if>">
                    <div class="biblio-wizard-head_ico">
                        <span class="s-wizard-library"></span>
                    </div>
                    <div class="biblio-wizard-head_name">
                        Выбор библиотек
                    </div>
                </a>

                <a href="/payment" class="js-third biblio-wizard-head_i <c:if test="${step eq 'third'}">active</c:if>">
                    <div class="biblio-wizard-head_ico">
                        <span class="s-wizard-payment"></span>
                    </div>
                    <div class="biblio-wizard-head_name">
                        Оплата подписок
                    </div>
                </a>
            </layout:block>
        </div>

        <div class="biblio-wizard">
            <layout:block name="biblio-wizard">
                <div class="biblio-title">
                    <div class="biblio-title_text">${step_title}</div>
                </div>
                <div class="biblio-wizard_text">${wizard_text}</div>
                <layout:block name="biblio-wizard-filter_out">
                <div class="biblio-wizard-filter">
                    <layout:block name="biblio-wizard-filter">
                        <div class ="js-library-choose"></div>
                    </layout:block>

                </div>
                </layout:block>
                <layout:block name="biblio-wizard-payment"> </layout:block>
                <div class="biblio-wizard_cont">
                    <layout:block name="biblio-wizard_cont-part">
                        <div class="biblio-all-library js-library-get-random">
                            <div class="biblio-all-library_check">
                                <div class="biblio-counter">
                                    <div class="biblio-counter_check"></div>
                                </div>
                            </div>
                            <div class="biblio-all-library_ico"></div>
                            <div class="biblio-all-library_cont">
                                <div class="biblio-all-library_name">
                                    Любая библиотека
                                </div>
                                <div class="biblio-all-library_text">
                                    Выбор библиотеки для доставки изданий осуществляется случайным образом!
                                </div>
                            </div>
                        </div>
                        <div class="biblio-all-library js-library-get-random-temp hidden">
                            <div class="biblio-all-library_cont">
                                <div class="biblio-all-library_name">

                                </div>
                            </div>
                        </div>
                        <div class="biblio-map hidden">
                            <div id="map" style="position: relative; overflow: hidden; transform: translateZ(0px); background-color: rgb(229, 227, 223);"></div>
                        </div>
                        <div class="biblio-library hidden">
                            <div class="js-biblio-libraries"></div>
                        </div>
                    </layout:block>
                    <div class="biblio-wizard_library-total"></div>
                    <div class="biblio-wizard_fix-wrap unfixed">
                        <div class="biblio-wizard_fix-cont">
                            <div class="wrap">
                                <div class="biblio-wizard_total"></div>
                                <div class="biblio-wizard_action">
                                    <div class="biblio-wizard_add-info biblio-wizard_add-info__terms">
                                    <layout:block name="biblio-wizard_add-info"></layout:block>
                                    </div>
                                    <div class="biblio-wizard_btn">
                                        <c:if test="${not empty prev_step}">
                                            <a href="/${prev_step}" class="btn btn-biblio">Предыдущий шаг</a>
                                        </c:if>
                                        <c:if test="${not empty next_step}">
                                            <button class="btn btn-primary">${next_step_btn_label}</button>
                                        </c:if>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </layout:block>
        </div>
    </div>

    <script>
        $(function() {
            $('.biblio-wizard-head_i').click(function (e) {
                e.preventDefault();
                var el = $(e.currentTarget);
                if (!el.hasClass('disabled')) {
                    Biblio.Utils.changeUrl(el.attr('href'))
                }
            });
        });
    </script>
</layout:override> 

<layout:override name="js-block">  
    <%@ include file="/WEB-INF/jsp/includes/js/library-js.jsp" %>
</layout:override>



<%@ include file="/WEB-INF/jsp/base.jsp" %>

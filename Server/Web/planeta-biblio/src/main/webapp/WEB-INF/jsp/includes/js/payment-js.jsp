<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/js/share/book.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/js/share/bin.jsp" %>

<script id="biblio-wizard-payment" type="text/x-jquery-template">
    <div class="biblio-wizard_payment-face">
        <div class="pln-payment-face">
            <div class="pln-payment-face_lbl pln-payment-face_one">Физическое лицо</div>
            <div class="pln-payment-face_switch"></div>
            <div class="pln-payment-face_lbl pln-payment-face_two">Юридическое лицо</div>
        </div>
    </div>
    <div class="js-fiz-payment"></div>
    <div class="js-ul-payment"></div>
</script>

<script id="email-check-template" type="text/x-jquery-template">
<div class="dropdown-popup">
    <div class="dropdown-popup-caret"></div>
    <div class="dropdown-popup-body">
        <div id="hint" class="warning-message-error" style="display: block;">
            <spring:message code="campaign-donate.jsp.propertie.34" text="default text"> </spring:message>
            <br>
            <a class="signup-link" href="javascript:void(0)">
                <spring:message code="campaign-donate.jsp.propertie.14" text="default text"> </spring:message>
            </a>
            <br>
            <sub>
                <spring:message code="campaign-donate.jsp.propertie.35" text="default text"> </spring:message>
                <a href="mailto:support@planeta.ru">support@planeta.ru</a>
            </sub>
        </div>
    </div>
</div>
</script>

    <script id="fiz-payment-template" type="text/x-jquery-template">
    <div class="biblio-wizard_payment-type"></div>
    <div class="biblio-wizard_forms">
        <div class="biblio-form">
            <div class="wrap-row">
                <div class="col-6">
                    <div class="biblio-form_field">
                        <div class="biblio-form_label"> Ваши фамилия, имя и отчество</div>
                        <div class="biblio-form_input"> <input class="form-control" name="fio" value="{{= fio}}"> </div>
                        <div class="biblio-form_help">Ваши ФИО необходимы библиотекам для учета изданий, полученных от частного лица</div>
                    </div>
                </div>
                <div class="col-6">
                {{if !hasEmail}}
                    <div class="biblio-form_field">
                        <div class="biblio-form_label">Адрес электронной почты</div>
                        <div class="biblio-form_input js-anchr-email-block dropdown input-warning-dropdown">
                            <input name="email" placeholder="Введите e-mail" class="js-email form-control" value="{{= email}}"/>
                            {{tmpl '#email-check-template'}}
                        </div>
                        <div class="pln-payment-box_field-row pln-payment-box_field-row_hidden">
                            <input name="email" type="hidden">
                            <div class="pln-payment-box_field-text-error">Пожалуйста введите email</div>
                        </div>
                        <div class="biblio-form_help">На этот адрес будет направлен Бонус для мецената, если информация о нем была указана в описании издания.</div>
                    </div>
                {{/if}}
                </div>
            </div>
        </div>
    </div>
    </script>

<script id="ul-payment-template" type="text/x-jquery-template">
<div class="biblio-wizard_forms">
    <div class="pln-payment-box pln-payment-box__bottom-0">
        <div class="pln-payment-box_head">Кто оплачивает покупку?</div>
        <div class="pln-payment-box_field-group">
            <div class="pln-payment-box_field-row pln-payment-box_field-pdg">
                <b>Внимание!</b> Будьте внимательны при заполнении: все указанные данные должны соответствовать документам организации. Это необходимо для того, чтобы подтвердить и однозначно идентифицировать организацию и сформировать договор.
            </div>
            <div class="pln-payment-box_field-row">
                <div class="pln-payment-box_field-label">
                    Номер телефона 
                    <div data-tooltip="Телефон в формате +7 901 111 11 11" class="ppb_fl_help tooltip"></div>
                </div>
                <div class="pln-payment-box_field-value">
                    <input type="text" value="{{= investInfo.phoneNumber}}" name="phoneNumber" placeholder="+7 ___ ___ __ __" class="pln-payment-box_field-input js-phone">
                    <div class="js-error"></div>
                </div>
            </div>
            <div class="pln-payment-box_field-row js-confirmation-code hidden">
                <div class="pln-payment-box_field-label">
                    Код подтверждения
                    <div class="ppb_fl_help tooltip" data-tooltip="Код подтверждения из SMS"></div>
                </div>
                <div class="pln-payment-box_field-value">
                    <input class="pln-payment-box_field-input js-sms-code" type="text" name="confirmationCode">
                </div>
            </div>
            {{if !hasEmail}}
            <div class="pln-payment-box_field-row">
                <div class="pln-payment-box_field-label">Введите Ваш e-mail</div>
                <div class="pln-payment-box_field-value">
                    <div class="input-field js-anchr-email-block dropdown input-warning-dropdown">
                        <input type="email" name="email" placeholder="me@example.com" class="js-email pln-payment-box_field-input"/>
                        {{tmpl '#email-check-template'}}
                        <div class="js-error"></div>
                    </div>
                </div>
            </div>
            {{/if}}
            <div class="pln-payment-box_field-row">
                <div class="pln-payment-box_field-label"> Полное наименование организации </div>
                <div class="pln-payment-box_field-value"> <textarea class="pln-payment-box_field-input" rows="2" name="orgName">{{= investInfo.orgName}}</textarea> </div>
            </div>
            <div class="pln-payment-box_field-row">
                <div class="pln-payment-box_field-label"> Сокращённое наименование организации (бренд) </div>
                <div class="pln-payment-box_field-value"> <textarea class="pln-payment-box_field-input" rows="2" name="orgBrand">{{= investInfo.orgBrand}}</textarea> </div>
            </div>
            <div class="pln-payment-box_field-row">
                <div class="pln-payment-box_field-label">Дата регистрации</div>
                <div class="pln-payment-box_field-value">
                    <input type="text" name="birthDate" data-planeta-ui="dateSelect" placeholder="12.12.1986" class="pln-payment-box_field-input pln-payment-box_field-input__date js-date">
                    <div class="js-error"></div>
                </div>
            </div>
            <div class="pln-payment-box_field-row">
                <div class="pln-payment-box_field-label">
                    ИНН <div data-tooltip="10 или 12 цифр" class="ppb_fl_help tooltip"></div>
                </div>
                <div class="pln-payment-box_field-value">
                    <input type="text" value="{{= investInfo.inn}}" name="inn" class="pln-payment-box_field-input js-inn">
                </div>
            </div>
            <div class="pln-payment-box_field-row">
                <div class="pln-payment-box_field-label"> Веб-сайт </div>
                <div class="pln-payment-box_field-value"> <input type="text" value="{{= investInfo.webSite}}" class="pln-payment-box_field-input" name="webSite"> </div>
            </div>
        </div>
    </div>
    <div class="pln-payment-box pln-payment-box__bottom-0">
        <div class="pln-payment-box_head"> Местонахождение в соответствии с уставом </div>
        <div class="pln-payment-box_field-group">
            <div class="pln-payment-box_field-row">
                <div class="pln-payment-box_field-label">Страна</div>
                <div class="pln-payment-box_field-value"> 
                    <select class="pln-select pln-select-xlg" data-planeta-ui="country" name="regCountryId" value="{{= investInfo.regCountryId}}"></select>
                    <div class="js-error"></div>
                </div>
            </div>
            <div class="pln-payment-box_field-row">
                <div class="pln-payment-box_field-label">
                    Регион <div class="ppb_fl_help tooltip" data-tooltip="Название области, округа, края, республики"></div>
                </div>
                <div class="pln-payment-box_field-value">
                    <input class="pln-payment-box_field-input" type="text" name="regRegion" value="{{= investInfo.regRegion}}">
                    <div class="js-error"></div>
                </div>
            </div>
            <div class="pln-payment-box_field-row">
                <div class="pln-payment-box_field-label">
                    Населённый пункт <div class="ppb_fl_help tooltip" data-tooltip="Название города или иного населенного пункта"></div>
                </div>
                <div class="pln-payment-box_field-value">
                    <input class="pln-payment-box_field-input" type="text" name="regLocation" value="{{= investInfo.regLocation}}">
                    <div class="js-error"></div>
                </div>
            </div>
            <div class="pln-payment-box_field-row">
                <div class="pln-payment-box_field-label">
                    Адрес <div class="ppb_fl_help tooltip" data-tooltip="Название улицы, номер дома, корпуса, квартиры"></div>
                </div>
                <div class="pln-payment-box_field-value">
                    <input class="pln-payment-box_field-input" type="text" name="regAddress" value="{{= investInfo.regAddress}}">
                    <div class="js-error"></div>
                </div>
            </div>
            <div class="pln-payment-box_field-row">
                <div class="pln-payment-box_field-label">Индекс</div>
                <div class="pln-payment-box_field-value">
                    <input class="pln-payment-box_field-input" type="text" name="regIndex" value="{{= investInfo.regIndex}}">
                    <div class="js-error"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="pln-payment-box pln-payment-box__bottom-0">
        <div class="pln-payment-box_head"> Фактическое местонахождение </div>
        <div class="pln-payment-box_field-group">
            <div class="pln-payment-box_field-row pln-payment-box_field-pdg">
                <div class="check-row js-address-toggle">
                    <span class="checkbox active"></span>
                    <span class="checkbox-label"><b>Фактическое местонахождение совпадает с указанным в уставе</b></span>
                </div>
            </div> 
        </div>
        <div class="pln-payment-box_field-group js-address-block hide">
            <div class="pln-payment-box_field-row">
                <div class="pln-payment-box_field-label">Страна</div>
                <div class="pln-payment-box_field-value"> 
                    <select class="pln-select pln-select-xlg" data-planeta-ui="country" name="liveCountryId" value="{{= investInfo.regCountryId}}"></select>
                    <div class="js-error"></div>
                </div>
            </div>
            <div class="pln-payment-box_field-row">
                <div class="pln-payment-box_field-label">
                    Регион <div class="ppb_fl_help tooltip" data-tooltip="Название области, округа, края, республики"></div>
                </div>
                <div class="pln-payment-box_field-value">
                    <input class="pln-payment-box_field-input" type="text" name="liveRegion" value="{{= investInfo.liveRegion}}">
                    <div class="js-error"></div>
                </div>
            </div>
            <div class="pln-payment-box_field-row">
                <div class="pln-payment-box_field-label">
                    Населённый пункт <div class="ppb_fl_help tooltip" data-tooltip="Название города или иного населенного пункта"></div>
                </div>
                <div class="pln-payment-box_field-value">
                    <input class="pln-payment-box_field-input" type="text" name="liveLocation" value="{{= investInfo.liveLocation}}">
                    <div class="js-error"></div>
                </div>
            </div>
            <div class="pln-payment-box_field-row">
                <div class="pln-payment-box_field-label">
                    Адрес <div class="ppb_fl_help tooltip" data-tooltip="Название улицы, номер дома, корпуса, квартиры"></div>
                </div>
                <div class="pln-payment-box_field-value">
                    <input class="pln-payment-box_field-input" type="text" name="liveAddress" value="{{= investInfo.liveAddress}}">
                    <div class="js-error"></div>
                </div>
            </div>
            <div class="pln-payment-box_field-row">
                <div class="pln-payment-box_field-label">Индекс</div>
                <div class="pln-payment-box_field-value">
                    <input class="pln-payment-box_field-input" type="text" name="liveIndex" value="{{= investInfo.liveIndex}}">
                    <div class="js-error"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="pln-payment-box">
        <div class="pln-payment-box_head"> Информация о руководителе </div>
        <div class="pln-payment-box_field-group">
            <div class="pln-payment-box_field-row">
                <div class="pln-payment-box_field-label"> Фамилия </div>
                 <div class="pln-payment-box_field-value"> 
                    <input type="text" name="lastName" class="pln-payment-box_field-input" value="{{= investInfo.lastName}}"> 
                    <div class="js-error"></div>
                 </div>
            </div>
            <div class="pln-payment-box_field-row">
                <div class="pln-payment-box_field-label"> Имя </div>
                <div class="pln-payment-box_field-value"> 
                    <input type="text" name="firstName" class="pln-payment-box_field-input" value="{{= investInfo.firstName}}">
                    <div class="js-error"></div>
                </div>
            </div>
            <div class="pln-payment-box_field-row">
                <div class="pln-payment-box_field-label"> Отчество </div>
                <div class="pln-payment-box_field-value"> 
                    <input type="text" name="middleName" class="pln-payment-box_field-input" value="{{= investInfo.middleName}}"> 
                    <div class="js-error"></div>
                </div>
            </div>
            <div class="pln-payment-box_field-row">
                <div class="pln-payment-box_field-label"> Должность </div>
                <div class="pln-payment-box_field-value"> <input type="text" class="pln-payment-box_field-input" name="bossPosition" value="{{= investInfo.bossPosition}}"> </div>
            </div>
            <div class="pln-payment-box_field-row pln-payment_type-help"> Внимание! Для юридических лиц возможна оплата только по безналичному расчёту. </div>
        </div>
    </div>
</div>
    </script>

<p:script src="biblio-payment.js"></p:script>

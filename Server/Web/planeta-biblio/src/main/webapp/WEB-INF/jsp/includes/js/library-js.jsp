<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/js/share/book.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/js/share/bin.jsp" %>
<p:script src="biblio-library.js"></p:script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAcNhAbtfdB6fkVVs7BoaIoW5HEPjAn4Mw&v=3.31"></script>
<script type="text/javascript">
    $(document).ready(function () {
        // Инициализация корзины
        Biblio.data.bin = new Biblio.Models.Bin();
        Biblio.data.bin.fetch().success(function () {
            new Biblio.Views.LibraryTotalBin({
                model: Biblio.data.bin
            }).render();
            new Biblio.Views.Bin({
                model: Biblio.data.bin
            }).render();

            var addInfo = new BaseModel({books: 0, libs: 0});

            $('.biblio-wizard-head_i.js-second').addClass('disabled');
            addInfo.on('change', function () {
                var needDisable = addInfo.get('books') <= 0 || addInfo.get('libs') <= 0;
                $('.biblio-wizard_btn > button.btn-primary').prop('disabled', needDisable);
                if (needDisable){
                    $('.biblio-wizard-head_i.js-third').addClass('disabled');
                } else {
                    $('.biblio-wizard-head_i.js-third').removeClass('disabled');
                }
            });

            Biblio.data.bin.on('books', function (count) {
                addInfo.set('books', count);
            });

            Biblio.data.bin.on('libs', function (count) {
                addInfo.set('libs', count);
            });

            new Biblio.Views.AddInfoLib({model: addInfo});
            Biblio.data.bin.booksChanged();
            Biblio.data.bin.libsChanged();
        });

        var query = _.object(
                _.compact(_.map(window.location.search.slice(1).split('&'), function (item) {
                    if (item) {
                        return item.split('=');
                    }
                })));

        var filter = new Biblio.Views.LibraryChoose({
            query: decodeURI(query.query||''), 
            model: new (BaseModel.extend())({
                filter: new Biblio.Models.LibraryFilter({state: 1}), 
                libraryCollection: new Biblio.Models.ViewLibraries(), 
                mapCollection: new Biblio.Models.MapLibraries(),
                minZoom: ${properties['biblio.map.minZoomLevel']}
            }), 
            el:'.js-library-choose'
        });        
        filter.render();



        $('.biblio-wizard_btn > .btn-primary').click(function (e) {
            e.preventDefault();
            Biblio.Utils.changeUrl('/payment');
        });
        $('.biblio-wizard_btn > .btn-biblio').click(function (e) {
            e.preventDefault();
            Biblio.Utils.changeUrl('/books');
        });


        $('.js-library-get-random').click(function (e) {
            if (filter.showSuccMessage) {
                return;
            }
            filter.addRandomLibrary().done(function (resp) {

                filter.showSuccMessage = true;
                console.log(resp);
                var name = resp.name;
                var text = "Была выбрана " + name;
                setTimeout(function () {
                    filter.showSuccMessage = false;
                    $('.js-library-get-random').removeClass("hidden");
                    $('.js-library-get-random-temp').addClass("hidden");
                }, 3000);
                $('.js-library-get-random').addClass("hidden");
                $('.js-library-get-random-temp').removeClass("hidden");
                $('.js-library-get-random-temp').find(".biblio-all-library_name").html(text);
            });
        });

    });
</script>

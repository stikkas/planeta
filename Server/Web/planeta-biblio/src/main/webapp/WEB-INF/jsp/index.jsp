<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.rapid-framework.org.cn/rapid" prefix="layout" %> 

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="imagesDir" value="//${hf:getStaticBaseUrl('')}/images"/>

<layout:override name="center-container">  
    <div class="biblio-lead">
        <div class="wrap">
            <div class="wrap-row">
                <div class="col-8">
                    <div class="biblio-lead_head">
                        Хороший журнал в каждую библиотеку!
                    </div>
                    <div class="biblio-lead_text">
                        <p>Все вместе мы&nbsp;решаем проблему многих региональных библиотек, у&nbsp;которых нет денег для подписки на&nbsp;<nobr>научно-популярные</nobr> и&nbsp;образовательные журналы, а&nbsp;также на&nbsp;интеллектуальную литературу. Оказывается, в&nbsp;век Интернета и&nbsp;информационного бума многие полезные книги и&nbsp;журналы недоступны для читателей.</p>
                        <p>Здесь можно сделать доброе дело и&nbsp;стать меценатом. Подарите библиотеке подписку на&nbsp;хороший журнал, получите Сертификат мецената и&nbsp;подарок от&nbsp;издательства. Библиотека может находиться в&nbsp;вашем городе детства, рядом с&nbsp;домом, в&nbsp;котором вы&nbsp;живете, или в&nbsp;школе, в&nbsp;которой вы&nbsp;учились.</p>
                    </div>
                    <div class="biblio-lead_blockquote">
                        <div class="biblio-lead_blockquote_text">
                            &laquo;&hellip;Библиотеки важнее всего в&nbsp;культуре. Может не&nbsp;быть университетов, институтов, других культурных учреждений, но если библиотеки есть&nbsp;&mdash; культура не&nbsp;погибнет в&nbsp;такой стране&raquo;.
                        </div>
                        <div class="biblio-lead_blockquote_author">
                            Академик Д. Лихачев
                        </div>
                    </div>
                    <div class="biblio-lead_share">
                        <div class="biblio-lead_share_lbl">
                            Поделитесь в соцсетях:
                        </div>
                        <div class="biblio-lead_share_val">
                            <div class="sharing-popup-social sharing-mini">
                                <div class="sps-button"></div>
                                <div class="sps-count">Поделились<span>87 чел.</span></div>
                            </div>
                        </div>
                    </div>
                    <div class="biblio-lead_request-link">
                        Вы представляете библиотеку или периодическое издание?
                        <br>
                        <a id="send-request" href="javascript:void(0);">Отправить заявку на участие</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="biblio-main-steps">
        <div class="wrap">
            <div class="biblio-main-steps_cont">
                <div class="biblio-main-steps_list">
                    <div class="biblio-main-steps_i">
                        <div class="biblio-main-steps_head">
                            1-й шаг.
                            <br>
                            Выберите издание
                        </div>
                        <div class="biblio-main-steps_text">
                            Выберите одно или несколько изданий, на&nbsp;которые вы&nbsp;хотели&nbsp;бы оформить подписку в&nbsp;поддержку библиотек. Далее вы&nbsp;сможете перейти к&nbsp;выбору библиотеки.
                        </div>
                    </div>
                    <div class="biblio-main-steps_i">
                        <div class="biblio-main-steps_head">
                            2-й шаг.
                            <br>
                            Выберите библиотеку
                        </div>
                        <div class="biblio-main-steps_text">
                            Выберите на&nbsp;карте библиотеку, которую вы&nbsp;хотите поддержать. Это может быть библиотека в&nbsp;вашем городе детства, рядом с&nbsp;домом, в&nbsp;котором вы&nbsp;живете, или в&nbsp;школе, в&nbsp;которой вы&nbsp;учились.
                        </div>
                    </div>
                    <div class="biblio-main-steps_i">
                        <div class="biblio-main-steps_head">
                            3-й шаг.
                            <br>
                            Оплатите подписку
                        </div>
                        <div class="biblio-main-steps_text">
                            Оплатите выбранные подписки
                        </div>
                    </div>
                </div>
                <div class="biblio-main-steps_action">
                    <a href="/books" class="btn btn-primary">
                        Подарить подписку
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="biblio-stats"><div class="wrap"><div class="js-stat-block"></div></div></div>
    <div class="biblio-main-list">
        <div id="advertises-first"></div>
    </div>
    <div class="biblio-partners"></div>
</layout:override> 

<layout:override name="modal-block">  
    <div id="modal-book-card" class="modal-dialog in" style="display: none"></div>
    <div id="modal-biblio-request" class="modal-dialog in" style="display: none"></div>
    <div id="success-request-modal" class="modal-dialog in" style="display: none"></div>
    <div class="modal-backdrop modal-backdrop-biblio in" style="display: none"></div>
</layout:override>

<layout:override name="js-block">
    <%@ include file="/WEB-INF/jsp/includes/js/index-js.jsp" %>
</layout:override>

<%@ include file="/WEB-INF/jsp/base.jsp" %>
<script>
    <c:if test="${openPopup =='library'}">
        Biblio.openLibrary = true;
    </c:if>
    <c:if test="${openPopup =='book'}">
        Biblio.openBook = true;
    </c:if>
</script>
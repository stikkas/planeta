<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script id="library-basket-item" type="text/x-jquery-template">
    <div class="biblio-library-basket_name">
        {{= name}}
        <span class="biblio-library-basket_del"><span class="s-icon s-icon-close"></span></span>
    </div>
    <div class="biblio-library-basket_delivery">{{= address}}</div>
</script>

<script id="library-basket-list" type="text/x-jquery-template">
</script>

<script id="library-basket-list-more" type="text/x-jquery-template">
    <div class="biblio-library-basket_more">
        <span class="biblio-library-basket_more-link">Показать все</span>
    </div>
</script>

<script id="library-bin" type="text/x-jquery-template">
    <div class="biblio-library-basket_head">Выбранные библиотеки</div>
    <div class="js-library-list biblio-library-basket_list"></div>
    <div class="js-library-list-more"></div>
</script>

<script id="biblio-wizard_library-total" type="text/x-jquery-template">
    <div class="biblio-wizard_library js-library-bin">
    </div>
</script>

<script id="biblio-wizard_total" type="text/x-jquery-template">
    <div class="biblio-wizard_sum">
        <div class="biblio-wizard_sum_lbl">Общая стоимость</div>
        <div class="biblio-wizard_sum_val">{{= StringUtils.humanNumber(totalSum)}}<span class="b-rub"> Р</span></div>
    </div>
    <div class="biblio-wizard-total_books">
        <div class="biblio-books-basket">
            <div class="biblio-books-basket_head">Выбранные издания</div>
            <div class="biblio-books-basket_list"></div>
        </div>
    </div>
</script>


<script id="basket-item" type="text/x-jquery-template">
{{if !Biblio.data.stepThree}}
<div class="biblio-books-basket_del"><span class="s-icon s-icon-close"></span></div>
{{/if}}
{{if count > 1}}
<div class="biblio-books-basket_count">{{= count}}</div>
{{/if}}
<div class="biblio-books-basket_cover-wrap">
    <img class="biblio-books-basket_cover" src="{{= ImageUtils.getThumbnailUrl(book.attributes.imageUrl, ImageUtils.SMALL)}}">
</div>
<div class="biblio-books-basket_info" >
    <div class="biblio-books-basket_name">{{= book.attributes.title}}</div>
    <div class="biblio-books-basket_price">{{= StringUtils.humanNumber(book.attributes.price)}}<span class="b-rub"> Р</span></div>
</div>
</script>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.rapid-framework.org.cn/rapid" prefix="layout" %>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<layout:override name="css-block">
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/payment.css"/>
</layout:override>

<layout:override name="step">
    <c:set var="step" value="third"/>
    <c:set var="step_title" value="3. Оплата подписок"/>
    <c:set var="next_step" value="payment"/>
    <c:set var="prev_step" value="library"/>
    <c:set var="next_step_btn_label" value="Перейти к оплате"/>
    <c:set var="wizard_text" value="Почти получилось!<br>Выберите удобный способ оплаты."/>
</layout:override>

<layout:override name="biblio-wizard-filter_out">
</layout:override> 

<layout:override name="biblio-wizard-payment"> 
<div class="biblio-wizard_payment"></div>
</layout:override> 

<layout:override name="biblio-wizard_cont-part">
</layout:override>

<layout:override name="biblio-wizard_add-info">
</layout:override>

<layout:override name="js-block">
    <%@ include file="/WEB-INF/jsp/includes/js/payment-js.jsp" %> 
</layout:override> 

<%@ include file="/WEB-INF/jsp/library.jsp" %> 

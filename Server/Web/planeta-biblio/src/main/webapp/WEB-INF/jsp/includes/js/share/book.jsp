<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script id="book-card_i" type="text/x-jquery-template">
<div data-target="#modal-book-card" data-toggle="modal" class="book-card_i-cont">
    <div class="book-card_link">
        <div class="book-card_cover-wrap"><img src="{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.MEDIUM)}}" class="book-card_cover"></div>
        <div class="book-card_cont">
            <div class="book-card_name">{{= title}}</div>
            <div class="book-card_price">{{= StringUtils.humanNumber(price)}} <span class="b-rub">Р</span></div>
        </div>
        <div class="book-card_hover">
            <div class="book-card_hover-cont">Подробнее об&nbsp;издании</div>
        </div>
    </div>
    <div class="book-card_counter biblio-counter">
        <div class="biblio-counter_val">
            <div class="biblio-counter_count">0</div>
            <div class="biblio-counter_minus"><span class="s-icon s-icon-minus"></span></div>
            <div class="biblio-counter_plus"><span class="s-icon s-icon-plus"></span></div>
        </div>
    </div>
</div>
</script>

<script id="book-view-template" type="text/x-jquery-template">
<div class="book-view_cover"><img src="{{= ImageUtils.getThumbnailUrl(imageUrl, ImageUtils.MEDIUM)}}" /></div>
<div class="book-view_cont">
    <div class="book-view_name">{{= title}}</div>
    <div class="book-view_info">
        <div class="book-view_info_i">
            <div class="book-view_info_lbl">Периодичность</div>
            <div class="book-view_info_val">{{= periodicity}}</div>
        </div>
        <div class="book-view_info_i">
            <div class="book-view_info_lbl">Бонус для мецената</div>
            <div class="book-view_info_val">{{= bonus}}</div>
        </div>
        <div class="book-view_info_i">
            <div class="book-view_info_lbl">Издательство</div>
            <div class="book-view_info_val">{{= publishingHouseName}}</div>
        </div>
    </div>
    <div class="book-view_descr">{{= description}}</div>
    <div class="book-view_value">
        <div class="book-view_cost"><span></span> <span class="b-rub">Р</span> </div>
        <div class="book-view_count">
            <div class="book-view_count_lbl"> Количество </div>
            <div class="book-view_count_minus"></div>
            <div class="book-view_count_val"><input type="text" value="1" class="form-control"/> </div>
            <div class="book-view_count_plus"></div>
        </div>
    </div>
    <div class="book-view_value-text">{{= comment}}</div>
</div>
<div class="book-view_action">
    <span class="btn btn-biblio book-view_action-left-btn">Выбрать еще издание</span>
    <span class="btn btn-primary">Перейти к выбору библиотеки</span>
</div>
</script>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>


<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/common.css"/>
<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/bibliorodina.css"/>

<!--[if lte IE 9]>
<link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css/placeholder_polyfill.css"/>
<![endif]-->


<%@include file="/WEB-INF/jsp/includes/generated/common-favicon.jsp" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script id="biblio-modal-template" type="text/x-jquery-template">
<div class="modal modal-biblio{{if clazz}} {{= clazz}}{{/if}}">
    <div data-dismiss="modal" class="modal-biblio-close"> 
        <span class="s-icon s-icon-close"></span>
    </div>
    <div class="modal-biblio-head">
        <div class="wrap-row">
            <div class="col-4"> 
                <div class="biblio-logo"></div>
            </div>
            <div class="col-8"> 
                <div class="modal-biblio-head_title">{{= title}}</div>
            </div>
        </div>
    </div>
    <div class="modal-biblio-body"></div>
</div>
</script> 
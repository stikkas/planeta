<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.rapid-framework.org.cn/rapid" prefix="layout" %>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="title" value="Обеспечим библиотеки научными изданиями! | БиблиоРодина"/>

<layout:override name="center-container">  
<div class="wrap">
                <div class="col-12">
                    <div class="biblio-wizard-head">
                        <c:choose>
                            <c:when test="${transaction.status == 'NEW' && !isMobilePayment && !isDeferred}">
                                <div class="biblio-wizard-head_i biblio-wizard-head_i__waiting active">
                                    <div class="biblio-wizard-head_ico">
                                        <span class="s-wizard-waiting"></span>
                                    </div>
                                    <div class="biblio-wizard-head_name">
                                        Ожидание
                                    </div>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="biblio-wizard-head_i biblio-wizard-head_i__success active">
                                    <div class="biblio-wizard-head_ico">
                                        <span class="s-wizard-success"></span>
                                    </div>
                                    <div class="biblio-wizard-head_name">
                                        Успешная оплата
                                    </div>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="biblio-wizard">
                        <div class="biblio-title">
                            <div class="biblio-title_text">Спасибо!</div>
                        </div>
                        <div class="biblio-wizard_text">
                        <c:choose>
                            <c:when test="${transaction.paymentMethodId == 12}">
                                Вам выставлен счет на оплату. <br>
                                <div class="biblio-cert-info_btn">
                                    <a href="https://${properties['application.host']}/welcome/invoice/pdf.html?transactionId=${transaction.transactionId}" class="btn btn-primary">скачать счет</a>
                                </div>
                            </c:when>
                            <c:when test="${transaction.status == 'NEW' && !isMobilePayment && !isDeferred}">
                                Ваш платёж зарегистрирован. Спасибо за вашу поддерку библиотек.
                            </c:when>
                            <c:otherwise>
                            Оплата проведена успешно. На ваш e-mail <b>${email}</b> отправлено письмо с информацией об оформленной подписке в&nbsp;поддержку библиотек.
                            </c:otherwise>
                        </c:choose>
                        </div>
                        <div class="biblio-cert-info">
                            <div class="biblio-cert-info_txt">
                                Информация об оформленных подписках находится в вашем <a href="${redirectUrl}">Личном кабинете</a>
                            </div>
                            <c:choose>
                            <c:when test="${transaction.status == 'NEW' && !isMobilePayment && !isDeferred}">
                            </c:when>
                            <c:otherwise>
                            <div class="biblio-cert-info_btn">
                                <a href="https://${properties['application.host']}/api/welcome/biblio/cert-pdf.html?transactionId=${transaction.transactionId}" class="btn btn-primary">Скачать сертификат</a>
                            </div>
                            </c:otherwise>
                        </c:choose>
                        </div>
                        <div class="biblio-wizard_share">
                            <div class="biblio-wizard_share-head">
                                Поделитесь в соцсетях:
                            </div>
                            <div class="share-cont-horizontal"></div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
</layout:override>

<layout:override name="js-block">  
<script type="text/javascript">
    $(function() {
        var getDataForShare = {
            className: 'horizontal sharing-popup-social sharing-mini',
            counterEnabled: false,
            hidden: false,
            url: 'https://' + workspace.serviceUrls.libraryUrl + '/share.html',
            description: 'Сертификат выдан за личный вклад в поддержку проекта "БиблиоРодина"',
            title: 'Я - меценат!',
            imageUrl: 'https://s3.planeta.ru/i/149a68/1475781538312_renamed.jpg'
        };
        $('.share-cont-horizontal').share(getDataForShare);
    });
    $(document).ready(function() {
        if (window.gtm) {
            var transactionInfo = {
                transactionId: '${transaction.transactionId}',
                amount: '${order.totalPrice}'
            };
            window.gtm.trackPurchaseBiblioBooks(transactionInfo, ${hf:toJson(order.orderObjectsInfo)});
        }
    });
</script>
</layout:override>

<%@ include file="/WEB-INF/jsp/base.jsp" %>
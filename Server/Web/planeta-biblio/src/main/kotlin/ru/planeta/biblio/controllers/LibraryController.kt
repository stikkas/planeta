package ru.planeta.biblio.controllers

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import ru.planeta.api.search.SearchService
import ru.planeta.api.service.biblio.BinService
import ru.planeta.api.service.biblio.LibraryService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.dao.bibliodb.BinDAO
import ru.planeta.model.bibliodb.Library
import ru.planeta.model.bibliodb.LibraryFilter
import ru.planeta.model.bibliodb.enums.BiblioObjectStatus
import ru.planeta.model.bibliodb.enums.LibraryType
import java.util.*

@Controller
class LibraryController(private val libraryService: LibraryService,
                        private val binService: BinService,
                        private val binDAO: BinDAO,
                        private val searchService: SearchService) {


    @Value("\${biblio.map.minZoomLevel}")
    private var minZoomLevel: Long = 0

    @GetMapping(Urls.LIBRARY_SEARCH)
    @ResponseBody
    fun searchLibraries(@RequestParam(required = false) query: String?,
                        @RequestParam(required = false) libraryType: LibraryType?,
                        @RequestParam(defaultValue = "0") offset: Int,
                        @RequestParam(defaultValue = "20") limit: Int): List<Library> {
        val filter = LibraryFilter()
        filter.searchStr = query
        filter.statuses = EnumSet.of(BiblioObjectStatus.ACTIVE)
        filter.libraryType = libraryType
        filter.offset = offset
        filter.limit = limit
        val librarySearchResult = searchService.searchForLibraries(filter)
        return librarySearchResult.searchResultRecords
    }

    @GetMapping(Urls.LIBRARY_MAP_SEARCH)
    @ResponseBody
    fun searchMapLibraries(@RequestParam(required = false) query: String?,
                           @RequestParam(required = false) libraryType: LibraryType?,
                           @RequestParam(defaultValue = "0") north: Double,
                           @RequestParam(defaultValue = "0") south: Double,
                           @RequestParam(defaultValue = "0") east: Double,
                           @RequestParam(defaultValue = "0") west: Double,
                           @RequestParam(defaultValue = "0") zoomLevel: Long,
                           @RequestParam(defaultValue = "0") tagId: Int): List<*> {
        val result: MutableList<*>
        if (east > west) {
            result = libraryService.searchClusterLibraries(query, libraryType, north, south, east, west, zoomLevel, minZoomLevel)
        } else {
            result = libraryService.searchClusterLibraries(query, libraryType, north, south, 180.0, west, zoomLevel, minZoomLevel)
            result.addAll(libraryService.searchClusterLibraries(query, libraryType, north, south, east, -180.0, zoomLevel, minZoomLevel))
        }
        return result
    }

    @GetMapping(Urls.LIBRARY_RANDOM)
    @ResponseBody
    fun randomLibrary(): Library {
        val bin = binService.get(myProfileId(), binDAO)
        return libraryService.randomLibrary(bin.libraries)
    }

}

package ru.planeta.biblio.controllers

import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.MessageSource
import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseBody
import ru.planeta.api.helper.ProjectService
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.service.biblio.BinService
import ru.planeta.api.service.billing.order.BiblioAfterFundService
import ru.planeta.api.service.billing.payment.PaymentService
import ru.planeta.api.service.billing.payment.settings.PaymentSettingsService
import ru.planeta.api.service.common.InvestingOrderInfoService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.service.statistic.OrderShortLinkStatService
import ru.planeta.api.web.authentication.isAnonymous
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.BasePaymentControllerService
import ru.planeta.biblio.controllers.dto.PaymentModel
import ru.planeta.commons.model.Gender
import ru.planeta.commons.model.OrderShortLinkStat
import ru.planeta.commons.web.CookieUtils
import ru.planeta.commons.web.WebUtils
import ru.planeta.dao.bibliodb.BinDAO
import ru.planeta.model.bibliodb.BiblioPurchase
import ru.planeta.model.common.InvestingOrderInfo
import ru.planeta.model.enums.ContractorType
import ru.planeta.model.enums.ModerateStatus
import ru.planeta.model.enums.ProjectType
import java.math.BigDecimal
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid

@Controller
class BiblioPaymentController(private val binService: BinService,
                              private val binDAO: BinDAO,
                              private val investOrderService: InvestingOrderInfoService,
                              private val biblioAfterFundService: BiblioAfterFundService,
                              private val paymentSettingsService: PaymentSettingsService,
                              private val messageSource: MessageSource,
                              private val paymentService: PaymentService,
                              private val projectService: ProjectService,
                              private val profileService: ProfileService,
                              private val basePaymentControllerService: BasePaymentControllerService,
                              private val orderShortLinkStatService: OrderShortLinkStatService) {

    private val logger = Logger.getLogger(BiblioPaymentController::class.java)

    @Value("\${biblio.merchant.id}")
    private var biblioMerchantId: Long = 0

    @GetMapping(Urls.PAYMENT_METHODS)
    @ResponseBody
    fun paymentMethods(): PaymentModel {
        val model = PaymentModel(paymentSettingsService.getPaymentMethods(ProjectType.BIBLIO, false, false))
        var result: InvestingOrderInfo? = investOrderService.select(myProfileId(), ContractorType.LEGAL_ENTITY)
        if (result == null) {
            result = InvestingOrderInfo()
            result.gender = Gender.NOT_SET
            result.userType = ContractorType.LEGAL_ENTITY
        }
        model.investInfo = result
        return model
    }

    @PostMapping(Urls.PAY)
    @ResponseBody
    fun pay(@Valid @RequestBody purchase: BiblioPurchase,
            result: BindingResult, request: HttpServletRequest, response: HttpServletResponse): ActionStatus<*> {
        val wasAnonimous = isAnonymous()
        if (result.hasErrors()) {
            return ActionStatus.createErrorStatus<Any>(result, messageSource)
        }
        val errorsAndUserId = basePaymentControllerService.checkEmailAndGetProfileId(purchase.email, result, request, response)
        if (errorsAndUserId.left.hasErrors()) {
            return ActionStatus.createErrorStatus<Any>(result, messageSource)
        }
        val buyerId = errorsAndUserId.right

        val biblioBin = binService.get(buyerId, binDAO)

        var paymentMethodId = purchase.paymentMethodId
        var amount = purchase.totalSum
        if (purchase.customerType != ContractorType.INDIVIDUAL) {
            amount = purchase.price
        } else {
            profileService.setDisplayName(myProfileId(), buyerId, purchase.fio)
        }
        if (purchase.isFromBalance && paymentMethodId == 0L) {
            logger.error("Should never happen")
            paymentMethodId = paymentSettingsService.internalPaymentMethod.id
        }
        if (paymentMethodId == paymentSettingsService.internalPaymentMethod.id) {
            amount = BigDecimal.ZERO
        }

        val order = biblioAfterFundService.createBiblioOrder(buyerId, buyerId, biblioMerchantId, purchase.price, biblioBin)
        val transaction = paymentService.createOrderPayment(order, amount, paymentMethodId, ProjectType.BIBLIO, purchase.payeerPhone, true)

        try {
            val shortLinkId = CookieUtils.getCookieValue(request.getCookies(), OrderShortLinkStat.SHORTLINK_ID_COOKIE_NAME, "0").toLong()
            if (shortLinkId > 0) {
                val orderShortLinkStat = OrderShortLinkStat()
                orderShortLinkStat.orderId = order.orderId
                orderShortLinkStat.shortLinkId = CookieUtils.getCookieValue(request.getCookies(), OrderShortLinkStat.SHORTLINK_ID_COOKIE_NAME, "0").toLong()
                orderShortLinkStat.referrer = CookieUtils.getCookieValue(request.getCookies(), OrderShortLinkStat.SHORTLINK_REFERRER_COOKIE_NAME, "")
                orderShortLinkStatService.add(orderShortLinkStat)
            }
        } catch (ex: Exception) {
            logger.error("ERROR during trying to store shortlink cookie data to db, orderId = " + order.orderId)
            logger.error(ex)
        }

        val ioi = purchase.investInfo
        if (purchase.customerType != ContractorType.INDIVIDUAL) {
            ioi.investingOrderInfoId = order.orderId
            ioi.moderateStatus = ModerateStatus.APPROVED
            investOrderService.save(ioi)
        }

        var redirectUrl = projectService.getPaymentGateRedirectUrl(transaction)

        binService.clear(biblioBin, binDAO)

        return ActionStatus.createSuccessStatus(redirectUrl)
    }
}

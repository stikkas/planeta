package ru.planeta.biblio.controllers

import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ResponseBody
import ru.planeta.dao.bibliodb.BookDAO
import ru.planeta.dao.bibliodb.LibraryDAO
import ru.planeta.dao.commondb.BiblioOrderStatsDAO
import ru.planeta.model.bibliodb.Statistics
import javax.annotation.PostConstruct

@Controller
class BiblioStatController(private val biblioOrderStatsDAO: BiblioOrderStatsDAO,
                           private val bookDAO: BookDAO,
                           private val libraryDAO: LibraryDAO) {

    @GetMapping(Urls.STATS)
    @ResponseBody
    fun getStatistics(): Statistics = statistics

    @PostConstruct
    @Scheduled(fixedDelay = UPDATE_DELAY)
    fun updateStat() {
        statistics.books = bookDAO.countActiveBooks()
        statistics.libraries = libraryDAO.countActiveLibraries()
        statistics.signedIssues = biblioOrderStatsDAO.purchasedBooksCount()
        statistics.totalSum = biblioOrderStatsDAO.purchasedBooksSum()
    }

    companion object {
        private val statistics = Statistics()
        private const val UPDATE_DELAY = (1000 * 60 * 15).toLong()
    }
}

package ru.planeta.biblio.controllers

import ru.planeta.api.web.controllers.IAction

enum class Actions(override val path: String) : IAction {
    LIBRARY("library"),
    BOOKS("books"),
    SHARE("share"),
    PAYMENT("payment"),
    SUCCESS("success"),
    INDEX("index"),
    PAYMENT_SUCCESS("biblio-success"),
    PAYMENT_FAIL("includes/generated/payment-fail");


    override val actionName: String
        get() = path

    override fun toString(): String = path

}






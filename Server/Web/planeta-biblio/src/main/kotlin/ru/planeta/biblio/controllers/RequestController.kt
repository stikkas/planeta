package ru.planeta.biblio.controllers

import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.service.biblio.RequestService
import ru.planeta.api.service.notifications.NotificationService
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.biblio.model.BookRequest
import ru.planeta.biblio.model.LibraryRequest
import javax.validation.Valid

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 28.04.16<br></br>
 * Time: 14:40
 */
@RestController
class RequestController(private val requestService: RequestService,
                        private val notificationService: NotificationService,
                        private val baseControllerService: BaseControllerService) {

    @PostMapping(Urls.REQUEST_CREATE_LIBRARY)
    fun createRequestLibrary(@Valid @RequestBody library: LibraryRequest, result: BindingResult): ActionStatus<*> {
        if (result.hasErrors()) {
            return baseControllerService.createErrorStatus<Any>(result)
        }
        library.region = ""
        requestService.saveLibraryRequest(library)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @PostMapping(Urls.REQUEST_CREATE_BOOK)
    fun createRequestBook(@Valid @RequestBody book: BookRequest, result: BindingResult): ActionStatus<*> {
        if (result.hasErrors()) {
            return baseControllerService.createErrorStatus<Any>(result)
        }
        requestService.saveBookRequest(book)
        notificationService.sendBiblioBookRequest(book)
        return ActionStatus.createSuccessStatus<Any>()
    }
}

package ru.planeta.biblio.controllers

import org.apache.log4j.Logger
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.service.biblio.BinService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.dao.bibliodb.BinDAO
import ru.planeta.model.enums.ProjectType
import javax.servlet.http.HttpServletRequest

@Controller
class PageController(private val binService: BinService,
                     private val binDAO: BinDAO,
                     private val baseControllerService: BaseControllerService) {

    private val projectType: ProjectType = ProjectType.BIBLIO

    @GetMapping(Urls.INDEX)
    fun indexPage(): ModelAndView = createBiblioModelAndView("biblio.title.index", Actions.INDEX)
            .addObject("openPopup", "")

    @GetMapping(Urls.REQUEST_BOOK)
    fun indexPageBook(): ModelAndView = createBiblioModelAndView("biblio.title.index", Actions.INDEX)
            .addObject("openPopup", "book")

    @GetMapping(Urls.REQUEST_LIBRARY)
    fun indexPageLibrary(): ModelAndView = createBiblioModelAndView("biblio.title.index", Actions.INDEX)
            .addObject("openPopup", "library")

    @GetMapping(Urls.BOOKS)
    fun chooseBooksPage(): ModelAndView = createBiblioModelAndView("biblio.title.books.choose", Actions.BOOKS)

    @GetMapping(Urls.SHARE)
    fun share(request: HttpServletRequest): ModelAndView {
        log.info("userAgent " + request.getHeader("user-agent"))
        return createBiblioModelAndView("biblio.title", Actions.SHARE)
    }


    @GetMapping(Urls.LIBRARY)
    fun chooseLibraryPage(): ModelAndView {
        return if (binService.get(myProfileId(), binDAO).books.isEmpty()) {
            chooseBooksPage()
        } else createBiblioModelAndView("biblio.title.library.choose", Actions.LIBRARY)
    }

    @GetMapping(Urls.PAYMENT, Urls.PAYMENT_UL)
    fun paymentPage(): ModelAndView {
        val bin = binService.get(myProfileId(), binDAO)
        if (bin.books.isEmpty()) {
            return chooseBooksPage()
        }
        return if (bin.libraries.isEmpty()) {
            chooseLibraryPage()
        } else createBiblioModelAndView("biblio.title.payment", Actions.PAYMENT)
    }

    private fun createBiblioModelAndView(title: String, action: Actions): ModelAndView =
            baseControllerService.createDefaultModelAndView(action, projectType)
                    .addObject("title",
                            """${baseControllerService.getParametrizedMessage("biblio.title")}::${baseControllerService.getParametrizedMessage(title)}""")

    companion object {
        private val log = Logger.getLogger(PageController::class.java)
    }
}

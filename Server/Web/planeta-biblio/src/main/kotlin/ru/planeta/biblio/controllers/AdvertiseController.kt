package ru.planeta.biblio.controllers

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.ResponseBody
import ru.planeta.api.model.ConfigurationType
import ru.planeta.api.model.biblio.Advertise
import ru.planeta.api.service.configurations.ConfigurationService

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 27.04.16<br></br>
 * Time: 16:14
 */
@Controller
class AdvertiseController(private val configurationService: ConfigurationService) {

    @GetMapping(Urls.ADVERTISES)
    @ResponseBody
    fun partners(): List<Advertise> {
        return configurationService.getJsonArrayConfig(Advertise::class.java, ConfigurationType.BIBLIO_ADVERTISE_CONFIGURATION_LIST)
    }

}

package ru.planeta.biblio.controllers

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.service.biblio.BookService
import ru.planeta.model.bibliodb.Book
import ru.planeta.model.bibliodb.BookFilter
import ru.planeta.model.bibliodb.BookTag
import ru.planeta.model.bibliodb.enums.BiblioObjectStatus

@RestController
class BookController(private val bookService: BookService) {

    @GetMapping(Urls.BOOKS_INDEX)
    fun indexBooks(): List<Book> = bookService.indexBooks

    @GetMapping(Urls.BOOKS_SEARCH)
    fun searchBooks(@RequestParam(required = false) query: String?,
                    @RequestParam(required = false) tagId: Int?,
                    @RequestParam(defaultValue = "0") limit: Int,
                    @RequestParam(defaultValue = "0") offset: Int): List<Book> {
        val filter = BookFilter()
        filter.tagId = tagId
        filter.searchStr = query
        filter.setStatus(BiblioObjectStatus.ACTIVE)
        filter.offset = offset
        filter.limit = limit
        return bookService.searchBooks(filter)
    }

    @GetMapping(Urls.BOOK_GET)
    fun searchBooks(@RequestParam bookId: Long): Book = bookService.getActiveBookById(bookId)

    @GetMapping(Urls.BOOK_TAGS)
    fun getTags(@RequestParam(defaultValue = "0") limit: Long,
                @RequestParam(defaultValue = "0") offset: Long): List<BookTag> = bookService.getBookTags(limit, offset)

    @PostMapping(Urls.BOOK_EXISTS_TAG_IN_BOOKS)
    fun isBookTagsContainRussianPostTag(@RequestParam(value = "bookIds[]") bookIds: List<Long>): Boolean {
        return if (bookIds.isNotEmpty()) bookService.isBooksContainTag(bookIds, BookTag.RUSSIANPOST) else false
    }

}

package ru.planeta.biblio.controllers

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.service.biblio.BinService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.biblio.controllers.Urls.BIN
import ru.planeta.dao.bibliodb.BinDAO
import ru.planeta.model.bibliodb.Bin

/**
 *
 * @author stikkas<stikkas></stikkas>@yandex.ru>
 */
@RestController
class BinConroller(private val binDAO: BinDAO,
                   private val binService: BinService) {

    @GetMapping(BIN)
    fun bin(): Bin = binService.get(myProfileId(), binDAO)

    @PostMapping(BIN)
    fun saveBin(@RequestBody bin: Bin): ActionStatus<Bin> {
        binService.save(bin, binDAO)
        return ActionStatus.createSuccessStatus(bin)
    }

}

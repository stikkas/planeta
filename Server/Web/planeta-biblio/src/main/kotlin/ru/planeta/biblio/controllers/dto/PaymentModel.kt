package ru.planeta.biblio.controllers.dto

import ru.planeta.model.common.InvestingOrderInfo
import ru.planeta.model.common.PaymentMethod

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 09.06.16<br></br>
 * Time: 16:41
 */
class PaymentModel {

    var investInfo: InvestingOrderInfo? = null
    var methods: List<PaymentMethod>? = null

    constructor(methods: List<PaymentMethod>) {
        this.methods = methods
    }

    constructor() {}

}

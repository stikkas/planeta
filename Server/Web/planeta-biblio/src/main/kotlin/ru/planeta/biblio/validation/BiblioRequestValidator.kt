package ru.planeta.biblio.validation

import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.planeta.api.service.biblio.BookService
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.biblio.model.BookRequest
import ru.planeta.model.bibliodb.Book

/**
 *
 * @author ameshkov
 */
@Component
class BiblioRequestValidator : Validator {

    @Autowired
    private val bookService: BookService? = null

    override fun supports(type: Class<*>): Boolean {
        return type.isAssignableFrom(BookRequest::class.java)
    }

    override fun validate(o: Any, errors: Errors) {
        val request = o as BookRequest

        ValidateUtils.rejectIfContainsNotValidString(errors, "email", "wrong.chars")
        ValidateUtils.rejectIfContainsNotValidString(errors, "site", "wrong.chars")
        ValidateUtils.rejectIfContainsNotValidString(errors, "contact", "wrong.chars")
        ValidateUtils.rejectIfContainsNotValidString(errors, "title", "wrong.chars")

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "field.required")
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "contact", "field.required")
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "field.required")

        if (!ValidateUtils.isValidEmail(request.email)) {
            errors.rejectValue("email", "check.email")
        }

        if (StringUtils.isNotEmpty(request.title) && StringUtils.isNotEmpty(request.email)) {
            if (bookService!!.hasBookRequests(request.title.trim { it <= ' ' }, request.email.trim { it <= ' ' })) {
                errors.rejectValue("title", "biblio.book.exist")
            }
        }
    }
}

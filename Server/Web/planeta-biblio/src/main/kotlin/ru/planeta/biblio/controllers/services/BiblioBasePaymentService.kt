package ru.planeta.biblio.controllers.services

import org.springframework.stereotype.Service
import ru.planeta.api.web.controllers.services.BasePaymentService
import ru.planeta.biblio.controllers.Actions
import ru.planeta.model.common.Order
import ru.planeta.model.common.TopayTransaction

@Service
class BiblioBasePaymentService : BasePaymentService {
    override fun getPaymentSuccessActionName(order: Order?): String = Actions.PAYMENT_SUCCESS.toString()

    override fun getPaymentSourceUrl(transaction: TopayTransaction): String? = null
}
package ru.planeta.biblio.validation

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.util.StringUtils
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.planeta.api.service.biblio.LibraryService
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.biblio.model.LibraryRequest
import ru.planeta.model.bibliodb.Library

/**
 *
 * @author Alexey
 */
@Component
class BiblioRequestLibraryValidator(private val libraryService: LibraryService) : Validator {


    override fun supports(type: Class<*>): Boolean {
        return type.isAssignableFrom(LibraryRequest::class.java)
    }

    override fun validate(o: Any, errors: Errors) {
        val library = o as LibraryRequest

        ValidateUtils.rejectIfContainsNotValidString(errors, "name", "wrong.chars")
        ValidateUtils.rejectIfContainsNotValidString(errors, "address", "wrong.chars")
        ValidateUtils.rejectIfContainsNotValidString(errors, "postIndex", "wrong.chars")
        ValidateUtils.rejectIfContainsNotValidString(errors, "contractor", "wrong.chars")
        ValidateUtils.rejectIfContainsNotValidString(errors, "email", "wrong.chars")
        ValidateUtils.rejectIfContainsNotValidString(errors, "site", "wrong.chars")

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "field.required")
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address", "field.required")
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "postIndex", "field.required")
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "contractor", "field.required")
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "field.required")
        ValidationUtils.rejectIfEmpty(errors, "libraryType", "field.required")

        if (!StringUtils.isEmpty(library.name)) {
            if (libraryService.hasLibraryRequests(library.name.trim(' '), library.email.trim(' '))) {
                errors.rejectValue("name", "biblio.library.exist")
            }
        }

        if (!ValidateUtils.isValidEmail(library.email)) {
            errors.rejectValue("email", "check.email")
        }
    }
}

package ru.planeta.biblio.controllers

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import ru.planeta.api.model.ConfigurationType
import ru.planeta.api.model.biblio.Partner
import ru.planeta.api.service.configurations.ConfigurationService

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 27.04.16<br></br>
 * Time: 16:14
 */
@RestController
class PartnerController(private val configurationService: ConfigurationService) {

    @GetMapping(Urls.PARTNERS)
    fun partners(): List<Partner> =
            configurationService.getJsonArrayConfig(Partner::class.java, ConfigurationType.BIBLIO_PARTNERS_CONFIGURATION_LIST)

}

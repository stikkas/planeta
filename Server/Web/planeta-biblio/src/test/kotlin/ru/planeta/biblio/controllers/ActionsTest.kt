/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.planeta.biblio.controllers

import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.test.context.junit4.SpringRunner

/**
 *
 * @author Serge Blagodatskih<stikkas17></stikkas17>@gmail.com>
 */
@RunWith(SpringRunner::class)
class ActionsTest {

    @Test
    fun testValues() {
        assertEquals("index", Actions.INDEX.path)
        assertEquals("library", Actions.LIBRARY.path)
        assertEquals("books", Actions.BOOKS.path)
        assertEquals("success", Actions.SUCCESS.path)
        assertEquals("payment", Actions.PAYMENT.path)
    }

}

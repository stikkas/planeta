/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.planeta.biblio.controllers

import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*

/**
 *
 * @author Serge Blagodatskih<stikkas17></stikkas17>@gmail.com>
 */
@Ignore
@RunWith(SpringRunner::class)
@WebMvcTest(PageController::class)
@ContextConfiguration(locations = ["classpath*:/spring/applicationContext-*.xml"])
class LibraryControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Value("\${biblio.title.index}")
    lateinit var indexTitle: String

    @Value("\${biblio.title.books.choose}")
    lateinit var booksChooseTitle: String

    @Value("\${biblio.title.library.choose}")
    lateinit var libraryChooseTitle: String

    @Value("\${biblio.title.payment}")
    lateinit var paymentTitle: String

    @Value("\${biblio.title.success}")
    lateinit var successTitle: String


    @Test
    fun testIndex() {
        mockMvc.perform(get(Urls.INDEX))
                .andExpect(status().isOk)
                .andExpect(view().name(Actions.INDEX.path))
                .andExpect(model().attribute("title", "Обеспечим библиотеки научными изданиями! | БиблиоРодина::$indexTitle"))

        mockMvc.perform(get(Urls.ANY))
                .andExpect(status().is4xxClientError)

    }

    @Test
    fun testLibraryChoose() {
        mockMvc.perform(get(Urls.LIBRARY))
                .andExpect(status().isOk)
                .andExpect(view().name(Actions.BOOKS.path))
                .andExpect(model().attribute("title", "Обеспечим библиотеки научными изданиями! | БиблиоРодина::$booksChooseTitle"))
    }

    @Test
    fun testBooksChoose() {
        mockMvc.perform(get(Urls.BOOKS))
                .andExpect(status().isOk)
                .andExpect(view().name(Actions.BOOKS.path))
                .andExpect(model().attribute("title", "Обеспечим библиотеки научными изданиями! | БиблиоРодина::$booksChooseTitle"))
    }

    @Test
    fun testPayment() {
        mockMvc.perform(get(Urls.PAYMENT))
                .andExpect(status().isOk)
                .andExpect(view().name(Actions.BOOKS.path))
                .andExpect(model().attribute("title", "Обеспечим библиотеки научными изданиями! | БиблиоРодина::$booksChooseTitle"))
    }

    @Test
    fun testSearchLibraries() {
        mockMvc.perform(post(Urls.LIBRARY_SEARCH))
                .andExpect(status().is4xxClientError)
    }


}

package ru.planeta.shop.model.jaxb.yml.categories

import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlValue

/**
 * User: sshendyapin
 * Date: 05.07.13
 * Time: 16:31
 */
class Category {

    @XmlAttribute
    private var id: Long? = null
    @XmlValue
    private var value: String? = null

    constructor() {}

    constructor(category: ru.planeta.model.shop.Category) {
        this.id = category.categoryId
        this.value = category.value
    }
}

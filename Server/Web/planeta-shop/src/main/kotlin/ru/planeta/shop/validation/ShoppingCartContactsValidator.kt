package ru.planeta.shop.validation

import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.AnonymousAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.planeta.api.model.authentication.AuthenticatedUserDetails
import ru.planeta.api.model.shop.ShoppingCartContactsDTO
import ru.planeta.api.service.profile.AuthorizationService
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.model.enums.EmailStatus
import ru.planeta.model.shop.enums.DeliveryType

/**
 * Created with IntelliJ IDEA.
 * User: michail
 * Date: 30.08.16
 * Time: 15:42
 */

@Component
class ShoppingCartContactsValidator : Validator {

    @Autowired
    protected lateinit var authorizationService: AuthorizationService

    override fun supports(aClass: Class<*>): Boolean {
        return ShoppingCartContactsDTO::class.java.isAssignableFrom(aClass)
    }

    override fun validate(o: Any?, errors: Errors) {
        val contacts = o as? ShoppingCartContactsDTO
        if (contacts == null) {
            errors.rejectValue("customerName", "field.required")
            return
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customerName", "field.required")
        ValidateUtils.rejectIfContainsNotValidString(errors, "customerName", "wrong.chars")
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phoneNumber", "field.required")
        ValidateUtils.rejectIfContainsNotValidString(errors, "phoneNumber", "wrong.chars")


        if (StringUtils.isEmpty(contacts.email)) {
            errors.rejectValue("email", "field.required")
        }
        if (!ValidateUtils.isValidEmail(contacts.email)) {
            errors.rejectValue("email", "wrong.email")
        }

        val securityContext = SecurityContextHolder.getContext()
        val emailStatus = authorizationService.checkEmail(contacts.email)
        if (securityContext != null) {
            val authentication = securityContext.authentication
            if (authentication is AnonymousAuthenticationToken) {
                if (emailStatus == EmailStatus.ALREADY_EXIST) {
                    errors.rejectValue("email", "registration.current.email.already.exists")
                }
            } else {
                val authenticatedUserDetails = authentication.principal as AuthenticatedUserDetails
                val email = authenticatedUserDetails.userAuthorizationInfo.userPrivateInfo.email
                if (StringUtils.isEmpty(email)) {
                    if (emailStatus == EmailStatus.ALREADY_EXIST) {
                        errors.rejectValue("email", "registration.current.email.already.exists")
                    }
                }
            }
        }

        if (!contacts.onlyDigitalProducts) {
            if (contacts.deliveryType == DeliveryType.NOT_SET) {
                errors.rejectValue("deliveryType", "field.required")
            }

            if (contacts.deliveryType != DeliveryType.CUSTOMER_PICKUP) {
                if (contacts.customerContacts == null) {
                    errors.rejectValue("deliveryType", "delivery.required")
                    return
                }
                if (contacts.customerContacts.countryId == 0) {
                    errors.rejectValue("customerContacts.countryId", "field.required")
                }
                ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customerContacts.city", "field.required")
                ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customerContacts.street", "field.required")
                ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customerContacts.zipCode", "field.required")
            }
        }
    }
}

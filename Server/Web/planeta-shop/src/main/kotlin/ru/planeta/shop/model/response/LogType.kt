package ru.planeta.shop.model.response

enum class LogType private constructor(val value: Int) {
    CONTRACTOR(1),
    NOMENCLATURE(2),
    NOMENCLATURE_GROUP(3),
    ORDER(4)
}

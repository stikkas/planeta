package ru.planeta.shop.model.jaxb.yml.currencies

import javax.xml.bind.annotation.XmlElement
import java.util.LinkedList

/**
 * User: sshendyapin
 * Date: 05.07.13
 * Time: 16:30
 */
class Currencies {

    @XmlElement(name = "currency")
    private var currencyList: List<Currency>? = null

    /**
     * this is default constructor, that will create Currencies with only RUB Currency
     */
    constructor() {
        val currencies = LinkedList<Currency>()
        currencies.add(Currency(1))
        this.currencyList = currencies
    }

    constructor(curList: List<Currency>) {
        this.currencyList = curList
    }

}


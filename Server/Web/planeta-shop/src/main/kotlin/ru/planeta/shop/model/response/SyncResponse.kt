package ru.planeta.shop.model.response

class SyncResponse {

    var id: Long = 0
    var syncId: Long = 0
    var logType: LogType? = null
    var importStatus: ImportStatusType? = null
    var importError: String? = null
    var importIndex: Long = 0

    constructor() {}

    constructor(id: Long, syncId: Long, logType: LogType, importStatus: ImportStatusType, importError: String, importIndex: Long) {
        this.id = id
        this.syncId = syncId
        this.logType = logType
        this.importStatus = importStatus
        this.importError = importError
        this.importIndex = importIndex
    }
}

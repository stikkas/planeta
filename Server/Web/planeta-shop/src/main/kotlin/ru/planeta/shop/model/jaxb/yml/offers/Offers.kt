package ru.planeta.shop.model.jaxb.yml.offers

import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import ru.planeta.dao.shopdb.ProductAttributeDAO
import ru.planeta.model.shop.enums.ProductStatus
import ru.planeta.model.shop.Product
import ru.planeta.shop.model.jaxb.yml.currencies.Currency

import javax.xml.bind.annotation.XmlElement
import java.util.LinkedList

/**
 * User: sshendyapin
 * Date: 05.07.13
 * Time: 16:31
 */
class Offers {

    @Autowired
    lateinit var productAttributeDAO: ProductAttributeDAO

    @XmlElement(name = "offer")
    private var offerList: List<Offer>? = null

    constructor() {}

    constructor(products: List<Product>) {
        this.offerList = products.mapTo(LinkedList()) { fillOfferFromProduct(it) }
    }

    private fun fillOfferFromProduct(product: Product): Offer {
        val offer = Offer()
        offer.setOfferId(product.productId)
        offer.setUrl(HTTP_SHOP_PLANETA_RU_PRODUCTS + product.productId)
        offer.setPrice(product.price)
        offer.setCurrencyId(Currency.CURRENCY_RUB)
        offer.setCategoryId(product.tags[0].categoryId)
        offer.setDescription(StringUtils.abbreviate(product.description, 512))
        offer.setName(StringUtils.abbreviate(product.name, 255))
        offer.setPicture(product.coverImageUrl)
        offer.setDelivery(true)
        offer.setAvailable(product.productStatus == ProductStatus.ACTIVE)
        return offer
    }

    companion object {

        const val HTTP_SHOP_PLANETA_RU_PRODUCTS = "http://shop.planeta.ru/products/"
    }
}

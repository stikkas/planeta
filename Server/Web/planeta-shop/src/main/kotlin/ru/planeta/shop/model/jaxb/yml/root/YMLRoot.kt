package ru.planeta.shop.model.jaxb.yml.root

import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement
import java.text.SimpleDateFormat
import java.util.Date

/**
 * User: sshendyapin
 * Date: 08.07.13
 * Time: 17:26
 */
@XmlRootElement(name = "yml_catalog")
class YMLRoot {

    @XmlAttribute
    private var date: String? = null
    @XmlElement(name = "shop")
    private var shopXml: ShopXml? = null

    fun setDate(date: String) {
        this.date = date
    }

    fun setShopXml(shopXml: ShopXml) {
        this.shopXml = shopXml
    }

    constructor() {}

    constructor(date: Date, shopXml: ShopXml) {
        this.date = SimpleDateFormat("yyyy-MM-dd HH:mm").format(date)
        this.shopXml = shopXml
    }
}

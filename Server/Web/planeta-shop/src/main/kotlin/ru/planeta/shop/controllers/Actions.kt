package ru.planeta.shop.controllers

import ru.planeta.api.web.controllers.IAction

enum class Actions(private val text: String, prefix: Prefixes = Prefixes.EMPTY) : IAction {

    WELCOME("welcome"),
    CONTACTS("contacts"),
    PAYMENT("payment"),
    DELIVERY("delivery"),
    EXCHANGE("exchange"),
    FAQ("faq"),
    OFFER("offer"),
    SEARCH_PRODUCTS("search-products"),
    PRODUCT("product"),
    SHOPPING_CART("shopping-cart"),
    SHOPPING_CART_CONTACTS("shopping-cart-contacts"),
    SHOPPING_CART_CONFIRM("shopping-cart-confirm"),
    SHOPPING_CART_PAYMENT("shopping-cart-payment"),
    PAYMENT_SUCCESS("payment-success"),
    PAYMENT_FAIL("payment-fail", Prefixes.COMMON_INCLUDES);

    override val path: String
        get() = prefix + text

    override val actionName: String
        get() = text

    private val prefix: String = prefix.text

    override fun toString(): String = path


    private enum class Prefixes(val text: String) {
        COMMON_INCLUDES("includes/generated/"),
        EMPTY("")
    }

}





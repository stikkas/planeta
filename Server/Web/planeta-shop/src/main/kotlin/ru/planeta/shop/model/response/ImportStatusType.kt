package ru.planeta.shop.model.response

import java.util.HashMap

enum class ImportStatusType private constructor(val value: Int) {

    OK(1),
    ERROR(2);


    companion object {

        private val lookup = HashMap<Int, ImportStatusType>()

        init {
            for (s in values()) {
                lookup.put(s.value, s)
            }
        }

        fun getByValue(code: Int): ImportStatusType? {
            return lookup[code]
        }
    }
}

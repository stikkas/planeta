package ru.planeta.shop.model.jaxb.yml.offers

import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement
import java.math.BigDecimal

/**
 * User: sshendyapin
 * Date: 05.07.13
 * Time: 16:31
 */
class Offer {

    @XmlAttribute
    private var available: Boolean = false
    @XmlAttribute(name = "id")
    private var offerId: Long = 0
    @XmlElement
    private var url: String? = null
    @XmlElement
    private var price: BigDecimal? = null
    @XmlElement
    private var currencyId: String? = null
    @XmlElement
    private var categoryId: Long = 0
    @XmlElement
    private var picture: String? = null
    @XmlElement
    private var delivery: Boolean = false
    @XmlElement
    private var name: String? = null
    @XmlElement
    private var description: String? = null

    fun setAvailable(available: Boolean) {
        this.available = available
    }


    fun setOfferId(offerId: Long) {
        this.offerId = offerId
    }


    fun setUrl(url: String) {
        this.url = url
    }


    fun setPrice(price: BigDecimal) {
        this.price = price
    }


    fun setCurrencyId(currencyId: String) {
        this.currencyId = currencyId
    }


    fun setCategoryId(categoryId: Long) {
        this.categoryId = categoryId
    }

    fun setPicture(picture: String) {
        this.picture = picture
    }


    fun setDelivery(delivery: Boolean) {
        this.delivery = delivery
    }

    fun setName(name: String) {
        this.name = name
    }

    fun setDescription(description: String) {
        this.description = description
    }
}

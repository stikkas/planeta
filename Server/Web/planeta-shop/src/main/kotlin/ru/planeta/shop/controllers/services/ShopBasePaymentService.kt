package ru.planeta.shop.controllers.services

import org.springframework.stereotype.Service
import ru.planeta.api.web.controllers.services.BasePaymentService
import ru.planeta.model.common.Order
import ru.planeta.model.common.TopayTransaction
import ru.planeta.shop.controllers.Actions
import ru.planeta.shop.controllers.Urls

@Service
class ShopBasePaymentService : BasePaymentService {
    override fun getPaymentSuccessActionName(order: Order?): String = Actions.PAYMENT_SUCCESS.path
    override fun getPaymentSourceUrl(transaction: TopayTransaction): String? = Urls.SHOPPING_CART
}
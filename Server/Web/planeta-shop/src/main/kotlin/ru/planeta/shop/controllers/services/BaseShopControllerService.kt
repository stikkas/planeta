package ru.planeta.shop.controllers.services

import org.springframework.stereotype.Service
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.service.shop.ProductTagService
import ru.planeta.api.web.controllers.IAction
import ru.planeta.api.web.controllers.services.BaseControllerService
import ru.planeta.model.enums.ProjectType

/**
 * Base controller for shop
 *
 * @author m.shulepov
 * Date: 12.04.13
 */
@Service
class BaseShopControllerService(val productTagService: ProductTagService,
                                val baseControllerService: BaseControllerService)  {

    fun createDefaultModelAndView(action: IAction): ModelAndView =
            baseControllerService.createDefaultModelAndView(action, ProjectType.SHOP)
                    .addObject("productTags", productTagService.getProductTags(0, 0))

    companion object {
        const val MAX_VISIBLE_OTHER_PRODUCTS_COUNT = 6
    }
}


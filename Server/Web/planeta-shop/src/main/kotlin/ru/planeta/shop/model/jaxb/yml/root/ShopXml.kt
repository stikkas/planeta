package ru.planeta.shop.model.jaxb.yml.root

import ru.planeta.shop.model.jaxb.yml.categories.Categories
import ru.planeta.shop.model.jaxb.yml.currencies.Currencies
import ru.planeta.shop.model.jaxb.yml.offers.Offers

import javax.xml.bind.annotation.XmlType

/**
 * User: sshendyapin
 * Date: 05.07.13
 * Time: 16:19
 */

@XmlType(propOrder = ["name", "company", "url", "currencies", "categories", "offers"])
class ShopXml {

    var currencies: Currencies? = null
    var categories: Categories? = null
    var offers: Offers? = null
    var name = PLANETA_SHOP_NAME
    var company = PLANETA_COMPANY_NAME
    var url = PLANETA_SHOP_URL

    constructor() {}

    constructor(currencies: Currencies, categories: Categories, offers: Offers) {
        this.currencies = currencies
        this.categories = categories
        this.offers = offers
    }

    companion object {
        private const val PLANETA_SHOP_NAME = "Магазин Planeta"
        private const val PLANETA_COMPANY_NAME = "Портал «Планета»"
        private const val PLANETA_SHOP_URL = "https://shop.planeta.ru"
    }
}

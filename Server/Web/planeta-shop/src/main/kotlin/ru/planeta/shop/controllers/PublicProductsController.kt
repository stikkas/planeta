package ru.planeta.shop.controllers

import org.apache.commons.collections4.IterableUtils
import org.apache.log4j.Logger
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.search.ProductsSearch
import ru.planeta.api.search.SearchService
import ru.planeta.api.service.configurations.ConfigurationService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.service.security.PermissionService
import ru.planeta.api.service.shop.ProductTagService
import ru.planeta.api.service.shop.ProductUsersService
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.model.profile.Profile
import ru.planeta.model.shop.Category
import ru.planeta.model.shop.Product
import ru.planeta.model.shop.ProductAttributeType
import ru.planeta.model.shop.enums.ProductState
import ru.planeta.model.shop.enums.ProductState.ATTRIBUTE
import ru.planeta.model.shop.enums.ProductStatus
import ru.planeta.shop.controllers.services.BaseShopControllerService

/**
 * Public zone products controller
 */
@Controller
class PublicProductsController(private val productUsersService: ProductUsersService,
                               private val profileService: ProfileService,
                               private val permissionService: PermissionService,
                               private val searchService: SearchService,
                               private val configurationService: ConfigurationService,
                               private val productTagService: ProductTagService,
                               private val baseShopControllerService: BaseShopControllerService) {

    private val logger = Logger.getLogger(PublicProductsController::class.java)

    @GetMapping(Urls.PRODUCT)
    fun productView(@PathVariable("productId") productId: Long): ModelAndView {
        val myProfileId = myProfileId()
        val product = productUsersService.getProductShortCurrent(myProfileId, productId) ?:
                return baseShopControllerService.baseControllerService.createRedirectModelAndView(Urls.ROOT)

        val parentProductId = product.parentProductId
        if (isProductInState(ATTRIBUTE, product)) {
            return baseShopControllerService.baseControllerService
                    .createRedirectModelAndView(Urls.PRODUCT.replace("{productId}", parentProductId.toString()))
        }

        val productInfo = productUsersService.getProductCurrent(myProfileId, product.productId)

        if (productInfo.referrerId > 0) {
            try {
                productInfo.referrer = profileService.getProfileSafe(productInfo.referrerId)
            } catch (ex: NotFoundException) {
                logger.error(ex)
            }

        }

        if (productInfo.donateId > 0) {
            try {
                productInfo.donate = productUsersService.getProductSafe(productInfo.donateId)
            } catch (ex: NotFoundException) {
                logger.error(ex)
            }

        }

        if (productInfo.productStatus == ProductStatus.PAUSE && !permissionService.hasAdministrativeRole(myProfileId)) {
            return baseShopControllerService
                    .baseControllerService.createRedirectModelAndView(Urls.ROOT)
        }

        val mav = baseShopControllerService.createDefaultModelAndView(Actions.PRODUCT)
                .addObject("attributeType", ProductAttributeType.getDefault(0))
                .addObject("maxVisibleOtherProductsCount", BaseShopControllerService.MAX_VISIBLE_OTHER_PRODUCTS_COUNT)
                .addObject("otherProducts", productUsersService.getSimilarProducts(product.productId,
                        BaseShopControllerService.MAX_VISIBLE_OTHER_PRODUCTS_COUNT))

        val noRussianPostDelivery = IterableUtils.matchesAny(product.tags) { category -> category.mnemonicName == Category.NO_RUSSIAN_POST_CATEGORY }

        val noFreeDelivery = IterableUtils.matchesAny(product.tags) { category -> category.mnemonicName == Category.NO_FREE_DELIVERY }

        return mav.addObject("noRussianPostDelivery", noRussianPostDelivery)
                .addObject("noFreeDelivery", noFreeDelivery)
                .addObject("productInfo", productInfo)
                .addObject("productCategoryAttribute", ProductAttributeType.getDefault(0))
    }

    @RequestMapping(Urls.SEARCH_PRODUCTS)
    fun searchProducts(productsSearch: ProductsSearch): ModelAndView {
        val modelAndView = baseShopControllerService.createDefaultModelAndView(Actions.SEARCH_PRODUCTS)
        productsSearch.productTags = productTagService.getTagByMnemonicNameList(productsSearch.productTagsMnemonics)
        val products = searchService.searchForProductsNew(productsSearch)

        val mainTag: Category
        if (productsSearch.productTags != null && !productsSearch.productTags.isEmpty()) {
            mainTag = productsSearch.productTags[0]
            modelAndView.addObject("mainTag", mainTag)
        } else {
            mainTag = Category("Все товары", 0, "ALL")
            modelAndView.addObject("mainTag", mainTag)
        }

        val referrers: List<Profile>
        if (mainTag.mnemonicName == "CHARITY") {
            referrers = profileService.getProfiles(configurationService.shopFundsIds)
        } else {
            referrers = profileService.getShopProductsProfiles(mainTag.categoryId, 100)
        }

        return modelAndView.addObject("productsSearch", productsSearch)
                .addObject("products", products)
                .addObject("referrers", referrers)
    }

    @GetMapping(Urls.PRODUCTS_LIST)
    fun productsView(): ModelAndView = baseShopControllerService
            .baseControllerService.createPermanentRedirectModelAndView(Urls.ROOT)

    @GetMapping(Urls.ROOT)
    fun welcomePage(): ModelAndView = baseShopControllerService.createDefaultModelAndView(Actions.WELCOME)

    @GetMapping(Urls.REFERRER_LIST_JSON)
    @ResponseBody
    fun getReferrersListBySubstring(@RequestParam(required = false) mnemonicName: String,
                                    @RequestParam(defaultValue = "20") limit: Int): List<Profile> {
        val referrers: List<Profile>
        if ("CHARITY" == mnemonicName) {
            referrers = profileService.getProfiles(configurationService.shopFundsIds)
        } else {
            var tagId: Long = 0
            if ("ALL" != mnemonicName) {
                tagId = productTagService.getTagByMnemonicName(mnemonicName).categoryId
            }

            referrers = profileService.getShopProductsProfiles(tagId, limit)
        }
        return referrers
    }

    companion object {
        private fun isProductInState(state: ProductState?, product: Product?): Boolean = product != null && state != null && state == product.productState
    }

}

package ru.planeta.shop.controllers

/**
 * Contains shop web app urls
 * User: atropnikov
 * Date: 21.03.12
 * Time: 16:07
 */
object Urls {

    const val ROOT = "/"

    const val CONTACTS = "/contacts.html"
    const val PAYMENT = "/payment.html"
    const val DELIVERY = "/delivery.html"
    const val EXCHANGE = "/exchange.html"
    const val FAQ = "/faq.html"
    const val OFFER = "/offer.html"

    const val PRODUCTS_LIST = "/products"
    const val PRODUCT = "/products/{productId}"
    const val DOWNLOAD_PRODUCT = "/products/download/{orderObjectId}"
    const val SEARCH_PRODUCTS = "/products/search.html"

    const val REFERRER_LIST_JSON = "/api/util/referrer-list.json"

    const val SHOPPING_CART = "/payment/shopping-cart"
    const val REMOVE_FROM_SHOPPING_CART = "/payment/remove-from-shopping-cart.json"
    const val CHANGE_QUANTITY_IN_SHOPPING_CART = "/payment/change-quantity-in-shopping-cart.json"
    const val CLEAR_SHOPPING_CART = "/payment/clear-shopping-cart.json"
    const val GET_SHOPPING_CART = "/payment/get-shopping-cart.json"
    const val SHOPPING_CART_PURCHASING_CONTACTS = "/payment/shopping-cart-contacts"
    const val SHOPPING_CART_PURCHASING_CONTACTS_JSON = "/payment/shopping-cart-contacts.json"
    const val SHOPPING_CART_PURCHASING_PAYMENT = "/payment/shopping-cart-payment"
    const val SHOPPING_CART_PURCHASING_PAYMENT_JSON = "/payment/shopping-cart-payment.json"
    const val SHOPPING_CART_PURCHASING_ORDER = "/payment/shopping-cart-order"

    const val SHOPPING_CART_PURCHASING_CONFIRM = "/payment/shopping-cart-confirm"

    const val CHECK_PROMO_CODE = "/api/public/check-promo-code.json"

}

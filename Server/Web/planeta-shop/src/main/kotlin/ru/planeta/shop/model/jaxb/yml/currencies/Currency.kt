package ru.planeta.shop.model.jaxb.yml.currencies

import javax.xml.bind.annotation.XmlAttribute

/**
 * User: sshendyapin
 * Date: 05.07.13
 * Time: 16:30
 */
class Currency {

    @XmlAttribute(name = "id")
    val currencyId = CURRENCY_RUB
    @XmlAttribute
    private var rate: Int = 0

    fun setRate(rate: Int) {
        this.rate = rate
    }

    constructor() {}

    constructor(rate: Int) {
        this.rate = rate
    }

    companion object {
        val CURRENCY_RUB = "RUB"
    }
}

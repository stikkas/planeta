package ru.planeta.shop.controllers

import org.apache.commons.collections4.IterableUtils
import org.apache.commons.lang3.StringUtils
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.MessageSource
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import ru.planeta.api.aspect.logging.BillingLoggableRequest
import ru.planeta.api.exceptions.MessageCode
import ru.planeta.api.exceptions.PermissionException
import ru.planeta.api.exceptions.RegistrationException
import ru.planeta.api.helper.ProjectService
import ru.planeta.api.model.ActionStatus
import ru.planeta.api.model.json.DeliveryInfo
import ru.planeta.api.model.shop.ShoppingCartContactsDTO
import ru.planeta.api.model.shop.ShoppingCartPaymentDTO
import ru.planeta.api.service.billing.order.ProductAfterFundService
import ru.planeta.api.service.billing.payment.PaymentService
import ru.planeta.api.service.billing.payment.settings.PaymentSettingsService
import ru.planeta.api.service.campaign.DeliveryService
import ru.planeta.api.service.geo.DeliveryAddressService
import ru.planeta.api.service.geo.GeoService
import ru.planeta.api.service.profile.ProfileBalanceService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.api.service.shop.CartService
import ru.planeta.api.service.shop.ProductUsersService
import ru.planeta.api.service.shop.PromoCodeService
import ru.planeta.api.service.statistic.OrderShortLinkStatService
import ru.planeta.api.web.authentication.isAnonymous
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.api.web.controllers.services.BasePaymentControllerService
import ru.planeta.api.web.controllers.services.ControllerAutoLoginWrapService
import ru.planeta.commons.web.CookieUtils
import ru.planeta.commons.web.IpUtils
import ru.planeta.dao.SessionDAO
import ru.planeta.model.common.Address
import ru.planeta.model.common.Order
import ru.planeta.model.common.TopayTransaction
import ru.planeta.model.common.delivery.LinkedDelivery
import ru.planeta.model.common.delivery.SubjectType
import ru.planeta.model.enums.ProjectType
import ru.planeta.model.profile.location.LocationType
import ru.planeta.model.shop.Category
import ru.planeta.model.shop.Discount
import ru.planeta.model.shop.Product
import ru.planeta.model.shop.enums.DeliveryType
import ru.planeta.model.shop.enums.PaymentType
import ru.planeta.commons.model.OrderShortLinkStat
import ru.planeta.shop.controllers.services.BaseShopControllerService
import ru.planeta.shop.model.ShoppingCart
import java.math.BigDecimal
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid
import kotlin.collections.HashMap

/**
 * Shopping cart controller
 */
@Controller
class ShoppingCartController(private val deliveryAddressService: DeliveryAddressService,
                             private val profileBalanceService: ProfileBalanceService,
                             private val promoCodeService: PromoCodeService,
                             private val sessionDAO: SessionDAO,
                             private val cartService: CartService,
                             private val productAfterFundService: ProductAfterFundService,
                             private val messageSource: MessageSource,
                             private val productUsersService: ProductUsersService,
                             private val shoppingCart: ShoppingCart,
                             private val profileService: ProfileService,
                             private val paymentSettingsService: PaymentSettingsService,
                             private val deliveryService: DeliveryService,
                             private val paymentService: PaymentService,
                             private val projectService: ProjectService,
                             @Autowired(required = false)
                             private val geoService: GeoService?,
                             private val baseShopControllerService: BaseShopControllerService,
                             private val controllerAutoLoginWrapService: ControllerAutoLoginWrapService,
                             private val basePaymentControllerService: BasePaymentControllerService,
                             private val orderShortLinkStatService: OrderShortLinkStatService) {

    private var clientIpAddressesMap: HashMap<String, Int> = HashMap()


    @GetMapping(Urls.GET_SHOPPING_CART)
    @ResponseBody
    fun shoppingCartItems(): ActionStatus<*> {
        val result = shoppingCart
                .getItemsMapCopy(cartService)
                .map {
                    productUsersService.cartItemToDTO(it.key, it.value)
                }
        return ActionStatus.createSuccessStatus<List<Map<String, Any>>>(result)
    }

    @Scheduled(fixedDelay = MAP_CLEANUP_DELAY.toLong())
    fun clientIpAddressesMapCleanup() = clientIpAddressesMap.clear()

    @RequestMapping(Urls.CHECK_PROMO_CODE)
    @ResponseBody
    fun checkPromoCode(@RequestParam(value = "promoCode") code: String, request: HttpServletRequest): ActionStatus<Discount> {
        val clientAddr = IpUtils.getRemoteAddr(request)
        logger.warn("Client with ip " + clientAddr + "checked promocode " + code)
        var requestCount: Int? = clientIpAddressesMap[clientAddr]
        if (requestCount == null) {
            clientIpAddressesMap.put(clientAddr, 1)
        } else {
            if (requestCount >= MAX_REQUEST_COUNT) {
                return ActionStatus.createErrorStatus("promocode.validation.denied", messageSource)
            }
            clientIpAddressesMap.put(clientAddr, ++requestCount)
        }

        val cart = shoppingCart.getItemsMapCopy(cartService)
        val discount = promoCodeService.applyCode(myProfileId(), code, cart)
        return if (discount != null) {
            ActionStatus.createSuccessStatus(discount)
        } else {
            logger.warn("Discount is null for promocode " + code)
            ActionStatus.createErrorStatus("promocode.validation.error", messageSource)
        }
    }

    @PostMapping(Urls.REMOVE_FROM_SHOPPING_CART)
    @ResponseBody
    fun removeFromShoppingCart(@RequestParam(defaultValue = "0") productId: Long): ActionStatus<*> {
        shoppingCart.removeProduct(productId, cartService)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @PostMapping(Urls.CHANGE_QUANTITY_IN_SHOPPING_CART)
    @ResponseBody
    fun changeQuantityInShoppingCart(@RequestParam(defaultValue = "0") productId: Long,
                                     @RequestParam(defaultValue = "1") quantity: Int): ActionStatus<*> =
            if (shoppingCart.addProduct(productId, quantity, productUsersService, cartService)) {
                val dto = productUsersService.cartItemToDTO(productId, quantity)
                ActionStatus.createSuccessStatus(dto)
            } else {
                if (quantity > 1) {
                    ActionStatus.createErrorStatus<Any>("product.is.no.longer.in.stock", messageSource)
                } else {
                    ActionStatus.createErrorStatus<Any>("wrong.product.amount", messageSource)
                }
            }

    @PostMapping(Urls.CLEAR_SHOPPING_CART)
    @ResponseBody
    fun clearShoppingCart(): ActionStatus<*> {
        shoppingCart.clear(cartService)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @GetMapping(Urls.SHOPPING_CART)
    fun showShoppingCart(): ModelAndView {

        val otherProducts = if (shoppingCart.isEmpty) {
            emptyList()
        } else {
            productUsersService.getSimilarProducts(shoppingCart.anyProductId, BaseShopControllerService.MAX_VISIBLE_OTHER_PRODUCTS_COUNT)
        }
        return baseShopControllerService.createDefaultModelAndView(Actions.SHOPPING_CART)
                .addObject("otherProducts", otherProducts)
    }

    @GetMapping(Urls.SHOPPING_CART_PURCHASING_CONTACTS)
    fun showPurchaseShoppingCartContactsPage(): ModelAndView {
        if (shoppingCart.isEmpty) {
            logger.error("empty cart on CONTACTS ${myProfileId()}")
            return baseShopControllerService.baseControllerService.createRedirectModelAndView(Urls.SHOPPING_CART)
        }

        val modelAndView = baseShopControllerService.createDefaultModelAndView(Actions.SHOPPING_CART_CONTACTS)

        var shoppingCartContactsDTO: ShoppingCartContactsDTO? = sessionDAO.get(SESSION_PURCHASE_CONTACTS) as? ShoppingCartContactsDTO

        if (!isAnonymous()) {
            if (shoppingCartContactsDTO == null) {
                shoppingCartContactsDTO = ShoppingCartContactsDTO()
            }
            if (StringUtils.isBlank(shoppingCartContactsDTO.phoneNumber)) {
                shoppingCartContactsDTO.phoneNumber = profileService.getProfileSafe(myProfileId()).phoneNumber
            }
        }
        return modelAndView.addObject("contacts", shoppingCartContactsDTO)
                .addObject("linkedDeliveries", prepareDeliveries())
    }

    @PostMapping(Urls.SHOPPING_CART_PURCHASING_CONTACTS_JSON)
    @ResponseBody
    fun validatePurchaseShoppingCartContactsPage(@Valid contactsDTO: ShoppingCartContactsDTO, result: BindingResult): ActionStatus<*> {
        if (result.hasErrors()) {
            return ActionStatus.createErrorStatus<Any>(result, messageSource)
        }

        sessionDAO.put(SESSION_PURCHASE_CONTACTS, contactsDTO)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @GetMapping(Urls.SHOPPING_CART_PURCHASING_PAYMENT)
    fun showPurchaseShoppingCartPaymentPage(): ModelAndView {
        if (shoppingCart.isEmpty) {
            logger.error("empty cart on PAYMENTS ${myProfileId()}")
            return baseShopControllerService.baseControllerService.createRedirectModelAndView(Urls.SHOPPING_CART)
        }
        val contactsDTO = sessionDAO.get(SESSION_PURCHASE_CONTACTS) as? ShoppingCartContactsDTO
                ?: return baseShopControllerService.baseControllerService.createRedirectModelAndView(Urls.SHOPPING_CART_PURCHASING_CONTACTS)

        val modelAndView = baseShopControllerService.createDefaultModelAndView(Actions.SHOPPING_CART_PAYMENT)
        if (contactsDTO.deliveryPrice != null) {
            modelAndView.addObject("deliveryPrice", contactsDTO.deliveryPrice)
        }
        return modelAndView
    }

    private fun isBalanceNotEnough(dto: ShoppingCartPaymentDTO): Boolean {
        if (dto.paymentMethodId == paymentSettingsService.internalPaymentMethod.id) {
            val balance = profileBalanceService.getBalance(myProfileId())
            val orderPrice = dto.donateAmount.add(dto.deliveryPrice).subtract(BigDecimal(dto.discountAmount))
            if (balance.compareTo(orderPrice) < 0) {
                return true
            }
        }
        return false
    }

    @PostMapping(Urls.SHOPPING_CART_PURCHASING_PAYMENT_JSON)
    @ResponseBody
    fun validatePurchaseShoppingCartPaymentPage(@Valid paymentDTO: ShoppingCartPaymentDTO, result: BindingResult): ActionStatus<*> {

        val shoppingCartContactsDTO = sessionDAO.get(SESSION_PURCHASE_CONTACTS) as ShoppingCartContactsDTO
        if (shoppingCartContactsDTO == null) {
            logger.error("No contacts")
            return ActionStatus.createErrorStatus<Any>(MessageCode.UNEXPECTED_EXCEPTION.errorPropertyName, messageSource)
        }
        if (isDeliveryAndPaymentTypeInvalid(paymentDTO, shoppingCartContactsDTO) || isBalanceNotEnough(paymentDTO)) {
            result.rejectValue("paymentMethodId", "payment.required")
        }

        if (shoppingCartContactsDTO.deliveryType == DeliveryType.NOT_SET && !shoppingCartContactsDTO.onlyDigitalProducts) {
            result.rejectValue("deliveryType", "field.required")
        }
        if (result.hasErrors()) {
            return ActionStatus.createErrorStatus<Any>(result, messageSource)
        }

        sessionDAO.put(SESSION_PURCHASE_PAYMENT, paymentDTO)
        return ActionStatus.createSuccessStatus<Any>()
    }

    @GetMapping(Urls.SHOPPING_CART_PURCHASING_CONFIRM)
    fun showPurchaseShoppingCartConfirmPage(): ModelAndView {
        if (shoppingCart.isEmpty) {
            logger.error("empty cart on CONFIRM ${myProfileId()}")
            return baseShopControllerService.baseControllerService.createRedirectModelAndView(Urls.SHOPPING_CART)
        }
        val modelAndView = baseShopControllerService.createDefaultModelAndView(Actions.SHOPPING_CART_CONFIRM)

        val shoppingCartContactsDTO = sessionDAO.get(SESSION_PURCHASE_CONTACTS) as ShoppingCartContactsDTO
        if (shoppingCartContactsDTO != null) {
            modelAndView.addObject("contacts", shoppingCartContactsDTO)
            if (!shoppingCartContactsDTO.onlyDigitalProducts) {
                val baseDelivery = deliveryService.getLinkedDelivery(0, shoppingCartContactsDTO.serviceId, SubjectType.SHOP)
                val address: Address
                if (shoppingCartContactsDTO.deliveryType == DeliveryType.CUSTOMER_PICKUP) {
                    modelAndView.addObject("isPickup", true)
                    address = baseDelivery.address
                } else {
                    modelAndView.addObject("isPickup", false)
                    address = shoppingCartContactsDTO.customerContacts
                    val c = geoService?.getCountryById(address.countryId)
                    if (c != null) {
                        address.country = c.countryNameRus
                    }
                }
                modelAndView.addObject("deliveryAddress", address.toString())
                modelAndView.addObject("deliveryName", baseDelivery)
            }
        } else {
            logger.error("empty contacts on CONFIRM ${myProfileId()}")
            return baseShopControllerService.baseControllerService.createRedirectModelAndView(Urls.SHOPPING_CART_PURCHASING_CONTACTS)
        }

        val paymentDTO = sessionDAO.get(SESSION_PURCHASE_PAYMENT) as ShoppingCartPaymentDTO

        if (paymentDTO != null) {
            modelAndView.addObject("paymentModel", paymentDTO)
            if (paymentDTO.paymentType == PaymentType.CASH) {
                modelAndView.addObject("paymentSystem", baseShopControllerService.baseControllerService.getParametrizedMessage("shop.cash.purchase"))
            } else {
                modelAndView.addObject("paymentSystem", paymentService.getPaymentMethod(paymentDTO.paymentMethodId).name)
            }
            val promocodeEnabled = paymentDTO.promoCodeId > 0
            modelAndView.addObject("promocodeEnabled", promocodeEnabled)
        } else {
            logger.error("empty payment on CONFIRM ${myProfileId()}")
            return baseShopControllerService.baseControllerService.createRedirectModelAndView(Urls.SHOPPING_CART_PURCHASING_PAYMENT)
        }
        return modelAndView
    }


    private fun prepareDeliveries(): List<LinkedDelivery> {
        var totalPrice = BigDecimal.ZERO
        var freeDelivery = true
        var noFreeDelivery = false

        val products = ArrayList<Product>()
        val items = shoppingCart.getItemsMapCopy(cartService)
        for ((key, productCount) in items) {
            val product = productUsersService.getProductSafe(key)
            products.add(product)
            val productPrice = product.price.multiply(BigDecimal(productCount))
            totalPrice = totalPrice.add(productPrice)

            if (IterableUtils.matchesAny(product.tags) { category -> category.mnemonicName == Category.NO_FREE_DELIVERY }) {
                noFreeDelivery = true
            }

            if (!product.isFreeDelivery) {
                freeDelivery = false
            }
        }

        val linkedDeliveries = deliveryService.getEnabledLinkedDeliveries(0, SubjectType.SHOP)
        if (!noFreeDelivery && (totalPrice >= FREE_DELIVERY_THRESHOLD || freeDelivery)) {
            linkedDeliveries
                    .filter { it.location != null }
                    .forEach {
                        when (it.location.locationType) {
                            LocationType.COUNTRY -> if (it.location.locationId.toLong() == RUSSIA_LOCATION) {
                                it.price = BigDecimal.ZERO
                            }
                            LocationType.REGION -> if (it.location.parentLocationId.toLong() == RUSSIA_LOCATION) {
                                it.price = BigDecimal.ZERO
                            }
                            LocationType.CITY -> if (it.location.parent != null && it.location.parent.parentLocationId.toLong() == RUSSIA_LOCATION) {
                                it.price = BigDecimal.ZERO
                            }
                        }
                    }
        }
        deliveryService.filterProductDeliveries(linkedDeliveries, products)
        return linkedDeliveries
    }

    @PostMapping(Urls.SHOPPING_CART_PURCHASING_ORDER)
    @Throws(RegistrationException::class, PermissionException::class)
    fun purchaseShoppingCartHtml(request: HttpServletRequest, response: HttpServletResponse): ModelAndView {

        if (shoppingCart.isEmpty) {
            return baseShopControllerService.baseControllerService.createRedirectModelAndView(Urls.SHOPPING_CART)
        }

        val contactsDTO = sessionDAO.get(SESSION_PURCHASE_CONTACTS) as ShoppingCartContactsDTO
        val paymentDTO = sessionDAO.get(SESSION_PURCHASE_PAYMENT) as ShoppingCartPaymentDTO

        //do not move this method, it throw exception if email already exist
        val buyerId = basePaymentControllerService.getRegisteredOrCreateProfileId(contactsDTO.email, request, response)

        val deliveryInfo = DeliveryInfo(contactsDTO.serviceId, contactsDTO.deliveryPrice, contactsDTO.deliveryType, contactsDTO.comment)

        val deliveryAddress = deliveryAddressService.createFromShopOrderInfoDTO(contactsDTO)

        val cart = shoppingCart.getItemsMapCopy(cartService)
        val promoCodeId = paymentDTO.promoCodeId
        var discount: Discount? = null
        if (promoCodeId > 0) {
            discount = promoCodeService.applyCode(buyerId, promoCodeId, cart)
            promoCodeService.decreaseUsageCount(promoCodeId)
        }

        val order = productAfterFundService.createOrderFromShop(buyerId, paymentDTO.paymentType, deliveryInfo, deliveryAddress, cart, discount)
        try {
            val shortLinkId = CookieUtils.getCookieValue(request.getCookies(), OrderShortLinkStat.SHORTLINK_ID_COOKIE_NAME, "0").toLong()
            if (shortLinkId > 0) {
                val orderShortLinkStat = OrderShortLinkStat()
                orderShortLinkStat.orderId = order.orderId
                orderShortLinkStat.shortLinkId = CookieUtils.getCookieValue(request.getCookies(), OrderShortLinkStat.SHORTLINK_ID_COOKIE_NAME, "0").toLong()
                orderShortLinkStat.referrer = CookieUtils.getCookieValue(request.getCookies(), OrderShortLinkStat.SHORTLINK_REFERRER_COOKIE_NAME, "")
                orderShortLinkStatService.add(orderShortLinkStat)
            }
        } catch (ex: Exception) {
            logger.error("ERROR during trying to store shortlink cookie data to db, orderId = " + order.orderId)
            logger.error(ex)
        }

        sessionDAO.remove(SESSION_PURCHASE_CONTACTS)
        sessionDAO.remove(SESSION_PURCHASE_PAYMENT)
        shoppingCart.clear(cartService)

        val redirectUrl = if (paymentDTO.paymentType == PaymentType.CASH) {
            productAfterFundService.removeOrderedProductFromStore(order.orderId)
            projectService.getPaymentSuccessUrlByOrder(ProjectType.SHOP, order.orderId)
        } else {
            val transaction = createTransaction(paymentDTO, order)
            projectService.getPaymentGateRedirectUrl(transaction)
        }
        return baseShopControllerService.baseControllerService.createRedirectModelAndView(redirectUrl)
    }


    @BillingLoggableRequest(initial = true)
    fun createTransaction(shopOrderInfoDTO: ShoppingCartPaymentDTO, order: Order): TopayTransaction {
        if (shopOrderInfoDTO.isFromBalance && shopOrderInfoDTO.paymentMethodId == 0L) {
            shopOrderInfoDTO.amount = BigDecimal.ZERO
        }

        if (shopOrderInfoDTO.amount.compareTo(BigDecimal.ZERO) == 0) {
            shopOrderInfoDTO.paymentMethodId = paymentSettingsService.internalPaymentMethod.id
        }

        if (shopOrderInfoDTO.deliveryPrice == null) {
            shopOrderInfoDTO.deliveryPrice = BigDecimal.ZERO
        }
        return paymentService.createOrderPayment(order, shopOrderInfoDTO.amount, shopOrderInfoDTO.paymentMethodId,
                ProjectType.SHOP, shopOrderInfoDTO.paymentPhone, true)
    }

    companion object {

        private val logger = Logger.getLogger(ShoppingCartController::class.java)
        private const val MAX_REQUEST_COUNT = 100
        private const val MAP_CLEANUP_DELAY = 10 * 60 * 1000 // 10 mins

        private val FREE_DELIVERY_THRESHOLD = BigDecimal(5000)
        private const val RUSSIA_LOCATION: Long = 1

        private const val SESSION_PURCHASE_CONTACTS = "PURCHASE_CONTACTS"
        private const val SESSION_PURCHASE_PAYMENT = "PURCHASE_PAYMENT"

        private fun isDeliveryAndPaymentTypeInvalid(order: ShoppingCartPaymentDTO,
                                                    shoppingCartContactsDTO: ShoppingCartContactsDTO): Boolean =
                DeliveryType.RUSSIAN_POST == shoppingCartContactsDTO.deliveryType && PaymentType.CASH == order.paymentType
    }
}


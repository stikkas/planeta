package ru.planeta.shop.model.jaxb.yml.categories

import javax.xml.bind.annotation.XmlElement
import java.util.LinkedList

/**
 * User: sshendyapin
 * Date: 05.07.13
 * Time: 16:31
 */
class Categories {

    @XmlElement(name = "category")
    private val categoryList: MutableList<Category>

    constructor() {
        categoryList = LinkedList()
    }

    constructor(categoryList: List<ru.planeta.model.shop.Category>) : this() {
        categoryList
                .map { Category(it) }
                .forEach { this.categoryList.add(it) }
    }
}

package ru.planeta.shop.model

import org.springframework.context.annotation.Scope
import org.springframework.context.annotation.ScopedProxyMode
import org.springframework.stereotype.Repository
import org.springframework.web.context.WebApplicationContext
import ru.planeta.api.exceptions.NotFoundException
import ru.planeta.api.service.shop.CartService
import ru.planeta.api.service.shop.ProductUsersService
import ru.planeta.api.web.authentication.isAnonymous
import ru.planeta.api.web.authentication.myProfileId
import ru.planeta.model.shop.enums.ProductCategory
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentMap
import javax.annotation.PostConstruct

/**
 * WARNING serialazible data. NOT USE AUTOWIRING HERE
 * This class represents shopping cart.
 * User: m.shulepov
 * Date: 10.08.12
 * Time: 16:45
 */
@Repository
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
class ShoppingCart {
    private lateinit var items: ConcurrentMap<Long, Int>
    private var hasAnonim: Boolean = true
    private var firstLoad: Boolean = true

    val anyProductId: Long
        get() = items.keys.iterator().next()

    val isEmpty: Boolean
        get() = items.isEmpty()

    private fun getItems(cartService: CartService): ConcurrentMap<Long, Int> {
        if (firstLoad) {
            loadCartFromDb(cartService)
            firstLoad = false
        }
        return items
    }

    @PostConstruct
    private fun postConstruct() {
        items = ConcurrentHashMap()
        hasAnonim = isAnonymous()
        firstLoad = true
    }

    private fun loadCartFromDb(cartService: CartService) {
        if (isAnonymous()) {
            return
        }
        val list = cartService.getCart(myProfileId())
        for (shoppingCartShortItem in list) {
            items.put(shoppingCartShortItem.productId, shoppingCartShortItem.quantity)
        }
    }

    fun addProduct(productId: Long, count: Int, productUsersService: ProductUsersService, cartService: CartService): Boolean {
        try {
            val product = productUsersService.getProductSafe(productId)
            val maxCount = product.totalQuantity - product.totalOnHoldQuantity
            if (maxCount < count && product.productCategory == ProductCategory.PHYSICAL) {
                return false
            }
            getItems(cartService).put(productId, count)
            cartService.put(myProfileId(), productId, count)
        } catch (e: NotFoundException) {
            return false
        }

        return true
    }

    fun removeProduct(productId: Long, cartService: CartService) {
        getItems(cartService).remove(productId)
        cartService.remove(myProfileId(), productId)
    }

    fun clear(cartService: CartService) {
        items.clear()
        cartService.clear(myProfileId())
    }

    fun getItemsMapCopy(cartService: CartService): Map<Long, Int> {
        if (!isAnonymous() && hasAnonim) {
            if (items.isEmpty()) {
                loadCartFromDb(cartService)
            } else {
                cartService.clear(myProfileId())
            }
            hasAnonim = false
            return HashMap(items)
        }
        return HashMap(getItems(cartService))
    }
}

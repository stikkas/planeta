package ru.planeta.shop.controllers

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.servlet.ModelAndView
import ru.planeta.shop.controllers.services.BaseShopControllerService

/**
 * Member zone controller
 * User: a.savanovich
 * Date: 02.07.12
 * Time: 13:41
 */
@Controller
class WelcomeController(private val baseShopControllerService: BaseShopControllerService) {

    @GetMapping(Urls.CONTACTS)
    fun contactsInfo(): ModelAndView = baseShopControllerService.createDefaultModelAndView(Actions.CONTACTS)

    @GetMapping(Urls.DELIVERY)
    fun deliveryInfo(): ModelAndView = baseShopControllerService.createDefaultModelAndView(Actions.DELIVERY)

    @GetMapping(Urls.EXCHANGE)
    fun exchangeInfo(): ModelAndView = baseShopControllerService.createDefaultModelAndView(Actions.EXCHANGE)

    @GetMapping(Urls.PAYMENT)
    fun paymentInfo(): ModelAndView = baseShopControllerService.createDefaultModelAndView(Actions.PAYMENT)

    @GetMapping(Urls.FAQ)
    fun faq(): ModelAndView = baseShopControllerService.createDefaultModelAndView(Actions.FAQ)

    @GetMapping(Urls.OFFER)
    fun offer(): ModelAndView = baseShopControllerService.createDefaultModelAndView(Actions.OFFER)

}

package ru.planeta.shop.validation

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.planeta.api.model.shop.ShoppingCartPaymentDTO
import ru.planeta.api.service.billing.payment.settings.PaymentSettingsService
import ru.planeta.api.utils.ValidateUtils
import ru.planeta.model.shop.enums.PaymentType

/**
 * @author ds.kolyshev
 * Date: 29.08.12
 */
@Component
class ShopOrderInfoDTOValidator : Validator {
    override fun supports(aClass: Class<*>): Boolean {
        return ShoppingCartPaymentDTO::class.java.isAssignableFrom(aClass)
    }


    override fun validate(o: Any, errors: Errors) {
        val dto = o as ShoppingCartPaymentDTO

        if (dto.paymentMethodId == 0L && PaymentType.CASH != dto.paymentType) {
            errors.rejectValue("paymentMethodId", "payment.required")
        }

        if (dto.isNeedPhone) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "paymentPhone", "phone.required")
            if (!ValidateUtils.isValidPhone(dto.paymentPhone)) {
                errors.rejectValue("paymentPhone", "phone.required")
            }
        }

        if (PaymentType.NOT_SET == dto.paymentType) {
            errors.rejectValue("paymentType", "wrong.product.payment.type")
        }
    }
}

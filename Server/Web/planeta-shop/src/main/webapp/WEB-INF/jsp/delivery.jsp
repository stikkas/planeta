<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <title>Доставка</title>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
    <%@include file="/WEB-INF/jsp/includes/common-css.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>
    <%@ include file="/WEB-INF/jsp/includes/head.jsp" %>
</head>

<body class="grid-1200 shop-page">

<%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>

<div id="global-container" class="hidden">
    <div class="wrap">
        <div class="col-12">
        <%@include file="/WEB-INF/jsp/includes/shop-header.jsp" %>
            <div class="wrap-row">
                <div class="col-3">
                    <div class="shop-info-head">
                        Доставка
                    </div>
                </div>

                <div class="col-9">

                    <div class="shop-info-main">


                        <h3>Вы можете выбрать один из следующих способов доставки:</h3>


                        <div class="shop-info-hero">
                            <div class="shop-info-hero_list">
                                <div class="wrap-row">

                                    <div class="col-4">
                                        <div class="shop-info-hero_i">
                                            <div class="shop-info-hero_ico">
                                                <span class="s-product-info-pickup"></span>
                                            </div>

                                            <div class="shop-info-hero_cont">
                                                <div class="shop-info-hero_name">
                                                    <a href="#pickup-delivery">самовывоз из нашего офиса</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-4">
                                        <div class="shop-info-hero_i">
                                            <div class="shop-info-hero_ico">
                                                <span class="s-product-info-post"></span>
                                            </div>

                                            <div class="shop-info-hero_cont">
                                                <div class="shop-info-hero_name">
                                                    <a href="#post-delivery">почтой РФ по России</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <h3 id="pickup-delivery"><b>Самовывоз</b></h3>

                        <p>Самостоятельно забрать заказ вы можете в пункте самовывоза рядом с метро «Проспект Мира».</p>

                        <strong>Адрес:</strong>
                        <p class="mrg-t-0">129090, г. Москва, Проспект Мира, д. 19, стр. 3, вход со стороны улицы Дурова, этаж 3, офис Planeta.ru.</p>
                        <p>Схему проезда смотрите в разделе <a href="/contacts" target="_blank"><b>контакты</b></a>.</p>

                        <strong class="mrg-b-0">Время работы пункта самовывоза:</strong>
                        <p class="mrg-t-0">
                            По будням с 11:00 до 20:00,<br>
                            По субботам с 11:00 до 18:00,<br>
                            Воскресенье - выходной.
                        </p>

                        <p>Самовывоз возможен только после подтверждения заказа по телефону или электронной почте.</p>

                        <hr>

                        <h3 id="post-delivery"><b>Почта России</b></h3>

                        <p>Доставку по России и в другие страны мы осуществляем Почтой России. С почтой мы дружим и умеем отправлять так, чтобы ничего не потерялось и не разбилось. </p>
                        <p>Посылки отправляются службой логистики не реже одного раза в неделю. Подтверждение заказа при этом не требуется (мы свяжемся с вами только в случае каких-то вопросов по заказу). После отправки мы высылаем уведомление с трек-номером посылки, чтобы вы смогли отслеживать движение своего заказа на сайте Почты России. Будьте внимательны: с почты не всегда приходят уведомления о поступлении посылки в ваше отделение, поэтому мы высылаем трек-номера. Если вы не заберете посылку в течение месяца, она вернется нам обратно. В этом случае, чтобы мы повторно отправили ваш заказ, вам необходимо будет еще раз оплатить доставку.</p>

                        <table class="shop-info-table">
                            <tbody><tr>
                                <td>
                                    Стоимость отправки в любую точку России
                                </td>
                                <td>
                                    <span class="shop-info-table_digit">
                                        350 <span class="b-rub">Р</span>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Стоимость отправки в другие страны
                                </td>
                                <td>
                                    <span class="shop-info-table_digit">
                                        800 <span class="b-rub">Р</span> *
                                    </span>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <p>
                            <small>*на ряд тяжелых и габаритных товаров международная доставка рассчитывается отдельно</small>
                        </p>

                        <div class="cart-delivery-free">
                            При сумме заказа от <b>5 000 рублей</b> доставка по России осуществляется <b>бесплатно</b>.
                        </div>

                        <p>После оформления заказа вам необходимо дождаться его подтверждения. Менеджер интернет-магазина свяжется с вами по указанному телефону в течение дня.</p>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<%@ include file="/WEB-INF/jsp/includes/shop-footer.jsp" %>
</body>
</html>

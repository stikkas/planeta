<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>

<div class="shop-header">
    <div class="shop-header_basket">
        <div class="shop-basket pln-dropdown">
            <a class="shop-basket_link pln-d-switch" href="#">
                <span class="shop-basket_ico"></span>
                <span class="shop-basket_state">Ваша корзина<br>будет тут</span>
            </a>

            <div class="top-drop-block pln-d-popup basket-popup"></div>
            <div class="top-drop-block pln-d-popup basket-popup basket-popup-add js-basket"></div>
        </div>
    </div>

    <div class="shop-header_catalog-link visible-mobile">
        Каталог
    </div>
    <div class="shop-header_catalog-overlay"></div>

    <div class="shop-header_catalog">
        <div class="shop-header_home">
            <a href="https://${properties['shop.application.host']}" class="shop-home">
                <span class="shop-home_ico"></span>
            </a>
        </div>

        <div class="shop-header_menu">
            <div class="shop-menu pln-dropdown">
                    <span class="shop-menu_switch pln-d-switch">
                        <span class="shop-menu_name">Все товары</span>
                        <span class="shop-menu_arr"></span>
                    </span>
                <div class="pln-d-popup">
                    <div class="shop-menu_popup">
                        <div class="shop-menu_list-head visible-mobile">
                            Каталог
                        </div>
                        <div class="shop-menu_row">
                            <div class="shop-menu_list">
                                <div class='shop-menu_i'><a href="/products/search.html" class="shop-menu_link">Все товары</a></div>
                                <c:forEach items="${productTags}" var="productTag">
                                    <c:if test="${productTag.tagType == 'VISIBLE'}">
                                        <div class='shop-menu_i' data-mnemonic="${productTag.mnemonicName}"><a href="/products/search.html?productTagsMnemonics=${productTag.mnemonicName}" class="shop-menu_link">${productTag.value}</a></div>
                                    </c:if>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="shop-header_search">
            <a href="https://${properties['shop.application.host']}/products/search.html" class="shop-search">
                <span class="shop-search_ico">
                    <span class="s-icon s-icon-search"></span>
                </span>
            </a>
        </div>

        <script>
            $('.shop-search_ico').on('click', function () {
                $('.shop-search').toggleClass('active');
                $('.shop-search_input').select();
            });
        </script>

        <div class="shop-header_nav-head visible-mobile">
            О Планете
        </div>

        <div class="shop-header_nav">
            <div class="shop-nav">
                <div class="shop-nav_i <c:if test="${fn:contains(requestScope['javax.servlet.forward.servlet_path'],'/delivery')}"> active</c:if>">
                    <a href="https://${properties['shop.application.host']}/delivery" class="shop-nav_link">
                        <span class="shop-nav_ico"><span class="s-shop-nav-delivery"></span></span>
                        <span class="shop-nav_text">Доставка</span>
                    </a>
                </div>
                <div class="shop-nav_i<c:if test="${fn:contains(requestScope['javax.servlet.forward.servlet_path'],'/payment')}"> active</c:if>">
                    <a href="https://${properties['shop.application.host']}/payment" class="shop-nav_link">
                        <span class="shop-nav_ico"><span class="s-shop-nav-payment"></span></span>
                        <span class="shop-nav_text">Оплата</span>
                    </a>
                </div>
                <div class="shop-nav_i<c:if test="${fn:contains(requestScope['javax.servlet.forward.servlet_path'],'/exchange')}"> active</c:if>">
                    <a href="https://${properties['shop.application.host']}/exchange" class="shop-nav_link">
                        <span class="shop-nav_ico"><span class="s-shop-nav-return"></span></span>
                        <span class="shop-nav_text">Возврат и обмен</span>
                    </a>
                </div>
                <div class="shop-nav_i<c:if test="${fn:contains(requestScope['javax.servlet.forward.servlet_path'],'/contacts')}"> active</c:if>">
                    <a href="https://${properties['shop.application.host']}/contacts" class="shop-nav_link">
                        <span class="shop-nav_ico"><span class="s-shop-nav-contacts"></span></span>
                        <span class="shop-nav_text">Контакты</span>
                    </a>
                </div>
                <div class="shop-nav_i<c:if test="${fn:contains(requestScope['javax.servlet.forward.servlet_path'],'/faq')}"> active</c:if>">
                    <a href="https://${properties['shop.application.host']}/faq" class="shop-nav_link">
                        <span class="shop-nav_ico"><span class="s-shop-nav-help"></span></span>
                        <span class="shop-nav_text">Помощь</span>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="shop-header_m-search visible-mobile">
        <a href="https://${properties['shop.application.host']}/products/search.html" class="shop-m-search">
            <span class="shop-m-search_ico">
                <span class="s-icon s-icon-search"></span>
            </span>
        </a>
    </div>
</div>

<script>
    function shopCatalogMenu() {
        if ( !isMobileDev ) return;

        // open/close menu
        var menuIsOpen = false;
        var scrollPos;
        var menuToggle = $('.shop-header_catalog-link');
        var menuOverlay = $('.shop-header_catalog-overlay');
        $([menuToggle[0], menuOverlay[0]]).on('click', function () {
            if ( !menuIsOpen ) {
                scrollPos = $(window).scrollTop();

                //menuToggle.addClass('active');
                $('body').addClass('body-lock show-catalog-menu');

                $('html, body').scrollTop(0);
                $('#global-container').scrollTop(scrollPos);
            } else {
                //menuToggle.removeClass('active');
                $('body').removeClass('body-lock show-catalog-menu');

                $('html, body').scrollTop(scrollPos);
            }
            menuIsOpen = !menuIsOpen;
        });
    }

    shopCatalogMenu();
</script>
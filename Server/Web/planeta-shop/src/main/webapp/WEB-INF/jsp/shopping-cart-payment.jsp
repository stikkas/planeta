<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="hf" uri="http://planeta.ru/taglibs/HelperFunctions" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
	<%@ include file="/WEB-INF/jsp/includes/head.jsp" %>
    <meta name="description" content="Магазин Planeta.ru: эксклюзивные товары от&nbsp;официальных сообществ популярных артистов."/>
	<title>Магазин планеты: оформление заказа</title>

	<script type="text/javascript">
		$(document).ready(function() {
            var linkedDeliveries = ${hf:toJson(linkedDeliveries)} || [];
            var deliveryPrice = ${deliveryPrice} || 0;
            SessionStorageProvider.extendByKey('commonInfo', {'orderType': 'PRODUCT'});
            workspace.shoppingCart.set({editEnabled: false, onCheckoutPage: true}, {silent: true});
            var model = new Shop.Models.PaymentPage({
                profileModel: workspace.appModel.get('profileModel'),
                shoppingCartModel: workspace.shoppingCart,
                deliveryPrice: deliveryPrice,
                backButtonUrl: "/payment/shopping-cart-contacts"
			});

            var promoCodeModel = new Shop.Models.PromoCodeSection({
                parentModel: model
            });

            model.set({promoCodeModel: promoCodeModel});

            model.prefetch({ success: function() {
                var wrapper = new Shop.Views.ProductSelectPaymentPage({
                    el: '#center-container',
                    model: model
                });
                wrapper.render();
            }});
		});
	</script>
</head>
<body class="grid-1200 shop-page">
<c:set var="purchaseStep" value="2" />

<%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>

<div id="global-container">
    <div class="wrap" id="main-container">
        <div class="col-12">
            <div class="shop-order">
                <%@include file="/WEB-INF/jsp/includes/shop-purchase-header.jsp" %>
                <div class="shop-order_title">
                    Выберите способ оплаты
                </div>
                <div class="wrap-row" id="center-container"></div>
            </div>
        </div>
    </div>
<%@ include file="/WEB-INF/jsp/includes/shop-footer.jsp" %>
</body>
</html>

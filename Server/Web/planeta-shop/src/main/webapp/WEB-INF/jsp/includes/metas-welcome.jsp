<!-- Sharing meta data: start -->
<meta property="og:site_name" content="shop.planeta.ru"/>

<c:choose>
    <c:when test="${not empty customMetaTag.image}">
        <meta property="og:image" content="${hf:getThumbnailUrl(customMetaTag.image, "ORIGINAL", "VIDEO")}" />
    </c:when>
    <c:otherwise>
        <meta property="og:image" content="https://${hf:getStaticBaseUrl("")}/images/shop/shop-welcome.jpg" />
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${not empty customMetaTag.ogTitle}">
        <meta property="og:title" content="${customMetaTag.ogTitle}" />
    </c:when>
    <c:otherwise>
        <meta property="og:title" content="Интернет-магазин удивительных вещей с историей | Shop.planeta.ru" />
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${not empty customMetaTag.ogDescription}">
        <meta property="og:description" content="${customMetaTag.ogDescription}" />
    </c:when>
    <c:otherwise>
        <meta property="og:description" content="Магазин Планета.ру - это сотни товаров и подарков, созданных с помощью краудфандинга. Каждый из них имеет собственную историю, выбирайте прямо сейчас!" />
    </c:otherwise>
</c:choose>
<!-- Sharing meta data: end -->
<c:choose>
    <c:when test="${not empty customMetaTag.title}">
        <title>${customMetaTag.title}</title>
    </c:when>
    <c:otherwise>
        <title>${headTitle}</title>
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${not empty customMetaTag.description}">
        <meta name="description" content="${customMetaTag.description}"/>
    </c:when>
    <c:otherwise>
        <meta name="description" content="Магазин Планета.ру - это сотни товаров и подарков, созданных с помощью краудфандинга. Каждый из них имеет собственную историю, выбирайте прямо сейчас!"/>
    </c:otherwise>
</c:choose>

<c:if test="${not empty customMetaTag.keywords}">
    <meta name="keywords" content="${customMetaTag.keywords}"/>
</c:if>

<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "WebSite",
        "url": "https://shop.planeta.ru/",
        "potentialAction": {
        "@type": "SearchAction",
        "target": "https://www.google.com/search?q={search_term_string}%20site:shop.planeta.ru",
        "query-input": "required name=search_term_string"
        }
    }
</script>

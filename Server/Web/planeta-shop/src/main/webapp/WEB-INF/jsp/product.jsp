<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<jsp:useBean id="now" class="java.util.Date"/>
<head>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
    <%@ include file="/WEB-INF/jsp/includes/head.jsp" %>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/project.css"/>
    <%@include file="/WEB-INF/jsp/includes/product-page-meta.jsp" %>

    <script type="text/javascript">
        $(document).ready(function () {
            ShopUtils.initializeShopUtils();
            var model = new ProductPreview.Models.Main({
                product: ${hf:toJson(productInfo)},
                productType: '${attributeType.value}',
                otherProducts: ${hf:toJson(otherProducts)},
                maxVisibleOtherProductsCount: '${maxVisibleOtherProductsCount}' || 0
            });
            var view = new ProductPreview.Views.Main({
                el: '#main-container',
                model: model
            });
            view.renderAsync();

            if (model.get('product').productStatus == "ACTIVE") {
                $('.js-anchr-for-sharing').share({orientation: 'vertical', className: 'sharing-popup-social sharing-mini', counterEnabled: true, hidden: false, parseMetaTags: true});
            }

            if (window.isMobileDev && window.mobileCssLoadedDfd) {
                window.mobileCssLoadedDfd.done(function () {
                    var owl = $(".product-m-preview");
                    owl.addClass('owl-carousel').owlCarousel({
                        lazyLoad: true,
                        loop: true,
                        items: 1,
                        margin: 10,
                        nav: false,
                        dots: true,
                        mouseDrag: false,
                        autoplay: false
                    });
                });
            }
        });
    </script>
</head>

<body class="grid-1200 shop-page">

<%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>

<div id="global-container">
    <div class="wrap" id="main-container">
        <div class="col-12">
            <%@include file="/WEB-INF/jsp/includes/shop-header.jsp" %>
            <div class="wrap-row">

                <div class="col-7">
                    <div class="shop-view-left">
                        <div class="product-preview hidden-mobile">
                            <div class="product-preview_list-wrap">
                                <div class="scroll">
                                    <div class="scroll-content modal-scroll-content">
                                    </div>
                                </div>
                            </div>
                            <div class="product-preview_cover">
                                <img src="${hf:getThumbnailUrl(productInfo.coverImageUrl, "HUGE", "PRODUCT")}" alt = "${hf:escapeHtml4(productInfo.nameHtml)}"/>
                                <c:if test="${productInfo.productCategory eq 'DIGITAL'}">
                                    <span class="product-preview_digital">Электронный товар</span>
                                </c:if>
                            </div>
                        </div>

                        <div class="product-m-preview visible-mobile">
                            <c:forEach items="${productInfo.imageUrls}" var="url">
                                <div class="product-m-preview_i">
                                    <img class="product-m-preview_cover owl-lazy" data-src="${hf:getThumbnailUrl(url, "BIG", "PRODUCT")}" alt = "${hf:escapeHtml4(productInfo.nameHtml)}">
                                </div>
                            </c:forEach>
                        </div>

                        <div class="product-description-block hidden-mobile ">
                            <div class="product-share js-anchr-for-sharing"></div>

                            <div class="product-description-wrap">
                                <div class="product-description">
                                    ${productInfo.descriptionHtml}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-5">
                    <div class="shop-view-right">
                        <div class="product-name">${productInfo.nameHtml}</div>

                        <c:if test="${not empty productInfo.referrer}">
                            <div class="product-author">
                                Все товары:
                                <a class="product-author_link" href="/products/search.html?referrerIds=${productInfo.referrer.profileId}">${productInfo.referrer.displayName}</a>
                            </div>
                        </c:if>

                        <c:if test="${not empty productInfo.childrenProducts}">
                            <div class="product-options">
                                <div class="product-options_i">
                                    <div class="product-options_lbl">
                                        Параметры:
                                    </div>

                                    <div class="product-options_val">
                                        <div class="product-options_val-list">
                                            <c:forEach items="${productInfo.childrenProducts}" var="child" varStatus="status">
                                                <div class="form-ui product-options-form-ui">
                                                    <input class="form-ui-control js-child-product" data-child-id="${child.productId}" id="radio-${child.productId}" type="radio" name="radio"
                                                            <c:if test="${child.readyForPurchaseQuantity <= 0}"> disabled</c:if> >
                                                    <label class="form-ui-label" for="radio-${child.productId}">
                                                        <span class="form-ui-txt">${child.productAttribute.value}</span>
                                                    </label>
                                                </div>
                                            </c:forEach>
                                        </div>
                                    </div>

                                    <c:if test="${not empty productInfo.tableImageUrl}">
                                        <div class="product-size-table">
                                            <a href="${productInfo.tableImageUrl}" class="product-size-table_link photo-zoom-link">Таблица размеров</a>
                                        </div>
                                    </c:if>

                                </div>
                            </div>
                        </c:if>

                        <div class="product-action">
                            <div class="product-price">
                                <div class="product-price_cont">
                                    <div class="product-price_val">
                                        <fmt:formatNumber value="${productInfo.price}" /><sup class="b-rub">Р</sup>
                                    </div>
                                    <c:if test="${productInfo.oldPrice > 0}">
                                        <div class="product-price_old">
                                            <fmt:formatNumber value="${productInfo.oldPrice}" /><sup class="b-rub">Р</sup>
                                        </div>
                                    </c:if>
                                </div>
                            </div>

                            <div class="js-anchor-cart-controls"></div>
                        </div>

                        <c:if test="${productInfo.donateId > 0}">
                            <div class="product-crowd js-anchor-extended-controls"></div>
                        </c:if>

                        <div class="product-info">
                            <div class="product-info-tab">
                                <div class="product-info-tab_i active">
                                    Оплата
                                </div>
                                <c:if test="${productInfo.productCategory ne 'DIGITAL'}">
                                    <div class="product-info-tab_i">
                                        Доставка
                                    </div>
                                </c:if>
                            </div>

                            <div class="product-info-cont">
                                <div class="product-info-list active">
                                    <c:if test="${productInfo.cashAvailable == true}">
                                        <div class="product-info-list_i">
                                            <div class="product-info-list_ico"><span class="s-product-info-cash"></span></div>
                                            <div class="product-info-list_text">наличными<br>при&nbsp;получении</div>
                                        </div>
                                    </c:if>

                                    <div class="product-info-list_i">
                                        <div class="product-info-list_ico"><span class="s-product-info-card"></span></div>
                                        <div class="product-info-list_text">банковскими<br>картами</div>
                                    </div>

                                    <div class="product-info-list_i">
                                        <div class="product-info-list_ico"><span class="s-product-info-e-money"></span></div>
                                        <div class="product-info-list_text">электронными<br>деньгами</div>
                                    </div>

                                    <div class="product-info-list_i">
                                        <div class="product-info-list_ico"><span class="s-product-info-e-bank"></span></div>
                                        <div class="product-info-list_text">через<br>интернет-банкинг</div>
                                    </div>
                                </div>


                                <c:if test="${productInfo.productCategory ne 'DIGITAL'}">
                                <div class="product-info-list">
                                    <c:if test="${not noFreeDelivery}">
                                    <div class="product-info-list_i">
                                        <div class="product-info-list_ico"><span class="s-product-info-5000"></span></div>
                                        <div class="product-info-list_text"><c:if test="${productInfo.freeDelivery == false}">от 5&nbsp;000<br></c:if>бесплатно</div>
                                    </div>

                                    </c:if>
                                    <div class="product-info-list_i">
                                        <div class="product-info-list_ico"><span class="s-product-info-pickup"></span></div>
                                        <div class="product-info-list_text">самовывоз<br>из&nbsp;офиса<br>planeta.ru</div>
                                    </div>

                                    <c:if test="${not noRussianPostDelivery}">
                                        <div class="product-info-list_i">
                                            <div class="product-info-list_ico"><span class="s-product-info-post"></span></div>
                                            <div class="product-info-list_text">доставка&nbsp;по&nbsp;РФ<br>почтой&nbsp;России</div>
                                        </div>
                                    </c:if>
                                </div>
                                </c:if>
                                <script>
                                    var tab = $('.product-info-tab_i');
                                    var list = $('.product-info-list');

                                    tab.on('click', function () {
                                        if ( $(this).hasClass('active') ) return;
                                        tab.filter('.active').removeClass('active');
                                        $(this).addClass('active');

                                        var index = tab.index(this);
                                        list.filter('.active').removeClass('active');
                                        list.eq(index).addClass('active');
                                    })
                                </script>
                            </div>
                        </div>

                        <div class="product-description-block visible-mobile ">
                            <div class="product-description-wrap">
                                <div class="product-description">
                                    ${productInfo.descriptionHtml}
                                </div>
                            </div>
                        </div>

                        <div class="product-comments">
                            <div class="product-comments_head">
                                Отзывы
                            </div>
                            <div class="attach-comment js-anchor-comments"></div>
                        </div>
                    </div>
                </div>
            </div>

            <p:if test="${not empty otherProducts}">
                <div class="shop-related-product">
                    <div class="shop-related-product_head">
                        Похожие товары
                    </div>
                    <div class="shop-related-product_list js-anchor-other-products"></div>
                </div>
            </p:if>

        </div>
    </div>
    <%@ include file="/WEB-INF/jsp/includes/shop-footer.jsp" %>
</body>
</html>

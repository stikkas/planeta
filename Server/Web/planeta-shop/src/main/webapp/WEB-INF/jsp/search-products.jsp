<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>

<head>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
    <%@ include file="/WEB-INF/jsp/includes/head.jsp" %>
    <c:set var="headTitle" value="Поиск по товарам" />
    <%@include file="/WEB-INF/jsp/includes/metas-welcome.jsp" %>

    <script type="text/javascript">
        ShopUtils.setProductTags(${hf:toJson(productTags)});
        ShopUtils.initializeShopUtils();
        $(document).ready(function() {
            Banner.init('${mainAppUrl}', workspace.appModel.get('myProfile').get('isAuthor'));
            model = new ProductSearch.Models.Main({
                products: ${hf:toJson(products)},
                query: '${productsSearch.query}',
                mainCategoryMnemonic: '${mainTag.mnemonicName}',
                productTags: ${hf:toJson(productsSearch.productTagsMnemonics)},
                referrers: ${hf:toJson(referrers)},
                referrerIds: ${hf:toJson(productsSearch.referrerIds)},
                priceFrom: ${productsSearch.priceFrom},
                priceTo: ${productsSearch.priceTo},
                limit : 12
            });
            model.get('filter').set('sortOrder', '${productsSearch.sortOrder}');

            var view = new ProductSearch.Views.Controller({
                el: '#main-container',
                model: model
            });

            view.render();

            var referrersVeiw = new Products.Views.ReferrersView({
                el: '.js-referrers',
                model: model
            });

            referrersVeiw.render();

            $('.js-shop-filter_options-i').on('click', function(e) {
                setProductTag(e.target.value);
            });

            $('.shop-list-search_input').on('change', function(e) {
                setSearchQuery(e.target.value);
            });

            $('.shop-list-search_input').on('keyup', _.debounce(function(e) {
                setSearchQuery(e.target.value);
            }, 700));

            setSearchQuery = function(query) {
                model.get('filter').set('query', query);
                model.filterChanged();
            };

            setProductTag = function(mnemonicName) {
                var productTags = model.get('filter').get('productTags');

                if(productTags) {
                    var index = productTags.indexOf(mnemonicName);
                    if(index != -1) {
                        productTags.splice(index, 1);
                    } else {
                        productTags.push(mnemonicName);
                    }
                } else {
                    productTags = [];
                    productTags.push(mnemonicName);
                }

                model.get('filter').set('productTags', productTags);
                model.filterChanged();
            };

            $('.shop-filter-category_i').on('click', function(e) {
                $('.shop-filter-category_i ').removeClass('active');
                var tags = ShopUtils.getProductTags(),
                    mnemonicName = $(e.currentTarget).attr('data-mnemonic'),
                    text = e.currentTarget.innerText;

                var productTags = model.get('filter').get('productTags');
                var newTags = [];

                model.get('filter').set('referrerIds', null);
                model.get('referrers').mnemonicName = mnemonicName;
                model.get('referrers').load();

                if(productTags && productTags.length > 0) {
                    productTags.forEach(function(tag) {
                        tags.forEach(function (t) {
                            if(t.mnemonicName == tag && t.tagType == 'PROMO') {
                               newTags.push(tag);
                            }
                        });
                    });
                }

                if(mnemonicName == "CHARITY") {
                    $('.js-shop-filter_head').text('Фонды');
                } else {
                    $('.js-shop-filter_head').text('Авторы');
                }

                if(mnemonicName != "ALL") {
                    newTags.push(mnemonicName);
                }

                var selector = '[data-mnemonic=' + mnemonicName + ']';
                $(selector).filter('.shop-filter-category_i').addClass('active');
                $('.shop-list-name').text(text);
                model.get('filter').set('productTags', newTags);
                model.filterChanged();
            });


            setPriceFrom = function(price) {
                model.get('filter').set('priceFrom', price);
                model.filterChanged();
            };

            setPriceTo = function(price) {
                model.get('filter').set('priceTo', price);
                model.filterChanged();
            };

            $('.js-price-from').on('keyup', _.debounce(function(e) {
                setPriceFrom(e.target.value);
            }, 700));

            $('.js-price-to').on('keyup', _.debounce(function(e) {
                setPriceTo(e.target.value);
            }, 700));

            $('.product-sort_i').on('click', function(e) {
                $('.product-sort_val-name').text(e.target.innerText);
                $('.product-sort_val').removeClass('open');

                model.get('filter').set('sortOrder', $(e.currentTarget).attr('data-sort-value'));
                model.filterChanged();
            });

            var value = '${productsSearch.query}';
            $('#search-input').val(value);
            $('#search-input').focus();

            inputCurrencyMask.init($('.js-currency-mask'));
        });
    </script>

    <script src="//${hf:getStaticBaseUrl("")}/js/lib/jquery.sticky-kit.min.js"></script>
</head>

<body class="grid-1200 shop-page">

<%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>

<div id="global-container">
    <div class="wrap-container" id="main-container">
        <div class="wrap" id="center-container">
            <div class="col-12">
                <%@include file="/WEB-INF/jsp/includes/shop-header.jsp" %>
                <div class="shop-list-name">
                    ${mainTag.value}
                </div>

                <div class="shop-list-search">
                    <div class="shop-list-search_ico">
                        <span class="s-icon s-icon-search"></span>
                    </div>
                    <div class="shop-list-search_val">
                        <input id="search-input" type="text" class="form-control shop-list-search_input" placeholder="Поиск..." >
                    </div>
                </div>

                <div class="shop-list-wrap wrap-row">
                    <div class="shop-filter">
                        <div class="shop-filter-cont">
                            <div class="shop-filter-show-btn visible-mobile">
                                <span class="btn btn-primary btn-block js-show-filter-btn">Показать фильтр</span>
                            </div>

                            <script>
                                $(function () {
                                    $('.js-show-filter-btn').on('click', function() {
                                        $('.shop-filter-wrap').slideToggle(300);
                                    });
                                })
                            </script>

                            <div class="shop-filter-wrap">
                                <div class="shop-filter-category">
                                    <div class="shop-filter-category_i <p:if test="${empty productsSearch.productTagsMnemonics}">active</p:if>" data-mnemonic="ALL">
                                        <span class="shop-filter-category_link">
                                            Все товары
                                        </span>
                                    </div>

                                    <c:forEach items="${productTags}" var="productTag">
                                        <c:if test="${productTag.tagType == 'VISIBLE'}">
                                            <div class="shop-filter-category_i <p:if test="${mainTag.mnemonicName eq productTag.mnemonicName}">active</p:if>" data-mnemonic="${productTag.mnemonicName}">
                                                <span class="shop-filter-category_link">
                                                    ${productTag.value}
                                                </span>
                                            </div>
                                        </c:if>
                                    </c:forEach>
                                </div>


                                <div class="shop-filter_list">
                                    <div class="shop-filter_i">
                                        <div class="shop-filter_cont">
                                            <div class="shop-filter_options">
                                                <c:forEach items="${productTags}" var="productTag">
                                                    <c:if test="${productTag.tagType == 'PROMO'}">
                                                        <c:set var="checked" value="" />
                                                        <c:forEach items="${productsSearch.productTagsMnemonics}" var="mnemonicName">
                                                            <c:if test="${mnemonicName == productTag.mnemonicName}">
                                                                <c:set var="checked" value="checked" />
                                                            </c:if>
                                                        </c:forEach>
                                                        <div class="shop-filter_options-i">
                                                            <div class="form-ui form-ui-filter">
                                                                <input class="form-ui-control js-shop-filter_options-i" id="chck-${productTag.categoryId}" type="checkbox" name="checkbox" value="${productTag.mnemonicName}" ${checked}>
                                                                <label class="form-ui-label" for="chck-${productTag.categoryId}">
                                                                    <span class="form-ui-txt">${productTag.value}</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </c:if>
                                                </c:forEach>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="js-referrers shop-filter_i"></div>

                                    <div class="shop-filter_i">
                                        <div class="shop-filter_head">
                                            Стоимость
                                        </div>


                                        <div class="shop-filter_cont">
                                            <div class="shop-filter_range">
                                                <div class="shop-filter_range-col">
                                                    <div class="shop-filter_range-lbl">
                                                        от
                                                    </div>
                                                    <div class="shop-filter_range-val">
                                                        <input type="text" class="form-control js-currency-mask js-price-from" value="${productsSearch.priceFrom}">
                                                    </div>
                                                </div>
                                                <div class="shop-filter_range-col">
                                                    <div class="shop-filter_range-lbl">
                                                        до
                                                    </div>
                                                    <div class="shop-filter_range-val">
                                                        <input type="text" class="form-control js-currency-mask js-price-to" value="${productsSearch.priceTo}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>

                    <div class="product-card-list">
                        <div class="js-banner-container-top"></div>
                        
                        <div class="product-sort">
                            <div class="product-sort_lbl">
                                Сортировать:
                            </div>

                            <div class="product-sort_val pln-dropdown">
                                <span class="product-sort_val-link pln-d-switch">
                                    <span class="product-sort_val-name">по последней покупке</span>
                                    <span class="product-sort_val-arr"></span>
                                </span>

                                <div class="pln-d-popup">
                                    <div class="product-sort_popup">
                                        <div class="product-sort_list">
                                            <div class="product-sort_i" data-sort-value="SORT_DEFAULT">
                                                по последней покупке
                                            </div>
                                            <div class="product-sort_i" data-sort-value="SORT_BY_PRICE_ASC">
                                                цена по возрастанию
                                            </div>
                                            <div class="product-sort_i" data-sort-value="SORT_BY_PRICE_DESC">
                                                цена по убыванию
                                            </div>
                                            <div class="product-sort_i" data-sort-value="SORT_BY_TIME_ADDED_DESC">
                                                по новизне
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="js-product-container"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/includes/shop-footer.jsp" %>
</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <title>Контакты</title>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
    <%@include file="/WEB-INF/jsp/includes/common-css.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/about.css" />
    <%@ include file="/WEB-INF/jsp/includes/head.jsp" %>
</head>

<body class="grid-1200 shop-page">

<%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>

<div id="global-container" class="hidden">
    <div class="wrap">
        <div class="col-12">
            <%@include file="/WEB-INF/jsp/includes/shop-header.jsp" %>
            <div class="shop-info-head">
                Контакты
            </div>
            <div class="shop-info-main">
                <div class="wrap-row">
                    <div class="shop-info-main_col-i col-3">
                        <h3><b>Реквизиты компании</b></h3>
                        <p class="shop-info-text-sm">
                            Наименование: ООО "Магазин Планета"
                            <br>
                            ОГРН: 1127746676760
                            <br>
                            ИНН: 7709910718
                            <br>
                            КПП 770201001
                            <br>
                            Расчетный счет: 40702810100310000319
                            <br>
                            Банк ИПБ (АО)
                            <br>
                            БИК: 044525402
                        </p>

                    </div>

                    <div class="shop-info-main_col-i col-5">
                        <h3><b>Офис и пункт выдачи товаров для самовывоза</b></h3>

                        <h3 class="mrg-b-0"><b>Адрес:</b></h3>
                        <p class="mrg-t-0">129090, г.&nbsp;Москва, Проспект Мира, д.&nbsp;19, стр.&nbsp;3, вход со&nbsp;стороны улицы Дурова, 3&nbsp;этаж, офис&nbsp;Planeta.ru</p>

                        <h3 class="mrg-b-0"><b>Время работы офиса:</b></h3>
                        <p class="mrg-t-0">
                            По будням с 11:00 до 20:00,
                            <br>
                            По субботам с 11:00 до 18:00,
                            <br>
                            Суббота, воскресенье - выходной.
                        </p>
                    </div>

                    <div class="shop-info-main_col-i col-4">
                        <div class="shop-info-tel">
                            +7 (495) 181-06-06
                        </div>
                        <div class="shop-info-mail">
                            <a href="mailto:shop@planeta.ru">shop@planeta.ru</a>
                        </div>
                    </div>
                </div>

                <div class="shop-info-map" id="map"></div>
                <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyAcNhAbtfdB6fkVVs7BoaIoW5HEPjAn4Mw&v=3.31"></script>
                <p:script src="planeta-google-map.js"></p:script>

                <script>
                    google.maps.event.addDomListener(window, 'load', CustomPlnGoogleMap.init({
                        phoneNumber: '+7 (495) 181-06-06'
                    }));
                </script>
            </div>
        </div>
    </div>
</div>

<%@ include file="/WEB-INF/jsp/includes/shop-footer.jsp" %>
</body>
</html>

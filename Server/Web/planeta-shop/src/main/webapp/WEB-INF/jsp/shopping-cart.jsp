<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
    <%@ include file="/WEB-INF/jsp/includes/head.jsp" %>
    <meta name="description"
          content="Магазин Planeta.ru: эксклюзивные товары от&nbsp;официальных сообществ популярных артистов."/>
    <title>Магазин планеты: корзина</title>

    <script type="text/javascript">
        $(document).ready(function() {
            ShopUtils.initializeShopUtils();
            workspace.shoppingCart.set("inShoppingCart",true,{silent:true});
            var cartWrapper = new Shop.Views.ShoppingCartWrapper({
                el: '.js-anchor-cart-container',
                model: workspace.shoppingCart
            });
            new ProductPreview.Views.OtherProducts({
                el: '.js-anchor-other-products',
                model: new BaseModel({
                    otherProducts: ${hf:toJson(otherProducts)}
                })
            }).render();
        });
    </script>
</head>

<body class="grid-1200 shop-page">
<c:set var="purchaseStep" value="0" />

<%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>

    <div id="global-container">
        <div class="wrap" id="main-container">
            <div class="col-12">
                <div class="shop-order">
                    <%@include file="/WEB-INF/jsp/includes/shop-purchase-header.jsp" %>
                    <div class="shop-order-cart js-anchor-cart-container"></div>
                </div>

                <p:if test="${not empty otherProducts}">
                    <div class="shop-related-product">
                        <div class="shop-related-product_head">
                            Вас могут заинтересовать эти товары
                        </div>
                        <div class="shop-related-product_list js-anchor-other-products"></div>
                    </div>
                </p:if>
            </div>
        </div>
    </div>
</div>

<%@ include file="/WEB-INF/jsp/includes/shop-footer.jsp" %>
</body>
</html>

<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<!-- Sharing meta data: start -->
<meta property="og:site_name" content="Planeta.ru"/>
<c:if test="${empty customMetaTag.ogTitle}">
    <meta property="og:title" content="<c:out value="${productInfo.name}"/> - купить в интернет магазине Planeta"/>
</c:if>
<c:if test="${not empty customMetaTag.ogTitle}">
    <meta property="og:title" content="<c:out value="${customMetaTag.ogTitle}"/>"/>
</c:if>

<c:if test="${empty customMetaTag.ogDescription}">
    <meta property="og:description" content="<c:out value="${hf:getTextWithoutTags(productInfo.description)}"/> цена: <fmt:formatNumber value="${activeChild.price}"/> руб."/>
</c:if>
<c:if test="${not empty customMetaTag.ogDescription}">
    <meta property="og:description" content="<c:out value="${customMetaTag.ogDescription}"/>"/>
</c:if>

<c:if test="${empty customMetaTag.image}">
    <meta property="og:image" content="${productInfo.coverImageUrl}" />
    <link rel="image_src" href="${productInfo.coverImageUrl}"/>
</c:if>
<c:if test="${not empty customMetaTag.image}">
    <meta property="og:image" content="${customMetaTag.image}" />
    <link rel="image_src" href="${customMetaTag.image}"/>
</c:if>
<!-- Sharing meta data: end -->


<c:choose>
    <c:when test="${not empty customMetaTag.title}">
        <title>${customMetaTag.title}</title>
    </c:when>
    <c:otherwise>
        <title>Товар &laquo;${productInfo.name}&raquo;</title>
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${not empty customMetaTag.description}">
        <meta name="description" content="${customMetaTag.description}"/>
    </c:when>
    <c:otherwise>
        <meta name="description" content="<c:out value="${hf:getTextWithoutTags(productInfo.description)}"/> цена: <fmt:formatNumber value="${activeChild.price}"/> руб."/>
    </c:otherwise>
</c:choose>
<c:if test="${not empty customMetaTag.keywords}">
    <meta name="keywords" content="${customMetaTag.keywords}"/>
</c:if>
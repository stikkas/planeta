<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width"/>
    <meta property="og:site_name" content="shop.planeta.ru"/>
    <meta property="og:title" content="Я поддерживаю проект <c:out value="${firstObject.ownerName}"/>"/>
    <title>Спасибо за покупку!</title>
    <%@include file="/WEB-INF/jsp/includes/generated/common-favicon.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/common.css" />
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/payment.css"/>
    <link type="text/css" rel="stylesheet" href="//${hf:getStaticBaseUrl("")}/css-generated/shop.css"/>
    <script type="text/javascript" src="//${hf:getStaticBaseUrl("/res")}/js/libs.js?compress=${properties['static.compress']}&flushCache=${properties['static.flushCache']}"></script>
    <script type="text/javascript" src="//${hf:getStaticBaseUrl("")}/js/utils/plugins/share-widget.js?compress=${properties['static.compress']}&flushCache=${properties['static.flushCache']}"></script>

    <%@include file="/WEB-INF/jsp/includes/generated/common-templates.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-head.jsp" %>
    <%@include file="/WEB-INF/jsp/includes/planeta-js.jsp" %>
</head>
<body>
<%@ include file="/WEB-INF/jsp/includes/generated/screen-width.jsp" %>

<%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>

<%@ include file="/WEB-INF/jsp/includes/generated/mobile-header.jsp"%>


<script type="text/javascript">
    var cartItemList = ${hf:toJson(cartItemList)};

    var transactionInfo = {
        transactionId: '${transaction.transactionId}',
        amount: '${order.totalPrice}',
        deliveryPrice: '${order.deliveryPrice}',
        promoCodeId: '${order.promoCodeId}'
    };

    if (window.gtm) {
        window.gtm.trackPurchaseProducts(transactionInfo, cartItemList);
    }

    $(function() {
        ShopUtils.initializeShopUtils();
        var getDataForShare = {
            url : "https://${properties['shop.application.host']}/products/${firstObject.objectId}",
            title: 'Теперь у меня есть: ${hf:escapeEcmaScript(firstObject.objectName)} | Planeta',
            description: 'Вас ждет множество эксклюзивных товаров в магазине портала Planeta.ru. Успейте стать обладателем одного из них!',
            className: 'horizontal donate-sharing sharing-mini', counterEnabled: false, hidden: false,
            imageUrl: '${hf:getThumbnailUrl(firstObject.objectImageUrl, "ORIGINAL", "PRODUCT")}'
        };
        $('.share-cont-horizontal').share(getDataForShare);

        var relatedProducts = new Products.Models.ProductCollection(${hf:toJson(relatedProducts)});
        relatedProducts.allLoaded = true;

        var relatedCampaignsList = new Products.Views.ProductListView({
            el: '.js-product-list',
            collection: relatedProducts,
            gtmProductListTitle: 'Успешная оплата'
        });
        relatedCampaignsList.render();
//        relatedProducts.load();
    });
</script>

<div id="global-container">
    <div id="main-container" class="wrap-container">
        <div id="center-container" class="wrap">
            <div class="col-12">
                <div class="pln-payment-box">
                    <div class="pln-payment-success">

                        <c:choose>
                            <c:when test="${not empty isMobilePayment and isMobilePayment}">
                                <div class="pln-payment-success_head">
                                    Вам выставлен счет для оплаты!
                                </div>
                                <div class="pln-payment-success_sub-head">
                                    В ближайшие пять минут должна прийти SMS с инструкцией для оплаты.
                                    <br>
                                    Следуйте инструкции, и оплата будете завершена.
                                </div>
                            </c:when>
                            <c:when test="${paymentType == 'CASH'}">
                                <div class="pln-payment-success_head">
                                    Ваш заказ оформлен.
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="pln-payment-success_head">
                                    Оплата проведена <span class="hl">успешно</span>!
                                </div>
                            <div class="pln-payment-success_sub-head"></div>
                            </c:otherwise>
                        </c:choose>

                        <div class="pln-payment-success_widget cf">
                            <div class="pln-payment-success_widget_i">
                                <div class="pln-payment-success_widget_ico">
                                    <span class="success-widget-icon success-widget-icon-mail"></span>
                                </div>
                                <div class="pln-payment-success_widget_text">
                                    Мы отправили вам письмо с&nbsp;деталями вашего заказа.
                                </div>
                            </div>

                            <div class="pln-payment-success_widget_i">
                                <div class="pln-payment-success_widget_ico">
                                    <span class="success-widget-icon success-widget-icon-flag"></span>
                                </div>
                                <div class="pln-payment-success_widget_text">
                                    Купленные товары вы всегда найдете в вашем <a href="${redirectUrl}">личном&nbsp;кабинете</a>
                                </div>
                            </div>

                            <div class="pln-payment-success_widget_i">

                                <div class="pln-payment-success_widget_sharing">
                                    <div class="pln-payment-success_widget_sharing-head">Поделиться</div>
                                    <div class="share-cont-horizontal"></div>
                                </div>

                                <div class="pln-payment-success_widget_text">
                                    Поделитесь вашей
                                    <br>
                                    радостью от&nbsp;покупки
                                    <br>
                                    таких классных товаров!
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="project-related">
                    <div class="project-related-head">
                        Вас также может заинтересовать
                    </div>
                    <ul class="js-product-list product-card"></ul>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/footer.jsp" %>
</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="hf" uri="http://planeta.ru/taglibs/HelperFunctions" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
  <%@ include file="/WEB-INF/jsp/includes/head.jsp" %>
  <meta name="description" content="Магазин Planeta.ru: эксклюзивные товары от&nbsp;официальных сообществ популярных артистов."/>
  <title>Магазин планеты: оформление заказа</title>
  <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>

  <script type="text/javascript">
      $(document).ready(function () {
          workspace.shoppingCart.load().done(function () {
              var model = new BaseModel();
              model.set("cartItems", workspace.shoppingCart.get('cartItems').toJSON());
              var wrapper = new Shop.Views.OrderBlock({
                  el: '.js-cart',
                  model: model
              });
              wrapper.render();
          });
          $('.js-shop-confirm-order-button').click(function (e) {
              e.preventDefault();
              var self = this;
              $(self).attr("disabled", true);
              $('#shop-confirm-order-form').submit();
              setTimeout(function() {
                  $(self).attr("disabled", false);
              }, 3000);
          });
      });
  </script>
</head>
<body class="grid-1200 shop-page">
<c:set var="purchaseStep" value="3" />

<%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>

<div id="global-container">
  <div class="wrap" id="main-container">
    <div class="col-12">
      <div class="shop-order">
        <%@include file="/WEB-INF/jsp/includes/shop-purchase-header.jsp" %>
        <div class="shop-order_title">
          Подтверждение заказа
        </div>

        <div class="shop-confirm ">

          <div class="shop-confirm_i js-cart">
          </div>

          <c:if test="${not contacts.onlyDigitalProducts}">
              <div class="shop-confirm_i">

                <div class="shop-checkout">
                  <div class="shop-checkout_i">

                    <div class="cart-short-header">
                      <div class="cart-short-header_head">
                        Способ доставки
                      </div>
                      <a href="/payment/shopping-cart-contacts" class="cart-short-header_edit-link">изменить</a>
                    </div>

                    <div class="shop-checkout_val">
                      ${contacts.deliveryPrice} <sup class="b-rub">Р</sup>
                    </div>
                    <div class="shop-checkout_lbl">
                      <span class="shop-checkout_name">${deliveryName.name}</span>
                      <br>
                      <span class="shop-checkout_name-add">${deliveryName.description}</span>
                    </div>

                  </div>
                </div>


                <div class="shop-confirm_ico">
                  <span class="s-confirm-delivery"></span>
                </div>

              </div>



              <div class="shop-confirm_i">

                <div class="shop-checkout">
                  <div class="shop-checkout_i">

                    <div class="cart-short-header">
                      <div class="cart-short-header_head">
                        Адрес <c:choose><c:when test="${isPickup}">самовывоза</c:when><c:otherwise>доставки</c:otherwise></c:choose>
                      </div>
                      <a href="/payment/shopping-cart-contacts" class="cart-short-header_edit-link">изменить</a>
                    </div>

                    <div class="shop-checkout_lbl">
                      <span class="shop-checkout_name">${deliveryAddress}</span>
                    </div>

                  </div>
                </div>


                <div class="shop-confirm_ico">
                  <span class="s-confirm-map"></span>
                </div>

              </div>
          </c:if>


          <div class="shop-confirm_i">

            <div class="shop-checkout">
              <div class="shop-checkout_i">

                <div class="cart-short-header">
                  <div class="cart-short-header_head">
                    Способ оплаты
                  </div>
                  <a href="/payment/shopping-cart-payment" class="cart-short-header_edit-link">изменить</a>
                </div>

                <div class="shop-checkout_lbl">

                  <span class="shop-checkout_name">${paymentSystem}</span>
                </div>

              </div>
            </div>


            <div class="shop-confirm_ico">
              <span class="s-confirm-payment"></span>
            </div>

          </div>
        </div>



        <div class="shop-payment-term">
          Нажимая «Перейти к оплате» вы соглашаетесь с <a href="/offer">условиями предоставления сервиса</a>
        </div>

        <div class="cart-action-block">
          <div class="cart-action">
            <c:if test="${promocodeEnabled}">
            <div class="cart-action_cost">
              <div class="cart-action_cost_lbl">
                Введен промокод
              </div>
              <c:if test="${paymentModel.discountAmount > 0}">
              <div class="cart-action_cost_val">
                <span class="cart-action_cost_promocode">скидка <fmt:formatNumber value="${paymentModel.discountAmount}"  type="number"/><sup class="b-rub">Р</sup></span>
              </div>
              </c:if>
            </div>
            </c:if>

            <div class="cart-action_cost">
              <div class="cart-action_cost_lbl">
                Сумма заказа
              </div>
              <div class="cart-action_cost_val">
                ${paymentModel.donateAmount + contacts.deliveryPrice - paymentModel.discountAmount}<sup class="b-rub">Р</sup>
              </div>
            </div>

            <form id="shop-confirm-order-form" action="/payment/shopping-cart-order" method="POST" class="cart-action_btn">
              <button type="submit" class="shop-btn shop-btn-primary js-shop-confirm-order-button">Подтвердить оформление  заказа</button>
            </form>
          </div>
        </div>


      </div>
    </div>

  </div>
  <%@ include file="/WEB-INF/jsp/includes/shop-footer.jsp" %>
</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
<head>
    <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
    <%@ include file="/WEB-INF/jsp/includes/head.jsp" %>
    <c:set var="headTitle" value="Магазин Планета" />
    <%@ include file="/WEB-INF/jsp/includes/metas-welcome.jsp" %>

    <script type="text/javascript">
        $(document).ready(function() {
            Banner.init('${mainAppUrl}', workspace.appModel.get('myProfile').get('isAuthor'));

            var welcomeRedirectUrl = function (cat) {
                return '/products/search.html?productTagsMnemonics=' + cat
            };


            var viewType = Products.Views.HorizontalProductListView;

            var modelType = ProductSearch.Models.MainNew.extend({
                query: '',
                limit: 3
            });

            var addProducts = function(productTagName) {
                var model = new modelType({
                    productTags: ["WELCOME_" + productTagName]
                });

                model.filterChanged();

                var anchor = '.js-anchor-for-products-' + productTagName.toLowerCase();

                var ref = $(anchor).parent().parent().parent().find('a.shop-btn-default');
                ref.attr("href", welcomeRedirectUrl(productTagName));

                var view = new viewType({
                    el: anchor,
                    collection: model.getCollection()
                });

                view.render();
            };

            addProducts("BOOKS");
            addProducts("CLOTHES");
            addProducts("MUSIC");
            addProducts("EXCLUSIVE");
            addProducts("CHARITY");
            addProducts("CROWDGOODS");


            var model = new modelType({
                productTags: ["WELCOME_NEW"],
                limit: 6
            });

            model.filterChanged();

            var anchor = '.js-anchor-for-products-new';

            var view = new viewType({
                el: anchor,
                collection: model.getCollection()
            });

            view.render();
        });
    </script>
</head>

<body class="grid-1200 shop-page">

<%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>

<div id="global-container" class="hidden">
    <div class="wrap">
        <div class="col-12">
            <%@include file="/WEB-INF/jsp/includes/shop-header.jsp" %>

            <div class="shop-offer">
                <div class="wrap-row ">

                    <div class="col-9">
                        <div class="js-banner-shop-main-big"></div>
                    </div>
                    <div class="col-3 sidebar-banner-js">
                    </div>
                </div>
            </div>

            <div class="shop-welcome-list">

                <div class="shop-welcome-list_i">

                    <div class="wrap-row">
                        <div class="col-3">

                            <div class="shop-welcome-list_info">
                                <div class="shop-category">
                                    <div class="shop-category_name shop-category_name__m">
                                        Эксклюзив
                                    </div>
                                    <div class="shop-category-links">
                                        <div class="shop-category-links_ico">
                                            <img src="//${hf:getStaticBaseUrl("")}/images/shop/cat-star.png">
                                        </div>
                                        <div class="shop-category-links_name">
                                            Лучшее:
                                        </div>

                                        <div class="shop-category-links_list">
                                            <div class="shop-category-links_i">
                                                <a href="/products/search.html?query=&productTagsMnemonics=ART&sortOrder=SORT_BY_TIME_ADDED_DESC" class="shop-category-links_link">
                                                    Искусство
                                                </a>
                                            </div>
                                            <div class="shop-category-links_i">
                                                <a href="/products/search.html?query=&productTagsMnemonics=EXCLUSIVE&productTagsMnemonics=CLOTHES&sortOrder=SORT_DEFAULT" class="shop-category-links_link">
                                                    Вещи
                                                </a>
                                            </div>
                                            <div class="shop-category-links_i">
                                                <a href="/products/search.html?query=&productTagsMnemonics=CDEXCLUSIVE&sortOrder=SORT_DEFAULT" class="shop-category-links_link">
                                                    Диски
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-9">
                            <div class="shop-welcome-list_products">

                                <div class="product-card product-card__3 js-anchor-for-products-exclusive">

                                </div>

                            </div>
                        </div>

                        <div class="shop-welcome-list_all">
                            <a href="#" class="shop-btn shop-btn-default">Показать все</a>
                        </div>
                    </div>

                </div>

                <div class="shop-welcome-list_i">

                    <div class="wrap-row">
                        <div class="col-3">

                            <div class="shop-welcome-list_info">
                                <div class="shop-category">
                                    <div class="shop-category_name">
                                        Вещи
                                    </div>

                                    <div class="shop-category-links">
                                        <div class="shop-category-links_ico">
                                            <img src="//${hf:getStaticBaseUrl("")}/images/shop/cat-size.png">
                                        </div>
                                        <div class="shop-category-links_name">
                                            Популярные:
                                        </div>

                                        <div class="shop-category-links_list">
                                            <div class="shop-category-links_i">
                                                <a href="/products/search.html?query=Футболки" class="shop-category-links_link">
                                                    Футболки
                                                </a>
                                            </div>
                                            <div class="shop-category-links_i">
                                                <a href="/products/search.html?query=Авоськи" class="shop-category-links_link">
                                                    Авоськи
                                                </a>
                                            </div>
                                            <div class="shop-category-links_i">
                                                <a href="/products/search.html?productTagsMnemonics=CERAMICS" class="shop-category-links_link">
                                                    Керамика
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-9">
                            <div class="shop-welcome-list_products">

                                <div class="product-card product-card__3 js-anchor-for-products-clothes">

                                </div>

                            </div>
                        </div>

                        <div class="shop-welcome-list_all">
                            <a href="#" class="shop-btn shop-btn-default">Показать все</a>
                        </div>
                    </div>


                </div>

                <div class="shop-welcome-list_i">

                    <div class="wrap-row">
                        <div class="col-3">

                            <div class="shop-welcome-list_info">
                                <div class="shop-category">
                                    <div class="shop-category_name">
                                        Музыка
                                    </div>

                                    <div class="shop-category-links">
                                        <div class="shop-category-links_ico">
                                            <img src='//${hf:getStaticBaseUrl("")}/images/shop/cat-popular.png'>
                                        </div>

                                        <div class="shop-category-links_name">
                                            Популярные:
                                        </div>

                                        <div class="shop-category-links_list">
                                            <div class="shop-category-links_i">
                                                <a href="/products/search.html?query=Диски" class="shop-category-links_link">
                                                    Диски
                                                </a>
                                            </div>
                                            <div class="shop-category-links_i">
                                                <a href="/products/search.html?query=футболки" class="shop-category-links_link">
                                                    Футболки
                                                </a>
                                            </div>
                                            <div class="shop-category-links_i">
                                                <a href="/products/search.html?referrerIds=20034" class="shop-category-links_link">
                                                    Lumen
                                                </a>
                                            </div>
                                            <div class="shop-category-links_i">
                                                <a href="/products/search.html?referrerIds=27694" class="shop-category-links_link">
                                                    Ундервуд
                                                </a>
                                            </div>
                                            <div class="shop-category-links_i">
                                                <a href="/products/search.html?referrerIds=28134" class="shop-category-links_link">
                                                    Animal Джаz
                                                </a>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-9">
                            <div class="shop-welcome-list_products">

                                <div class="product-card product-card__3 js-anchor-for-products-music">

                                </div>

                            </div>
                        </div>

                        <div class="shop-welcome-list_all">
                            <a href="#" class="shop-btn shop-btn-default">Показать все</a>
                        </div>
                    </div>


                </div>



                <div class="shop-welcome-list_i">

                    <div class="wrap-row">
                        <div class="col-3">

                            <div class="shop-welcome-list_info">
                                <div class="shop-category">
                                    <div class="shop-category_name">
                                        Книги
                                    </div>

                                    <div class="shop-category-links">
                                        <div class="shop-category-links_ico">
                                            <img src='//${hf:getStaticBaseUrl("")}/images/shop/cat-popular.png'>
                                        </div>
                                        <div class="shop-category-links_name">
                                            Популярные авторы:
                                        </div>

                                        <div class="shop-category-links_list">
                                            <div class="shop-category-links_i">
                                                <a href="/products/search.html?query=Михаил+Алдашин" class="shop-category-links_link">
                                                    Михаил Алдашин
                                                </a>
                                            </div>
                                            <div class="shop-category-links_i">
                                                <a href="/products/search.html?query=Иртеньев" class="shop-category-links_link">
                                                    Игорь Иртеньев
                                                </a>
                                            </div>
                                            <div class="shop-category-links_i">
                                                <a href="/products/search.html?query=Шендерович" class="shop-category-links_link">
                                                    Виктор Шендерович
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-9">
                            <div class="shop-welcome-list_products">

                                <div class="product-card product-card__3 js-anchor-for-products-books"></div>
                            </div>
                        </div>

                        <div class="shop-welcome-list_all">
                            <a href="#" class="shop-btn shop-btn-default">Показать все</a>
                        </div>
                    </div>

                </div>

            </div>

            <div class="shop-related-product">
                <div class="shop-related-product_head">
                    Новинки
                </div>

                <div class="shop-related-product_list">

                    <div class="product-card product-card__small js-anchor-for-products-new">

                    </div>

                </div>
            </div>

        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/includes/shop-footer.jsp" %>
</body>
</html>

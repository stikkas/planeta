<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <%@ include file="/WEB-INF/jsp/includes/generated/doctype-header.jsp" %>
    <head>
        <title>Оплата</title>
        <%@include file="/WEB-INF/jsp/includes/generated/stat-counters-async.jsp"%>
        <%@ include file="/WEB-INF/jsp/includes/head.jsp" %>
    </head>

<body class="grid-1200 shop-page">

<%@ include file="/WEB-INF/jsp/includes/generated/irma/header.jsp" %>
<%@ include file="/WEB-INF/jsp/includes/generated/irma/main-nav.jsp" %>

<div id="global-container">
    <div class="wrap">
        <div class="col-12">
            <%@include file="/WEB-INF/jsp/includes/shop-header.jsp" %>
            <div class="wrap-row">
                <div class="col-3">

                    <div class="shop-info-head">
                        Оплата
                    </div>



                    <div class="shop-info-payment-system">
                        <div class="shop-info-payment-system_icons">
                            <img src="//${hf:getStaticBaseUrl("")}/images/shop/visa.png">
                            <img src="//${hf:getStaticBaseUrl("")}/images/shop/master-card.png">
                            <img src="//${hf:getStaticBaseUrl("")}/images/shop/mir-card.png">
                        </div>
                        <div class="shop-info-payment-system_text">
                            К оплате принимаются банковские карты платежных систем
                        </div>
                    </div>

                </div>





                <div class="col-9">

                    <div class="shop-info-main">

                        <h3>Вы можете оплатить заказ:</h3>


                        <div class="shop-info-hero shop-info-hero__other">
                            <div class="shop-info-hero_list">
                                <div class="wrap-row">

                                    <div class="col-3">
                                        <div class="shop-info-hero_i">
                                            <div class="shop-info-hero_ico">
                                                <span class="s-product-info-card"></span>
                                            </div>

                                            <div class="shop-info-hero_cont">
                                                <div class="shop-info-hero_name">
                                                    банковскими
                                                    <br>
                                                    картами
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-4">
                                        <div class="shop-info-hero_i">
                                            <div class="shop-info-hero_ico">
                                                <span class="s-product-info-e-money"></span>
                                            </div>

                                            <div class="shop-info-hero_cont">
                                                <div class="shop-info-hero_name">
                                                    электронными
                                                    <br>
                                                    деньгами
                                                </div>
                                                <div class="shop-info-hero_add">
                                                    Qiwi-кошелек, Яндекс.Деньги, WebMoney
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-4">
                                        <div class="shop-info-hero_i">
                                            <div class="shop-info-hero_ico">
                                                <span class="s-product-info-e-bank"></span>
                                            </div>

                                            <div class="shop-info-hero_cont">
                                                <div class="shop-info-delivery_name">
                                                    через
                                                    <br>
                                                    интернет-банкинг
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>



                        <div class="shop-info-atten">
                            <div class="shop-info-atten_ico">
                                <span class="s-icon s-icon-exclamation-outline"></span>
                                Обратите внимание:
                            </div>
                            <div class="shop-info-atten_cont">
                                Заказ поступает в работу только после получения автоматического подтверждения платежа.
                                <br>
                                Все расчеты производятся в рублях.
                            </div>
                        </div>


                        <hr>


                        <h3>Возврат денег после получения заказа</h3>

                        <p>Денежные средства возвращаются пользователю только после получения магазином возвращаемого товара и заявления на возврат денежных средств. Подробнее о процедуре возврата &ndash; в разделе <a href="/exchange">«Возврат и обмен»</a>.</p>
                        <p>Возврат товара проводится следующими способами:</p>

                        <ul>
                            <li>
                                лично в офисе магазина по адресу 129090, г. Москва, Проспект Мира, д. 19, стр. 3, этаж 3, офис Planeta.ru
                            </li>
                            <li>
                                почтовым отправлением на адрес магазина: ООО "Магазин Планета", 129090, Москва, а/я 25 (стоимость доставки оплачивает покупатель за исключением случаев возврата товара ненадлежащего качества)
                            </li>
                        </ul>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<%@ include file="/WEB-INF/jsp/includes/shop-footer.jsp" %>
</body>
</html>

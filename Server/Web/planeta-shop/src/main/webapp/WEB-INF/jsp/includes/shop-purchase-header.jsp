<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="p" uri="http://planeta.ru/tags" %>

<div class="shop-order_header">
    <div class="shop-order_head">
        Ваш заказ
    </div>

    <div class="shop-order_tel">
        <div class="shop-order-tel">
            <div class="shop-order-tel_tel">
                <span class="shop-order-tel_code">+7 (495) </span>181-06-06
            </div>
            <div class="shop-order-tel_hours">
                ПН-ПТ с 11:00 до 20:00, СБ-ВС - выходной
            </div>
        </div>
    </div>
</div>

<p:if test="${empty purchaseStep}">
    <c:set var="purchaseStep" value="0" />
</p:if>

<div class="shop-order-steps">
    <div class="shop-order-steps_i <c:if test="${purchaseStep == 0}">active</c:if>">
        Ваш заказ
    </div>
    <div class="shop-order-steps_i <c:if test="${purchaseStep == 1}">active</c:if>">
        1. Способ доставки
    </div>
    <div class="shop-order-steps_i <c:if test="${purchaseStep == 2}">active</c:if>">
        2. Способ оплаты
    </div>
    <div class="shop-order-steps_i <c:if test="${purchaseStep == 3}">active</c:if>">
        3. Подтверждение
    </div>
    <div class="shop-order-steps_i <c:if test="${purchaseStep == 4}">active</c:if>">
        4. Заказ оформлен
    </div>
</div>
<? $title = 'Видеоверсия спектакля Евгения Гришковца “+1”: история успеха на planeta.ru' ?>
<? require 'includes/header.php'; ?>

<div class="post">
    <div class="wrap">
        <a class="post-back-link" href="index.php">Истории успеха</a>
        <div class="post-title minionpro-boldit">Создание видеоверсии спектакля Евгения Гришковца “+1”</div>
        <div class="post-main">

            <div class="post-meta">
                <div class="post-meta-item collected">
                    <div class="p-meta-title proxima-reg">Проект собрал</div>
                    <div class="p-meta-num helveticaneue-bold">925 650 <span class="b-rub">Р</span></div>
                </div>
                <div class="post-meta-item target">
                    <div class="p-meta-title proxima-reg">Цель была</div>
                    <div class="p-meta-num helveticaneue-bold">900 000 <span class="b-rub">Р</span></div>
                </div>
                <div class="post-meta-item date">
                    <div class="p-meta-title proxima-reg">Сроки проведения</div>
                    <div class="p-meta-date-item">
                        <div class="pm-date-item-day">20</div>
                        <div class="pm-date-item-moth">августа</div>
                        <div class="pm-date-item-year">2012</div>
                    </div>
                    <div class="p-meta-date-item">
                        <div class="pm-date-item-day">15</div>
                        <div class="pm-date-item-moth">мая</div>
                        <div class="pm-date-item-year">2013</div>
                    </div>
                </div>
                <div class="post-meta-item members">
                    <div class="p-meta-title helveticaneue-bold">966</div>
                    <div class="p-meta-num proxima-reg">Акционеров приняли участие</div>
                </div>
            </div>

            <img class="post-big-img" src="images/5diez/big-image.jpg">
            <div class="post-middle">
                <? require 'includes/share.php' ?>
                <div class="post-content">

                    <div class="post-content-manager">
                        <div class="p-content-manager-ava">
                            <img src="images/5diez/ava-artist.jpg">
                        </div>
                        <div class="p-content-manager-name minionpro-mediumit">Ирина Юткина</div>
                        <div class="p-content-manager-role">продюсер</div>
                        <div class="p-content-manager-quote proxima-regular-it">
                            Честно говоря, я&nbsp;каждый день заглядываю на&nbsp;страницу Planeta.ru, и&nbsp;для меня
                            это такая азартная штука. Потому что это воспринимается как чудо. То&nbsp;есть, люди не&nbsp;только
                            купили билеты на&nbsp;съемку видеоверсии&nbsp;&mdash; ведь она состоялась 8 декабря 2012&nbsp;г. &mdash;&nbsp;каждый
                            день они тратят свои деньги на&nbsp;то, чтобы поддержать создание видеоверсии и&nbsp;купить
                            DVD с&nbsp;автографом или электронный вариант спектакля. Учитывая развитие пиратства и&nbsp;торрентов,
                            я&nbsp;реально воспринимаю это как чудо и&nbsp;очень вам благодарна.
                        </div>
                    </div>

                    <br>

                    <div class="h-video">
                        <iframe width="720" height="405" frameborder="0" allowfullscreen="" src="https://tv.planeta.ru/video-frame?profileId=22956&amp;videoId=21765&amp;autostart=false"></iframe>
                    </div>

                    <br><br>

                    <div class="post-content-text proxima-reg">
                        Одним из&nbsp;бонусов проекта стал билет на&nbsp;выступление и&nbsp;тот самый сантиметр. Те, кто
                        уже успел посмотреть спектакль &laquo;+1&raquo;, знают, какую роль для человечества играет
                        швейный сантиметр, который Евгений Гришковец использует в&nbsp;своей постановке. Именно его
                        автор лично подарил акционеру, который приобрел соответствующую акцию.

                        <div class="p-content-notice helveticaneue-bold">И «акционеры» получили весомое преимущество:
                            Создавая видеоверсию спектакля Евгений Гришковец получил больше, чем &laquo;+1&raquo;,&nbsp;&mdash; он&nbsp;получил в&nbsp;плюс сразу всю &laquo;Планету&raquo;!
                        </div>

                        <div class="h-video">
                            <iframe width="720" height="405" frameborder="0" allowfullscreen="" src="https://tv.planeta.ru/video-frame?profileId=22956&amp;videoId=21766&amp;autostart=false"></iframe>
                        </div>

                    </div>
                    <div class="post-content-manager">
                        <div class="p-content-manager-ava">
                            <img src="images/content/history-manager-ava.jpg">
                        </div>
                        <div class="p-content-manager-name minionpro-mediumit">Василина Горовая</div>
                        <div class="p-content-manager-role">куратор проекта</div>
                        <div class="p-content-manager-quote proxima-regular-it">
                            Бывают такие люди, узнавая которых, хочется в&nbsp;
                            <nobr>чем-то</nobr>
                            походить на&nbsp;них. Из&nbsp;общения с&nbsp;ними желаешь почерпнуть
                            <nobr>что-то</nobr>
                            важное, большое, хорошее. Мне верится, что после каждой встречи с&nbsp;Ириной Юткиной
                            (продюсером проектов Евгения Гришковца) я&nbsp;становлюсь лучше. Ира&nbsp;&mdash;
                            удивительно интересный человек, настоящий профессионал и&nbsp;просто чудесная девушка. Я&nbsp;счастлива,
                            что мне представился шанс поработать с&nbsp;ней и&nbsp;ее&nbsp;командой. Если&nbsp;бы у&nbsp;меня
                            была возможность обнять и&nbsp;расцеловать Евгения Валерьевича лично, я&nbsp;бы это сделала.
                            Но&nbsp;скромность не&nbsp;позволит, поэтому просто низкий поклон ему за&nbsp;то, что
                            поверил в&nbsp;нас и&nbsp;поддержал на&nbsp;самом старте. Это для нас очень важно. И,
                            конечно, СПАСИБО всем тем, кто стал акционером проектов Евгения Гришковца на&nbsp;&laquo;Планете&raquo;.
                            <nobr>По-моему</nobr>
                            , мы&nbsp;все вместе совершили маленькую революцию. Одну из&nbsp;самых благих революций в&nbsp;искусстве.
                        </div>
                    </div>
                </div>

                <div class="post-milestones">
                    <div class="post-milestones-img">
                        <img src="images/5diez/milestones.jpg">
                    </div>
                    <div class="post-milestones-list">
                        <div class="p-milestones-list-title minionpro-mediumit">Основные вехи</div>
                        <div class="p-milestones-list-items">
                            <div class="pm-list-items-item">
                                <div class="pml-items-item-date proxima-bold">8 декабря 2012</div>
                                <div class="pml-items-item-text proxima-reg">прошла съемка спектакля</div>
                            </div>
                            <div class="pm-list-items-item">
                                <div class="pml-items-item-date proxima-bold">25 марта 2013</div>
                                <div class="pml-items-item-text proxima-reg">акционерам «Планеты» отправилась готовая версия</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="post-content">
                    <a class="post-another-posts minionpro-semiboldit" href="index.php">Другие успешные проекты</a>
                </div>
            </div>
        </div>

    </div>
</div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
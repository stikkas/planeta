<?
$title = 'Best-альбом группы &laquo;Сансара&raquo; + концерт и&nbsp;съемка';

$collected = 220140;
$target = 200000;

$dateDuration = '3 месяца 4 дня';

$startDay = 28;
$startMonth = 'октября';
$startYear = 2013;

$endDay = 1;
$endMonth = 'февраля';
$endYear = 2014;

$members = 101;

$projectLink = 'https://planeta.ru/campaigns/1769';

$collected = number_format($collected, 0, '.', ' ');
$target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

    <div class="post" itemscope itemtype="http://schema.org/CreativeWork">
        <div class="wrap">
            <a class="post-back-link" href="index.php">Истории успеха</a>

            <div itemprop="name" class="post-title minionpro-boldit"><?=$title?></div>
            <div class="post-main">

                <? require 'includes/post-meta.php'; ?>

                <img itemprop="image" class="post-big-img" src="images/sansara/check.jpg">

                <div class="post-middle">
                    <? require 'includes/share.php' ?>
                    <div class="post-content">
                        <div itemprop="description" class="post-content-text proxima-reg">
                            Представьте, что вам предлагают стать соавтором &laquo;серии перерождений любимой группы&raquo;. Интригует? Еще&nbsp;бы! Именно такое предложение поступило от&nbsp;&laquo;Сансары&raquo; к&nbsp;своим поклонникам, накануне выпуска дебютного <nobr>Best-альбома</nobr>, который стал лишь первой частью избранного, записанного за&nbsp;последние пять лет существования коллектива.
                            <br><br>
                            Итогом такого заманчивого предложения стало &laquo;сотрудничество&raquo;, в&nbsp;процессе которого появился не&nbsp;только прекрасный альбом, но&nbsp;и&nbsp;запись концерта, на&nbsp;котором состоялась презентация последней работы.
                        </div>

                        <div class="post-content-text proxima-reg">
                            <div class="p-content-notice helveticaneue-bold">
                                Не&nbsp;только <nobr>&laquo;Best-альбом&raquo;</nobr>, но&nbsp;и&nbsp;<nobr>best-акции</nobr>. Своим акционерам &laquo;Сансара&raquo; предложила действительно нетрадиционные бонусы за&nbsp;участие в&nbsp;проекте. Регулировать степень нетрадиционности можно было самостоятельно: от&nbsp;картины, написанной персонально Александром Гагариным, до&nbsp;интеграции в&nbsp;<nobr>промо-кампанию</nobr> альбома.
                            </div>
                        </div>


                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/sansara/ava-artist.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Саша Гагарин</div>
                            <div class="p-content-manager-role">«Сансара»</div>
                            <div class="p-content-manager-quote">
                                Предложение участвовать в&nbsp;краудфандинговой истории, доселе нам неведомой, поступило от&nbsp;Планеты. А&nbsp;мысли по&nbsp;изданию Best’а&nbsp;параллельно шли и&nbsp;до&nbsp;этого. Группа &laquo;Сансара&raquo; еще ни&nbsp;разу не&nbsp;выпускала своих сборников. Так что все просто совпало. Когда готовится релиз, обязательны <nobr>какие-то</nobr> расходы, все музыканты это знают. В&nbsp;нашем случае сбор средств является, в&nbsp;сущности, предзаказом новой пластинки с&nbsp;расширенными опциями. И, кажется, у&nbsp;нас все получилось! Честно говоря, я&nbsp;удивлен. Приятно!
                            </div>
                        </div>


                        <br><br>

                        <div class="post-milestones large-milestones">
                            <div class="post-milestones-list clearfix">
                                <div class="p-milestones-list-title minionpro-mediumit">Вехи проекта</div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">28 октября 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Проект группы «Сансара» запущен на Планете.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">14 ноября 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Все акционеры проекта получают в виде приятного бонуса за участие в проекте <a href="https://planeta.ru/sansara/blog/121498" target="_blank">предыдущие альбомы и EP</a> группы.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">10 декабря 2013</div>
                                        <div class="pml-items-item-text proxima-reg">По просьбе акционеров проекта группа <a href="https://planeta.ru/sansara/blog/122055" target="_blank">решает выпустить</a> &laquo;The Best&raquo; на виниле.</div>
                                    </div>
                                </div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-text proxima-reg">
                                            <img src="images/sansara/milestones.jpg">
                                        </div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">26 июня 2014</div>
                                        <div class="pml-items-item-text proxima-reg">Состоялась московская презентация группы «Сансара». Онлайн-трансляция концерта была проведена на Планете.</div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="post-content-manager mrg-t-50 mrg-b-50">
                            <div class="p-content-manager-ava">
                                <img src="images/kurator/ava-polina.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Полина Максимова</div>
                            <div class="p-content-manager-role">куратор проекта</div>
                            <div class="p-content-manager-quote">
                                У&nbsp;меня есть ощущение, что некоторые места действительно можно назвать &laquo;местами силы&raquo;. Судя по&nbsp;всему, именно таким является Екатеринбург, в&nbsp;котором <nobr>какое-то</nobr> невероятное количество талантливых музыкантов. Ребята из&nbsp;группы &laquo;Сансара&raquo; так&nbsp;же оказались подтверждением этой теории. На&nbsp;протяжении трех месяцев проекта они не&nbsp;уставали придумывать необычные и&nbsp;креативные способы напоминания о&nbsp;проекте, так что, то&nbsp;что он&nbsp;состоялся, не&nbsp;стало ни&nbsp;для кого сюрпризом. Отдельно хочется отметить, что аудитория &laquo;Сансары&raquo;, как та, что посещает их&nbsp;концерты, так и&nbsp;та, что принимала участие в&nbsp;проекте, одна из&nbsp;самых приятных, с&nbsp;которой удалось поработать. В&nbsp;общем, со&nbsp;всех сторон удачный проект!
                            </div>
                        </div>

                    </div>

                </div>
            </div>
                <? include 'includes/index-data.php'; ?>

        </div>
    </div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
<?
$title = 'Создание виртуального музея резных наличников';
$collected = 118280;

$dateDuration = '3 месяца 24 дня';

$startDay = 6;
$startMonth = 'ноября';
$startYear = 2012;

$endDay = 2;
$endMonth = 'марта';
$endYear = 2013;

$members = 70;

$projectLink = 'https://planeta.ru/campaigns/263';

$collected = number_format($collected, 0, '.', ' ');
$target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

    <div class="post" itemscope itemtype="http://schema.org/CreativeWork">
        <div class="wrap">
            <a class="post-back-link" href="index.php">Истории успеха</a>

            <div itemprop="name" class="post-title minionpro-boldit">Создание виртуального музея резных наличников
            </div>
            <div class="post-main">

                <? require 'includes/post-meta.php'; ?>

                <img itemprop="image" class="post-big-img" src="images/nalichniki/nalichniki-check.jpg">

                <div class="post-middle">
                    <? require 'includes/share.php' ?>
                    <div class="post-content">
                        <div itemprop="description" class="post-content-text proxima-reg">
                            Старинные русские постройки привлекают даже тех, кто не&nbsp;особо интересуется архитектурой&nbsp;&mdash; прежде всего, тем, что в&nbsp;них сохраняются элементы нашей культуры, которые редко встретишь в&nbsp;повседневной жизни. Это своеобразное искусство, разительно отличающееся от&nbsp;современных типовых зданий&nbsp;&mdash; именно на&nbsp;это делал упор Иван Хафизов, создавая свой виртуальный музей резных наличников. Странствуя по&nbsp;русским городам и&nbsp;деревням, он&nbsp;фотографировал наиболее интересные образцы этих элементов традиционного декора, собрав уникальную коллекцию, отражающую дух старины. Вместе с&nbsp;ним путешествие смогли совершить и&nbsp;пользователи &laquo;Планеты&raquo;, при поддержке которых автор создал настоящий виртуальный музей наличников.
                        </div>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/nalichniki/ava-artist.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Иван Хафизов</div>
                            <div class="p-content-manager-role">автор проекта</div>
                            <div class="p-content-manager-quote">
                                &laquo;Успешное народное финансирование скажет о&nbsp;проекте лучше всякого руководства любой компании. Потому что если десять тысяч человек поддержат проект, то&nbsp;на&nbsp;любой вопрос можно смело отвечать&nbsp;&mdash; да!<br />
&mdash;&nbsp;да, резные наличники это интересно, красиво и&nbsp;это то, что хотелось&nbsp;бы увидеть и&nbsp;сохранить!<br />
&mdash;&nbsp;да, интерес к&nbsp;нашим традициям (русским, чувашским, татарским, башкирским, карельским, бурятским&nbsp;&mdash; <nobr>всем-всем</nobr> нашим)&nbsp;&mdash; жив!<br />
&mdash;&nbsp;да, народное финансирование, если от&nbsp;него выигрывают все&nbsp;&mdash; это реальный инструмент, который можно и&nbsp;нужно использовать всем на&nbsp;благо!&raquo;
                            </div>
                        </div>

                        <div class="post-content-text proxima-reg mrg-b-50 pdg-b-0">
                            <div class="p-content-notice helveticaneue-bold">
                                В&nbsp;качестве ответной благодарности Иван Хафизов предложил своим акционерам уникальную картинку для &laquo;Рабочего стола&raquo;&nbsp;&mdash; карту России, составленную из&nbsp;старинных резных наличников; набор из&nbsp;12 авторских открыток с&nbsp;изображением самых красивых наличников; личную встречу с&nbsp;акционерами, на&nbsp;которой автор рассказал об&nbsp;истории наличников и&nbsp;своих экспедициях; звания &laquo;Спонсор города&raquo; и&nbsp;&laquo;Спонсор области&raquo;.
                            </div>
                        </div>
                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/kurator/ava-sorokina.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Мария Сорокина</div>
                            <div class="p-content-manager-role">куратор проекта</div>
                            <div class="p-content-manager-quote">
                                &laquo;Иван Хафизов обладает потрясающей энергией, и&nbsp;у&nbsp;него получается заряжать этой энергией всех окружающих. Я&nbsp;желаю Ивану успешного развития виртуального музея наличников!&raquo;
                            </div>
                        </div>

                    </div>

                </div>
            </div>
                <? include 'includes/index-data.php'; ?>

        </div>
    </div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
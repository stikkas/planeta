<?
$title = '&laquo;Запись нового альбома &laquo;Торба-на-круче&raquo;';

$collected = 750000;
$target = 350000;

$dateDuration = '3 месяца 1 день';

$startDay = 14;
$startMonth = 'сентября';
$startYear = 2013;

$endDay = 15;
$endMonth = 'декабря';
$endYear = 2013;

$members = 321;

$projectLink = 'https://planeta.ru/campaigns/1484';

$collected = number_format($collected, 0, '.', ' ');
$target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

    <div class="post" itemscope itemtype="http://schema.org/CreativeWork">
        <div class="wrap">
            <a class="post-back-link" href="index.php">Истории успеха</a>

            <div itemprop="name" class="post-title minionpro-boldit"><?=$title?></div>
            <div class="post-main">

                <? require 'includes/post-meta.php'; ?>

                <img itemprop="image" class="post-big-img" src="images/torba/check.jpg">

                <div class="post-middle">
                    <? require 'includes/share.php' ?>
                    <div class="post-content">
                        <div itemprop="description" class="post-content-text proxima-reg">
                            Снарядив с&nbsp;сентября по&nbsp;декабрь 2013 года с&nbsp;помощью акционеров проекта нашу &laquo;Юнону-и- Авось&raquo;, мы&nbsp;отправились к&nbsp;новым берегам, и, может быть, к&nbsp;новым частям света. Перед началом этого путешествия они казались такими близкими и&nbsp;доступными, такими осязаемыми, но&nbsp;все&#776; оказалось совсем не&nbsp;так просто. Были и&nbsp;бури и&nbsp;шторма, ломался компас, мы&nbsp;сбивались с&nbsp;курса, на&nbsp;нас нападали электрические скаты и&nbsp;нарвалы, экипаж настигали болезни:)<br><br>Но&nbsp;впервые за&nbsp;все путешествия, коих было уже не&nbsp;мало, мы&nbsp;ощущаем такое спокойствие и&nbsp;уверенность. Такую чистую радость от&nbsp;того, что мы&nbsp;делаем.<br><br>Это всё стало возможным благодаря вере в&nbsp;нас тех людей, которые стали участниками проекта, которые помогли нашему кораблю отправиться в&nbsp;путь.<br><br>И&nbsp;новые земли близко. Уже прилетела и&nbsp;села на&nbsp;мачту нашего корабля птица с&nbsp;оливковой ветвью в&nbsp;клюве. А, значит, мы&nbsp;скоро выйдем на&nbsp;берег, тот, на&nbsp;котором ещё не&nbsp;были.<br><br>Мы&nbsp;чувствуем приближение этого момента.<br><br>Скорее всего, то, что мы&nbsp;откроем, появится на&nbsp;всех картах мира осенью 2014 года. Но, несомненно, до&nbsp;этого мы&nbsp;познакомим вас с&nbsp;частью наших открытий. До&nbsp;встреч на&nbsp;Планете!
                        </div>

                        <div class="post-content-text proxima-reg mrg-b-50">
                            <div class="p-content-notice helveticaneue-bold">
                                Для участников проекта <nobr>&laquo;Торба-на-Круче&raquo;</nobr> придумала оригинальные бонусы, начиная с&nbsp;нового альбома, выполненного в&nbsp;форме <nobr>usb-браслета</nobr> и&nbsp;новогоднего meet and greet’a&nbsp;с&nbsp;фуршетом, заканчивая персональной <nobr>видео-открыткои</nobr>&#774; с&nbsp;благодарностью (один из&nbsp;акционеров решил воспользоваться такой возможностью и&nbsp;с&nbsp;помощью любимой группы предложил девушке выйти&nbsp;замуж).
                            </div>
                        </div>


                        <div class="post-milestones large-milestones">
                            <div class="post-milestones-list clearfix">
                                <div class="p-milestones-list-title minionpro-mediumit">Вехи проекта</div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">28 августа 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Группа начала активное анонсирование перед запуском на&nbsp;planeta.ru, объявив тур в&nbsp;поддержку проекта. В&nbsp;рамках тура <a href="https://planeta.ru/torbanakruche/posts/137427">&laquo;На&nbsp;синхронных орбитах&raquo;</a> <nobr>&laquo;Торба-на-круче&raquo;</nobr> посетила 13 городов.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">23 сентября 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Вся необходимая сумма <a href="https://planeta.ru/torbanakruche/blog/120099">собрана</a> в рекордные сроки.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">14 ноября 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Состоялась онлай<nobr>н-трансляция</nobr> юбилейного концерта группы, на&nbsp;котором были продемонстрированы ролики победителей <a href="https://planeta.ru/torbanakruche/blog/121121">конкурса</a>.</div>
                                    </div>
                                </div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">14 сентября 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Запуск проекта на&nbsp;Планете во&nbsp;время концерта в&nbsp;питерском клубе &laquo;Зал ожидания&raquo;.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">31 октября 2013</div>
                                        <div class="pml-items-item-text proxima-reg"><nobr>&laquo;Торба-на-круче&raquo;</nobr> объявляет видео&nbsp;&mdash; <a href="https://planeta.ru/torbanakruche/blog/121121">конкурс</a> на&nbsp;лучшее поздравление группы с&nbsp;днем рождения.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">14 декабря 2013</div>
                                        <div class="pml-items-item-text proxima-reg"><a href="https://planeta.ru/campaigns/1484">Проект</a> завершился, собрав в&nbsp;два раза больше заявленной суммы.</div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="post-content-manager mrg-t-50 mrg-b-50">
                            <div class="p-content-manager-ava">
                                <img src="images/kurator/ava-german.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Ольга Герман</div>
                            <div class="p-content-manager-role">Куратор проекта</div>
                            <div class="p-content-manager-quote">Проект коллектива <nobr>&laquo;Торба-на-Круче&raquo;</nobr>&nbsp;&mdash; один из&nbsp;самых показательных на&nbsp;Планете. Если&nbsp;бы я&nbsp;решила запустить свой проект, то&nbsp;взяла&nbsp;бы опыт группы за&nbsp;основу и&nbsp;образец.<br><br>Ребята начали работу еще до&nbsp;запуска, активно знакомя аудиторию как с&nbsp;идеей краудфандинга и&nbsp;Планетой в&nbsp;целом, так и&nbsp;со&nbsp;своими будущими планами, концертами, активностями, плавно подводя к&nbsp;рассказу о&nbsp;запуске проекта по&nbsp;выпуску альбома. В&nbsp;результате этого и&nbsp;всех движений в&nbsp;рамках проекта необходимая сумма была собрана за&nbsp;неделю! А&nbsp;в&nbsp;финале было собрано уже 214% от&nbsp;требуемых средств!<br><br>Кстати, музыканты не&nbsp;баловали пользователей бонусами по&nbsp;ходу проекта, как часто делают авторы, но&nbsp;не&nbsp;забывали оригинально напоминать о&nbsp;сборе средств, делиться совместными успехами и&nbsp;результатами, проводить конкурсы и&nbsp;добавлять новые акции.<br><br>Одним словом&nbsp;&mdash; отличный проект получился!) Удачи ребятам в&nbsp;выпуске пластинки и&nbsp;будем рады снова видеть вас на&nbsp;Планете!
                            </div>
                        </div>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/torba/ava-torba.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Татьяна Ковалевская</div>
                            <div class="p-content-manager-role">координатор проекта <nobr>&laquo;Торба-на-Круче&raquo;</nobr></div>
                            <div class="p-content-manager-quote">
                                Поскольку проект действительно планировался как своеобразное испытание возможностей современных медиаресурсов, подготовка к&nbsp;нему велась практически в&nbsp;лабораторных условиях и&nbsp;со&nbsp;всей ответственностью перед сопродюсерами будущей пластинки. Хотелось, чтобы всем было интересно включиться в&nbsp;этот процесс, поэтому выбор бонусов был представлен самыми разнообразными &laquo;конфетами&raquo; от&nbsp;персональных видеооткрыток и&nbsp;эксклюзивной атрибутики до&nbsp;песни на&nbsp;&laquo;Бис&raquo; и&nbsp;участии в&nbsp;репетиции группы. Впечатлила география участия: проект заинтересовал людей со&nbsp;всех уголков России от&nbsp;Калининграда до&nbsp;Владивостока, а&nbsp;также из&nbsp;Казахстана, Украины и&nbsp;даже из&nbsp;Европы. Такая феерическая поддержка вывела эту историю в&nbsp;<nobr>Топ-5</nobr> лучших музыкальных проектов &laquo;Планеты&raquo;, и&nbsp;это, без сомнения, очень важный показатель.
                            </div>
                        </div>

                    </div>

                </div>
            </div>
                <? include 'includes/index-data.php'; ?>

        </div>
    </div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
<?
$title = 'ЛАМПАСЫ – запись нового альбома';

$collected = 104900;
$target = 100000;

$dateDuration = '1 месяц 24 дня';

$startDay = 17;
$startMonth = 'июня';
$startYear = 2013;

$endDay = 10;
$endMonth = 'августа';
$endYear = 2013;

$members = 65;

$projectLink = 'https://planeta.ru/campaigns/1101';

$blog_link = "https://planeta.ru/120322/blog/120173 ";
$blog_link_label = "Блог группы &laquo;Лампасы&raquo;";

$collected = number_format($collected, 0, '.', ' ');
$target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

    <div class="post" itemscope itemtype="http://schema.org/CreativeWork">
        <div class="wrap">
            <a class="post-back-link" href="index.php">Истории успеха</a>

            <div itemprop="name" class="post-title minionpro-boldit"><?=$title?></div>
            <div class="post-main">

                <? require 'includes/post-meta.php'; ?>

                <img itemprop="image" class="post-big-img" src="images/lampasy/check.jpg">

                <div class="post-middle">
                    <? require 'includes/share.php' ?>
                    <div class="post-content">
                        <div itemprop="description" class="post-content-text proxima-reg">
                            За&nbsp;11 лет существования группа &laquo;Лампасы&raquo; смогли покорить не&nbsp;только сердца своих слушателей, но&nbsp;и&nbsp;многотысячную аудиторию посетителей фестивалей Kubana и&nbsp;Нашествие, на&nbsp;которых успели неоднократно выступить. За&nbsp;это время также было записано пять альбомов, каждый из&nbsp;которых настолько отличался от&nbsp;предыдущего, что группа закрепила за&nbsp;собой звание самого жанрово разнообразного музыкального коллектива. К&nbsp;своему шестому альбому &laquo;Лампасы&raquo; подошли со&nbsp;всем присущим им&nbsp;оптимизмом и&nbsp;пригласили к&nbsp;сотрудничеству поклонников. Результатом стал &laquo;БОМ. Часть 1&raquo; и&nbsp;десять новых песен, вышедших аккурат за&nbsp;две недели до&nbsp;Нового года. Лучший подарок!
                        </div>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/lampasy/ava-lampas.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Группа &laquo;Лампасы&raquo;</div>
                            <div class="p-content-manager-role">&nbsp;</div>
                            <div class="p-content-manager-quote">
                                Проект на Планете был для нас новым интересным опытом. Мы очень рады, что так много людей откликнулись на наш призыв и помогли нам записать новый альбом! Мы и дальше постараемся радовать вас новыми работами, новыми проектами и новыми акциями.
                            </div>
                        </div>

                        <br><br>


                        <div class="post-content-text proxima-reg mrg-b-50">
                            <div class="p-content-notice helveticaneue-bold">
                                К&nbsp;бонусам для акционеров, &laquo;Лампасы&raquo; также отнеслись со&nbsp;всей серьезностью. Кроме оригинального мерча с&nbsp;символикой группы (майки, трусы и&nbsp;семейные наборы) они предложили участникам сходить вместе на&nbsp;экскурсию по&nbsp;Нижнему Новгороду&nbsp;&mdash; родному городу музыкантов и&nbsp;провести целый день вместе на&nbsp;фестивале &laquo;Нашествие&raquo;.
                            </div>
                        </div>


                        <div class="post-milestones large-milestones">
                            <div class="post-milestones-list clearfix">
                                <div class="p-milestones-list-title minionpro-mediumit">Вехи проекта</div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">17 июня 2013 </div>
                                        <div class="pml-items-item-text proxima-reg">Проект стартовал на planeta.ru.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">30 июля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Лампасы проводят конкурс на лучшее фан-видео для песни &laquo;Лягушка&raquo;.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">2 ноября 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Презентация нового альбома &laquo;Бом Бом&raquo; в клубе Grand Borboun.</div>
                                    </div>
                                </div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">23 июля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Добавлены новые акции для семейных пар со специальным мерчем.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">25 сентября 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Выходит клип на&nbsp;первый сингл с&nbsp;нового альбома &laquo;Лампасов&raquo; &laquo;Руки, губы&raquo;, в&nbsp;съемках которого приняли участие акционеры проекта на&nbsp;planeta.ru.</div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="post-content-manager mrg-t-50 mrg-b-50">
                            <div class="p-content-manager-ava">
                                <img src="images/kurator/ava-vitya.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Виктор Заря</div>
                            <div class="p-content-manager-role">куратор проекта</div>
                            <div class="p-content-manager-quote">Было приятно работать с&nbsp;ребятами из&nbsp;группы &laquo;Лампасы&raquo;. Они максимально серьезно и&nbsp;ответственно подходили к&nbsp;каждому этапу проекта, будь то&nbsp;продумывание акций или рассылка бонусов акционерам. А&nbsp;еще с&nbsp;ними здорово не&nbsp;только обсуждать детали проекта, но&nbsp;и&nbsp;просто общаться после концерта в&nbsp;клубе. Хочется пожелать ребятам удачи, пополнения дружного коллектива поклонников творчества, выпустить ещё кучу пластинок и&nbsp;сыграть миллион концертов!
                            </div>
                        </div>

                    </div>

                </div>
            </div>
                <? include 'includes/index-data.php'; ?>

        </div>
    </div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
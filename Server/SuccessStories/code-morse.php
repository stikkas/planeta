<?
$title = '&laquo;Азбука Морзе&raquo;&nbsp;&mdash; новый тур группы &laquo;Пикник&raquo;';

$collected = 741147;
$target = 500000;

$dateDuration = '4 месяца 9 дней';

$startDay = 23;
$startMonth = 'августа';
$startYear = 2013;

$endDay = 1;
$endMonth = 'января';
$endYear = 2014;

$members = 470;

$projectLink = 'https://planeta.ru/campaigns/1118';

$collected = number_format($collected, 0, '.', ' ');
$target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

    <div class="post" itemscope itemtype="http://schema.org/CreativeWork">
        <div class="wrap">
            <a class="post-back-link" href="index.php">Истории успеха</a>

            <div itemprop="name" class="post-title minionpro-boldit"><?=$title?></div>
            <div class="post-main">

                <? require 'includes/post-meta.php'; ?>

                <img itemprop="image" class="post-big-img" src="images/code-morse/check.jpg">

                <div class="post-middle">
                    <? require 'includes/share.php' ?>
                    <div class="post-content">
                        <div itemprop="description" class="post-content-text proxima-reg">
                            Сколько жителей может жить в&nbsp;сказочном королевстве? Ответ на&nbsp;этот вопрос предстояло выяснить группе &laquo;Пикник&raquo;, которая, запустив проект на&nbsp;Планете, пригласила своих будущих акционеров стать соучастниками невероятного тура.
                            <br>
                            <br>
                            Изначально его идея звучала настолько футуристично, что было сложно понять, как&nbsp;же будет выглядеть итоговый результат. Однако настоящих ценитель творчества не&nbsp;смутили ни&nbsp;кибернетические конструкции, ни&nbsp;телеграфный аппарат, который служит передатчиком зашифрованных сигналов. Они с&nbsp;радостью включились в&nbsp;процесс и&nbsp;поддержали любимую группу. Так жителей королевства стало на&nbsp;470 человек больше!                        </div>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/code-morse/ava-author.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Группа «Пикник»</div>
                            <div class="p-content-manager-role"></div>
                            <div class="p-content-manager-quote">
                                Наверное, не&nbsp;каждому доводилось держать в&nbsp;руках чек. А&nbsp;чек такого размера уж&nbsp;точно. Но&nbsp;благодаря акционерам Планеты, которые откликнулись на&nbsp;позывные Азбуки Морзе, группа &laquo;Пикник&raquo; стала обладателем вот такого документа. И&nbsp;чтобы удержать его в&nbsp;руках собрались не&nbsp;только музыканты группы, но&nbsp;и&nbsp;арлекины и&nbsp;даже двойник заморского мима Марселя Марсо!
                            </div>
                        </div>

                        <br>
                        <br>

                        <div class="post-content-text proxima-reg">
                            <div class="p-content-notice helveticaneue-bold">
                                Пожалуй, проект стал одним из&nbsp;самых щедрых на&nbsp;оригинальные и&nbsp;<nobr>по-настоящему</nobr> уникальные бонусы. Кроме&nbsp;того, он&nbsp;продемонстрировал, насколько всесторонне творческими являются участники группы. Книга, рассказывающая историю тридцатилетнего существования группы, футболки с&nbsp;авторскими эскизами и&nbsp;картины, нарисованные Эдмундом Шклярским, тому пример.
                            </div>
                        </div>

                        <img src="images/code-morse/painting.jpg">

                        <br>
                        <br>
                        <br>
                        <br>

                        <div class="post-milestones large-milestones">
                            <div class="post-milestones-list clearfix">
                                <div class="p-milestones-list-title minionpro-mediumit">Вехи проекта</div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">23 августа 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Проект запущен на Планете.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">10 сентября 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Проект собрал <a href="https://planeta.ru/piknik/blog/119773" target="_blank">половину суммы</a>, в&nbsp;честь чего каждый из&nbsp;акционеров получил сингл с&nbsp;нового альбома &laquo;Азбука Морзе&raquo;.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">24 октября 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Группа <a href="https://planeta.ru/piknik/blog/120950" target="_blank">объявляет</a> неделю эксклюзива для&nbsp;участников проекта. Акционеры становятся первыми зрителями и&nbsp;получают возможность виртуально наблюдать за&nbsp;туром группы.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">22 ноября 2013</div>
                                        <div class="pml-items-item-text proxima-reg">&laquo;Пикник&raquo; предлагает акционерам вписать свое имя в&nbsp;историю и&nbsp;рассказать о&nbsp;том, как начиналась история любви и&nbsp;дружбы с&nbsp;группой. Часть историй впоследствии была опубликована в&nbsp;книге &laquo;Пикник. 30 световых лет&raquo;.</div>
                                    </div>
                                </div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">24 декабря 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Все акционеры проекта получили <a href="https://planeta.ru/piknik/blog/122403" target="_blank">новогодний бонус</a> от&nbsp;группы. А&nbsp;именно песню на&nbsp;стихи Блока &laquo;Белый и&nbsp;пушистый&raquo;. Не&nbsp;обошлось и&nbsp;без чуда: специально для записи этой песни и&nbsp;видео на&nbsp;нее, группа сделала невозможное&nbsp;&mdash; нашла снег в&nbsp;первую неделю декабря!</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">7 марта 2014</div>
                                        <div class="pml-items-item-text proxima-reg">На&nbsp;Планете состоялась <a href="https://tv.planeta.ru/broadcast/225224" target="_blank">трансляция</a> весеннего концерта группы.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">7 июля 2014</div>
                                        <div class="pml-items-item-text proxima-reg">&laquo;Пикник&raquo; запустили <a href="https://planeta.ru/campaigns/7748/" target="_blank">второй проект</a> на&nbsp;Планете в&nbsp;поддержку нового альбома и&nbsp;тура.</div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="post-content-manager mrg-t-50 mrg-b-50">
                            <div class="p-content-manager-ava">
                                <img src="images/kurator/ava-german.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Ольга Герман</div>
                            <div class="p-content-manager-role">куратор проекта</div>
                            <div class="p-content-manager-quote">
                                Одно из&nbsp;самых ярких впечатлений от&nbsp;проекта &laquo;Пикника&raquo;&nbsp;&mdash; это единство и&nbsp;отзывчивость поклонников группы! С&nbsp;каким энтузиазмом участники не&nbsp;только поддерживали, но&nbsp;и&nbsp;распространяли информацию о&nbsp;проекте, делились эмоциями, предлагали акции. Думаю, что второй и&nbsp;последующие проекты группы будут не&nbsp;менее успешны и&nbsp;все благодаря колоссальной поддержке!
                                <br>
                                <br>
                                И, конечно, огромное удовольствие работать с&nbsp;музыкантами! Большая загадка&nbsp;&mdash; как им&nbsp;удается на&nbsp;все 100% (и&nbsp;даже больше) участвовать в&nbsp;проекте, оперативно реагировать на&nbsp;все запросы, не&nbsp;выпадая из&nbsp;активного гастрольного графика и&nbsp;студийной работы?
                            </div>
                        </div>

                    </div>

                </div>
            </div>
                <? include 'includes/index-data.php'; ?>

        </div>
    </div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
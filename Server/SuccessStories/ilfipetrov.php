<?

$title = 'Съемки документально-анимационного фильма &laquo;ИЛЬФИПЕТРОВ&raquo;';

$collected = 96726;

$dateDuration = '7 месяцев 14 дней';

$startDay = 18;
$startMonth = 'июня';
$startYear = 2012;

$endDay = 1;
$endMonth = 'февраля';
$endYear = 2013;

$members = 30;

$projectLink = 'https://planeta.ru/campaigns/92';

$collected = number_format($collected, 0, '.', ' ');
$target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

    <div class="post" itemscope itemtype="http://schema.org/CreativeWork">
        <div class="wrap">
            <a class="post-back-link" href="index.php">Истории успеха</a>

            <div itemprop="name" class="post-title minionpro-boldit">Съемки документально-анимационного фильма &laquo;ИЛЬФИПЕТРОВ&raquo; (режиссер
                Р. Либеров)
            </div>
            <div class="post-main">

                <? require 'includes/post-meta.php'; ?>

                <img itemprop="image" class="post-big-img" src="images/ilfpetrov/check.jpg">

                <div class="post-middle">
                    <? require 'includes/share.php' ?>
                    <div class="post-content">

                        <div itemprop="description" class="post-content-text proxima-reg">
                            Роман Либеров&nbsp;&mdash; выпускник ВГИК, режиссер, сценарист. Он&nbsp;появился на&nbsp;&laquo;Планете&raquo;, чтобы завершить работу над <nobr>документально-анимационным</nobr> фильмом &laquo;ИЛЬФИПЕТРОВ&raquo;. Это история самого знаменитого писательского дуэта страны, построенная вокруг дружеской шуточной беседы Ильи Ильфа и&nbsp;Евгения Петрова во&nbsp;время работы над очередным романом. &laquo;ИЛЬФИПЕТРОВ&raquo;&nbsp;&mdash; пятая лента в&nbsp;литературной серии режиссера. В&nbsp;фильмографии Либерова уже есть картины о&nbsp;Сергее Довлатове, Иосифе Бродском, Юрии Олеше, Жоре Владимове.
                        </div>

                        <div class="post-content-text proxima-reg mrg-b-0">
                            <div class="p-content-notice helveticaneue-bold">
                                Фильм будет сконструирован из&nbsp;неигровых съёмок, мультипликации на&nbsp;основе набросков Ильфа и&nbsp;Петрова, а&nbsp;также шедевров советской авангардной живописи <nobr>двадцатых-тридцатых</nobr>, музыки, которую&nbsp;&mdash; как мы&nbsp;робко, но&nbsp;оптимистично предполагаем&nbsp;&mdash; напишет замечательная группа <nobr>&laquo;Хоронько-оркестр&raquo;</nobr>.
                            </div>
                        </div>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/ilfpetrov/ava-artist.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Роман Либеров</div>
                            <div class="p-content-manager-role">режиссер</div>
                            <div class="p-content-manager-quote">
                                Краудфандинг, как мне кажется,&nbsp;&mdash; это, в&nbsp;том числе, возможность
                                оставаться
                                независимым, поскольку те, кто помогает, совершая пожертвование, хочет того&nbsp;же&nbsp;&mdash;
                                независимости конкретной работы. Краудфандинг, как оказалось, ещё и&nbsp;труд сам по&nbsp;себе&nbsp;&mdash;
                                регулярная поддержка интереса к&nbsp;тому, на&nbsp;что разыскиваются средства. &laquo;Планета&raquo;
                                в&nbsp;этом контексте &laquo;нянчится&raquo; с&nbsp;авторами проектов, как с&nbsp;родными.
                                Мне ни&nbsp;разу не&nbsp;довелось почувствовать, что барышням всё равно, получится у&nbsp;нас
                                или нет. Большое спасибо!
                            </div>
                        </div>

                        <br>

                        <div class="post-content-text proxima-reg">
                            <div class="p-content-notice helveticaneue-bold">
                                По&nbsp;понятным причинам Роман Либеров не&nbsp;мог предложить в&nbsp;качестве бонусов участие в&nbsp;массовке или права на&nbsp;прокат картины за&nbsp;рубежом, поэтому ограничился приглашением на&nbsp;закрытую премьеру фильма, а&nbsp;также возможностью вписать свое имя в&nbsp;финальные титры картины.
                            </div>
                        </div>

                        <div class="post-content-text proxima-reg mrg-b-0">
                            Роман Либеров о&nbsp;животворных каплях краудфандинга в&nbsp;разливанном море бюджета фильма.
                        </div>

                        <div class="h-video">
                            <iframe width="720" height="405" frameborder="0" allowfullscreen=""
                                    src="https://tv.planeta.ru/video-frame?profileId=22956&amp;videoId=21768&amp;autostart=false"></iframe>
                        </div>

                        <br><br>


                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/kurator/ava-sorokina.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Мария Сорокина</div>
                            <div class="p-content-manager-role">куратор проекта</div>
                            <div class="p-content-manager-quote">
                                Очень жду выхода фильма &laquo;Ильфипетров&raquo; Романа Либерова и&nbsp;желаю ему
                                удачи!
                            </div>
                        </div>
                    </div>

                    <!--<div class="check-photo">
                        <img src="images/ilfpetrov/check.jpg">
                    </div>-->

                    <!--<div class="post-content">
                        <a class="post-another-posts h-link minionpro-semiboldit" href="index.php">Другие успешные
                            проекты</a>
                    </div>-->

                </div>

            </div>
                <? include 'includes/index-data.php'; ?>

        </div>
    </div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
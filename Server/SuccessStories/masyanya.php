<?
$title = 'Новые серии &laquo;Масяни&raquo;! <nobr>115-ая</nobr> и&nbsp;тыды!';

$collected = 337533;
$target = 100000;

$dateDuration = '29 дней';

$startDay = 2;
$startMonth = 'июля';
$startYear = 2013;

$endDay = 31;
$endMonth = 'июля';
$endYear = 2013;

$members = 559;

$projectLink = 'https://planeta.ru/masyanya/campaigns/178';

$collected = number_format($collected, 0, '.', ' ');
$target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

    <div class="post" itemscope itemtype="http://schema.org/CreativeWork">
        <div class="wrap">
            <a class="post-back-link" href="index.php">Истории успеха</a>

            <div itemprop="name" class="post-title minionpro-boldit"><?=$title?></div>
            <div class="post-main">

                <? require 'includes/post-meta.php'; ?>

                <img itemprop="image" class="post-big-img" src="images/masyanya/check.jpg">

                <div class="post-middle">
                    <? require 'includes/share.php' ?>
                    <div class="post-content">
                        <div itemprop="description" class="post-content-text proxima-reg">
                            &laquo;Удастся&nbsp;ли приручить страшного зверя, имя которому краудфандинг?&raquo;&nbsp;&mdash; вопрошал Олег Куваев у&nbsp;акционеров своего будущего проекта. Акционеры ответили громким и&nbsp;единогласным &laquo;ДА!&raquo; Народная любовь к&nbsp;обаятельным персонажам&nbsp;&mdash; Масяне, Хрюнделю и&nbsp;Лохматому,&nbsp;&mdash; оказалась настолько безграничной, что менее, чем за&nbsp;месяц проекта, удалось собрать не&nbsp;на&nbsp;одну, а&nbsp;на&nbsp;целых три серии. Причем с&nbsp;удлиненным хронометражем.<br />А&nbsp;в&nbsp;новом году запустился и&nbsp;новый &laquo;фаундкрангдинговый&raquo; проект. Вот таким щедрым оказался румяный <nobr>зверь-краудфандинг</nobr>.
                        </div>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/masyanya/ava-masyanya.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Олег Куваев</div>
                            <div class="p-content-manager-role">&nbsp;</div>
                            <div class="p-content-manager-quote">
                                <p>Я&nbsp;очень доволен результатами &bdquo;крайдфандинга&ldquo;. Их&nbsp;у&nbsp;меня было два, и&nbsp;оба превзошли мои ожидания. Проекты были очень удачными. И&nbsp;команда Планеты, и&nbsp;акционеры сработали &bdquo;на&nbsp;ура&ldquo;. Без сучка, без задоринки. Что&nbsp;бы не&nbsp;значила эта &bdquo;задоринка&ldquo;.</p>
                                <p>Мне очень понравился один момент. По&nbsp;правилам краудфандинга, авторы должны предлагать людям <nobr>какие-то</nobr> ништяки в&nbsp;обмен на&nbsp;поддержку. И&nbsp;вот я&nbsp;решил напрямую спросить, что им&nbsp;хотелось&nbsp;бы получить: &bdquo;Напишите, и&nbsp;я&nbsp;добавлю, что вам кажется интересным&ldquo;. Один парень тогда написал: &bdquo;Ничего не&nbsp;нужно. Ты, главное, мульт рисуй, а&nbsp;мы&nbsp;и&nbsp;без нишятков поддержим&ldquo;. То&nbsp;есть поддержка была бескорыстной. Люди не&nbsp;ждали от&nbsp;меня <nobr>каких-то</nobr> особых танцев с&nbsp;бубном. Люди действительно хотели поддержать без <nobr>каких-то</nobr> плюсов для себя. Это было очень приятно узнать. То&nbsp;есть кроме финансовой удалось получить и&nbsp;изрядный заряд моральной поддержки.<br /><br />Вообще по&nbsp;завершении проекта хочется всех поблагодарить. Среди тех, кто был акционером на&nbsp;первом краудфандинге, был один хороший человек, который внес крупный вклад. Я&nbsp;не&nbsp;знаю, хотел&nbsp;бы он, чтобы я&nbsp;упомянул его имя, поэтому не&nbsp;буду. Так вот по&nbsp;обещанию от&nbsp;меня, он&nbsp;должен был получить мою картину (акварель). Так эта картина до&nbsp;сих пор у&nbsp;меня висит дома, я&nbsp;бы хотел ему передать, что она его собственность. И&nbsp;будет теперь висеть тут до&nbsp;скончания века. Вот этому парню отдельный привет. Хотя, впрочем, отдельный привет всем, кто участвовал, потому что мы&nbsp;же не&nbsp;знаем, кому эта поддержка чего на&nbsp;самом деле стоила. Надеюсь на&nbsp;новые проекты в&nbsp;будущем. Вместе все делать гораздо приятнее.</p>

                            </div>
                        </div>

                        <br><br>


                        <div class="post-content-text proxima-reg mrg-b-50">
                            <div class="p-content-notice helveticaneue-bold">
                                Кроме символичного масянькинского колдовства с бубном на удачу, можно было приобщиться к истории, вписав свое имя в титры, получив личную благодарность от Масяни за участие или даже став обладателем авторской акварели руки самого Олега Куваева. Последняя, кстати, даже несмотря на весьма высокий ценник, быстро нашла своего обладателя.
                            </div>
                        </div>


                        <div class="post-milestones large-milestones">
                            <div class="post-milestones-list clearfix">
                                <div class="p-milestones-list-title minionpro-mediumit">Вехи проекта</div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">2 июля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Проект &laquo;Новые серии &bdquo;Масяни&ldquo;! <nobr>115-ая</nobr> и&nbsp;тыды&raquo; запущен на&nbsp;Планете. Буквально через несколько часов заявленная сумма была собрана.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">31 июля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Успешное завершение проекта: за все время в нем поучаствовало 529 человек, и было собрано более 337 тысяч.</div>
                                    </div>
                                </div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">29 июля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Вышла 115 серия «Царь@ру». Олег Куваев начинает работать над 116 и 117 сериями мультфильма.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">27 января 2014</div>
                                        <div class="pml-items-item-text proxima-reg">Запущен второй проект на Планете на 121 серию «Масяни». На данный момент проект успешно  <a href="https://planeta.ru/masyanya/campaigns/4065">завершен</a>.</div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="post-content-manager mrg-t-50 mrg-b-50">
                            <div class="p-content-manager-ava">
                                <img src="images/kurator/ava-chechulina.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Катерина Чечулина</div>
                            <div class="p-content-manager-role">PR-директор Planeta.ru</div>
                            <div class="p-content-manager-quote">Если описать мои впечатления от&nbsp;первого проекта с&nbsp;Масяней языком Фейсбука, то&nbsp;это будет &bdquo;чувствует себя странно&ldquo;. Я&nbsp;как будто снималась в&nbsp;кино. 15 лет назад, когда я&nbsp;училась в&nbsp;университете и&nbsp;работала на&nbsp;одной популярной радиостанции, фраза &bdquo;Пошел в&nbsp;жопу директор!&ldquo; была для нас почти культурным кодом, а&nbsp;Олег Куваев&nbsp;&mdash; образцом самоиронии. Мы&nbsp;ждали каждую новую масяньскую серию и&nbsp;выходили в&nbsp;курилку только со&nbsp;словами &bdquo;<nobr>Пойдем-ка</nobr>, <nobr>покурим-ка</nobr>!&ldquo;.<br /><br />В&nbsp;жизни Куваев оказался именно таким, каким я&nbsp;его представляла. Масяня его <nobr>Альтер-эго</nobr>, и&nbsp;это очень круто, потому что она не&nbsp;выдуманная, а&nbsp;настоящая, она живет и&nbsp;развивается, а&nbsp;потому не&nbsp;теряет свой актуальности и&nbsp;аудитории. Олег абсолютный профессионал, очень щепетильный в&nbsp;вопросах свободы творчества, при этом он&nbsp;очень скромный человек, не&nbsp;обремененный звездной болезнью. Меня совсем не&nbsp;удивило, что за&nbsp;несколько часов поклонники Масяни собрали необходимую сумму для создания одной серии, а&nbsp;потом и&nbsp;еще двух. Куваев уже просил: &bdquo;Горшочек не&nbsp;вари&ldquo; и&nbsp;менял свое рабочее расписание так, чтобы успеть в&nbsp;срок сдать три серии вместо одной.
                            </div>
                        </div>

                        <div class="post-content-manager mrg-t-50 mrg-b-50">
                            <div class="p-content-manager-ava">
                                <img src="images/ava-2.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Саватеев Александр</div>
                            <div class="p-content-manager-role">Акционер проекта</div>
                            <div class="p-content-manager-quote">Олег и&nbsp;правда, спасибо огромное, за&nbsp;счастливое детство, оптимизм и&nbsp;Маську. Если так будет дальше, в&nbsp;моем лице вы&nbsp;нашли постоянного спонсора, в&nbsp;следующий раз две тыщи рублев, а&nbsp;то&nbsp;и&nbsp;больше.
                            </div>
                        </div>

                        <div class="post-content-manager mrg-t-50 mrg-b-50">
                            <div class="p-content-manager-ava">
                                <img src="images/ava-3.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Sadhvika</div>
                            <div class="p-content-manager-role">Акционер проекта</div>
                            <div class="p-content-manager-quote">Олег, ни&nbsp;в&nbsp;коем случае не&nbsp;сворачивай сборы! Давай соберем на&nbsp;117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127 и&nbsp;так далее серии!!! Пусть Масянька будет самым длинным мультипликационным <nobr>интернет-сериалом</nobr> в&nbsp;мире!
                            </div>
                        </div>



                    </div>

                </div>
            </div>
                <? include 'includes/index-data.php'; ?>

        </div>
    </div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
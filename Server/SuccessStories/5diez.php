<?
$title = '#####';

$collected = 158464;
$target = 150000;

    $dateDuration = '1 месяц 20 дней';

    $startDay = 19;
    $startMonth = 'декабря';
    $startYear = 2012;

$endDay = 7;
$endMonth = 'февраля';
$endYear = 2013;

$members = 183;

$projectLink = 'https://planeta.ru/campaigns/294';

$collected = number_format($collected, 0, '.', ' ');
$target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

    <div class="post" itemscope itemtype="http://schema.org/CreativeWork">
        <div class="wrap">
            <a class="post-back-link" href="index.php">Истории успеха</a>

            <div itemprop="name" class="post-title minionpro-boldit">Запись альбома группы ##### (5diez) &laquo;MMXIII&raquo;</div>
            <div class="post-main">

                <? require 'includes/post-meta.php'; ?>

                <img itemprop="image" class="post-big-img" src="images/5diez/check-title.jpg">

                <div class="post-middle">
                    <? require 'includes/share.php' ?>
                    <div class="post-content">
                        <div itemprop="description" class="post-content-text proxima-reg">
                            #####&nbsp;&mdash; это вовсе не&nbsp;непечатное слово (хотя &laquo;бьет по&nbsp;ушам&raquo;
                            не&nbsp;хуже!), а&nbsp;команда белорусских альтернативщиков 5diez, которые давно обрели
                            тысячи верных поклонников на&nbsp;всей территории бывшего СССР. 2013 год стал для группы
                            началом нового творческого периода, а&nbsp;его плоды&nbsp;&mdash; песни с&nbsp;нового
                            альбома &laquo;MMXIII&raquo;, созданного при поддержке акционеров &laquo;Планеты&raquo;,&nbsp;&mdash;
                            уже доносятся из&nbsp;всех проигрывателей.
                        </div>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/5diez/ava-artist.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">##### (5 diez)</div>
                            <div class="p-content-manager-role">из обращения участников группы ##### к акционерам проекта</div>
                            <div class="p-content-manager-quote">
                                Вы&nbsp;сделали это! Альбом &laquo;MMXIII&raquo;&nbsp;&mdash; это большой совместный
                                труд,
                                который действительно показал, что наше творчество вам небезразлично! Вместе мы&nbsp;совершили
                                маленькую, но&nbsp;очень важную революцию в&nbsp;мире отечественного
                                <nobr>шоу-бизнеса</nobr>. Чтобы не&nbsp;быть голословными, скажем одно&nbsp;&mdash; в&nbsp;первую секунду
                                появления
                                альбома мы&nbsp;на&nbsp;100% отбили все затраты на&nbsp;запись диска. А&nbsp;это значит,
                                что
                                наша работа нужна вам, что тяжелая русскоязычная музыка
                                <nobr>по-прежнему</nobr> жива и&nbsp;востребована. Будем осторожными оптимистами, но&nbsp;скажем, что все мы&nbsp;потихоньку
                                уходим от&nbsp;насквозь пиратской и&nbsp;жадной потребительской ситуации. Большой шаг
                                для
                                развития индустрии и&nbsp;подобной музыки сделан.
                            </div>
                        </div>

                        <br><br>

                        <div class="post-milestones">
                            <div class="post-milestones-sidebar">
                                <div class="p-msb-item">
                                    <img src="images/5diez/milestones.jpg">
                                </div>

                                <div class="p-msb-item">
                                    <div class="p-msb-video">
                                        <a href="#nogo" class="dlink" id="dlink-01"><img
                                                src="images/5diez/milestone-video.jpg"></a>

                                        <div class="p-msb-iframe">
                                            <iframe id="dcontent-01" width="509" height="286"
                                                    data-href="https://www.youtube.com/embed/KKU6EGi8XJY?autoplay=1" frameborder="0"
                                                    allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>

                                <div class="p-msb-item">
                                    <div class="p-msb-head">
                                        Некоторые акционеры получили такие футболки
                                    </div>
                                    <img src="images/5diez/t-short.jpg">
                                </div>
                            </div>
                            <div class="post-milestones-list">
                                <div class="p-milestones-list-title minionpro-mediumit">Вехи проекта</div>
                                <div class="p-milestones-list-items">

                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">19 декабря 2012</div>
                                        <div class="pml-items-item-text proxima-reg">
                                            Проект &laquo;Запись альбома группы ##### (5diez) &laquo;MMXIII&raquo; запущен на&nbsp;&laquo;Планете&raquo;.
                                        </div>
                                    </div>

                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">2 февраля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">
                                            Участники группы приняли решение включить имена всех акционеров проекта в&nbsp;буклет диска.
                                        </div>
                                    </div>

                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">4 февраля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">
                                            ##### представили секретное видео с&nbsp;приглашенной звездой пластинки&nbsp;&mdash; Иваном Чачей из&nbsp;групп &laquo;Radio Чача&raquo; и&nbsp;&laquo;Наив&raquo;. Кстати, поклонников пригласили для записи хора в&nbsp;песне &laquo;Я&nbsp;умею летать&raquo;.
                                        </div>
                                    </div>

                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">7 февраля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">
                                            Проект успешно завершен с&nbsp;превышением заявленной суммы. Однако группа не&nbsp;прекратила развитие проекта&hellip;
                                        </div>
                                    </div>

                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">18 февраля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">
                                            Акционеры проекта смогли познакомиться и&nbsp;с&nbsp;цифровой версией альбома. Остальным пользователям на&nbsp;тот момент был доступен только небольшой тизер издания.
                                        </div>
                                    </div>

                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">25 апреля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">
                                            Группа разыграла билеты на&nbsp;московский фестиваль &laquo;ReAктиV&raquo;, где 5diez представили расширенный сет с&nbsp;новыми песнями.
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="post-content-text proxima-reg mrg-b-0">
                            <div class="p-content-notice helveticaneue-bold">
                                Одним из&nbsp;самых интересных бонусов проекта стала акция &laquo;Почувствуй себя частью
                                #####!&raquo;,
                                которая включала поездку с&nbsp;группой в&nbsp;<nobr>Санкт-Петербург</nobr>, проживание с&nbsp;музыкантами в&nbsp;одной гостинице, совместный обед и&nbsp;ужин,
                                присутствие
                                на&nbsp;саундчеке, и, самое главное, совместное исполнение одной из&nbsp;песен на&nbsp;концерте!
                            </div>

                            <br>

                            Вокалист и&nbsp;поэт группы ##### Евгений &laquo;Женк&raquo; Белоусов о&nbsp;пользе
                            знакомств и&nbsp;объединяющем чувстве юмора. И&nbsp;все это с&nbsp;глубоким респектом в&nbsp;адрес &laquo;Планеты&raquo;
                            и&nbsp;ее&nbsp;обитателей.
                        </div>

                        <div class="h-video">
                            <iframe width="720" height="405" frameborder="0" allowfullscreen=""
                                    src="https://tv.planeta.ru/video-frame?profileId=22956&amp;videoId=21761&amp;autostart=false"></iframe>
                        </div>


                        <br><br>



                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/kurator/ava-egor.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Егор Ельчин</div>
                            <div class="p-content-manager-role">куратор проекта</div>
                            <div class="p-content-manager-quote">Прекрасно, когда по окончанию
                                проекта ты обзаводишься хорошими друзьями в лице артистов.
                            </div>
                        </div>
                    </div>

                    <!--<div class="check-photo">
                        <img src="images/5diez/check.jpg">
                    </div>-->

                    <!--<div class="post-content">
                        <a class="post-another-posts h-link minionpro-semiboldit" href="index.php">Другие успешные
                            проекты</a>
                    </div>-->

                </div>
            </div>
                <? include 'includes/index-data.php'; ?>

        </div>
    </div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
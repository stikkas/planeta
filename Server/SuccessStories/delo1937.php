<?
    $collected = 154000;

    $dateDuration = '7 месяцев 10 дней';

    $startDay = 24;
    $startMonth = 'апреля';
    $startYear = 2012;

    $endDay = 1;
    $endMonth = 'ноября';
    $endYear = 2012;

    $title = 'Выпуск аудио-спектакля &laquo;Дело №1937&raquo;';

    $members = 51;

    $projectLink = 'https://planeta.ru/campaigns/33';

    $collected = number_format($collected, 0, '.', ' ');
    $target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

<div class="post" itemscope itemtype="http://schema.org/CreativeWork">
    <div class="wrap">
        <a class="post-back-link" href="index.php">Истории успеха</a>
        <div itemprop="name" class="post-title minionpro-boldit">Выпуск аудиоспектакля &laquo;Дело №1937&raquo;</div>
        <div class="post-main">

        <? require 'includes/post-meta.php'; ?>

            <img itemprop="image" class="post-big-img" src="images/delo1937/check.jpg">
            <div class="post-middle">
                <? require 'includes/share.php' ?>
                <div class="post-content">

                    <div itemprop="description" class="post-content-text proxima-reg">
                        Аудиоспектакль &laquo;Дело &#8470;&nbsp;1937&raquo; (по&nbsp;роману Николая Нарокова &laquo;Мнимые величины&raquo;)&nbsp;&mdash; рассказ о&nbsp;нескольких месяцах из&nbsp;нашего недалекого прошлого&nbsp;&mdash; осени 1937 года. Отношения героев этой вымышленной детективной истории, ситуации, в&nbsp;которые ставит их&nbsp;жизнь&nbsp;&mdash; точный, удивительно емкий, захватывающий рассказ о&nbsp;том времени. Над проектом работало творческое содружество актеров, режиссеров и&nbsp;художников &laquo;Букворечник&raquo;. Среди участников аудиопостановки были Александр Балуев, Александр Феклистов, Ольга Волкова, Андрей Панин и&nbsp;другие замечательные актеры.

                        <div class="p-content-notice helveticaneue-bold">&laquo;Дело &#8470;&nbsp;1937&raquo; стало делом номер один для поклонников театра на&nbsp;&laquo;Планете&raquo;&nbsp;&mdash; и&nbsp;это дело завершилось успехом авторов и&nbsp;участников проекта! Несмотря на&nbsp;сравнительно скромное количество акционеров (51) проект собрал солидную сумму&nbsp;&mdash; 150&nbsp;000&nbsp;рублей&raquo;.
                        </div>
                    </div>

                    <div class="post-content-manager">
                        <div class="p-content-manager-ava">
                            <img src="images/delo1937/ava-artist.jpg">
                        </div>
                        <div class="p-content-manager-name minionpro-mediumit">Мария Величкина</div>
                        <div class="p-content-manager-role">режиссер, автор проекта</div>
                        <div class="p-content-manager-quote">
                            Здесь весь вопрос в&nbsp;доверии. А&nbsp;оно придет со&nbsp;временем, обязательно. Могу это
                            утверждать, потому что знаю тех людей, которые рискнули стать первопроходцами и&nbsp;которые
                            отвечают за&nbsp;честность и&nbsp;надежность работы сервиса! А&nbsp;потом мы&nbsp;все
                            постепенно привыкнем к&nbsp;такому способу сосуществования творцов и&nbsp;их&nbsp;аудитории,
                            и&nbsp;безнадежные (в&nbsp;плане государственного финансирования) проекты получат
                            возможность выжить. И&nbsp;<nobr>интернет-пиратство</nobr> будет нам не&nbsp;страшно. Я&nbsp;абсолютно в&nbsp;этом убеждена, ведь сила нашей &laquo;горизонтальной&raquo;
                            самоорганизации с&nbsp;помощью Интернета уже начала себя проявлять. Сама идея мне очень
                            нравится, ведь это крайне тяжело&nbsp;&mdash; просить, искать деньги, поддержку. А&nbsp;тут
                            мы&nbsp;просто заранее продаем свой продукт, всякие приятные для пользователя штуки, которые
                            потом, в&nbsp;случае успеха затеи, могут стать раритетом, редкостью.
                        </div>
                    </div>

                    <div class="post-content-text proxima-reg">
                        <div class="p-content-notice helveticaneue-bold">В&nbsp;числе эксклюзивных бонусов авторы предложили почетные звания бронзового, серебряного и&nbsp;золотого спонсоров проекта. Обладателям этих акций выражалась персональная благодарность во&nbsp;вкладыше диска, а&nbsp;также предоставлялись приглашения на&nbsp;все мероприятия, связанные с&nbsp;проектом. Наконец, акционер получал именной диск с&nbsp;автографами и&nbsp;билет на&nbsp;спектакль с&nbsp;участием одного из&nbsp;актеров, задействованных в&nbsp;аудиопостановке.</div>
                    </div>

                    <div class="post-content-text proxima-reg">&laquo;Автор спектакля Мария Величкина о&nbsp;пользе авантюр, после которых вырастают крылья&raquo;</div>

                    <div class="h-video mrg-b-40">
                        <iframe width="720" height="405" frameborder="0" allowfullscreen=""
                                src="https://tv.planeta.ru/video-frame?profileId=22956&amp;videoId=21771&amp;autostart=false"></iframe>
                    </div>

                    <div class="post-content-manager">
                        <div class="p-content-manager-ava">
                            <img src="images/kurator/ava-sorokina.jpg">
                        </div>
                        <div class="p-content-manager-name minionpro-mediumit">Мария Сорокина</div>
                        <div class="p-content-manager-role">куратор проекта</div>
                        <div class="p-content-manager-quote">
                            Прослушав черновую запись аудиоспектакля, я&nbsp;подумала, что этот диск должен услышать
                            каждый, кто интересуется российской историей и&nbsp;кому небезразличен такой жанр, как
                            аудиоспектакль. И&nbsp;заблуждение, что сейчас есть только начитанные одним голосом
                            аудиокниги, остается заблуждением: &laquo;Дело 1937&raquo;&nbsp;&mdash; яркий пример
                            настоящего спектакля для ушей.
                        </div>
                    </div>
                </div>

<!--                 <div class="post-milestones">
                    <div class="post-milestones-list">
                        <div class="p-milestones-list-title minionpro-mediumit">Вехи проекта</div>
                        <div class="p-milestones-list-items">
                            <div class="pm-list-items-item">
                                <div class="pml-items-item-date proxima-bold">16 декабря 2012</div>
                                <div class="pml-items-item-text proxima-reg">диск &laquo;Дело &#8470;&nbsp;1937&raquo; был презентован в&nbsp;Центре Сахарова</div>
                            </div>
                        </div>
                    </div>
                </div> -->

                <!--<div class="check-photo">
                    <img src="images/delo1937/check.jpg">
                </div>-->

                <!--<div class="post-content">
                    <a class="post-another-posts h-link minionpro-semiboldit" href="index.php">Другие успешные проекты</a>
                </div>-->

            </div>
        </div>
            <? include 'includes/index-data.php'; ?>

    </div>
</div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
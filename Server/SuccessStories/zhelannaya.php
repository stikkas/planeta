<?
$title = 'Инна Желанная – новый альбом';

$collected = 600000;
$target = 350000;

$dateDuration = '4 месяца 9 дней';

$startDay = 18;
$startMonth = 'ноября';
$startYear = 2013;

$endDay = 12;
$endMonth = 'марта';
$endYear = 2014;

$members = 337;

$projectLink = 'https://planeta.ru/campaigns/2565';

$collected = number_format($collected, 0, '.', ' ');
$target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

    <div class="post" itemscope itemtype="http://schema.org/CreativeWork">
        <div class="wrap">
            <a class="post-back-link" href="index.php">Истории успеха</a>

            <div itemprop="name" class="post-title minionpro-boldit"><?=$title?></div>
            <div class="post-main">

                <? require 'includes/post-meta.php'; ?>

                <img itemprop="image" class="post-big-img" src="images/zhelannaya/check.jpg">

                <div class="post-middle">
                    <? require 'includes/share.php' ?>
                    <div class="post-content">
                        <div itemprop="description" class="post-content-text proxima-reg">
                            Проект на&nbsp;Планете для Инны Желанной и&nbsp;ее&nbsp;группы стал во&nbsp;многих смыслах экспериментальным. Судите сами: планировали выпустить альбом, а&nbsp;в&nbsp;итоге он&nbsp;получился двойным. Вернулись в&nbsp;студию после десятилетнего перерыва. Впервые записали материал в&nbsp;новом составе.
                            <br><br>
                            И&nbsp;интерес к&nbsp;экспериментам, и&nbsp;широта заявленной цели&nbsp;&mdash; перевернуть представление о&nbsp;народной песне,&nbsp;&mdash; привлекли в&nbsp;проект не&nbsp;только акционеров, но&nbsp;и&nbsp;многочисленных <nobr>друзей-музыкантов</nobr>, которые так&nbsp;же поработали над альбомом &laquo;Изворот&raquo;.
                        </div>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/zhelannaya/ava-author.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Инна Желанная</div>
                            <div class="p-content-manager-role"></div>
                            <div class="p-content-manager-quote">
                                Друзья! Мы&nbsp;успешно завершили свой первый проект на&nbsp;Планета.ру!
                                <br>
                                <br>
                                Для нас это был своего рода эксперимент, мы&nbsp;долго к&nbsp;этому шагу морально готовились и, честно говоря, немного сомневались в&nbsp;успехе. Но&nbsp;когда проект стартовал, мы&nbsp;поняли, что это было абсолютно верное, если не&nbsp;единственно верное решение.
                                <br>
                                <br>
                                С&nbsp;нами работали приятные и&nbsp;очень ответственные менеджеры, они помогали нам буквально на&nbsp;каждом шагу, советовали и&nbsp;направляли. За&nbsp;первые несколько дней мы&nbsp;собрали половину заявленной суммы, за&nbsp;следующие 2&ndash;3 недели&nbsp;&mdash; оставшуюся, а&nbsp;приток инвестиций все не&nbsp;останавливался! Мы&nbsp;были удивлены и&nbsp;одновременно счастливы, что у&nbsp;нас столько неравнодушных поклонников, что наша музыка нужна, что наш новый альбом ждут, и&nbsp;это придавало нам сил и&nbsp;уверенности, что у&nbsp;нас все получится!
                                <br>
                                <br>
                                Теперь мы&nbsp;знаем&nbsp;&mdash; будущее за&nbsp;краудфандингом! Мы&nbsp;непременно будем продолжать пользоваться системой краудфандинга, это очень интересный и&nbsp;полезный опыт сотрудничества. А&nbsp;главное&nbsp;&mdash; это взаимовыгодный способ создавать музыку и&nbsp;делиться ею&nbsp;с&nbsp;вами! Ведь мы&nbsp;фактически сделали этот альбом все вместе!
                                <br>
                                <br>
                                Спасибо Планете и&nbsp;ее&nbsp;обитателям! До&nbsp;новых встреч на&nbsp;музыкальных орбитах!
                            </div>
                        </div>

                        <br>

                        <div class="post-content-text proxima-reg">
                            <div class="p-content-notice helveticaneue-bold">
                                Участники проекта получили доступ к&nbsp;различным музыкальным бонусам от&nbsp;многочисленных приглашений на&nbsp;концерт, до&nbsp;раритетных в&nbsp;виде виниловой пластинки &laquo;Водоросль&raquo; 1995 года или посещения студии в&nbsp;один из&nbsp;дней записи альбома.
                            </div>
                        </div>


                        <div class="text-center">
                            <img src="images/zhelannaya/bonuses.jpg" width="600" height="328">
                        </div>

                        <br>
                        <br>
                        <br>


                        <div class="post-milestones large-milestones">
                            <div class="post-milestones-list clearfix">
                                <div class="p-milestones-list-title minionpro-mediumit">Вехи проекта</div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">18 ноября 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Проект Инны Желанной запущен на&nbsp;Планете.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">30 декабря 2013</div>
                                        <div class="pml-items-item-text proxima-reg">В&nbsp;проект добавлена <a href="https://planeta.ru/zhelannaja/blog/122481" target="_blank">уникальная акция</a>&nbsp;&mdash; переизданный на&nbsp;виниле первый сольный альбом Инны Желанной &laquo;Водоросль&raquo;.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">21 января 2014</div>
                                        <div class="pml-items-item-text proxima-reg">В&nbsp;рамках проекта разыгран <a href="https://planeta.ru/zhelannaja/blog/122828" target="_blank">бонус с&nbsp;приколом</a>&nbsp;&mdash; приглашение в&nbsp;святая святых, репетиционную студию.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">12 марта 2014</div>
                                        <div class="pml-items-item-text proxima-reg">Проект на&nbsp;Планете с&nbsp;успехом <a href="https://planeta.ru/zhelannaja/blog/124094" target="_blank">завершен</a>, собрав в&nbsp;сумму почти вдвое больше. По&nbsp;итогам проекта группа решает выпустить двойной альбом на&nbsp;CD&nbsp;и&nbsp;виниле.</div>
                                    </div>
                                </div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">25 апреля 2014</div>
                                        <div class="pml-items-item-text proxima-reg">Состоялась презентация EP&nbsp;&laquo;Изворот&raquo;.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">Осень 2014</div>
                                        <div class="pml-items-item-text proxima-reg">Выход двойного альбома «Изворот».</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-text proxima-reg">
                                            <img src="images/zhelannaya/milestones-1.jpg">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="post-content-manager mrg-t-50 mrg-b-50">
                            <div class="p-content-manager-ava">
                                <img src="images/kurator/ava-german.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Ольга Герман</div>
                            <div class="p-content-manager-role">куратор проекта</div>
                            <div class="p-content-manager-quote">
                                Приступая к&nbsp;проекту коллектива &laquo;Инны Желанной&raquo; чувствовалась огромная ответственность, ведь этот проект посвящен первому студийному альбому группы и&nbsp;его выпуск полностью зависел от&nbsp;успеха кампании в&nbsp;целом.
                                <br>
                                <br>
                                Инна&nbsp;&mdash; невероятный профессионал своего дела, с&nbsp;серьезным и&nbsp;требовательным подходом к&nbsp;любому вопросу. Думаю, что это и&nbsp;стало залогом успеха, вместе с&nbsp;отзывчивостью поклонников группы и&nbsp;поддержкой коллег по&nbsp;цеху.
                                <br>
                                <br>
                                Хочется отметить и&nbsp;сказать отдельное &laquo;спасибо&raquo; Инне за&nbsp;всестороннюю поддержку проектов других авторов!
                                <br>
                                <br>
                                И&nbsp;отдельное спасибо за&nbsp;креатив!
                            </div>
                        </div>

                    </div>

                </div>
            </div>
                <? include 'includes/index-data.php'; ?>

        </div>
    </div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
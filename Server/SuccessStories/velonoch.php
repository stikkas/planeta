<?
$title = 'Организация VII московской Велоночи';

$collected = 133600;
$target = 97000;

$dateDuration = '1 месяц 4 дня';

$startDay = 18;
$startMonth = 'июня';
$startYear = 2013;

$endDay = 22;
$endMonth = 'июля';
$endYear = 2013;

$members = 99;

$projectLink = 'https://planeta.ru/campaigns/1179';

$collected = number_format($collected, 0, '.', ' ');
$target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

    <div class="post" itemscope itemtype="http://schema.org/CreativeWork">
        <div class="wrap">
            <a class="post-back-link" href="index.php">Истории успеха</a>

            <div itemprop="name" class="post-title minionpro-boldit"><?=$title?></div>
            <div class="post-main">

                <? require 'includes/post-meta.php'; ?>

                <img itemprop="image" class="post-big-img" src="images/velonoch/check.jpg">

                <div class="post-middle">
                    <? require 'includes/share.php' ?>
                    <div class="post-content">
                        <div itemprop="description" class="post-content-text proxima-reg">
                            К&nbsp;моменту запуска на&nbsp;Планете проект &laquo;Велоночь&raquo; был известен даже тем, кто не&nbsp;так уж&nbsp;и&nbsp;часто крутит педали. Однако повод громко заявить об&nbsp;очередной &laquo;Велоночи&raquo; был и&nbsp;довольно весомый&nbsp;&mdash; пятнадцатилетие проекта в&nbsp;целом и&nbsp;седьмая московская &laquo;прогулка&raquo; в&nbsp;частности. Подготовка к&nbsp;этому событию была действительно масштабной, начиная от&nbsp;участия в&nbsp;нем разнообразных партнеров, до&nbsp;впечатляющего маршрута, длинной в&nbsp;двадцать километров.
                            <br><br>
                            Среди десяти тысяч людей, принявших участие в&nbsp;заезде, были и&nbsp;акционеры Планеты, которые с&nbsp;радостью откликнулись на&nbsp;призыв поддержать юбилейную &laquo;Велоночь&raquo;. И&nbsp;даже дождь не&nbsp;смог им&nbsp;помешать!
                        </div>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/velonoch/ava-artist.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Сергей Никитин</div>
                            <div class="p-content-manager-role">основатель проекта «Велоночь»</div>
                            <div class="p-content-manager-quote">
                                Друзья! Вместе с&nbsp;Вами мы&nbsp;провели Южную Московскую Велоночь, эту монументальную экскурсию, в&nbsp;которой приняло участие около десяти тысяч человек&nbsp;&mdash; не&nbsp;считая помогавшей нам полиции. Никогда не&nbsp;забуду Варшавское шоссе&nbsp;&mdash; широкое, бесконечное&nbsp;&mdash; и&nbsp;только для нас; горнолыжный курорт в&nbsp;свете прожекторов, запруженный велосипедистами, и&nbsp;финиш у&nbsp;церкви Вознесения в&nbsp;Коломенском, ключевом памятнике русской истории. Счастье! Капли того сильного июльского дождя высохли, впечатления остались на&nbsp;всю жизнь. Планете только пара лет, а&nbsp;уже столько счастливых свершений случилось благодаря Вам и&nbsp;Вашему заботливому отношению к&nbsp;делу. Спасибо и&nbsp;успехов!
                            </div>
                        </div>

                        <br>
                        <br>

                        <div class="post-content-text proxima-reg">
                            <div class="p-content-notice helveticaneue-bold">
                                Послушать, почитать, приодеться, прокатиться и&nbsp;поучаствовать&nbsp;&mdash; вот те&nbsp;опции, которыми могли воспользоваться акционеры проекта. А&nbsp;поскольку юбилейная Велоночь также оказалась и&nbsp;первой поэтической, (так как была приурочена к&nbsp;<nobr>120-летию</nobr> со&nbsp;дня рождения Владимира Маяковского), то&nbsp;среди прочих акций участники могли приобрести диски со&nbsp;стихами поэта от&nbsp;Москультпрога, приглашения на&nbsp;спектакль в&nbsp;театр Маяковского и&nbsp;путеводитель по&nbsp;одноименному музею.
                            </div>
                        </div>

                        <br><br>

                        <div class="post-milestones large-milestones">
                            <div class="post-milestones-list clearfix">
                                <div class="p-milestones-list-title minionpro-mediumit">Вехи проекта</div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">18 июня 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Проект по помощи в организации VII московской Велоночи запущен на Планете.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">3 июля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Специально к VII московской тематическо-поэтической Велоночи  Москультпрог совместно с фирмой «Мелодия» выпускает тираж дисков со стихами Владимира Маяковского. Акции с дисками <a href="https://planeta.ru/128318/blog/118308" target="_blank">добавлены в проект.</a></div>
                                    </div>
                                </div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">20 июля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Московская Велоночь <a href="https://planeta.ru/128318/blog/118752" target="_blank">проехала по Югу столицы</a>. Акционеры проекта на Планете также приняли в мероприятии активное участие, примкнув к пятикилометровой колонне велосипедистов.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">22 июля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Проект на Планете с успехом завершился, собрав больше заявленной первоначально суммы.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">2 августа 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Состоялась <a href="https://planeta.ru/128318/blog/118860" target="_blank">after-party</a> по случаю седьмого сезона Велоночи.</div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="post-content-manager mrg-t-50 mrg-b-50">
                            <div class="p-content-manager-ava">
                                <img src="images/kurator/ava-virtser.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Даша Вирцер</div>
                            <div class="p-content-manager-role">куратор проекта</div>
                            <div class="p-content-manager-quote">
                                Так получилось, что с&nbsp;Сергеем Никитиным мы&nbsp;были знакомы задолго до&nbsp;проекта на&nbsp;Планете. В&nbsp;свое время мне посчастливилось слушать его лекции о&nbsp;русском искусстве и&nbsp;топонимике, танцевать под его сумасшедшие диджейские сеты и, конечно, крутить педали по&nbsp;ночной Москве. И&nbsp;за&nbsp;что&nbsp;бы он&nbsp;ни&nbsp;брался, эффект ощущаешь один&nbsp;&mdash; ВДОХНОВЕНИЕ, ни&nbsp;больше ни&nbsp;меньше. Важнее самой задачи, которую поставила перед собой команда &laquo;Москультпрога&raquo;,&nbsp;&mdash; ее&nbsp;исполнение. То, с&nbsp;каким трепетом, фантазией, осмыслением и, главное, юмором эти ребята оживляют любимый город, не&nbsp;перестает удивлять. Поэтому не&nbsp;было никаких сомнений, что седьмой московский праздник будет принят на&nbsp;&laquo;ура&raquo; и&nbsp;найдет единомышленников. Большая радость и&nbsp;удача&nbsp;&mdash; уверена, в&nbsp;этом меня поддержит каждый акционер и&nbsp;участник &laquo;Велоночи&raquo;,&nbsp;&mdash; чувствовать себя сопричастным к&nbsp;такому большому делу. Я&nbsp;желаю огромных успехов &laquo;Москультпрогу&raquo; и&nbsp;с&nbsp;нетерпением жду новых проектов.
                            </div>
                        </div>

                    </div>

                </div>
            </div>
                <? include 'includes/index-data.php'; ?>

        </div>
    </div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
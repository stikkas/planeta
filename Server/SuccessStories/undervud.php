<?
$title = 'Ундервуд';
$collected = 354861;
$target = 300000;

$dateDuration = '3 месяца 9 дней';

$startDay = 6;
$startMonth = 'февраля';
$startYear = 2013;

$endDay = 15;
$endMonth = 'мая';
$endYear = 2013;

$members = 150;

$projectLink = 'https://planeta.ru/campaigns/304';

$collected = number_format($collected, 0, '.', ' ');
$target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

    <div class="post" itemscope itemtype="http://schema.org/CreativeWork">
        <div class="wrap">
            <a class="post-back-link" href="index.php">Истории успеха</a>

            <div itemprop="name" class="post-title minionpro-boldit">Запись альбома группы &laquo;Ундервуд&raquo; &laquo;Женщины и дети&raquo;</div>
            <div class="post-main">

                <? require 'includes/post-meta.php'; ?>

                <img itemprop="image" class="post-big-img" src="images/undervud/check.jpg">

                <div class="post-middle">
                    <? require 'includes/share.php' ?>
                    <div class="post-content">

                        <div itemprop="description" class="post-content-text proxima-reg">
                            &laquo;Ундервуд&raquo;&nbsp;&mdash; музыкальный коллектив, основанный в&nbsp;1995 году в&nbsp;Симферополе двумя <nobr>студентами-медиками</nobr>&nbsp;&mdash; Максимом Кучеренко и&nbsp;Владимиром Ткаченко. Неизвестно, потеряла&nbsp;ли медицина, но&nbsp;отечественная <nobr>рок-сцена</nobr> точно приобрела. Выбери они&nbsp;другую стезю, милая никогда&nbsp;бы не&nbsp;узнала о&nbsp;том, что &laquo;все пройдет&raquo;, а&nbsp;&laquo;бабло побеждает зло&raquo;. &laquo;Припланетились&raquo; ребята в&nbsp;2012&nbsp;г. с&nbsp;целью записать свой шестой студийный альбом с&nbsp;рабочим названием &laquo;Женщины и&nbsp;дети&raquo;.
                        </div>

                        <div class="post-content-text proxima-reg">

                            <div class="p-content-notice helveticaneue-bold">
                                К&nbsp;&laquo;Женщинам и&nbsp;детям&raquo; группы &laquo;Ундервуд&raquo; присоединились
                                и&nbsp;остальные пользователи &laquo;Планеты&raquo;&nbsp;&mdash; и&nbsp;пластинка
                                успешно увидела свет!
                            </div>

                        </div>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/undervud/ava-artist.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Владимир Ткаченко</div>
                            <div class="p-content-manager-role">музыкант</div>
                            <div class="p-content-manager-quote">
                                Идея нашего участия возникла у&nbsp;басиста
                                <nobr>&laquo;Би-2&raquo;</nobr>&nbsp;Макса Лакмуса. Он&nbsp;работает на&nbsp;planeta.ru, и, когда мы&nbsp;в&nbsp;течение двух
                                прекрасных декабрьских дней висели в&nbsp;&laquo;Олимпийском&raquo;
                                на&nbsp;репетициях &laquo;Рождественских встреч&raquo;, он&nbsp;нам
                                и&nbsp;предложил &laquo;покраудфандить&raquo;.
                                А&nbsp;поскольку нас хлебом не&nbsp;корми, дай только ввязаться в&nbsp;новую модную
                                историю,
                                мы&nbsp;согласились. Мне кажется, все остались довольны.
                            </div>
                        </div>

                        <br><br>
                        <div class="post-milestones large-milestones">
                            <div class="post-milestones-list clearfix">
                                <div class="p-milestones-list-title minionpro-mediumit">Вехи проекта</div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">6 февраля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Проект «Запись нового альбома группы «Ундервуд» открыт на «Планете»
                                        </div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">10 апреля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Проект собрал заявленную сумму, но музыканты решили не останавливаться на достигнутом и продлили проект до 15 мая
                                        </div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">4 июня 2012</div>
                                        <div class="pml-items-item-text proxima-reg">Пользователям отправлен сингл «Самая красивая девушка в мире» с будущего альбома
                                        </div>
                                    </div>
                                </div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">14 марта 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Был приобретен первый из трех супер-бонусов – «вечный билет» на все публичные выступления группы
                                        </div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">1 мая 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Пользователи «Планеты» увидели прямую трансляцию концерта группы «Ундервуд» из клуба «16 Тонн»
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="post-content-text proxima-reg">
                            <div class="p-content-notice helveticaneue-bold relative">
                                В&nbsp;качестве бонусов &laquo;ундервуды&raquo; предложили акционерам настоящие раритеты: шляпу и&nbsp;футболку из&nbsp;клипа &laquo;Йога и&nbsp;алкоголь&raquo;, черновики четырех песен и&nbsp;целых три (!) супербонуса&nbsp;&mdash; &laquo;вечные билеты&raquo; на&nbsp;выступления коллектива. И&nbsp;(о, чудо!) все три билета были раскуплены преданными поклонниками группы.
                                <img src="images/undervud/prize-01.jpg" class="side-pic">
                            </div>
                        </div>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/kurator/ava-egor.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Егор Ельчин</div>
                            <div class="p-content-manager-role">куратор проекта</div>
                            <div class="p-content-manager-quote">
                                Покупка ТРЕХ вечных билетов! Это&nbsp;ли не&nbsp;показатель того, что у&nbsp;группы
                                потрясающие поклонники. И&nbsp;такие они благодаря творчеству &laquo;Ундервуд&raquo;
                            </div>
                        </div>

                    </div>



                    <!--<div class="check-photo">
                        <img src="images/undervud/check.jpg">
                    </div>-->

                    <!--<div class="post-content">
                        <a class="post-another-posts h-link minionpro-semiboldit" href="index.php">Другие успешные
                            проекты</a>
                    </div>-->

                </div>
            </div>
                    <? include 'includes/index-data.php'; ?>

        </div>
    </div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
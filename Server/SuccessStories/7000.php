<?
$title = '7000$ — новый альбом';

$collected = 148421;
$target = 125000;

$dateDuration = '1 год 3 дня';

$startDay = 6;
$startMonth = 'марта';
$startYear = 2013;

$endDay = 9;
$endMonth = 'марта';
$endYear = 2014;

$members = 173;

$projectLink = 'https://planeta.ru/campaigns/425';

$collected = number_format($collected, 0, '.', ' ');
$target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

    <div class="post" itemscope itemtype="http://schema.org/CreativeWork">
        <div class="wrap">
            <a class="post-back-link" href="index.php">Истории успеха</a>

            <div itemprop="name" class="post-title minionpro-boldit"><?=$title?></div>
            <div class="post-main">

                <? require 'includes/post-meta.php'; ?>

                <img itemprop="image" class="post-big-img" src="images/7000/check.jpg">

                <div class="post-middle">
                    <? require 'includes/share.php' ?>
                    <div class="post-content">
                        <div itemprop="description" class="post-content-text proxima-reg">
                            Нижегородская группа &laquo;7000$&raquo; выбрала верную стратегию по&nbsp;отношению к&nbsp;своему проекту на&nbsp;Планете. Ребята использовали эту возможность как способ наведения прямого моста между собой и&nbsp;своей аудиторией. А&nbsp;помогла им&nbsp;в&nbsp;этом нелегком процессе музыка и&nbsp;новый альбом с&nbsp;говорящим названием &laquo;Тень независимости&raquo;.
                            <br>
                            <br>
                            В&nbsp;итоге творческий процесс оказался настолько захватывающим, что группа успела не&nbsp;только записать альбом, но&nbsp;и&nbsp;провести несколько конкурсов, создать закрытое сообщество для акционеров и, конечно&nbsp;же&hellip; запустить еще один проект!
                        </div>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/7000/ava-author.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Группа «7000$»</div>
                            <div class="p-content-manager-role"></div>
                            <div class="p-content-manager-quote">
                                Мы&nbsp;очень рады, что все наши проекты состоялись и&nbsp;нас поддержали очень много людей со&nbsp;всей России и&nbsp;зарубежья. Это отличная возможность для нового уровня общения непосредственно с&nbsp;нами, возможность получить майки диски и&nbsp;много других сюрпризов. Конечно, мы&nbsp;не&nbsp;ставили себе цель много заработать&nbsp;&mdash; это &laquo;пробный залп&raquo;, возможность получить отличные акции и&nbsp;поддержать нас. И&nbsp;все прошло на&nbsp;отлично! А&nbsp;значит можно продолжать, ведь у&nbsp;группы впереди много и&nbsp;мы&nbsp;будем счастливы, если наши слушатели смогут стать частью этого!
                                <br>
                                <br>
                                Благодарим всех за&nbsp;участие и&nbsp;поддержку! И&nbsp;до&nbsp;встречи на&nbsp;презентациях!
                            </div>
                        </div>

                        <br>

                        <div class="post-content-text proxima-reg">
                            <div class="p-content-notice helveticaneue-bold">
                                Секрет быстрого наведения мостов в&nbsp;прямом контакте, поэтому акционеры проекта были приглашены не&nbsp;только на&nbsp;концерты и&nbsp;в&nbsp;репетиционную студию, но&nbsp;и&nbsp;смогли проникнуть за&nbsp;сцену, воспользовавшись специальным &laquo;вездеходом&raquo; и&nbsp;даже на&nbsp;<nobr>радио-эфир</nobr> вместе с&nbsp;группой.
                            </div>
                        </div>


                        <br>


                        <div class="post-milestones large-milestones">
                            <div class="post-milestones-list clearfix">
                                <div class="p-milestones-list-title minionpro-mediumit">Вехи проекта</div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">9 марта 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Проект по записи нового альбома «7000$» стартовал на Планете.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">6 мая 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Создано <a href="https://planeta.ru/tfg7000">закрытое сообщество</a> для акционеров проекта True Fan Group, в&nbsp;котором группа начинает размещать эксклюзивные материалы о&nbsp;готовящемся альбоме.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">24 мая 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Группа разыгрывает первый в&nbsp;рамках проекта Вечный билет на&nbsp;все концерта &laquo;7000$&raquo;.</div>
                                    </div>
                                </div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">13 февраля 2014</div>
                                        <div class="pml-items-item-text proxima-reg">Группа <a href="https://planeta.ru/planet7000/blog/123379">приглашает</a> акционеров принять участие в&nbsp;эфире &laquo;Наше радио&raquo; и&nbsp;стать первыми слушателями новых песен с&nbsp;альбома &laquo;Тень независимости&raquo;.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">18 марта 2014</div>
                                        <div class="pml-items-item-text proxima-reg"><a href="https://planeta.ru/planet7000/blog/124145">Запущен второй проект</a> &laquo;7000$&raquo; по&nbsp;сведению англоязычной версии альбома &laquo;Тень независимости&raquo;.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">28 марта 2013</div>
                                        <div class="pml-items-item-text proxima-reg">В&nbsp;Москве состоялась презентация нового альбома группы &laquo;Тень независимости&raquo;.</div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="post-content-manager mrg-t-50 mrg-b-50">
                            <div class="p-content-manager-ava">
                                <img src="images/kurator/ava-nika.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Ника Зеленова</div>
                            <div class="p-content-manager-role">куратор проекта</div>
                            <div class="p-content-manager-quote">
                                Это одна из&nbsp;тех историй, когда совместная работа над проектом с&nbsp;авторами в&nbsp;дальнейшем перерастает в&nbsp;искреннюю дружбу. Соответственно, все, что могу сказать&nbsp;&mdash; огромное спасибо за&nbsp;невероятную отзывчивость к&nbsp;новым идеям, предложениям, а&nbsp;так&nbsp;же за&nbsp;собственную завидную инициативу в&nbsp;рамках проекта! Пожалуй, парни подарили мне самый благодарный проект в&nbsp;моей жизни. Очень люблю этих ребят, и&nbsp;всей душой верила в&nbsp;их&nbsp;успех!
                            </div>
                        </div>

                    </div>

                </div>
            </div>
                <? include 'includes/index-data.php'; ?>

        </div>
    </div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
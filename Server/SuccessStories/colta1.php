<?
$title = 'Запуск сайта Colta.ru, нового проекта команды OpenSpace';

$collected = 695103;
$target = 600000;

    $dateDuration = '4 месяца 10 дней';

    $startDay = 6;
    $startMonth = 'июля';
    $startYear = 2012;

$endDay = 16;
$endMonth = 'октября';
$endYear = 2012;

$members = 524;

$projectLink = 'https://planeta.ru/campaigns/106';

$collected = number_format($collected, 0, '.', ' ');
$target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

    <div class="post" itemscope itemtype="http://schema.org/CreativeWork">
        <div class="wrap">
            <a class="post-back-link" href="index.php">Истории успеха</a>
            <div itemprop="name" class="post-title minionpro-boldit">Запуск сайта Colta.ru, нового проекта команды OpenSpace</div>
            <div class="post-main">

                <? require 'includes/post-meta.php'; ?>

                <img itemprop="image" class="post-big-img" src="images/colta1/big-image.jpg">

                <div class="post-middle">
                    <? require 'includes/share.php' ?>
                    <div class="post-content">

                        <div itemprop="description" class="post-content-text proxima-reg">
                            <a href="http://colta.ru/" target="_blank">Colta.ru</a>&nbsp;&mdash; это первое и&nbsp;фактически единственное в&nbsp;России общественное СМИ о&nbsp;культуре и&nbsp;духе времени. Независимый проект бывшей команды OpenSpace, над которым работают лучшие люди профессии,&nbsp;&mdash; лаконичный, современный, правдивый. Мы&nbsp;гордимся, что рождение Colta.ru стало возможным благодаря краудфандингу и&nbsp;&laquo;Планете&raquo;.

                            <div class="p-content-notice helveticaneue-bold">
                                Благодаря пользователям &laquo;Планеты&raquo; проект Colta еще долго не&nbsp;будет
                                требовать перезарядки&nbsp;&mdash; а&nbsp;главное, что только с&nbsp;ними он&nbsp;стал
                                возможен!
                            </div>

                        </div>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/colta1/ava-artist.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Михаил Ратгауз</div>
                            <div class="p-content-manager-role">заместитель главного редактора Colta.ru</div>
                            <div class="p-content-manager-quote">
                                Встречи с&nbsp;&laquo;Планетой&raquo; (их&nbsp;было две, а&nbsp;одна еще продолжается)
                                доставили редакции сайта Colta.ru много незабываемых минут. Никогда еще читатель не&nbsp;был
                                так близок, никогда еще его привязанность к&nbsp;сайту не&nbsp;носила такие конкретные
                                очертания. Да&nbsp;что говорить, &laquo;Кольте&raquo; скоро будет год&nbsp;&mdash; и&nbsp;ее&nbsp;просто&nbsp;бы
                                не&nbsp;было без сайта Planeta.ru. Спасибо, ребята! И&nbsp;спасибо тем, кто на&nbsp;эту
                                самую &laquo;Планету&raquo; приземлялся с&nbsp;поддержкой и&nbsp;продолжает это делать!
                            </div>
                        </div>

                        <br>

                        <div class="post-milestones large-milestones">
                            <div class="post-milestones-list clearfix">
                                <div class="p-milestones-list-title minionpro-mediumit">Вехи проекта</div>
                                <div class="p-milestones-list-items">

                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">6 июля 2012</div>
                                        <div class="pml-items-item-text proxima-reg">
                                            Проект по&nbsp;запуску сайта Colta.ru стартовал на&nbsp;&laquo;Планете&raquo;.
                                        </div>
                                    </div>

                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">26 июля 2012</div>
                                        <div class="pml-items-item-text proxima-reg">
                                            В&nbsp;рамках проекта прошел &laquo;Вечер Colta.ru&raquo; в&nbsp;клубе &laquo;Солянка&raquo;. Программа включала: выступления поэтов ЛЬВА РУБИНШТЕЙНА и&nbsp;ДМИТРИЯ ВОДЕННИКОВА, показ фильма <nobr>&laquo;Мамон-Лобан&raquo;</nobr> СЕРГЕЯ ЛОБАНА (<nobr>&laquo;Шапито-шоу&raquo;</nobr>) о&nbsp;ПЕТРЕ МАМОНОВЕ, выступление модной московской команды THE RETUSES. И&nbsp;специальный получасовой лайв ДЕЛЬФИНА в&nbsp;поддержку Colta.ru!
                                        </div>
                                    </div>

                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">16 октября </div>
                                        <div class="pml-items-item-text proxima-reg">
                                            Проект благополучно завершен&nbsp;&mdash; его сборы на&nbsp;95 тысяч рублей превысили заявленную сумму.
                                        </div>
                                    </div>

                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">14 января 2013</div>
                                        <div class="pml-items-item-text proxima-reg">
                                            На&nbsp;&laquo;Планете&raquo; стартовал второй, дополнительный проект Colta.ru&nbsp;&mdash; &laquo;Colta.ru. Перезарядка&raquo;. Сайт собирал средства на&nbsp;развитие и&nbsp;продолжение существования до&nbsp;конца 2013 года. И&nbsp;уже через три дня после старта было собрано 40% от&nbsp;заявленной суммы в&nbsp;400&nbsp;000&nbsp;рублей.
                                        </div>
                                    </div>

                                </div>
                                <div class="p-milestones-list-items">

                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-text proxima-reg">
                                            <img src="https://s1.planeta.ru/i/be8e/medium.jpg">
                                        </div>
                                    </div>

                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">7 апреля  2013</div>
                                        <div class="pml-items-item-text proxima-reg">
                                            В&nbsp;рамках дополнительного проекта &laquo;Colta.ru. Перезарядка&raquo; редакция организовала закрытую презентацию, которая прошла в&nbsp;клубе ArteFAQ. Мероприятие состояло из&nbsp;четырех частей: началось с&nbsp;короткометражных фильмов Ренаты Литвиновой, Бориса Хлебникова, Михаила Сегала и&nbsp;премьеры микросериала Александра Расторгуева и&nbsp;Павла Костомарова; продолжилось премьерой спектакля Дмитрия Волкострелова по&nbsp;пьесе, специально для этого вечера написанной Михаилом Дурненковым. Завершили вечер выступления групп &laquo;Шкловский&raquo; и&nbsp;<nobr>On-The-Go</nobr>, продолжившиеся дискотекой для всех участников.
                                        </div>
                                    </div>

                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">18 апреля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">
                                            Проект &laquo;Colta.ru. Перезарядка&raquo; продлен до&nbsp;1 августа. На&nbsp;сегодня в&nbsp;его копилке&nbsp;&mdash; 833&nbsp;000&nbsp;рублей&nbsp;&mdash; это более чем в&nbsp;2 раза больше заявленной суммы.
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                        <div class="post-content-text proxima-reg">
                            <div class="p-content-notice helveticaneue-bold">
                                Акции проекта сопровождали поистине уникальные бонусы: книга Льва Рубинштейна и&nbsp;Бориса
                                Акунина &laquo;От&nbsp;мая до&nbsp;мая&raquo; с&nbsp;автографами авторов, экскурсия для 5
                                человек по&nbsp;закулисью Большого театра, ретвит от&nbsp;Олега Кашина, сюрреалистические
                                брюки
                                из&nbsp;клипа &laquo;Ляписа Трубецкого&raquo;, прогулка с&nbsp;Дмитрием Гутовым по&nbsp;залам
                                Третьяковской галереи, бутлег The Cure &laquo;Three Imaginary Boys&raquo; с&nbsp;автографами
                                музыкантов, плакаты фильма &laquo;Фауст&raquo;, подписанные Александром Сокуровым и&nbsp;многое
                                другое.
                            </div>
                        </div>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/kurator/ava-chechulina.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Катерина Чечулина</div>
                            <div class="p-content-manager-role">PR-директор Planeta.ru</div>
                            <div class="p-content-manager-quote">
                                Проект Colta.ru был для нас знаковым. Мы&nbsp;запустили его ровно через месяц после
                                официального открытия &laquo;Планеты&raquo;, 6 июля 2012. Тогда мало кто в&nbsp;России
                                знал,
                                что такое краудфандинг.&nbsp;Мария Степанова и&nbsp;вся редакция не&nbsp;побоялись
                                эксперимента, за&nbsp;что им&nbsp;низкий поклон. Помню, как долго мы&nbsp;с&nbsp;Мишей
                                Ратгаузом совещались, исправляли тексты, акции и&nbsp;нажали на&nbsp;&laquo;старт&raquo;
                                в&nbsp;три
                                часа ночи. Через сутки в&nbsp;копилке проекта было 200 тысяч рублей, а&nbsp;через 20
                                дней&nbsp;&mdash;
                                вся необходимая сумма. Этот успех был свидетельством не&nbsp;только огромной любви
                                аудитории
                                к&nbsp;редакции, но&nbsp;и&nbsp;четким подтверждением того, что краудфандинг в&nbsp;России
                                возможен. Люди увидели, что он&nbsp;работает! Вообще, &laquo;Кольта&raquo; разрушила все обычные
                                правила
                                краудфандинга.
                                <nobr>Во-первых</nobr>, это журналистский проект, на&nbsp;которые даже на&nbsp;Западе сборы идут довольно
                                сложно.
                                <nobr>Во-вторых</nobr>, Кольта, собрав почти 700 тысяч в&nbsp;первом проекте, открыла следующий и&nbsp;вновь
                                собрала уже около 800 тысяч, что тоже редкость. Я&nbsp;очень рада, что &laquo;Планета&raquo;
                                причастна к&nbsp;тому, что Colta.ru, фактически единственное общественное СМИ в&nbsp;России,
                                сегодня существует.
                            </div>
                        </div>
                    </div>


                    <!--<div class="post-content">
                        <a class="post-another-posts h-link minionpro-semiboldit" href="index.php">Другие успешные
                            проекты</a>
                    </div>-->

                </div>
            </div>
                <? include 'includes/index-data.php'; ?>

        </div>
    </div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
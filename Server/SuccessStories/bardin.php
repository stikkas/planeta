<?
$title = 'Новый мультфильм Гарри Бардина &laquo;Три мелодии&raquo;';

$collected = 2249681;
$target = 2200000;

$dateDuration = '3 месяца 2 дня';

$startDay = 29;
$startMonth = 'мая';
$startYear = 2013;

$endDay = 31;
$endMonth = 'августа';
$endYear = 2013;

$members = 883;

$projectLink = 'https://planeta.ru/campaigns/995';

$blog_link = "https://planeta.ru/bardin/blog/119283";
$blog_link_label = "Блог Гарри Бардина";

$collected = number_format($collected, 0, '.', ' ');
$target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

    <div class="post" itemscope itemtype="http://schema.org/CreativeWork">
        <div class="wrap">
            <a class="post-back-link" href="index.php">Истории успеха</a>

            <div itemprop="name" class="post-title minionpro-boldit"><?=$title?></div>
            <div class="post-main">

                <? require 'includes/post-meta.php'; ?>

                <img itemprop="image" class="post-big-img" src="images/bardin/check-title.jpg">

                <div class="post-middle">
                    <? require 'includes/share.php' ?>
                    <div class="post-content">
                        <div itemprop="description" class="post-content-text proxima-reg">
                            Когда Гарри Яковлевич Бардин начал работать над новой музыкальной трилогией, Министерство культуры выделило средства всего на&nbsp;два мультфильма из&nbsp;трех. Режиссеру пришлось обратиться к&nbsp;краудфандингу, чтобы проект получился таким, каким задумывался изначально. Идею поддержали как простые пользователи, так и&nbsp;известные деятели культуры: свое видеообращение к&nbsp;пользователям Сети записали Эльдар Рязанов и&nbsp;Виктор Шендерович, участники &laquo;Квартета И&raquo; и&nbsp;Алексей Кортнев.<br /><br />
                            Министерство культуры сократило не&nbsp;только финансирование работы, но&nbsp;и&nbsp;время, отведенное на&nbsp;создание трилогии. Гарри Яковлевичу не&nbsp;удалось закончить мультфильм в&nbsp;указанный срок, ведь ему было необходимо собрать средства и&nbsp;снять оставшуюся часть &laquo;Трех мелодий&raquo; на&nbsp;должном уровне. После того, как стало известно, о&nbsp;штрафных санкциях, наложенных на&nbsp;режиссера за&nbsp;нарушение сроков, пользователи собрали оставшиеся 400 тысяч рублей буквально за&nbsp;один день. Портал &laquo;Планета&raquo;, в&nbsp;свою очередь, добавил еще 200 тысяч рублей для оплаты штрафа. После этого режиссер признался, что изменил свое отношение как к&nbsp;Министерству культуры, так и&nbsp;к&nbsp;зрителям, которые удивили его своим теплым откликом и&nbsp;готовностью помочь.
                        </div>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/bardin/ava-artist.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Гарри Бардин</div>
                            <div class="p-content-manager-role">автор проекта</div>
                            <div class="p-content-manager-quote">
                                Хорошо, что есть Planeta.ru, к&nbsp;которой я&nbsp;однажды имел отношение. Для меня это&nbsp;&mdash; &laquo;с&nbsp;миру по&nbsp;нитке&raquo;, но&nbsp;ниток оказалось так много, что это дало мне возможность закончить картину. Помимо &laquo;Планеты&raquo; я&nbsp;благодарен еще и&nbsp;тем акционерам, которые поучаствовали в&nbsp;сборе средств на&nbsp;завершение &laquo;Трех мелодий&raquo;.
                            </div>
                        </div>

                        <br><br>


                        <div class="post-content-text proxima-reg mrg-b-0">
                            <div class="p-content-notice helveticaneue-bold">
                                Помогая создавать &laquo;Три мелодии&raquo;, пользователи &laquo;Планеты&raquo; смогли приобщиться к&nbsp;волшебному миру мультипликации во&nbsp;всех смыслах. В&nbsp;благодарность за&nbsp;поддержку Гарри Бардин провел для них экскурсию по&nbsp;анимационной студии, где создаются шедевры автора, а&nbsp;также предоставил в&nbsp;качестве бонусов куклы, использовавшиеся в&nbsp;съемках &laquo;Гадкого утенка&raquo;.
                            </div>
                        </div>

                        <div class="h-video">
                            <iframe width="720" height="405" frameborder="0" allowfullscreen="" src="//tv.planeta.ru/video-frame?profileId=91664&amp;videoId=27308&amp;autostart=false&amp;fromCampaign="></iframe>
                        </div>


                        <br><br>



                        <div class="post-content-manager mrg-b-50">
                            <div class="p-content-manager-ava">
                                <img src="images/kurator/ava-sorokina.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Мария Сорокина</div>
                            <div class="p-content-manager-role">куратор проекта</div>
                            <div class="p-content-manager-quote">Встреча и&nbsp;работа с&nbsp;Гарри Яковлевичем Бардиным&nbsp;&mdash; это большая радость! Это уникальный человек, сочетающий в&nbsp;себе редкие на&nbsp;сегодняшний день человеческие качества, удивительный талант и&nbsp;чувство юмора. Всем рекомендую прочитать его книгу &bdquo;И&nbsp;вот наступило потом&hellip;&ldquo;. Рада, что все успешно завершилось. Поздравляю Гарри Яковлевича и&nbsp;желаю успехов!
                            </div>
                        </div>

                        <div class="post-milestones large-milestones">
                            <div class="post-milestones-list clearfix">
                                <div class="p-milestones-list-title minionpro-mediumit">Вехи проекта</div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">29 мая 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Запуск проекта.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">15 июля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Эльдар Рязанов поддержал Гарри Бардина, сделав видео-обращение.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">17 июля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Алексей Кортнев поддержал проект Гарри Бардина, сделав видео-обращение.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">13 августа 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Стало известно, что Гарри Бардин будет вынужден заплатить штраф Министерству культуры. После этой новости поклонники внесли в проект более 400 000 рублей за одни сутки.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">31 августа 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Успешное завершение проекта.</div>
                                    </div>
                                </div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">8 июля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">&laquo;Квартет И&raquo; поддержал проект Гарри Бардина, сделав <nobr>видео-обращение</nobr>.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">16 июля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">В&nbsp;проект была добавлена новая акция: &laquo;Раскадровка мультфильма &laquo;Гадкий утенок&raquo;.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">22 июля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Виктор Шендерович поддержал проект Гарри Бардина, сделав видео-обращение.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">15 августа 2013</div>
                                        <div class="pml-items-item-text proxima-reg">&laquo;Планета&raquo; внесла 200&nbsp;000&nbsp;рублей в&nbsp;проект Гарри Бардина, чтобы покрыть штрафные санкции Министерства культуры.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
                <? include 'includes/index-data.php'; ?>

        </div>
    </div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
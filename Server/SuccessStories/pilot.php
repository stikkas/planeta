<?

$title = 'Пилот';

$collected = 609433;
$target = 400000;

$dateDuration = '3 месяца 22 дня';

$startDay = 9;
$startMonth = 'февраля';
$startYear = 2013;

$endDay = 31;
$endMonth = 'мая';
$endYear = 2013;

$members = 427;

$projectLink = 'https://planeta.ru/campaigns/226';

$collected = number_format($collected, 0, '.', ' ');
$target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

    <div class="post" itemscope itemtype="http://schema.org/CreativeWork">
        <div class="wrap">
            <a class="post-back-link" href="index.php">Истории успеха</a>

            <div itemprop="name" class="post-title minionpro-boldit">Запись нового альбома группы &laquo;Пилот&raquo;</div>
            <div class="post-main">

                <? require 'includes/post-meta.php'; ?>

                <img itemprop="image" class="post-big-img" src="images/pilot/check.jpg">

                <div class="post-middle">
                    <? require 'includes/share.php' ?>
                    <div class="post-content">
                        <div itemprop="description" class="post-content-text proxima-reg">
                            Достигнув музыкального совершеннолетия, музыканты питерской
                            <nobr>рок-группы</nobr> &laquo;Пилот&raquo; решили, что им&nbsp;наконец можно, и&nbsp;перешли
                            на&nbsp;принципиально новые отношения с&nbsp;поклонниками (а&nbsp;за&nbsp;16 лет
                            существования коллектива таковых собралось немало). Свой
                            <nobr>13-й</nobr> альбом Илья Чёрт и&nbsp;Ко&nbsp;создавали с&nbsp;помощью краудфандинга. Релиз новой
                            пластинки, которая пока не&nbsp;имеет названия, запланирован на&nbsp;начало осени.
                        </div>

                        <div class="post-content-text proxima-reg">
                            <div class="p-content-notice helveticaneue-bold">
                                &laquo;Пилотный&raquo; проект группы &laquo;Пилот&raquo; прошел под грамотным
                                управлением менеджеров &laquo;Планеты&raquo;&nbsp;&mdash; и&nbsp;группа совершила мягкую
                                посадку в&nbsp;студии для записи альбома.
                            </div>
                        </div>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/pilot/ava-artist.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Пилот</div>
                            <div class="p-content-manager-role">Из обращения участников группы к акционерам</div>
                            <div class="p-content-manager-quote">
                                Мы&nbsp;сделали это! Наш проект на&nbsp;Planeta.ru завершился успешнее некуда! От&nbsp;всей
                                души благодарим вас за&nbsp;активное участие и&nbsp;поддержку!
                            </div>
                        </div>

                        <br>

                        <div class="post-content-text proxima-reg">
                            <div class="p-content-notice helveticaneue-bold">
                                В&nbsp;числе самых интересных бонусов проекта присутствовал набор, включавший в&nbsp;себя LP&nbsp;и&nbsp;CD, постер с&nbsp;автографами и&nbsp;футболку группы, а&nbsp;главное&nbsp;&mdash; право посетить <nobr>Boat-Live-Show</nobr> в&nbsp;<nobr>Санкт-Петербурге</nobr> и&nbsp;пообщаться с&nbsp;музыкантами, первыми услышав новые песни.
                            </div>
                        </div>

                        <div class="post-content-text proxima-reg mrg-b-0">
                            Лидер группы &laquo;Пилот&raquo; Илья Чёрт о&nbsp;своих братских чувствах к&nbsp;&laquo;Планете&raquo; в&nbsp;целом, и&nbsp;к&nbsp;Василине Горовой, в&nbsp;частности.
                        </div>

                        <div class="h-video mrg-b-30">
                            <iframe width="720" height="405" frameborder="0" allowfullscreen=""
                                    src="https://tv.planeta.ru/video-frame?profileId=22956&amp;videoId=21767&amp;autostart=false"></iframe>
                        </div>

                        <div class="post-milestones large-milestones">
                            <div class="post-milestones-list clearfix">
                                <div class="p-milestones-list-title minionpro-mediumit">Вехи проекта</div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">9 февраля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Проект «Запись нового альбома группы «Пилот» открыт на «Планете»
                                        </div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">31 мая 2012</div>
                                        <div class="pml-items-item-text proxima-reg">Проект «Пилотов» был успешно завершен. Собранная сумма в полтора раза превысила заявленную. Это дало музыкантам возможность добавить в трэк-лист будущего диска еще несколько композиций.
                                        </div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">3 июня 2013</div>
                                        <div class="pml-items-item-text proxima-reg">«Пилоты» приступили к работе над новым альбомом в звукозаписывающей студии
                                        </div>
                                    </div>
                                </div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">16 апреля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Группа с огоньком отметила свое 16-летие в прямом эфире на Planeta.ru. Поклонники следили за онлайн трансляцией праздничного концерта из клуба Arena Moscow.
                                        </div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">1 июня 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Завершение своего проекта «Пилоты» решили отметить концертом в столичном клубе P!PL. Поклонники, купившие специальные акции, могли подняться на сцену прямо во время выступления и сделать с музыкантами коллективное фото!
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <br><br>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/kurator/ava-skvortsov.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Сергей Скворцов</div>
                            <div class="p-content-manager-role">куратор проекта</div>
                            <div class="p-content-manager-quote">
                                Очень своевременно отвечающие и&nbsp;быстрореагирующие артисты. Приятно работать с&nbsp;ними
                                и&nbsp;их&nbsp;менеджером!
                            </div>
                        </div>
                    </div>

                </div>

            </div>
                <? include 'includes/index-data.php'; ?>

        </div>
    </div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
<?
$title = 'Новый альбом Tinavie &laquo;Комета&raquo;';

$collected = 269500;
$target = 250000;

$dateDuration = '2 месяца 22 дня';

$startDay = 30;
$startMonth = 'августа';
$startYear = 2013;

$endDay = 21;
$endMonth = 'ноября';
$endYear = 2013;

$members = 167;

$projectLink = 'https://planeta.ru/campaigns/1966';

$collected = number_format($collected, 0, '.', ' ');
$target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

    <div class="post" itemscope itemtype="http://schema.org/CreativeWork">
        <div class="wrap">
            <a class="post-back-link" href="index.php">Истории успеха</a>

            <div itemprop="name" class="post-title minionpro-boldit"><?=$title?></div>
            <div class="post-main">

                <? require 'includes/post-meta.php'; ?>

                <img itemprop="image" class="post-big-img" src="images/tinavie/check.jpg">

                <div class="post-middle">
                    <? require 'includes/share.php' ?>
                    <div class="post-content">
                        <div itemprop="description" class="post-content-text proxima-reg">
                            Tinavie существуют уже более пяти лет. За&nbsp;это время группа успела заслужить звание одного из&nbsp;лучших музыкальных проектов последних лет, выпустить три альбома, сыграть несколько десятков концертов, принять участие в&nbsp;многочисленных музыкальных фестивалях и&nbsp;съездить в&nbsp;тур по&nbsp;Америке вместе с&nbsp;группой Zorge. Свой последний альбом группа решила записать при участии поклонников и&nbsp;запустила проект на&nbsp;planeta.ru. Так появилась &laquo;&laquo;Kometa&raquo;&nbsp;&mdash; один из&nbsp;самых ярких альбомов ушедшего года, презентация которого с&nbsp;большим успехом прошла в&nbsp;клубе &laquo;16 тонн&raquo;
                        </div>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/tinavie/ava-artist.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Дмитрий Зильперт</div>
                            <div class="p-content-manager-role">музыкант, гитарист Tinavie</div>
                            <div class="p-content-manager-quote">
                                Когда мы&nbsp;решились на&nbsp;создание проекта на&nbsp;&laquo;Планете&raquo;, мы&nbsp;очень переживали, поддержат&nbsp;ли его наши&nbsp;друзья и&nbsp;поклонники. Поэтому, думаю, самое большое наше достижение в&nbsp;этой истории это то, что теперь мы&nbsp;знаем, что вокруг Tinavie есть люди, для которых наша музыка и&nbsp;идеи так&nbsp;же важны, как и&nbsp;для нас. Спасибо всем, кто поучаствовал создании нашего нового альбома &laquo;Комета&raquo;, и&nbsp;отдельное спасибо команде &laquo;Планеты&raquo;!
                            </div>
                        </div>

                        <br><br>


                        <div class="post-content-text proxima-reg mrg-b-50">
                            <div class="p-content-notice helveticaneue-bold">
                                Для участников проекта группа подготовила много акций с&nbsp;возможностью личной встречи: <nobr>мастер-класс</nobr> по&nbsp;вокалу от&nbsp;Тины, по&nbsp;барабанам от&nbsp;Дмитрия Фролова, встреча с&nbsp;группой на&nbsp;репетиции и&nbsp;приглашение на&nbsp;презентацию в&nbsp;Москве и&nbsp;Питере. Одной из&nbsp;самых интересных акций в&nbsp;проекте оказался концерт в&nbsp;Центре фотографии им.&nbsp;Братьев Люмьер, где песни Tinavie исполнились в&nbsp;сопровождении струнного квартета.
                            </div>
                        </div>


                        <div class="post-milestones large-milestones">
                            <div class="post-milestones-list clearfix">
                                <div class="p-milestones-list-title minionpro-mediumit">Вехи проекта</div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">30 августа 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Проект по&nbsp;записи альбома Tinavie &laquo;Kometa&raquo; запустился на&nbsp;&laquo;Планете&raquo;.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">25 октября 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Tinavie проводит первый концерт со&nbsp;струнным оркестром в&nbsp;центре фотографии им.&nbsp;Братьев Люмьер.</div>
                                    </div>
                                </div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">17 сентября 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Были выкуплены все акции с&nbsp;вечным билетом на&nbsp;двоих на&nbsp;концерты группы.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">21 ноября 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Состоялась официальная презентация нового альбома группы в&nbsp;клубе &laquo;16 тонн&raquo;.</div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="post-content-manager mrg-t-50 mrg-b-50">
                            <div class="p-content-manager-ava">
                                <img src="images/kurator/ava-polina.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Полина Максимова</div>
                            <div class="p-content-manager-role">куратор проекта</div>
                            <div class="p-content-manager-quote">Здорово, что ребята были заинтересованы в&nbsp;проекте от&nbsp;начала и&nbsp;до&nbsp;конца: несмотря на&nbsp;постоянные концерты и&nbsp;разъезды, они были постоянно на&nbsp;связи и&nbsp;вовлечены в&nbsp;нашу совместную работу. Благодаря этому, все сложилось и&nbsp;получилось! Для меня&nbsp;же лично &laquo;Kometa&raquo; стала одним из&nbsp;лучших альбомов 2013 года. Тем приятней осознавать, что &laquo;Планета&raquo; также приняла участие в&nbsp;его&nbsp;выпуске.
                            </div>
                        </div>

                    </div>

                </div>
            </div>
                <? include 'includes/index-data.php'; ?>

        </div>
    </div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
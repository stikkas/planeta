<?
$title = 'Запись альбома &laquo;Тараканов!&raquo; &laquo;Maximum Happy&raquo;';
$collected = 500134;
$target = 250000;

$dateDuration = '7 месяцев 23 дня';

$startDay = 16;
$startMonth = 'ноября';
$startYear = 2012;

$endDay = 9;
$endMonth = 'июля';
$endYear = 2013;

$members = 448;

$projectLink = 'https://planeta.ru/campaigns/252';

$collected = number_format($collected, 0, '.', ' ');
$target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

    <div class="post" itemscope itemtype="http://schema.org/CreativeWork">
        <div class="wrap">
            <a class="post-back-link" href="index.php">Истории успеха</a>

            <div itemprop="name" class="post-title minionpro-boldit">Запись альбома &laquo;Тараканов!&raquo; &laquo;Maximum Happy&raquo;</div>
            <div class="post-main">

                <? require 'includes/post-meta.php'; ?>

                <img itemprop="image" class="post-big-img" src="images/tarakany-check.jpg">

                <div class="post-middle">
                    <? require 'includes/share.php' ?>
                    <div class="post-content">
                        <div itemprop="description" class="post-content-text proxima-reg">
                            Одна из&nbsp;самых отвязных отечественных <nobr>панк-команд</nobr> &laquo;Тараканы!&raquo;, задумывая альбом &laquo;Maximum Happy&raquo;, даже не&nbsp;подозревала, что максимум&nbsp;&mdash; это гораздо больше, чем 100%. Не&nbsp;подозревала, но&nbsp;готовилась изрядно: пластинку ребята планировали записывать не&nbsp;<nobr>где-нибудь</nobr>, а&nbsp;в&nbsp;Дюссельдорфе, на&nbsp;студии с&nbsp;говорящим названием Rock or&nbsp;Die, где, в&nbsp;частности, творят свои альбомы известные немецкие бунтари Die Toten Hosen. Пользуясь географическим положением, &laquo;Тараканы!&raquo; для достижения максимального счастья прихватили на&nbsp;свои треки и&nbsp;пару зарубежных звезд из&nbsp;групп Die Arzte и&nbsp;<nobr>Anti-Flag</nobr>, а&nbsp;также отечественных исполнителей. Ну, а&nbsp;дальше завертелось: конкурсы, предпрослушивания готового материала, презентация первого и&nbsp;второго диска&nbsp;&mdash; и&nbsp;Сид Спирин сотоварищи уверенно собрали в&nbsp;два раза больше требуемой суммы.
                        </div>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/ava-tarakan.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Дмитрий Спирин</div>
                            <div class="p-content-manager-role">лидер группы &laquo;Тараканы!&raquo;</div>
                            <div class="p-content-manager-quote">
                                Сейчас, оглядываясь назад, в&nbsp;ноябрь 2012, когда мы&nbsp;приняли решение отправляться в&nbsp;Дюссельдорф, не&nbsp;дожидаясь результатов нашего проекта на&nbsp;сайте Planeta.ru, мы&nbsp;сильно рисковали. Наиболее скептически настроенные наши коллеги даже отговаривали нас: &bdquo;Куда вы&nbsp;едете? Вы&nbsp;что, миллионеры? Что?! Фэны вам помогут? Ахахахххаххаха, не&nbsp;смешите! Фэны рока в&nbsp;России могут только потреблять музыку на&nbsp;халяву, сидя на&nbsp;форумах пиратских сайтов и&nbsp;совсем не&nbsp;задумываясь о&nbsp;том, во&nbsp;что это встало артистам!&ldquo;, говорили они.<br />
                                Мы&nbsp;счастливы, что у&nbsp;нас теперь есть полное право утереть нос этим коллегам и&nbsp;показать всему миру, что поклонники &bdquo;Тараканов!&ldquo; самые лучшие, самые преданные, самые крутые <nobr>рок-фэны</nobr>! Спасибо вам! И&nbsp;спасибо &bdquo;Планете&ldquo; за&nbsp;такую возможность!
                            </div>
                        </div>

                        <div class="post-content-text proxima-reg mrg-b-50 pdg-b-0">
                            <div class="p-content-notice helveticaneue-bold">
                                В&nbsp;благодарность за&nbsp;возможность записать крутой альбом &laquo;Тараканы!&raquo; подготовили для акционеров не&nbsp;менее крутые бонусы. Например, поклонники могли не&nbsp;только поприсутствовать на&nbsp;репетиции группы или получить музыкальный <nobr>мастер-класс</nobr> от&nbsp;одного из&nbsp;участников коллектива, но&nbsp;и&nbsp;получили возможность самостоятельно вписать любимый трек &laquo;Тараканов!&raquo; в&nbsp;<nobr>сет-лист</nobr> презентации.
                            </div>
                        </div>
                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/kurator/ava-nika.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Ника Зеленова</div>
                            <div class="p-content-manager-role">куратор проекта</div>
                            <div class="p-content-manager-quote">
                                Это было супер! На&nbsp;все собранные в&nbsp;итоге 200%! Двойной альбом, двойная отдача и&nbsp;в&nbsp;результате&nbsp;&mdash; двойное удовольствие для поклонников. И&nbsp;вести этот проект было вдвойне интересней. Повторим?
                            </div>
                        </div>
                        <iframe class="mrg-t-30" width="720" height="405" frameborder="0" allowfullscreen="" src="https://tv.planeta.ru/video-frame?profileId=91664&amp;videoId=23342&amp;autostart=false&amp;fromCampaign="></iframe>
                    </div>

                </div>
            </div>
                <? include 'includes/index-data.php'; ?>

        </div>
    </div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
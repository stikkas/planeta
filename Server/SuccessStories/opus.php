<?
$title = 'Дмитрий Ревякин';
$collected = 406350;
$target = 400000;

$dateDuration = '3 месяца 28 дней';

$startDay = 29;
$startMonth = 'ноября';
$startYear = 2012;

$endDay = 27;
$endMonth = 'марта';
$endYear = 2013;

$members = 282;

$projectLink = 'https://planeta.ru/campaigns/241';

$collected = number_format($collected, 0, '.', ' ');
$target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

    <div class="post" itemscope itemtype="http://schema.org/CreativeWork">
        <div class="wrap">
            <a class="post-back-link" href="index.php">Истории успеха</a>

            <div itemprop="name" class="post-title minionpro-boldit">Запись сольного альбома Дмитрия Ревякина &laquo;Grandi Canzoni,
                Opus 1&raquo;
            </div>
            <div class="post-main">

                <? require 'includes/post-meta.php'; ?>

                <img itemprop="image" class="post-big-img" src="images/opus/big-image.jpg">

                <div class="post-middle">
                    <? require 'includes/share.php' ?>
                    <div class="post-content">
                        <div itemprop="description" class="post-content-text proxima-reg">
                            &laquo;Главный шаман&raquo; отечественной <nobr>рок-сцены</nobr>, лидер и&nbsp;основатель легендарной новосибирской
                            группы &laquo;Калинов мост&raquo; Дмитрий Ревякин пришел на&nbsp;&laquo;Планету&raquo;,
                            чтобы записать свой альбом &laquo;Grandi Canzoni, Opus&nbsp;1&raquo;. Этот диск стал седьмой
                            сольной работой одного из&nbsp;самых харизматичных рок-музыкантов страны.
                        </div>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/opus/ava-artist.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Дмитрий Ревякин</div>
                            <div class="p-content-manager-role">музыкант, поэт, лидер группы &laquo;Калинов мост&raquo;</div>
                            <div class="p-content-manager-quote">
                                Скажу вам честно, мне очень приятно видеть имена людей, которые поддержали меня, имена
                                тех,
                                кому небезразлична судьба моего творчества. Благодаря вам я&nbsp;понимаю, что эти песни
                                действительно нужны, что их&nbsp;стоит записывать и&nbsp;выпускать в&nbsp;свет. Спасибо
                                вам
                                огромное! Страна у&nbsp;нас большая, и, вне зависимости от&nbsp;вашего географического
                                положения, вы&nbsp;всегда со&nbsp;мной рядом на&nbsp;Planeta.ru!
                            </div>
                        </div>

                        <br>

                        <div class="post-milestones large-milestones">
                            <div class="post-milestones-list clearfix">
                                <div class="p-milestones-list-title minionpro-mediumit">Вехи проекта</div>
                                <div class="p-milestones-list-items">

                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">29 ноября 2012</div>
                                        <div class="pml-items-item-text proxima-reg">
                                            На&nbsp;&laquo;Планете&raquo; стартовал проект &laquo;Запись сольного альбома Дмитрия Ревякина &laquo;Grandi Canzoni, Opus 1&raquo;.
                                        </div>
                                    </div>

                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">16 февраля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">
                                            Пользователи &laquo;Планеты&raquo; смогли посмотреть онлайн-трансляцию концерта Дмитрия Ревякина и&nbsp;группы &laquo;Калинов мост&raquo; из&nbsp;столичного клуба &laquo;16 Тонн&raquo;
                                        </div>
                                    </div>

                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">27 марта 2013</div>
                                        <div class="pml-items-item-text proxima-reg">
                                            Проект успешно завершен с&nbsp;небольшим превышениям заявленной суммы. А&nbsp;уже 2 февраля все акционеры проекта получили ссылку на&nbsp;бесплатное скачивание нового альбома Дмитрия Ревякина &laquo;Grandi Canzoni, Opus 1&raquo;.
                                        </div>
                                    </div>

                                </div>
                                <div class="p-milestones-list-items">

                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">29 марта 2013</div>
                                        <div class="pml-items-item-text proxima-reg">
                                            Дмитрий Ревякин провел в&nbsp;Доме Журналиста на&nbsp;Никитском бульваре творческую встречу с&nbsp;акционерами проекта. На&nbsp;вечере прозвучали не&nbsp;только песни из&nbsp;Grandi Canzoni, но&nbsp;и&nbsp;другие новые композиции из&nbsp;репертуара &laquo;Калинова Моста&raquo; и&nbsp;сольного творчества Дмитрия. Кроме того, музыкант ответил на&nbsp;многочисленные вопросы поклонников&hellip;
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="post-content-text proxima-reg mrg-b-50 pdg-b-0">
                            <div class="p-content-notice helveticaneue-bold">
                                Одним из&nbsp;самых востребованных бонусов проекта стали книга Дмитрия Ревякина &laquo;Знаки
                                небес&raquo; с&nbsp;автографом автора. Издания разлетались среди акционеров, как горячие
                                пирожки, и, по&nbsp;многочисленным просьбам, акции с&nbsp;книгой еще несколько раз
                                добавлялись в&nbsp;проект.
                            </div>

                            <br>

                            <img src="images/opus/book-1.jpg">

                            <br>

                            <img src="images/opus/book-2.jpg">

                        </div>
                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/kurator/ava-egor.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Егор Ельчин</div>
                            <div class="p-content-manager-role">куратор проекта</div>
                            <div class="p-content-manager-quote">
                                Самый душевный мой проект
                            </div>
                        </div>
                    </div>

                    <!--<div class="post-content">
                        <a class="post-another-posts h-link minionpro-semiboldit" href="index.php">Другие успешные
                            проекты</a>
                    </div>-->

                </div>
            </div>
                <? include 'includes/index-data.php'; ?>

        </div>
    </div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
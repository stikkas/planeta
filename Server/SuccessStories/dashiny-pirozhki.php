<?
$title = 'Дашины пирожки. Оборудование для пекарни';

$collected = 202016;
$target = 200000;

$dateDuration = '1 месяц 17 дней';

$startDay = 29;
$startMonth = 'июля';
$startYear = 2013;

$endDay = 15;
$endMonth = 'сентября';
$endYear = 2013;

$members = 123;

$projectLink = 'https://planeta.ru/campaigns/1547';

$collected = number_format($collected, 0, '.', ' ');
$target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

    <div class="post" itemscope itemtype="http://schema.org/CreativeWork">
        <div class="wrap">
            <a class="post-back-link" href="index.php">Истории успеха</a>

            <div itemprop="name" class="post-title minionpro-boldit"><?=$title?></div>
            <div class="post-main">

                <? require 'includes/post-meta.php'; ?>

                <img itemprop="image" class="post-big-img" src="images/dashiny-pirozhki/check.jpg">

                <div class="post-middle">
                    <? require 'includes/share.php' ?>
                    <div class="post-content">
                        <div itemprop="description" class="post-content-text proxima-reg">
                            К&nbsp;тому моменту, когда Даша Сонькина решила собирать деньги на&nbsp;Планете, проекту &laquo;Дашины пирожки&raquo; не&nbsp;было даже года. За&nbsp;это время из&nbsp;небольшой частной инициативы со&nbsp;страничкой в&nbsp;фейсбуке, Дашино дело превратилось в&nbsp;полноценный бизнес&nbsp;&mdash; душевный и&nbsp;вкусный. Что успели оценить по&nbsp;достоинству не&nbsp;только близкие друзья, но&nbsp;и&nbsp;посетители кулинарных фестивалей, гастрономических мероприятий, клиенты магазина &laquo;LavkaLavka&raquo; и, конечно&nbsp;же, заказчики, которых с&nbsp;каждым днем становилось все больше.
                            <br><br>
                            Вполне логично, что следующим шагом для &laquo;Дашиных пирожков&raquo; стало обустройство пекарного цеха, для которого в&nbsp;первую очередь нужно было закупить профессиональное оборудование. За&nbsp;полтора месяца акционеры Планеты помогли Даше справиться с&nbsp;этой задачей. Результат не&nbsp;заставил долго ждать: благодаря приобретенному на&nbsp;собранные деньги оборудованию, ребята выпекают около 2000 пирожков в&nbsp;месяц.
                        </div>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/dashiny-pirozhki/ava-artist.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Дарья Сонькина</div>
                            <div class="p-content-manager-role">Создатель проекта «Дашины пирожки»</div>
                            <div class="p-content-manager-quote">
                                Честно говоря, запуская краудфандинг на&nbsp;Планете, я&nbsp;думала больше о&nbsp;пиаре проекта, нежели о&nbsp;деньгах. Я&nbsp;очень сомневалась, что незнакомые люди ни&nbsp;с&nbsp;того ни&nbsp;с&nbsp;сего будут жертвовать деньги на&nbsp;неизвестный проект. К&nbsp;моменту начала краудфандинга у&nbsp;нас, конечно, уже были свои поклонники, но&nbsp;их&nbsp;вложений точно&nbsp;бы не&nbsp;хватило. Так что успешный сбор оказался очень приятным сюрпризом. Честно говоря, даже не&nbsp;знаю, что меня обрадовало больше: финансовый или моральный результат краудфандинга. Многие писали, что болеют за&nbsp;нас, а&nbsp;одна женщина фактически закрыла проект, купив две самых дорогостоящих акции. Впоследствии она написала, что очень волновалась, что за&nbsp;три дня до&nbsp;конца проекта у&nbsp;нас все еще не&nbsp;хватало <nobr>какой-то</nobr> суммы, поэтому она решила просто ее&nbsp;внести.
                            </div>
                        </div>

                        <br>
                        <br>

                        <div class="post-content-text proxima-reg">
                            <div class="p-content-notice helveticaneue-bold">
                                Каков проект, такие и&nbsp;бонусы: каждый акционер смог не&nbsp;только выбрать пирожки с&nbsp;любой начинкой, но&nbsp;и&nbsp;попасть на&nbsp;чаепитие с&nbsp;пирогами, устроить пикник на&nbsp;пять или десять друзей или получить пожизненные скидочные карточки на&nbsp;продукцию &laquo;Дашиных пирожков&raquo;. Особо любопытным предлагалось посетить <nobr>мастер-класс</nobr> и&nbsp;понаблюдать за&nbsp;творческим процессом.
                            </div>
                        </div>

                        <br><br>

                        <div class="post-milestones large-milestones">
                            <div class="post-milestones-list clearfix">
                                <div class="p-milestones-list-title minionpro-mediumit">Вехи проекта</div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">29 июля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Запуск проекта на «Планете».</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">2 августа 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Проект поддержала LavkaLavka, написав о «Дашиных пирожках» в <a href="http://lavkalavka.com/blog/lavka/pirozhok-v-kazhdyy-dom" target="_blank">своем блоге</a>.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">19 августа 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Каждый акционер, принявший участие в проекте, получил от Даши фирменный рецепт вдобавок к купленной акции.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">3 сентября 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Про &laquo;Дашины пирожки&raquo; <a href="http://chadeyka.livejournal.com/276063.html" target="_blank">написала</a> известный кулинарный блогер chadeyka.</div>
                                    </div>
                                </div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">Октябрь-декабрь 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Были проведены  мастер-классы по <a href="https://www.facebook.com/media/set/?set=a.553257818090826.1073741835.423203527762923&type=1&stream_ref=10" target="_blank">приготовлению пирожков.</a></div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-text proxima-reg">
                                            <img src="images/dashiny-pirozhki/milestones.jpg">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="post-content-manager mrg-t-50 mrg-b-50">
                            <div class="p-content-manager-ava">
                                <img src="images/kurator/ava-german.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Ольга Герман</div>
                            <div class="p-content-manager-role">куратор проекта</div>
                            <div class="p-content-manager-quote">
                                Для меня Даша Сонькина стала примером во&nbsp;всем! Она невероятно активная, ответственная, серьезная, обаятельная и&nbsp;уверенная в&nbsp;себе <nobr>девушка-предприниматель</nobr>. Работать с&nbsp;ней вместе в&nbsp;рамках проекта по&nbsp;открытию цеха было исключительное удовольствие и&nbsp;опыт. А&nbsp;посещение <nobr>мастер-класса</nobr> стало настоящим открытием и&nbsp;посвящением в&nbsp;таинство кулинарии. Уверена, что Дашу ждет большой успех в&nbsp;любом ее&nbsp;начинании. Талантливый человек талантлив во&nbsp;всем, проверено проектом &laquo;Дашины пирожки&raquo;!
                            </div>
                        </div>

                    </div>

                </div>
            </div>
                <? include 'includes/index-data.php'; ?>

        </div>
    </div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
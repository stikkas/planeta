<? require 'includes/header.php'; ?>

<div class="counters">
    <div class="wrap">
        <div class="counters-item">
            <div class="counters-item-num helveticaneue-bold">110</div>
            <div class="counters-item-desc proxima-reg">активных проектов на planeta.ru</div>
        </div>
        <div class="counters-item">
            <div class="counters-item-num helveticaneue-bold">65</div>
            <div class="counters-item-desc proxima-reg">успешных проектов</div>
        </div>
        <div class="counters-item">
            <div class="counters-item-num helveticaneue-bold">15%</div>
            <div class="counters-item-desc proxima-reg">собрали около 200% от цели</div>
        </div>
        <div class="counters-item">
            <div class="counters-item-num helveticaneue-bold">20</div>
            <div class="counters-item-desc proxima-reg">знаменитостей собрали средства</div>
        </div>
    </div>
</div>

<div class="wrap">
    <div class="stat">
        <div class="stat-item">
            <div class="stat-item-num helveticaneue-bold">20 000 000+</div>
            <div class="stat-item-desc proxima-reg">рублей собрано</div>
        </div>
        <div class="stat-item">
            <div class="stat-item-num helveticaneue-bold">18 000+</div>
            <div class="stat-item-desc proxima-reg">акций куплено</div>
        </div>
        <div class="stat-item">
            <div class="stat-item-num helveticaneue-bold">1 262 000 <span class="b-rub">Р</span></div>
            <div class="stat-item-desc proxima-reg">рекорд проекта</div>
        </div>
        <div class="stat-item">
            <div class="stat-item-num helveticaneue-bold">2000+</div>
            <div class="stat-item-desc proxima-reg">акционеров поддерживают более 1 проекта</div>
        </div>
    </div>

    <div class="map">
        <div class="map-title minionpro-mediumit">Количество купленных акций по России</div>
        <div class="map-subtitle proxima-reg">Актуально на май 2013</div>
        <div class="map-circle" style="top: 210px; left: 60px;">
            <div class="map-circle-inner">
                <div class="map-circle-desc">СПБ</div>
                <div class="map-circle-num helveticaneue-bold">976</div>
            </div>
            <span class="vertical-align"></span>
        </div>
        <div class="map-circle capital" style="top: 300px; left: 76px;">
            <div class="map-circle-inner">
                <i class="capital-icon"></i>
                <div class="map-circle-desc">Москва</div>
                <div class="map-circle-num helveticaneue-bold">4810</div>
            </div>
            <span class="vertical-align"></span>
        </div>
        <div class="map-circle" style="top: 341px; left: 190px;">
            <div class="map-circle-inner">
                <div class="map-circle-desc">ЕКБ</div>
                <div class="map-circle-num helveticaneue-bold">181</div>
            </div>
            <span class="vertical-align"></span>
        </div>
        <div class="map-circle" style="top: 283px; left: 140px;">
            <div class="map-circle-inner">
                <div class="map-circle-desc">Нижний Новгород</div>
                <div class="map-circle-num helveticaneue-bold">161</div>
            </div>
            <span class="vertical-align"></span>
        </div>
        <div class="map-circle" style="top: 301px; left: 280px;">
            <div class="map-circle-inner">
                <div class="map-circle-desc">Челябинск</div>
                <div class="map-circle-num helveticaneue-bold">128</div>
            </div>
            <span class="vertical-align"></span>
        </div>
        <div class="map-circle" style="top: 306px; left: 363px;">
            <div class="map-circle-inner">
                <div class="map-circle-desc">Ново-<br>сибирск</div>
                <div class="map-circle-num helveticaneue-bold">172</div>
            </div>
            <span class="vertical-align"></span>
        </div>
        <div class="map-circle" style="top: 400px; left: 83px;">
            <div class="map-circle-inner">
                <div class="map-circle-desc">Краснодар</div>
                <div class="map-circle-num helveticaneue-bold">148</div>
            </div>
            <span class="vertical-align"></span>
        </div>
        <div class="map-circle more" style="top: 132px; left: 348px;">
            <div class="map-circle-inner">
                <div class="map-circle-desc">Еще более<br>400 городов</div>
                <div class="map-circle-num helveticaneue-bold">6000+</div>
            </div>
            <span class="vertical-align"></span>
        </div>
    </div>
</div>

<div class="create-project">
    <a class="flat-btn" href="https://planeta.ru/funding-rules">Создать проект на Планете</a>
</div>

<? require 'includes/footer.php'; ?>
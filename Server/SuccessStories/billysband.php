<?
$title = 'Billy\'s Band';

$collected = 269840;
$target = 250000;

    $dateDuration = '6 месяцев 28 дней';

    $startDay = 19;
    $startMonth = 'сентября';
    $startYear = 2012;

$endDay = 15;
$endMonth = 'апреля';
$endYear = 2013;

$members = 100;

$projectLink = 'https://planeta.ru/campaigns/169';

$collected = number_format($collected, 0, '.', ' ');
$target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

    <div class="post" itemscope itemtype="http://schema.org/CreativeWork">
        <div class="wrap">
            <a class="post-back-link" href="index.php">Истории успеха</a>

            <div itemprop="name" class="post-title minionpro-boldit">Запись альбома группы Billy's Band &laquo;Когда был
                один&raquo;</div>
            <div class="post-main">

                <? require 'includes/post-meta.php'; ?>

                <img itemprop="image" class="post-big-img" src="images/billys/check.jpg">

                <div class="post-middle">
                    <? require 'includes/share.php' ?>
                    <div class="post-content">
                        <div itemprop="description" class="post-content-text proxima-reg">
                            Выходцы из&nbsp;питерского андеграунда Billy’s&nbsp;Band давно заслужили статус культовой
                            группы. Музыканты постоянно ищут новые формы, переходя с&nbsp;<nobr>алко-блюзового</nobr> угара на&nbsp;утонченный джазовый минимализм. Неизменно лишь одно &mdash; их&nbsp;пронзительные
                            песни и&nbsp;сумасшедшая энергетика покоряют сердца даже самых искушенных слушателей. Именно
                            благодаря им&nbsp;и&nbsp;краудфандингу на&nbsp;свет появилась
                            пластинка &laquo;биллисов&raquo; &laquo;Когда был один&raquo;.
                            <div class="p-content-notice helveticaneue-bold">Вопреки названию, альбом &laquo;Когда был один&raquo; Billy&rsquo;s Band делали вовсе не&nbsp;в&nbsp;одиночку, а&nbsp;с&nbsp;помощью пользователей &laquo;Планеты&raquo;!</div>
                        </div>
                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/billys/ava-artist.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Билли Новик</div>
                            <div class="p-content-manager-role">музыкант, лидер группы Billy’s Band</div>
                            <div class="p-content-manager-quote">
                                Краудфандинг&nbsp;&mdash; это наша реакция в&nbsp;ответ на&nbsp;изменения окружающей
                                среды. Мы&nbsp;открыты для экспериментов, если они не&nbsp;противоречат нашим моральным
                                нормам.
                            </div>
                        </div>

                        <br><br>

                        <div class="post-milestones large-milestones">
                            <div class="post-milestones-list clearfix">
                                <div class="p-milestones-list-title minionpro-mediumit">Вехи проекта</div>
                                <div class="p-milestones-list-items">

                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">19 сентября 2012</div>
                                        <div class="pml-items-item-text proxima-reg">
                                            Проект записи альбома &laquo;Когда был один&raquo; открыт на&nbsp;&laquo;Планете&raquo;.
                                        </div>
                                    </div>

                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">20 марта 2013</div>
                                        <div class="pml-items-item-text proxima-reg">
                                            Музыканты еще до&nbsp;официального релиза альбома разослали сингл &laquo;Абажур&raquo;
                                            акционерам проекта, которые также эксклюзивно получили 20&nbsp;марта 2013
                                            года новую песню &laquo;Stay in&nbsp;line&raquo;.
                                        </div>
                                    </div>

                                </div>
                                <div class="p-milestones-list-items">

                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">9 февраля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">
                                            Была организована <nobr>онлайн-трансляция</nobr> концерта группы в&nbsp;Государственном театре киноактера.
                                        </div>
                                    </div>

                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">15 апреля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">
                                            Проект благополучно завершен с&nbsp;превышением заявленной суммы.
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                        <div class="post-content-text proxima-reg mrg-b-0">
                            <div class="p-content-notice helveticaneue-bold"> Придумывая бонусы для своего проекта, Billy’s&nbsp;Band решили расстаться с&nbsp;самым дорогим. Среди их&nbsp;акций были настоящие артефакты: рукописи с&nbsp;текстами песен и&nbsp;&laquo;визитная карточка&raquo; группы&nbsp;&mdash; шляпа Билли Новика. Ну а&nbsp;гвоздем проекта стала настоящая <nobr>фан-реликвия</nobr>&nbsp;&mdash; разбитый контрабас фронтмена группы.</div>
                            <br>

                            Лидер и&nbsp;основатель группы Billy’s&nbsp;Band Билли Новик&nbsp;&mdash; коротко, о&nbsp;главном:
                        </div>

                        <div class="h-video">
                            <iframe width="720" height="405" frameborder="0" allowfullscreen=""
                                    src="https://tv.planeta.ru/video-frame?profileId=22956&amp;videoId=21770&amp;autostart=false"></iframe>
                        </div>

                        <br><br>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/kurator/ava-vasilina.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Василина Горовая</div>
                            <div class="p-content-manager-role">куратор проекта</div>
                            <div class="p-content-manager-quote">
                                Работать с&nbsp;такими потрясающими артистами, как Billy’s&nbsp;Band, очень волнительно.
                                Каждый из&nbsp;музыкантов&nbsp;&mdash; отдельная вселенная, талант и&nbsp;личность. И&nbsp;просто
                                познакомиться с&nbsp;каждым из&nbsp;них&nbsp;&mdash; это уже большая удача. А&nbsp;теперь
                                представьте, что у&nbsp;вас есть счастье работать с&nbsp;этими удивительными
                                джентльменами в&nbsp;течение нескольких месяцев. Невероятное удовольствие! Спасибо
                                парням, их&nbsp;дирекции и&nbsp;особенно Билли!
                            </div>
                        </div>
                    </div>

                    <!--<div class="check-photo">
                        <img src="images/billys/check.jpg">
                    </div>-->

                    <!--<div class="post-content">
                        <a class="post-another-posts h-link minionpro-semiboldit" href="index.php">Другие успешные
                            проекты</a>
                    </div>-->
                </div>
            </div>
                <? include 'includes/index-data.php'; ?>
        </div>
    </div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
<div class="post-meta">
    <div class="post-meta-item collected">
        <div class="p-meta-title proxima-reg">Проект собрал</div>
        <div class="p-meta-num helveticaneue-bold<? if (strlen($collected) > 8) echo ' over-billion' ?>"><?=$collected?> <span class="b-rub">Р</span></div>
    </div>
    <? if ($target): ?>
    <div class="post-meta-item target">
        <div class="p-meta-title proxima-reg">Цель была</div>
        <div class="p-meta-num helveticaneue-bold<? if (strlen($target) > 8) echo ' over-billion' ?>"><?=$target?> <span class="b-rub">Р</span></div>
    </div>
    <? endif; ?>
    <div class="post-meta-item date">
        <div class="p-meta-title proxima-reg">
            Сроки проведения
            <div class="p-meta-date-duration helveticaneue-bold<? if (strlen($dateDuration) > 17) echo ' with-year' ?>"><?=$dateDuration?></div>
        </div>
        <div class="p-meta-date-item">
            <div class="pm-date-item-day"><?=$startDay?></div>
            <div class="pm-date-item-moth"><?=$startMonth?></div>
            <div class="pm-date-item-year"><?=$startYear?></div>
        </div>
        <div class="p-meta-date-item">
            <div class="pm-date-item-day"><?=$endDay?></div>
            <div class="pm-date-item-moth"><?=$endMonth?></div>
            <div class="pm-date-item-year"><?=$endYear?></div>
        </div>
    </div>
    <div class="post-meta-item members">
        <div class="p-meta-title proxima-reg">Приняли участие</div>

        <?
        $modulo = $members % 10;
        switch ($modulo) {
            case 1:
                $backers = 'акционер';
                break;
            case 2:
            case 3:
            case 4:
                $backers = 'акционера';
                break;
            default:
                $backers = 'акционеров';
        }
        ?>

        <div class="p-meta-num helveticaneue-bold"><?=$members?></div>
        <div class="p-meta-num-spn proxima-reg"> <?=$backers?></div>
    </div>
    <div class="post-meta-item pm-page-link">
        <div class="p-meta-title proxima-reg"><a href="<?=$projectLink?>">Страница
                проекта</a></div>
    </div>
</div>
<div class="post-meta-blog">
    <a class="post-meta-blog-link minionpro-semiboldit" href="<?=$blog_link?>" target="_blank"><?=$blog_link_label?></a>
</div>
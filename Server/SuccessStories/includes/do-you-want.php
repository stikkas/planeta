<div class="do-you-want">
    <div class="wrap">
        <div class="do-you-want-title helveticaneue-bold">Кажется, здесь не&nbsp;хватает вашего имени?</div>
        <div class="do-you-want-sub-title minionpro-mediumit">Впишите его в&nbsp;историю&nbsp;&mdash; это действительно возможно!</div>
        <div class="do-you-want-text proxima-reg">
            Помните, что заниматься любимым делом можно не&nbsp;только в&nbsp;одиночку. Чем ближе идея вашим
            единомышленникам, тем ближе и&nbsp;сам результат. <span class="do-you-want-text-spn proxima-bold">И&nbsp;насколько ярко ваш замысел воплотится в&nbsp;жизнь,
            зависит именно от&nbsp;вас!</span>
        </div>
        <a class="flat-btn" href="https://planeta.ru/funding-rules">Создать проект на Планете</a>
    </div>
</div>
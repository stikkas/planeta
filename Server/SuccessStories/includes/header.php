<?
date_default_timezone_set('Europe/Moscow');

define('ROOT_DIR', $_SERVER["DOCUMENT_ROOT"]);
include("./tmpl/autoload.php");

$page = rtrim($_SERVER['SCRIPT_NAME'],'/');
$page = explode('/', $page);
$page = array_pop($page);
$page = preg_replace('/\.php/', '', $page);
$curr_domain = $_SERVER['SERVER_NAME'];
$curr_tag_managed_id = 'GTM-N2JH8F';
switch ($curr_domain) {
    case "dev.planeta.ru":
        $curr_tag_managed_id = 'GTM-N2JH8F';
        break;
    case "test.planeta.ru":
        $curr_tag_managed_id = 'GTM-5GWQ7V';
        break;
    case "planeta.ru":
        $curr_tag_managed_id = 'GTM-T9PKNN';
        break;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">

        <meta name="viewport" content="width=1000, initial-scale=1.0">

        <title>
        <?
        if ($title) {
            echo('Истории успеха: ' . $title . ' | planeta.ru');
        } else {
            echo('Истории успеха');
        }
        ?>
    </title>

    <link rel="icon" type="image/ico" href="https://static.planeta.ru/images/favicons/favicon.ico" />

        <!-- bootstrap css -->
        <link href="css/history.css" media="only screen" type="text/css" rel="stylesheet">

        <script type="text/javascript" src="https://static.planeta.ru/js/lib/jquery-1.8.3.min.js"></script>
        <script type="text/javascript" src="https://static.planeta.ru/js/lib/underscore-1.4.4.min.js"></script>
        <script type="text/javascript" src="https://static.planeta.ru/js/utils/plugins/share-widget.js"></script>
        <script type="text/javascript" src="js/jquery.lazyload.min.js"></script>

        <script>

            $(function(){
                $('.dlink').click(function(e){
                    e.preventDefault();
                    var dlinkID = $(e.currentTarget).attr('id');
                    var patt=/dlink-(\d+)/i;
                    dlinkID = 'dcontent-'+dlinkID.replace(patt, "$1");
                    $(e.currentTarget).hide();
                    var $obj = $('#'+dlinkID);
                    $obj.attr('src',$obj.attr('data-href')).parent('.p-msb-iframe').show();
                })
        });

        $(function() {
            $('a').each(function() {
                var $this = $(this);
                var phpUrl = this.href;
                if(phpUrl.match(/.*index.php/)) {
                    $this.attr({href: phpUrl.substr(0, phpUrl.length - 9)});
                } else if(phpUrl.match(/.*\.php/)) {
                    $this.attr({href: phpUrl.substr(0, phpUrl.length - 4)});
                }
            });
        });

        </script>

    </head>
    <body>
        <!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=<?php echo $curr_tag_managed_id ?>" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>
            dataLayer = [];
            (function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start': new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src = '//www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', '<?php echo $curr_tag_managed_id ?>');
            dataLayer.push({
                event: 'pageView',
                ecommerce: {}
            });
        </script>
        <!-- End Google Tag Manager -->
        <div class="header-wrap">
            <div class="wrap">
                <header class="header">
                    <a class="header-logo" href="https://planeta.ru/"></a>

                    <? if ($page == 'index') : ?>
                        <div class="header-title helveticaneue-bold">Истории успеха</div>
                    <? else : ?>
                        <div class="header-title helveticaneue-bold"><a href="https://planeta.ru/stories/">Истории успеха</a></div>
                    <? endif; ?>
                </header>
            </div>
        </div>

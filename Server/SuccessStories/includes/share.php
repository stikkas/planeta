<div class="post-share"></div>
<script type="text/javascript">
    $(function() {
        var getDataForShare = {
            className: 'vertical',
            counterEnabled: false,
            hidden: false
        };
        $('.post-share').share(getDataForShare);
    });
</script>

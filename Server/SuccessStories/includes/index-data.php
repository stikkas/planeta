<?
/* Add new items to end of the list */
$projectsSmall = array(
    array(
        'link' => '5diez.php',
        'cover' => 'images/content/5diez.jpg',
        'name' => '#####',
        'price' => '158 000',
        'text' => '##### (5diez) попробовали не&nbsp;только альтернативную музыку, но&nbsp;и альтернативу шоу-бизнесу&nbsp;&mdash; и&nbsp;выпустили альбом «MMXIII» вместе с «Планетой»!',
    ),
    array(
        'link' => 'billysband.php',
        'cover' => 'images/content/billis-band.jpg',
        'coverShift' => '-11px',
        'name' => 'Billy\'s Band',
        'price' => '269 000',
        'text' => 'Пластинку «Когда был один» Billy\'s&nbsp;Band делали совсем не&nbsp;в&nbsp;одиночку&nbsp;&mdash; и с помощью пользователей «Планеты» альбом получился на славу!',
    ),
    array(
        'link' => 'jukeboxtrio.php',
        'cover' => 'images/content/jukebox.jpg',
        'name' => 'Jukebox Trio',
        'price' => '91 000',
        'text' => 'Для ребят из Jukebox Trio &laquo;Планета&raquo; стала почти второй родиной&nbsp;&mdash; поэтому и&nbsp;альбом &laquo;Rodina, часть&nbsp;2&raquo; появился именно здесь!',
    ),
    array(
        'link' => 'grishkovets1.php',
        'cover' => 'images/content/grishkovec-plus1.jpg',
        'name' => 'Евгений Гришковец',
        'price' => '925 000',
        'text' => 'Создавая видеоверсию спектакля, Евгений Гришковец получил больше, чем &laquo+1&raquo,&nbsp;&mdash; он получил в&nbsp;плюс сразу всю &laquo;Планету&raquo;!',
    ),
    array(
        'link' => 'colta1.php',
        'cover' => 'images/content/colta.jpg',
        'name' => 'Colta.ru',
        'price' => '695 000',
        'text' => 'Благодаря пользователям &laquo;Планеты&raquo; проект Colta еще долго не&nbsp;будет требовать перезарядки&nbsp;&mdash; а&nbsp;главное, что только с&nbsp;ними он&nbsp;стал возможен!',
    ),
    array(
        'link' => 'opus.php',
        'cover' => 'images/opus/cover.jpg',
        'name' => 'Дмитрий Ревякин',
        'price' => '406 000',
        'text' => 'Проект &laquo;Grandi Canzoni, Opus 1&raquo; стал для Дмитрия Ревякина настолько успешным, что мы&nbsp;уверены&nbsp;&mdash; можно ожидать в&nbsp;будущем &laquo;Opus 2&raquo;!',
    ),
    array(
        'link' => 'ilfipetrov.php',
        'cover' => 'images/content/ilfpetrov.jpg',
        'name' => 'ИЛЬФИПЕТРОВ',
        'price' => '96 000',
        'text' => 'Неизвестно, что сказали&nbsp;бы о&nbsp;&laquo;Планете&raquo; Ильф и&nbsp;Петров, но&nbsp;благодаря ей&nbsp;Роман Либеров сам смог сказать многое о&nbsp;культовых отечественных <nobr>писателях-сатириках</nobr>.',
    ),
    array(
        'link' => 'undervud.php',
        'cover' => 'images/content/underwood.jpg',
        'name' => 'Ундервуд',
        'price' => '354 000',
        'text' => 'К&nbsp;&laquo;Женщинам и&nbsp;детям&raquo; группы &laquo;Ундервуд&raquo; присоединились и&nbsp;остальные пользователи &laquo;Планеты&raquo;&nbsp;&mdash; и&nbsp;пластинка успешно увидела свет!',
    ),
    array(
        'link' => 'pilot.php',
        'cover' => 'images/content/pilot.jpg',
        'name' => 'Пилот',
        'price' => '609 000',
        'text' => '&laquo;Пилотный&raquo; проект группы &laquo;Пилот&raquo; прошел под грамотным управлением менеджеров &laquo;Планеты&raquo;&nbsp;&mdash; и&nbsp;группа совершила мягкую посадку в&nbsp;студии для записи альбома.',
    ),
    array(
        'link' => 'stim.php',
        'cover' => 'images/content/stim.jpg',
        'name' => 'St1m',
        'price' => '105 000',
        'text' => 'Записывая «Коридоры» на&nbsp;«Планете», St1m убедился – перед ним лежат не просто коридоры, а безграничные пути сотрудничества с поклонниками.',
    ),
    array(
        'link' => 'delo1937.php',
        'cover' => 'images/content/delo1937.jpg',
        'name' => 'Дело №1937',
        'price' => '154 000',
        'text' => '&laquo;Дело &#8470;&nbsp;1937&raquo; стало делом номер один для поклонников театра на&nbsp;&laquo;Планете&raquo;&nbsp;&mdash; и&nbsp;это дело завершилось успехом авторов и&nbsp;участников проекта!',
    ),
    array(
        'link' => 'bolshe-tepla.php',
        'cover' => 'images/content/blago1.jpg',
        'name' => 'Проект «Больше тепла»',
        'price' => '65 000',
        'text' => 'Отклик на проект «Больше тепла» был не просто теплым, а по-настоящему жарким: поделиться вещами, а главное, душевным теплом пожелали люди из разных уголков России. С их помощью волонтеры смогли устроить для бездомных оттепель зимой.',
    ),
    array(
        'link' => 'muzej.php',
        'cover' => 'images/content/nalichniki.jpg',
        'name' => 'Музей резных наличников',
        'price' => '118 000',
        'text' => 'Если &laquo;Планета&raquo;&nbsp;&mdash; это окно в&nbsp;мир краудфандинга, то&nbsp;проект Ивана Хафизова&nbsp;&mdash; это его украшение. Почему? Просто он&nbsp;посвящен оконным наличникам&nbsp;&mdash; традиционному декоративному элементу русской архитектуры!',
    ),
    array(
        'link' => 'animaljazz.php',
        'cover' => 'images/content/animal.jpg',
        'name' => 'Animal ДжаZ',
        'price' => '569 000',
        'text' => 'Группа &laquo;Animal ДжаZ&raquo; доказала, что главное в&nbsp;построении правильных отношений с&nbsp;поклонниками&nbsp;&mdash; это не&nbsp;спать и&nbsp;быть максимально активным. Даже если ты&nbsp;собираешь на&nbsp;&laquo;Фазу быстрого сна&raquo;! Именно благодаря этому проект записи нового альбома стал одним из&nbsp;самых энергично развивающихся на&nbsp;&laquo;Планете&raquo;.',
    ),
    array(
        'link' => 'spirit.php',
        'cover' => 'images/content/spirit.jpg',
        'name' => 'Альбом Би-2 &laquo;Spirit&raquo;',
        'price' => '1 260 000',
        'text' => 'Первым быть сложно, а&nbsp;проект альбома &laquo;Spirit&raquo; был именно первым&nbsp;&mdash; как для &laquo;Планеты&raquo;, так и&nbsp;для &laquo;Би-2&raquo;, которые решили попробовать новый способ взаимодействия с&nbsp;поклонниками.',
    ),
    array(
        'link' => 'lavkalavka.php',
        'cover' => 'images/content/lavkalavka.jpg',
        'coverShift' => '-23px',
        'name' => 'Магазин LavkaLavka',
        'price' => '692 000',
        'text' => 'Основатели LavkaLavka доказали, что когда дело касается продуктов, то&nbsp;ферма лучше, чем фирма. Именно поэтому с&nbsp;помощью пользователей &laquo;Планеты&raquo; к&nbsp;кафе LavkaLavka прибавился еще и&nbsp;магазин фермерской еды.',
    ),
    array(
        'link' => 'bardin.php',
        'cover' => 'images/content/bardin-thumb.png',
        'name' => 'Мультфильм Гарри Бардина &laquo;Три мелодии&raquo;',
        'price' => '2 249 000',
        'text' => 'Имя Гарри Бардина знакомо всем ценителям советской и&nbsp;российской мультипликации. Пользователи &laquo;Планеты&raquo; пополнили эту копилку талантливых работ еще одним произведением&nbsp;&mdash; завершающей частью трилогии &laquo;Три мелодии&raquo;.',
    ),
    array(
        'link' => 'tinavie.php',
        'cover' => 'images/content/tinavie.png',
        'name' => 'Новый альбом Tinavie &laquo;Комета&raquo;',
        'price' => '269 000',
        'text' => 'В&nbsp;декабре жители &laquo;Планеты&raquo; наблюдали за&nbsp;самой яркой кометой. &laquo;Виной&raquo; тому &laquo;Tinavie&raquo; и&nbsp;их&nbsp;одноименный альбом, выход которого поддержали поклонники группы.',
    ),
    array(
        'link' => 'lampasy.php',
        'cover' => 'images/content/lampasy.jpg',
        'name' => 'Лампасы',
        'price' => '104 000',
        'text' => 'Акционеры проекта &laquo;Лампасы&raquo; доказали, что разделяют &laquo;безграничную любовь группы к&nbsp;неудержимому веселью, отсутствию внутренних запретов и&nbsp;бесшабашному позитиву&raquo; и&nbsp;поддержали выход нового альбома.',
    ),
    array(
        'link' => 'masyanya.php',
        'cover' => 'images/content/masyanya.jpg',
        'name' => 'Новые серии &laquo;Масяни&raquo;',
        'price' => '337 000',
        'text' => '&laquo;И&nbsp;тыды!&raquo; по&nbsp;отношению к&nbsp;новым сериям Масяни оказались пророческими: вместо одной серии, удалось собрать на&nbsp;целых три! И&nbsp;это еще не&nbsp;предел. Как говорилось в&nbsp;одном известном мультфильме: &laquo;Шире надо думать!&raquo;',
    ),
    array(
        'link' => 'torba-na-kruche.php',
        'cover' => 'images/content/torba.jpg',
        'name' => 'Торба-на-круче',
        'price' => '750 000',
        'text' => '&laquo;Торба-на-круче&raquo; задумывала проект на&nbsp;Планете исключительно как эксперимент, и&nbsp;он&nbsp;не&nbsp;только удался, но&nbsp;и&nbsp;превзошел сам себя&nbsp;&mdash; буквально за&nbsp;неделю необходимая сумма была собрана.',
    ),
    array(
        'link' => 'neschastnyj-sluchaj.php',
        'cover' => 'images/content/slu4ay.jpg',
        'name' => 'Несчастный случай',
        'price' => '555 000',
        'text' => 'На&nbsp;тридцатилетие со&nbsp;дня своего существования группа &laquo;Несчастный случай&raquo; позвала&hellip; всю Планету. 274 акционера, с&nbsp;радостью отозвались на&nbsp;приглашение и&nbsp;поддержали юбилейный проект!',
    ),
    array(
        'link' => 'sansara.php',
        'cover' => 'images/content/sansara.jpg',
        'name' => '&laquo;Сансара&raquo;',
        'price' => '220 000',
        'text' => '&laquo;Сансара&raquo; решила &laquo;представить на&nbsp;суд&raquo; своим поклонникам лучшее. Или&nbsp;другими словами &laquo;The&nbsp;Best&raquo;. Работа именно над этим альбомом прошла при поддержке акционеров Планеты.',
    ),
    array(
        'link' => 'dashiny-pirozhki.php',
        'cover' => 'images/content/dashiny-pirozhki.jpg',
        'name' => 'Дашины пирожки',
        'price' => '200 000',
        'text' => 'Как &laquo;горячие пирожки&raquo; разошлись бонусы в&nbsp;проекте Даши Сонькиной. Что позволило ей&nbsp;обзавестись оборудованием для пекарни и&hellip; испечь еще больше пирожков. В&nbsp;выигрыше остались все!',
    ),
    array(
        'link' => 'velonoch.php',
        'cover' => 'images/content/velonoch.jpg',
        'name' => 'VII московская Велоночь',
        'price' => '130 000',
        'text' => 'Десять тысяч человек и&nbsp;много километров счастья: именно так можно описать VII московскую Велоночь. Акционеры Планеты, недолго думая, присоединились к&nbsp;стройным рядам велосипедистов.',
    ),
    array(
        'link' => 'sugar-lies.php',
        'cover' => 'images/content/sugar-lies.jpg',
        'name' => 'Петр Налич',
        'price' => '404 000',
        'text' => 'Петр Налич знает секрет успешного сингла: запускаешь проект на&nbsp;Планете, ждешь когда к&nbsp;тебе присоединятся 114 участников и&nbsp;записываешь новый хит. Имя которому &laquo;Sugar lies&raquo;.',
    ),
    array(
        'link' => 'code-morse.php',
        'cover' => 'images/content/code-morse.jpg',
        'name' => 'Пикник',
        'price' => '740 000',
        'text' => '&laquo;Пикник&raquo; собрал вокруг себя группу настоящих единомышленников, которые готовы поддержать любую идею коллектива. Сначала новый тур, теперь&nbsp;&mdash; запись нового альбома. Вся Планета с&nbsp;нетерпением ждет, что&nbsp;же будет дальше!',
    ),
    array(
        'link' => 'zhelannaya.php',
        'cover' => 'images/content/zhelannaya.jpg',
        'name' => 'Инна Желанная',
        'price' => '600 000',
        'text' => 'Вернувшись в&nbsp;студию после десятилетнего перерыва, Инна Желанная и&nbsp;ее&nbsp;музыканты решили записать новый альбом. Жители Планеты поддержали их&nbsp;в&nbsp;этом решении настолько активно, что альбом получился двойным. Вот такой вот &laquo;Изворот&raquo;!',
    ),
    array(
        'link' => 'tarakany.php',
        'cover' => 'images/pix-on-mainpage.jpg',
        'name' => 'Тараканы',
        'price' => '500 000',
        'text' => 'Если на «Планете» заводятся «Тараканы!» – это к максимальному счастью. По крайней мере, для пользователей, поддержавших выпуск альбома «Maximum Happy», было именно так. И результат не заставил себя ждать – счастье удалось на все 200%!',
    ),
    array(
        'link' => '7000.php',
        'cover' => 'images/content/7000.jpg',
        'name' => '7000$ – новый альбом',
        'price' => '148 000',
        'text' => 'С&nbsp;таким названием группа &laquo;7000$&raquo; могла рассчитывать только на&nbsp;оглушительный финансовый успех. Так и&nbsp;произошло: собрав необходимую сумму в&nbsp;первом проекте, ребята, не&nbsp;долго думая, запустили второй!',
    ),
    array(
        'link' => 'lyapis-crew.php',
        'cover' => 'images/content/lyapis-crew.jpg',
        'name' => 'Lyapis Crew: ТРУбьют',
        'price' => '403 000',
        'text' => 'Хоть группа &laquo;Ляпис Трубецкой&raquo; закончила свое существование в&nbsp;начале сентября этого года, сформировав две новые – &laquo;Trubetskoy&raquo; и&nbsp;&laquo;BRUTTO&raquo;, это не&nbsp;мешает нам вспомнить о&nbsp;проекте &laquo;ТРУбьют&raquo;, который стал достойным и&nbsp;ярким финалом всей истории.',
    ),
);

$projectsSmall = array_reverse($projectsSmall);

if ($page != 'index' && $page != 'main-list') {
    $countArray = count($projectsSmall) - 1;

    $tempArray = array();
    $finalArray = array();

    for ($i = 0; $i < 3; $i++) {
        do {
            $rand = rand(0, $countArray);
            $randLink = preg_replace('/\.php/', '', $projectsSmall[$rand]['link']);
        } while ($randLink == $page || in_array($rand, $tempArray));
        $tempArray[] = $rand;
        $finalArray[] = $projectsSmall[$rand];
    }
?>

    <div class="projects-small-block">
        <? foreach ($finalArray as $item) : ?>
            <div class="projects-small">
                <a class="projects-small-link" href="<?=$item['link']?>">
                <span class="projects-small-cover">
                    <img class="projects-small-img" src="<?=$item['cover']?>"<? if ( $item['coverShift'] ) echo ' style="top:'. $item['coverShift'] .'"' ?>>
                    <span class="projects-small-name minionpro-semiboldit <?=$item['class']?>"><?=$item['name']?></span>
                </span>
                    <span class="projects-small-price"><?=$item['price']?> <span class="b-rub">Р</span></span>
                <span class="projects-small-text proxima-reg"><?=$item['text']?></span>
                </a>
            </div>
        <? endforeach; ?>
    </div>

<? } ?>
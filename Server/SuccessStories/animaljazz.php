<?
$title = 'AnimalJazz';

$collected = 569055;
$target = 300000;

    $dateDuration = '4 месяца 3 дня';

    $startDay = 25;
    $startMonth = 'октября';
    $startYear = 2012;

$endDay = 5;
$endMonth = 'февраля';
$endYear = 2013;

$members = 348;

$projectLink = 'https://planeta.ru/campaigns/232';

$collected = number_format($collected, 0, '.', ' ');
$target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

    <div class="post" itemscope itemtype="http://schema.org/CreativeWork">
    <div class="wrap">
        <a class="post-back-link" href="index.php">Истории успеха</a>

        <div itemprop="name" class="post-title minionpro-boldit">Запись альбома группы &laquo;Animal ДжаZ&raquo; &laquo;Фаза быстрого сна&raquo;</div>
        <div class="post-main">

            <? require 'includes/post-meta.php'; ?>

            <img itemprop="image" class="post-big-img" src="images/animal-djaz/check.jpg">

            <div class="post-middle">
                <? require 'includes/share.php' ?>
                <div class="post-content">
                    <div itemprop="description" class="post-content-text proxima-reg">
                        &laquo;Animal ДжаZ&raquo;&nbsp;&mdash; петербургская группа, созданная в&nbsp;2000 году. Сами
                        музыканты называют свою стилистику &laquo;тяжёлой гитарной эклектикой&raquo;. Музыканты записали
                        7 электрических альбомов, 3 акустических, 9&nbsp;синглов и&nbsp;4 DVD. Именно на&nbsp;седьмой альбом
                        ребята собрали средства на&nbsp;&laquo;Планете&raquo;, причем группа перевыполнила план и&nbsp;собрала
                        почти в&nbsp;два раза больше денег, чем требовалось.

                        <div class="p-content-notice helveticaneue-bold">
                            Для нас это яркий пример того, что даже
                            небольшая, но&nbsp;активная группа поклонников может обеспечить успех любого творческого
                            проекта.
                        </div>
                    </div>
                    <div class="post-content-manager">
                        <div class="p-content-manager-ava">
                            <img src="images/animal-djaz/ava-artist.jpg">
                        </div>
                        <div class="p-content-manager-name minionpro-mediumit">Александр Красовицкий</div>
                        <div class="p-content-manager-role">музыкант, лидер группы «Animal Jazz»</div>
                        <div class="p-content-manager-quote">
                            Я&nbsp;теперь стал реально фанатом идеи краудфандинга, я&nbsp;почувствовал, что это работает. Это революция, она еще не&nbsp;прочувствована всеми, но, судя по&nbsp;тому, что из&nbsp;крутых групп этим занимается <nobr>&laquo;Би-2&raquo;</nobr>, потихоньку дойдет и&nbsp;до&nbsp;всех. Оказывается, люди готовы платить вперед, кто&nbsp;бы мог подумать! Для многих групп это станет спасением, ведь индустрия продаж дисков рухнула: CD&nbsp;не&nbsp;продаются. А&nbsp;тут, хочешь диск и&nbsp;<nobr>что-то</nobr> к&nbsp;нему&nbsp;&mdash; пожалуйста.

                            <br><br>

                            Вообще я&nbsp;не&nbsp;вижу ни&nbsp;одного плохого последствия краудфандинга. Кто&nbsp;бы
                            мог предположить, что с&nbsp;развитием Интернета лопнет
                            <nobr>лейбл-индустрия</nobr>, и&nbsp;люди просто начнут все качать и&nbsp;шерить. А&nbsp;так появился реальный
                            выход.
                            2013 год точно станет Годом краудфандинга.
                        </div>
                    </div>

                    <br><br>

                    <div class="post-milestones">
                        <div class="post-milestones-sidebar">

                            <div class="p-msb-item">
                                <div class="p-msb-video">
                                    <a href="#nogo" class="dlink" id="dlink-01"><img
                                            src="images/animal-djaz/milestone-01.jpg"></a>

                                    <div class="p-msb-iframe">
                                        <iframe id="dcontent-01" width="509" height="286" frameborder="0" allowfullscreen=""
                                                data-href="https://tv.planeta.ru/video-frame?profileId=28286&amp;videoId=8761&amp;autostart=true"></iframe>
                                    </div>
                                </div>
                            </div>

                            <div class="p-msb-item">
                                <a href="https://planeta.ru/animaljazz/blog/111090" target="_blank"><img src="https://s2.planeta.ru/i/27c55/medium.jpg"></a>
                            </div>

                        </div>
                        <div class="post-milestones-list">
                            <div class="p-milestones-list-title minionpro-mediumit">Вехи проекта</div>
                            <div class="p-milestones-list-items">

                                <div class="pm-list-items-item">
                                    <div class="pml-items-item-date proxima-bold">25 октября 2012</div>
                                    <div class="pml-items-item-text proxima-reg">
                                        Проект &laquo;Запись альбома группы &laquo;Animal ДжаZ&raquo; &laquo;Фаза быстрого сна&raquo; стартовал на&nbsp;&laquo;Планете&raquo;.
                                    </div>
                                </div>

                                <div class="pm-list-items-item">
                                    <div class="pml-items-item-date proxima-bold">9 ноября 2012</div>
                                    <div class="pml-items-item-text proxima-reg">
                                        &laquo;Animal ДжаZ&raquo; организовали среди пользователей <a href="https://planeta.ru/animaljazz/blog/110030" target="_blank">конкурс мемов</a> о&nbsp;коллективе. Победитель получил 2 билета на&nbsp;&laquo;деньрожденные&raquo; концерты в&nbsp;Москве и&nbsp;<nobr>Санкт-Петербурге</nobr>.
                                    </div>
                                </div>

                                <div class="pm-list-items-item">
                                    <div class="pml-items-item-date proxima-bold">8 декабря 2012</div>
                                    <div class="pml-items-item-text proxima-reg">
                                        Группа запустила <a href="https://planeta.ru/animaljazz/blog/111090" target="_blank">конкурс логотипов</a>. <nobr>Лого-победитель</nobr> был нанесен на&nbsp;<nobr>бас-бочку</nobr> и&nbsp;отправился вместе с&nbsp;группой в&nbsp;концертный тур.
                                    </div>
                                </div>

                                <div class="pm-list-items-item">
                                    <div class="pml-items-item-date proxima-bold">15 декабря 2012</div>
                                    <div class="pml-items-item-text proxima-reg">
                                        В&nbsp;прямом эфире &laquo;Планеты&raquo; прошла онлайн трансляция &laquo;деньрожденного&raquo; концерта &laquo;Animal ДжаZ&raquo;.
                                    </div>
                                </div>

                                <div class="pm-list-items-item">
                                    <div class="pml-items-item-date proxima-bold">5 февраля 2013</div>
                                    <div class="pml-items-item-text proxima-reg">
                                        Проект успешно завершен. Собранные на&nbsp;&laquo;Планете&raquo; средства почти в&nbsp;полтора раза превысили заявленную сумму!
                                    </div>
                                </div>

                                <div class="pm-list-items-item">
                                    <div class="pml-items-item-date proxima-bold">23 марта и 25 мая 2013</div>
                                    <div class="pml-items-item-text proxima-reg">
                                        Музыканты разыграли билеты на&nbsp;презентацию электрического альбома &laquo;Фаза быстрого сна&raquo;.
                                    </div>
                                </div>

                                <div class="pm-list-items-item">
                                    <div class="pml-items-item-date proxima-bold">29 марта 2013</div>
                                    <div class="pml-items-item-text proxima-reg">
                                        Специально для акционеров проекта &laquo;Animal ДжаZ&raquo; устроили закрытое предпрослушивание новой пластинки &laquo;Фаза быстрого сна&raquo;.
                                    </div>
                                </div>

                                <div class="pm-list-items-item">
                                    <div class="pml-items-item-date proxima-bold">30 марта 2013</div>
                                    <div class="pml-items-item-text proxima-reg">
                                        Пользователи &laquo;Планеты&raquo; увидели онлайн-трансляцию <nobr>концерта-презентации</nobr> новой пластинки.
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="post-content-text proxima-reg mrg-b-0">
                        <div class="p-content-notice helveticaneue-bold">
                            В&nbsp;число самых интересных бонусов проекта вошла целая
                            <nobr>ANIMAL-ФАН-ДжаZ</nobr> коллекция. В&nbsp;этот набор вошли не&nbsp;только номерной виниловый диск c&nbsp;новым
                            альбомом,
                            CD&nbsp;(диджибук, подарочное издание) и&nbsp;цифровая версия, но&nbsp;и&nbsp;постер с&nbsp;автографами
                            артистов, футболка, браслет, медиатор и, наконец, самый главный приз&nbsp;&mdash; именной
                            бейдж,
                            дающий право на&nbsp;бесплатное посещение любых сольных концертов группы с&nbsp;апреля 2013
                            по&nbsp;декабрь
                            2013.
                        </div>

                        <br>

                        Лидер группы &laquo;Animal Jazz&raquo; о&nbsp;&laquo;горящих&raquo; людях &laquo;Планеты&raquo;:
                    </div>

                    <div class="h-video">
                        <iframe width="720" height="405" frameborder="0" allowfullscreen=""
                                src="https://tv.planeta.ru/video-frame?profileId=22956&amp;videoId=21773&amp;autostart=false"></iframe>
                    </div>

                    <br><br>



                    <div class="post-content-manager">
                        <div class="p-content-manager-ava">
                            <img src="images/kurator/ava-german.jpg">
                        </div>
                        <div class="p-content-manager-name minionpro-mediumit">Ольга Герман</div>
                        <div class="p-content-manager-role">куратор проекта</div>
                        <div class="p-content-manager-quote">
                            Пожалуй, для &laquo;Планеты&raquo; в&nbsp;целом этот проект&nbsp;&mdash; один из&nbsp;самых значимых и&nbsp;показательных.
                            Общаясь с&nbsp;другими музыкантами, постоянно привожу группу &laquo;Animal ДжаZ&raquo; в&nbsp;качестве
                            примера не&nbsp;только успешного проекта, собравшего необходимую для записи альбома
                            сумму
                            денег, но&nbsp;и&nbsp;как образец выстраивания и&nbsp;поддержания грамотных
                            взаимоотношений
                            с&nbsp;поклонниками. Ребята постоянно придумывают новые конкурсы и&nbsp;интерактивы,
                            выкладывают закулисные и&nbsp;концертные фото- и&nbsp;видеоматериалы, а&nbsp;также не&nbsp;забывают
                            давать полезные советы и&nbsp;рекомендации непосредственно ресурсу! С&nbsp;большим
                            интересом
                            наблюдаю за&nbsp;жизнью этой группы на&nbsp;&laquo;Планете&raquo; и&nbsp;всем советую подключиться к&nbsp;<a href="https://planeta.ru/animaljazz" target="_blank">https://planeta.ru/animaljazz</a>!
                        </div>
                    </div>
                </div>





                <!--<div class="post-content">
                    <a class="post-another-posts h-link minionpro-semiboldit" href="index.php">Другие успешные
                        проекты</a>
                </div>-->

            </div>

        </div>
                <?include 'includes/index-data.php';?>
    </div>
    </div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
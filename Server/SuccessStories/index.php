<? require 'includes/header.php'; ?>


<?
$totalSumJson = file_get_contents('https://planeta.ru/api/util/campaign/total-purchase-sum.json');
$totalSum = intval(json_decode($totalSumJson));
?>

<div class="main-total">
    <div class="main-total_head">Наши авторы уже собрали</div>
    <div class="main-total_val">
        <?= number_format($totalSum, 0, '.', ' ') ?> <span class="main-total_rub"></span>
    </div>
</div>



<? include_once "tmpl/projectCard.php" ?>
<? $projectCard = new projectCard; ?>

<div class="main-categories">
    <div class="main-categories_i">
        <div class="main-categories_link main-categories_link__million">
            <div class="main-categories_link-cont">
                <div class="main-categories_head">Как собрать больше миллиона?</div>
                <div class="main-categories_text">
                    Проекты-рекордсмены, главные блокбастеры Планеты,
                    <br>
                    громкие истории и яркие герои
                </div>
            </div>
        </div>

        <div class="main-categories_cont">

            <div class="wrap">
                <?
                $projectCard->draw(array(
                    'items' => array(
                        array(
                            "id" => 995,
                            "link" => "bardin",
                        ),
                        array(
                            "id" => 19,
                            "link" => "spirit",
                        ),
                        array( "id" => 9739 ),
                        array( "id" => 10100 ),
                        array( "id" => 2253 ),
                        array( "id" => 1250 ),
                        array( "id" => 4707 ),
                        array( "id" => 2849 ),
                        array( "id" => 22152 ),
                        array( "id" => 7442 ),
                        array( "id" => 8423 ),
                        array( "id" => 7748 ),
                        array( "id" => 14929 ),
                        array( "id" => 6962 ),
                        array( "id" => 10960 ),
                        array( "id" => 9897 ),
                        array( "id" => 922 ),
                        array( "id" => 15200 ),
                        array( "id" => 167 ),
                        array( "id" => 6511 ),
                        array( "id" => 11991 ),
                        array( "id" => 4631 ),
                        array( "id" => 1566 ),
                        array( "id" => 5919 ),
                    )
                ));
                ?>
                <!--<div class="list-preloader"></div>-->
            </div>

        </div>
    </div>


    <div class="main-categories_i">
        <div class="main-categories_link main-categories_link__creative">
            <div class="main-categories_link-cont">
                <div class="main-categories_head">Как реализовать свою творческую идею?</div>
                <div class="main-categories_text">
                    Частные творческие инициативы, организация мероприятий,
                    <br>
                    запуск собственного авторского проекта
                </div>
            </div>
        </div>

        <div class="main-categories_cont">

            <div class="wrap">
                <?
                $projectCard->draw(array(
                    'items' => array(
                        /*array(
                            "id" => 1179,
                            "link" => "velonoch",
                        ),*/
                        array(
                            "id" => 1547,
                            "link" => "dashiny-pirozhki",
                        ),
                        array(
                            "id" => 1231,
                            "link" => "lavkalavka",
                        ),
                        /*array(
                            "id" => 263,
                            "link" => "muzej",
                        ),*/
                        /*array(
                            "id" => 33,
                            "link" => "delo1937",
                        ),*/
                        array(
                            "id" => 106,
                            "link" => "colta1",
                        ),
                        array( "id" => 9708 ),
                        array( "id" => 6305 ),
                        array( "id" => 10586 ),
                        array( "id" => 17059 ),
                        array( "id" => 14266 ),
                        array( "id" => 7768 ),
                        array( "id" => 3473 ),
                        array( "id" => 15736 ),
                        array( "id" => 17758 ),
                        array( "id" => 19094 ),
                        array( "id" => 10419 ),
                        array( "id" => 11052 ),
                        array( "id" => 7526 ),
                        array( "id" => 3557 ),
                        array( "id" => 11893 ),
                        array( "id" => 4673 ),
                        array( "id" => 7976 ),
                    )
                ));
                ?>
            </div>

        </div>
    </div>


    <div class="main-categories_i">
        <div class="main-categories_link main-categories_link__film">
            <div class="main-categories_link-cont">
                <div class="main-categories_head">Как снять фильм?</div>
                <div class="main-categories_text">
                    Анимация, видео-клипы, короткометражки и полный метр,
                    <br>
                    документальное кино, цикл передач и записи концертов
                </div>
            </div>
        </div>

        <div class="main-categories_cont">

            <div class="wrap">
                <?
                $projectCard->draw(array(
                    'items' => array(
                        array(
                            "id" => 178,
                            "link" => "masyanya",
                        ),
                        /*array(
                            "id" => 92,
                            "link" => "ilfipetrov",
                        ),*/
                        array(
                            "id" => 107,
                            "link" => "grishkovets1",
                        ),
                        array( "id" => 15529 ),
                        array( "id" => 5441 ),
                        array( "id" => 10143 ),
                        array( "id" => 10280 ),
                        array( "id" => 9311 ),
                        array( "id" => 5942 ),
                        array( "id" => 6442 ),
                        array( "id" => 7471 ),
                        array( "id" => 10224 ),
                        array( "id" => 11780 ),
                        array( "id" => 5816 ),
                        array( "id" => 7880 ),
                        array( "id" => 17204 ),
                        array( "id" => 17655 ),
                        array( "id" => 8303 ),
                        array( "id" => 8270 ),
                        array( "id" => 13736 ),
                        array( "id" => 5252 ),
                        array( "id" => 12989 ),
                        array( "id" => 12784 ),
                        array( "id" => 5839 ),
                        array( "id" => 1491 ),
                    )
                ));
                ?>
            </div>

        </div>
    </div>


    <div class="main-categories_i">
        <div class="main-categories_link main-categories_link__charity">
            <div class="main-categories_link-cont">
                <div class="main-categories_head">Как собрать деньги на доброе дело?</div>
                <div class="main-categories_text">
                    Благотворительные и социально значимые проекты,
                    <br>
                    помощь нуждающимся и оказавшимся в трудной жизненной ситуации
                </div>
            </div>
        </div>

        <div class="main-categories_cont">

            <div class="wrap">
                <?
                $projectCard->draw(array(
                    'items' => array(
                        /*array(
                            "id" => 372,
                            "link" => "bolshe-tepla",
                        ),*/
                        array( "id" => 7221 ),
                        array( "id" => 7621 ),
                        array( "id" => 14343 ),
                        array( "id" => 20476 ),
                        array( "id" => 3519 ),
                        array( "id" => 2758 ),
                        array( "id" => 824 ),
                        array( "id" => 782 ),
                        array( "id" => 11092 ),
                        array( "id" => 2567 ),
                        array( "id" => 4210 ),
                        array( "id" => 3738 ),
                        array( "id" => 7650 ),
                        array( "id" => 10542 ),
                        array( "id" => 13150 ),
                        array( "id" => 11663 ),
                    )
                ));
                ?>
            </div>

        </div>
    </div>


    <div class="main-categories_i">
        <div class="main-categories_link main-categories_link__music">
            <div class="main-categories_link-cont">
                <div class="main-categories_head">Как записать альбом?</div>
                <div class="main-categories_text">
                    Сведение и выпуск альбомов, синглов, запись концертов,
                    <br>
                    организация музыкальных туров
                </div>
            </div>
        </div>

        <div class="main-categories_cont">

            <div class="wrap">
                <?
                $projectCard->draw(array(
                    'items' => array(
                        array(
                            "id" => 1431,
                            "link" => "lyapis-crew",
                        ),
                        /*array(
                            "id" => 425,
                            "link" => "7000",
                        ),*/
                        array(
                            "id" => 252,
                            "link" => "tarakany",
                        ),
                        array(
                            "id" => 2565,
                            "link" => "zhelannaya",
                        ),
                        array(
                            "id" => 4947,
                            "link" => "sugar-lies",
                        ),
                        array(
                            "id" => 1118,
                            "link" => "code-morse",
                        ),
                        /*array(
                            "id" => 1769,
                            "link" => "sansara",
                        ),*/
                        array(
                            "id" => 272,
                            "link" => "neschastnyj-sluchaj",
                        ),
                        array(
                            "id" => 1484,
                            "link" => "torba-na-kruche",
                        ),
                        /*array(
                            "id" => 1101,
                            "link" => "lampasy",
                        ),*/
                        /*array(
                            "id" => 1966,
                            "link" => "tinavie",
                        ),*/
                        array(
                            "id" => 232,
                            "link" => "animaljazz",
                        ),
                        /*array(
                            "id" => 436,
                            "link" => "stim",
                        ),*/
                        array(
                            "id" => 226,
                            "link" => "pilot",
                        ),
                        array(
                            "id" => 304,
                            "link" => "undervud",
                        ),
                        array(
                            "id" => 241,
                            "link" => "opus",
                        ),
                        /*array(
                            "id" => 26,
                            "link" => "jukeboxtrio",
                        ),
                        array(
                            "id" => 169,
                            "link" => "billysband",
                        ),
                        array(
                            "id" => 294,
                            "link" => "5diez",
                        ),*/
                        array( "id" => 18128 ),
                        array( "id" => 2836 ),
                        array( "id" => 650 ),
                        array( "id" => 2022 ),
                        array( "id" => 21986 ),
                        array( "id" => 496 ),
                        array( "id" => 2662 ),
                        array( "id" => 18211 ),
                        array( "id" => 7254 ),
                        array( "id" => 4997 ),
                        array( "id" => 3065 ),
                        array( "id" => 6389 ),
                        array( "id" => 9602 ),
                    )
                ));
                ?>
            </div>

        </div>
    </div>
</div>

<script>
    $('.main-categories_link').click(function () {
        var link = $(this);
        if ( link.hasClass('active') ) {
            link.next().slideUp(300, function () {
                link.removeClass('active');
            });
        } else {
            var prevActive = $('.main-categories_link.active');

            if ( prevActive.length ) {
                var contHeight = prevActive.next().height();

                prevActive.next().slideUp(300, function () {
                    prevActive.removeClass('active');
                });

                if ( prevActive.offset().top < link.offset().top ) {
                    $("html, body").animate({scrollTop: link.offset().top - contHeight}, 300);
                }
            }

            link.addClass('active');
            link.next().slideDown(300);

            link.next().find("img.lazy").trigger('lazy');
        }
    });

    $(function() {
        $("img.lazy").lazyload({
            skip_invisible : true,
            event : "lazy"
        });
    });
</script>


<div class="main-lead">
    <div class="main-lead_head">
        Вдохновились историями успеха?
    </div>
    <div class="main-lead_text">
        Помните, что заниматься любимым делом можно не&nbsp;только в&nbsp;одиночку. Чем ближе идея вашим единомышленникам, тем ближе и&nbsp;сам результат. И&nbsp;насколько ярко ваш замысел воплотится в&nbsp;жизнь, зависит именно от&nbsp;вас!
    </div>
    <div class="main-lead_action">
        <a class="flat-btn" href="https://planeta.ru/funding-rules">Создать проект на Планете</a>
    </div>
</div>

<div class="main-footer">
    &copy;<?= date("Y") ?> Planeta.ru
</div>


</body>
</html>

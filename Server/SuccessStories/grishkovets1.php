<?
    $title = 'Видеоверсия спектакля Евгения Гришковца &laquo;+1&raquo;: история успеха на planeta.ru';

    $collected = 925650;
    $target = 900000;

    $dateDuration = '8 месяцев 28 дней';

    $startDay = 20;
    $startMonth = 'августа';
    $startYear = 2012;
    $dateDuration = '8 месяцев 28 дней';

    $startDay = 20;
    $startMonth = 'августа';
    $startYear = 2012;

    $endDay = 15;
    $endMonth = 'мая';
    $endYear = 2013;

    $members = 966;

    $projectLink = 'https://planeta.ru/campaigns/107';

    $collected = number_format($collected, 0, '.', ' ');
    $target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

<div class="post" itemscope itemtype="http://schema.org/CreativeWork">
    <div class="wrap">
        <a class="post-back-link" href="index.php">Истории успеха</a>
        <div itemprop="name" class="post-title minionpro-boldit">Создание видеоверсии спектакля Евгения Гришковца &laquo;+1&raquo;</div>
        <div class="post-main">

        <? require 'includes/post-meta.php'; ?>

            <img itemprop="image" class="post-big-img" src="images/grishkovec1/check.jpg">
            <div class="post-middle">
                <? require 'includes/share.php' ?>
                <div class="post-content">

                    <div itemprop="description" class="post-content-text proxima-reg">
                        Евгений Гришковец&nbsp;&mdash; культовый писатель, актер, музыкант и&nbsp;режиссер или проще&nbsp;&mdash;
                        человек, который заново научил театр искренности,&nbsp;&mdash; пришел на&nbsp;&laquo;Планету&raquo;
                        в&nbsp;2012&nbsp;г. Создание видеоверсии спектакля &laquo;+1&raquo; стало одним из&nbsp;наших
                        первых театральных проектов.
                    </div>

                    <div class="post-content-text proxima-reg">
                        <div class="p-content-notice helveticaneue-bold">
                            Создавая видеоверсию спектакля, Евгений Гришковец получил больше, чем &laquo;+1&raquo;,&nbsp;&mdash; он&nbsp;получил в&nbsp;плюс сразу всю &laquo;Планету&raquo;!
                        </div>
                    </div>

                    <div class="post-content-manager">
                        <div class="p-content-manager-ava">
                            <img src="images/grishkovec1/ava-artist.jpg">
                        </div>
                        <div class="p-content-manager-name minionpro-mediumit">Ирина Юткина</div>
                        <div class="p-content-manager-role">продюсер</div>
                        <div class="p-content-manager-quote">
                            Честно говоря, я&nbsp;каждый день заглядываю на&nbsp;страницу Planeta.ru, и&nbsp;для меня
                            это такая азартная штука. Потому что это воспринимается как чудо. То&nbsp;есть, люди не&nbsp;только
                            купили билеты на&nbsp;съемку видеоверсии&nbsp;&mdash; ведь она состоялась 8 декабря 2012&nbsp;г.&nbsp;&mdash; каждый
                            день они тратят свои деньги на&nbsp;то, чтобы поддержать создание видеоверсии и&nbsp;купить
                            DVD с&nbsp;автографом или электронный вариант спектакля. Учитывая развитие пиратства и&nbsp;торрентов,
                            я&nbsp;реально воспринимаю это как чудо и&nbsp;очень вам благодарна.
                        </div>
                    </div>

                    <br>

                    <div class="post-content-text proxima-reg">
                        Продюсер Е.Гришковца Ирина Юткина о&nbsp;самой &laquo;неразгаданной&raquo; тайне &laquo;Планеты&raquo;:

                        <br><br>

                        <div class="h-video">
                            <iframe width="720" height="405" frameborder="0" allowfullscreen="" src="https://tv.planeta.ru/video-frame?profileId=22956&amp;videoId=21766&amp;autostart=false"></iframe>
                        </div>
                    </div>


                    <div class="post-milestones">
                        <div class="post-milestones-sidebar">
                            <div class="p-msb-item">
                                <img src="images/grishkovec1/milestone-01.jpg">
                            </div>
                        </div>

                        <div class="post-milestones-list">
                            <div class="p-milestones-list-title minionpro-mediumit">Вехи проекта</div>
                            <div class="p-milestones-list-items">

                                <div class="pm-list-items-item">
                                    <div class="pml-items-item-date proxima-bold">20 августа 2012</div>
                                    <div class="pml-items-item-text proxima-reg">
                                        Проект по&nbsp;созданию видеоверсии спектакля Е.Гришковца &laquo;+1&raquo; стартовал на&nbsp;&laquo;Планете&raquo;.
                                    </div>
                                </div>

                                <div class="pm-list-items-item">
                                    <div class="pml-items-item-date proxima-bold">8 декабря 2012</div>
                                    <div class="pml-items-item-text proxima-reg">
                                        В&nbsp;Центре имени Мейерхольда состоялась съемка спектакля &laquo;+1&raquo;, на&nbsp;которую были приглашены обладатели соответствующих акций.
                                    </div>
                                </div>

                                <div class="pm-list-items-item">
                                    <div class="pml-items-item-date proxima-bold">25 марта 2013</div>
                                    <div class="pml-items-item-text proxima-reg">
                                        Акционерам &laquo;Планеты&raquo; отправилась готовая версия DVD с&nbsp;записью спектакля.
                                    </div>
                                </div>

                                <div class="pm-list-items-item">
                                    <div class="pml-items-item-date proxima-bold">15 мая 2013</div>
                                    <div class="pml-items-item-text proxima-reg">
                                        Проект благополучно завершен, с&nbsp;превышением заявленной суммы.
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="post-content-text proxima-reg">
                        <div class="p-content-notice helveticaneue-bold">
                            Одним из&nbsp;бонусов проекта стал билет на&nbsp;выступление и&nbsp;тот самый сантиметр. Те, кто
                            уже успел посмотреть спектакль &laquo;+1&raquo;, знают, какую роль для человечества играет
                            швейный сантиметр, который Евгений Гришковец использует в&nbsp;своей постановке. Именно его
                            автор лично подарил акционеру, который приобрел соответствующую акцию.
                        </div>
                    </div>

                    <div class="post-content-manager">
                        <div class="p-content-manager-ava">
                            <img src="images/kurator/ava-vasilina.jpg">
                        </div>
                        <div class="p-content-manager-name minionpro-mediumit">Василина Горовая</div>
                        <div class="p-content-manager-role">куратор проекта</div>
                        <div class="p-content-manager-quote">
                            Бывают такие люди, узнавая которых, хочется в&nbsp;<nobr>чем-то</nobr> походить на&nbsp;них. Из&nbsp;общения с&nbsp;ними желаешь почерпнуть
                            <nobr>что-то</nobr> важное, большое, хорошее. Мне верится, что после каждой встречи с&nbsp;Ириной Юткиной
                            (продюсером проектов Евгения Гришковца) я&nbsp;становлюсь лучше. Ира&nbsp;&mdash;
                            удивительно интересный человек, настоящий профессионал и&nbsp;просто чудесная девушка. Я&nbsp;счастлива,
                            что мне представился шанс поработать с&nbsp;ней и&nbsp;ее&nbsp;командой. Если&nbsp;бы у&nbsp;меня
                            была возможность обнять и&nbsp;расцеловать Евгения Валерьевича лично, я&nbsp;бы это сделала.
                            Но&nbsp;скромность не&nbsp;позволит, поэтому просто низкий поклон ему за&nbsp;то, что
                            поверил в&nbsp;нас и&nbsp;поддержал на&nbsp;самом старте. Это для нас очень важно. И,
                            конечно, СПАСИБО всем тем, кто стал акционером проектов Евгения Гришковца на&nbsp;&laquo;Планете&raquo;.
                            <nobr>По-моему</nobr>, мы&nbsp;все вместе совершили маленькую революцию. Одну из&nbsp;самых благих революций в&nbsp;искусстве.
                        </div>
                    </div>

                    <br>

                    <div class="post-content-text proxima-reg">
                        Продюсер Е.Гришковца Ирина Юткина о&nbsp;том, что при всем многообразии выбора&nbsp;&mdash; &laquo;Планета&raquo; только одна.

                        <br><br>

                        <div class="h-video">
                            <iframe width="720" height="405" frameborder="0" allowfullscreen="" src="https://tv.planeta.ru/video-frame?profileId=22956&amp;videoId=21765&amp;autostart=false"></iframe>
                        </div>

                    </div>

                </div>



                <!--<div class="check-photo">
                    <img src="images/grishkovec1/check.jpg">
                </div>-->

                <!--<div class="post-content">
                    <a class="post-another-posts h-link minionpro-semiboldit" href="index.php">Другие успешные проекты</a>
                </div>-->

            </div>
        </div>
            <? include 'includes/index-data.php'; ?>

    </div>
</div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
<?

$title = 'Благотворительный проект «Больше тепла!»';

$collected = 65601;
$target = 30000;

$dateDuration = '29 дней';

$startDay = 31;
$startMonth = 'января';
$startYear = 2013;

$endDay = 28;
$endMonth = 'февраля';
$endYear = 2013;

$members = 73;

$projectLink = 'https://planeta.ru/campaigns/372';

$collected = number_format($collected, 0, '.', ' ');
$target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

    <div class="post" itemscope itemtype="http://schema.org/CreativeWork">
        <div class="wrap">
            <a class="post-back-link" href="index.php">Истории успеха</a>

            <div itemprop="name" class="post-title minionpro-boldit">Благотворительный проект «Больше тепла!»</div>
            <div class="post-main">

                <? require 'includes/post-meta.php'; ?>

                <img itemprop="image" class="post-big-img" src="images/bolshe-tepla-check.jpg">

                <div class="post-middle">
                    <? require 'includes/share.php' ?>
                    <div class="post-content">
                        <div itemprop="description" class="post-content-text proxima-reg">
                            В&nbsp;холодные зимние месяцы каждый из&nbsp;нас нуждается в&nbsp;человеческом тепле. Но&nbsp;особенно в&nbsp;нем нуждаются бездомные люди, оказавшиеся по&nbsp;разным причинам без крыши над головой. Ведь часто они замерзают не&nbsp;столько от&nbsp;холода, сколько от&nbsp;человеческого равнодушия. Поэтому движение &laquo;Друзья на&nbsp;улице&raquo;, благотворительный фонд &laquo;Предание&raquo; и&nbsp;Благотворительное собрание &laquo;Все вместе&raquo; запустили в&nbsp;конце января 2013 года проект &laquo;Больше тепла!&raquo;. Его целью был сбор средств на&nbsp;одежду, теплое белье и&nbsp;средства гигиены для бездомных обитателей Москвы. Для того, чтобы обеспечить самым необходимым 555 человек требовалось собрать всего 30 тысяч рублей. Но&nbsp;уже через день (!) после старта проект собрал необходимую сумму. А&nbsp;его общие сборы более чем в&nbsp;два раза превысили первоначальную цель.
                        </div>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/projectblago.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Наталья Маркова</div>
                            <div class="p-content-manager-role">Участница движения «Друзья на улицах»</div>
                            <div class="p-content-manager-quote">
                                Не&nbsp;все, что нам могут принести добрые люди, мы&nbsp;можем передать, поэтому мы&nbsp;придумали проект с&nbsp;&bdquo;Планетой&ldquo;, потому что нижнее белье часто требуется и&nbsp;это то, что людям стыдно просить. Мы&nbsp;не&nbsp;можем передать бывшее в&nbsp;употреблении, нужно купить новое, и&nbsp;это вопрос достоинства человека.
                            </div>
                        </div>

                        <br>

                        <div class="post-content-text proxima-reg">
                            <a class="mrg-t-30" style="border: 0" href="https://planeta.ru/planeta/blog/113891" target="_blank"><img src="images/content/bolshe-tepla-img.jpg"></a>
                            <div class="p-content-notice helveticaneue-bold">
                                Люди, оказавшие помощь движению &laquo;Друзья на&nbsp;улице&raquo; в&nbsp;покупке теплых вещей для бездомных, и&nbsp;сами получили свою порцию тепла. Среди благодарственных бонусов проекта были зимние шапки шарфы от&nbsp;марки Brazgovka, варежки ручной вязки и&nbsp;авторская зимняя шапка, связанная директором фонда &laquo;Лицом к&nbsp;лицу&raquo;&nbsp;&mdash; Елизаветой Таль.
                            </div>
                        </div>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/kurator/ava-sorokina.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Мария Сорокина</div>
                            <div class="p-content-manager-role">куратор проекта</div>
                            <div class="p-content-manager-quote">
                                Очень рада, что познакомилась с&nbsp;таким удивительным движением &laquo;Друзья на&nbsp;улице&raquo; и&nbsp;с&nbsp;Натальей Марковой. Надо заметить, что после поездки вместе с&nbsp;ребятами из&nbsp;Движения на&nbsp;вокзал Москвы, мое мнение о&nbsp;бездомных резко изменилось. Очень&nbsp;бы хотелось, чтобы больше людей узнавало о&nbsp;Движении, так как сразу приходит понимание, что мы&nbsp;все одно целое и, помогая бездомным, мы&nbsp;помогаем себе
                            </div>
                        </div>
                    </div>

                </div>

            </div>
                <? include 'includes/index-data.php'; ?>

        </div>
    </div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
<?php
/**
 * projectCard::draw( array() );
 */

class projectCard {

    public function draw($options = null) {

        // name
        // imageUrl
        // shortDescription
        // collectedAmount
        // targetAmount

        $apiId = '';
        foreach ($options['items'] as $item) {
            $apiId .= '&id=' . $item['id'];
        }
        $apiId = substr($apiId, 1);

        $projectJson = file_get_contents('https://planeta.ru/api/campaign/campaigns.json?' . $apiId);
        $projects = json_decode($projectJson);

        ?>

        <div class="project-card-list">
            <? foreach ($options['items'] as $item) : ?>

                <?
                $project = array_shift($projects);
                if ( isset($item['link']) ) {
                    $link = $item['link'] . ".php";
                } else {
                    $link = "https://planeta.ru/campaigns/" . $item['id'];
                }
                ?>

                <div class="project-card-item">
                    <a class="project-card-link" href="<?= $link ?>">
                        <span class="project-card-cover-wrap">
                            <img class="project-card-cover lazy" data-original="https://s2.planeta.ru/p?url=<?= urlencode($project->imageUrl) ?>&width=220&height=134&crop=false&pad=true&disableAnimatedGif=false" width="220" height="134">
                        </span>

                        <span class="project-card-text">
                            <span class="project-card-name"><?= $project->name ?></span>

                            <span class="project-card-descr"><?= $project->shortDescription ?></span>
                        </span>

                        <span class="project-card-footer">

                            <? if ( $project->targetAmount > 0 ) : ?>
                                <?
                                $percent = intval($project->collectedAmount / $project->targetAmount * 100);
                                ?>
                                <span class="project-card-progress<? if ( $percent > 100 ) echo ' over-progress' ?>">
                                    <span class="pcp-bar" style="width:<?= $percent % 100?>%;"></span>
                                </span>
                            <? else : ?>
                                <span class="project-card-no-progress"></span>
                            <? endif; ?>

                            <span class="project-card-info">
                                <span class="project-card-info-i">
                                    <? if ( $project->targetAmount > 0 ) : ?>
                                        <?
                                        $percent = intval($project->collectedAmount / $project->targetAmount * 100);
                                        ?>
                                        <span class="pci-label">Собрано <?= $percent ?>%</span>
                                    <? else : ?>
                                        <span class="pci-label">Собрано</span>
                                    <? endif; ?>
                                    <span class="pci-value"><?= number_format($project->collectedAmount, 0, '.', ' ') ?> <span class="b-rub">Р</span></span>
                                </span>
                                <? if ( $project->targetAmount > 0 ) : ?>
                                    <span class="project-card-info-i">
                                        <span class="pci-label">Цель</span>
                                        <span class="pci-value"><?= number_format($project->targetAmount, 0, '.', ' ') ?> <span class="b-rub">Р</span></span>
                                    </span>
                                <? endif; ?>
                            </span>

                        </span>
                    </a>
                </div>
            <? endforeach; ?>
        </div>

    <?
    }

}

<?php
/**
 *
 * tagInsert::add( array(
 *      'name',      => string
 *      'path',   => string
 * ) );
 *
 * tagInsert::draw( array(
 *      'name',      => string
 * ) );
 *
 */

class tagInsert {
    private $tags = array();

    protected function __clone()   { }
    protected function __wakeup()  { }
    protected function __construct(){ }

    static private $Instance = NULL;

    static public function getInstance() {
        if(self::$Instance==NULL){
            $class = __CLASS__;
            self::$Instance = new $class;
        }
        return self::$Instance;
    }

    public function add($options = null) {
        $name = $options['name'];
        $isKey = isset($this->tags[$name]) || array_key_exists($name, $this->tags);
        if ( !$isKey ) {
            $this->tags[$name] = array();
        }

        if ( is_string($options['path']) ) $options['path'] = array($options['path']);
        foreach ($options['path'] as $path) {
            array_push($this->tags[$options['name']], $path);
        }
    }

    public function draw($options = null) {
        if ( empty($this->tags) ) return;
        foreach ($this->tags[$options['name']] as $tag) {
            $file = ROOT_DIR.$tag;
            if (file_exists($file)) include ($file);
        }

    }
}

<?php
/**
 *
 * less::compile( array(
 *      'lessPath'  => string,
 *      'cssPath'   => string,
 *      'lessFile'  => string,             default = 'bootstrap'
 * )
 *
 */

class less {
    public function compile($options = null) {
        $cssPath = $options['cssPath'] == '' ? '/css/' : $options['cssPath'];
        $lessPath = $options['lessPath'] == '' ? '/less/' : $options['lessPath'];
    ?>
        <? if ( $_SERVER['SERVER_ADDR']=='192.168.10.130' ) : ?>
            <link href="<?= $cssPath . $options['lessFile'] ?>.css" type="text/css" rel="stylesheet">
        <? else: ?>

            <? if ( is_string($options['lessFile']) ) $options['lessFile'] = array($options['lessFile']); ?>

            <? foreach ($options['lessFile'] as $lessFile) : ?>
                <link href="<?= $lessPath . $lessFile ?>.less" type="text/x-less" rel="stylesheet/less">
            <? endforeach; ?>

            <script type="text/javascript">less = { env: 'development'};</script>
            <script src="/js/less-2.0.0.min.js" type="text/javascript"></script>
        <? endif; ?>

    <?
    }
}
<?php
/**
 * sharingBlock::draw(
 *      'class'         => string,                  default = null
 *      'size'          => string,                  default = null
 *      'counter'       => true || false,           default = true
 * );
 *
 */
class sharingBlock {
    public function draw($options = null) {
        $class = '';
        if ( isset($options['class']) ) {
            $class .= ' ' . trim($options['class']);
        }

        if ( $options['size'] == 'mini' ) {
            $class .= ' sharing-mini';
        }
        ?>

        <!--  sharing template  -->
        <div class="sharing-popup-social<?=$class?>">
            <div class="sps-button">
                <div class="spsb-item" title="В контакте"><a href="javascript:void(0)"><i class="bs-icons-vk"></i></a></div>
                <div class="spsb-item" title="Facebook"><a href="javascript:void(0)"><i class="bs-icons-fb"></i></a></div>
                <div class="spsb-item" title="Twitter"><a href="javascript:void(0)"><i class="bs-icons-tw"></i></a></div>
                <div class="spsb-item" title="SurfingBird"><a href="javascript:void(0)"><i class="bs-icons-sb"></i></a></div>
                <div class="spsb-item" title="Мой Мир"><a href="javascript:void(0)"><i class="bs-icons-mr"></i></a></div>
                <div class="spsb-item" title="Одноклассники"><a href="javascript:void(0)"><i class="bs-icons-ok"></i></a></div>
                <div class="spsb-item" title="Google+"><a href="javascript:void(0)"><i class="bs-icons-gp"></i></a></div>
                <div class="spsb-item" title="Telegram"><a href="javascript:void(0)"><i class="bs-icons-tg"></i></a></div>
            </div>
            <? if ( $options['counter'] !== false ) : ?>
            <div class="sps-count">Поделились<span>87 чел.</span></div>
            <? endif; ?>
        </div>
        <!--  // sharing template  -->

        <?
    }
}

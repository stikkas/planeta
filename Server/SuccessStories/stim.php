<?
    $title = 'St1m';

    $collected = 105200;
    $target = 30000;

$dateDuration = '2 месяца 2 дня';

    $startDay = 28;
    $startMonth = 'февраля';
    $startYear = 2013;

    $endDay = 30;
    $endMonth = 'апреля';
    $endYear = 2013;

    $members = 185;

    $projectLink = 'https://planeta.ru/campaigns/436';

    $collected = number_format($collected, 0, '.', ' ');
    $target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

<div class="post" itemscope itemtype="http://schema.org/CreativeWork">
    <div class="wrap">
        <a class="post-back-link" href="index.php">Истории успеха</a>

        <div itemprop="name" class="post-title minionpro-boldit">Запись сингла St1m &laquo;Коридоры&raquo;</div>
        <div class="post-main">

        <? require 'includes/post-meta.php'; ?>

            <img itemprop="image" class="post-big-img" src="images/stim/check.jpg">
            <div class="post-middle">
                <? require 'includes/share.php' ?>
                <div class="post-content">

                    <div itemprop="description" class="post-content-text proxima-reg">
                        St1m (Стим)&nbsp;&mdash;
                        <nobr>хип-хоп</nobr> исполнитель, ставший известным благодаря Всемирной сети. Его популярность исчисляется миллионами
                        просмотров на&nbsp;youtube (попробуй, найди в&nbsp;наш век показатели убедительней!), а&nbsp;лучшие
                        треки звучат в&nbsp;кино и&nbsp;на&nbsp;телевидении. Пользователи &laquo;Планеты&raquo; высоко
                        оценили не&nbsp;только музыкальный талант Стима (собрав на&nbsp;запись сингла &laquo;Коридоры&raquo;
                        сумму втрое выше заявленной), но&nbsp;и&nbsp;его спортивные и&nbsp;кулинарные способности: акции
                        с&nbsp;походом в&nbsp;тренажерный зал и&nbsp;приглашением на&nbsp;ужин собственного
                        приготовления были раскуплены моментально.

                        <div class="p-content-notice helveticaneue-bold">
                            Записывая &laquo;Коридоры&raquo; на&nbsp;&laquo;Планете&raquo;, St1m убедился&nbsp;&mdash;
                            перед ним лежат не&nbsp;просто коридоры, а&nbsp;безграничные пути сотрудничества с&nbsp;поклонниками.
                        </div>
                    </div>

                    <div class="post-content-manager">
                        <div class="p-content-manager-ava">
                            <img src="images/stim/ava-artist.jpg">
                        </div>
                        <div class="p-content-manager-name minionpro-mediumit">St1m</div>
                        <div class="p-content-manager-role">хип-хоп исполнитель</div>
                        <div class="p-content-manager-quote">
                            Скажу честно: я&nbsp;очень сомневался в&nbsp;том, стоит&nbsp;ли его затевать, боялся как
                            неправильной реакции на&nbsp;него (что мы&nbsp;<nobr>все-таки</nobr> увидели), так и&nbsp;того, что нам не&nbsp;удастся собрать указанную сумму, и&nbsp;это будет
                            позорно. Но! Совершенно невероятным образом всё сложилось очень круто, и&nbsp;проект
                            оправдал себя не&nbsp;просто на&nbsp;100%, но&nbsp;на&nbsp;351%!
                        </div>
                    </div>

                    <br>

                    <div class="h-video mrg-b-40">
                        <iframe width="720" height="405" frameborder="0" allowfullscreen="" src="https://tv.planeta.ru/video-frame?profileId=22956&amp;videoId=21762&amp;autostart=false"></iframe>
                    </div>

                <div class="post-milestones large-milestones">
                    <div class="post-milestones-list clearfix">
                        <div class="p-milestones-list-title minionpro-mediumit">Вехи проекта</div>
                        <div class="p-milestones-list-items">
                            <div class="pm-list-items-item">
                                <div class="pml-items-item-date proxima-bold">28 февраля 2013</div>
                                <div class="pml-items-item-text proxima-reg">Проект «Запись сингла St1m «Коридоры» открыт на «Планете»</div>
                            </div>
                            <div class="pm-list-items-item">
                                <div class="pml-items-item-date proxima-bold">21 марта 2013</div>
                                <div class="pml-items-item-text proxima-reg">Спустя всего 20 (!) дней после запуска, проект собрал заявленную сумму в 30 000 рублей.</div>
                            </div>
                            <div class="pm-list-items-item">
                                <div class="pml-items-item-date proxima-bold">29 апреля 2013</div>
                                <div class="pml-items-item-text proxima-reg">St1m пригласил акционеров на предпрослушивание сингла, которое состоится 1 сентября.</div>
                            </div>
                        </div>
                        <div class="p-milestones-list-items">
                            <div class="pm-list-items-item">
                                <div class="pml-items-item-date proxima-bold">6 марта 2013</div>
                                <div class="pml-items-item-text proxima-reg">St1m провел в Twitcam онлайн-конференцию, посвященную запуску проекта "Запишем Коридоры вместе!".</div>
                            </div>
                            <div class="pm-list-items-item">
                                <div class="pml-items-item-date proxima-bold">27 апреля 2013 в 14:25</div>
                                <div class="pml-items-item-text proxima-reg">За три дня до окончания проекта его сборы превысили 300% от заявленной суммы. До этих пор еще ни один из проектов на «Планете» не доходил до столь высокой отметки. </div>
                            </div>
                            <div class="pm-list-items-item">
                                <div class="pml-items-item-date proxima-bold">30 апреля 2013 </div>
                                <div class="pml-items-item-text proxima-reg">Проект завершен с показателем 351%!</div>
                            </div>
                        </div>
                    </div>
                </div>


                    <div class="post-content-text proxima-reg mrg-t-40">
                        <div class="p-content-notice helveticaneue-bold">
                            Стремительный и&nbsp;небывалый успех проекта Стима во&nbsp;многом определили оригинальные и&nbsp;щедрые бонусы. Среди акций проекта были: персональное посвящение песни на&nbsp;концерте; объяснение в&nbsp;любви, записанное Стимом от&nbsp;имени акционера; оригинальный рингтон и&nbsp;запись на&nbsp;автоответчик, прогулка с&nbsp;музыкантом на&nbsp;теплоходе по&nbsp;<nobr>Москва-реке</nobr>; совместный поход в&nbsp;кино и&nbsp;спортзал, и&nbsp;даже ужин, приготовленный Стимом для акционера и&nbsp;его друзей. Словом, устоять было трудно.
                        </div>
                    </div>

                    <br>

                    <div class="post-content-manager">
                        <div class="p-content-manager-ava">
                            <img src="images/kurator/ava-tatarskaya.jpg">
                        </div>
                        <div class="p-content-manager-name minionpro-mediumit">Мария Татарская</div>
                        <div class="p-content-manager-role">куратор проекта</div>
                        <div class="p-content-manager-quote">
                            Работать со&nbsp;St1m и&nbsp;его аудиторией&nbsp;&mdash; сплошное удовольствие! Поклонники
                            рэпера были очень рады интересным акциям, благодаря которым могли лично пообщаться с&nbsp;исполнителем.
                        </div>
                    </div>
                </div>


                <!--<div class="check-photo">
                    <img src="images/stim/check.jpg">
                </div>-->

                <!--<div class="post-content">
                    <a class="post-another-posts h-link minionpro-semiboldit" href="index.php">Другие успешные проекты</a>
                </div>-->

            </div>
        </div>
            <? include 'includes/index-data.php'; ?>

    </div>
</div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
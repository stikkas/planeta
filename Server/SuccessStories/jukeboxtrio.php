<?
$title = 'JukeboxTrio';

$collected = 91517;

    $dateDuration = '9 месяцев 14 дней';

    $startDay = 23;
    $startMonth = 'апреля';
    $startYear = 2012;

$endDay = 1;
$endMonth = 'февраля';
$endYear = 2013;

$members = 96;

$projectLink = 'https://planeta.ru/campaigns/26';

$collected = number_format($collected, 0, '.', ' ');
$target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

    <div class="post" itemscope itemtype="http://schema.org/CreativeWork">
        <div class="wrap">
            <a class="post-back-link" href="index.php">Истории успеха</a>

            <div itemprop="name" class="post-title minionpro-boldit">Jukebox Trio &laquo;RODINA. Часть 2&raquo;</div>
            <div class="post-main">

                <? require 'includes/post-meta.php'; ?>

                <img itemprop="image" class="post-big-img" src="images/jukebox-trio/check.jpg">

                <div class="post-middle">
                    <? require 'includes/share.php' ?>
                    <div class="post-content">
                        <div itemprop="description" class="post-content-text proxima-reg">
                            Jukebox Trio&nbsp;&mdash; главные вокальные эквилибристы отечественной сцены, чьи голоса
                            способны заменить целый оркестр. Это уникальный коллектив, не&nbsp;использующий музыкальных
                            инструментов, но&nbsp;при этом обладающий энергетикой
                            <nobr>рок-бэнда</nobr> и&nbsp;обаянием молодежных кумиров.

                            <div class="p-content-notice helveticaneue-bold">
                                Для ребят из&nbsp;Jukebox Trio &laquo;Планета&raquo; стала почти второй родиной&nbsp;&mdash; поэтому и&nbsp;альбом &laquo;Rodina, часть 2&raquo; появился именно здесь.
                            </div>
                        </div>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/jukebox-trio/ava-artist.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Jukebox Trio</div>
                            <div class="p-content-manager-quote">
                                Так уж&nbsp;исторически сложилось, что мы&nbsp;всегда обращали пристальное внимание на&nbsp;свежие
                                веяния со&nbsp;стороны Запада. Музыка, кино, тачки&nbsp;;) С&nbsp;этим у&nbsp;них всегда было
                                хорошо. Из&nbsp;самих свежих удачных поступлений&nbsp;&mdash; краудфандинг&hellip; Долго
                                разбирались, но&nbsp;когда въехали, то&nbsp;поняли, что это идеальный
                                вариант
                                взаимодействия со&nbsp;своими поклонниками. Без всяких
                                <nobr>крово/деньгососущих</nobr> продюсеров, лейблов, радиостанций и&nbsp;прочих прихлебателей. А&nbsp;Planeta только в&nbsp;помощь.
                                Она как воспитатель в&nbsp;детском саду&nbsp;&mdash; нянчится с&nbsp;твоим чадом
                                (альбомом),
                                пока родители (артисты и&nbsp;их&nbsp;почитатели) заняты.
                            </div>
                        </div>

                        <br><br>

                        <div class="post-milestones">
                            <div class="post-milestones-sidebar">
                                <div class="p-msb-item">
                                    <div class="p-msb-video">
                                        <a href="#nogo" class="dlink" id="dlink-01"><img
                                                src="images/jukebox-trio/milestone-02.jpg"></a>

                                        <div class="p-msb-iframe">
                                            <iframe id="dcontent-01" width="509" height="356"
                                                    data-href="http://www.youtube.com/embed/tOB6sE_nPyI?autoplay=1"
                                                    frameborder="0" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="post-milestones-list">
                                <div class="p-milestones-list-title minionpro-mediumit">Вехи проекта</div>
                                <div class="p-milestones-list-items">

                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">23 апреля 2012</div>
                                        <div class="pml-items-item-text proxima-reg">
                                            Проект по&nbsp;записи альбома &laquo;RODINA. Часть 2&raquo; стартовал на&nbsp;&laquo;Планете&raquo;.
                                        </div>
                                    </div>

                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">6 сентября 2012</div>
                                        <div class="pml-items-item-text proxima-reg">
                                            &laquo;Джукбоксы&raquo; провели в&nbsp;Dandy Cafe презентацию клипа на&nbsp;композицию &laquo;Собачий блюз&raquo; с&nbsp;будущего альбома.
                                        </div>
                                    </div>

                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">18 октября 2012</div>
                                        <div class="pml-items-item-text proxima-reg">
                                            За&nbsp;10 дней до&nbsp;официального релиза только для акционеров проекта группа устроила презентацию своей новой пластинки в&nbsp;клубе &laquo;БарДак&raquo;.
                                        </div>
                                    </div>

                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">28 октября 2012</div>
                                        <div class="pml-items-item-text proxima-reg">
                                            Состоялась официальная презентация пластинки &laquo;RODINA. Часть 2&raquo;.
                                        </div>
                                    </div>

                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">1 февраля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">
                                            Проект успешно завершен.
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <br>


                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/jukebox-trio/ava-kurator.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Валерий Марьянов</div>
                            <div class="p-content-manager-role">импрессарио Jukebox Trio</div>
                            <div class="p-content-manager-quote">
                                Если не&nbsp;обращать внимание на&nbsp;сложновыговариваемый термин &laquo;краудфандинг&raquo;,
                                мне кажется, это наиболее продвинутый способ отношений между талантами и&nbsp;поклонниками.
                                И&nbsp;еще немало
                                <nobr>сектантов-краудфандеров</nobr> &laquo;поляжет&raquo; в&nbsp;боях с&nbsp;&laquo;консерваторами&raquo;
                                и&nbsp;любителями халявы, зато потом&hellip; Каждый будет считать это самой простой
                                схемой&nbsp;&mdash;
                                если у&nbsp;тебя есть идея (песня, кино, картина, игра
                                <nobr>и т. д.</nobr>), и&nbsp;это может быть интересно еще
                                <nobr>кому-нибудь</nobr>&nbsp;&mdash; значит, у&nbsp;тебя есть все шансы воплотить это в&nbsp;жизнь. Без
                                посредников! Есть маленькое уточнение&nbsp;&mdash; это действительно должно быть
                                интересно
                                всем, а&nbsp;не&nbsp;только тебе и&nbsp;твоей маме. Тут Planeta бессильна:(
                            </div>
                        </div>

                        <div class="post-content-text proxima-reg mrg-b-0">
                            <div class="p-content-notice helveticaneue-bold">
                                Своим поклонникам &laquo;джукбоксы&raquo; предложили целый букет заманчивых бонусов.
                                Акционеры получили приглашение побывать на&nbsp;студии и&nbsp;стать свидетелями рождения
                                нового альбома, провести
                                <nobr>skype-встречу</nobr> с&nbsp;коллективом (причем, ребята сами связывались с&nbsp;обладателями акции, болтали по&nbsp;душам
                                и&nbsp;даже исполняли свои композиции) и, наконец,
                                <nobr>супер-бонус</nobr>&nbsp;&mdash; персональный рингтон от&nbsp;Jukebox Trio! А&nbsp;это, согласитесь, дорого
                                стоит!
                            </div>

                            <br>
                            Благодарные &laquo;Джукбоксы&raquo; решили вспомнить всех поименно&nbsp;&mdash; от&nbsp;&laquo;Планеты&raquo; до&nbsp;Солнца.
                        </div>

                        <div class="h-video">
                            <iframe width="720" height="405" frameborder="0" allowfullscreen="" src="https://tv.planeta.ru/video-frame?profileId=22956&amp;videoId=22135&amp;autostart=false"></iframe>
                        </div>

                        <br><br>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/kurator/ava-vasilina.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Василина Горовая</div>
                            <div class="p-content-manager-role">куратор проекта</div>
                            <div class="p-content-manager-quote">
                                ОБОЖАЮ Jukebox Trio! Вот представьте, что у&nbsp;вас есть безгранично талантливый друг.&nbsp;Причем
                                друг, с&nbsp;которым можно и&nbsp;веселиться, стаптывая ноги в&nbsp;танце, и&nbsp;переживать
                                непростые моменты жизни. А&nbsp;теперь помножьте все вышесказанное на&nbsp;4, и&nbsp;получаются
                                Вова, Игорь, Илюха и&nbsp;Валера. Потрясающе талантливые, невероятно обаятельные. Очень
                                их&nbsp;уважаю и&nbsp;люблю.
                            </div>
                        </div>
                    </div>

                    <!--<div class="check-photo">
                        <img src="images/jukebox-trio/check.jpg">
                    </div>-->

                    <!--<div class="post-content">
                        <a class="post-another-posts h-link minionpro-semiboldit" href="index.php">Другие успешные
                            проекты</a>
                    </div>-->

                </div>

            </div>
                <? include 'includes/index-data.php'; ?>

        </div>
    </div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
<?
$title = 'Музыкальный проект Петра Налича. Новый сингл';

$collected = 404000;
$target = 300000;

$dateDuration = '2 месяца 4 дня';

$startDay = 17;
$startMonth = 'марта';
$startYear = 2014;

$endDay = 21;
$endMonth = 'мая';
$endYear = 2014;

$members = 114;

$projectLink = 'https://planeta.ru/campaigns/4947';

$collected = number_format($collected, 0, '.', ' ');
$target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

    <div class="post" itemscope itemtype="http://schema.org/CreativeWork">
        <div class="wrap">
            <a class="post-back-link" href="index.php">Истории успеха</a>

            <div itemprop="name" class="post-title minionpro-boldit"><?=$title?></div>
            <div class="post-main">

                <? require 'includes/post-meta.php'; ?>

                <img itemprop="image" class="post-big-img" src="images/sugar-lies/check.jpg">

                <div class="post-middle">
                    <? require 'includes/share.php' ?>
                    <div class="post-content">
                        <div itemprop="description" class="post-content-text proxima-reg">
                            Музыка Петра Налича известна даже тем, кто никогда не&nbsp;включал телевизор, не&nbsp;слушал радио, не&nbsp;выходил в&nbsp;интернет и&nbsp;вообще провел последние 10 лет в&nbsp;засаде. Даже одной песни, услышанной случайно из&nbsp;форточки, окна проезжающей мимо машины или от&nbsp;друга, напевающей ее&nbsp;уже который день, будет достаточно, чтобы запомнить навсегда, будь то&nbsp;&laquo;Guitar&raquo;, &laquo;Сердце поэта&raquo; или лиричная &laquo;Lost and forgotten&raquo;.
                            <br><br>
                            Теперь к&nbsp;этому списку добавилась еще одна&nbsp;&mdash; &laquo;Sugar Lies&raquo;, к&nbsp;выпуску которой присоединились акционеры &laquo;Планеты&raquo;.
                        </div>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/sugar-lies/ava-author.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Петр Налич</div>
                            <div class="p-content-manager-role">Музыкальный коллектив Петра Налича</div>
                            <div class="p-content-manager-quote">
                                Самым неожиданным и&nbsp;приятным итогом проекта стало то, что мы&nbsp;собрали на&nbsp;100&nbsp;000 больше, чем рассчитывали. Это дало нам возможность записать песни по&nbsp;несколько раз, перевести их&nbsp;и&nbsp;многое другое. Думаю, что слаженная работа всех участников проекта и&nbsp;стала основной причиной его успешного завершения. Так что спасибо всем, кто нам помог: Даше Вирцер, Константину Швецову, Стансилаву и&nbsp;Владиславу Любаускасову, Сергею Соколову, Оскару Чунтонову, Игорю Джава-Заде, Дмитрию Симонову, Юрию Костенко и&nbsp;Дмитрию Сланскому.
                            </div>
                        </div>

                        <br>
                        <br>
                        <br>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/sugar-lies/ava-shvecov.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Константин Швецов</div>
                            <div class="p-content-manager-role">Музыкальный коллектив Петра Налича</div>
                            <div class="p-content-manager-quote">
                                Для меня, как для организатора от&nbsp;нашего музыкального коллектива, самым приятным оказалось то, что всё было с&nbsp;самого начала и&nbsp;до&nbsp;конца абсолютно ровно и&nbsp;хорошо спланировано и&nbsp;реализовано. От&nbsp;первоначального разговора с&nbsp;нашим куратором Дашей Вирцер и&nbsp;до&nbsp;раздачи акций после окончания проекта. Всё как по&nbsp;часам, с&nbsp;минимальными задержками. Это действительно редкость. Я, честно говоря, с&nbsp;самого начала был готов к&nbsp;<nobr>каким-нибудь</nobr> накладкам и&nbsp;был приятно удивлён тем, что даже в&nbsp;мелочах всё получилось идеально.
                                <br>
                                <br>
                                Как ни&nbsp;крути, но&nbsp;залог успеха любого подобного проекта это первоначальная <nobr>идея-материал</nobr> и&nbsp;люди, которые эту идею поддержали. Если идея пустая, то&nbsp;никого она не&nbsp;тронет. Но&nbsp;конечно&nbsp;же даже если идея супер, то&nbsp;в&nbsp;итоге всё равно всё упирается в&nbsp;конкретных людей, которые тебя поддерживают или нет. Люди и&nbsp;идея, идея и&nbsp;люди&hellip;оба фактора ключевые.
                                <br>
                                <br>
                                Спасибо всей команде Планеты и&nbsp;Даше Вирцер лично. Даша, ты&nbsp;лучшая! Спасибо тем 114 акционерам, которые нас поддержали и&nbsp;остальным семи миллиардам ста тридцати семи миллионам пятистам семидесяти семи тысячам шестистам сорока шести людям, которые держали всей планетой за&nbsp;нас кулачки. Ребята, если&nbsp;бы не&nbsp;вы, то&nbsp;всё было&nbsp;бы гораздо сложнее.
                            </div>
                        </div>

                        <br>
                        <br>
                        <br>


                        <div class="post-milestones large-milestones">
                            <div class="post-milestones-list clearfix">
                                <div class="p-milestones-list-title minionpro-mediumit">Вехи проекта</div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">17 марта 2014</div>
                                        <div class="pml-items-item-text proxima-reg">Проект по&nbsp;записи нового сингла &laquo;Sugar&nbsp;lies&raquo; запущен на&nbsp;Планете.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">1 апреля 2014</div>
                                        <div class="pml-items-item-text proxima-reg">Меньше чем за&nbsp;неделю проекта 50% необходимой суммы было собрано. Группа&nbsp;приоткрывает завесу тайны и&nbsp;<a href="https://planeta.ru/189950/blog/124599" target="_blank">делится</a> с&nbsp;участниками сообщества небольшим студийным видео, сделанным во&nbsp;время записи нового сингла.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">8 апреля 2014</div>
                                        <div class="pml-items-item-text proxima-reg">В&nbsp;проект <a href="https://planeta.ru/189950/blog/124828" target="_blank">добавлены новые акции</a>&nbsp;&mdash; фирменные значки с&nbsp;заглавным символом альбома &laquo;Песни любви и&nbsp;родины&raquo;.</div>
                                    </div>
                                </div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">16 апреля 2014</div>
                                        <div class="pml-items-item-text proxima-reg">Всем зомбак! Группа <a href="https://planeta.ru/189950/blog/125069" target="_blank">объявляет бонус</a> для&nbsp;акционера, который последним внесет недостающую сумму в&nbsp;проект.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">21 мая 2014</div>
                                        <div class="pml-items-item-text proxima-reg">Проект успешно <a href="https://planeta.ru/189950/blog/125965" target="_blank">завершился</a>. Всем акционерам проекта за&nbsp;несколько дней до&nbsp;релиза был выслан новый сингл.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">30 мая 2014</div>
                                        <div class="pml-items-item-text proxima-reg">В&nbsp;московском клубе <nobr>&laquo;Известия-hall&raquo;</nobr> состоялась презентация нового сингла группы.</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br>

                        <div class="post-content-manager mrg-t-50 mrg-b-50">
                            <div class="p-content-manager-ava">
                                <img src="images/kurator/ava-virtser.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Даша Вирцер</div>
                            <div class="p-content-manager-role">куратор проекта</div>
                            <div class="p-content-manager-quote">
                                Курировать проект МКПН было сплошным удовольствием. Впрочем, необходимость в&nbsp;контроле и&nbsp;рекомендациях фактически отсутствовала, так как музыканты подошли к&nbsp;делу очень ответственно: внимательно и&nbsp;бережно подготовили контент, придумали для акционеров крутые бонусы, быстро реагировали на&nbsp;фидбэки от&nbsp;пользователей и&nbsp;&mdash; самое главное&nbsp;&mdash; оперативно и&nbsp;четко выполнили все обязательства перед акционерами. В&nbsp;общем, талантливые люди талантливы во&nbsp;всем! Отдельное спасибо хочу сказать Косте, который является не&nbsp;только незаменимым игроком на&nbsp;поле, но&nbsp;и&nbsp;блестяще справляется с&nbsp;огромным объемом административной работы, не&nbsp;теряя веселый нрав и&nbsp;сохраняя бдительность! Желаю больших успехов ребятам, Музыкальному коллективу Петра Налича&nbsp;&mdash; ура!
                            </div>
                        </div>

                        <br>

                        <div class="post-tips">
                            <div class="post-tips_img"><img src="images/sugar-lies/tips.png"></div>
                            <div class="post-tips_head">
                                <span class="post-tips_head-spn">
                                    Три правила
                                    <br>
                                    успешного
                                    <br>
                                    краудфандинга от
                                    <br>
                                    <span style="margin-left: -.533333333em;">«</span>Музыкального
                                    <br>
                                    коллектива Петра
                                    <br>
                                    Налича»
                                </span>
                            </div>

                            <ol class="rich-ol">
                                <li>
                                    Больше разных акций. Не&nbsp;бойтесь экспериментировать и&nbsp;придумывать <nobr>что-то</nobr>, что до&nbsp;вас не&nbsp;делали, генерите <nobr>какие-то</nobr> характерные для вас идеи, акции, которые будут отличать ваш проект от&nbsp;остальных. В&nbsp;конечном итоге ведь никто не&nbsp;знает, какая именно акция &laquo;выстрелит&raquo;.
                                    <br><br>
                                </li>
                                <li>
                                    Довольно важно точно определить нужную для полноценной реализации проекта сумму. Понятно, что сложно оценить траты на&nbsp;творческие проекты, ведь в&nbsp;поиске можно находится сколько угодно времени и&nbsp;денег на&nbsp;реализацию любой идеи можно вбухать сколько угодно. Закладывайте чуть больше чем вам надо, но&nbsp;и&nbsp;не&nbsp;жадничайте.
                                    <br><br>
                                </li>
                                <li>
                                    Работайте плотнее с&nbsp;вашей аудиторией, постоянно напоминайте о&nbsp;вашем проекте на&nbsp;всех ваших и&nbsp;дружеских ресурсах. В&nbsp;идеале конечно каждый раз для этого нужен <nobr>какой-то</nobr> повод: новая интересная акция, весёлый скетч, объединяющий танец, видео с&nbsp;чихающим чихуахуа, купание с&nbsp;резиновыми уточками в&nbsp;горном озере в&nbsp;Тибете&hellip;
                                </li>
                            </ol>
                        </div>


                    </div>

                </div>
            </div>
                <? include 'includes/index-data.php'; ?>

        </div>
    </div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
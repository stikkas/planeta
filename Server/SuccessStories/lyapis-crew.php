<?
$title = 'Lyapis Crew: ТРУбьют';

$collected = 410465;
$target = 300000;

$dateDuration = '1 год 1 месяц 8 дней';

$startDay = 24;
$startMonth = 'июля';
$startYear = 2013;

$endDay = 1;
$endMonth = 'сентября';
$endYear = 2014;

$members = 431;

$projectLink = 'https://planeta.ru/campaigns/1431';

$collected = number_format($collected, 0, '.', ' ');
$target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

    <div class="post" itemscope itemtype="http://schema.org/CreativeWork">
        <div class="wrap">
            <a class="post-back-link" href="index.php">Истории успеха</a>

            <div itemprop="name" class="post-title minionpro-boldit"><?=$title?></div>
            <div class="post-main">

                <? require 'includes/post-meta.php'; ?>

                <img itemprop="image" class="post-big-img" src="images/lyapis-crew/check.jpg">

                <div class="image-mark">* К моменту, когда была сделана эта фотография, проект еще не был окончен.</div>

                <div class="post-middle">
                    <? require 'includes/share.php' ?>
                    <div class="post-content">
                        <div itemprop="description" class="post-content-text proxima-reg">
                            Было&nbsp;бы странно предполагать, что такая во&nbsp;всех смыслах нестандартная группа как &laquo;Ляпис Трубецкой&raquo; к&nbsp;созданию проекта на&nbsp;Планете подойдет стандартно. Поэтому и&nbsp;краудфандинг у&nbsp;них получился народный, если не&nbsp;сказать массовый.
                            <br>
                            <br>
                            Lyapis Crew: ТРУбьют стал первым проектом на&nbsp;Планете, который объединил молодых исполнителей с&nbsp;уже состоявшимися музыкантами. А&nbsp;слушатели в&nbsp;свою очередь поддержали и&nbsp;тех и&nbsp;других.
                        </div>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/lyapis-crew/ava-author.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Александр Бергер</div>
                            <div class="p-content-manager-role">пресс-атташе «Ляписа Трубецкого»</div>
                            <div class="p-content-manager-quote">
                                Когда мы&nbsp;задумали этот проект, у&nbsp;нас было несколько целей. <nobr>Во-первых</nobr>, хотелось напомнить слушателям о&nbsp;существовании огромного пласта творчества группы &laquo;Ляпис Трубецкой&raquo;, к&nbsp;которому Сергей Михалок не&nbsp;обращался последние лет десять, а&nbsp;то&nbsp;и&nbsp;больше. Это почти все песни, записанные до&nbsp;альбома &laquo;Капитал&raquo;&nbsp;&mdash; среди них много настоящих хитов, добрых, веселых, ироничных. Автор имеет полное право отказаться от&nbsp;своих песен по&nbsp;субъективным причинам, но&nbsp;большое количество фанов до&nbsp;сих по&nbsp;ним скучает&nbsp;&mdash; так почему&nbsp;бы не&nbsp;дать им&nbsp;новую жизнь в&nbsp;виде интересных <nobr>кавер-версий</nobr>? <nobr>Во-вторых</nobr>, нам хотелось за&nbsp;счет громкого имени &laquo;Ляписов&raquo; поддержать молодые команды из&nbsp;разных городов и&nbsp;стран&nbsp;&mdash; мы&nbsp;сознательно не&nbsp;обращались к&nbsp;&laquo;звездам&raquo;, не&nbsp;просили их&nbsp;принять участие в&nbsp;ТРУбьюте. Мы&nbsp;хотели дать дорогу молодым. Конечно, некоторые наши&nbsp;друзья&nbsp;&mdash; Noize MC, &laquo;Кирпичи&raquo;, НОМ и&nbsp;проч. &mdash;&nbsp;записали свои каверы для проекта, но&nbsp;это было результатом, скорее, нашей дружбы, чем <nobr>каким-то</nobr> <nobr>шоу-бизнес-ходом</nobr>. И&nbsp;я&nbsp;могу сказать, что обе цели проекта в&nbsp;итоге были достигнуты!
                            </div>
                        </div>

                        <br>
                        <br>
                        <br>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/lyapis-crew/ava-author-2.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Евгений Колмыков</div>
                            <div class="p-content-manager-role">продюсер «Ляписа Трубецкого»</div>
                            <div class="p-content-manager-quote">
                                В&nbsp;первую очередь, мы&nbsp;обращали внимание на&nbsp;оригинальный подход музыкантов к&nbsp;<nobr>кавер-версиям</nobr>. Если группа исполняла песню Manifest один в&nbsp;один, как &laquo;Ляписы&raquo;&nbsp;&mdash; кому это могло быть интересно? А&nbsp;если группа записывала &laquo;Нафту&raquo; в&nbsp;стиле Napalm Death или поднимала <nobr>какой-то</nobr> всеми забытый трек из&nbsp;первого альбома &laquo;Ляписов&raquo; и&nbsp;делала из&nbsp;него шикарный <nobr>панк-боевик</nobr>&nbsp;&mdash; конечно, этот трек сразу привлекал наше внимание и&nbsp;в&nbsp;итоге входил в&nbsp;альбом.
                            </div>
                        </div>


                        <br>
                        <br>
                        <br>


                        <div class="post-milestones large-milestones">
                            <div class="post-milestones-list clearfix">
                                <div class="p-milestones-list-title minionpro-mediumit">Вехи проекта</div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">24 июля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Проект запущен на&nbsp;Планете. Начинается <a href="https://planeta.ru/lyapis/blog/118799">отбор каверов для ТРУбьюта</a>. Всего за&nbsp;время существования проекта было прислано 170 каверов, а&nbsp;к&nbsp;посту было оставлено более 1000 комментариев.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">22 августа 2013</div>
                                        <div class="pml-items-item-text proxima-reg"><a href="https://planeta.ru/lyapis/blog/119373">Первый &laquo;звездный&raquo; кавер</a> для проекта записан. В&nbsp;проекте приняла участие группа &laquo;Тараканы!&raquo;</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">6 ноября 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Обложка <nobr>Тру-альбома</nobr> готова. К&nbsp;этому моменту к&nbsp;проекту подключились группа &laquo;Наив&raquo; и&nbsp;Йохан Мейер.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-text proxima-reg">
                                            <img src="images/lyapis-crew/milestones-1.jpg">
                                        </div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">декабрь 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Проект торжественно продлен, первые финалисты объявлены, а&nbsp;свое участие в&nbsp;проекте уже подтвердили группы &laquo;НОМ&raquo;, &laquo;Бумбокс&raquo;, Noize MC, &laquo;Элизиум&raquo;, &laquo;7000$&raquo;, Карл Хламкин и&nbsp;другие.</div>
                                    </div>
                                </div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">17 марта 2014</div>
                                        <div class="pml-items-item-text proxima-reg">Группа &laquo;Ляпис Трубецкой&raquo; неожиданно <a href="https://planeta.ru/lyapis/blog/124098">объявляет об&nbsp;окончании своей творческой деятельности</a>. На&nbsp;проект возложена почетная миссия стать достойным последним &laquo;аккордом&raquo; в&nbsp;истории группы. Сбор каверов продолжается, а&nbsp;проект выходит на&nbsp;новую ступень&nbsp;&mdash; ТРУбьют обещает быть тройным!</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">14 июля 2014</div>
                                        <div class="pml-items-item-text proxima-reg">Первый из трех ТРУбьютов <a href="https://planeta.ru/lyapis/blog/127447">увидел свет</a>. В&nbsp;него вошло 28 треков.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-text proxima-reg">
                                            <img src="images/lyapis-crew/milestones-2.jpg">
                                        </div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">11 августа 2014</div>
                                        <div class="pml-items-item-text proxima-reg"><a href="https://planeta.ru/lyapis/blog/128154">Второй ТРУбьют</a> на подходе с 26 новыми треками.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-text proxima-reg">
                                            <img src="images/lyapis-crew/milestones-3.jpg">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="post-content-manager mrg-t-50 mrg-b-50">
                            <div class="p-content-manager-ava">
                                <img src="images/kurator/ava-egor.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Егор Ельчин</div>
                            <div class="p-content-manager-role">куратор проекта</div>
                            <div class="p-content-manager-quote">
                                В&nbsp;1997 году мне, дурачку, было 10 неполных лет. Тогда в&nbsp;доме, уж&nbsp;и&nbsp;не&nbsp;вспомню каким образом, появилась кассета с&nbsp;музыкальным сборником &laquo;СОЮЗ 20&raquo;. На&nbsp;стороне &laquo;А&raquo; финальной песней был &laquo;Паренек&raquo; &laquo;Ляписа Трубецкого&raquo;. Так я&nbsp;и&nbsp;познакомился с&nbsp;голосом Сергея Михалка и&nbsp;творчеством группы, а&nbsp;чуть позже выяснилось, что Михалок и&nbsp;жил некоторое время в&nbsp;соседнем городе. С&nbsp;тех самых пор их&nbsp;музыка всегда была со&nbsp;мной, а&nbsp;кассет &laquo;Трубецкого&raquo; в&nbsp;доме стало прибавляться. В&nbsp;школьные годы не&nbsp;было ничего веселее, забойнее, чем песни &laquo;ЛТ&raquo;. Нравилось все, от&nbsp;обложки альбома (до&nbsp;сих пор обожаю оформление альбома &laquo;Тяжкий&raquo;), до&nbsp;самых грустных и&nbsp;лирических песен. Потом университет, новый этап, и&nbsp;новый &laquo;Ляпис&raquo; как нельзя лучше вписался в&nbsp;мой жизненный саундтрек. От&nbsp;&laquo;Любови капец&raquo; до&nbsp;&laquo;Матрешки&raquo;. От&nbsp;<nobr>&laquo;Цыки-цык&raquo;</nobr> до&nbsp;&laquo;Воинов света&raquo;. Я&nbsp;горжусь тем, что имею прямое отношение к&nbsp;созданию <nobr>трибьют-альбомов</nobr> и&nbsp;очень рад, что все получилось куда масштабнее изначального плана. Спасибо Саше Бергеру, Жене Колмыкову и&nbsp;Антону Азизбекяну за&nbsp;слаженную работу над проектом. И&nbsp;всем, кто внес свой вклад в&nbsp;ТРУбьют, материально или творчески!
                            </div>
                        </div>

                        <br>

                        <div class="post-tips">
                            <div class="post-tips_img" style="padding: 0 0 0 220px; background:#000;"><img src="images/lyapis-crew/tips.jpg" height="209"></div>
                            <div class="post-tips_head">
                                <span class="post-tips_head-spn">
                                    Три правила
                                    <br>
                                    успешного
                                    <br>
                                    краудфандинга
                                    <br>
                                    от Ляписа
                                    <br>
                                    Трубецкого
                                </span>
                            </div>

                            <ol class="rich-ol">
                                <li>
                                    Проект должен быть оригинальным, а&nbsp;еще лучше&nbsp;&mdash; уникальным. Чтобы человек, которого мотивируют сделать инвестицию, понимал: если не&nbsp;состоится этот проект&nbsp;&mdash; ничего подобного не&nbsp;будет.
                                    <br><br>
                                </li>
                                <li>
                                    Автор проекта должен быть на&nbsp;постоянной связи со&nbsp;своими потенциальными акционерами&nbsp;&mdash; напоминать о&nbsp;себе, придумывать <nobr>что-то</nobr> новое, увлекать, развлекать. Если ты&nbsp;объявил о&nbsp;начале проекта и&nbsp;забыл о&nbsp;нем&nbsp;&mdash; значит, и&nbsp;аудитория забудет.
                                    <br><br>
                                </li>
                                <li>
                                    Если ты&nbsp;уверен в&nbsp;том, что твой проект&nbsp;&mdash; не&nbsp;парафин, не&nbsp;нужно стесняться просить друзей и&nbsp;знакомых о&nbsp;поддержке. Даже финансовой. Прямо напрямую, адресно.
                                </li>
                            </ol>
                        </div>

                    </div>

                </div>
            </div>

            <div class="short-blockquote">
                <div class="short-blockquote_wrap">

                    <div class="short-blockquote_head">Финалисты ТРУбьюта о своем участии в проекте</div>

                    <div class="short-blockquote_list">
                        <div class="short-blockquote_i">
                            <div class="short-blockquote_ava">
                                <img src="images/lyapis-crew/rasted.jpg">
                            </div>
                            <div class="short-blockquote_cont">
                                <div class="short-blockquote_name">
                                    <span>Группа «Rusted»</span>
                                </div>
                                <div class="short-blockquote_text">
                                    Участвовать в&nbsp;проекте, посвященному трибьюту такой группы&nbsp;&mdash; большое удовольствие. Кавер сделали практически через несколько дней после начала проекта. Борьба была интересной, и&nbsp;мы&nbsp;регулярно заходили в&nbsp;каверохранилище и&nbsp;слушали новые песни. Сами выбрали песню &laquo;Мужчины не&nbsp;плачут&raquo;, так как это одна из&nbsp;любимых песен Ляписов. Она оптимистична, <nobr>по-мужски</nobr> бодрая. Мы&nbsp;просто дали ей&nbsp;ту&nbsp;оболочку, в&nbsp;которой нам&nbsp;бы хотелось её услышать!
                                </div>
                            </div>
                        </div>
                        <div class="short-blockquote_i">
                            <div class="short-blockquote_ava">
                                <img src="images/lyapis-crew/hlamkin.jpg">
                            </div>
                            <div class="short-blockquote_cont">
                                <div class="short-blockquote_name">
                                    <span>Карл Хламкин</span>
                                </div>
                                <div class="short-blockquote_text">
                                    Мы&nbsp;искали не&nbsp;сильно известную песню, в&nbsp;которой есть текст с&nbsp;налетом романтического абсурда и&nbsp;менее известную. С&nbsp;Ляписами знаком лет 20. Так получилось, что вся эта минская компания мне нравится по&nbsp;<nobr>какому-то</nobr> человеческому фактору: люди из&nbsp;одного цыганского табора. Многие песни очень мощные и&nbsp;отсылки к&nbsp;Олди (&laquo;Комитет Охраны Тепла&raquo;), а&nbsp;Олди это очень интересная личность.
                                </div>
                            </div>
                        </div>
                        <div class="short-blockquote_i">
                            <div class="short-blockquote_ava">
                                <img src="images/lyapis-crew/kagadeev.jpg">
                            </div>
                            <div class="short-blockquote_cont">
                                <div class="short-blockquote_name">
                                    <span>Андрей Кагадеев, группа «НОМ»</span>
                                </div>
                                <div class="short-blockquote_text">
                                    Песня выбрана на&nbsp;мой вкус. Альбом Веселые картинки мой любимый. А&nbsp;слова &laquo;Все истины есть в&nbsp;букваре и&nbsp;в&nbsp;детской <nobr>книжке-раскраске</nobr>&raquo; вообще самое умное, что придумал Михалок&raquo;.
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <? include 'includes/index-data.php'; ?>

        </div>
    </div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
<? $title = 'Colta.ru: история успеха на planeta.ru' ?>
<? require 'includes/header.php'; ?>

<div class="post" itemscope itemtype="http://schema.org/CreativeWork">
    <div class="wrap">
        <a class="post-back-link" href="index.php">Истории успеха</a>
        <div itemprop="name" class="post-title minionpro-boldit">Запуск сайта Colta.ru, нового проекта команды OpenSpace</div>
        <div class="post-main">

            <div class="post-meta">
                <div class="post-meta-item collected">
                    <div class="p-meta-title proxima-reg">Проект собрал</div>
                    <div class="p-meta-num helveticaneue-bold">695 103 <span class="b-rub">Р</span></div>
                </div>
                <div class="post-meta-item target">
                    <div class="p-meta-title proxima-reg">Цель была</div>
                    <div class="p-meta-num helveticaneue-bold">600 000 <span class="b-rub">Р</span></div>
                </div>
                <div class="post-meta-item date">
                    <div class="p-meta-title proxima-reg">Сроки проведения</div>
                    <div class="p-meta-date-item">
                        <div class="pm-date-item-day">6</div>
                        <div class="pm-date-item-moth">июля</div>
                        <div class="pm-date-item-year">2012</div>
                    </div>
                    <div class="p-meta-date-item">
                        <div class="pm-date-item-day">16</div>
                        <div class="pm-date-item-moth">октября</div>
                        <div class="pm-date-item-year">2012</div>
                    </div>
                </div>
                <div class="post-meta-item members">
                    <div class="p-meta-title helveticaneue-bold">524</div>
                    <div class="p-meta-num proxima-reg">Акционера приняли участие</div>
                </div>
            </div>

            <img itemprop="image" class="post-big-img" src="images/5diez/big-image.jpg">
            <div class="post-middle">
                <? require 'includes/share.php' ?>
                <div class="post-content">

                    <div class="post-content-manager">
                        <div class="p-content-manager-ava">
                            <img src="images/5diez/ava-artist.jpg">
                        </div>
                        <div class="p-content-manager-name minionpro-mediumit">Михаил Ратгауз</div>
                        <div class="p-content-manager-role">заместитель главного редактора Colta.ru</div>
                        <div class="p-content-manager-quote proxima-regular-it">
                            Встречи с&nbsp;&laquo;Планетой&raquo; (их&nbsp;было две, а&nbsp;одна еще продолжается)
                            доставили редакции сайта Colta.ru много незабываемых минут. Никогда еще читатель не&nbsp;был
                            так близок, никогда еще его привязанность к&nbsp;сайту не&nbsp;носила такие конкретные
                            очертания. Да&nbsp;что говорить, &laquo;Кольте&raquo; скоро будет год&nbsp;&mdash; и&nbsp;ее&nbsp;просто&nbsp;бы
                            не&nbsp;было без сайта Planeta.ru. Спасибо, ребята! И&nbsp;спасибо, тем, кто на&nbsp;эту
                            самую &laquo;Планету&raquo; приземлялся с&nbsp;поддержкой и&nbsp;продолжает это делать!
                        </div>
                    </div>

                    <br>

                    <div class="h-video">
                        <iframe width="720" height="405" frameborder="0" allowfullscreen="" src="https://tv.planeta.ru/video-frame?profileId=22956&amp;videoId=21771&amp;autostart=false"></iframe>
                    </div>

                    <br><br>

                    <div itemprop="description" class="post-content-text proxima-reg">
                        Акции проекта сопровождали поистине уникальные бонусы: книга Льва Рубинштейна и&nbsp;Бориса
                        Акунина &laquo;От&nbsp;мая до&nbsp;мая&raquo; с&nbsp;автографами авторов, экскурсия для 5
                        человек по&nbsp;закулисью Большого театра, ретвит от&nbsp;Олега Кашина, сюрреалистические брюки
                        из&nbsp;клипа &laquo;Ляписа Трубецкого&raquo;, прогулка с&nbsp;Дмитрием Гутовым по&nbsp;залам
                        Третьяковской галереи, бутлег The Cure &laquo;Three Imaginary Boys&raquo; с&nbsp;автографами
                        музыкантов, плакаты фильма &laquo;Фауст&raquo;, подписанные Александром Сокуровым и&nbsp;многое
                        другое.

                        <div class="p-content-notice helveticaneue-bold">
                            Благодаря пользователям &laquo;Планеты&raquo; проект Colta еще долго не&nbsp;будет требовать перезарядки&nbsp;&mdash; а&nbsp;главное, что только с&nbsp;ними он&nbsp;стал возможен!
                        </div>

                        В&nbsp;рамках поддержки проекта &laquo;Кольта&raquo; организовала закрытую презентацию, которая
                        прошла 7 апреля 2013 года в&nbsp;клубе ArteFAQ. Мероприятие состояло из&nbsp;четырех частей:
                        началось с&nbsp;короткометражных фильмов Ренаты Литвиновой, Бориса Хлебникова, Михаила Сегала и&nbsp;премьерой
                        микросериала Александра Расторгуева и&nbsp;Павла Костомарова; продолжилась премьерой спектакля
                        Дмитрия Волкострелова по&nbsp;пьесе, специально для этого вечера написанной Михаилом
                        Дурненковым. Завершили вечер выступления групп &laquo;Шкловский&raquo; и&nbsp;
                        <nobr>On-The-Go</nobr>
                        , продолжившиеся дискотекой для всех участников.
                    </div>
                    <div class="post-content-manager">
                        <div class="p-content-manager-ava">
                            <img src="images/content/history-manager-ava.jpg">
                        </div>
                        <div class="p-content-manager-name minionpro-mediumit">Катерина Чечулина</div>
                        <div class="p-content-manager-role">PR-директор Planeta.ru</div>
                        <div class="p-content-manager-quote proxima-regular-it">
                            Проект Colta.ru был для нас знаковым. Мы&nbsp;запустили его ровно через месяц после
                            официального открытия &laquo;Планеты&raquo;, 6 июля 2012. Тогда мало кто в&nbsp;России знал,
                            что такое краудфандинг.&nbsp;Мария Степанова и&nbsp;вся редакция не&nbsp;побоялись
                            эксперимента, за&nbsp;что им&nbsp;низкий поклон. Помню, как долго мы&nbsp;с&nbsp;Мишей
                            Ратгаузом совещались, исправляли тексты, акции и&nbsp;нажали на&nbsp;&laquo;старт&raquo; в&nbsp;три
                            часа ночи. Через сутки в&nbsp;копилке проекта было 200 тысяч рублей, а&nbsp;через 20 дней&nbsp;&mdash;
                            вся необходимая сумма. Этот успех был свидетельством не&nbsp;только огромной любви аудитории
                            к&nbsp;редакции, но&nbsp;и&nbsp;четким подтверждением того, что краудфандинг в&nbsp;России
                            возможен. Люди увидели, что он&nbsp;работает! Вообще, Кольта разрушила все обычные правила
                            краудфандинга.
                            <nobr>Во-первых</nobr>
                            , это журналистский проект, на&nbsp;которые даже на&nbsp;Западе сборы идут довольно сложно.
                            <nobr>Во-вторых</nobr>
                            , Кольта, собрав почти 700 тысяч в&nbsp;первом проекте, открыла следующий и&nbsp;вновь
                            собрала уже около 800 тысяч, что тоже редкость. Я&nbsp;очень рада, что &laquo;Планета&raquo;
                            причастна к&nbsp;тому, что Colta.ru, фактически единственное общественное СМИ в&nbsp;России,
                            сегодня существует.
                        </div>
                    </div>
                </div>

                <div class="post-content">
                    <a class="post-another-posts minionpro-semiboldit" href="index.php">Другие успешные проекты</a>
                </div>
            </div>
        </div>

    </div>
</div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
<?
$title = 'Несчастный случай. &laquo;Гоняясь за&nbsp;бизоном&raquo;';

$collected = 555233;
$target = 500000;

$dateDuration = '7 месяцев 3 дня';

$startDay = 12;
$startMonth = 'апреля';
$startYear = 2013;

$endDay = 15;
$endMonth = 'ноября';
$endYear = 2013;

$members = 274;

$projectLink = 'https://planeta.ru/campaigns/272';

$collected = number_format($collected, 0, '.', ' ');
$target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

    <div class="post" itemscope itemtype="http://schema.org/CreativeWork">
        <div class="wrap">
            <a class="post-back-link" href="index.php">Истории успеха</a>

            <div itemprop="name" class="post-title minionpro-boldit"><?=$title?></div>
            <div class="post-main">

                <? require 'includes/post-meta.php'; ?>

                <img itemprop="image" class="post-big-img" src="images/neschastnyj-sluchaj/check.jpg">

                <div class="post-middle">
                    <? require 'includes/share.php' ?>
                    <div class="post-content">
                        <div itemprop="description" class="post-content-text proxima-reg">
                            30 лет. 13 альбомов. Множество концертов в&nbsp;разных городах России и&nbsp;Зарубежья. К&nbsp;творческому юбилею группа &laquo;Несчастный случай&raquo; решила отнестись со&nbsp;всей серьезностью и&nbsp;предложила своим слушателям записать альбом вместе. Задача предстояла нелегкая: из&nbsp;материала, накопившегося за&nbsp;все время существования коллектива, нужно было выбрать 12 лучших песен, которые должны были войти&nbsp;в&nbsp;&laquo;Гоняясь за&nbsp;бизоном&raquo;.<br><br>Благодаря поддержке аудитории, которая очень быстро откликнулась на&nbsp;призыв группы, удалось собрать больше половины требуемой суммы меньше, чем за&nbsp;месяц. В&nbsp;итоге, вместо 12 положенных песен в&nbsp;альбом вошло 19, а&nbsp;большинство акционеров, поучаствовавших в&nbsp;проекте, были приглашены на&nbsp;юбилейный концерт в&nbsp;Crocus City Hall, где и&nbsp;отметили круглую дату вместе с&nbsp;любимой группой.
                        </div>

                        <div class="post-content-manager">
                            <div class="p-content-manager-ava">
                                <img src="images/neschastnyj-sluchaj/author-kortnev.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Алексей Кортнев</div>
                            <div class="p-content-manager-role">участник группы &laquo;Несчастный случай&raquo;</div>
                            <div class="p-content-manager-quote">
                                Друзья, дорогие! Можно сказать, что опыт краудфандинга на&nbsp;Planeta.ru оказался для нашей группы не&nbsp;просто успешным, а, можно сказать, блестящим. Потому что мы&nbsp;собрали больше денег, чем было заявлено &#8213; и&nbsp;уверяю вас, что излишки использовали тоже на&nbsp;благие цели, а&nbsp;не&nbsp;пропили в&nbsp;подворотне. Мы&nbsp;собрали большую часть этой суммы в&nbsp;первый&nbsp;же месяц после запуска проекта, что, несомненно, очень сильно помогло быстрой и&nbsp;эффективной работе. Надо сказать, что у&nbsp;нас и&nbsp;от&nbsp;самого процесса работы с&nbsp;Planeta.ru, и&nbsp;от&nbsp;последующей встречи с&nbsp;нашими основными акционерами, и&nbsp;вообще от&nbsp;записи этого альбома остались очень солнечные светлые впечатления, потому что, конечно, когда делаешь <nobr>что-то</nobr> с&nbsp;большой группой лиц, добровольно присоединившихся, любая работа становится легче, веселее, эффективнее. Поэтому спасибо огромное сайту&nbsp;Planeta.ru! Мы&nbsp;несомненно продолжим сотрудничество &#8213; скоро соберемся делать следующий проект, надеемся, что сделаем его вместе. Еще раз спасибо!
                            </div>
                        </div>

                        <div class="post-content-text proxima-reg mrg-b-50">
                            <div class="p-content-notice helveticaneue-bold">
                                Специально к&nbsp;проекту на&nbsp;Планете, группа подготовила юбилейные наборы, включающие в&nbsp;себя билеты на&nbsp;двоих на&nbsp;юбилейный концерт, альбом в&nbsp;трех форматах (цифровой, виниловый и&nbsp;cd), афишу с&nbsp;автографами и&nbsp;футболку. Так&nbsp;же у&nbsp;акционеров была возможность получить полную дискографию группы за&nbsp;все 30 лет.
                            </div>
                        </div>


                        <div class="post-milestones large-milestones">
                            <div class="post-milestones-list clearfix">
                                <div class="p-milestones-list-title minionpro-mediumit">Вехи проекта</div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">12 апреля 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Проект &laquo;Несчастный случай. Гоняясь за&nbsp;бизоном&raquo; запущен на&nbsp;Планете.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">18 сентября 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Состоялась премьера клипа на&nbsp;новую песню группы&nbsp;&mdash; &laquo;Суета сует&raquo;. В&nbsp;честь этого на&nbsp;Планете был <a href="https://planeta.ru/sluchaj/blog/119976">запущен</a> аукцион, главным лотом которого стал памятный осколок тарелки, разбитый на&nbsp;съемках клипа.</div>
                                    </div>
                                </div>
                                <div class="p-milestones-list-items">
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">1 августа 2013</div>
                                        <div class="pml-items-item-text proxima-reg">Записано <a href="https://planeta.ru/sluchaj/blog/118996">видеоприглашение</a> для акционеров на юбилейный концерт в Crocus City Hall.</div>
                                    </div>
                                    <div class="pm-list-items-item">
                                        <div class="pml-items-item-date proxima-bold">30 ноября 2013</div>
                                        <div class="pml-items-item-text proxima-reg">В Crocus City Hall при полном аншлаге прошел юбилейный концерт группы. Одновременно на Планете была организована онлайн-трансляция</div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="post-content-manager mrg-t-50 mrg-b-50">
                            <div class="p-content-manager-ava">
                                <img src="images/kurator/ava-polina.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Полина Максимова</div>
                            <div class="p-content-manager-role">куратор проекта</div>
                            <div class="p-content-manager-quote">Работать с&nbsp;командой &laquo;Несчастного случая&raquo; невероятно приятно: буквально каждый человек, имеющий отношение к&nbsp;группе, профессионал своего дела. Поэтому не&nbsp;было не&nbsp;единого сомнения, что наш &laquo;случай&raquo; окажется счастливым. Большое спасибо за&nbsp;это директору Андрею Воронцову, <nobr>пресс-атташе</nobr> Марии Петровской, всем участникам группы, и, конечно, Алексею Кортневу, который с&nbsp;воодушевлением поддерживал (и&nbsp;до&nbsp;сих пор продолжает) множество других проектов на&nbsp;Планете.
                            </div>
                        </div>

                        <div class="post-content-manager mrg-t-50 mrg-b-50">
                            <div class="p-content-manager-ava">
                                <img src="images/kurator/ava-egor.jpg">
                            </div>
                            <div class="p-content-manager-name minionpro-mediumit">Егор Ельчин</div>
                            <div class="p-content-manager-role">куратор проекта</div>
                            <div class="p-content-manager-quote">Все было <nobr>по-мужски</nobr>: обстоятельно, размеренно, уверенно. Без лишней суеты. Даже с&nbsp;излишней перестраховкой, но&nbsp;это так&hellip; на&nbsp;всякий случай) Благодаря такой собранности, еще перед стартом проекта была полная уверенность в&nbsp;его успехе. Спасибо &laquo;Несчастному случаю&raquo; за&nbsp;такой подход к&nbsp;делу и, в&nbsp;особенности,&nbsp;&mdash; Андрею Воронцову. Огромная честь быть знакомым с&nbsp;такими людьми и, уж&nbsp;тем паче, работать вместе.
                            </div>
                        </div>

                    </div>

                </div>
            </div>
                <? include 'includes/index-data.php'; ?>

        </div>
    </div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
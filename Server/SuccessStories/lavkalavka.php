<?
$title = 'Открытие фермерского магазина LavkaLavka!';

$collected = 692300;
//$target = 300000;

$dateDuration = '1 месяц 22 дня';

$startDay = 1;
$startMonth = 'июля';
$startYear = 2013;

$endDay = 22;
$endMonth = 'августа';
$endYear = 2013;

$members = 32;

$projectLink = 'https://planeta.ru/campaigns/1231';

$collected = number_format($collected, 0, '.', ' ');
$target = number_format($target, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

    <div class="post" itemscope itemtype="http://schema.org/CreativeWork">
    <div class="wrap">
        <a class="post-back-link" href="index.php">Истории успеха</a>

        <div itemprop="name" class="post-title minionpro-boldit">Открытие фермерского магазина LavkaLavka!</div>
        <div class="post-main">

            <? require 'includes/post-meta.php'; ?>

            <img itemprop="image" class="post-big-img" src="images/lavkalavka/check.jpg">

            <div class="post-middle">
                <? require 'includes/share.php' ?>
                <div class="post-content">
                    <div itemprop="description" class="post-content-text proxima-reg">
                        Сегодня спрос на&nbsp;фермерские продукты постоянно растет, особенно в&nbsp;крупных городах, где экология оставляет желать лучшего. И&nbsp;лучшее у&nbsp;нас есть&nbsp;&mdash; в&nbsp;этом уверены основатели фермерского кооператива под названием LavkaLavka. Предлагая посетителям вкусную и&nbsp;здоровую пищу по&nbsp;приемлемым ценам, они завоевали популярность среди московской публики всех возрастов. Постепенно их&nbsp;бизнес вырос и&nbsp;приблизился к&nbsp;расцвету, поэтому пришла пора пустить ростки и&nbsp;в&nbsp;новые направления рынка. Так появилась идея открытия фермерского магазина в&nbsp;центре Москвы, которую горячо поддержали пользователи &laquo;Планеты&raquo;.
                    </div>

                    <div class="post-content-manager">
                        <div class="p-content-manager-ava">
                            <img src="images/lavkalavka/ava-artist.jpg">
                        </div>
                        <div class="p-content-manager-name minionpro-mediumit">Борис Акимов</div>
                        <div class="p-content-manager-role">один из авторов проекта</div>
                        <div class="p-content-manager-quote">
                            То, что у&nbsp;нас получилось собрать денег на&nbsp;&laquo;Планете&raquo;,&nbsp;&mdash; это настоящее революционное событие! Мы, конечно, надеялись, <nobr>что-то</nobr> получится, но&nbsp;то, что мы&nbsp;перевыполним план почти на&nbsp;200 тысяч&nbsp;&mdash; о&nbsp;таком результате можно было только мечтать. Для нас это очень важно&nbsp;&mdash; это символ того, что нас поддерживают и&nbsp;нас понимают. А&nbsp;для нас это самое главное!
                        </div>
                    </div>


                    <div class="post-content-text proxima-reg mrg-b-0">
                        <div class="p-content-notice helveticaneue-bold">
                            Спасибо за&nbsp;поддержку должно быть таким, чтобы пользователи в&nbsp;ответ сами сказали &laquo;Спасибо&raquo;&nbsp;&mdash; по&nbsp;такому принципу действовали основатели LavkaLavka, разрабатывая бонусы для участников проекта. И&nbsp;бонусы действительно оказались приятными: специальный чайный набор, фермерская курица, обед на&nbsp;20 человек и&nbsp;даже поездка на&nbsp;ферму&nbsp;&mdash; естественно, с&nbsp;вкуснейшими экологически чистыми продуктами. Но&nbsp;самое главное&nbsp;&mdash; это возможность стать членом большой кооперативной семьи LavkaLavka и&nbsp;пожизненно приобретать продукты по&nbsp;внутренним ценам!
                        </div>
                    </div>

                    <div class="post-content-manager">
                        <div class="p-content-manager-ava">
                            <img src="images/kurator/ava-german.jpg">
                        </div>
                        <div class="p-content-manager-name minionpro-mediumit">Ольга Герман</div>
                        <div class="p-content-manager-role">куратор проекта</div>
                        <div class="p-content-manager-quote">
                            Открытие фермерского магазина LavkaLavka&nbsp;&mdash; первый масштабный и&nbsp;успешный проект на&nbsp;&laquo;Планете&raquo; в&nbsp;направлении &laquo;Еда&raquo;. Перед самым стартом были волнения и&nbsp;сомнения: а&nbsp;получится&nbsp;ли? Сможем&nbsp;ли мы&nbsp;вместе с&nbsp;покупателями &laquo;Лавки&raquo; открыть целый магазин?! Поддержат&nbsp;ли они такую инициативу? Смогли, поддержали и&nbsp;открыли! Спасибо ребятам из&nbsp;&laquo;Лавки&raquo; за&nbsp;отличный опыт! Уверена, что благодаря их&nbsp;успешному примеру на&nbsp;&laquo;Планете&raquo; будет появляться больше проектов фермерского движения.
                        </div>
                    </div>

                    <br><br>

                    <div class="post-milestones large-milestones">
                        <div class="post-milestones-list">
                            <div class="p-milestones-list-title minionpro-mediumit">Вехи проекта</div>
                            <div class="p-milestones-list-items">

                                <div class="pm-list-items-item">
                                    <div class="pml-items-item-date proxima-bold">1 июля 2013</div>
                                    <div class="pml-items-item-text proxima-reg">
                                        Запуск проекта.
                                    </div>
                                </div>

                                <div class="pm-list-items-item">
                                    <div class="pml-items-item-date proxima-bold">9 июля 2013</div>
                                    <div class="pml-items-item-text proxima-reg">
                                        <a href="https://planeta.ru/lavkalavka/blog/118488" target="_blank">Каталонский ужин</a> с&nbsp;Борисом Акимовым для акционеров проекта и&nbsp;покупателей &laquo;Лавки&raquo;.
                                    </div>
                                </div>

                            </div>
                            <div class="p-milestones-list-items">

                                <div class="pm-list-items-item">
                                    <div class="pml-items-item-date proxima-bold">12 августа 2013</div>
                                    <div class="pml-items-item-text proxima-reg">
                                        Состоялось открытие <a href="http://lavkagazeta.com/otvetstvennost/magazin-lavkalavka-na-patriarshih-otkrylsya" target="_blank">магазина LavkaLavka</a>, о чем написано в <a href="https://planeta.ru/planeta/blog/119213" target="_blank">блоге Planeta.ru.</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <br>

                    <div class="h-video">
                        <iframe width="720" height="405" frameborder="0" allowfullscreen="" src="https://tv.planeta.ru/video-frame?profileId=22956&amp;videoId=26242&amp;autostart=false"></iframe>
                    </div>

                </div>





                <!--<div class="post-content">
                    <a class="post-another-posts h-link minionpro-semiboldit" href="index.php">Другие успешные
                        проекты</a>
                </div>-->

            </div>

        </div>
                <?include 'includes/index-data.php';?>
    </div>
    </div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
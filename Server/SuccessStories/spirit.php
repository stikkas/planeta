<?
$title = 'Альбом Би-2 &laquo;Spirit&raquo;';

$collected = 1262250;
//$target = 300000;

$dateDuration = '1 год 4 месяца 13 дней';

$startDay = 6;
$startMonth = 'июня';
$startYear = 2011;

$endDay = 19;
$endMonth = 'октября';
$endYear = 2012;

$members = 1271;

$projectLink = 'https://planeta.ru/campaigns/19';

$collected = number_format($collected, 0, '.', ' ');
$target = number_format($target, 0, '.', ' ');
$members = number_format($members, 0, '.', ' ');
?>
<? require 'includes/header.php'; ?>

    <div class="post" itemscope itemtype="http://schema.org/CreativeWork">
    <div class="wrap">
        <a class="post-back-link" href="index.php">Истории успеха</a>

        <div itemprop="name" class="post-title minionpro-boldit">Новый альбом Би-2 &laquo;Spirit&raquo;</div>
        <div class="post-main">

            <? require 'includes/post-meta.php'; ?>

            <img itemprop="image" class="post-big-img" src="images/spirit/check.jpg">

            <div class="post-middle">
                <? require 'includes/share.php' ?>
                <div class="post-content">
                    <div itemprop="description" class="post-content-text proxima-reg">
                        Альбом &laquo;Spirit&raquo; можно назвать знаковым не&nbsp;только для группы <nobr>&laquo;Би-2&raquo;</nobr>, но&nbsp;и&nbsp;для &laquo;Планеты&raquo; в&nbsp;целом. Прежде всего, потому, что для музыкантов это был первый опыт совместной работы со&nbsp;слушателями&nbsp;&mdash; а&nbsp;для портала, имевшего в&nbsp;тот момент адрес portalplaneta.ru, это был первый опыт краудфандинга в&nbsp;России. На&nbsp;тот момент не&nbsp;существовало продуманного механизма разработки акций, широкого штата авторов и&nbsp;менеджеров по&nbsp;работе с&nbsp;артистами, и, тем не&nbsp;менее, проект стал успешным. Конечно, этому предшествовали долгие подготовительные работы: музыкантам недостаточно было назвать студию, где они собираются записывать новый альбом, а&nbsp;также предложить слушателям интересные бонусы: еще до&nbsp;запуска &laquo;Планеты&raquo; и&nbsp;проекта на&nbsp;ней <nobr>&laquo;Би-2&raquo;</nobr> вели разъяснительную работу на&nbsp;тему того, что такое краудфандинг и&nbsp;чем он&nbsp;лучше привычной схемы выпуска дисков. Именно эта подготовка и&nbsp;сделала возможным проект, который тогда казался смелой авантюрой. На&nbsp;призыв музыкантов поддержать новую пластинку откликнулись 1&nbsp;271 человек, которые собрали 1&nbsp;262&nbsp;250&nbsp;рублей на&nbsp;общее дело. Именно они заложили успех российского краудфандинга и&nbsp;позволили создать максимально эффективные механизмы взаимодействия авторов с&nbsp;поклонниками.
                    </div>

                    <div class="post-content-manager">
                        <div class="p-content-manager-ava">
                            <img src="images/spirit/ava-artist.jpg">
                        </div>
                        <div class="p-content-manager-name minionpro-mediumit">Мария Вьюгина</div>
                        <div class="p-content-manager-role">PR-директор &laquo;Би-2&raquo;</div>
                        <div class="p-content-manager-quote">
                            Мы&nbsp;начинали акционирование альбома &laquo;Spirit&raquo; 6 июня 2011 года, став первым проектом на&nbsp;Planeta.ru. Мне очень хорошо запомнился этот день и&nbsp;подготовка к&nbsp;нему, ведь была проделана грандиозная работа! А&nbsp;дальше запуск: <nobr>пресс-конференция</nobr>, ожидания, отчеты. 2 года назад мы&nbsp;многое объясняли и&nbsp;рассказывали, готовили <nobr>какие-то</nobr> длиннющие тексты, а&nbsp;теперь это просто счастье&nbsp;&mdash; наблюдать успех. И&nbsp;успех не&nbsp;только проектов <nobr>&laquo;Би-2&raquo;</nobr> на&nbsp;Planeta.ru.

                            <br><br>

                            Planeta.ru&nbsp;&mdash; это действительно Планета, и&nbsp;это замечательные люди, прежде всего! На&nbsp;100% уверена, что каждый день в&nbsp;их&nbsp;офисе царит атмосфера праздника. Иначе и&nbsp;не&nbsp;может быть, так как это <nobr>работники-творцы</nobr>, и&nbsp;дела их&nbsp;высокие&nbsp;&mdash; музыка, искусство, благотворительность&hellip; И&nbsp;даже с&nbsp;совещаний все они выходят окрыленные. Сама видела. Сама выходила.
                        </div>
                    </div>


                    <div class="post-content-text proxima-reg mrg-b-0">
                        <div class="p-content-notice helveticaneue-bold">
                            Поддерживая первый &laquo;народный&raquo; альбом группы (и&nbsp;первый проект &laquo;Планеты&raquo;), пользователи убедились в&nbsp;результативности российского краудфандинга: довольно быстро проект принес им&nbsp;плоды в&nbsp;виде цифровой и&nbsp;дисковой версии &laquo;Spirit&raquo;, а&nbsp;некоторые также получили возможность присутствовать на&nbsp;закрытой презентации пластинки.
                        </div>
                    </div>

                    <div class="post-content-manager">
                        <div class="p-content-manager-ava">
                            <img src="images/kurator/ava-vasilina.jpg">
                        </div>
                        <div class="p-content-manager-name minionpro-mediumit">Василина Горовая</div>
                        <div class="p-content-manager-role">куратор проекта</div>
                        <div class="p-content-manager-quote">
                            Запускать проекты <nobr>&laquo;Би-2&raquo;</nobr>&nbsp;&mdash; это как играть с&nbsp;друзьями в&nbsp;пейнтбол: процесс безумно веселый и&nbsp;интересный, но&nbsp;каждое мгновение стоит ждать <nobr>каких-нибудь</nobr> неожиданностей. Все потому, что у&nbsp;ребят каждый день происходит настолько много событий, что различные обновления, конкурсы, специальные мероприятия&nbsp;&mdash; это, скорее, норма для музыкантов. Это так, юмор. А&nbsp;о&nbsp;реальности хочется сказать немного другое.

                            <br><br>

                            Настолько трудолюбивых, внимательных к&nbsp;деталям и&nbsp;очень отзывчивых к&nbsp;аудитории музыкантов стоит еще поискать. Спасибо парням, Маше Вьюгиной и&nbsp;Жене Евдокимовой. Девушки&nbsp;&mdash; пусть и&nbsp;незримый, но&nbsp;очень крепкий тыл этих супермужчин. Спасибо и&nbsp;успешных проектов!
                        </div>
                    </div>

                    <br><br>

                    <div class="post-milestones large-milestones">
                        <div class="post-milestones-list">
                            <div class="p-milestones-list-title minionpro-mediumit">Вехи проекта</div>
                            <div class="p-milestones-list-items">

                                <div class="pm-list-items-item">
                                    <div class="pml-items-item-date proxima-bold">6 июня 2011</div>
                                    <div class="pml-items-item-text proxima-reg">
                                        Запуск проекта с&nbsp;одновременным запуском портала Planeta.ru (тогда еще PortalPlaneta.ru). <nobr>Пресс-концеренция</nobr> в&nbsp;РИА &laquo;Новости&raquo;.
                                    </div>
                                </div>

                            </div>
                            <div class="p-milestones-list-items">

                                <div class="pm-list-items-item">
                                    <div class="pml-items-item-date proxima-bold">1 декабря 2011</div>
                                    <div class="pml-items-item-text proxima-reg">
                                        Закрытая <a href="https://planeta.ru/b2band/blog/1401" target="_blank">презентация альбома</a> в&nbsp;КЛУБЕ &laquo;16 тонн&raquo; только для акционеров.
                                    </div>
                                </div>

                                <div class="pm-list-items-item">
                                    <div class="pml-items-item-date proxima-bold">6 апреля 2012</div>
                                    <div class="pml-items-item-text proxima-reg">
                                        Большой <a href="https://planeta.ru/b2band/blog/102161" target="_blank">концерт</a> в Stadium, завершающий проект.
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>



            </div>

        </div>
                <?include 'includes/index-data.php';?>
    </div>
    </div>
<? require 'includes/do-you-want.php'; ?>

<? require 'includes/footer.php'; ?>
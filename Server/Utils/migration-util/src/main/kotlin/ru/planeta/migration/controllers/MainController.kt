package ru.planeta.migration.controllers

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.planeta.migration.service.MigrationService
import java.io.IOException
import javax.servlet.http.HttpServletResponse

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 18.07.16<br></br>
 * Time: 09:48
 */
@RestController
class MainController(private val migrationService: MigrationService) {

    @RequestMapping(RENEW_CAMPAIGN_DESCRIPTION)
    fun renewCampaignDescription(@RequestParam("campaign_id") campaignId: Long, response: HttpServletResponse): String {
        migrationService.updateCampaignDescription(campaignId)
        return OK
    }

    @RequestMapping(COUNT_AUTHOR_PROJECTS)
    fun countAuthorProjects(@RequestParam("profile_id") profileId: Long, response: HttpServletResponse): String {
        migrationService.updateAuthorProjectCounts(profileId)
        return OK
    }

    @RequestMapping(COUNT_SUBSCRIBERS)
    fun countSubscribers(@RequestParam("profile_id") profileId: Long, response: HttpServletResponse): String {
        migrationService.updateSubscribersCounts(profileId)
        migrationService.updateNewSubscribersCounts(profileId)
        return OK
    }

    @RequestMapping(GENERATE_PROFILES_AND_PROJECTS_FOR_TECHNOBATTLE)
    fun createGroupsAndProjectsForTechnobattle(response: HttpServletResponse): String {
        migrationService.createGroupsAndProjectsForTechnobattle()
        return OK
    }

    companion object {
        const val OK = "OK"
    }
}

package ru.planeta.migration.service

import org.apache.commons.lang3.math.NumberUtils
import org.apache.log4j.Logger
import org.springframework.stereotype.Service
import ru.planeta.api.aspect.transaction.Transactional
import ru.planeta.api.service.admin.VideoUpdaterService
import ru.planeta.api.service.profile.ProfileService
import ru.planeta.dao.profiledb.ProfileSubscriptionMapperDAO
import ru.planeta.dao.profiledb.GroupDAO
import ru.planeta.dao.profiledb.ProfileDAO
import ru.planeta.dao.promo.TechnobattleProjectDAO
import ru.planeta.model.enums.GroupCategory
import ru.planeta.model.enums.ProfileStatus
import ru.planeta.model.enums.ProfileType
import ru.planeta.model.profiledb.ProfileSubscription
import ru.planeta.model.profile.Group
import ru.planeta.model.profile.Profile
import ru.planeta.model.promo.TechnobattleProject

/**
 * Migration service, which contains helper methods
 *
 * @author m.shulepov
 */
@Service
class MigrationService(private val videoUpdaterService: VideoUpdaterService,
                       private val profileService: ProfileService,
                       private val profileDAO: ProfileDAO,
                       private val groupDAO: GroupDAO,
                       private val technobattleProjectDAO: TechnobattleProjectDAO,
                       private val profileSubscriptionMapper: ProfileSubscriptionMapperDAO) {

    fun getTechnobattleProjects(): List<Map<String, String>> =
            listOf(mapOf(
                    "name" to "fishball.io - первая умная кормушка для аквариума",
                    "target_audience" to "аквариумисты и те, кто только собирается завести аквариум",
                    "description" to "<p>" + "Устройство и бесплатное приложение, которые превратят абсолютно любой аквариум в умную экосистему и полностью освободят вас от мороки по уходу за аквариумом. С ним рыбки будут сыты, а параметры воды и освещенности будут поддерживаться на нужном уровне. fishball.io исключает из процесса человека и дарит ему абсолютную свободу выбора: любой аквариум, любые рыбки - а когда придет время, fishball.io сам закажет для них нужный корм!" + "</p>",
                    "team" to "Егор Войтенков, Юрий Коновалов, Денис Ртищев, Иван Филатов, Дмитрий Сафин, Александра Алымова",
                    "video" to "//vk.com/video_ext.php?oid=-133141378&id=456239021&hash=598454af29b30af4&hd=2",
                    "image" to "https://s3.planeta.ru/i/17305e/1485524381120_renamed.jpg",
                    "sharing_title" to "Я поддерживаю разработчиков fishball.io на «Битве технологий»!",
                    "admin_profile_id" to "626453"),
                    mapOf(
                            "name" to "Модуль управления запирающими устройствами Lock4Gate",
                            "target_audience" to "Люди, использующие для доступа брелки и  магнитные карты: жильцы частных и многоквартирных домов, коттеджных и дачных посёлков, автолюбители, офисные и бизнес-центры, дошкольные, образовательные и другие учреждения, парковки и другие закрытые территории.",
                            "description" to "<p>" + "Модуль Lock4Gate позволяет управлять воротами, турникетами, шлагбаумами и другими запирающими механизмами на любом расстоянии, позволяя выдавать каждому посетителю отдельный доступ через мобильное приложение." + "</p>" +
                                    "<br><p>" + "Устройство легко подключается к любому механизму по радиосигналу и не требует монтажа, а взаимодействие с ним осуществляется при помощи мобильного приложения, веб-функционала или по SMS." + "</p>" +
                                    "<br><p>" + "Владельцы системы Lock4Gate могут разграничивать права доступа, предоставлять его неограниченному числу пользователей и подключать автоматические системы оплаты, что создает широкие возможности для применения модуля." + "</p>",
                            "team" to "Артур Ахметов, Амир Вафин, Камиль Мухаметзянов,  Денис Асханутдинов",
                            "video" to "//vk.com/video_ext.php?oid=-133141378&id=456239029&hash=c10bc91ebe3d8feb&hd=2",
                            "image" to "https://s3.planeta.ru/i/173060/1485524381945_renamed.jpg",
                            "sharing_title" to "Я поддерживаю разработчиков Lock4Gate на «Битве технологий»!",
                            "admin_profile_id" to "626534"),
                    mapOf(
                            "name" to "Электрическая приставка для инвалидного кресла UNA",
                            "target_audience" to "колясочники с активной жизненной позицией",
                            "description" to "<p>" + "Проект UNA - электрическая приставка, преобразующая обычное инвалидное кресло в электрическое, и приложение для построения доступных маршрутов. С приставкой UNA любая коляска становится проходимее, мобильнее и удобнее, но при этом не теряются преимущества механического кресла - легкость, складная конструкция и маневренность. Задача проекта -  показать маломобильным людям, что можно перемещаться по городу без помощи сопровождающих, а жизнь не должна ограничиваться стенами квартиры. Мы полностью поглощены процессом доработки, тестирования и запуска производства этих приставок, и нам нужна ваша поддержка!" + "</p>",
                            "team" to "Николай Юдин, Игорь Гаков, Сергей Костеневич, Андрей Кармацкий, Сергей Подболоцкий",
                            "video" to "//vk.com/video_ext.php?oid=-133141378&id=456239024&hash=1146733494b8fcc4&hd=2",
                            "image" to "https://s3.planeta.ru/i/173065/1485524384715_renamed.jpg",
                            "sharing_title" to "Я поддерживаю разработчиков UNA на «Битве технологий»!",
                            "admin_profile_id" to "190370"),
                    mapOf(
                            "name" to "Power Drive – зарядное устройство + flash-накопитель",
                            "target_audience" to "владельцы смартфонов",
                            "description" to "<p>" + "Power Drive - это беспроводной миниатюрный Power Bank на 3000 mAh и флешка в одном устройстве. Режимы легко переключаются боковой кнопкой, есть индикатор заряда. Обладает компактным размером и стильным дизайном, подходит для самых современных смартфонов. Вес: 75 гр.\n" +
                                    "</br>Карты памяти от 16 ГБ до 256 ГБ." + "</p>" +
                                    "<br><p>" + "Российское производство to алюминиевый корпус с покрытием, стойким к царапинам и повреждениям." + "</p>",
                            "team" to "Михаил Галицкий, Наталья Романова, Алексей Смирнов, Кристина Клецова, Алексей Брешев, Антон Козырев",
                            "video" to "//vk.com/video_ext.php?oid=-133141378&id=456239031&hash=e08835dd80e0bfb1&hd=2",
                            "image" to "https://s3.planeta.ru/i/173063/1485524383813_renamed.jpg",
                            "sharing_title" to "Я поддерживаю разработчиков Power Drive на «Битве технологий»!",
                            "admin_profile_id" to "626336"),
                    mapOf(
                            "name" to "Голосовой компьютер «Chronos»",
                            "target_audience" to "владельцы и производители цифровой техники и систем умного дома",
                            "description" to "<p>" + "Компьютер «Chronos» позволяет управлять сложной техникой при помощи голосовых команд. Аппаратная часть функционирует на базе программной платформы «Роборзоид», которая может быть настроена как для взаимодействия с одним устройством, так и с целой системой умного дома. «Chronos» обучается новым командам и воспринимает их даже в шумной обстановке, а система распознавания ошибок поможет людям с нарушениями речи без труда делать звонки, узнавать новости и погоду, включать и выключать технику и активировать ее отдельные функции." + "</p>",
                            "team" to "Мансур Сулеман, Олег Искра",
                            "video" to "//vk.com/video_ext.php?oid=-133141378&id=456239028&hash=4ba47d0d2d84c074&hd=2",
                            "image" to "https://s3.planeta.ru/i/17305d/1485524380682_renamed.jpg",
                            "sharing_title" to "Я поддерживаю разработчиков Chronos на «Битве технологий»!",
                            "admin_profile_id" to "628415"),
                    mapOf(
                            "name" to "Комплект для создания робототехники LiveTronic",
                            "target_audience" to "родители детей от 10-12 лет, поклонники робототехники",
                            "description" to "<p>" + "Развивающий робототехнический набор позволяет без применения специального оборудования создавать с помощью клеевого пистолета программируемые устройства из различных материалов – картона и пенопласта. В результате получаются довольно сложные движущиеся конструкции, которыми можно управлять с помощью  мобильного телефона." + "</p>",
                            "team" to "Александр Матвеев, Лариса Шинелёва, Варвара Аникушина, Артур Голубцов",
                            "video" to "//vk.com/video_ext.php?oid=-133141378&id=456239023&hash=a569da259fc0ec0a&hd=2",
                            "image" to "https://s3.planeta.ru/i/17305f/1485524381550_renamed.jpg",
                            "sharing_title" to "Я поддерживаю разработчиков LiveTronic на «Битве технологий»!",
                            "admin_profile_id" to "349230"),
                    mapOf(
                            "name" to "Бионический протез руки MeHand от MaxBionic",
                            "target_audience" to "люди с ампутацией рук",
                            "description" to "<p>" + "Бионический протез MeHand от MaxBionic – это настоящее технологическое чудо, замещающее утраченную конечность. Этот бионический протез может определять хрупкость и твердость предметов, рассчитывать силу сжатия, а электрический ротационный механизм позволяет вращать кисть, давая ей абсолютную свободу движения. Корпус выполнен из высокопрочного алюминиевого сплава с карбоновыми и фторопластовыми вставками." + "</p>" +
                                    "<br><p>" + "Название MeHand появилось не просто так: наши “пилоты” практически не расстаются с протезом и называют его частью себя." + "</p>",
                            "team" to "Максим Ляшко, Тимур Сайфутдинов, Александр Бекарь, Александр Похилин, Владислав Кац, Никита Тукеев, Юрий Вередюк",
                            "video" to "//vk.com/video_ext.php?oid=-133141378&id=456239025&hash=7047e8bb037e137c&hd=2",
                            "image" to "https://s3.planeta.ru/i/173061/1485524382436_renamed.jpg",
                            "sharing_title" to "Я поддерживаю разработчиков MaxBionic на «Битве технологий»!",
                            "admin_profile_id" to "626470"),
                    mapOf(
                            "name" to "Автоматическая кормушка для животных Animo",
                            "target_audience" to "владельцы кошек и собак",
                            "description" to "<p>" + "«Анимо» - это дистанционно управляемая умная кормушка для кошек и небольших собак" + "</p>" +
                                    "<br><p>" + "Она подходит для тех to чьи животные питаются по особой диете, а также для владельцев породистых кошек и собак, которые дают своим питомцам строго выверенную порцию корма. В приложении для телефона “Анимо”, разработанном нашими программистами, вы можете воспользоваться специальным алгоритмом выдачи корма. Вам нужно один раз ввести параметры вашего домашнего животного, выбрать время кормления, после чего “Анимо” рассчитает размер порции и будет выдавать ее по заданному вами расписанию. Ваш любимец не останется голодным, даже если вы задержались на работе или отправились в срочную командировку. Планируется снабдить “Анимо” функцией видеосвязи, поэтому в приложении вы можете посмотреть на то, как ест ваш питомец, или проверить текущее состояние кормушки." + "</p>" +
                                    "<br><p>" + "Наши инженеры модернизировали конструкцию так to чтобы в ней не застревал корм, а также разработали технологию циркуляции фильтрованной воды, которая будет встроена в ограниченное количество кормушек." + "</p>",
                            "team" to "Владислав Здоренко, Никита Войтенко, Владимир Галченков, Максим Сорокин, Дмитрий Скороходов, Зинаида Реуцкова, Анна Путинцева, Михаил Савкин, Алексей Николаев",
                            "video" to "//vk.com/video_ext.php?oid=-133141378&id=456239020&hash=c4b96cf8ff5aee36&hd=2",
                            "image" to "https://s3.planeta.ru/i/17305c/1485524380307_renamed.jpg",
                            "sharing_title" to "Я поддерживаю разработчиков Animo на «Битве технологий»!",
                            "admin_profile_id" to "626506"),
                    mapOf(
                            "name" to "Детская игрушка «MISHKA»",
                            "target_audience" to "Прогрессивные родители детей в возрасте от 3-6 лет. Основная целевая группа - родители в возрасте 25-45 лет со средним доходом и выше, высшим образованием, преимущественно работники умственного труда, активные пользователи Интернета и смартфонов, любители гаджетов",
                            "description" to "<p>" + "Интерактивная игрушка-компаньон “MISHKA” с ежедневно обновляемым контентом - сказками, играми и полезными советами. Плюшевый мишка, оживающий благодаря встроенному чипу с WiFi-модулем и технологией NFC, настраивается родителями через приложение, заботится о своем друге - ребенке, напоминая ему о важных вещах, рассказывая истории, пополняя его словарный запас и расширяя кругозор." + "</p>" +
                                    "<br><p>" + "“MISHKA” расскажет лишь те истории to которые помогут и не навредят малышу. Мы работаем с популярными современными авторами и издательствами. Формирование контентной сетки происходит под контролем детских психологов. Благодаря системе RFID-меток и возможности взаимодействия игрушки с Android и IOS-устройствами у “MISHKA” богатый интерактивный функционал." + "</p>" +
                                    "<br><p>" + "“MISHKA” знает to как вести себя с ребёнком вечером, знает, когда ему пора спать и как помочь проснуться, способен научить ребёнка счёту или алфавиту, может вместе с родителями прочитать сказку по ролям. “MISHKA” — такой мы представляли идеальную игрушку в своем детстве." + "</p>",
                            "team" to "Андрей Тесленко, Роман Федотов, Дмитрий Бондаренко, Александр Кузнецов, Павел Федоровский",
                            "video" to "//vk.com/video_ext.php?oid=-133141378&id=456239022&hash=362ace7d946fd6bc&hd=2",
                            "image" to "https://s3.planeta.ru/i/173062/1485524383034_renamed.jpg",
                            "sharing_title" to "Я поддерживаю разработчиков игрушки «MISHKA» на «Битве технологий»!",
                            "admin_profile_id" to "626292"),
                    mapOf(
                            "name" to "Тайм-трекер Timeflip",
                            "target_audience" to "фрилансеры, внештатные профессионалы, работники с почасовой оплатой труда, менеджеры проектов, управленцы",
                            "description" to "<p>" + "Timeflip отслеживает время, затраченное на выполнение работ в течение дня, автоматизируя систему таск-менеджмента. Устройство представляет собой шести-, восьми- или двенадцатигранник с поверхностью, на которой можно записывать различные задачи. После выполнения задачи с одной из граней достаточно перевернуть Timeflip на другую, и время завершения работы будет зафиксировано на облачной платформе. Устройство интегрируется с внешними сервисами тайм-трекинга, а также может давать рекомендации по повышению продуктивности." + "</p>",
                            "team" to "Павел Чешев, Илья Белицер, Павел Антонюк, Дмитрий Стародубцев, Игорь Ковалев",
                            "video" to "//vk.com/video_ext.php?oid=-133141378&id=456239030&hash=19e3cf079ea3a059&hd=2",
                            "image" to "https://s3.planeta.ru/i/173064/1485524384297_renamed.jpg",
                            "sharing_title" to "Я поддерживаю разработчиков Timeflip на «Битве технологий»!",
                            "admin_profile_id" to "615548"))


    fun updateCampaignDescription(campaignId: Long) = videoUpdaterService.updateCampaignDescription(campaignId, null)

    fun updateAuthorProjectCounts(profileId: Long) = profileDAO.updateProjectsCount(profileId)

    fun updateSubscribersCounts(profileId: Long) = profileDAO.updateSubscribersCount(profileId)

    fun updateNewSubscribersCounts(profileId: Long) = profileService.updateNewSubscribersCount(profileId)

    @Transactional
    fun createGroupsAndProjectsForTechnobattle() {
        getTechnobattleProjects().forEach {

            //---------- creating profile for project --------------

            val groupProfile = Profile()
            groupProfile.profileType = ProfileType.GROUP
            groupProfile.status = ProfileStatus.GROUP_ACTIVE
            groupProfile.groupCategory = GroupCategory.OTHER
            groupProfile.displayName = it["name"]
            groupProfile.creatorProfileId = 6 // VASL profile ID
            groupProfile.imageUrl = it["image"]

            profileDAO.insert(groupProfile)

            //---------- creating group for group profile --------------

            val group = Group()
            group.profileId = groupProfile.profileId

            groupDAO.insert(group)

            //---------- creating project with profile ID --------------

            val project = TechnobattleProject()
            project.title = it["name"]
            project.consumers = it["target_audience"]
            project.description = it["description"]
            project.team = it["team"]
            project.smallImageUrl = it["image"]
            project.videoSrc = it["video"]
            project.sharingTitle = it["sharing_title"]
            project.profileId = groupProfile.profileId

            technobattleProjectDAO.insert(project)

            //---------- creating relations between admin and project profile --------------

            val adminProfileId = it["admin_profile_id"]
            log.info(adminProfileId)
            adminProfileId?.let {
                profileSubscriptionMapper.insert(ProfileSubscription.Builder()
                        .profileId(NumberUtils.toLong(adminProfileId))
                        .subjectProfileId(groupProfile.profileId)
                        .isAdmin(true)
                        .build())
            }
        }

    }

    companion object {
        private val log = Logger.getLogger(MigrationService::class.java)
    }

}

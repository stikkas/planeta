package ru.planeta.migration.controllers

/**
 * User: m.shulepov
 * Date: 19.04.12
 * Time: 14:06
 */
const val RENEW_CAMPAIGN_DESCRIPTION = "/renew-campaign-description"
const val COUNT_AUTHOR_PROJECTS = "/count-author-projects"
const val COUNT_SUBSCRIBERS = "/count-subscribers"
const val GENERATE_PROFILES_AND_PROJECTS_FOR_TECHNOBATTLE = "/generate-profiles-and-projects-for-technobattle"

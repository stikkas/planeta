package ru.planeta.migration

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.ImportResource

/**
 *
 * User: Serge Blagodatskih<stikkas17></stikkas17>@gmail.com><br></br>
 * Date: 18.07.16<br></br>
 * Time: 09:31
 */
@ImportResource(locations = ["classpath*:/ru/planeta/spring/propertyConfigurer.xml",
    "classpath*:/spring/applicationContext-dao.xml", "classpath*:/spring/applicationContext-geo.xml"])
@SpringBootApplication(scanBasePackages = ["ru.planeta"])
class MigrationApp {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(MigrationApp::class.java, *args)
        }
    }
}

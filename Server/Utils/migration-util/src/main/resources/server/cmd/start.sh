#!/bin/bash
chown -R tomcat6.tomcat6 /opt/java/migration_util
/sbin/start-stop-daemon --start --quiet --oknodo -m -c tomcat6 -d /opt/java/migration_util/ --pidfile /var/run/planeta/migration_util.pid --exec /bin/bash /opt/java/migration_util/run.sh &
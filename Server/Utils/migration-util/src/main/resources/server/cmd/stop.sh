#!/bin/sh

/sbin/start-stop-daemon --stop --quiet --oknodo -m -c tomcat6 -d /opt/java/planeta_migration_util/ \
--pidfile /var/run/planeta/planeta_migration_util.pid --exec /bin/bash /opt/java/planeta_migration_util/bin/run.sh &

ps -ef | grep 'port=8090' | awk '{print $2}' | xargs kill
rm -f /var/run/planeta/planeta_migration_util.lock

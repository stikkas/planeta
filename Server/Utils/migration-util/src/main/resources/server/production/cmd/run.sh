#!/bin/sh

export JAVA_HOME=/usr/lib/jvm/jdk1.7.0_80
export LANG=en_US.UTF-8

LD_LIBRARY_PATH=`pwd`
export LD_LIBRARY_PATH
touch /var/run/planeta/planeta_migration_util.lock
$JAVA_HOME/bin/java -Xms128m -Xmx256m -jar migration-util-1.0-SNAPSHOT.jar --server.port=8090 --spring.profiles.active=prod --logging.file=/opt/java/planeta_migration_util/logs/migration-util.log

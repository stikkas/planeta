/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.planeta.migration.service

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.util.Assert.notNull
import ru.planeta.api.service.admin.VideoUpdaterService
import ru.planeta.dao.profiledb.ProfileDAO
import ru.planeta.migration.MigrationApp

@RunWith(SpringRunner::class)
@SpringBootTest(classes = [(MigrationApp::class)])
class MigrationServiceTest {

    @Autowired
    lateinit var videoUpdaterService: VideoUpdaterService

    @Autowired
    lateinit var profileDAO: ProfileDAO

    @Test
    fun testUpdateCampaignDescription() = notNull(videoUpdaterService, "Video Updater Service is NULL")

    @Test
    fun testUpdateAuthorProjectCounts() = notNull(profileDAO, "Profile DAO is NULL")

}

package ru.planeta.test

import org.apache.log4j.Logger
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import org.xml.sax.SAXException
import ru.planeta.api.service.admin.VideoParser
import ru.planeta.api.service.admin.VideoUpdaterService
import ru.planeta.dao.profiledb.VideoDAO
import ru.planeta.migration.MigrationApp
import java.io.IOException

/**
 * Created by a.savanovich on 02.12.2015.
 */
@Transactional
@RunWith(SpringRunner::class)
@SpringBootTest(classes = [MigrationApp::class])
class DescriptionVideoTransferTest {

    @Autowired
    lateinit var videoDAO: VideoDAO

    @Autowired
    lateinit var videoUpdaterService: VideoUpdaterService

    internal var htmlFlot = "<p><span>&quot;<i>Друзья. Мы, &quot;Северный Флот&quot;, начинаем подготовку к записи нашего второго альбома. Пробный сингл, включающий песни &quot;Революция На Вылет&quot; и &quot;Поднимая Знамя&quot;, показал нам, что мы на верном пути в поиске звука, и студия <a target=\"_blank\" href=\"/api/util/away.html?to=http%3A%2F%2Fvk.com%2Fastiastudio\">ASTIA </a>в Финляндии, и саундпродюсер <a target=\"_blank\" href=\"/api/util/away.html?to=http%3A%2F%2Fvk.com%2Fanssikippo\">Ансси Киппо</a>, с которым мы имели честь работать над синглом, подходят нам как нельзя лучше.</i></span></p>\n" +
            "<p><span><p:photo id=\"927282\" owner=\"412172\" image=\"http://s1.planeta.ru/p?url=https%3A%2F%2Fpp.vk.me%2Fc628231%2Fv628231783%2F1642c%2FqLs4YK7dur8.jpg&amp;width=1000&amp;height=1000&amp;crop=false&amp;pad=false&amp;disableAnimatedGif=false\"></p:photo></span></p>\n" +
            "<p><i>Для группы &quot;Северный Флот&quot; главным приоритетом всегда было и является сейчас качество записанного материала. По нынешним временам это очень недешево, и мы просим своих друзей и подруг, братьев и сестер, каждого, кто неравнодушен к нашему творчеству, помочь нам в осуществлении наших планов. Мы приглашаем Вас принять участие в краудфандинговом проекте на сайте Планета.ру в поддержку нашего будущего альбома.</i></p>\n" +
            "<p><i>Все собранные средства будут направлены на оплату студии, работу звукорежиссера по записи, сведению и мастерингу альбома.</i></p>\n" +
            "<p><i>Поднимем Знамя Вместе!</i></p>\n" +
            "<p><span></span><span><b>Группа &quot;Северный Флот&quot;</b>.</span></p>\n" +
            "<p><span><p:video duration=\"252\" id=\"47762\" name=\"Северный Флот - Вперёд и Вверх\" owner=\"412037\" image=\"http://s2.planeta.ru/p?url=https%3A%2F%2Fi.ytimg.com%2Fvi%2FH9IOTzKY_Qk%2Fsddefault.jpg&amp;width=400&amp;height=300&amp;pad=true\"></p:video></span></p>\n" +
            "<p><a class=\"mem_link\" target=\"_blank\" href=\"/api/util/away.html?to=https%3A%2F%2Fvk.com%2Fsever_flot\" mention=\"\" mention_id=\"club62330024\">&laquo;Северный Флот&raquo;</a>&nbsp;- группа, созданная музыкантами группы &laquo;Король и Шут&raquo; летом 2013 года. Дебютный сингл музыкантов - &laquo;Стрелы&raquo; - сразу же взлетел на первое место хит-парада &laquo;НАШЕго Радио&raquo;, молниеносно завоевав сердца поклонников и заявив о появлении новой музыкальной единицы &ndash; качественной, самобытной, бескомпромиссной. Теперь легендарные рок-музыканты под уже полюбившимся флагом дают полноформатные концерты по всей стране и даже больше &ndash; первый же тур &laquo;Северного Флота&raquo; включил в себя около 40 городов России и Беларуси. Дебютный альбом группы под названием &laquo;Всё внутри&raquo; за неделю занял лидирующие места в рейтингах Google play и iTunes, вызвав широкий резонанс среди слушателей.</p>\n" +
            "<p><span><span><p:photo id=\"927281\" owner=\"412172\" image=\"http://s1.planeta.ru/p?url=https%3A%2F%2Fpp.vk.me%2Fc627722%2Fv627722783%2F81e4%2FgWqIN5OfsSk.jpg&amp;width=1000&amp;height=1000&amp;crop=false&amp;pad=false&amp;disableAnimatedGif=false\"></p:photo></span></span></p>\n" +
            "<p>В июне этого года коллектив представил две новые композиции и теперь готовит ещё несколько релизов, презентация которых пройдёт в рамках ряда осенних концертов &laquo;Северного Флота&raquo;. В программу грядущих выступлений войдут не только знакомые боевики из альбома &laquo;Всё внутри&raquo; и свежие синглы, но и знаковые композиции &laquo;Короля и Шута&raquo;, а также номера из зонг-оперы TODD.&nbsp;</p>"

    @Test
    fun test() {
        try {
            val handler = VideoParser(videoDAO)
            handler.setVideoUpdater(null)
            val result = handler.replace(htmlFlot)
            log.info(result)
        } catch (e: IOException) {
            log.error(e)
        } catch (e: SAXException) {
            log.error(e)
        }

    }

    @Test
    fun testDescription() = videoUpdaterService.updateCampaignDescription(19, null)

    companion object {
        private val log = Logger.getLogger(DescriptionVideoTransferTest::class.java)
    }
}

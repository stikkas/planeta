/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.planeta.migration.controllers

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpStatus
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.util.Assert.isTrue
import ru.planeta.migration.MigrationApp

/**
 *
 * @author Serge Blagodatskih<stikkas17></stikkas17>@gmail.com>
 */
@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = [MigrationApp::class])
class MainControllerTest {

    @Autowired
    lateinit var restTemplate: TestRestTemplate

    private val fakedId = -134

    @Test
    fun testRenewCampaignDescription() {
        val responseEntity = restTemplate.getForEntity(String.format("%s?campaign_id=%d", RENEW_CAMPAIGN_DESCRIPTION, fakedId), String::class.java)
        isTrue(responseEntity.statusCode == HttpStatus.OK)
        isTrue(responseEntity.body == "OK")
    }

    @Test
    fun testCountAuthorProjects() {
        val responseEntity = restTemplate.getForEntity(String.format("%s?profile_id=%d", COUNT_AUTHOR_PROJECTS, fakedId), String::class.java)
        isTrue(responseEntity.statusCode == HttpStatus.OK)
        System.out.println()
        isTrue(responseEntity.body == "OK")
    }

}

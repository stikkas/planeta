#!/bin/sh

mvn install:install-file -DgroupId=org.sphx.api -DartifactId=sphinx-api -Dversion=2.01-beta -Dpackaging=jar -Dfile=./sphinxapi-2.01-beta.jar
mvn install:install-file -DgroupId=kefir-bb -DartifactId=kefir-bb -Dversion=0.6 -Dpackaging=jar -Dfile=./kefir-bb-0.6.jar
mvn install:install-file -DgroupId=org.krysalis.barcode4j -DartifactId=barcode4j -Dversion=2.1.0 -Dpackaging=jar -Dfile=./barcode4j.jar
mvn install:install-file -DgroupId=com.ibm.icu -DartifactId=icu -Dversion=4.8.1.1 -Dpackaging=jar -Dfile=./icu4j-4_8_1_1.jar
mvn install:install-file -DgroupId=com.googlecode -DartifactId=jsonplugin -Dversion=0.34 -Dpackaging=jar -Dfile=./jsonplugin-0.34.jar
mvn install:install-file -DgroupId=im4java -DartifactId=im4java -Dversion=1.1.0 -Dpackaging=jar -Dfile=./im4java-1.1.0.jar
mvn install:install-file -DgroupId=de.umass.lastfm -DartifactId=lastfm -Dversion=0.1.1 -Dpackaging=jar -Dfile=./last.fm-bindings-0.1.1.jar
mvn install:install-file -DgroupId=com.asual.lesscss -DartifactId=lesscss-engine -Dversion=1.1.5-beta2 -Dpackaging=jar -Dfile=./lesscss-engine-1.1.5-SNAPSHOT.jar
mvn install:install-file -DgroupId=com.asual.lesscss -DartifactId=lesscss-servlet -Dversion=1.1.5-beta2 -Dpackaging=jar -Dfile=./lesscss-servlet-1.1.5-SNAPSHOT.jar
mvn install:install-file -DgroupId=net.sf.ehcache -DartifactId=ehcache -Dversion=2.5.1 -Dpackaging=jar -Dfile=./ehcache-core-2.5.1.jar
mvn install:install-file -DgroupId=com.maxmind -DartifactId=geoip -Dversion=1.1 -Dpackaging=jar -Dfile=./geoip-1.1.jar